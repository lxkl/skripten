\begin{para}[CDF einer ZV]
  Analog zur PMF definiere die \term{Verteilungsfunktion} oder \term{kumulative Verteilungsfunktion}
  oder \term{cumulative distribution function (CDF)} der ZV $X$:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{F_{X}}{\RR}{\intcc{0}{1}}{u \mapsto \prob{X \leq u}}
  \end{IEEEeqnarray*}
  Offenbar ist diese monoton steigend.
  Unter der \term{Verteilung} von $X$ verstehen wir jede Information,
  aus der man die CDF von $X$ erschließen kann.
  Aus der CDF wiederum kann man die PMF erschließen (Beweis folgt) und daraus,
  wie oben gezeigt, auch $\prob{X \in U}$ für alle $U \subseteq \RR$,
  also die W'keit aller Events, die man über $X$ formulieren kann.
  Wir werden im diskreten Fall fast nur mit der PMF arbeiten.
  Die CDF ist vor allem im nicht\-/diskreten Fall interessant
\end{para}

\begin{satz}[Eigenschaften der CDF]
  Für alle $u \in \RR$ gilt $\prob{X=u} = F_{X}(u) - \limx{k} $
\end{satz}

\begin{satz}[CDF ergibt PMF]
  Für alle $u \in \RR$ gilt $\prob{X=u} = F_{X}(u) - \limx{k} $
\end{satz}

  Wir zeigen, dass $\famij{X_{j}}$ unabhängig ist für alle $J \subseteq I$,
  indem wir Induktion über $\card{J}$ führen.
  Der Induktionsanfang $\card{J} = 0$ ist trivial;
  also sei $\card{J} \geq 1$ und wähle $k \in J$.
  Es gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{\bigwedge_{j \in J} \p{X_{j} \in U_{j}}}
    = \prob{\p{X_{k} \in U_{k}} \land \bigwedge_{j \in J \setminus \set{k}} \p{X_{j} \in U_{j}}}
  \end{IEEEeqnarray*}


  und $\ffn{\al}{I}{\setn{n}}$ mit $n \in \NN$ sei eine Umindizierung.

  \begin{IEEEeqnarray*}{0l}
    \prob{\bigwedge_{i \in I} \p{X_{i} \in U_{i}}}
    = \prob{\dotbigvee_{\famii{u_{i}} \in U} \bigwedge_{i \in I} \p{X_{i}=u_{i}}}
    = \sum_{\famii{u_{i}} \in U} \prob{\bigwedge_{i \in I} \p{X_{i}=u_{i}}} \\
    = \sum_{\famii{u_{i}} \in U} \prod_{i\in I} \prob{X_{i} = u_{i}}
    = \sum_{u_{\al(1)} \in U_{\al(1)}} \hdots \sum_{u_{\al(n)} \in U_{\al(n)}}
    \prod_{i\in I} \prob{X_{i} = u_{i}} \\
    = \sum_{u_{\al(1)} \in U_{\al(1)}} \prob{X_{\al(1)} = u_{\al(1)}}
    \quad \hdots \sum_{u_{\al(n)} \in U_{\al(n)}} \prob{X_{\al(n)} = u_{\al(n)}}
  \end{IEEEeqnarray*}

    \prob{\bigwedge_{i \in I} \p{X_{i} \in U_{i}}}
    = \prob{\dotbigvee_{\famii{u_{i}} \in U} \bigwedge_{i \in I} \p{X_{i}=u_{i}}} \\
  \begin{IEEEeqnarray*}{0l"s}
    \expect{\abs{X}} \ccdot \expect{\abs{Y}}
    = \sum_{u \in \supp(X)} \abs{u} \ccdot \prob{X=u}
    \sum_{v \in \supp(Y)} \abs{v} \ccdot \prob{Y=v} \\
    = \sum_{u \in \supp(X)} \sum_{v \in \supp(Y)} \abs{uv} \ccdot \prob{X=u} \ccdot \prob{Y=v} \\
    = \sum_{u \in \supp(X)} \sum_{v \in \supp(Y)} \abs{uv} \ccdot \prob{X=u \land Y=v}
    & Unabhängigkeit \\
    = \sum_{u \in \supp(X) \setminus \set{0}} \sum_{v \in u \supp(Y)}
    \abs{v} \ccdot \prob{X=u \land Y=\frac{v}{u}}
  \end{IEEEeqnarray*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  Im allgemeinen Fall benötigt man ein Argument; es gilt mit LOTUS:
  \begin{IEEEeqnarray*}{0l"s}
    \RR \ni \expect{\abs{X}} \ccdot \expect{\abs{Y}} \\
    = \sum_{u \in \RR} \abs{u} \ccdot \prob{X=u}
    \sum_{v \in \RR} \abs{v} \ccdot \prob{Y=v} \\
    = \sum_{u \in \RR} \sum_{v \in \RR} \abs{uv} \ccdot \prob{X=u} \ccdot \prob{Y=v} \\
    = \sum_{u \in \RR} \sum_{v \in \RR} \abs{uv} \ccdot \prob{X=u \land Y=v}
    & Unabhängigkeit \\
    = \sum_{u \in \RRnz} \sum_{v \in u \RR}
    \abs{u\frac{v}{u}} \ccdot \prob{X=u \land Y=\frac{v}{u}} \\
    = \sum_{u \in \RRnz} \sum_{v \in \RR}
    \abs{v} \ccdot \prob{X=u \land Y=\frac{v}{u}}
    & $\forall u \in \RRnz \holds u \RR = \RR$ \\
    = \sum_{v \in \RRnz} \abs{v} \sum_{u \in \RRnz} \prob{X=u \land Y=\frac{v}{u}} \\
    = \sum_{v \in \RRnz} \abs{v} \sum_{u \in \RRnz} \prob{X=u \land XY=v} \\
    = \sum_{v \in \RRnz} \abs{v} \sum_{u \in \RR} \prob{X=u \land XY=v}
    & $\forall v \in \RRnz \holds \prob{X=0 \land XY=v} = 0$ \\
    = \sum_{v \in \RRnz} \abs{v} \ccdot \prob{XY=v}
    = \sum_{v \in \RR} \abs{v} \ccdot \prob{XY=v}
  \end{IEEEeqnarray*}
  Wieder nach LOTUS hat $XY$ also einen EW.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  \begin{IEEEeqnarray*}{0l}
    \sum_{\om \in \Lam} \abs{X(\om)} \ccdot \prob{\om}
    = \sum_{u \in \fnimg{\abs{X}}(\Lam)} \sum_{\substack{\om \in \Lam \\ \aba{X(\om)}=u}} X(\om) \ccdot \prob{\om}
    = \sum_{u \in \fnimg{\abs{X}}(\Lam)} u \sum_{\substack{\om \in \Lam \\ \abs{X(\om)}=u}} \prob{\om} \\
    \leq \sum_{u \in \fnimg{X}(\Lam)} \abs{u} \sum_{\substack{\om \in \Lam \\ \abs{X(\om)}=\abs{u}}} \prob{\om}
    \leq \sum_{u \in \fnimg{X}(\Lam)} \abs{u} \ccdot \prob{\abs{X(\om)}=\abs{u}} \\
    = \sum_{u \in \fnimg{X}(\Lam)} \abs{u} \ccdot \p{\prob{X(\om)=u} + \prob{X(\om)=-u}} \\
    = \sum_{u \in \fnimg{X}(\Lam)} \abs{u} \ccdot \prob{X(\om)=u}
    + \sum_{u \in \fnimg{X}(\Lam)} \abs{-u} \ccdot \prob{X(\om)=-u}
    \leq 2 s
  \end{IEEEeqnarray*}

  Zunächst habe $X$ einen EW.
  Dann gilt:
  \begin{IEEEeqnarray*}{0l"s}
    \sum_{u \in \supp(X)} \abs{u} \ccdot \prob{X=u}
    = \sum_{u \in \supp(X)} \abs{u} \sum_{\substack{\om \in \Om \\ X(\om)=u}} \prob{\om}
    & Additivität \\
    = \sum_{u \in \supp(X)} \sum_{\substack{\om \in \Om \\ X(\om)=u}} \abs{u} \ccdot \prob{\om}
    & Skalierung \\
    = \sum_{u \in \supp(X)} \sum_{\substack{\om \in \Om \\ X(\om)=u}} \abs{X(\om)} \ccdot \prob{\om} \\
    = \sum_{\om \in \Om} \abs{X(\om)} \ccdot \prob{\om}
    & GU (???)
  \end{IEEEeqnarray*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{proof}
  Für alle $u \in \img(X)$ gilt nach der Additivität:
  \begin{IEEEeqnarray}{0l}
    \label{stochainf/cha04:eq-u-vu}
    \begin{IEEEeqnarraybox}[][c]{l}
      \abs{f(u)} \ccdot \prob{X=u}
      = \abs{f(u)} \sum_{v \in \supp(f \circ X)} \prob{f \circ X = v \land X=u} \\
      = \sum_{v \in \supp(f \circ X)} \abs{v} \ccdot \prob{f \circ X = v \land X=u}
    \end{IEEEeqnarraybox}
  \end{IEEEeqnarray}
  Es habe nun $f \circ X$ einen EW.
  Dann gilt:
  \begin{IEEEeqnarray*}{0l"s}
    \pinfty > \sum_{v \in \supp(f \circ X)} \abs{v} \ccdot \prob{f \circ X = v} \\
    = \sum_{v \in \supp(f \circ X)} \abs{v} \sum_{u \in \supp(X)} \prob{f \circ X = v \land X=u}
    & Additivität \\
    = \sum_{v \in \supp(f \circ X)} \sum_{u \in \supp(X)} \abs{v} \ccdot \prob{f \circ X = v \land X=u}
    & Skalierung \\
    = \sum_{u \in \supp(X)} \sum_{v \in \supp(f \circ X)} \abs{v} \ccdot \prob{f \circ X = v \land X=u}
    & GU \\
    = \sum_{u \in \supp(X)} \abs{f(u)} \ccdot \prob{X=u}
    & nach \eqref{stochainf/cha04:eq-u-vu}
  \end{IEEEeqnarray*}
  Vor der Anwendung des GU haben wir an der bisherigen Umformung durch die Umkehrung des GU bemerkt,
  dass folgende Familie summierbar ist:
  \begin{IEEEeqnarray*}{0l}
    \fami{f(u) \ccdot \prob{f \circ X = v \land X = u}}{(v,u)}{\supp(f \circ X) \times \supp(X)}
  \end{IEEEeqnarray*}
\end{proof}

\begin{proof}
  Die äußere Struktur des Beweises ist ähnlich wie bei~\autoref{stochainf/cha04:ew-om}.
  Zunächst habe $f \circ X$ einen EW,
  notiere $s \df \sum_{v \in \supp(f \circ X)} \abs{v} \ccdot \prob{f \circ X = v}$.
  Für alle $\Lam \subseteq \supp(X)$ endlich gilt:
  \begin{IEEEeqnarray*}{0l}
    \sum_{u \in \Lam} \abs{f(u)} \ccdot \prob{X=u} \\
    = \sum_{u \in \Lam} \abs{f(u)} \sum_{v \in \fnimg{f}(\Lam)} \prob{f \circ X = v \land X=u} \\
    = \sum_{u \in \Lam} \sum_{v \in \fnimg{f}(\Lam)} \abs{v} \ccdot \prob{f \circ X = v \land X=u} \\
    = \sum_{v \in \fnimg{f}(\Lam)} \sum_{u \in \Lam} \abs{v} \ccdot \prob{f \circ X = v \land X=u} \\
    = \sum_{v \in \fnimg{f}(\Lam)} \abs{v} \sum_{u \in \Lam} \prob{f \circ X = v \land X=u} \\
    \leq \sum_{v \in \fnimg{f}(\Lam)} \abs{v} \ccdot \prob{f \circ X = v}
    \leq s
  \end{IEEEeqnarray*}
  Also ist $\fami{f(u) \ccdot \prob{X=u}}{u}{\supp(X)}$ summierbar.
  \par
  Für die andere Richtung sei $\fami{f(u) \ccdot \prob{X=u}}{u}{\supp(X)}$ summierbar.
  Für alle ${u \in \supp(X)}$ gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{X=u} = \sum_{v \in \supp(f \circ X)} \prob{f \circ X = v \land X = u}
  \end{IEEEeqnarray*}
  Da bei dieser Zerlegung keine negativen Zahlen vorkommen,
  ist auch folgende Familie summierbar:
  \begin{IEEEeqnarray*}{0l}
    \fami{f(u) \ccdot \prob{f \circ X = v \land X = u}}{(v,u)}{\supp(f \circ X) \times \supp(X)}
  \end{IEEEeqnarray*}
  Nach dem großen Umordnungssatz, angewendet auf diese Familie,
  gilt (fast dieselbe Rechnung wie oben):
  \begin{IEEEeqnarray*}{0l}
    \pinfty > \sum_{u \in \supp(X)} \abs{f(u)} \ccdot \prob{X=u} \\
    = \sum_{u \in \supp(X)} \abs{f(u)} \sum_{v \in \supp(f \circ X)} \prob{f \circ X = v \land X=u} \\
    = \sum_{u \in \supp(X)} \sum_{v \in \supp(f \circ X)} \abs{f(u)} \ccdot \prob{f \circ X = v \land X=u} \\
    = \sum_{v \in \supp(f \circ X)} \sum_{u \in \supp(X)} \abs{f(u)} \ccdot \prob{f \circ X = v \land X=u} \\
    = \sum_{v \in \supp(f \circ X)} \sum_{u \in \supp(X)} \abs{v} \ccdot \prob{f \circ X = v \land X=u} \\
    = \sum_{v \in \supp(f \circ X)} \abs{v} \sum_{u \in \supp(X)} \prob{f \circ X = v \land X=u} \\
    = \sum_{v \in \supp(f \circ X)} \abs{v} \ccdot \prob{f \circ X = v}
  \end{IEEEeqnarray*}
  Also hat $f \circ X$ einen EW.
  Die Formel folgt schließlich aus obiger Rechnung ohne die Beträge.
\end{proof}

\begin{proof}
  Definiere $\tiOm \df \img(X)$ und definiere das W'maß $\tiprob{}$
  auf dem Eventraum $(\tiOm, \cP(\tiOm))$ durch $\tiprob{u} \df \prob{X=u}$ für alle $u \in \tiOm$.
  Mit $\tiexpect{}$ \bzw $\tisupp$ bezeichnen wir den EW \bzw Träger
  von ZV über $(\tiOm, \cP(\tiOm), \tiprob{})$.
  Es gilt:
  \begin{IEEEeqnarray*}{0l}
    \tiexpect{f} \in \RR
    \textrel{\autoref{stochainf/cha04:ew-om}}{\iff} \sum_{u \in \tiOm} \abs{f(u)} \ccdot \tiprob{u} < \pinfty
    \iff \sum_{u \in \tiOm} \abs{f(u)} \ccdot \prob{X=u} < \pinfty
  \end{IEEEeqnarray*}
  Auch nach \autoref{stochainf/cha04:ew-om} gilt im Falle der Existenz
  $\tiexpect{f} = \sum_{u \in \tiOm} f(u) \ccdot \prob{X=u}$.
  \par
  Ferner gilt:
  \begin{IEEEeqnarray*}{0l}
    \tiexpect{f} \in \RR
    \iff \sum_{v \in \tisupp(f)} \abs{v} \ccdot \tiprob{f = v} < \pinfty
  \end{IEEEeqnarray*}
\end{proof}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  Notiere $s_{X} \df \sum_{u \in \supp(X)} \abs{u} \ccdot \prob{X=u}$
  und $s_{Y} \df \sum_{v \in \supp(Y)} \abs{v} \ccdot \prob{Y=v}$.
  Es sei $\Lam \subseteq \supp(X,Y)$ endlich.
  Dann gibt es $A \subseteq \supp(X)$ und $B \subseteq \supp(Y)$ endlich so,
  dass $\Lam \subseteq A \times B$ gilt. Es gilt:
  \begin{IEEEeqnarray*}{0l}
    \sum_{(u,v) \in \Lam} \abs{\lam_{X}u + \lam_{Y}v} \ccdot \prob{(X,Y)=(u,v)} \\
    \leq \sum_{(u,v) \in A \times B} \p{\abs{\lam_{X}u} + \abs{\lam_{Y}v}} \ccdot \prob{(X,Y)=(u,v)} \\
    = \sum_{u \in A} \abs{\lam_{X}u} \sum_{v \in B} \prob{(X,Y)=(u,v)}
    + \sum_{v \in B} \abs{\lam_{Y}v} \sum_{u \in A} \prob{(X,Y)=(u,v)} \\
    \leq \sum_{u \in A} \abs{\lam_{X}u} \ccdot \prob{X=u} + \sum_{v \in B} \abs{\lam_{Y}v} \ccdot \prob{Y=v} \\
    = \abs{\lam_{X}} \sum_{u \in A} \abs{u} \ccdot \prob{X=u}
    + \abs{\lam_{Y}} \sum_{v \in B} \abs{v} \ccdot \prob{Y=v} \\
    \leq \abs{\lam_{X}} \ccdot s_{X} + \abs{\lam_{Y}} \ccdot s_{Y}
  \end{IEEEeqnarray*}
  Nach LOTUS hat also $f \circ X$ einen EW.
  Weiter mit LOTUS gilt:

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  Wir möchten LOTUS auf $f \circ (X,Y)$ anwenden.
  Notiere $s_{X} \df \sum_{u \in \supp(X)} \abs{u} \ccdot \prob{X=u}$
  und $s_{Y} \df \sum_{v \in \supp(Y)} \abs{v} \ccdot \prob{Y=v}$.
  Es sei $\Lam \subseteq \supp(X,Y)$ endlich.
  Dann gibt es $A \subseteq \supp(X)$ und $B \subseteq \supp(Y)$ endlich so,
  dass $\Lam \subseteq A \times B$ gilt. Es gilt:
  \begin{IEEEeqnarray*}{0l}
    \sum_{(u,v) \in \Lam} \abs{\lam_{X}u + \lam_{Y}v} \ccdot \prob{(X,Y)=(u,v)} \\
    \leq \sum_{(u,v) \in A \times B} \p{\abs{\lam_{X}u} + \abs{\lam_{Y}v}} \ccdot \prob{(X,Y)=(u,v)} \\
    = \sum_{u \in A} \abs{\lam_{X}u} \sum_{v \in B} \prob{(X,Y)=(u,v)}
    + \sum_{v \in B} \abs{\lam_{Y}v} \sum_{u \in A} \prob{(X,Y)=(u,v)} \\
    \leq \sum_{u \in A} \abs{\lam_{X}u} \ccdot \prob{X=u} + \sum_{v \in B} \abs{\lam_{Y}v} \ccdot \prob{Y=v} \\
    = \abs{\lam_{X}} \sum_{u \in A} \abs{u} \ccdot \prob{X=u}
    + \abs{\lam_{Y}} \sum_{v \in B} \abs{v} \ccdot \prob{Y=v} \\
    \leq \abs{\lam_{X}} \ccdot s_{X} + \abs{\lam_{Y}} \ccdot s_{Y}
  \end{IEEEeqnarray*}
  Nach LOTUS hat also $f \circ X$ einen EW.
  Weiter mit LOTUS gilt:

  So erhalten wir einen W'raum $(\RR^{I}, \cB_{I}, \tiprob{})$,
  wobei $\ffnx{\tiprob{}}{\cB_{I}}{\intcc{0}{1}}{U \mapsto \prob{X \in U}}$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{para}[CDF]
  Man kann außedem zeigen, dass die Verteilung von $X$ bereits eindeutig festgelegt ist,
  wenn sie auf $\cG_{I}$ feststeht.
  Das führt auf den folgenden Begriff,
  den wir schon kurz in \autoref{stochainf/cha03:ex-patient} gesehen hatten.
  Die folgende Funktion nennen wir die \term{kumulative Verteilungsfunktion}
  oder \term{cumulative distribution function (CDF)} der Familie $X$:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{F_{X}}{\RR^{I}}{\intcc{0}{1}}{u \mapsto \prob{X \leq u}}
  \end{IEEEeqnarray*}
  Dabei ist $X \leq u$ komponentenweise gemeint, d.h. $\forall i \in I \holds X_{i} \leq u_{i}$.
  Offenbar kennen wir die Verteilung von $X$ auf $\cG_{I}$ genau dann,
  wenn wir die CDF von $X$ kennen.
  Somit gilt: Die Verteilung von $X$ ist bereits durch die CDF von $X$ festgelegt.
\end{para}

  \par
  Der Vollständigkeit halber halten wir die dortigen Aussagen in der Sprache der Borelmengen fest.
  Für alle $U, V \in \cB^{I}$ gilt:
  \begin{IEEEeqnarray*}{0l+l+l}
    \RR^{I} \setminus U \in \cB^{I} & U \cap V \in \cB^{I}& \vphi \lor \psi
  \end{IEEEeqnarray*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

$X=\famiiint{X_{i}}{n}$ unabhängig mit $X_{i} \distr \cD^{\hat{\vtet}}_{i}$ für alle $i \in \setn{n}$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  Eigentlich müsste man $X^{\vtet}_{i}$ und $Y^{\vtet}_{i}$ schreiben;
  wir schreiben stattdessen $\probparam{}$ für das W'maß des unterliegenden W'raums zum Parameter~$\vtet$,
  und wir schreiben $\expectparam{}$ \bzw $\varparam{}$
  für den EW und die Varianz einer ZV bezüglich dieses W'maßes.
  Das gibt eine handliche und ausreichend präzise Notation.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{satz}[Quantile der Normalverteilung]
  \label{stochainf/cha06:norm-quant}
  Für alle $\mu \in \RR$ und alle $\sig > 0$ und alle $p \in \intoo{0}{1}$ gilt:
  \begin{IEEEeqnarray*}{0l}
    Q_{\Norm(\mu,\sig^{2})}(p) = \sig \ccdot Q_{\Norm(0,1)}(p) + \mu
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}
  Übung.
\end{proof}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
