%%% Copyright 2023 Lasse Kliemann <l.kliemann@math.uni-kiel.de>
%%% CC BY-SA 4.0

\chapter{Anwendungen diskreter W'räume}

\section{Nummerierte Kugeln ziehen}

\begin{para}[Fallende Faktorielle]
  Für alle $n, k \in \NNzero$ mit $k \leq n$ definiere die \term{fallende Faktorielle}:\footnote{%
    Die Notation $(n)_k$ wird in der Literatur nicht einheitlich verwendet.
    Einige Autoren verwenden sie für $\prod_{i=0}^{k-1} (n+i)$.}
  \begin{IEEEeqnarray*}{0l}
    (n)_k \df \prod_{i=0}^{k-1} (n-i) = n (n-1) \ccdot \hdots \ccdot (n-(k-1))
  \end{IEEEeqnarray*}
  Zum Beispiel ist $(8)_3 = 8 \ccdot 7 \ccdot 6 = 336$.
  Auf vielen Taschenrechnern gibt es eine Taste für die fallende Faktorielle,
  beschriftet mit~\lstinline|nPr|.
  Wenn $k=0$ gilt, so gibt es keine Faktoren in dem Produkt, und in diesem Falle einigt man sich ja darauf,
  dass das Produkt den Wert $1$ bekommt.
  Es gilt also $(n)_0 = 1$ für alle $n \in \NNzero$, insbesondere auch~$(0)_0 = 1$.
\end{para}

\begin{para}[Fakultät]
  Ein anderer wichtiger Spezialfall ist $(n)_n$,
  den man die \term{Fakultät} von $n$ nennt und mit $n!$ bezeichnet:
  \begin{IEEEeqnarray*}{0l}
    n! \df (n)_n = \prod_{i=1}^n i = 1 \ccdot 2 \ccdot \hdots \ccdot (n-1) \ccdot n
  \end{IEEEeqnarray*}
  Zum Beispiel:
  \begin{IEEEeqnarray*}{0rCl}
    0! & = & 1 \\
    1! & = & 1 \\
    2! & = & 1 \ccdot 2 = 2\\
    3! & = & 1 \ccdot 2 \ccdot 3 = 6
  \end{IEEEeqnarray*}
\end{para}

\begin{para}[Tupel mit paarweise verschiedenen Komponenten]
  Es sei $M$ eine Menge.
  Für alle $k \in \NNone$ definiere:
  \begin{IEEEeqnarray*}{0l}
    M^k_{\neq}
    \df \set{(\eli{x}{k}) \in M^k \suchthat \forall i,j \in \setn{k} \holds i \neq j \implies x_i \neq x_j}
  \end{IEEEeqnarray*}
  Das ist die Menge aller \xtupel{k} mit Komponenten aus $M$, die so sind,
  dass in keinem der Tupel ein Element aus $M$ mehrfach aufgeführt wird.
  Man sagt auch: Die Komponenten in den Tupeln sind paarweise verschieden.
  Zum Beispiel ist
  \begin{IEEEeqnarray*}{0l}
    \setn{3}^2_{\neq} = \set{ (1,2), (1,3), (2,1), (2,3), (3,1), (3,2) } \comma
  \end{IEEEeqnarray*}
  aber $(1,1) \not\in \setn{3}^2_{\neq}$, denn in diesem Tupel kommt die $1$ mehrfach vor.
  Man sieht leicht (Übung),
  dass für alle endlichen, nicht\-/leeren Mengen $M$ gilt $\card{M^k_{\neq}} = (n)_{k}$.
\end{para}

\begin{para}[Experiment]
  Aus einer Urne mit $n \in \NNone$ Kugeln, nummeriert von $1$ bis~$n$,
  werden ohne Hinsehen und ohne Zurücklegen $k \in \setn{n}$ Kugeln gezogen,
  und wir notieren, welche Kugeln gezogen werden und in welcher Reihenfolge.
  Wir wissen nichts über die Anordnung der Kugeln in der Urne.
  Die Menge der möglichen Ziehungen können wir mit $T \df \setn{n}^{k}_{\neq}$ modellieren.
  Für die weitere Modellierung des Experimentes wählen wir
  $X = (\eli{X}{k}) \distr \Unif(T)$,
  wobei $X_{i}$ für die Nummer der im \xten{i} Zug gezogenen Kugel steht.
  Mit der uniformen Verteilung drücken wir aus, dass wir keinen Anlass sehen,
  eine der Ziehungen für wahrscheinlicher zu halten als eine andere;
  das geht darauf zurück, dass wir nichts über die Anordnung der Kugeln in der Urne wissen
  und dass wir ohne Hinsehen ziehen.
\end{para}

\begin{para}[Verteilung von $X_{i}$]
  Es sei $i \in \setn{k}$.
  Dann gilt für alle $l \in \setn{n}$:
  \begin{IEEEeqnarray*}{0l}
    \prob{X_{i} = l} = \sum_{\substack{u \in T \\ u_{i} = l}} \prob{X=u}
    = \frac{\card{\set{u \in T \suchthat u_{i} = l}}}{(n)_{k}} \\
    = \frac{(n-1)_{i-1} (n-i)_{k-i}}{(n)_{k}}
    = \frac{\prod_{j=0}^{i-2} (n-1-j) \ccdot \prod_{j=0}^{k-i-1} (n-i-j)}{(n)_{k}} \\
    = \frac{\prod_{j=0}^{i-2} (n-1-j) \ccdot \prod_{j=i-1}^{k-2} (n-i-(j-(i-1)))}{(n)_{k}} \\
    = \frac{\prod_{j=0}^{i-2} (n-1-j) \ccdot \prod_{j=i-1}^{k-2} (n-1-j)}{(n)_{k}}
    = \frac{(n-1)_{k-1}}{(n)_{k}} = \frac{1}{n}
  \end{IEEEeqnarray*}
  Es folgt $X_{i} \distr \Unif(\setn{n})$.
\end{para}

\begin{para}[Diskussion und konditionierte W'keit]
  Wie gut stimmt $X_{i} \distr \Unif(\setn{n})$ mit unserer Vorstellung über das Experiment überein?
  Für $i=1$, also für den ersten Zug, scheint es stimmig.
  Für $i \geq 2$ vielleicht nicht sofort, denn ab dem \xten{2} Zug fehlen Kugeln.
  Müsste das nicht berücksichtigt werden?
  Aber wie?
  Man denkt vielleicht an etwas wie
  \enquote{$X_{2} \distr \Unif(\setn{n} \setminus \set{X_{1}})$},
  aber das ergibt keinen Sinn -- man erinnere sich, dass $\ffn{X_{1}}{\Om}{\RR}$ gilt,
  also $X_{1}$ ist eine \emphasis{Funktion}.
  Auf den zweiten Blick ist auch $X_{i} \distr \Unif(\setn{n})$
  im Einklang mit unserer Vorstellung über das Experiment.
  Denn es gibt keine Kugel, der wir den Vorzug für den \xten{i} Zug geben möchten
  -- solange wir nichts über die zuvor erfolgten Züge wissen,
  und solches Wissen fehlt derzeit.
  \par
  Kommt solches Wissen hinzu, dann ist Konditionierung der richtige Weg.
  Wir schauen uns das für $X_{1}$ und $X_{2}$ an.
  Zunächst seien $t,l \in \setn{n}$ so, dass $t \neq l$ gilt.
  Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{X_{2} = l \cond X_{1} = t}
    = \frac{\prob{X_{1} = t \land X_{2} = l}}{\prob{X_{1}=t}} \\
    = n \frac{\card{\set{u \in T \suchthat (u_{1}, u_{2}) = (l,t)}}}{(n)_{k}} \\
    = n \frac{(n-2)_{k-2}}{(n)_{k}}
    = n \frac{1}{n (n-1)}
    = \frac{1}{n-1}
  \end{IEEEeqnarray*}
  Das ist genau das, was wir im Hinblick auf das Experiment erwarten
  und eine weitere Bestätigung, dass Konditionierung der richtige Weg ist,
  um neue Informationen zu berücksichtigen.
  Schließlich haben wir für alle $l \in \setn{n}$:
  \begin{IEEEeqnarray*}{0l}
    \prob{X_{2} = l \cond X_{1} = l}
    = \frac{\prob{X_{1} = l \land X_{2} = l}}{\prob{X_{1}=l}}
    = n \frac{\card{\set{u \in T \suchthat (u_{1}, u_{2}) = (l,l)}}}{(n)_{k}}
    = n \frac{0}{(n)_{k}} = 0
  \end{IEEEeqnarray*}
  Auch das ist genau das Erwartete: Dieselbe Kugel kann nicht wieder gezogen werden,
  also wird $\prob{X_{2} = l \cond X_{1} = l}$ der kleinstmögliche Wert zugeordnet,
  nämlich~$0$.
\end{para}

\section{Rote und blaue Kugeln ziehen}

\begin{para}[Binomialkoeffizient]
  Für alle $n, k \in \NNzero$ mit $k \leq n$ definieren wir den \term{Binomialkoeffizient} $\binom{n}{k}$,
  gelesen als \enquote{$n$~über~$k$}, als:
  \begin{IEEEeqnarray*}{0l}
    \binom{n}{k} \df \frac{(n)_k}{k!} = \prod_{i=0}^{k-1} \frac{n-i}{k-i}
  \end{IEEEeqnarray*}
  Zum Beispiel ist, wenn wir die zweite Darstellung benutzen:
  \begin{IEEEeqnarray*}{0l}
    \binom{7}{5}
    = \frac{7}{5} \ccdot \frac{6}{4} \ccdot \frac{5}{3} \ccdot \frac{4}{2} \ccdot \frac{3}{1}
    = \frac{2520}{120} = 21
  \end{IEEEeqnarray*}
  Auf dem Taschenrechner ist die Funktion zum Berechnen von Binomialkoeffizienten
  üblicherweise mit \lstinline|nCr| bezeichnet.
  Für alle $n,k \in \NNzero$ mit $n < k$ definieren wir ferner $\binom{n}{k} \df 0$.
\end{para}

\begin{satz}[Kombinatorische Bedeutung des Binomialkoeffizienten]
  Es sei $M$ eine endliche, nicht\-/leere Menge und $k \in \NNzero$.
  Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    \card{\set{A \subseteq M \suchthat \card{A} = k}} = \binom{\card{M}}{k}
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}[Skizze]
  Der Fall $k > \card{M}$ ist klar, also sei $k \leq \card{M}$.
  Wir zählen die \xelementigen{k} Teilmengen von $M$ zunächst,
  indem wir jede dieser Mengen in jeder ihrer Anordnungen wahrnehmen.
  Dabei erhalten wir $(\card{M})_{k}$ und überzählen um den Faktor $k!$,
  da wir ja letztlich die Teilmengen ohne Anordnung zählen möchten.
  Das kompensieren wir, indem wir durch $k!$ teilen, was den Binomialkoeffizienten ergibt.
\end{proof}

\begin{para}[Experiment]
  In einer Urne liegen $r \in \NNzero$ rote und $b \in \NNzero$ blaue Kugeln,
  und daraus werden $k \in \setn{r+b}$ Kugeln ohne Hinsehen und ohne Zurücklegen gezogen.
  Wir interessieren uns für die Anzahl der roten Kugeln in der Menge der gezogenen Kugeln.
  Es ist nicht offensichtlich, was für diese Anzahl eine passende Verteilung ist.
  Der folgende Absatz macht dafür einen Vorschlag.
\end{para}

\begin{para}[Verteilung für die Anzahl gezogener roter Kugeln]
  Wir suchen zunächst eine Betrachtung, wo wieder die uniforme Verteilung passt.
  Dazu stellen wir uns vor, die Kugeln sind nummeriert von $1$ bis $r+b$.
  Dann gibt es $N \df \binom{r+b}{k}$ mögliche Ziehungen,
  wenn man bei einer Ziehung nicht auf die Reihenfolge achtet,
  in welcher die Kugeln gezogen werden.
  Wir nummerieren diese $N$ Ziehungen willkürlich von $1$ bis $N$,
  etwa $\eli{A}{N} \subseteq \setn{r+b}$.
  Es ist ${X \distr \Unif(\setn{N})}$ ein gutes Modell für die Nummer der Ziehung in dem Experiment,
  denn wir haben wie im vorigen Abschnitt keinen Anlass,
  eine Ziehung für wahrscheinlicher zu halten als eine andere.
  Innerhalb dieses Modells erschließen wir nun die Verteilung für die Anzahl der roten Kugeln in der Ziehung,
  alles nun Folgende sind also mathematische Argumente.
  Definiere $\ffn{f}{\setn{N}}{\NNzero}$ als die Funktion,
  die jedes $j \in \setn{N}$ auf die Anzahl roter Kugeln in $A_{j}$ abbildet.
  Wir können $X$ so wählen, dass $\img(X) = \setn{N}$ gilt,
  und dann ist $Y \df f \circ X$ eine ZV.
  \par
  Es sei $t \in \setft{0}{k}$.
  Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{Y = t}
    = \prob{X \in \fnpre{f}(t)}
    = \frac{\card{\fnpre{f}(t)}}{N}
  \end{IEEEeqnarray*}
  Welche Zahl steht im Zähler dieses Bruches, d.h.
  wie viele Ziehungen gibt es, in denen genau $t$ rote Kugeln (und somit genau $k-t$ blaue Kugeln) vorkommen?
  Es gibt $\binom{r}{t}$ Möglichkeiten, die roten Kugeln auszuwählen,
  und dann $\binom{b}{k-t}$ Möglichkeiten, die blauen Kugeln auszuwählen,
  was $\binom{r}{t} \binom{b}{k-t}$ Möglichkeiten insgesamt ergibt.
  (Bemerke, dass einer dieser Faktoren~$0$ sein kann:
  Es ist $\binom{r}{t} = 0$ wenn $r < t$ gilt, d.h. wenn es nicht genügend rote Kugeln gibt;
  es ist $\binom{b}{k-t} = 0$ wenn $b < k-t$ gilt, d.h. wenn es nicht genügend blaue Kugeln gibt,
  um die übrigen $k-t$ Kugeln der Ziehung als blaue Kugeln zu wählen.)
  Es folgt:
  \begin{IEEEeqnarray}{0l}
    \label{stochainf/cha03:hyp}
    \prob{Y = t} = \frac{\binom{r}{t} \binom{b}{k-t}}{N}
    = \frac{\binom{r}{t} \binom{b}{k-t}}{\binom{r+b}{k}}
  \end{IEEEeqnarray}
  Ferner gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{Y \in \setn{k}} = \prob{X \in \fnpre{f}(\setn{k})} = \prob{X \in \setn{N}} = 1
  \end{IEEEeqnarray*}
  Es folgt $\supp(Y) \subseteq \setn{k}$,
  somit haben wir die PMF von $Y$ mit obiger Formel \eqref{stochainf/cha03:hyp},
  die ja für alle $t \in \setft{0}{k}$ gilt, vollständig erschlossen.
  (Nebenbei haben wir $\binom{r+b}{k} = \sum_{t=0}^{k} \binom{r}{t} \binom{b}{k-t}$ gezeigt,
  was man die \term{Vandermonde\-/Identität} nennt.)
\end{para}

\begin{para}[Hypergeometrische Verteilung]
  Die Verteilung, die $Y$ in vorigem Absatz hat,
  nennen wir \term{hypergeometrische Verteilung} mit $r$ roten und $b$ blauen Kugeln und $k$ Zügen,
  kurz $\Hyp(r,b,k)$.
\end{para}

\begin{example}[Anzahl Richtige beim Lotto]
  Die Anzahl der \enquote{Richtigen} beim Lotto (\enquote{$6$ aus $49$})
  wird durch die Verteilung $\Hyp(6,43,6)$ gut modelliert.
  Die Zahlen, die man auf dem Lottoschein angekreuzt hat, entsprechen dabei den roten Kugeln.
  Was ist die W'keit, beim Lotto mindestens $4$ Richtige zu haben?
  Dies ist $\prob{X \geq 4}$ für eine ZV $X$ mit ${X \distr \Hyp(6,43,6)}$.
  Wir berechnen:
  \begin{IEEEeqnarray*}{0l}
    \prob{X \geq 4}
    = \frac{\binom{6}{4} \binom{43}{6-4}}{\binom{49}{6}}
    + \frac{\binom{6}{5} \binom{43}{6-5}}{\binom{49}{6}}
    + \frac{\binom{6}{6} \binom{43}{6-6}}{\binom{49}{6}}
    \approx 0.0009871
  \end{IEEEeqnarray*}
\end{example}

\section{Erfolge zählen}

\begin{para}[Experiment]
  Wir haben ein Experiment, das aus $n$ Teilexperimenten besteht.
  Jedes Teilexperiment kann \enquote{Erfolg} oder \enquote{Misserfolg} liefern.
  Oft ist in so einem Fall $\p{\eli{X}{n}} \distr \Ber(p)$ \iid
  eine passende Modellierung, für ein $p \in \intcc{0}{1}$.
  Dabei steht $X_{i}$ für das Ergebnis des \xten{i} Teilexperimentes für alle $i \in \setn{n}$,
  und $X_{i}=1$ bedeutet Erfolg, während $X_{i}=0$ Misserfolg bedeutet.
  Den Parameter $p$ der Bernoulliverteilung $\Ber(p)$ hatten wir in \autoref{stochainf/cha02:ber}
  \enquote{Erfolgsw'keit} genannt;
  die Motivation für diese Namensgebung sollte nun klar sein.
  Ein Modell mit Unabhängigkeit ist nicht immer sinnvoll,
  doch momentan nehmen wir es so hin und interessieren uns für die Verteilung von
  $S \df \sum_{i=1}^{n} X_{i}$, was offenbar in der Sprache des Experimentes
  die Anzahl der Erfolge ist.
  Wir benötigen eine weitere Notation, und dann klären wir die Verteilung von $S$.
\end{para}

\begin{para}[Frequenz in einem Tupel]
  Es sei $a$ ein Tupel.
  Für alle $x$ bezeichnen wir mit $\cntele{x}{a}$ die Anzahl der Komponenten von $a$,
  die gleich $x$ sind, auch genannt die \term{Frequenz} von $x$ in $a$.
  Zum Beispiel für $a \df (0,1,0,-1,2,0.5,1,1)$ haben wir
  $\cntele{1}{a} = 3$ und $\cntele{2}{a} = 1$ und $\cntele{4}{a} = 0$.
\end{para}

\begin{satz}[Summe unabhängiger Bernoulli\-/ZV]
  Es sei $p \in \intcc{0}{1}$ und $X = \p{\eli{X}{n}} \distr \Ber(p)$ \iid,
  und definiere $S \df \sum_{i=1}^{n} X_{i}$.
  Dann gilt für alle ${k \in \setft{0}{n}}$:
  \begin{IEEEeqnarray*}{0l}
    \prob{S=k} = \binom{n}{k} p^{k} (1-p)^{n-k}
  \end{IEEEeqnarray*}
  Ferner ist damit die PMF von $S$ vollständig erschlossen.
\end{satz}

\begin{proof}
  Es sei $k \in \setft{0}{n}$.
  Für alle $u \in \OI^n$ gilt aufgrund der Unabhängigkeit:
  \begin{IEEEeqnarray*}{0l}
    \prob{ X = u } = \prod_{i=1}^n \prob{X_i = u_i}
    = \prod_{i=1}^n
    \begin{cases}
      p & u_i = 1 \\
      (1-p) & u_i = 0
    \end{cases}
  \end{IEEEeqnarray*}
  Diese W'keit hängt also nur von der Frequenz der Einsen in $u$ ab,
  für die wir ja auch $\cntele{1}{u}$ schreiben.
  Es gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{ X = u } = p^{\cntele{1}{u}} (1-p)^{n-\cntele{1}{u}}
  \end{IEEEeqnarray*}
  Es gibt genau $\binom{n}{k}$ Vektoren $u \in \OI^n$ mit $\cntele{1}{u} = k$.
  Für jeden ist die W'keit,
  dass er von $X$ angenommen wird, ${p^k (1-p)^{n-k}}$.
  Es folgt:
  \begin{IEEEeqnarray*}{0l}
    \prob{S = k} = \prob{ \cntele{1}{X} = k }
    = \sum_{\substack{u \in \OI^{n}\\\cntele{1}{u}=k}} \prob{ X = u }
    = \sum_{\substack{u \in \OI^{n}\\\cntele{1}{u}=k}} {p^k (1-p)^{n-k}}
    = \binom{n}{k} {p^k (1-p)^{n-k}}
  \end{IEEEeqnarray*}
  Da $\supp(X) \subseteq \OI^{n}$ gilt, haben wir ferner:
  \begin{IEEEeqnarray*}{0l}
    \sum_{k=0}^{n} \prob{S = k}
    = \sum_{k=0}^{n} \sum_{\substack{u \in \OI^{n}\\\cntele{1}{u}=k}} \prob{ X = u }
    = \sum_{u \in \OI^{n}} \prob{ X = u } = 1
  \end{IEEEeqnarray*}
  Damit ist klar, dass die PMF vollständig erschlossen wurde.
  (Alternativ sieht man $\sum_{k=0}^{n} \binom{n}{k} {p^k (1-p)^{n-k}}
  = (p+(1-p))^{n} = 1^{n} = 1$
  mit dem binomischen Lehrsatz.)
\end{proof}

\begin{para}[Binomialverteilung]
  Eine ZV mit Verteilung wie $S$ im vorigen Satz heißt \term{binomialverteilt},
  oder wir sagen, sie hat \term{Binomialverteilung},
  mit Parametern $n$ und~$p$, kurz $\Bin(n,p)$.
  Für alle ZV $\eli{X}{n}$ gilt also:
  \begin{IEEEeqnarray*}{0l}
    \text{$\p{\eli{X}{n}} \distr \Ber(p)$ \iid} \implies
    \sum_{i=1}^{n} X_{i} \distr \Bin(n,p)
  \end{IEEEeqnarray*}
\end{para}

\begin{example}[Anzahl geheilter Patienten]
  \label{stochainf/cha03:ex-patient}
  Ein Medikament, das mit W'keit $0.6$ zur Heilung führt, wird an $200$ Patienten ausgegeben.\footnote{%
    Das ist eine saloppe Ausdrucksweise.
    Bevor wir modelliert haben, gibt es keine W'keit.
    Gemeint ist hier:
    Es ist $Y \distr \Ber(0.6)$ ein gutes Modell,
    wenn $Y$ für einen Patienten angibt, ob dieser geheilt wird.
    Da wir nach unseren gründlichen Vorbereitungen aber wissen was wir tun,
    ist die saloppe Ausdrucksweise ohne Gefahr.}
  Was ist die W'keit, dass mindestens $150$ von ihnen dadurch geheilt werden?\footnote{%
    Auch hier ist \enquote{W'keit} eine saloppe Ausdrucksweise.
    Genauer wäre: \enquote{Was sollte unser Grad der Überzeugung sein, dass \dots}.
    Wieder gilt: Da wir wissen was wir tun,
    ist die saloppe Ausdrucksweise ohne Gefahr.}
  Es stehe $X$ für die Anzahl der geheilten Patienten; gesucht ist also $\prob{X \geq 150}$.
  Wir nehmen an, dass $X \distr \Bin(200, 0.6)$ ein geeignetes Modell ist.
  Es gilt $\prob{X \geq 150} = \sum_{k=150}^{200} \prob{X=k}$, was offenbar mühsam zu berechnen ist,
  jedenfalls mit einem einfachen Taschenrechner.
  Mit einem Computer könnten wir diese Summe leicht berechnen,
  aber auch da gibt es einen eleganteren Weg.
  Die folgende Funktion nennen wir die \term{kumulative Verteilungsfunktion}
  oder \term{cumulative distribution function (CDF)} einer ZV $X$
  (egal welche Verteilung sie hat):
  \begin{IEEEeqnarray*}{0l}
    \ffnx{F_{X}}{\RR}{\intcc{0}{1}}{u \mapsto \prob{X \leq u}}
  \end{IEEEeqnarray*}
  Man überlegt sich leicht (Übung), wie man PMF und CDF ineinander umrechnet.
  Für viele grundlegende Verteilungen ist die CDF in statistischen Computersystemen zugänglich,
  zum Beispiel in R~\cite{SoftwareR} über die Funktion \lstinline|pbinom|.
  Es gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{X \geq 150} = 1 - \prob{X < 150} = 1 - \prob{X \leq 149} = 1 - F_{X}(149)
  \end{IEEEeqnarray*}
  Für unsere Aufgabe bedeutet das, dass wir lediglich die CDF der $\Bin(200, 0.6)$ an $149$ auswerten müssen,
  und daraus berechnet sich sofort die gefragte W'keit $\prob{X \geq 150} \approx \np{5.899e-06}$.
  Wir werden auf die CDF ausführlich zu sprechen kommen,
  wenn wir in späteren Kapiteln den allgemeinen Fall behandeln,
  wo die Ergebnismenge überabzählbar sein darf.
\end{example}

\begin{example}[Mehrfaches Lottospiel]
  Sie spielen jeden Monat einmal Lotto (\enquote{$6$ aus $49$}).
  Von einem \term{Gewinn} sprechen wir, wenn Sie mindestens $5$ Richtige haben.
  Für alle $n \in \NNone$ stehe $X_n$ für die Anzahl der Gewinne nach $n$ Jahren.
  Was ist $\prob{X_{n} \geq 1}$, also was ist die W'keit,
  dass Sie mindestens einen Gewinn haben in $n$ Jahren?
  \par
  Es bezeichne $p$ die W'keit, in einem einzelnen Lottospiel einen Gewinn zu haben.
  Wir modellieren $X_{n} \distr \Bin(12n,p)$.
  Dann haben wir noch $p$ zu bestimmen.
  Ein einzelnes Lottospiel lässt sich ja mit der hypergeometrischen Verteilung modellieren:
  Wenn $Y$ für die Anzahl der Richtigen in einem Spiel steht,
  gilt nach unserer Modellierung aus dem vorigen Abschnitt $Y \distr \Hyp(6,43,6)$.
  Es folgt:
  \begin{IEEEeqnarray*}{0l}
    p = \prob{Y \geq 5}
    = \prob{Y = 5} + \prob{Y = 6}
    = \frac{\binom{6}{5} \binom{43}{1}
      + \binom{6}{6} \binom{43}{0}}{\binom{49}{6}} \\
    = \frac{6 \ccdot 43 + 1 \ccdot 1}{\binom{49}{6}}
    = \frac{37}{1997688} \approx \np{1.852e-5}
  \end{IEEEeqnarray*}
  Die gesuchte W'keit ist somit:
  \begin{IEEEeqnarray*}{0l}
    \prob{X_{n} \geq 1} = 1 - \prob{X_{n} = 0}
    = 1 - (1-p)^{12n}
  \end{IEEEeqnarray*}
  Beispielsweise gilt $\prob{X_{20} \geq 1} \approx 0.004435$,
  also die W'keit in $20$ Jahren mindestens einen Gewinn zu haben,
  ist weniger als $0.45\%$.
\end{example}

\section{Auf Krankheit testen}

\begin{para}[Bevölkerung, Prävalenz]
  Wir fixieren eine bestimmte Menge von Personen, die wir die \term{Bevölkerung} nennen.
  Dann fixieren wir aus dieser Bevölkerung eine Person und fragen danach,
  ob diese Person eine bestimmte Krankheit hat oder nicht.
  Es sei bekannt, dass ein Anteil von ${\pi \in \intoo{0}{1}}$ der Bevölkerung die Krankheit hat,
  man nennt $\pi$ die \term{Prävalenz} der Krankheit (in dieser Bevölkerung).
  Es gebe $X$ an, ob die Person krank ist ($X=1$ bedeutet \enquote{ja}, $X=0$ bedeutet \enquote{nein}).
  Solange wir nichts weiter über die Person wissen, als dass sie aus dieser Bevölkerung stammt,
  ist ${X \distr \Ber(\pi)}$ ein sinnvolles Modell.
\end{para}

\begin{para}[Sensitivität, Spezifität]
  Es sei eine Testmethode, im folgenden kurz \enquote{Test} genannt,
  für die uns interessierende Krankheit gegeben.
  Oft kann so ein Test die Krankheit nicht mit Sicherheit feststellen oder ausschließen.
  Vielmehr kann er
  manchmal eine gesunde Person als krank klassifizieren (\term{falsch\-/positives} Testergebnis)
  oder eine kranke Person als gesund (\term{falsch\-/negatives} Testergebnis).
  Für eine Quantifizierung führt man an einer Menge garantiert gesunder Personen den Test aus,
  ebenso an einer Menge garantiert kranker Personen.
  Diese Garantien werden zum Beispiel durch aufwendigere medizinische Untersuchungen sichergestellt.
  Es bezeichne $n$ die Anzahl Personen in der Menge garantiert kranker Personen,
  die wir zu Untersuchungszwecken zusammengestellt haben,
  und $\tin \leq n$ die Anzahl der Personen aus dieser Menge,
  für die der Test positiv ausfiel.
  Analog sei $m$ die Anzahl Personen in einer Menge garantiert gesunder Personen
  und $\tim \leq m$ die Anzahl der Personen aus dieser Menge,
  für die der Test negativ ausfiel.
  Wir wollen so modellieren, dass gilt:
  \begin{itemize}
  \item Es ist $\frac{\tin}{n}$ die W'keit,
    dass der Test positiv ist, \emphasis{konditioniert darauf}, dass die Person krank ist.
    Diese Größe wird \term{Sensitivität} oder \term{true positive rate} des Tests genannt.
    Wir wollen dafür in Formeln die Bezeichnung $\TPR$ verwenden.
  \item Es ist $\frac{\tim}{m}$ die W'keit,
    dass der Test negativ ist, \emphasis{konditioniert darauf}, dass die Person gesund ist.
    Diese Größe wird \term{Spezifität} des Tests genannt.
    Wir wollen dafür in Formeln die Bezeichnung $\SPC$ verwenden.
  \end{itemize}
  Wir haben hier zum ersten Mal den Fall,
  dass eine Modellierung über die Vorgabe konditionierter W'keiten erfolgt.
  Dafür haben wir \autoref{stochainf/cha02:exists-cond}.
\end{para}

\begin{para}[Anwendung des Satzes auf die Testsituation]
  Es gebe $T$ an, ob der Test für die von uns fixierte Person positiv ausfällt;
  nach wie vor gibt $X$ an, ob die Person die Krankheit hat.
  Wir modellieren:
  \begin{IEEEeqnarray*}{0l"s}
    \prob{X=1} = \pi            & Prävalenz \\
    \prob{T=1 \cond X=1} = \TPR & Sensitivität \\
    \prob{T=0 \cond X=0} = \SPC & Spezifität
  \end{IEEEeqnarray*}
  Da offenbar $X$ und $T$ Bernoulliverteilung haben sollen,
  sind diese Angaben ausreichend.
  Nach \autoref{stochainf/cha02:exists-cond} ist so eine Modellierung möglich
  und führt auf eine eindeutig bestimmte Verteilung von $(X,T)$.
  Was uns nun viel mehr interessiert als die oben angegebene W'keiten
  sind die beiden folgenden konditionierten W'keiten
  -- je nachdem, ob die von uns fixierte Person ein positives (${T=1}$)
  oder ein negatives (${T=0}$) Testergebnis erhalten hat:
  \begin{IEEEeqnarray*}{0l"s}
    \PPV \df \prob{X=1 \cond T=1} & positiver Vorhersagewert \\
    \NPV \df \prob{X=0 \cond T=0} & negativer Vorhersagewert
  \end{IEEEeqnarray*}
  Denn dieses ist die W'keit,\footnote{%
    Genauer müssten wir hier \enquote{Grad der Überzeugung} schreiben statt \enquote{W'keit}.
    Aber wir hatten uns ja schon geeinigt,
    das eine saloppe Ausdrucksweise in Ordnung ist,
    da wir wissen was wir tun.}
  bei positivem Test tatsächlich krank zu sein
  (\term{positiver Vorhersagewert} oder \term{positive predictive value})
  \bzw die W'keit, bei negativem Test tatsächlich gesund zu sein
  (\term{negativer Vorhersagewert} oder \term{negative predictive value}).
\end{para}

\begin{para}[Formeln für $\PPV$ und $\NPV$]
  Angenommen, wir kennen die Prävalenz~$\pi$,
  die Sensitivität $\TPR$ und die Spezifität $\SPC$, und
  es seien diese drei Werte jeweils in $\intoo{0}{1}$.
  Wir wollen den positiven und negativen Vorhersagewert berechnen.
  Dazu arbeiten wir mit den jeweiligen Odds, die wir ja mit $\PPV'$ und $\NPV'$ bezeichnen:
  \begin{IEEEeqnarray*}{0l+l}
    \PPV' = \odds{X=1 \cond T=1}
    &\NPV' = \odds{X=0 \cond T=0}
  \end{IEEEeqnarray*}
  Mit der Odds\-/Version der Formel von Bayes erhalten wir:
  \begin{IEEEeqnarray*}{0l}
    \PPV' = \odds{X=1} \ccdot \LR(T=1, X=1)
    = \odds{X=1} \frac{\prob{T=1 \cond X=1}}{\prob{T=1 \cond X=0}} \\
    = \odds{X=1} \frac{\prob{T=1 \cond X=1}}{1 - \prob{T=0 \cond X=0}}
    = \pi' \frac{\TPR}{1 - \SPC}
  \end{IEEEeqnarray*}
  Man nennt $\PPV'$ auch die \term{Nachtest\-/Odds}
  und $\pi' = \odds{X=1} = \frac{\pi}{1-\pi}$ die \term{Vortest\-/Odds}.
  Man kann sich also merken: Nachtest\-/Odds ist gleich Vortest\-/Odds mal Likelihood\-/Ratio.
  \par
  Für den negativen Vorhersagewert $\NPV$ gibt es eine ähnliche Formel:
  \begin{IEEEeqnarray*}{0l}
    \NPV' = \odds{X=0} \ccdot \LR(T=0,X=0)
    = \odds{X=0} \frac{\prob{T=0 \cond X=0}}{\prob{T=0 \cond X=1}} \\
    = \odds{X=0} \frac{\prob{T=0 \cond X=0}}{1 - \prob{T=1 \cond X=1}}
    = (1-\pi)' \frac{\SPC}{1-\TPR}
  \end{IEEEeqnarray*}
  Durch unsere Voraussetzung,
  dass $0 < \pi, \TPR,  \SPC < 1$ gilt, sind die Nenner in obigen Formeln stets ungleich~$0$
  (vergleiche mit den Voraussetzungen für \autoref{stochainf/cha01:bayes-odds}).
  Die so ausgeschlossenen Grenzfälle sind für die Praxis weniger relevant.
\end{para}

\begin{example}
  Für eine HIV\=/Infektion \bzw einen üblichen HIV\=/Test
  waren um das Jahr 1998 herum folgende Werte gültig~\cite{GigerenzerEtAl98}:
  \begin{itemize}
  \item Prävalenz $\pi = 0.0001$
  \item Sensitivität $\TPR = 0.998$
  \item Spezifität $\SPC = 0.9999$
  \end{itemize}
  Daraus errechnen wir:
  \begin{IEEEeqnarray*}{0l}
    \PPV' = \frac{\pi}{1-\pi} \frac{\TPR}{1 - \SPC}
    = \frac{0.0001}{1-0.0001} \frac{0.998}{1-0.9999}
    \approx 0.9981
  \end{IEEEeqnarray*}
  Dieser Wert ist nahe bei $1$ und lässt eine W'keit nahe $0.5$ vermuten.
  Rechnen wir das mit $\PPV = \frac{\PPV'}{1+\PPV'}$ in W'keit um,
  so erhalten wir $\approx 0.4995$.
  Eine Person, die zu jener Zeit ein positives Testergebnis erhielt,
  war also nur mit einer W'keit von $\approx 0.5$ tatsächlich infiziert.
  Wichtig ist hierbei allerdings, alle Informationen einfließen zu lassen.
  Wenn die Person zum Beispiel zu einer relevanten Risikogruppe gehört oder schon Symptome zeigt,
  dann sollte man eine entsprechend höhere Prävalenz ansetzen
  oder diese zusätzliche Information in anderer geeigneter Weise einfließen lassen.
  \par
  Für den $\NPV$ haben wir:
  \begin{IEEEeqnarray*}{0l}
    \NPV'
    = (1-\pi)' \frac{\SPC}{1 - \TPR}
    = (1-0.0001)' \frac{0.9999}{1-0.998}
    \approx \np{4.999e6}
  \end{IEEEeqnarray*}
  Der Wert ist sehr groß, was einen Wert nahe bei~$1$ für $\NPV$ vermuten lässt.
  In der Tat ergibt sich nach $\NPV = \frac{\NPV'}{1+\NPV'}$ der Wert von $\approx 1.000$.
  Bei einem negativen Testergebnis ist die W'keit also fast~$1$,
  tatsächlich nicht infiziert zu sein.
\end{example}

\section{Zufällige Münze werfen}

\begin{para}[Experiment, Modell]
  In einer Urne liegen zwei Münzen:
  Der Wurf von Münze~$1$ ist gut mit $\Ber(\frac{1}{2})$ modelliert,
  während der Wurf von Münze~$2$ gut mit $\Ber(p)$ modelliert ist,
  für ein $p \in \intcc{0}{1}$.
  Äußerlich sind die Münzen nicht zu unterscheiden, und wir wissen nichts über deren Anordnung in der Urne.
  Wir greifen in die Urne und ziehen ohne Hinsehen eine der beiden Münzen.
  Es sei $X$ die ZV, die sagt, welche der beiden Münzen wir haben.
  Dann ist ${X \distr \Unif(\set{1,2})}$ ein sinnvolles Modell.
  Nach dem Ziehen der Münze werfen wir die gezogene Münze zweimal,
  es seien $Y_{1}, Y_{2}$ die Ergebnisse der Würfe.
  Wir modellieren für alle $j \in \set{1,2}$:
  \begin{IEEEeqnarray*}{0l+l}
    \prob{Y_{j}=1 \cond X=1} = \frac{1}{2} & \prob{Y_{j}=0 \cond X=1} = \frac{1}{2} \\
    \prob{Y_{j}=1 \cond X=2} = p & \prob{Y_{j}=0 \cond X=2} = 1-p
  \end{IEEEeqnarray*}
  Dies schreibt man kurz auch so:
  \begin{IEEEeqnarray*}{0l}
    \forall i \in \set{1,2} \holds
    (Y_{j} \cond X=i) \distr
    \begin{cases}
      \Ber(\frac{1}{2}) & \text{wenn $i=1$} \\
      \Ber(p) & \text{wenn $i=2$}
    \end{cases}
  \end{IEEEeqnarray*}
  Ferner modellieren wir, dass $(Y_{1},Y_{2})$ unabhängig ist,
  wenn feststeht, welche der beiden Münzen wir haben, d.h.:
  \begin{IEEEeqnarray*}{0l}
    \forall i \in \set{1,2} \innerholds \forall y_{1},y_{2} \in \OI \holds \\
    \prob{Y_{1}=y_{1} \land Y_{2}=y_{2} \cond X=i}
    = \prob{Y_{1}=y_{1} \cond X=i} \ccdot \prob{Y_{2}=y_{2} \cond X=i}
  \end{IEEEeqnarray*}
  \autoref{stochainf/cha02:exists-cond} liefert wieder die Existenz und Eindeutigkeit eines solchen Modells.
\end{para}

\begin{para}[Analyse]
  Wie ist es mit der (nicht\-/konditionierten) Unabhängigkeit von $(Y_{1}, Y_{2})$?
  Immerhin beeinflussen sich die Münzwürfe nicht im physischen Sinne,
  so dass wir im allerersten Ansatz vielleicht erwarten, dass Unabhängigkeit gilt.
  Es gilt mit dem Satz von der totalen W'keit für alle $j \in \set{1,2}$:
  \begin{IEEEeqnarray*}{0l}
    \prob{Y_j=1}
    = \prob{Y_j=1 \cond X=1} \ccdot \prob{X=1} + \prob{Y_{j}=1 \cond X=2} \ccdot \prob{X=2} \\
    = \frac{1}{2} \frac{1}{2} + p \frac{1}{2}
    = \frac{1}{4} + \frac{p}{2}
  \end{IEEEeqnarray*}
  Es folgt:
  \begin{IEEEeqnarray*}{0l}
    \prob{Y_1=1} \ccdot \prob{Y_2=1} = \frac{1}{16} + \frac{p}{4} + \frac{p^2}{4}
  \end{IEEEeqnarray*}
  Ferner gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{Y_1=1 \land Y_2=1} \\
    = \prob{Y_1=1 \land Y_2=1 \cond X=1} \ccdot \prob{X=1}
    + \prob{Y_1=1 \land Y_2=1 \cond X=2} \ccdot \prob{X=2} \\
    = \prob{Y_1=1 \cond X=1} \ccdot \prob{Y_2=1 \cond X=1} \ccdot \prob{X=1} \\
    \phantom{=} + \prob{Y_1=1 \cond X=2} \ccdot \prob{Y_2=1 \cond X=2} \ccdot \prob{X=2} \\
    = \parens{\frac{1}{2}}^2 \frac{1}{2} + p^2 \frac{1}{2}
    = \frac{1}{8} + \frac{p^2}{2}
  \end{IEEEeqnarray*}
  Es folgt schließlich:
  \begin{IEEEeqnarray*}{0l}
    \prob{Y_1=1 \land Y_2=1} = \prob{Y_1=1} \ccdot \prob{Y_2=1}
    \iff \frac{1}{8} + \frac{p^2}{2} = \frac{1}{16} + \frac{p}{4} + \frac{p^2}{4} \\*
    \iff \frac{2}{16} + \frac{2 p^2}{4} = \frac{1}{16} + \frac{p}{4} + \frac{p^2}{4}
    \iff \frac{p^2}{4} - \frac{p}{4} + \frac{1}{16} = 0
    \iff \parens{p - \frac{1}{2}}^2 = 0 \\*
    \iff p - \frac{1}{2} = 0
    \iff p = \frac{1}{2}
  \end{IEEEeqnarray*}
  Wenn also $p \neq \frac{1}{2}$ gilt, so ist $(Y_1,Y_2)$ nicht unabhängig.
  Leicht sieht man ein, dass sogar gilt:
  Es ist $(Y_1, Y_2)$ genau dann unabhängig, wenn $p=\frac{1}{2}$ gilt.
\end{para}

\begin{para}[Diskussion]
  Wie passt das zu unserer Interpretation von Unabhängigkeit?
  Es ist wahr -- nach dem üblichen Verständnis über die Abläufe in der Welt, in der wir leben --
  dass sich die Münzwürfe nicht im physischen Sinne gegenseitig beeinflussen.
  Allerdings bezieht sich Unabhängigkeit darauf,
  was die Kenntnis über das Eintreten gewisser Events
  auf \emphasis{unseren Grad der Überzeugung} bezüglich gewisser Events bewirkt.
  Der erste Münzwurf beeinflusst zwar den zweiten nicht im physischen Sinne,
  jedoch kann es sein, dass der erste Münzwurf uns etwas \emphasis{über die Münze} sagt.
  Wir erwarten, dass wenn etwa $p>\frac{1}{2}$ gilt und wir $Y_{1}=0$ beobachten,
  sich unser Grad der Überzeugung bezüglich $X=2$ reduziert.
  Ein Blick auf die Odds\-/Version der Formel von Bayes legt nahe,
  dass man diese Reduktion an der Likelihood\-/Ratio sehen sollte.
  In der Tat gilt:
  \begin{IEEEeqnarray*}{0l}
    \LR(Y_{1}=0, X=2)
    = \frac{\prob{Y_{1}=0 \cond X=2}}{\prob{Y_{1}=0 \cond X=1}}
    = \frac{1-p}{\frac{1}{2}} = 2 (1-p)
    \begin{cases}
      < 1 & p > \frac{1}{2} \\
      = 1 & p = \frac{1}{2} \\
      > 1 & p < \frac{1}{2}
    \end{cases}
  \end{IEEEeqnarray*}
  Im Fall $p>\frac{1}{2}$ spricht $Y_{1}=0$ also gegen $X=2$;
  im Fall $p<\frac{1}{2}$ spricht $Y_{1}=0$ für~${X=2}$.
\end{para}

\section{Auf ersten Erfolg warten}

\begin{para}[Geometrische Verteilung]
  Es sei $p \in \intoo{0}{1}$.
  Wir führen wiederholt ein Experiment durch, das Erfolg oder Misserfolg ergeben kann
  und gut durch $\Ber(p)$ modelliert ist.
  Wir stoppen beim ersten Erfolg und interessieren uns für die Anzahl $X$ an Misserfolgen,
  die davor beobachtet werden.
  Wir werden im nächsten Absatz argumentieren, dass eine sinnvolle Verteilung für $X$ ist:
  \begin{IEEEeqnarray*}{0l}
    \forall k \in \NNzero \holds \prob{X=k} = (1-p)^{k} p
  \end{IEEEeqnarray*}
  Dies nennen wir die \term{geometrische Verteilung mit Erfolgsw'keit} $p$
  und schreiben ${X \distr \Geom(p)}$.\footnote{%
    In der Literatur gibt es unterschiedliche Konventionen für diese Verteilung;
    manchmal wird der erste Erfolg mitgezählt.
    Die Umrechnung ist einfach, man muss nur darauf achten, welche der beiden Versionen gemeint ist.}
  Es ist unsere erste Verteilung mit unendlichem (aber noch immer abzählbarem) Träger,
  nämlich dieser ist $\NNzero$.
  Wir überprüfen schnell, dass so tatsächlich eine Verteilung festgelegt wird;
  es gilt nämlich nach der bekannten geometrischen Reihe:
  \begin{IEEEeqnarray*}{0l}
    \sum_{k=0}^{\infty} (1-p)^{k} p
    = p \sum_{k=0}^{\infty} (1-p)^{k}
    = p \frac{1}{1 - (1-p)} = 1
  \end{IEEEeqnarray*}
\end{para}

\begin{para}[Bezug zum Experiment]
  \label{stochainf/cha03:unendlicher-muenzwurf}
  Wir motivieren informell den Zusammenhang
  zwischen der geometrischen Verteilung und dem beschriebenen Experiment.
  Wir stellen uns vor, das Experiment wird unendlich oft wiederholt.
  Als Modell liegt eine unabhängige Folge $\seqeli{Y} \distr \Ber(p)$ nahe.
  Für sowas haben wir (noch) keinen Existenzsatz und eigentlich auch noch keinen Begriff der Unabhängigkeit.
  Sagen wir, die Unabhängigkeit bedeutet,
  dass $\famii{Y_{i}}$ unabhängig ist für alle \emphasis{endlichen} $I \subseteq \NNone$.
  Einen entsprechenden Existenzsatz werden wir in einem späteren Kapitel nachreichen;
  uns geht es gerade nur um eine informelle Motivation.
  In einer Anwendung ist natürlich zu hinterfragen,
  ob diese Unabhängigkeit unsere Vorstellung über das Experiment zum Ausdruck bringt.
  \par
  Es sei nun $k \in \NNzero$.
  Was ist die W'keit, dass wir zunächst $k$ Misserfolge und dann einen Erfolg haben?
  Diese ist mit der Unabhängigkeit:
  \begin{IEEEeqnarray*}{0l}
    \prob{\p{\bigwedge_{i=1}^{k} \p{Y_{i}=0}} \land Y_{k+1}=1}
    = \p{\prod_{i=1}^{k} \prob{Y_{i}=0}} \ccdot \prob{Y_{k+1}=1}
    = (1-p)^{k} p
  \end{IEEEeqnarray*}
  So kommen wir auf die $\Geom(p)$.
\end{para}

\begin{example}[Rote und blaue Kugeln ziehen]
  Es seien $r,b \in \NNone$.
  In einer Urne liegen $r$ rote und $b$ blaue Kugeln.
  Wir ziehen wiederholt ohne Hinsehen eine Kugel und werfen sie danach wieder zurück;
  wir stoppen sobald wir eine rote Kugel ziehen.
  Was ist die W'keit, dass wir die erste rote Kugel in einem ungeraden Zug ziehen?
  Es sei $X \distr \Geom(p)$ mit $p \df \frac{r}{r+b}$.
  Die gesuchte W'keit ist:
  \begin{IEEEeqnarray*}{0l}
    \prob{\exists t \in \NNzero \holds X = 2t}
    = \sum_{t \in \NNzero} \prob{X = 2t}
    = \sum_{t \in \NNzero} (1-p)^{2t} p
    = p \sum_{t \in \NNzero} \p{(1-p)^{2}}^{t} \\
    = \frac{p}{1-(1-p)^{2}}
    = \frac{p}{1-1+2p-p^{2}}
    = \frac{1}{2-p}
  \end{IEEEeqnarray*}
\end{example}
