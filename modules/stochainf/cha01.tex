%%% Copyright 2023 Lasse Kliemann <l.kliemann@math.uni-kiel.de>
%%% CC BY-SA 4.0

\chapter{Mathematische Modellierung des Zufalls}
\label{stochainf/cha01}

\section{Wahrschscheinlichkeitsräume}

\begin{para}[\sigalg, Eventraum]
  Es sei $\Om$ eine nicht\-/leere Menge und $\SIG \subseteq \cP(\Om)$.
  Für jedes $A \subseteq \Om$ definiere das \term{Komplement} von $A$ (in $\Om$)
  als $A^{c} \df \Om \setminus A$.
  Wir nennen $\SIG$ eine \term{\sigalg} über~$\Om$,
  wenn gilt:
  \begin{enumerate}
  \item $\Om \in \SIG$
  \item $\forall A \in \SIG \holds A^{c} \in \SIG$;
    man sagt: $\SIG$ ist \term{abgeschlossen unter Komplementen}.
  \item $\forall \seqone{A_{i}}{i} \in \seqset{\SIG} \holds \bigcupone{i} A_{i} \in \SIG$;
    man sagt: $\SIG$ ist \term{abgeschlossen unter abzählbaren Vereinigungen}.
    Das \enquote{abzählbar} bezieht sich auf die Anzahl der Mengen,
    die vereinigt werden, denn $\set{A_{i} \suchthat i \geq 1}$ ist abzählbar.
    Es bezieht sich nicht auf diese Mengen oder ihre Vereinigung selbst,
    d.h. $A_{i}$ mit $i \in \setn{n}$ darf überabzählbar sein.
    (Bei uns bedeutet \term{abzählbar}: endlich oder abzählbar unendlich.
    Streng genommen sagt die dritte Eigenschaft also nur,
    dass $\SIG$ abgeschlossen unter abzählbar unendlichen Vereinigungen ist.
    Man überlegt sich aber sehr leicht, dass sich das auf endliche Vereinigungen überträgt;
    siehe unten folgenden Satz.)
  \end{enumerate}
  Das Paar $(\Om, \SIG)$ nennen wir einen \term{Ereignisraum} oder \term{Eventraum}.
  In diesem Kontext nennen wir \dots
  \begin{itemize}
  \item $\Om$ den \term{Ergebnisraum} und jedes Element von $\Om$ ein \term{Ergebnis};
  \item jedes Element von $\SIG$ ein \term{Ereignis} oder \term{Event}.
  \end{itemize}
  Zu jedem $A \in \SIG$ nennen wir $A^{c}$ das \term{Gegenereignis} oder \term{Gegenevent} zu~$A$.
  Einen Eventraum nennen wir \term{diskret}, wenn $\Om$ abzählbar ist.
  Wir verzichten in diesem und dem nächsten Abschnitt bewusst
  auf eine Diskussion von Anwendungen oder Analogien;
  sondern wir nehmen die Bezeichnungen als solche hin.
\end{para}

\begin{example}[Eventraum]
  Es sei $\Om$ eine Menge.
  Dann ist $\set{\emptyset, \Om}$ und auch $\cP(\Om)$ eine \sigalg über $\Om$,
  also $(\Om, \set{\emptyset, \Om})$ und $(\Om, \cP(\Om))$ sind Eventräume.
  Als weiteres Beispiel definiere $\Om \df \setn{4}$
  und $\SIG \df \set{\emptyset, \set{1,2}, \set{3,4}, \Om}$;
  dann ist $(\Om, \SIG)$ ein Eventraum.
  Die dazugehörigen Beweise sind leicht zu erbringen und zur Übung empfohlen.
\end{example}

\begin{satz}[Einfache Eigenschaften eines Eventraumes]
  Es sei $(\Om,\SIG)$ ein Eventraum. Dann gilt:
  \begin{enumerate}
  \item $\emptyset \in \SIG$
  \item $\forall A, B \in \SIG \holds A \cup B \in \SIG$
    \par
    (Per Induktion folgt daraus leicht eine Version für endlich viele Mengen, nämlich:
    $\forall \eli{A}{n} \in \SIG \holds \bigcup_{i=1}^{n} A_{i} \in \SIG$.
    Wir werden im Folgenden in solchen Situationen
    meistens nur die Version für zwei Objekte, hier also zwei Events, angeben.
    Die Fortsetzung auf endlich viele werden wir danach kommentarlos verwenden,
    wenn sie, wie hier, korrekt ist und auf der Hand liegt.)
  \item $\forall \seqone{A_{i}}{i} \in \seqset{\SIG} \holds \bigcapone{i} A_{i} \in \SIG$
  \item $\forall A, B \in \SIG \holds A \cap B \in \SIG$
  \item $\forall A, B \in \SIG \holds A \setminus B \in \SIG$
  \end{enumerate}
\end{satz}

\begin{proof}
  \begin{enumerate}
  \item Es gilt $\emptyset = \Om^{c} \in \SIG$; Letzteres da $\Om \in \SIG$ gilt
    und $\SIG$ unter Komplementen abgeschlossen ist.
  \item Sieht man durch die Folge $(A,B,\emptyset,\emptyset,\hdots)$.
  \item Wir zeigen:
    \begin{IEEEeqnarray*}{0l}
      \bigcapone{i} A_{i} = \p[bigg]{\bigcupone{i} A_{i}^{c}}^{c}
    \end{IEEEeqnarray*}
    Daraus folgt die Behauptung, denn $A_{i}^{c}$ ist Element von $\SIG$ für alle $i \geq 1$,
    und damit auch $\bigcupone{i} A_{i}^{c}$
    und damit auch das Komplement davon.
    \par
    Um nun obige Mengengleichheit einzusehen, bemerke dass für alle $x \in \Om$ gilt:
    \begin{IEEEeqnarray*}{0l}
      x \in \p[bigg]{\bigcupone{i} A_{i}^{c}}^{c}
      \iff
      x \not\in \bigcupone{i} A_{i}^{c}
      \iff
      \forall i \geq 1 \holds x \not\in A_{i}^{c} \\*
      \iff
      \forall i \geq 1 \holds x \in A_{i}
      \iff
      x \in \bigcapone{i} A_{i}
    \end{IEEEeqnarray*}
  \item Folgt aus dem vorigen Punkt durch die Folge $(A,B,\Om,\Om,\hdots)$.
  \item Es gilt $A \setminus B = A \cap B^{c} \in \SIG$.\qedhere
  \end{enumerate}
\end{proof}

\begin{para}[W'maß]
  Es sei $(\Om, \SIG)$ ein Eventraum.
  Wir nennen eine Funktion $\ffn{\prob{}}{\SIG}{\intcc{0}{1}}$ ein \term{Wahrscheinlichkeitsmaß (W'maß)}
  auf $(\Om, \SIG)$, wenn gilt:
  \begin{description}[font=\it]
  \item[Normierung:] $\prob{\Om} = 1$
  \item[Additivität:] $\displaystyle
    \forall \seqone{A_{i}}{i} \in \seqset{\SIG} \with \text{paarweise disjunkt} \holds
    \prob{\dotbigcupone{i} A_{i}} = \sumone{i} \prob{A_{i}}$
  \end{description}
  Das Tripel $\wraum$ nennen wir einen \term{Wahrscheinlichkeitsraum (W'raum)}.
  Für jedes $A \in \SIG$, also für jedes Event~$A$,
  nennen wir $\prob{A}$ die \term{Wahrscheinlichkeit} (W'keit) von~$A$.
  Wir nennen $\prob{A^{c}}$ die \term{Gegenw'keit} von $A$;
  also ist die Gegenw'keit von $A$ die W'keit vom Gegenevent zu $A$.
\end{para}

\begin{para}[Erklärung zur Summe]
  \label{stochainf/cha01:summe}
  Mit $\sumone{i} \prob{A_{i}}$ ist natürlich $\limx{n} \sum_{i=1}^{n} \prob{A_{i}}$ gemeint,
  also der Limes der Reihe über $\seqone{\prob{A_{i}}}{i}$,
  wofür man auch $\sum_{i=1}^{\infty} \prob{A_{i}}$ schreibt.
  Da nur nicht\-/negative Zahlen addiert werden,
  verändert sich dieser Limes nicht unter Umordnung:
  Wenn $\ffn{\al}{\NNone}{\NNone}$ bijektiv zwischen $\NNone$ und $\NNone$ ist,
  dann gilt $\sum_{i=1}^{\infty} \prob{A_{\al(i)}} = \sum_{i=1}^{\infty} \prob{A_{i}}$.
\end{para}

\begin{satz}[Grundlegende Eigenschaften der W'keit]
  \label{stochainf/cha01:wkeit}
  Es sei $\wraum$ ein W'raum. Dann gilt:
  \begin{enumerate}
  \item $\prob{\emptyset} = 0$
  \item $\forall A, B \in \SIG
    \with A \cap B = \emptyset \holds \prob{A \dotcup B} = \prob{A} + \prob{B}$
  \item $\forall A, B \in \SIG \with
    A \subseteq B \holds \prob{B \setminus A} = \prob{B} - \prob{A}$
  \item\label{stochainf/cha01:wkeit:mono}
    $\forall A, B \in \SIG \with A \subseteq B \holds \prob{A} \leq \prob{B}$
  \item $\forall A \in \SIG \holds \prob{A^{c}} = 1 - \prob{A}$
  \item\label{stochainf/cha01:wkeit:inklexkl}
    $\forall A, B \in \SIG \holds \prob{A \cup B}
    = \prob{A} + \prob{B} - \prob{A \cap B}$
    \par
    Gleichheit gilt offenbar, wenn $\prob{A \cap B} = 0$ gilt,
    was insbesondere dann der Fall ist, wenn $A \cap B = \emptyset$ gilt.
  \item\label{stochainf/cha01:wkeit:union-2}
    $\forall A, B \in \SIG \holds \prob{A \cup B}
    \leq \prob{A} + \prob{B}$
  \item $\forall \seqone{A_{i}}{i} \in \seqset{\SIG}
    \with \text{monoton steigend\footnotemark}
    \holds \limx{i} \prob{A_{i}} = \prob{\bigcupone{i} A_{i}}$
    \footnotetext{$A_{1} \subseteq A_{2} \subseteq \hdots$}
  \item $\forall \seqone{A_{i}}{i} \in \seqset{\SIG}
    \holds \limx{k} \prob{\bigcup_{i=1}^{k} A_{i}} = \prob{\bigcupone{i} A_{i}}$
  \item $\forall \seqone{A_{i}}{i} \in \seqset{\SIG} \holds
    \prob{\bigcupone{i} A_{i}} \leq \sumone{i} \prob{A_{i}}$
  \end{enumerate}
\end{satz}

\begin{proof}
  \begin{enumerate}
  \item Die Folge $(\emptyset, \emptyset, \hdots)$ ist paarweise disjunkt.
    Wenn $\prob{\emptyset} > 0$ gilt,
    so gilt $\pinfty = \sumone{i} \prob{\emptyset} = \prob{\emptyset}$;
    ein Widerspruch.
  \item Sieht man durch die Folge $(A,B,\emptyset,\emptyset,\hdots)$.
  \item Es seien $A, B \in \SIG$ so, dass $A \subseteq B$ gilt.
    Es gilt $B = (B \setminus A) \dotcup A$ und somit $\prob{B} = \prob{B \setminus A} + \prob{A}$.
  \item Es seien wieder $A, B \in \SIG$ so, dass $A \subseteq B$ gilt.
    Aus dem vorigen Punkt folgt $\prob{B} - \prob{A} = \prob{B \setminus A} \geq 0$.
  \item Es gilt $\prob{A^{c}} = \prob{\Om \setminus A} = \prob{\Om} - \prob{A} = 1 - \prob{A}$.
  \item Es gilt $A \cup B = A \dotcup (B \setminus (A \cap B))$
    und $A \cap B \subseteq B$.
    Es folgt
    $\prob{A \cup B} = \prob{A} + \prob{B \setminus (A \cap B)}
    = \prob{A} + \prob{B} - \prob{A \cap B}$.
  \item Folgt direkt aus dem vorigen Punkt.
  \item Zunächst ist die reelle Folge $\seqone{\prob{A_{i}}}{i}$
    auf der linken Seite monoton steigend und beschränkt
    (durch $1$), also konvergent und zwar gegen einen Wert von höchstens~$1$.
    Definiere $B_{1} \df A_{1}$, und weiter
    für alle $k \geq 2$ definiere $B_{k} \df A_{k} \setminus \bigcup_{i=1}^{k-1} A_{i}$.
    Dann gilt
    $A_{k} = \bigcup_{i=1}^{k} A_{i} = \dotbigcup_{i=1}^{k} B_{i}$ für alle $k \geq 1$
    und somit auch $\bigcupone{i} A_{i} = \dotbigcupone{i} B_{i}$.
    Mit der Additivität folgt:
    \begin{IEEEeqnarray*}{0l}
      \prob{\bigcupone{i} A_{i}}
      = \prob{\dotbigcupone{i} B_{i}}
      = \limx{k} \sum_{i=1}^{k} \prob{B_{i}}
      = \limx{k} \prob{\dotbigcup_{i=1}^{k} B_{i}}
      = \limx{k} \prob{A_{k}}
    \end{IEEEeqnarray*}
  \item Für alle $k \geq 1$ definiere $B_{k} \df \bigcup_{i=1}^{k} A_{i}$.
    Dann ist $\seqone{B_{k}}{k} \in \seqset{\SIG}$ monoton steigend,
    und es gilt $\bigcupone{i} B_{i} = \bigcupone{i} A_{i}$.
    Die Behauptung folgt aus dem vorigen Punkt.
  \item Wenn $s \df \sumone{i} \prob{A_{i}} = \pinfty$ gilt, dann ist nichts zu tun;
    es gelte also $s \in \RRnn$.
    Für alle $k \in \NNone$ gilt:
    \begin{IEEEeqnarray*}{0l}
      \prob{\bigcup_{i=1}^{k} A_{i}} \leq \sum_{i=1}^{k} \prob{A_{i}}
      \leq s
    \end{IEEEeqnarray*}
    Die reelle Folge $\seqone{\prob{\bigcup_{i=1}^{k} A_{i}}}{k}$ konvergiert,
    da sie monoton steigend ist (wegen~\autoref{stochainf/cha01:wkeit:mono})
    und nach oben durch~$s$ beschränkt ist.
    Nach dem vorigen Punkt und der Monotonie des Limes folgt:
    \begin{IEEEeqnarray*}{0l+x*}
      \prob{\bigcupone{i} A_{i}}
      = \limx{k} \prob{\bigcup_{i=1}^{k} A_{i}}
      \leq s & \qedhere
    \end{IEEEeqnarray*}
  \end{enumerate}
\end{proof}

\begin{para}[Fortsetzungseigenschaft]
  Es sei $\wraum$ ein W'raum.
  Dann genügt es, wenn wir von $\prob{}$ gewisse Teile kennen,
  um andere Teile von $\prob{}$ zu erschließen.
  Wenn wir zum Beispiel $\prob{A}$ und $\prob{B}$ für gewisse $A,B \in \SIG$ mit ${A \cap B = \emptyset}$
  kennen, dann kennen wir $\prob{A \cup B}$, auch $\prob{A \dotcup B}$ geschrieben;
  nämlich nach der Additivität ist das $\prob{A} + \prob{B}$.
  Hier deutet sich das Hauptgeschäft der Wahrscheinlichkeitstheorie an,
  das wir an dieser Stelle salopp so zusammenfassen:
  Wir legen die W'keit einiger Events fest und berechnen daraus die W'keit anderer Events,
  an denen wir interessiert sind.
\end{para}

\begin{para}[W'maß über W'keit von Elementarevents]
  \label{stochainf/cha01:fortsetz-elementar}
  Ein interessanter Spezialfall der Fortsetzungseigenschaft ist:
  $\wraum$ ist ein diskreter W'raum so, dass $\SIG = \cP(\Om)$ gilt,
  und es möge $\prob{\set{\om}}$ bekannt sein für alle $\om \in \Om$.
  Nach der Additivität gilt $\prob{A} = \sum_{\om \in A} \prob{\set{\om}}$
  für alle $A \subseteq \Om$,
  also ist $\prob{}$ bereits vollständig bekannt.
  Die Events der Form $\set{\om}$ nennen wir \term{Elementarevents},
  und man schreibt auch $\prob{\om}$ anstelle von $\prob{\set{\om}}$.
  \par
  Mit derselben Idee lassen sich W'maße definieren.
  Gegeben eine abzählbare, nicht\-/leere Menge $\Om$,
  so erhalten wir auf dem Eventraum $(\Om, \cP(\Om))$ ein W'maß folgendermaßen:
  Für alle $\om \in \Om$ wähle $\prob{\om} \in \intcc{0}{1}$ so,
  dass $\sum_{\om \in A} \prob{\om} = 1$ gilt.
  Dann definiere $\prob{A} \df \sum_{\om \in A} \prob{\set{\om}}$ für alle $A \subseteq \Om$.
  Man überzeuge sich zur Übung davon,
  dass auf diesem Wege tatsächlich ein W'raum $\wraum$ entsteht.
\end{para}

\begin{para}[Uniformes W'maß]
  \label{stochainf/cha01:unif}
  Es sei $\Om$ endlich und nicht\-/leer.
  Für alle $\om \in \Om$ definiere $\prob{\om} \df \frac{1}{\card{\Om}}$.
  Das auf diese Weise festgelegte W'maß $\ffn{\prob{}}{\cP(\Om)}{\intcc{0}{1}}$
  nennen wir \term{uniformes W'maß} auf $\Om$.
  Offenbar gilt $\prob{A} = \frac{\card{A}}{\card{\Om}}$ für alle $A \subseteq \Om$.
\end{para}

\section{Konditionierung und Formel von Bayes}

In diesem Abschnitt sei stets $\wraum$ ein W'raum,
und alle Events seien darüber.

\begin{para}[Konditionierung]
  Es seien $A, B \in \SIG$ so, dass $\prob{B} > 0$ gilt.
  Wir definieren die \term{W'keit von $A$ konditioniert auf $B$},
  oder \term{W'keit von $A$ gegeben $B$}:
  \begin{IEEEeqnarray*}{0l}
    \prob{A \cond B} \df \frac{\prob{A \cap B}}{\prob{B}}
  \end{IEEEeqnarray*}
  Offenbar gilt $\prob{A \cond \Om} = \prob{A}$.
\end{para}

\begin{para}[Konditionierte W'keiten sind W'keiten]
  Konditionierte W'keiten verhalten sich wie normale W'keiten.
  Letztere können als Spezialfall der ersteren verstanden werden,
  wenn man auf $\Om$ konditioniert.
  Es gilt eine Version von \autoref{stochainf/cha01:wkeit},
  bei der $\prob{\hdots}$ überall ersetzt wurde durch $\prob{\hdots \cond E}$,
  wobei $E \in \SIG$ irgendein Event mit $\prob{E} > 0$ ist.
  Zur Übung schreibe man einige der Aussagen in dieser erweiterten Form aus und beweise sie.
\end{para}

%%% FIXME
%%%
%%% Später sollte eine Version für diskrete ZV formuliert und bewiesen
%%% werden.
%%%

\begin{satz}[von der totalen W'keit]
  \label{stochainf/cha01:total}
  Es sei $A \in \SIG$ und ${\eliz{B}{N} \in \SIG}$ seien paarweise disjunkt mit $\prob{B_{0}} = 0$.
  \begin{enumerate}
  \item\label{stochainf/cha01:total:1}
    Es gelte ${\prob{B_i} > 0}$ für alle~$i \in \setn{N}$, sowie:
    \begin{IEEEeqnarray*}{0l}
      A \subseteq B_{0} \cup \dotbigcup_{i=1}^N B_i
    \end{IEEEeqnarray*}
    Dann gilt:
    \begin{IEEEeqnarray*}{0l}
      \prob{A} = \sum_{i=1}^N \prob{A \cond B_i} \ccdot \prob{B_i}
    \end{IEEEeqnarray*}
  \item\label{stochainf/cha01:total:2}
    Es sei $E \in \cE$ so,
    dass $\prob{E} > 0$ und ${\prob{B_i \cap E} > 0}$ für alle~$i \in \setn{N}$ gilt, sowie:
    \begin{IEEEeqnarray*}{0l}
      A \cap E \subseteq B_{0} \cup \dotbigcup_{i=1}^N B_i
    \end{IEEEeqnarray*}
    Dann gilt:
    \begin{IEEEeqnarray*}{0l}
      \prob{A \cond E} = \sum_{i=1}^N \prob{A \cond B_i \cap E} \ccdot \prob{B_i \cond E}
    \end{IEEEeqnarray*}
  \end{enumerate}
\end{satz}

\begin{proof}
  Es genügt, \autoref{stochainf/cha01:total:2} zu zeigen,
  da \autoref{stochainf/cha01:total:1} daraus über den Fall $E=\Om$ folgt.
  \par
  Notiere ${B_{0}' \df B_{0} \setminus \dotbigcup_{i=1}^N B_i}$.
  Es gilt:
  \begin{IEEEeqnarray*}{0l}
    \sum_{i=1}^N \prob{A \cond B_i \cap E} \ccdot \prob{B_i \cond E}
    = \sum_{i=1}^N \frac{\prob{A \cap B_i \cap E}}{\prob{B_i \cap E}}
    \frac{\prob{B_i \cap E}}{\prob{E}} \\
    = \frac{1}{\prob{E}} \sum_{i=1}^N \prob{A \cap B_i \cap E} \\
    = \frac{1}{\prob{E}} \parens{\prob{A \cap B'_0 \cap E}
      + \sum_{i=1}^N \prob{A \cap B_i \cap E}} \\
    = \frac{1}{\prob{E}} \prob{A \cap E}
    = \prob{A \cond E}
  \end{IEEEeqnarray*}
  Wir haben dabei benutzt,
  dass $A \cap E \subseteq B_{0}' \dotcup \dotbigcup_{i=1}^N B_i$ gilt.
\end{proof}

\begin{satz}[Bayes\protect\footnote{%
    Thomas Bayes, englischer Mathematiker, 1701--1761.}]
  \label{stochainf/cha01:bayes}
  Es seien $A,B \in \SIG$ so, dass $\prob{A}, \prob{B} > 0$ gilt.
  Dann~gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{A \cond B} = \frac{\prob{A}}{\prob{B}} \prob{B \cond A}
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}
  Folgt direkt aus der Definition.
\end{proof}

Die Formel von Bayes ist oft handlicher in einer anderen Formulierung.
Für diese benötigen wir zwei weitere Begriffe.

\begin{para}[Odds]
  Es seien $A, B \in \SIG$ so, dass $\prob{B} > 0$ gilt.
  Wir definieren die \term{Odds} von $A$ \term{konditioniert} auf $B$
  \bzw die \term{Odds} von $A$ als:
  \begin{IEEEeqnarray*}{0l}
    \odds{A \cond B} \df
    \begin{cases}
      \frac{\prob{A \cond B}}{\prob{A^{c} \cond B}}
      = \frac{\prob{A \cond B}}{1-\prob{A \cond B}}
      & \prob{A \cond B} < 1 \\
      \pinfty & \prob{A \cond B} = 1
    \end{cases} \\
    \odds{A} \df \odds{A \cond \Om} =
    \begin{cases}
      \frac{\prob{A}}{\prob{A^{c}}}
      = \frac{\prob{A}}{1-\prob{A}}
      & \prob{A} < 1 \\
      \pinfty & \prob{A} = 1
    \end{cases}
  \end{IEEEeqnarray*}
  Wir bilden also den Quotienten aus (konditionierter) W'keit und (konditionierter) Gegenw'keit.
  Im Spezialfall, dass diese Gegenw'keit~$0$ ist, definiert man die Odds zu~$\pinfty$.
  Odds sind eine andere Art, W'keiten anzugeben.
  Gelegentlich wird auch das Wort \enquote{Chancen} dafür verwendet,
  wir wollen aber bei \enquote{Odds} bleiben.
  \par
  Die folgenden einfachen Umrechnungsformeln genügen oft:
  \begin{itemize}
  \item Ist $p \in \intco{0}{1}$ eine W'keit (egal wie konditioniert),
    dann sind ${p' \df \frac{p}{1-p}}$ die zugehörigen Odds.
  \item Sind $o \in \RRnn$ Odds (egal wie konditioniert),
    dann ist $\frac{o}{o+1}$ die zugehörige W'keit.
  \end{itemize}
  So können wir also leicht zwischen W'keit und Odds umrechnen.
\end{para}

\begin{para}[Likelihood\-/Ratio]
  Es seien $A,B \in \SIG$ so, dass gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{A}, \, \prob{A^{c}}, \, \prob{B \cond A^{c}} > 0
  \end{IEEEeqnarray*}
  Wir definieren die \term{Likelihood\-/Ratio} von $(B,A)$ als:
  \begin{IEEEeqnarray*}{0l}
    \LR(B,A) \df \frac{\prob{B \cond A}}{\prob{B \cond A^{c}}}
    = \frac{\prob{B \cond A}}{1 - \prob{B^{c} \cond A^{c}}}
    = \frac{1}{\odds{A}} \frac{\prob{B \cap A}}{\prob{B \cap A^{c}}}
  \end{IEEEeqnarray*}
  Dieser Wert gibt also an, wie viel höher die W'keit
  von $B$ konditioniert auf $A$ ist als konditioniert auf $A^{c}$.
\end{para}

Wir sind nun in der Lage, die angekündigte andere Version der Formel von Bayes anzugeben:

\begin{satz}[Odds\-/Version der Formel von Bayes]
  \label{stochainf/cha01:bayes-odds}
  Es seien $A,B \in \SIG$ so, dass gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{A}, \, \prob{A^{c}}, \, \prob{B \cond A^{c}} > 0
  \end{IEEEeqnarray*}
  Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    \odds{A \cond B} = \odds{A} \ccdot \LR(B,A)
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}
  Folgt leicht aus der Definition und der bekannten Formel von Bayes
  von \autoref{stochainf/cha01:bayes}.
  Die Voraussetzungen stellen sicher, dass:
  \begin{itemize}
  \item die W'keiten aller Events, auf die konditioniert wird, positiv sind;
  \item bei der Likelihood\-/Ratio nicht durch~$0$ geteilt wird;
  \item alle Odds endlich sind.\qedhere
  \end{itemize}
\end{proof}

\begin{para}[Unabhängigkeit]
  Es sei $\famii{A_{i}}$ eine endliche Familie von Events.\footnote{%
    Es sei $M$ eine Menge.
    Eine \term{Familie} in $M$ oder eine Familie von Elementen in $M$ ist eine Funktion $\ffn{f}{I}{M}$.
    In diesem Kontext nennt man die Domain auch die \term{Indexmenge};
    wir setzen diese stets als nicht\-/leer voraus.
    Als Notation wird häufig $\seq{f_{i}}{i \in I}$ verwendet, oft mit einem Zusatz,
    aus dem die Menge $M$ hervorgeht.
    Möchten wir uns später mit einem einzigen Symbol auf die Familie beziehen,
    so führen wir diese in der Form $f = \seq{f_{i}}{i \in I}$ ein.
    Die Notation und Redeweisen sind also ähnlich wie bei Folgen oder Tupeln.
    Eine Familie nennen wir \term{endlich} wenn sie endliche Indexmenge hat;
    offenbar sind Tupel Beispiele für endliche Familien.
    Eine Familie nennen wir \term{abzählbar} wenn sie abzählbare Indexmenge hat.}
  Wir nennen diese Familie \term{unabhängig}, wenn gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall I' \subseteq I \holds \prob{\bigcap_{i \in I'} A_{i}} = \prod_{i \in I'} \prob{A_{i}}
  \end{IEEEeqnarray*}
  Dabei verstehen wir den Fall $I'=\emptyset$ so,
  dass $\bigcap_{i \in \emptyset} A_{i} = \Om$ gilt.
  Wegen der Normierung und $\prod_{i \in \emptyset} \prob{A_{i}} = 1$ ist in diesem Fall
  die Gleichung also stets erfüllt.
  Dieser Fall darf also ignoriert werden, wenn man Unabhängigkeit überprüft.
  Ebenso darf man auch den Fall $\card{I'}=1$ ignorieren.
\end{para}

\begin{para}[Einfache Beobachtungen zur Unabhängigkeit]\hfill
  \begin{itemize}
  \item Offenbar bleibt Unabhängigkeit erhalten unter Umindizierung:
    Es ist $\famii{A_{i}}$ genau dann unabhängig,
    wenn $\famij{A_{\pi(j)}}$ unabhängig ist
    für alle Indexmengen $J$ und alle $\ffn{\pi}{J}{I}$ bijektiv zwischen $J$ und $I$.
  \item Offenbar überträgt sich Unabhängigkeit auf Teilfamilien:
    Wenn $\famii{A_{i}}$ unabhängig ist,
    dann auch $\famij{A_{j}}$ für alle $J \subseteq I$.
  \end{itemize}
\end{para}

\begin{satz}[Verbindung zwischen Konditionierung und Unabhängigkeit]
  \label{stochainf/cha01:kond-unab}
  Es sei $\famii{A_{i}}$ eine endliche Familie von Events so,
  dass $\prob{\bigcap_{i \in I'} A_{i}} > 0$ für alle $I' \subseteq I$ gilt.
  Dann sind äquivalent:
  \begin{enumerate}
  \item\label{stochainf/cha01:kond-unab:1} $\seq{A_{i}}{i \in I}$ ist unabhängig.
  \item\label{stochainf/cha01:kond-unab:2} $\forall J_{1}, J_{2} \subseteq I \with J_{1} \cap J_{2} = \emptyset
    \holds \prob{\bigcap_{j \in J_{1}} A_{j} \cond \bigcap_{j \in J_{2}} A_{j}} = \prob{\bigcap_{j \in J_{1}} A_{j}}$
  \item\label{stochainf/cha01:kond-unab:3} $\forall k \in I \innerholds
    \forall J \subseteq I \setminus\set{k}
    \holds \prob{A_{k} \cond \bigcap_{j \in J} A_{j}} = \prob{A_{k}}$
  \end{enumerate}
\end{satz}

\begin{proof}
  \impref{stochainf/cha01:kond-unab:1}{stochainf/cha01:kond-unab:2}
  Folgt direkt aus der Definition.
  \par
  \impref{stochainf/cha01:kond-unab:2}{stochainf/cha01:kond-unab:3}
  trivial.
  \par
  \impref{stochainf/cha01:kond-unab:3}{stochainf/cha01:kond-unab:1}
  Es sei $\emptyset \neq I' \subseteq I$.
  Wähle irgendein $k \in I'$ und definiere $J \df I' \setminus \set{k}$.
  Es gilt:
  \begin{IEEEeqnarray*}{0l"s+x*}
    \prob{\bigcap_{i \in I'} A_{i}}
    = \prob{A_{k} \cap \bigcap_{j \in J} A_{j}} \\
    = \prob{A_{k}} \prob{\bigcap_{j \in J} A_{j}} & Voraussetzung \\
    = \prob{A_{k}} \prod_{j \in J} \prob{A_{j}} & Induktion, da $\card{J} < \card{I'}$ \\
    = \prod_{i \in I'} \prob{A_{i}} && \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\section{Grundlagen der Anwendung}

In den vorigen beiden Abschnitten haben wir bewusst auf Anwendungen und Analogien verzichtet.
Es ist wichtig, dass wir unsere eigentlichen Schlüsse auch weiterhin mathematisch führen.
Dennoch wollen wir uns in diesem Abschnitt überlegen,
wie man Wahrscheinlichkeitstheorie grundsätzlich (außerhalb der Mathematik) anwenden kann.
Zum einen ist dies tatsächlich im Hinblick auf Anwendungen interessant.
Zum anderen kann es helfen, die mathematischen Resultate zu kategorisieren,
Verbindungen zwischen ihnen zu erkennen, und sie sich einzuprägen.
Im Hinblick darauf werden wir schematische Experimente
--~zum Beispiel das Ziehen einer Kugel aus einer Urne~--
in den folgenden Kapiteln öfters begleitend zu den mathematischen Resultaten anführen.

\begin{para}[Problemstellung]
  \label{stochainf/cha01:problemstellung}
  Wir betrachten folgendes Problem:
  Wir interessieren uns für den Zustand eines bestimmten Teils der
  Welt zu einem bestimmten Zeitpunkt;
  dieser Zustand ist uns aber nicht bekannt.
  Beispiele:
  \begin{enumerate}
  \item\label{stochainf/cha01:problemstellung:1} Aus einer Urne mit $n$ Kugeln,
    nummeriert von $1$ bis~$n$, wird ohne Hinsehen eine Kugel gezogen.
    Welche Nummer wird diese Kugel haben?
  \item Wie das vorige Experiment, doch die Kugel wurde bereits gezogen,
    uns aber nicht gezeigt.
    Welche Nummer hat die gezogene Kugel?
  \item Wir werfen eine Münze.
    Welche Seite wird nach der Landung nach oben zeigen?
  \item Wir führen einen Algorithmus aus, der die Zufallsbits des Computers nutzt.
    Welches Ergebnis wird er liefern?
  \item Wir behandeln $n$ Patienten mit einem neuen Medikament.
    Wie vielen von ihnen wird es in einer Woche besser gehen?
    Oder mit mehr Details:
    Welchen der $n$ Patienten wird es in einer Woche besser gehen
    und welchen nicht?
  \item Hat eine bestimmte Person zum aktuellen Zeitpunkt eine bestimmte Krankheit?
  \item Wird es morgen regnen?
  \end{enumerate}
\end{para}

\begin{para}[Experiment]
  In den ersten fünf Beispielen haben wir eine Beschreibung
  eines \term{Experimentes} oder \term{Zufallsprozesses},
  und der Zustand der Welt, nach dem wir fragen,
  ist geprägt durch das Ergebnis des Experimentes.
  Auch die Fragen nach dem aktuellen Gesundheitszustand einer Person
  und nach dem Wetter des nächsten Tages lassen sich so auffassen:
  Das Experiment ist das Leben der Person bis zum aktuellen Zeitpunkt,
  oder die Person wurde zufällig aus der Bevölkerung gewählt;
  \bzw das Experiment umfasst die Vorgänge in der Troposphäre der Erde.
  Ab jetzt werden wir stets von Experimenten und ihren Ergebnissen sprechen.
\end{para}

\begin{para}[Motivation der Modellierung]
  Wir wollen solche Situationen mathematisch so modellieren,
  dass mithilfe des Modells
  die uns interessierenden Fragen möglichst genau beantwortet werden können.
  Dabei erhalten wir zunächst
  --~mittels exakter Schlüsse, wie in der Mathematik üblich~--
  Aussagen im Rahmen des Modells.
  Diese können nach geeigneten Vorüberlegungen oft in naheliegender Weise
  in Bezug auf die Welt außerhalb der Mathematik interpretiert werden.
\end{para}

\begin{para}[Ablauf einer Modellierung und Anwendung]
  Unser Modell wird immer ein W'raum sein.
  Grundsätzlich läuft eine Modellierung mit Anwendung so ab:
  \begin{itemize}
  \item Wähle die Ergebnismenge $\Om$ so,
    dass jedes mögliche Ergebnis des Experimentes durch ein Element von $\Om$ repräsentiert wird.
  \item Wähle eine \sigalg $\SIG$ über $\Om$.
    Für jedes Event $A \in \SIG$ sagen wir, dass dieses \term{eintritt},
    wenn das Experiment ein Ergebnis liefert,
    das durch ein Element von $A$ repräsentiert wird.
  \item Benenne Events $\eli{A}{n} \in \SIG$, die für uns von Interesse sind,
    d.h. wir interessieren uns dafür, ob diese Events eintreten.
    Natürlich müssen $\Om$ und $\SIG$ fein genug gewählt werden,
    dass unsere Interessen in dieser Weise formuliert werden können.
  \item Definiere, zumindeste partiell, ein W'maß $\ffn{\prob{}}{\SIG}{\intcc{0}{1}}$ folgendermaßen:
    Lege die W'keit $\prob{A}$ für genügend viele $A \in \SIG$ fest so,
    dass dies zum einen unser Verständnis über das Experiment ausdrückt
    und zum anderen dadurch über die Fortsetzungseigenschaft
    auch die W'keiten $\elixx{\prob{A_{1}}}{\prob{A_{n}}}$ festgelegt sind.
  \item Berechne die W'keiten $\elixx{\prob{A_{1}}}{\prob{A_{n}}}$.
  \end{itemize}
\end{para}

\begin{para}[Diskussion der Wahrscheinlichkeit]
  Wir haben nun also:
  \begin{itemize}
  \item Die W'keit, die vom W'maß kommt.
  \item Eine Wahrscheinlichkeit mit Bezug auf die Anwendung,
    nämlich als einen \term{Grad der Überzeugung}:
    Wie stark wollen wir davon ausgehen,
    dass der uns interessierende Teil der Welt tatsächlich einen bestimmten Zustand hat?
  \end{itemize}
  Um eine Modellierung wie im vorigen Absatz zu rechtfertigen,
  sollte eine enge Beziehung zwischen W'keit und Grad der Überzeugung bestehen.
  Zumindest sollte man sich davon überzeugen,
  dass die beiden definierenden Eigenschaften eines W'maßes,
  nämlich die Normierung und die Additivität,
  im Einklang damit sind, wie wir über den Grad der Überzeugung denken.
  Bei der Normierung ist das einfach:
  Dass überhaupt etwas passiert, hat für uns höchstmöglichen Grad der Überzeugung
  und wird damit über die höchstmögliche W'keit ausgedrückt, nämlich über~$1$.
  Die Additivität leuchtet üblicherweise auch schnell ein;
  das ist aber schwieriger in Worte zu fassen.
  Wir vertiefen das hier nicht weiter.
\end{para}

\begin{example}[Nummerierte Kugel ziehen]
  \label{stochainf/cha01:kugeln}
  Wir betrachten das Beispiel aus \autoref{stochainf/cha01:problemstellung}
  \autoref{stochainf/cha01:problemstellung:1}.
  Naheliegenderweise wähle als Ergebnisraum $\Om \df \setn{n}$.
  Als Eventraum wähle $(\Om, \cP(\Om))$, denn es gibt keinen Grund,
  eine kleinere \sigalg als die Potenzmenge zu verwenden.
  Als W'maß wählen wir das uniforme (\autoref{stochainf/cha01:unif}).
  Das erscheint sinnvoll angesichts der Beschreibung des Experimentes,
  dass ohne Hinsehen gezogen wird.
  Wir gehen dabei stillschweigend davon aus,
  dass wir nichts über die Lage der Kugeln in der Urne wissen
  und auch, dass es keine anderen Hinweise gibt,
  die uns dazu bewegen sollten, in unserem Modell einige Kugeln anders zu behandeln als andere.
  Die Modellierung ist damit abgeschlossen.
  Für alle $\om \in \Om$ gilt $\prob{\om} = \frac{1}{n}$,
  also die W'keit, dass die gezogene Kugel Nummer $\om$ hat, ist $\frac{1}{n}$.
  Darin ist kein Tiefsinn, denn mit dieser Aussage wiederholen wir nur unsere Modellierung.
  Interessanter ist die Frage,
  ob die gezogene Kugel eine Nummer in einer Menge $A \subseteq \Om$ haben wird;
  die W'keit davon ist $\prob{A} = \frac{\card{A}}{n}$.
  Zum Beispiel ist die W'keit,
  dass die gezogene Kugel eine Nummer $\leq \ceil{\frac{n}{3}}$ hat,
  gleich $\frac{\ceil{\frac{n}{3}}}{n}$.
\end{example}

\begin{para}[Berücksichtigung neuer Informationen]
  Es sei $\wraum$ ein W'raum,
  der durch eine Modellierung zustande gekommen ist.
  Es sei $A \in \SIG$ ein Event, für das wir uns interessieren.
  Nun kommt es oft vor, dass wir nach erfolgter Modellierung neue Informationen erhalten,
  die vielleicht nicht einwandfrei klären, ob $A$ eintritt oder nicht,
  jedoch schon unseren Grad der Überzeugung bezüglich $A$ verändern.
  Beispiel: $A$ ist das Event, dass es am Samstag regnen wird.
  Am Freitag liegt $\prob{A}$ fest aufgrund der dann verfügbaren Informationen.
  Am Samstagmorgen regnet es zwar noch nicht, aber wir sehen schwarze Wolken am Himmel.
  Unser Grad der Überzeugung für Regen am Samstag ist nun größer als~$\prob{A}$.
  Grad der Überzeugung und W'keit weichen nun voneinander ab.
\end{para}

\begin{para}[Darstellung über konditionierte W'keit]
  Wie können wir dies systematisch behandeln?
  Zunächst muss unser Modell die Möglichkeit bieten, die Beobachtungen als Event auszudrücken.
  In obigem Beispiel bräuchten wir also nicht nur ein Event, das ausdrückt,
  ob es am Samstag regnet,
  sondern auch ein Event dafür, ob wir am Samstagmorgen schwarze Wolken sehen.
  \par
  Es sei $A$ das Event, das uns interessiert, und es sei $B$ das Event, das wir beobachten,
  von dem wir also inzwischen wissen, dass es eintritt.
  Nach der Beobachtung von $B$ ist unser Grad der Überzeugung bezüglich $A$
  möglicherweise nicht mehr~$\prob{A}$.
  Wir wollen uns kurz überlegen,
  dass unser Grad der Überzeugung bezüglich $A$ nun $\prob{A \cond B}$ sein sollte
  (vorausgesetzt natürlich, dass $\prob{B} > 0$ gilt, sonst geht das nicht).
  \par
  Als ersten Ansatz betrachten wir
  $\ffnx{\widetilde{\prob{}}}{\SIG}{\intcc{0}{1}}{E \mapsto \prob{E \cap B}}$
  als Vorschlag für ein aktualisiertes W'maß.
  Damit berücksichtigen wir, dass unter dem Wissen, dass $B$ eintritt,
  von $E$ nur noch der Anteil eintreten kann, der mit $B$ übereinstimmt.
  Dieser Ansatz ist aber noch nicht sachgerecht,
  denn es gilt $\widetilde{\prob{}}\p{E} \leq \prob{E}$ für alle $E \in \SIG$,
  d.h. der Grad der Überzeugung eines Events kann niemals größer werden durch neue Informationen.
  Das ist offenbar nicht was wir wollen.
  Außerdem kann bei $\widetilde{\prob{}}$ die Normierung verletzt sein.
  Um diese wieder herzustellen, teile durch $\prob{B}$,
  und damit sind wir bei der konditionierten W'keit.
  \par
  Wir geben durch Betrachtung einiger Extremfälle weitere Argumente,
  warum die konditionierte W'keit der richtige Weg ist, um neue Informationen zu integrieren:
  \begin{itemize}
  \item Es gilt $\prob{A \cond \Om} = \prob{A}$.
    Das ist sinnvoll,
    denn die Information, dass $\Om$ eintritt, hat keinen Gehalt
    -- dass $\Om$ eintritt, wussten wir von Anfang an.
  \item Es gilt $\prob{B \cond B} = 1$.
    Das ist sinnvoll,
    denn wenn wir wissen, dass $B$ eintritt,
    sollte $B$ den höchsten Grad der Überzeugung für uns haben.
    Daher passt es, dass die auf $B$ konditionierte W'keit von $B$
    den höchstmöglichen Wert bekommt, nämlich~$1$.
  \item Es gelte $A \cap B = \emptyset$.
    Dann gilt $\prob{A \cond B} = 0$.
    Das ist sinnvoll,
    denn wenn wir wissen, dass $B$ eintritt, dann tritt $A$ gewiss nicht ein.
    Es sollte daher $A$ den niedrigsten Grad der Überzeugung für uns haben.
    Daher passt es, dass die auf $B$ konditionierte W'keit von $A$
    den niedrigsten möglichen Wert bekommt, nämlich~$0$.
  \item Es sei $B \subseteq A$.
    Dann gilt $\prob{A \cond B} = 1$.
    Das ist sinnvoll,
    denn wenn wir wissen, dass $B$ eintritt, dann tritt $A$ gewiss ein.
    Es sollte daher $A$ den höchsten Grad der Überzeugung für uns haben.
    Daher passt es, dass die auf $B$ konditionierte W'keit von $A$
    den höchstmöglichen Wert bekommt, nämlich~$1$.
  \end{itemize}
\end{para}

\begin{para}[Zusammenhang mit der Unabhängigkeit]
  Wir erinnern an \autoref{stochainf/cha01:kond-unab}.
  Angesichts dessen, was Konditionierung nach obiger Diskussion bedeutet,
  hilft uns dieser Satz zu verstehen, warum man die Unabhängigkeit so nennt.
  Dass eine Familie von Events unabhängig ist, bedeutet nämlich gerade,
  dass sich unser Grad der Überzeugung bezüglich keines dieser Events dieser Familie verändert,
  wenn wir erfahren, dass irgendwelche der anderen Events dieser Familie eintreten.
  Bei der Unabhängigkeit und der Konditionierung geht es also um den Grad der Überzeugung.
  Das ist besonders wichtig zu verstehen in Bezug auf die Unabhängigkeit,
  denn oft wird --~fälschlicherweise~-- Unabhängigkeit nur als eine physische Unabhängigkeit interpretiert.
\end{para}

\begin{para}[Bedeutung der Formel von Bayes]
  Die Formel von Bayes erlaubt es uns,
  zwischen $\prob{A \cond B}$ und $\prob{B \cond A}$ umzurechnen.
  Das kommt in vielen Anwendungen vor.
  Zum Beispiel hat man bei fehlerbehafteten Tests auf ein bestimmtes Merkmal oft eine Angabe,
  mit welcher W'keit der Test ein positives Ergebnis liefert, wenn das Merkmal vorliegt.
  Viel interessanter ist aber die W'keit, dass das Merkmal vorliegt,
  nachdem ein positiver Test beobachtet wurde.
  Wir werden uns das im folgenden Kapitel genauer ansehen.
\end{para}

\begin{para}[Bedeutung der $\LR$]
  \label{stochainf/cha01:lr}
  Wir erinnern an \autoref{stochainf/cha01:bayes-odds}.
  Wenn $\LR(B,A) > 1$ gilt, dann gilt $\odds{A \cond B} > \odds{A}$ nach diesem Satz;
  man sagt naheliegenderweise auch, die Beobachtung $B$ \term{spricht für}~$A$.
  Wenn ${\LR(B,A) < 1}$ gilt, dann sagt man, $B$ \term{spricht gegen}~$A$.
  \par
  Wenn $(A,B)$ unabhängig ist, so sieht man leicht, dass $\LR(A,B) = 1$ gilt.
  Im unabhängigen Fall spricht $B$ also weder für noch gegen $A$, wie erwartet.
\end{para}

\begin{example}[Nummerierte Kugel ziehen]
  Wir befinden uns im Kontext von \autoref{stochainf/cha01:kugeln} mit $n=50$.
  Es sei $Q$ das Event, dass wir eine Primzahl ziehen, also:
  \begin{IEEEeqnarray*}{0l+l}
    Q = \set{2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47} &
    \card{Q} = 15
  \end{IEEEeqnarray*}
  Es gilt $\prob{Q} = 0.3$ und $\odds{Q} = \frac{3}{7}$.
  Angenommen, wir erfahren, dass die gezogene Kugel $\leq 10$ ist.
  Wie sollte nun unser Grad der Überzeugung bezüglich $Q$ sein?
  Es gilt:
  \begin{IEEEeqnarray*}{0l}
    \LR(\setn{10}, Q)
    = \frac{7}{3} \frac{\card{\set{2, 3, 5, 7}}}{\card{\set{1, 4, 6, 8, 9, 10}}}
    = \frac{7}{3} \frac{4}{6} = \frac{14}{9} \approx 1.556 > 1
  \end{IEEEeqnarray*}
  Die Beobachtung $\setn{10}$ spricht also für $Q$.
  Genauer haben wir:
  \begin{IEEEeqnarray*}{0l+l}
    \odds{Q \cond \setn{10}} = \frac{3}{7} \frac{14}{9} = \frac{2}{3} &
    \prob{Q \cond \setn{10}} = 0.4
  \end{IEEEeqnarray*}
\end{example}
