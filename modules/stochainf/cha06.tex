%%% Copyright 2024 Lasse Kliemann <l.kliemann@math.uni-kiel.de>
%%% CC BY-SA 4.0

\chapter{Konfidenzintervalle in Normalverteilungsmodellen}
\label{stochainf/cha06}

\lxklKitReviewWarning

In der Wissenschaft hat man oft die Aufgabe,
aus einer Stichprobe, als Element von $\RR^{n}$ gegeben,
auf die zugrundeliegenden Gesetzmäßigkeiten zu schließen.
Eine mögliche theoretische Grundlage dafür ist ein statistisches Modell.

\textit{Notation.}
In diesem Kapitel verwenden wir konsequent folgende vereinfachte Notation,
die in der Statistik üblich ist.
Nämlich für jede ZV $X$ und jede Funktion~$f$, so dass $f \circ X$ definiert ist und eine ZV ist,
schreiben wir $f(X) \df f \circ X$.
In den Situationen, die wir betrachten, wird das zu keinen Missverständnissen führen.

\section{Grundbegriffe}

\begin{para}[Statistisches Modell]
  Es sei $\Theta \subseteq \RR^{d}$ und $n \in \NNone$.
  Ein \term{statistisches Modell} ist eine Familie \statmodel,
  wobei $\cD^{\vtet}_{i}$ eine Verteilung für eine ZV ist,
  für alle $\vtet \in \Theta$ und alle $i \in \setn{n}$.
  Die Menge $\Theta$ nennen wir in diesem Kontext den \term{Parameterraum},
  und jedes Elemente davon einen \term{Parameter}.
\end{para}

\begin{para}[Stichprobenvariablen]
  Es sei \statmodel\xspace ein statistisches Modell.
  Zu jedem Parameter $\vtet \in \Theta$ gibt es nach Existenzsatz
  eine Familie $X^{\vtet}$ von ZV mit folgenden Eigenschaften:
  \begin{IEEEeqnarray}{0l+l}
    \label{stochainf/cha06:stich-zv}
    \text{$X^{\vtet}=\famiiint{X^{\vtet}_{i}}{n}$ ist unabhängig} &
    \forall i \in \setn{n} \holds X^{\vtet}_{i} \distr \cD^{\vtet}_{i}
  \end{IEEEeqnarray}
  Die ZV in der Familie $X^{\vtet}$ nennen wir die \term{Stichprobenvariablen}
  zum Parameter~$\vtet$.
  Streng genommen ist der bestimmte Artikel falsch,
  denn es könnte viele verschiedene solche Familien geben.
  Im Hinblick auf die Verteilung besteht jedoch Eindeutigkeit,
  und nur darauf kommt es uns an.
  \par
  Von nun an notieren wir unsere statistischen Modelle oft in der Form \statmodelX,
  was gleich die Stichprobenvariablen mit einführt.
  Mit dieser Schreibweise meinen wir, dass $X^{\vtet}$ für jedes $\vtet \in \Theta$ so ist,
  dass \eqref{stochainf/cha06:stich-zv} gilt.
  Für eine übersichtlichere Notation verzichten wir manchmal auf den Superskript $\vtet$
  an den Stichprobenvariablen
  und schreiben stattdessen $\probparam{}$, $\expectparam{}$ und $\varparam{}$.
\end{para}

\begin{para}[Vereinfachte Notation]
  Oft umfassen unsere statistischen Modelle nur eine oder zwei Verteilungen pro Parameter.
  Die Modelle können wir dann so notieren,
  wobei $\cD^{\vtet}$ \bzw $\cD^{\vtet}_{1}, \cD^{\vtet}_{2}$ für alle $\vtet \in \Theta$ Verteilungen sind:
  \begin{IEEEeqnarray*}{0l+l}
    \statmodelOne{X}{n}{\cD^{\vtet}}{\vtet}{\Theta} &
    \statmodelTwo{X}{n}{\cD^{\vtet}_{1}}{Y}{m}{\cD^{\vtet}_{2}}{\vtet}{\Theta}
  \end{IEEEeqnarray*}
\end{para}

\begin{example}[Modell mit uniformer Verteilung]
  \label{stochainf/cha06:model-unif}
  Es sei $n \in \NNone$ mit $n \geq 7$.
  Definiere $\Theta \df \set{(a,b) \in \RR^{2} \suchthat a < b}$,
  und betrachte folgendes Modell:
  \begin{IEEEeqnarray*}{0l}
    \statmodelOne{X}{n}{\Unif(a,b)}{(a,b)}{\Theta}
  \end{IEEEeqnarray*}
  Dann gilt für alle $(a,b) \in \Theta$:
  \begin{IEEEeqnarray*}{0l}
    \probparamx{(a,b)}{X_{1} > a + \frac{b-a}{3} \, \land \, X_{7} < a + \frac{b-a}{10}} \\
    = \probparamx{(a,b)}{X_{1} > a + \frac{b-a}{3}} \ccdot \probparamx{(a,b)}{X_{7} < a + \frac{b-a}{10}} \\
    = \frac{2}{3} \ccdot \frac{1}{10}
    = \frac{1}{15}
  \end{IEEEeqnarray*}
  Ferner gilt $\expectparamx{(a,b)}{X_{i}} = \frac{a+b}{2}$
  und $\varparamx{(a,b)}{X_{i}} = \frac{\p{b-a}^{2}}{12}$
  für alle $i \in \setn{n}$.
\end{example}

\begin{para}[Reelle Kenngröße]
  Es sei \statmodelX\ ein statistisches Modell und $\ffn{\tau}{\Theta}{\RR}$.
  Die Idee hinter $\tau$ ist, aus einem Parameter eine Größe zu berechnen,
  an der wir besonders interessiert sind.
  In diesem Kontext nennen wir $\tau$ eine \term{reelle Kenngröße}.
  Die Berechnung kann eine schlichte Extraktion sein,
  zum Beispiel $\ffnx{\tau}{\Theta}{\RR}{\vtet \mapsto \vtet_{1}}$.
  Oder es ist eine Berechnung, zum Beispiel im Kontext von \autoref{stochainf/cha06:model-unif}
  könnte man $\ffnx{\tau}{\Theta}{\RR}{(a,b) \mapsto \frac{a+b}{2}}$ betrachten,
  d.h., man ist am EW (der jeweiligen uniformen Verteilung) interessiert.
\end{para}

\begin{para}[Aufgabe]
  Wir beschreiben salopp und informell die grundsätzliche Aufgabe,
  mit der wir uns befassen wollen.
  Es sei $u \in \RR^{n}$ die Stichprobe.
  Die Aufgabe~ist:
  \begin{itemize}
  \item Wähle ein statistisches Modell \statmodelX\xspace so,
    dass Folgendes plausibel ist:
    Es gibt genau ein $\trueparam \in \Theta$, den \term{wahren Parameter},
    so dass $u$ durch einen Prozess entstanden ist,
    der gut modelliert wird durch $X^{\trueparam}$.
  \item Auf Grundlage von $u$ und dem Modell,
    berechne einen guten Tipp für $\tau(\trueparam)$.
  \end{itemize}
  Bei dem zweiten Punkt spricht man auch von einer Schätzung der reellen Kenngröße.
  Es gibt dafür zwei Ansätze:
  \begin{itemize}
  \item Punktschätzung: Es wird genau ein Wert berechnet.
  \item Intervallschätzung: Es wird ein Intervall berechnet.
  \end{itemize}
  Der Wert aus der Punktschätzung \bzw das Intervall bei der Intervallschätzung
  sollen in einem gewissen Zusammenhang zu $\tau(\trueparam)$ stehen.
  Im Folgenden zeigen wir verschiedene Ansätze auf,
  zunächst diese Ideen zu formalisieren
  und dann die sich daraus ergebenden konkreten Aufgaben zu lösen.
\end{para}

\section{Punktschätzung}

Unser hauptsächliches Thema ist die Intervallschätzung,
und wir schneiden die Punktschätzung nur kurz an.

\begin{para}[Punktschätzer]
  Es sei \statmodelX\ ein statistisches Modell und $\ffn{\tau}{\Theta}{\RR}$ eine reelle Kenngröße.
  Ein \term{Punktschätzer} für $\tau$ ist schlicht eine Funktion $\ffn{T}{\RR^{n}}{\RR}$.
  Eine nähere Beziehung zum Modell oder zu $\tau$ wird erst über weitere Eigenschaften von $T$ hergestellt.
  Eine mögliche solche Eigenschaft ist die Erwartungstreue.
\end{para}

\begin{para}[Erwartungstreue]
  Wir bleiben im Kontext des vorigen Absatzes.
  Für alle $\vtet \in \Theta$ definieren wir den \term{Bias} von $T$ für $\tau$
  als $\biasparam{T(X)} \df \expectparam{T(X)} - \tau(\vtet)$,
  womit implizit vorausgesetzt wird, dass dieser EW existiert.
  Wir nennen $T$ einen \term{erwartungstreuen} oder \term{unbiased} Punktschätzer für $\tau$
  (in diesem Modell),
  wenn der Bias stets~$0$ ist, d.h. wenn gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall \vtet \in \Theta \holds \expectparam{T(X)} = \tau(\vtet)
  \end{IEEEeqnarray*}
  Erwartungstreue bedeutet also:
  Egal was der wahre Parameter ist, im EW liefert $T(X)$ die gesuchte Kenngröße.
  Erwartungstreue ist nicht die einzige Eigenschaft, die einen guten Schätzer ausmachen kann;
  für eine ausführliche Diskussion lesen Interessierte \cite[Kapitel~7]{Georgii15}.
\end{para}

\begin{satz}[Erwartungstreue Schätzer für EW und Varianz]\hfill\\
  Es sei \statmodelOne{X}{n}{\cD^{\vtet}}{\vtet}{\Theta}\ ein statistisches Modell mit $n \geq 2$.
  Es seien $\ffn{\mu}{\Theta}{\RR}$ und $\ffn{v}{\Theta}{\RRnn}$ reelle Kenngrößen so,
  dass $\mu(\vtet) = \expect{\cD^{\vtet}}$ und ${v(\vtet) = \var{\cD^{\vtet}}}$ für alle $\vtet\in\Theta$ gilt.
  Dann sind\footnote{%
    Erinnerung: Für alle $u \in \RR^{n}$ notiere den \term{Mittelwert} von $u$ als
    ${\mean{u} \df \frac{1}{n} \sum_{i=1}^{n} u_{i}}$.}
  $\mean{\cdot}$ und
  \begin{IEEEeqnarray*}{0l}
    \ffnx{s^2}{\RR^{n}}{\RRnn}{u \mapsto \frac{1}{n-1} \sum_{i=1}^{n} \p[big]{u_{i} - \mean{u}}^{2}}
  \end{IEEEeqnarray*}
  jeweils erwartungstreue Schätzer für $\mu$ \bzw $v$.
\end{satz}

\begin{proof}
  Die Aussage über $\mu$ folgt leicht aus der Linearität des EW,
  und wir hatten das bereits festgehalten, als wir Simulationen betrachtet hatten.
  \par
  Es sei $\vtet \in \Theta$.
  Für alle $i \in \setn{n}$ gilt mit der Linearität des EW:
  \begin{IEEEeqnarray*}{0l}
    \expectparam{X_{i} - \mean{X}}
    = \expectparam{X_{i}} - \expectparam{X_{i}} = 0
  \end{IEEEeqnarray*}
  Es folgt für alle $i$:
  \begin{IEEEeqnarray*}{0l}
    \varparam{X_{1} - \mean{X}}
    = \varparam{X_{i} - \mean{X}} \\
    = \expectparam{\p{X_{i} - \mean{X} - 0}^{2}}
    = \expectparam{\p{X_{i} - \mean{X}}^{2}}
  \end{IEEEeqnarray*}
  Schließlich folgt:
  \begin{IEEEeqnarray*}{0l+x*}
    \expectparam{s^2(X)}
    = \frac{1}{n-1} \sum_{i=1}^{n} \expectparam{\p{X_{i} - \mean{X}}^{2}} \\
    = \frac{1}{n-1} \sum_{i=1}^{n} \varparam{X_{1} - \mean{X}} \\
    = \frac{1}{n-1} \sum_{i=1}^{n} \varparam{\p{1-\frac{1}{n}} X_{1} - \frac{1}{n} \sum_{j=2}^{n} X_{j}} \\
    = \frac{1}{n-1} \sum_{i=1}^{n} \p{\p{1-\frac{1}{n}}^{2} \varparam{X_{1}}
      + \frac{1}{n^{2}} \sum_{j=2}^{n} \varparam{X_{j}}} \\
    = \sum_{i=1}^{n} \p{\frac{n-1}{n^{2}} \varparam{X_{1}}
      + \frac{1}{n^{2} (n-1)} \sum_{j=2}^{n} \varparam{X_{1}}} \\
    = \sum_{i=1}^{n} \p{\p{\frac{1}{n} - \frac{1}{n^{2}}} \varparam{X_{1}}
      + \frac{1}{n^{2}} \varparam{X_{1}}}
    = \varparam{X_{1}} & \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\begin{para}[Bezeichnungen]
  Es sei $u \in \RR^{n}$ eine Stichprobe.
  Dann nennen wir \dots
  \begin{itemize}
  \item $\mean{u}$ den \term{empirischen Erwartungswert} oder \term{empirischen Mittelwert} von $u$;
  \item $s^{2}(u)$ die \term{empirische Varianz} von $u$;
  \item $s(u) \df \sqrt{s^{2}(u)}$ die \term{empirische Standardabweichung} von $u$.
  \end{itemize}
  Dabei ist die empirische Standardabweichung allerdings nicht erwartungstreu
  bezüglich der Standardabweichung der zugrundeliegenden Verteilung.
  Das heißt, im Kontext des obigen Satzes gilt \emphasis{nicht} im Allgemeinen
  $\expectparam{s(X)} = \sqrt{\var{\cD^{\vtet}}}$.
  Dennoch wird die empirische Standardabweichung in der Praxis
  oft als Punktschätzer für die Standardabweichung verwendet.
  Sie spielt auch bei der Intervallschätzung eine Rolle,
  dort auch für theoretische Resultate.
\end{para}

\section{Intervallschätzung}

Eine Punktschätzung auf Basis einer Stichprobe aus, sagen wir, $\RR^{1000}$,
überzeugt uns vom Gefühl her mehr als eine Stichprobe aus, sagen wir, $\RR^{20}$.
Bei einem erwartungstreuen Punktschätzer
können wir das sogar über die Varianz und die Tschebyschow\-/Ungleichung begründen.

Für den Rest dieses Kapitels betrachten wir ein Kalkül,
in dem als Schätzung kein einzelner Wert berechnet wird, sondern ein Intervall.
In der Regel wird dieses Intervall schmaler sein, je größer die Stichprobe ist.
Das Kalkül bietet außerdem eine Stellschraube, das \sog Konfidenzniveau,
über das wir zwischen Breite des Intervalls und einer gewissen Erfolgsw'keit balancieren können.

In diesem Abschnitt
sei stets \statmodelX\ ein statistisches Modell und $\ffn{\tau}{\Theta}{\RR}$ eine reelle Kenngröße.

\begin{para}[Konfidenzintervallschätzer]
  Es sei $\gam \in \intoo{0}{1}$ und $\ffn{K_{1},K_{2}}{\RR^{n}}{\RRbar}$.
  Dann nennen wir das Paar $(K_{1},K_{2})$ einen \term{Konfidenzintervallschätzer}
  oder \term{confidence interval estimator (CIE)}
  zum \term{Konfidenzniveau} oder \term{Niveau} $\gam$ für $\tau$ (in diesem Modell), wenn gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall \vtet \in \Theta \holds \probparam{\tau(\vtet) \in \intncc{K_{1}(X)}{K_{2}(X)}} \geq \gam
  \end{IEEEeqnarray*}
  Also, egal was der wahre Parameter ist, der CIE liefert mit W'keit mindestens $\gam$
  ein Intervall, in welchem die reelle Kenngröße des wahren Parameters liegt.
  Dabei bezieht sich die W'keit offenbar auf das Experiment,
  durch welches die Stichprobe gewonnen wird.
  Der CIE selbst ist deterministisch, bringt also keinerlei Zufall mit!
\end{para}

\begin{para}[Trivialer CIE]
  Definiere:
  \begin{IEEEeqnarray*}{0l+l}
    \ffnx{K_{1}}{\RR^{n}}{\RRbar}{u \mapsto \ninfty} &
    \ffnx{K_{2}}{\RR^{n}}{\RRbar}{u \mapsto \pinfty}
  \end{IEEEeqnarray*}
  Dann ist offenbar $(K_{1},K_{2})$ ein CIE zu jedem Niveau,
  für jede mögliche reelle Kenngröße,
  in jedem Modell (mit $n$ Stichprobenvariablen).
  Das zeigt, dass \emphasis{irgendein} CIE trivial anzugeben ist.
  Die Herausforderung ist offenbar, die Distanz $K_{2}-K_{1}$ gering zu halten,
  d.h. möglichst schmale Intervalle zu konstruieren.
\end{para}

\begin{para}[Berichten einer Konfidenzintervallschätzung]
  Es sei $(K_{1},K_{2})$ ein CIE zum Niveau $\gam$.
  In einer Anwendung haben wir eine Stichprobe $u \in \RR^{n}$
  und berechnen ein Intervall $\intncc{K_{1}(u)}{K_{2}(u)}$.
  Dies ist dann ein Intervall, ohne ZV, es gilt $K_{1}(u), K_{2}(u) \in \RR$.
  Ist nun folgende Aussage sinnvoll, wobei wieder $\trueparam$ den wahren Parameter bezeichnet?
  \begin{IEEEeqnarray}{0l}
    \label{stochainf/cha06:kis-falsch}
    \probparamx{\trueparam}{\tau(\trueparam) \in \intncc{K_{1}(u)}{K_{2}(u)}} \geq \gam
  \end{IEEEeqnarray}
  \emphasis{Nein!} Beachte, dass das Event keinerlei ZV mehr enthält.
  Für alle $\vtet \in \Theta$ gilt:
  \begin{IEEEeqnarray*}{0l}
    \probparam{\tau(\vtet) \in \intncc{K_{1}(u)}{K_{2}(u)}} =
    \begin{cases}
      0 & \text{wenn $\tau(\vtet) \not\in \intncc{K_{1}(u)}{K_{2}(u)}$} \\
      1 & \text{wenn $\tau(\vtet) \in \intncc{K_{1}(u)}{K_{2}(u)}$}
    \end{cases}
  \end{IEEEeqnarray*}
  Da $\gam > 0$ gilt, bedeutet \eqref{stochainf/cha06:kis-falsch},
  dass $\tau(\trueparam) \in \intncc{K_{1}(u)}{K_{2}(u)}$ gilt.
  Aber das wollen wir bestimmt nicht behaupten.
  \par
  Hier ist ein Vorschlag, wie man es stattdessen formulieren könnte:
  \begin{lxklquote}
    $\intncc{K_{1}(u)}{K_{2}(u)}$ ist ein Konfidenzintervall (CI) zum Niveau $\gam$
    für die reelle Kenngröße $\tau$ auf Grundlage der vorliegenden Stichprobe~$u$.
  \end{lxklquote}
  Noch besser ist, man gibt zusätzlich einen Hinweis, welchen CIE man genau verwendet hat.
  Anstelle von \enquote{für die reelle Kenngröße $\tau$}
  wird man meistens eine Beschreibung mit Worten wählen,
  zum Beispiel \enquote{für den Erwartungswert} oder \enquote{für die Varianz}.
\end{para}

\section{Vorbereitung}

Wir betrachten in den folgenden drei Abschnitten
jeweils eine Klasse statistischer Modelle, die auf der Normalverteilung beruhen.
Diese finden vielfach Anwendung in der Wissenschaft.
Wir entwickeln jeweils einen CIE für interessante reelle Kenngrößen.

Alle unsere CIE werden von der folgenden Form sein:

\begin{para}[Form unserer CIE]
  \label{stochainf/cha06:moe}
  Unsere CIE $(K_{1},K_{2})$ werden alle so aussehen,
  dass $K_{1} = \ciemean - \moe$ und $K_{2} = \ciemean + \moe$ gilt.
  Dabei sind $\ciemean$ und $\moe$ geeignete Funktionen.
  Naheliegenderweise nennt man $\moe$ die \term{Fehlerspanne} oder \term{margin of error}.
  Die Funktion~$\ciemean$, die das Zentrum des Intervalls bestimmt,
  wird stets eine Form von Mittelwert eines Tupels sein.
\end{para}

Auch nützlich ist folgender Begriff:

\begin{para}[Quantil]
  Es sei $\cD$ eine Verteilung mit $F_{\cD}$ streng monoton und stetig.
  Man kann zeigen, dass dann $\img(F_{\cD}) = \intoo{0}{1}$ gilt,
  also hat $F_{\cD}$ eine Umkehrfunktion $\ffn{F_{\cD}^{-1}}{\intoo{0}{1}}{\RR}$.
  Man schreibt $Q_{\cD} \df F_{\cD}^{-1}$ und nennt für alle $p \in \intoo{0}{1}$
  den Wert $Q_{\cD}(p)$ das \term{Quantil} von $\cD$ zur W'keit $p$.
  Es gibt auch eine Verallgemeinerung dieses Begriffes für allgemeine Verteilungen,
  den wir vorerst aber nicht brauchen werden.
  \par
  Man kann zeigen, dass die Normalverteilungen alle erforderlichen Voraussetzungen mitbringen,
  d.h. $F_{\Norm(\mu,\sig^{2})}$ ist streng monoton und stetig,
  also ist $Q_{\Norm(\mu,\sig^{2})}(p)$ definiert für alle $p \in \intoo{0}{1}$.
\end{para}

\begin{satz}[Rechenregeln für symmetrische PDF]
  \label{stochainf/cha06:symm}
  Es sei $\ffn{\pdfun}{\RR}{\RRnn}$ eine PDF, die symmetrisch um $0$ ist,
  d.h. $\pdfun(-v) = \pdfun(v)$ für alle $v \in \RR$.
  Es möge $F_{\rho}$ streng monoton sein.
  Es sei $X$ eine ZV mit PDF $\rho$ und $\cD$ die zugehörige Verteilung.
  Dann gilt:
  \begin{enumerate}
  \item $\forall v \geq 0 \holds \prob{-v \leq X \leq v} = 2 F_{X}(v) - 1$
  \item $\forall v \geq 0 \innerholds \forall \gam \in \intoo{0}{1}
    \holds \prob{-v \leq X \leq v} = \gam \iff v = Q_{\cD}\p{\frac{\gam+1}{2}}$
  \end{enumerate}
\end{satz}

\begin{proof}
  Übung.
\end{proof}

Wir betrachten nun die drei verschiedenen Modelle.

\section{Bekannte Varianz}

\begin{para}[Modell]
  Es sei $\sig > 0$ und $n \in \NNone$.
  Wir betrachten folgendes statistisches Modell:
  \begin{IEEEeqnarray}{0l}
    \IEEEtag{I}\label{stochainf/cha06:model-1}
    \statmodelOne{X}{n}{\Norm(\mu,\sig^{2})}{\mu}{\RR}
  \end{IEEEeqnarray}
  Wir möchten den EW schätzen,
  also die reelle Kenngröße ist schlicht $\ffnx{\tau}{\RR}{\RR}{\mu \mapsto \mu}$
  und braucht hier nicht als Funktion formuliert zu werden.
\end{para}

\begin{satz}[CIE bei bekannter Varianz]
  \label{stochainf/cha06:cie-1}
  Es sei $\gam \in \intoo{0}{1}$.
  Dann liefern folgende Definitionen für Zentrum und Fehlerspanne
  einen CIE zum Niveau $\gam$ für den EW im Modell \eqref{stochainf/cha06:model-1}:
  \begin{IEEEeqnarray*}{0l+l}
    \ciemean \df \mean{\cdot} & \moe \df \frac{\sig}{\sqrt{n}} Q_{\Norm(0,1)}\p{\frac{\gam+1}{2}}
  \end{IEEEeqnarray*}
  (Es ist also $\ciemean$ der Mittelwert, als Funktion $\fn{\RR^{n}}{\RR}$;
  und $\moe$ ist eine konstante Funktion,
  in deren Wert $\sig$, $n$ und $\gam$ einfließen.)
\end{satz}

\begin{proof}
  Es sei $\mu \in \RR$. Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    \mean{X^{\mu}} - \mu
    = \p{\frac{1}{n} \sum_{i=1}^{n} X_{i}^{\mu}} - \mu
    \distr \Norm\p{0, \frac{\sig^{2}}{n}}
  \end{IEEEeqnarray*}
  Es folgt:
  \begin{IEEEeqnarray}{0l}
    \label{stochainf/cha06:pivot-norm01}
    \frac{\mean{X^{\mu}} - \mu}{\sig / \sqrt{n}} \distr \Norm(0,1)
  \end{IEEEeqnarray}
  Notiere $q \df Q_{\Stud(n-1)}\p{\frac{\gam+1}{2}}$.
  Dann gilt
  \begin{IEEEeqnarray*}{0l"s+x*}
    \probparamx{\mu}{\mean{X} - \moe \leq \mu \leq \mean{X} + \moe} = \gam \\
    \iff \probparamx{\mu}{-\moe \leq \mean{X} - \mu \leq \moe} = \gam \\
    \iff \probparamx{\mu}{-q \leq \frac{\mean{X} - \mu}{\sig / \sqrt{n}} \leq q} = \gam \\
    \iff q = Q_{\Norm(0,1)}\p{\frac{\gam+1}{2}}
    & \eqref{stochainf/cha06:pivot-norm01} und \autoref{stochainf/cha06:symm}
    & \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\begin{para}[Diskussion]
  Entscheidend in dem Beweis ist,
  dass die Verteilung von $\mean{X^{\mu}} - \mu$ \bzw von $\frac{\mean{X^{\mu}} - \mu}{\sig / \sqrt{n}}$
  nur von bekannten Kenngrößen abhängt,
  nämlich von $\sig$ und $n$.
  Eine solche ZV, in der unbekannte Kenngrößen (hier~$\mu$) zwar vorkommen dürfen,
  deren \emphasis{Verteilung} aber von keiner unbekannten Kenngröße abhängt,
  nennt man eine \term{Schlüsselvariable} oder \term{Pivotvariable}.
  Damit eine Pivotvariable nützlich ist,
  muss sie natürlich in geeignetem Zusammenhang mit den Stichprobenvariablen
  und der gesuchten reellen Kenngröße stehen.
  Dieser Zusammenhang wird hier über die Umrechnung aus \autoref{stochainf/cha06:moe} eingesehen.
\end{para}

\begin{example}[Berechnung eines CI]
  % u <- signif(rnorm(11, mean=2, sd=3), digits=4)
  \label{stochainf/cha06:example-1}
  Es liegt folgende Stichprobe~$u \in \RR^{11}$ vor:
  \par\smallskip
  \begin{tabular}{l*{5}{n{2}{4}}}
    \toprule
    $u$
    & 5.431 & -1.33 & 2.722 & -0.5360 & 2.544 \\
    & 8.086 & -1.136 & -2.108 & -0.6016 & 3.899 \\
    & 4.471 \\
    \bottomrule
  \end{tabular}
  \par\smallskip
  Wir nehmen an, das Modell~\eqref{stochainf/cha06:model-1} ist angemessen,
  und wir wissen, dass die Varianz $\sig^2 = 9$ ist.
  Gesucht ist ein CI zum Niveau $0.95$ für den EW auf Grundlage dieser Stichprobe.
  Man errechnet schnell $\mean{u} \approx 1.949$.
  Das relevante Quantil $Q_{\Norm(0,1)}\parens{1.95/2} = Q_{\Norm(0,1)}\parens{0.975}$
  liest man in einer Tabelle ab oder benutzt in \R den Befehl \lstinline|qnorm|.
  Man erhält $Q_{\Norm(0,1)}\parens{1.95/2} = 1.9600$.
  Wir einigen uns, Quantile immer auf $5$ signifikante Stellen gerundet anzugeben
  und mit $=$ zu schreiben anstelle von $\approx$.
  Damit drücken wir aus, dass wir genau mit diesem Wert weiterrechnen.
  Sonst verwenden wir für Zwischenergebnisse immer~$\approx$,
  rechnen aber tatsächlich mit dem ungerundeten Wert im Taschenrechner weiter.
  \par
  Es folgt, dass $\intncc{0.1763}{3.722}$ ein CI zum Niveau $0.95$ für den EW ist
  auf Grundlage der vorliegenden Stichprobe $u$.
  Aus Gründen der Übersichtlichkeit lassen wir bei CI das Zeichen $\approx$ meistens weg,
  auch wenn es sich um gerundete Werte handelt.
\end{example}

\section{Unbekannte Varianz}

\begin{para}[Modell]
  Es sei $n \in \NNone$ mit $n \geq 2$.
  Wir betrachten folgendes statistisches Modell:
  \begin{IEEEeqnarray}{0l}
    \IEEEtag{II}\label{stochainf/cha06:model-2}
    \statmodelOne{X}{n}{\Norm(\mu,\sig^{2})}{(\mu,\sig^{2})}{\RR\times\RRpos}
  \end{IEEEeqnarray}
  Wir möchten wieder den EW schätzen, also die reelle Kenngröße ist:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{\tau}{\RR\times\RRpos}{\RR}{(\mu,\sig^{2}) \mapsto \mu}
  \end{IEEEeqnarray*}
\end{para}

\begin{para}[Ansatz]
  Die grundlegende Idee ist,
  die unbekannte Standardabweichung bei vorliegender Stichprobe $u$
  mithilfe der empirischen Standardabweichung $s(u)$ zu schätzen.
  Die Fehlerspanne, die im Modell mit bekannter Varianz
  die Form $\frac{\sig}{\sqrt{n}} q$ mit einem passenden Quantil $q$ hatte,
  bekommt nun die Form $\frac{s(u)}{\sqrt{n}} q$
  und hängt somit auch von der Stichprobe~$u$ ab.
  Man kann zeigen, dass es wieder eine Pivotvariable gibt,
  nämlich mit $(\mu,\sig^{2}) \in \RR\times\RRpos$ betrachte:
  \begin{IEEEeqnarray*}{0l}
    \frac{\mean{X^{(\mu,\sig^{2})}} - \mu}{s(X^{(\mu,\sig^{2})}) / \sqrt{n}}
  \end{IEEEeqnarray*}
  Diese hat keine Normalverteilung,
  sondern die \term{Studentverteilung}\footnote{%
    William S. Gosset, 1876--1937, englischer Statistiker,
    publizierte unter dem Pseudonym \enquote{Student}.}
  oder \term{$\Stud$\=/Verteilung}.
  Diese Verteilung hat einen Parameter, die \sog \term{Freiheitsgrade};
  die Studentverteilung mit $k$ Freiheitsgraden notieren wir als~$\Stud(k)$.
  Der Name \enquote{Freiheitsgrade} soll uns nicht weiter beschäftigen.
  Es genügt zu wissen, dass für das vorliegende Modell
  die richtige Wahl der Freiheitsgrade $n-1$ ist, also eins weniger als die Größe der Stichprobe.
  Wir legen dies im Folgenden dar.
\end{para}

\begin{para}[Studentverteilung]
  Es sei $k \in \NNone$.
  Wir sagen, eine ZV $X$ hat \term{Studentverteilung} oder \term{$\Stud$\=/Verteilung}
  mit $k$ Freiheitsgraden,
  in Zeichen $X \distr \Stud(k)$, wenn $X$ folgende Funktion als PDF hat:
  \begin{IEEEeqnarray*}{0l}
    \fnx{\RR}{\RRnn}{t \mapsto
      \frac{\Gamma\p{\frac{k+1}{2}}}{\sqrt{\pi k} \Gamma\p{\frac{k}{2}}}
      \p{1+\frac{t^{2}}{k}}^{-\frac{k+1}{2}}}
  \end{IEEEeqnarray*}
  Dabei ist $\Gam$ die \sog $\Gam$\=/Funktion, welches eine Verallgemeinerung der Fakultät ist.
  Wir brauchen $\Gam$ hier nicht weiter zu verstehen,
  zumal diese Funktion für die Studentverteilung offenbar nur benötigt wird,
  um die geeignete Skalierung herzustellen.
  \par
  Offenbar ist diese PDF symmetrisch, somit kann \autoref{stochainf/cha06:symm}
  für die Studentverteilung angewendet werden.
\end{para}

Die wesentliche Arbeit steckt in folgendem Satz,
welcher der Verdienst von William \enquote{Student} Gosset ist:

\begin{satz}[Verteilung der Pivotvariable]
  \label{stochainf/cha06:t}
  Es sei $\mu \in \RR$ und $\sig > 0$ und $n \in \NNone$ mit $n \geq 2$.
  Es sei $\zvtup{X}{n} \distr \Norm(\mu,\sig^{2})$ \iid Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    \frac{\mean{X} - \mu}{s(X) / \sqrt{n}}
    \distr \Stud(n-1)
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}
  Interessierte lesen \cite[Satz~9.17]{Georgii15}.
\end{proof}

\begin{para}[Empirische Standardabweichung gleich Null]
  Es mag im Kontext obigen Satzes die Frage aufkommen, ob der Nenner denn nicht~$0$ werden könnte.
  Mit unseren Kombinationssätzen über stetige Verteilungen überlegt man sich leicht,
  dass $s^{2}(X)$ und auch $s(X)$ eine stetige Verteilung hat.
  Es gilt also ${\prob{s(X) = 0} = 0}$.
  Dadurch wird es plausibel, dass dieser Fall für die Verteilung keine Rolle spielt.
  Um es etwas genauer einzusehen, definiere für jedes $y \in \RR$:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{f_{y}}{\RR^{n}}{\RR}{u \mapsto
      \begin{cases}
        \frac{\mean{u} - \mu}{s(u) / \sqrt{n}} & s(u) \neq 0 \\
        y & s(u) = 0
      \end{cases}}
  \end{IEEEeqnarray*}
  Damit ist $f_{y}(X)$ eine ZV, wo wir keine Sorge um Division durch Null zu haben brauchen.
  %%%
  %%% FIXME
  %%%
  %%% Hier brauchen wir einen Satz wie 8.17 (Rechenregeln für ZV) aber
  %%% für mehrere ZV. Das könnte man vielleicht in 8.19 integrieren
  %%% oder als extra Satz in der Nähe von 8.19 formulieren. Die
  %%% gemeinsame Verteilung in Ihrer Allgemeinheit ist dafür ja nicht
  %%% nötig.
  %%%
  Es gilt für alle $u,y,y' \in \RR$:
  \begin{IEEEeqnarray*}{0l}
    \prob{f_{y}(X) \leq u} \\
    = \prob{f_{y}(X) \leq u \land s(X) \neq 0} + \prob{f_{y}(X) \leq u  \land s(X) = 0} \\
    = \prob{f_{y}(X) \leq u \land s(X) \neq 0} \\
    = \prob{f_{y'}(X) \leq u \land s(X) \neq 0} \\
    = \prob{f_{y'}(X) \leq u \land s(X) \neq 0} + \prob{f_{y'}(X) \leq u  \land s(X) = 0} \\
    = \prob{f_{y'}(X) \leq u}
  \end{IEEEeqnarray*}
  Für die Verteilung von $f_{y}(X)$ ist also die Wahl von $y$ unerheblich.
  Das heißt, wir bekommen dieselbe Verteilung, egal wie wir die Division durch Null \enquote{reparieren}.
  So ist \autoref{stochainf/cha06:t} zu verstehen.
  \par
  Wenn man in der Praxis eine Stichprobe $u$ hat, für die $s(u)=0$ gilt,
  dann haben wir also \xmal{n} denselben Messwert.
  Damit stehen ganz andere Fragen an als die Berechnung eines CI,
  zum Beispiel sollte man die Messmethode und den Versuchsaufbau überprüfen.
  Daher wird auch für die Praxis die Division durch Null kein Thema werden.
\end{para}

\begin{satz}[CIE bei unbekannter Varianz]
  Es sei $\gam \in \intoo{0}{1}$.
  Dann liefern folgende Definitionen für Zentrum und Fehlerspanne
  einen CIE zum Niveau $\gam$ für den EW im Modell \eqref{stochainf/cha06:model-2}:
  \begin{IEEEeqnarray*}{0l+l}
    \ciemean \df \mean{\cdot} &
    \ffnx{\moe}{\RR^{n}}{\RRnn}{u \mapsto \frac{s(u)}{\sqrt{n}} Q_{\Stud(n-1)}\p{\frac{\gam+1}{2}}}
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}
  Analog zum Beweis von \autoref{stochainf/cha06:cie-1},
  wobei wir statt \eqref{stochainf/cha06:pivot-norm01} den \autoref{stochainf/cha06:t} zitieren
  für die Verteilung der Pivotvariable.
\end{proof}

\begin{example}[Berechnung eines CI]
  % u <- signif(rnorm(8, mean=5, sd=2), digits=4)
  \label{stochainf/cha06:example-2}
  Es liegt folgende Stichprobe~$u \in \RR^{8}$ vor:
  \par\smallskip
  \begin{tabular}{l*{5}{n{1}{3}}}
    \toprule
    $u$
    & 5.004 & 4.175 & 3.383 & 3.466 & 5.51 \\
    & 1.290 & 3.932 & 4.17 \\
    \bottomrule
  \end{tabular}
  \par\smallskip
  Wir nehmen an, das Modell~\eqref{stochainf/cha06:model-2} ist angemessen.
  Gesucht ist ein CI zum Niveau $0.95$ für den EW auf Grundlage dieser Stichprobe.
  Man errechnet schnell $\mean{u} \approx 3.866$
  und mit etwas mehr Aufwand auch ${s(u) \approx 1.266}$.
  Das relevante Quantil $Q_{\Stud(7)}\parens{1.95/2} = Q_{\Stud(7)}\parens{0.975}$
  liest man in einer Tabelle ab oder benutzt in \R den Befehl \lstinline|qnorm|;
  man erhält $Q_{\Stud(7)}\parens{1.95/2} = 2.3646$.
  Es folgt, dass $\intncc{2.808}{4.925}$ ein CI zum Niveau $0.95$ für den EW ist
  auf Grundlage der vorliegenden Stichprobe~$u$.
\end{example}

\begin{para}[Vorher-Nachher-Vergleich]
  Oft interessiert man sich dafür, wie eine Behandlung eine bestimmte Messgröße beeinflusst.
  Dazu stellen wir eine Gruppe von $n \in \NNone$, mit $n \geq 2$, Probanden zusammen.
  Die Messgröße vor der Behandlung modellieren wir mit ZV $\eli{A}{n}$,
  und die Messgröße nach der Behandlung mit ZV $\eli{B}{n}$,
  wobei $A_{i}$ und $B_{i}$ für Probanden~$i$ sind, für alle $i \in \setn{n}$.
  Definiere $D_i \df A_i - B_i$ für alle $i \in \setn{n}$.
  Wir nehmen an,
  es gibt $\trueparamx{\mugrA}, \trueparamx{\mugrA} \in \RR$ und $\trueparamx{v} \in \RRpos$ so,
  dass gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall i \in \setn{n} \holds \expect{A_{i}} = \trueparamx{\mugrA}
    \land \expect{B_{i}} = \trueparamx{\mugrB} \\
    \p{\eli{D}{n}} \distr \Norm(\trueparamx{\mugrA} - \trueparamx{\mugrB}, \, \trueparamx{v})
    \quad \text{\iid}
  \end{IEEEeqnarray*}
  Der EW der paarweisen Differenzen $\eli{D}{n}$ folgt dabei nach der Linearität des EW schon aus der Annahme
  über den EW der $\eli{A}{n}$ und der $\eli{B}{n}$.
  Abgesehen von diesen Annahmen dürfen die $\eli{A}{n},\eli{B}{n}$ beliebig verteilt sein.
  \par
  Wir betrachten $\trueparamx{\mugrA} - \trueparamx{\mugrB}$ als ein Maß dafür,
  wie stark die Behandlung die Messgröße senkt.
  Wie können wir dafür ein CI berechnen?
  Offenbar verhalten sich die $\eli{D}{n}$ wie die Stichprobenvariablen
  im Modell~\eqref{stochainf/cha06:model-2} zum Parameter
  ${(\trueparamx{\mugrA} - \trueparamx{\mugrB}, \, \trueparamx{v})}$.
  Wir können also die paarweisen Differenzen bilden und als Eingabe für den oben entwickelten CIE nehmen.
\end{para}

\begin{example}[Vorher-Nachher-Vergleich]
  % a <- signif(rnorm(10, mean=85, sd=9), digits=4)
  % b <- signif(x - rnorm(10, mean=10, sd=3), digits=4)
  \label{stochainf/cha06:example-3}
  Gegeben sind folgende Messwerte, wobei $a \in \RR^{10}$ vor der Behandlung
  und $b \in \RR^{10}$ nach der Behandlung gemessen wurde:
  \par\smallskip
  \begin{tabular}{l*{5}{n{2}{2}}}
    \toprule
    $a$
    & 88.14 & 74.96 & 92.00 & 96.60 & 72.04 \\
    & 97.16 & 88.22 & 86.35 & 76.70 & 86.63 \\
    \midrule
    $b$
    & 79.89 & 62.49 & 79.53 & 85.93 & 63.01 \\
    & 83.47 & 78.38 & 72.36 & 63.01 & 73.69 \\
    \bottomrule
  \end{tabular}
  \par\smallskip
  Wir nehmen an, das Modell~\eqref{stochainf/cha06:model-2} ist für die paarweisen Differenzen angemessen.
  Wir berechnen:
  \begin{IEEEeqnarray*}{0l+l}
    \mean{a-b} \approx 11.70 & s(a-b) \approx 2.093
  \end{IEEEeqnarray*}
  Wir berechnen ein CI zum Niveau $0.99$ für die Differenz der EW.
  Das relevante Quantil ist $Q_{\Stud(9)}(1.99/2) = 3.2498$.
  Wir erhalten das CI $\intncc{9.553}{13.86}$.
\end{example}

\section{Unbekannte Varianz -- zwei Gruppen}

\begin{para}[Modell]
  Es seien $\ngrA, \ngrB \in \NNone$ mit $\ngrA, \ngrB \geq 2$.
  Wir betrachten folgendes statistisches Modell:
  \begin{IEEEeqnarray}{0l}
    \IEEEtag{III}\label{stochainf/cha06:model-3}
    \statmodelTwo%
    {A}{\ngrA}{\Norm(\mugrA,\sig^{2})}%
    {B}{\ngrB}{\Norm(\mugrB,\sig^{2})}%
    {(\mugrA,\mugrB,\sig^{2})}{\RR\times\RR\times\RRpos}
  \end{IEEEeqnarray}
  Wir möchten die Differenz der EW schätzen,
  also die reelle Kenngröße ist:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{\tau}{\RR\times\RR\times\RRpos}{\RR}{(\mugrA,\mugrB,\sig^{2}) \mapsto \mugrA-\mugrB}
  \end{IEEEeqnarray*}
  Eine Stichprobe in diesem Modell ist ein Element von $\RR^{\ngrA+\ngrB}$,
  wir werden aber in naheliegender Weise eine Darstellung als Element
  von $\RR^{\ngrA}\times\RR^{\ngrB}$ verwenden.
\end{para}

\begin{para}[Pooled Standard Deviation]
  In der Fehlerspanne verwenden wir folgende Erweiterung der empirischen Standardabweichung,
  auch bekannt als \term{pooled standard deviation}:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{s}{\RR^{\ngrA} \times \RR^{\ngrB}}{\RRnn}%
    {(a,b) \mapsto \sqrt{\frac{(\ngrA-1) \ccdot s^2(a) + (\ngrB-1) \ccdot s^2(b)}{\ngrA+\ngrB-2}}}
  \end{IEEEeqnarray*}
  Dies ist eine, wie man zeigen kann, geeignete Mittelung
  zwischen den empirischen Standardabweichungen von $a$ und~$b$;
  beachte, dass der Nenner gleich $(n_{A}-1) + {(n_{B}-1)}$ ist,
  also die Summe der Gewichte, mit denen die empirischen Varianzen von $a$ und $b$ im Zähler eingehen.
  Wenn $\ngrA=\ngrB$ gilt, so reduziert es sich zu der einfacheren Formel
  $s(a,b) = \sqrt{\frac{s^2(a) + s^2(b)}{2}}$.
\end{para}

Wir geben ohne Beweis:

\begin{satz}[CIE bei unbekannter Varianz -- zwei Gruppen]
  Es sei $\gam \in \intoo{0}{1}$.
  Dann liefern folgende Definitionen für Zentrum und Fehlerspanne
  einen CIE zum Niveau $\gam$ für die Differenz der EW im Modell~\eqref{stochainf/cha06:model-3}:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{\ciemean}{\RR^{\ngrA} \times \RR^{\ngrB}}{\RR}{(a,b) \mapsto \mean{a}-\mean{b}} \\
    \ffnx{\moe}{\RR^{\ngrA} \times \RR^{\ngrB}}{\RR}{(a,b) \mapsto%
      s(a,b) \ccdot
      \sqrt{\frac{1}{\ngrA} + \frac{1}{\ngrB}} \ccdot
      Q_{\Stud(\ngrA+\ngrB-2)}\parens{\frac{\gam+1}{2}}}
  \end{IEEEeqnarray*}
\end{satz}

\begin{example}[Berechnung eines CI]
  % a <- signif(rnorm(10, mean=1, sd=1.5), digits=4)
  % b <- signif(rnorm( 8, mean=2, sd=1.5), digits=4)
  \label{stochainf/cha06:example-4}
  Es liegt folgende Stichprobe~$(a,b) \in {\RR^{10} \times \RR^{8}}$ vor:
  \par\smallskip
  \begin{tabular}{l*{5}{n{2}{4}}}
    \toprule
    $a$
    & 2.18 & 1.487 & 3.407 & -0.775 & 2.268 \\
    & -1.196 & 0.9737 & 0.2895 & -0.327 & 0.5844 \\
    \midrule
    $b$
    & 1.981 & 1.499 & -0.9851  & 2.167 & 3.348 \\
    & 4.185 & 1.691 & 1.733 \\
    \bottomrule
  \end{tabular}
  \par\smallskip
  Wir nehmen an, das Modell~\eqref{stochainf/cha06:model-3} ist angemessen.
  Gesucht ist ein CI zum Niveau $0.95$ für die Differenz der EW auf Grundlage dieser Stichprobe.
  Es gilt:
  \begin{IEEEeqnarray*}{0l+l+l}
    \mean{a-b} \approx -1.063 & s(a,b) \approx 1.484 & Q_{\Stud(16)}(1.99/2) = 2.9208
  \end{IEEEeqnarray*}
  Wir erhalten das CI $\intncc{-3.120}{0.9932}$.
\end{example}

\section{Modell~(\protect\ref*{stochainf/cha06:model-2}) und~(\protect\ref*{stochainf/cha06:model-3}) mit \R}

In \R sind unsere CIE für die beiden Modelle,
und noch vieles mehr, in der Funktion \lstinline|t.test| implementiert.
Wir demonstrieren die Bedienung,
indem wir die drei Beispiele \autoref{stochainf/cha06:example-2},
\autoref{stochainf/cha06:example-3} und \autoref{stochainf/cha06:example-4} bearbeiten.
Die Funktion \lstinline|t.test| gibt noch einige Informationen mehr aus als nur ein CI,
aber das CI ist deutlich erkennbar.

\begin{description}
\item[\autoref*{stochainf/cha06:example-2}:]
  Der Aufruf ist sehr einfach in diesem Beispiel,
  unter anderem da das Niveau von $0.95$ die Voreinstellung ist:
  \lstinputlisting{../stochainf/screen-example-2.txt}
\item[\autoref*{stochainf/cha06:example-3}:]
  Wir machen es zunächst so wie hier besprochen,
  nämlich wir bilden die Differenz und berechnen dafür ein CI mit demselben CIE wie im vorigen Beispiel:
  \lstinputlisting{../stochainf/screen-example-3a.txt}
  Die Funktion \lstinline|t.test| bietet alternativ eine Option \lstinline|paired| an:
  \lstinputlisting{../stochainf/screen-example-3b.txt}
\item[\autoref*{stochainf/cha06:example-4}:]
  Wir übergeben die Messwerte für die beiden Gruppen getrennt
  und geben außerdem an, dass die Varianz für die erste Gruppe gleich der Varianz der zweiten Gruppe ist,
  was ja eine Annahme dieses Modells ist:
  \lstinputlisting{../stochainf/screen-example-4a.txt}
  Wenn die Varianzen nicht gleich sind, oder man sich diesbezüglich nicht sicher ist,
  sollte man das \lstinline|var.equal=TRUE| weglassen.
  Es wird dann eine etwas andere Methode angewendet, nach dem Statistiker Welch\footnote{%
    Bernard L. Welch, 1911-1989, britischer Statistiker.} benannt.
  Wir führen das zur Demonstration mit der vorliegenden Stichprobe aus:
  \lstinputlisting{../stochainf/screen-example-4b.txt}
\end{description}
