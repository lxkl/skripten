%%% Copyright 2023 Lasse Kliemann <l.kliemann@math.uni-kiel.de>
%%% CC BY-SA 4.0

\chapter{Zufallsvariablen über diskreten W'räumen}

\section{Zufallsvariablen und logische Formeln}

\begin{para}[Zufallsvariablen]
  Es sei $\diskwraum$ ein diskreter W'raum.
  Wir werden fast nur solche W'räume betrachten in diesem Kapitel.
  Bemerke, dass wir die Potenzmenge als \sigalg gewählt haben.
  Das gibt uns die größtmögliche Ausdrucksmöglichkeit
  und führt (bei abzählbarem $\Om$) zu keinen Schwierigkeiten.
  Bei überabzählbarem $\Om$ müsste man hier mehr aufpassen,
  aber diese Diskussion verschieben wir auf spätere Kapitel.
  \par
  Jede Funktion von $\Om$ nach $\RR$ nennen wir eine \term{Zufallsvariable} (ZV) über diesem W'raum.
  Wann immer wir mit mehreren ZV zur Zeit arbeiten,
  so versteht sich (wenn nicht ausdrücklich anders erklärt), dass diese über demselben W'raum sind,
  der in diesem Kapitel ferner als diskret vorausgesetzt wird.
  Es hat sich gezeigt, dass es meistens einfacher ist,
  mit ZV zu arbeiten und nicht direkt mit dem darunterliegenden W'raum.
  Das wird im Laufe dieses und der folgenden Kapitel deutlich werden.
\end{para}

\begin{para}[Events über logische Formeln]
  Über ZV lassen sich Events elegant formulieren.
  Wir führen die zugehörige Notation nicht formal ein, sondern es genügen einige Beispiele.
  Es sei $X$ eine ZV, $U \subseteq \RR$ und $u \in \RR$.
  Beispiele für Notationen und Redeweisen:
  \begin{IEEEeqnarray*}{0l+s}
    \event{X \in U} \df \set{\om \in \Om \suchthat X(\om) \in U}
    & ($X$ nimmt einen Wert in $U$ an) \\
    \event{X \leq u} \df \set{\om \in \Om \suchthat X(\om) \leq u}
    & ($X$ nimmt einen Wert höchstens $u$ an) \\
    \event{X = u} \df \set{\om \in \Om \suchthat X(\om) = u}
    & ($X$ nimmt den Wert $u$ an)
  \end{IEEEeqnarray*}
  Wenn wir die W'keiten solcher Events notieren,
  dann lassen wir die eckigen Klammern meistens fort, also wir haben:
  \begin{IEEEeqnarray*}{0l+s}
    \prob{X \in U}
    & (W'keit, dass $X$ einen Wert in $U$ annimmt) \\
    \prob{X \leq u}
    & (W'keit, dass $X$ einen Wert höchstens $u$ annimmt) \\
    \prob{X = u}
    & (W'keit, dass $X$ den Wert $u$ annnimmt)
  \end{IEEEeqnarray*}
  In naheliegender Weise setzen wir das auf alle geeigneten logischen Formeln fort,
  auch mit mehreren ZV.
  Zum Beispiel für eine Familie $\famii{X_{i}}$ von ZV
  und eine Familie $\famii{U_{i}}$ von Teilmengen von $\RR$ haben wir:
  \begin{IEEEeqnarray*}{0l}
    \prob{\bigwedge_{i \in I} \p{X_{i} \in U_{i}}}
    = \prob{\forall {i \in I} \holds X_{i} \in U_{i}}
    = \prob{\set{\om \in \Om \suchthat \forall {i \in I} \holds X_{i}(\om) \in U_{i}}}
  \end{IEEEeqnarray*}
  Das ist die W'keit, dass $X_{i}$ eine Wert in $U_{i}$ annimmt, für alle $i \in I$.
\end{para}

\begin{para}[Implikation]
  Es seien $\vphi$ und $\psi$ Formeln dieser Art.
  Dann schreiben wir $\vphi \evimplies \psi$ und sagen, $\vphi$ \term{impliziert} $\psi$,
  wenn $\event{\vphi} \subseteq \event{\psi}$ gilt.
  Zu letzterem können wir aufgrund der im vorigen Kapitel im Zusammenhang mit Anwendungen eingeführten Redeweisen
  auch sagen: Wenn $\vphi$ eintritt, dann tritt auch $\psi$ ein.
  Das wiederum macht die Notation $\vphi \evimplies \psi$ besonders schön.
  \par
  Man überlegt sich leicht, dass $\vphi \evimplies \psi$ jedenfalls dann gilt, wenn wir Folgendes haben:
  Betrachten wir die ZV in den beiden Formeln als freie Variablen,
  dann gilt die übliche Implikation $\vphi \implies \psi$ unter Allquantifizierung dieser Variablen
  über die Bildmengen der jeweiligen ZV.
  So kann man zum Beispiel mit einem Blick erkennen,
  dass für alle ZV $X$ und alle $a,b \in \RR$ mit $a \leq b$ gilt:
  $X \leq a \evimplies X \leq b$,
  also $\event{X \leq a} \subseteq \event{X \leq b}$,
  also $\prob{X \leq a} \leq \prob{X \leq b}$.
\end{para}

\begin{para}[Äquivalenz]
  Wir schreiben $\vphi \eviff \psi$ und sagen, $\vphi$ und $\psi$ sind \term{äquivalent},
  wenn $\event{\vphi} = \event{\psi}$ gilt.
  Offenbar gilt dies genau dann, wenn $\vphi \evimplies \psi \land \psi \evimplies \vphi$ gilt.
  Den Trick mit der Allquantifizierung über reelle Variablen können wir auch hier anwenden.
\end{para}

\begin{para}[Gegenseitiger Ausschluss]
  Die bekannten falschen und wahren Aussagen $\falseprop$ und $\trueprop$ werden wir auch verwenden,
  offenbar ergibt sich ${\event{\falseprop} = \emptyset}$ und $\event{\trueprop} = \Om$.
  Wir sagen, $\vphi$ und $\psi$ \term{schließen sich gegenseitig aus},
  wenn ${\vphi \land \psi \eviff \falseprop}$ gilt.
  Wenn sich $\vphi$ und $\psi$ gegenseitig ausschließen, gilt natürlich $\prob{\vphi \land \psi} = 0$.
\end{para}

\begin{para}[Redeweise]
  Von nun an werden wir die Bezeichnung \enquote{Event} im Sinne einer logischen Formel
  wie den hier vorgestellten verstehen,
  wenn nicht anders ausgewiesen oder durch den Kontext anders bestimmt.
  Wir fomulieren für zukünftige Referenz eine Version
  von \autoref{stochainf/cha01:wkeit} für solche Events
  und integrieren auch gleich die Konditionierung und nehmen einige Zusammenfassungen vor.
\end{para}

\begin{satz}[Umgang mit W'keiten]
  \label{stochainf/cha02:wkeit}
  Es seien $\vphi,\psi,\bet$ Events und $\seq{\phi_{i}}{i\geq1}$ ein Folge von Events,
  wobei $\prob{\bet} > 0$ gelte.
  Dann gelten:
  \begin{enumerate}
  \item $\prob{\falseprop\cond\bet} = 0$ und $\prob{\trueprop\cond\bet} = 1$
  \item $\prob{\neg \vphi \cond \bet} = 1 - \prob{\vphi \cond \bet}$
  \item $\prob{\vphi\lor\psi\cond\bet} = \prob{\vphi\cond\bet} + \prob{\psi\cond\bet} - \prob{\vphi\land\psi\cond\bet}$
  \item $\prob{\vphi\lor\psi\cond\bet} = \prob{\vphi\cond\bet} + \prob{\psi\cond\bet}$,
    wenn $\prob{\vphi \land \psi \cond \bet} = 0$
  \item $\prob{\vphi\lor\psi\cond\bet} \leq \prob{\vphi\cond\bet} + \prob{\psi\cond\bet}$
  \item $\prob{\lorone{i}\phi_{i}\cond\bet} \leq \sumone{i} \prob{\phi_{i}\cond\bet}$
  \item $\prob{\psi\land\neg\vphi\cond\bet} = \prob{\psi\cond\bet} - \prob{\vphi\cond\bet}$, wenn $\vphi\evimplies\psi$
  \item $\prob{\vphi\cond\bet} \leq \prob{\psi\cond\bet}$, wenn $\vphi\evimplies\psi$
  \item $\limx{k} \prob{\bigvee_{i=1}^{k} \phi_{i} \cond \bet} = \prob{\lorone{i} \phi_{i} \cond \bet}$
  \end{enumerate}
\end{satz}

\section{Verteilung}

\begin{para}[Verteilung und PMF]
  \label{stochainf/cha02:verteil-1}
  Es sei $X$ eine ZV.
  Man sieht leicht, dass Folgendes ein W'maß auf dem Eventraum $(\RR,\cP(\RR))$ ergibt:\footnote{%
    Dieser Eventraum ist nicht diskret.
    Wir betrachten über diesem aber auch keine ZV.}
  \begin{IEEEeqnarray*}{0l}
    \fnx{\cP(\RR)}{\intcc{0}{1}}{U \mapsto \prob{X \in U}}
  \end{IEEEeqnarray*}
  Wir nennen dieses W'maß die \term{Verteilung} von $X$.
  Wie können wir diese konzise darstellen?
  Wir definieren den \term{Träger} von $X$ als:
  \begin{IEEEeqnarray*}{0l}
    \supp(X) \df \set{u \in \RR \suchthat \prob{X=u} > 0}
  \end{IEEEeqnarray*}
  Offenbar gilt $\supp(X) \subseteq \img(X)$, und letztere Menge ist abzählbar,
  also ist auch $\supp(X)$ abzählbar.
  Für alle $U \subseteq \RR$ gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{X \in U} = \prob{X \in U \cap \supp(X)} + \prob{X \in U \setminus \supp(X)} \\
    = \prob{X \in U \cap \supp(X)}
    = \sum_{u \in U \cap \supp(X)} \prob{X=u}
  \end{IEEEeqnarray*}
  Es genügt also, die W'keiten der \term{Punktevents} $X = u$ für alle $u \in \supp(X)$ zu kennen,
  um $\prob{X \in U}$ für alle $U \subseteq \RR$ zu berechnen.
  Mit dem Verständnis, dass die Summe nur abzählbar viele Summanden $\neq 0$ enthält,
  schreibt man auch kurz:
  \begin{IEEEeqnarray*}{0l}
    \prob{X \in U} = \sum_{u \in U} \prob{X=u}
  \end{IEEEeqnarray*}
  Das werden wir zukünftig öfters kommentarlos so handhaben.
  \par
  Wir nennen die Funktion $\ffnx{\rho_{X}}{\RR}{\intcc{0}{1}}{u \mapsto \prob{X=u}}$
  die \term{Zähldichte} oder die \term{probability mass function (PMF)} von $X$.
  Die PMF wird unsere bevorzugte Weise sein, um die Verteilung einer ZV anzugeben (über einem diskreten W'raum).
\end{para}

\begin{para}[Kartesisches Produkt einer Familie]
  \label{stochainf/cha02:kart-fami}
  Es sei $I$ eine endliche Indexmenge und $\cM$ eine Menge.
  Für eine Familie $M = \famii{M_{i}} \in \cP(\cM)^{I}$
  von Teilmengen von $\cM$ definiere deren kartesisches Produkt:
  \begin{IEEEeqnarray*}{0l}
    \bigtimes_{i \in I} M_{i} = \set{\famii{x_{i}} \suchthat \forall i \in I \holds x_{i} \in M_{i}}
  \end{IEEEeqnarray*}
  Im Fall $I = \setn{n}$ für ein $n \in \NN$ ist dies das bekannte kartesische Produkt von~$n$ Mengen.
  Ist $M_{i}$ abzählbar für jedes~$i$, so ist $\bigtimes_{i \in I} M_{i}$ auch abzählbar.
  \par
  Wir verwenden meistens eine vereinfachte Schreibweise,
  nämlich wir schreiben schlicht die Familie selbst,
  also $M$ oder $\famii{M_{i}}$, anstelle von $\bigtimes_{i \in I} M_{i}$.
  Genauer schreiben wir:
  \begin{IEEEeqnarray*}{0l}
    \forall x = \famii{x_{i}} \in \cM^{I}
    \holds x \in \famii{M_{i}} \dfiff \forall i \in I \holds x_{i}  \in M_{i}
  \end{IEEEeqnarray*}
  Die Mengeninklusion $\subseteq$ wird entsprechend verstanden.
  Unsere Schreibweise ist vor allem daher von Vorteil,
  weil sie auch im Fall $\card{I}=1$ natürlich erscheint.
\end{para}

\begin{para}[Verteilung einer Familie]
  \label{stochainf/cha02:verteil-2}
  Wir erweitern die Begriffe des vorigen Absatzes von einer ZV auf endliche Familien von ZV.
  Dabei ändert sich nicht viel, es wird nur notationell aufwändiger.
  Es sei $X = \famii{X_{i}}$ eine endliche Familie von ZV.
  Für Familien $u = \famii{u_{i}} \in \RR^{I}$ und $U = \famii{U_{i}} \in \cP(\RR)^{I}$
  verstehen wir nach vorigem Absatz die folgenden Events:
  \begin{IEEEeqnarray*}{0l+l}
    X \in U \eviff \forall i \in I \holds X_{i} \in U_{i} &
    X = u \eviff \forall i \in I \holds X_{i} = u_{i}
  \end{IEEEeqnarray*}
  Man sieht wieder leicht, dass Folgendes ein W'maß auf dem Eventraum $(\RR^{I},\cP(\RR^{I}))$ ergibt:
  \begin{IEEEeqnarray*}{0l}
    \fnx{\cP(\RR^{I})}{\intcc{0}{1}}{U \mapsto \prob{X \in U}}
  \end{IEEEeqnarray*}
  Wir nennen dieses W'maß die \term{Verteilung} von $X$.
  Im Falle $\card{I} \geq 2$ nennen wir es zur Verdeutlichung,
  dass wir mehrere ZV haben, manchmal die \term{gemeinsame Verteilung} von~$X$.
  Wieder suchen wir nach Wegen, eine solche Verteilung konzise anzugeben.
  Definiere den \term{Träger} von $X$ als:
  \begin{IEEEeqnarray*}{0l}
    \supp(X) \df \set{u \in \RR^{I} \suchthat \prob{X=u} > 0}
    \subseteq \famii{\img(X_{i})}
  \end{IEEEeqnarray*}
  Der Träger ist als Teilmenge des kartesischen Produktes der Bildmengen wieder abzählbar.
  Analog zum Fall einer ZV überlegt man sich, dass gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{X \in U}
    = \sum_{u \in U \cap \supp(X)} \prob{X=u}
    = \sum_{u \in U} \prob{X=u}
  \end{IEEEeqnarray*}
  Wir erhalten also äußerlich dieselbe Formel.
  Der Unterschied zu vorher ist, dass wir bei den Summen über Familien von Zahlen laufen,
  deren Indexmenge jeweils $I$ ist.
  Die letzte Summe schreiben wir wieder im Wissen, dass nur abzählbar viele Summanden $\neq 0$ sind.
  Wir nennen die Funktion $\ffnx{\rho_{X}}{\RR^{I}}{\intcc{0}{1}}{u \mapsto \prob{X=u}}$
  die \term{Zähldichte} oder die \term{probability mass function (PMF)} von $X$.
  Im Falle $\card{I} \geq 2$ sprechen wir zur Verdeutlichung manchmal von der \term{gemeinsamen PMF} von~$X$.
  Der Fall $\card{I} = 1$ entspricht genau dem aus \autoref{stochainf/cha02:verteil-1} bekannten.
  \par
  Offenbar hängt die PMF einer ZV oder einer Familie von ZV nur von deren Verteilung ab.
  Man spricht daher auch von der PMF einer Verteilung,
  und wir schreiben $\rho_{\cD}$ dafür, wobei $\cD$ die Verteilung bezeichnet
  (wir lernen am Ende dieses Abschnitts Beispiele kennen,
  welche den Platz von $\cD$ einnehmen können).
\end{para}

\begin{para}[Notation als Tupel]
  Wer die Familien unhandlich findet, darf gerne zunächst an den Spezialfall $I=\setn{n}$
  mit $n \in \NNone$ denken, also wo wir ein Tupel $X=(\eli{X}{n})$ von ZV haben.
  Dann sieht das so aus:
  \begin{IEEEeqnarray*}{0l}
    X = u \eviff (\eli{X}{n}) = (\eli{u}{n}) \eviff \forall i \in \setn{n} \holds X_{i} = u_{i} \\
    \supp(X) = \set{u \in \RR^{n} \suchthat \prob{X=u} > 0}
    \subseteq \bigtimes_{i=1}^{n} \img(X_{i}) \subseteq \RR^{n} \\
    \ffnx{\rho_{X}}{\RR^{n}}{\intcc{0}{1}}{u \mapsto \prob{X=u}}
  \end{IEEEeqnarray*}
\end{para}

\begin{para}[Verteilung einer Teilfamilie]
  \label{stochainf/cha02:margin}
  Es sei $\famii{X_{i}}$ eine endliche Familie von ZV,
  deren Verteilung uns bekannt sein möge.
  Es sei $J \subseteq I$.
  Dann kennen wir auch die Verteilung von $\famij{X_{j}}$,
  nämlich für alle $\famij{u_{j}} \in \RR^{J}$ gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{\famij{X_{j}} = \famij{u_{j}}} \\
    = \prob[big]{\p{\famij{X_{j}} = \famij{u_{j}}}
      \land \p{\fami{X_{i}}{i}{I \setminus J} \in \fami{\supp(X_{i})}{i}{I \setminus J}}} \\
    = \prob{\p[big]{\famij{X_{j}} = \famij{u_{j}}}
      \land \p[big]{\dotbigvee_{\fami{u_{i}}{i}{I \setminus J} \in \fami{\supp(X_{i})}{i}{I \setminus J}}
        \fami{X_{i}}{i}{I \setminus J} = \fami{u_{i}}{i}{I \setminus J}}} \\
    = \prob{\dotbigvee_{\fami{u_{i}}{i}{I \setminus J} \in \fami{\supp(X_{i})}{i}{I \setminus J}}
      \p[big]{\p{\famij{X_{j}}=\famij{u_{j}}} \land \p{\fami{X_{i}}{i}{I \setminus J}=\fami{u_{i}}{i}{I \setminus J}}}} \\
    = \prob[big]{\dotbigvee_{\fami{u_{i}}{i}{I \setminus J} \in \fami{\supp(X_{i})}{i}{I \setminus J}}
      \famii{X_{i}} = \famii{u_{i}}} \\
    = \sum_{\fami{u_{i}}{i}{I \setminus J} \in \fami{\supp(X_{i})}{i}{I \setminus J}} \prob{\famii{X_{i}} = \famii{u_{i}}} \\
    = \sum_{\fami{u_{i}}{i}{I \setminus J} \in \fami{\supp(X_{i})}{i}{I \setminus J}} \rho_{X}(\famii{u_{i}})
  \end{IEEEeqnarray*}
  Eine Rechnung dieser Art nennt man eine \term{Marginalisierung}.
  Eine angebliche Motivation dieser Bezeichnung liegt darin,
  dass die Einbringung des Events
  \begin{IEEEeqnarray*}{0l}
    \fami{X_{i}}{i}{I \setminus J} \in \fami{\supp(X_{i})}{i}{I \setminus J}
  \end{IEEEeqnarray*}
  in der zweiten Zeile der Rechnung zum Ausdruck bringt,
  dass uns egal ist, was die ZV in der Familie $\fami{X_{i}}{i}{I \setminus J}$ machen.
  Diese ZV werden somit marginalisiert.
  Es gibt noch eine ganz andere Motivation für diese Bezeichnung, die wir im nächsten Abschnitt kennenlernen werden.
\end{para}

\begin{para}[PMF]
  Es sei $I$ eine endliche Indexmenge.
  Für jede Funktion $\ffn{\rho}{\RR^{I}}{\intcc{0}{1}}$ definieren wir deren \term{Träger} als:
  \begin{IEEEeqnarray*}{0l}
    \supp(\rho) \df \set{u \in \RR^{I} \suchthat \rho(u) > 0}
  \end{IEEEeqnarray*}
  Wir nennen $\rho$ eine \term{Zähldichte} oder eine \term{probability mass function (PMF)},
  wenn gilt:
  \begin{itemize}
  \item $\supp(\rho)$ ist abzählbar;
  \item $\sum_{u \in \supp(\rho)} \rho(u) = 1$.
  \end{itemize}
  Offenbar ist die PMF einer jeden endlichen Familie von ZV eine PMF in diesem Sinne.
  Der folgende Satz zeigt, dass auch der umgekehrte Weg möglich ist.
\end{para}

\begin{satz}[Existenz bei gegebener PMF]
  \label{stochainf/cha02:exists-pmf}
  Es sei $I$ eine endliche Indexmenge und $\ffn{\rho}{\RR^{I}}{\intcc{0}{1}}$ eine PMF.
  Dann gibt es einen diskreten W'raum und darüber eine Familie $X = \famii{X_{i}}$ von ZV so,
  dass $\rho_{X} = \rho$ gilt.
  Ferner kann $X$ so gewählt werden, dass $\img(X) = \supp(\rho)$ gilt.
\end{satz}

\begin{proof}
  Definiere $\Om \df \supp(\rho)$.
  Für alle $\om \in \Om$ definiere $\prob{\om} \df \rho(\om)$,
  und setze $\prob{}$ zu einem W'maß auf dem Messraum $(\Om, \cP(\Om))$ fort;
  vergleiche \autoref{stochainf/cha01:fortsetz-elementar}.
  Für alle $i \in I$ definiere $\ffnx{X_{i}}{\Om}{\RR}{\om \mapsto \om_{i}}$.
\end{proof}

\begin{para}[Uniforme Verteilung]
  Es sei $I$ eine endliche Indexmenge und $M \subseteq \RR^{I}$ endlich.
  Für eine Familie ${X = \famii{X_{i}}}$ von ZV schreiben wir $X \distr \Unif(M)$
  und sagen, dass $X$ auf $M$ \term{uniform verteilt} ist
  oder auf $M$ \term{uniforme Verteilung} hat, wenn gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall u \in M \holds \prob{X=u} = \frac{1}{\card{M}}
  \end{IEEEeqnarray*}
  Zu jeder solchen Menge $M$ findet man eine solche Familie von ZV nach obigem Existenzsatz,
  denn Folgendes ist eine PMF, wie man leicht nachrechnet:
  \begin{IEEEeqnarray*}{0l}
    \fnx{\RR^{I}}{\intcc{0}{1}}{u \mapsto
      \begin{cases}
        \frac{1}{\card{M}} & u \in M \\
        0 & \text{sonst}
      \end{cases}}
  \end{IEEEeqnarray*}
  Dies ist also $\rho_{\Unif(M)}$, die PMF der Verteilung $\Unif(M)$.
\end{para}

\begin{example}
  \label{stochainf/cha02:example-unif}
  Es sei $(X_{1}, X_{2}) \distr \Unif(\set{(0,1), (0,2), (2,1)})$.
  Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{(X_{1}, X_{2}) = (0,1)}
    = \prob{(X_{1}, X_{2}) = (0,2)}
    = \prob{(X_{1}, X_{2}) = (2,1)}
    = \frac{1}{3}
  \end{IEEEeqnarray*}
  Beispielhaft führen wir eine Marginalisierung aus:
  \begin{IEEEeqnarray*}{0l}
    \prob{X_{1} = 0}
    = \prob{(X_{1} = 0) \land (X_{2} = 1 \lor X_{2} = 2)} \\
    = \prob{(X_{1}, X_{2}) = (0,1) \lor (X_{1}, X_{2}) = (0,2)} \\
    = \prob{(X_{1}, X_{2}) = (0,1)} + \prob{(X_{1}, X_{2}) = (0,2)}
    = \frac{2}{3}
  \end{IEEEeqnarray*}
  Bemerke, wie wir uns bei der Berechnung zunutze machen,
  dass wir uns auf Werte im Träger der jeweiligen ZV beschränken dürfen.
\end{example}

\begin{para}[Bernoulliverteilung]
  \label{stochainf/cha02:ber}
  Es sei $p \in \intcc{0}{1}$.
  Für eine ZV $X$ schreiben wir ${X \distr \Ber(p)}$
  und sagen, dass $X$ \term{Bernoulliverteilung mit Erfolgsw'keit} $p$ hat,
  wenn gilt:
  \begin{IEEEeqnarray*}{0l+l}
    \prob{X=1} = p & \prob{X=0} = 1-p
  \end{IEEEeqnarray*}
  Wieder nach dem Existenzsatz findet man zu jedem $p$ eine solche ZV,
  denn Folgendes ist eine PMF:
  \begin{IEEEeqnarray*}{0l}
    \fnx{\RR}{\intcc{0}{1}}{u \mapsto
      \begin{cases}
        p & u = 1 \\
        1-p & u = 0 \\
        0 & \text{sonst}
      \end{cases}}
  \end{IEEEeqnarray*}
  Dies ist also $\rho_{\Ber(p)}$, die PMF der Verteilung $\Ber(p)$.
\end{para}

\section{Unabhängigkeit}

\begin{para}[Definition]
  Es sei $X = \famii{X_{i}}$ eine endliche Familie von ZV.
  Diese nennen wir \term{unabhängig}, wenn gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall U = \famii{U_{i}} \in \cP(\RR)^{I} \holds
    \prob{X \in U} = \prod_{i\in I} \prob{X_{i} \in U_{i}}
  \end{IEEEeqnarray*}
  Etwas anders --~und vielleicht gefälliger~-- geschrieben:
  \begin{IEEEeqnarray*}{0l}
    \forall \famii{U_{i}} \in \cP(\RR)^{I} \holds
    \prob{\bigwedge_{i \in I} \p{X_{i} \in U_{i}}} = \prod_{i\in I} \prob{X_{i} \in U_{i}}
  \end{IEEEeqnarray*}
  Offenbar erhalten wir denselben Begriff,
  wenn wir uns auf solche $\famii{U_{i}} \in \cP(\RR)^{I}$ beschränken,
  für die $U_{i} \subseteq \supp(X_{i})$ gilt, die also insbesondere abzählbar sind.
  Offenbar bleibt Unabhängigkeit von ZV erhalten bei Umindizierung.
  \par
  Anders als bei Events betrachten wir bei der Unabhängigkeit von ZV Teilfamilien nicht explizit;
  diese werden automatisch berücksichtigt wie man im nach dem Beispiel unten folgenden Satz und Beweis sieht.
  Es wird dann auch deutlich, dass sich Unabhängigkeit von ZV auf Teilfamilien überträgt;
  also wenn $\famii{X_{i}}$ unabhängig ist, dann auch $\famij{X_{j}}$ für alle $J \subseteq I$.
\end{para}

\begin{example}
  \label{stochainf/cha02:ex-1}
  Die Verteilung von $(X,Y)$ sei durch die in folgender Tabelle gegebene PMF von $(X,Y)$ festgelegt,
  was laut unserem Existenzsatz zulässig ist,
  da die Zahlen aus $\intcc{0}{1}$ sind und sich insgesamt zu~$1$ addieren:\par
  \begin{tabular}{r|n{1}{2}n{1}{2}n{1}{2}n{1}{2}}%
    \toprule
    \diagbox{$X$}{$Y$}&1&2&3&4\\
    \midrule
    1&0.1&0.15&0.1&0.2\\
    2&0.05&0.01&0.05&0.02\\
    3&0.04&0.25&0.02&0.01\\
    \bottomrule
  \end{tabular}
  \medskip\par
  Wir untersuchen $(X,Y)$ auf Unabhängigkeit.
  Es gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{X=1} = 0.1 + 0.15 + 0.1 + 0.2 = 0.55 \\
    \prob{Y=1} = 0.1 + 0.05 + 0.04 = 0.19 \\
    \prob{X=1 \land Y=1} = 0.1 \neq 0.1045 = \prob{X=1} \ccdot \prob{Y=1}
  \end{IEEEeqnarray*}
  Also ist $(X,Y)$ nicht unabhängig.
  \par
  Bei der Berechnung von $\prob{X=1}$ und von $\prob{Y=1}$ haben wir letztlich
  die W'keiten der ersten Zeile \bzw der ersten Spalte der Tabelle addiert.
  Entsprechend würde man vorgehen, wenn man die Verteilung von $X$ und von $Y$ jeweils vollständig erschließt:
  Man summiert entlang von Zeilen \bzw Spalten.
  Es ist üblich, diese Summen dann am rechten \bzw unteren Rand der Tabelle zu notieren.
  \enquote{Rand} heißt auf Englisch \enquote{margin},
  womit wir die zweite Motivation für die Bezeichnung \enquote{Marginalisierung} haben;
  vergleiche \autoref{stochainf/cha02:margin}.
\end{example}

\begin{satz}[Unabhängigkeit von ZV und Events]
  Es sei $\famii{X_{i}}$ eine endliche Familie von ZV.
  Dann sind äquivalent:
  \begin{enumerate}
  \item Die Familie von ZV $\famii{X_{i}}$ ist unabhängig.
  \item Die Familie von Events $\famii{X_{i} \in U_{i}}$ ist unabhängig
    für alle $\famii{U_{i}} \in \cP(\RR)^{I}$.
  \end{enumerate}
\end{satz}

\begin{proof}
  \imprefx{1}{2}
  Es sei $\famii{U_{i}} \in \cP(\RR)^{I}$ und $I' \subseteq I$.
  Dann gilt:
  \begin{IEEEeqnarray*}{0l"s}
    \prob{\bigwedge_{i \in I'} \p{X_{i} \in U_{i}}} \\
    = \prob{\bigwedge_{i \in I'} \p{X_{i} \in U_{i}}
      \land \bigwedge_{i \in I \setminus I'} \p{X_{i} \in \RR}} \\
    = \prod_{i \in I'} \prob{X_{i} \in U_{i}} \prod_{i \in I \setminus I'} \prob{X_{i} \in \RR}
    & Unabhängigkeit der ZV \\
    = \prod_{i \in I'} \prob{X_{i} \in U_{i}}
    & $\forall i \in I \holds \prob{X_{i} \in \RR} = 1$
  \end{IEEEeqnarray*}
  \imprefx{2}{1} trivial.
\end{proof}

\begin{satz}[Punktweises Kriterium für Unabhängigkeit]
  \label{cha02/stochainf:punkt}
  Es sei $\famii{X_{i}}$ eine endliche Familie von ZV.
  Dann sind äquivalent:
  \begin{enumerate}
  \item $\famii{X_{i}}$ ist unabhängig.
  \item $\forall u \in \RR^{I} \holds \prob{\bigwedge_{i \in I} \p{X_{i} = u_{i}}}
    = \prod_{i\in I} \prob{X_{i} = u_{i}}$
  \end{enumerate}
\end{satz}

Der Beweis ist formal etwas aufwändig, enthält aber keine schwierigen Argumente.
Beweisen Sie das Resultat zunächst als Übung für $I = \set{1,2}$
und dann für $I = \set{1,2,3}$.

\begin{proof}
  \imprefx{1}{2} trivial.
  \par
  \imprefx{2}{1}
  Es sei $U = \famii{U_{i}} \in \cP(\RR)^{I}$.
  Wir zeigen weiter unten per Induktion über $\card{I}$ folgende beiden Aussagen:
  \begin{IEEEeqnarray}{0l}
    \label{stochainf/cha02:punkt:eqn-1}
    \bigwedge_{i \in I} \p{X_{i} \in U_{i}}
    \eviff \dotbigvee_{u \in U} \bigwedge_{i \in I} \p{X_{i}=u_{i}} \\
    \label{stochainf/cha02:punkt:eqn-2}
    \sum_{u \in U} \prod_{i\in I} \prob{X_{i} = u_{i}}
    = \prod_{i\in I} \prob{X_{i} \in U_{i}}
  \end{IEEEeqnarray}
  Damit folgt dann:
  \begin{IEEEeqnarray*}{0l"s}
    \prob{\bigwedge_{i \in I} \p{X_{i} \in U_{i}}}
    = \prob{\dotbigvee_{u \in U} \bigwedge_{i \in I} \p{X_{i}=u_{i}}}
    & nach \eqref{stochainf/cha02:punkt:eqn-1} \\
    = \sum_{u \in U} \prob{\bigwedge_{i \in I} \p{X_{i}=u_{i}}} & Additivität \\
    = \sum_{u \in U} \prod_{i\in I} \prob{X_{i} = u_{i}} & Voraussetzung \\
    = \prod_{i\in I} \prob{X_{i} \in U_{i}}
    & nach \eqref{stochainf/cha02:punkt:eqn-2}
  \end{IEEEeqnarray*}
  Wir kommen zu \eqref{stochainf/cha02:punkt:eqn-1} und \eqref{stochainf/cha02:punkt:eqn-2}.
  Der Induktionsanfang $\card{I} = 1$ folgt jeweils leicht aus der Additivität.
  Es sei also $\card{I} \geq 2$, und wähle irgendein $k \in I$,
  und notiere $I' \df I \setminus \set{k}$.
  \begin{description}
  \item[zu \eqref{stochainf/cha02:punkt:eqn-1}:]
    Es gilt:
    \begin{IEEEeqnarray*}{0l"s}
      \bigwedge_{i \in I} \p{X_{i} \in U_{i}}
      \eviff \p{X_{k} \in U_{k}} \land \bigwedge_{i \in I'} \p{X_{i} \in U_{i}} \\
      \eviff \p[big]{\dotbigvee_{u_{k} \in U_{k}} \p{X_{k} = u_{k}}}
      \land \bigwedge_{i \in I'} \p{X_{i} \in U_{i}} \\
      \eviff \p[big]{\dotbigvee_{u_{k} \in U_{k}} \p{X_{k} = u_{k}}}
      \land \p[big]{\dotbigvee_{\famiiprime{u_{i}} \in \famiiprime{U_{i}}}
        \bigwedge_{i \in I'} \p{X_{i}=u_{i}}} & Induktion \\
      \eviff \dotbigvee_{u_{k} \in U_{k}}
      \dotbigvee_{\famiiprime{u_{i}} \in \famiiprime{U_{i}}}
      \p{\p{X_{k} = u_{k}} \land \bigwedge_{i \in I'} \p{X_{i}=u_{i}}}
      & Logik\-/Distributivgesetz \\
      \eviff \dotbigvee_{u \in U} \bigwedge_{i \in I} \p{X_{i}=u_{i}}
    \end{IEEEeqnarray*}
  \item[zu \eqref{stochainf/cha02:punkt:eqn-2}:]
    Es gilt:\footnote{%
      Der hier gegebene Beweis ist für uns nur nachzuvollziehen,
      wenn die Träger der ZV endlich sind.
      Um den allgemeinen Fall zu verstehen,
      benötigt man den \enquote{Großen Umordnungssatz (GU)} für Reihen~\cite[Abschnitt~6.3]{Koenigsberger04Ana1}.
      Dieser ist nicht kompliziert und wäre für uns ohne Weiteres zu verstehen;
      unsere Beweise würden sich nicht so sehr verändern.
      Wir verzichten trotzdem im aktuellen Kapitel darauf, um den Fokus auf anderen Dingen zu behalten
      (in \autoref{stochainf/cha04} behandeln wir aber den GU).
      Bei uns werden die Träger meistens endlich sein,
      also decken die Beweise für endlichen Träger die für uns hauptsächlich relevanten Fälle bereits ab.}
    \begin{IEEEeqnarray*}{0l"s+x*}
      \sum_{u \in U} \prod_{i\in I} \prob{X_{i} = u_{i}}
      = \sum_{u_{k} \in U_{k}} \sum_{\famiiprime{u_{i}} \in \famiiprime{U_{i}}} \prod_{i\in I} \prob{X_{i} = u_{i}} \\
      = \sum_{u_{k} \in U_{k}} \prob{X_{k} = u_{k}} \sum_{\famiiprime{u_{i}} \in \famiiprime{U_{i}}}
      \prod_{i\in I'} \prob{X_{i} = u_{i}} \\
      = \prob{X_{k} = U_{k}} \sum_{\famiiprime{u_{i}} \in \famiiprime{U_{i}}}
      \prod_{i\in I'} \prob{X_{i} = u_{i}} & Additivität \\
      = \prob{X_{k} = U_{k}} \prod_{i\in I'} \prob{X_{i} \in U_{i}} & Induktion \\
      = \prod_{i\in I} \prob{X_{i} \in U_{i}} && \qedhere
    \end{IEEEeqnarray*}
  \end{description}
\end{proof}

\begin{satz}[Uniforme Verteilung auf kartesischem Produkt]
  Es sei ${\famii{A_{i}} \in \cP(\RR)^{I}}$
  eine endliche Familie nicht\-/leerer, endlicher Mengen reeller Zahlen.
  Es sei $\famii{X_{i}}$ eine Familie von ZV.
  Dann sind äquivalent:
  \begin{enumerate}
  \item $\famii{X_{i}} \distr \Unif(\famii{A_{i}})$,
    d.h. $\famii{X_{i}}$ ist uniform verteilt
    auf dem kartesischen Produkt der Familie $\famii{A_{i}}$.
  \item $\famii{X_{i}}$ ist unabhängig, und $X_{i} \distr \Unif(A_{i})$ für alle $i \in I$.
  \end{enumerate}
\end{satz}

\begin{proof}
  Notiere $N \df \prod_{i \in I} \card{A_{i}}$, dann gilt $\card{\famii{A_{i}}} = N$.
  \par
  \imprefx{1}{2}
  Für alle $k \in I$ gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall \xi \in A_{k} \holds
    \prob{X_{k} = \xi}
    = \frac{\card{\set{u \in \famii{A_{i}} \suchthat u_{k} = \xi}}}{N}
    = \frac{\prod_{i \in I \setminus \set{k}} \card{A_{i}}}{N} = \frac{1}{\card{A_{k}}}
  \end{IEEEeqnarray*}
  Also ist $X_{k} \distr \Unif(A_{k})$ für alle $k \in I$.
  Es sei nun $u \in \famii{A_{i}}$.
  Dann gilt, mit dem eben Gezeigten:
  \begin{IEEEeqnarray*}{0l}
    \prod_{i \in I} \prob{X_{i} = u_{i}}
    = \prod_{i\in I} \frac{1}{\card{A_{i}}} = \frac{1}{N} = \prob{X=u}
  \end{IEEEeqnarray*}
  Also ist $\famii{X_{i}}$ unabhängig.
  \par\imprefx{2}{1}
  Für alle $u \in \famii{A_{i}}$ gilt:
  \begin{IEEEeqnarray*}{0l"s+x*}
    \prob{X=u} = \prod_{i \in I} \prob{X_{i} = u_{i}} & Unabhängigkeit \\
    = \prod_{i \in I} \frac{1}{\card{A_{i}}} & $\forall i \in I \holds X_{i} \distr \Unif(A_{i})$ \\
    = \frac{1}{N} && \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\begin{para}[Warnung]
  \autoref{stochainf/cha02:example-unif} zeigt,
  dass die Unabhängigkeit nicht zu gelten braucht,
  wenn die Menge kein kartesisches Produkt ist, sondern nur eine Teilmenge eines solchen.
  Zur Übung rechne man dies in dem Beispiel nach.
\end{para}

\begin{satz}[Blocklemma für ZV]
  \label{stochainf/cha02:blocklemma}
  Es sei $\famii{X_{i}}$ eine unabhängige Familie von ZV.
  Es sei $\famij{M_{j}}$ eine paarweise disjunkte Familie von Teilmengen von $I$,
  und für alle $j \in J$ definiere $\tiX_{j} \df \fami{X_{i}}{i}{M_{j}}$.
  Für alle $j \in J$ sei ferner $\ffn{f_{j}}{D_{j}}{\RR}$
  mit ${\img(\tiX_{j}) \subseteq D_{j} \subseteq \RR^{M_{j}}}$.
  \par
  Dann ist $\famij{f_{j} \circ \tiX_{j}}$ unabhängig.
\end{satz}

Der Beweis ist ähnlich dem von \autoref{cha02/stochainf:punkt}.
Auch hier ist empfohlen, zunächst einige einfache Spezialfälle zu behandlen,
zum Beispiel $J = \set{1,2}$.

\begin{proof}
  Es sei $\famij{v_{j}} \in \RR^{J}$, und definiere $A_{j} \df \fnpre{f_{j}}(v_{j})$ für alle $j \in J$.
  Es gilt:
  \begin{IEEEeqnarray*}{0l"s}
    \prob{\bigwedge_{j \in J} \p{f_{j} \circ \tiX_{j} = v_{j}}}
    = \prob{\bigwedge_{j \in J} \p{\tiX_{j} \in A_{j}}} \\
    = \prob{\bigwedge_{j \in J} \dotbigvee_{\xi \in A_{j}} \bigwedge_{i \in M_{j}} \p{X_{i} = \xi_{i}}} \\
    = \prob{\dotbigvee_{\famij{\idx{\xi}{j}} \in \famij{A_{j}}}
      \bigwedge_{j \in J} \bigwedge_{i \in M_{j}} \p{X_{i} = \idx{\xi}{j}_{i}}} \\
    = \sum_{\famij{\idx{\xi}{j}} \in \famij{A_{j}}}
    \prob{\bigwedge_{j \in J} \bigwedge_{i \in M_{j}} \p{X_{i} = \idx{\xi}{j}_{i}}}
    & Additivität \\
    = \sum_{\famij{\idx{\xi}{j}} \in \famij{A_{j}}}
    \prod_{j \in J} \prod_{i \in M_{j}} \prob{X_{i} = \idx{\xi}{j}_{i}}
    & $\famii{X_{i}}$ unabh.; $\famij{M_{j}}$ pw. disj.
  \end{IEEEeqnarray*}
  Für den Rest führe Induktion über $\card{J}$.
  Der Induktionsanfang $\card{J}=1$ ist klar, also sei $\card{J} \geq 2$.
  Wähle irgendein $k \in J$ und definiere $J' \df J \setminus \set{k}$.
  Dann gilt, obige Gleichungskette fortsetzend:\footnote{%
    Wieder ist bei nicht\-/endlichen Trägern der GU zu verwenden.
    Bei endlichen Trägern kann der Beweis hingegen so wie hier gegeben nachvollzogen werden.}
  \begin{IEEEeqnarray*}{0l"s+x*}
    \hdots =
    \sum_{\idx{\xi}{k} \in A_{k}} \; \sum_{\famijprime{\idx{\xi}{j}} \in \famijprime{A_{j}}} \;
    \prod_{j \in J} \prod_{i \in M_{j}} \prob{X_{i} = \idx{\xi}{j}_{i}} \\
    = \sum_{\idx{\xi}{k} \in A_{k}}
    \prod_{i \in M_{k}} \prob{X_{i} = \idx{\xi}{k}_{i}}
    \sum_{\famijprime{\idx{\xi}{j}} \in \famijprime{A_{j}}}
    \prod_{j \in J'} \prod_{i \in M_{j}} \prob{X_{i} = \idx{\xi}{j}_{i}} \\
    = \prod_{j \in J'} \prob{f_{j} \circ \tiX_{j} = v_{j}}
    \sum_{\idx{\xi}{k} \in A_{k}}
    \prod_{i \in M_{k}} \prob{X_{i} = \idx{\xi}{k}_{i}}
    & Induktion \\
    = \prod_{j \in J'} \prob{f_{j} \circ \tiX_{j} = v_{j}}
    \sum_{\idx{\xi}{k} \in A_{k}}
    \prob{\bigwedge_{i \in M_{k}} \p{X_{i} = \idx{\xi}{k}_{i}}}
    & Unabh. \\
    = \p{\prod_{j \in J'} \prob{f_{j} \circ \tiX_{j} = v_{j}}} \prob{\tiX_{k} \in A_{k}}
    & Additivität \\
    = \prod_{j \in J} \prob{f_{j} \circ \tiX_{j} = v_{j}} && \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\begin{example}[Blocklemma für ZV]
  \label{stochainf/cha02:blocklemma-ex}
  Es sei $(X,Y,Z)$ unabhängig.
  Dann ist auch $(Y, X^{2}e^{X+Z})$ unabhängig.
\end{example}

\begin{corollary}[Blocklemma für Events]
  Es seien $\famii{X_{i}}$ und $\famij{M_{j}}$ und $\tiX_{j}$ mit $j \in J$ wie oben.
  Es sei $\famij{\vphi_{j}}$ eine Familie von Events so,
  dass in $\vphi_{j}$ als ZV nur die aus $\tiX_{j}$ oder Kombinationen dieser vorkommen.
  \par
  Dann ist $\famij{\vphi_{j}}$ unabhängig.
\end{corollary}

\begin{proof}
  Für alle $j \in J$ finde $A_{j} \subseteq \RR^{M_{j}}$ so,
  dass $\vphi_{j} \eviff \tiX_{j} \in A_{j}$ gilt.
  Definiere:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{f_{j}}{\img(\tiX_{j})}{\RR}{\xi \mapsto
      \begin{cases}
        1 & \xi \in A_{j} \\
        0 & \text{sonst}
      \end{cases}}
  \end{IEEEeqnarray*}
  Wende nun das Blocklemma für ZV an.
\end{proof}

\begin{example}[Blocklemma für Events]
  Es sei $(X,Y,Z)$ unabhängig.
  Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{Y > 0 \land X^{2}e^{X+Z} \leq 1} = \prob{Y > 0} \ccdot \prob{X^{2}e^{X+Z} \leq 1}
  \end{IEEEeqnarray*}
\end{example}

\begin{para}[Beobachtung zur Verteilung]
  Wir erinnern an die Verteilung einer Familie mehrerer ZV,
  was wir zur Verdeutlichung ja auch deren gemeinsame Verteilung nennen.
  Ist diese Familie unabhängig, so liegt deren gemeinsame Verteilung schon fest
  durch die Verteilung der einzelnen ZV.
  Das braucht bei nicht\-/unabhängigen Familien nicht so zu sein.
  Wohl aber geht stets die andere Richtung:
  Per Marginalisierung kommt man von der gemeinsamen Verteilung einer Familie
  auf die gemeinsame Verteilung jeder Teilfamilie
  und insbesondere auf die Verteilung jeder einzelnen ZV in der Familie.
\end{para}

\section{Existenzsätze}

Wir erinnern zunächst an den bereits bekannten Existenzsatz \autoref{stochainf/cha02:exists-pmf}.
Dieser sichert uns zu, dass zu gegebener PMF $\rho$ eine Familie von ZV existiert,
deren PMF genau $\rho$ ist.
Der nächste Satz erklärt, dass man zu einer Familie von eindimensionalen PMFs
immer eine entsprechende unabhängige Familie von ZV findet.

\begin{satz}[Existenz unabhängiger ZV]
  \label{stochainf/cha02:exists-unab}
  Es sei $I$ eine endliche Indexmenge und $\famii{\rho_{i}}$ eine Familie von PMFs,
  der Form $\ffn{\rho_{i}}{\RR}{\intcc{0}{1}}$ für alle $i \in I$.
  Dann gibt es einen diskreten W'raum und darüber eine unabhängige Familie $\famii{X_{i}}$ von ZV so,
  dass $\rho_{X_{i}} = \rho_{i}$ für alle $i \in I$ gilt.
\end{satz}

\begin{proof}
  Man prüft leicht nach (Übung), dass Folgendes eine PMF ist:
  \begin{IEEEeqnarray*}{0l}
    \fnx{\RR^{I}}{\intcc{0}{1}}{\famii{u_{i}} \mapsto
      \prod_{i \in I} \rho_{i}(u_{i})}
  \end{IEEEeqnarray*}
  Die Behauptung folgt nun aus dem bekannten Existenzsatz (\autoref{stochainf/cha02:exists-pmf}).
\end{proof}

\begin{para}[Unabhängig und identisch verteilt]
  Oft hat man es mit unabhängigen Familien zu tun,
  wo jede einzelne ZV dieselbe Verteilung hat.
  Dafür gibt es die weit verbreitete Kurzschreibweise \enquote{\iid},
  was steht für \enquote{independent and identically distributed}.
  Es sei $\cD$ eine Verteilung für eine einzelne ZV,
  zum Beispiel $\cD = \Unif(M)$ mit $M \subseteq \RR$
  oder $\cD = \Ber(p)$ mit $p \in \intcc{0}{1}$.
  Wir schreiben $X = \famii{X_{i}} \distr \cD$ \iid für den Sachverhalt,
  dass $X$ unabhängig ist und $X_{i} \distr \cD$ für alle $i \in I$ gilt.
\end{para}

\begin{example}[Bernoulli-ZVs \iid]
  Es sei $(\eli{X}{n}) \distr \Ber(\frac{1}{4})$ \iid mit $n \geq 9$.
  Dann gilt zum Beispiel:
  \begin{IEEEeqnarray*}{0l}
    \prob{\bigwedge_{i=1}^{3} \p{X_{i}=0} \land \bigwedge_{i=5}^{9} \p{X_{i}=1}}
    = \p{\frac{3}{4}}^{3} \p{\frac{1}{4}}^{5} = \frac{27}{65536}
  \end{IEEEeqnarray*}
\end{example}

In der Praxis ergeben sich aus dem Experiment oft Vorgaben für gewisse konditionierte W'keiten.
In solchen Fällen kann folgender Satz bei der Modellierung helfen.
Die speziellen Indexmengen verwenden wir in dem Satz aus notationellen Gründen;
natürlich lässt das Resultat auf jede andere Indexmenge übertragen.

\begin{satz}[Existenz bei Konditionierungsvorgaben]
  \label{stochainf/cha02:exists-cond}
  Es sei $\ffn{\rho_{0}}{\RR}{\intcc{0}{1}}$ eine PMF und $n \in \NNone$.
  Für alle $u \in \supp(\rho_{0})$ sei $\ffn{\rho_{u}}{\RR^{n}}{\intcc{0}{1}}$ eine PMF.
  Dann gibt es einen diskreten W'raum und darüber eine Familie $(X,\eli{Y}{n})$ so, dass gilt:
  \begin{enumerate}
  \item\label{stochainf/cha02:exists-cond:1}
    $\rho_{X} = \rho_{0}$
  \item\label{stochainf/cha02:exists-cond:2}
    Für alle $u \in \supp(\rho_{0})$ ist konditioniert auf $X=u$
    die Funktion $\rho_{u}$ die PMF von $Y=(\eli{Y}{n})$,
    d.h. $\prob{Y=v \cond X=u} = \rho_{u}(v)$ für alle $v \in \RR^{n}$.
  \end{enumerate}
  Ferner hat jede Familie $(X,\eli{Y}{n})$ mit den beiden obigen Eigenschaften dieselbe Verteilung,
  nämlich mit folgender PMF:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{\rho}{\RR^{n+1}}{\intcc{0}{1}}{(u,\eli{v}{n}) \mapsto \rho_{0}(u) \rho_{u}(v)}
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}
  Definiere:
  \begin{IEEEeqnarray*}{0l}
    S \df \set{(u,\eli{v}{n}) \in \RR^{n+1} \suchthat u \in \supp(\rho_{0}) \land v \in \supp(\rho_{u})}
  \end{IEEEeqnarray*}
  Dann ist $S$ abzählbar.
  Es gilt:\footnote{%
    Bei nicht\-/endlichen Trägern ist der GU zu verwenden bei dieser Rechnung.}
  \begin{IEEEeqnarray*}{0l}
    \sum_{(u,\eli{v}{n}) \in S} \rho(u,\eli{v}{n})
    = \sum_{\substack{u \in \supp(\rho_{0}) \\ v \in \supp(\rho_{u})}} \rho_{0}(u) \rho_{u}(v) \\
    \eqin = \sum_{u \in \supp(\rho_{0})} \rho_{0}(u)
    \underbrace{\sum_{v \in \supp(\rho_{u})} \rho_{u}(v)}_{=1}
    = \sum_{u \in \supp(\rho_{0})} \rho_{0}(u)
    = 1
  \end{IEEEeqnarray*}
  Also ist $\rho$ eine PMF, und wir sehen auch $\supp(\rho) \subseteq S$.
  Wir wenden \autoref{stochainf/cha02:exists-pmf} an
  und erhalten einen diskreten W'raum und darüber $(X,\eli{Y}{n})$ mit PMF~$\rho$.
  Wir überprüfen, ob diese ZV die gewünschten Eigenschaften haben.
  Es sei $u \in \supp(\rho_{0})$, und notiere $Y = (\eli{Y}{n})$.
  Dann gilt $\supp(\rho_{u}) \subseteq \supp(Y)$,
  und weiter:
  \begin{IEEEeqnarray*}{0l}
    \prob{X=u} = \prob{X=u \land Y \in \supp(Y)}
    = \sum_{v \in \supp(Y)} \prob{X=u \land Y = v} \\
    = \sum_{v \in \supp(Y)} \rho_{0}(u) \rho_{u}(v)
    = \rho_{0}(u) \sum_{v \in \supp(Y)} \rho_{u}(v)
    = \rho_{0}(u)
  \end{IEEEeqnarray*}
  Für alle $v \in \RR^{n}$ gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{Y=v \cond X=u}
    = \frac{1}{\rho_{0}(u)} \prob{Y=v \land X=u} \\
    = \frac{1}{\rho_{0}(u)} \rho(u,\eli{v}{n})
    = \frac{1}{\rho_{0}(u)} \rho_{0}(u) \rho_{u}(v)
    = \rho_{u}(v)
  \end{IEEEeqnarray*}
  Damit ist dieser Teil des Beweises abgeschlossen.
  \par
  Nun sei $(X,\eli{Y}{n})$ mit Eigenschaften~\ref{stochainf/cha02:exists-cond:1}
  und~\ref{stochainf/cha02:exists-cond:2}.
  Für alle $u \in \supp(\rho_{0})$ und alle $v \in \RR^{n}$ gilt:
  \begin{IEEEeqnarray*}{0l+x*}
    \prob{X=u \land Y=v}
    = \prob{Y=v \cond X=u} \ccdot \prob{X=u} \\
    \eqin = \rho_{u}(v) \rho_{0}(u)
    = \rho(u,\eli{v}{n})
    & \qedhere
  \end{IEEEeqnarray*}
\end{proof}

Anwendungen für alle diese Sätze werden wir in den nächsten Kapiteln kennenlernen.
