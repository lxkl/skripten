\begin{para}
  Manchmal ist es praktischer, bei obiger Schreibweise keine Grundmenge $M$ anzugeben,
  sondern \enquote{alle möglichen} Objekte einzusammeln, welche die Aussage erfüllen.
  Die Schreibweise ist dann $\set{x \suchthat \hdots}$.
  Das ist grundsätzlich nicht ohne Gefahr,
  weil es in bestimmten Fällen zu Widersprüchen führen kann~\cite{WikipediaRussellscheAntinomie}.
  Das wird uns aber nicht betreffen, und wir können diese Schreibweise getrost verwenden.
\end{para}

\begin{para}
  Es sei $\cM$ eine Menge von Mengen.
  Dann nennen wir $\cM$ \term{paarweise disjunkt},
  wenn $A \cap B = \emptyset$ für alle $A, B \in \cM$ mit $A \neq B$ gilt.
  Wenn $\cM$ paarweise disjunkt ist, schreiben wir statt $\bigcup \cM$
  zur Verdeutlichung auch $\dotbigcup \cM$ und sprechen von einer \term{disjunkten Vereinigung}.
\end{para}

  \par
  D\lstinline|D|
  M\lstinline|M|
  m\lstinline|m|
  0\lstinline|0|
  \par
  D\textsf{D}
  M\textsf{M}
  m\textsf{m}
  0\textsf{0}
  \par

    Wir wollen auch Ausdrücke wie $2 \leq x \leq 5$ oder $x \in M$ oder $N \subseteq M$ als Aussagen bezeichnen
  (das haben wir zum Beispiel in \autoref{grund/meng:pred-set} getan),
  obwohl ihr Wahrheitswert davon abhängt, für welche Objekte die darin vorkommenden Symbole stehen;
  genauer kann man hier von \term{Prädikaten} sprechen anstatt von Aussagen.

  \begin{para}
  \label{grund/logik:quant:1}
  Die Begriffe \enquote{für alle} (in Zeichen:~$\forall$)
  und \enquote{es gibt} oder \enquote{es existiert} (in Zeichen:~$\exists$)
  werden \term{Quantoren} genannt.
  Sie bieten eine Möglichkeit, aus Prädikaten solche Aussagen zu formen,
  die dann tatsächlich einen Wahrheitswert haben.
  So ist etwa $\exists x \in \RR \holds 2 \leq x \leq 5$ eine wahre Aussage,
  denn es gibt eine Zahl, die mindestens $2$ und höchstens $5$ ist, zum Beispiel die Zahl~$2$.
  Die Aussage ${\forall x \in \RR \holds 2 \leq x \leq 5}$ ist hingegen falsch,
  denn etwa für $x=7$ ist $2 \leq x \leq 5$ falsch.
\end{para}


\begin{para}
  \label{grund/logik:pred-ex}
  Manchmal möchte man sich mit Symbolen auf Aussagen, insbesondere auf Prädikate, beziehen.
  Dann kann man $\dfiff$ verwenden, was heißt,
  dass der Ausdruck auf der linken Seite fortan äquivalent sein soll zum Ausdruck auf der rechten Seite.
  Zum Beispiel für die in~\eqref{grund/meng:pred-set:1} verwendete Bedingung könnte man vermöge
  $P(x) \dfiff 2 \leq x \leq 5$ die Abkürzung $P$ einführen
  und dann mit $\set{x \in \NN \suchthat P(x)}$ oder auch mit $\set{y \in \NN \suchthat P(y)}$
  dieselbe Menge $\set{2,3,4,5}$ erhalten.
  Man nennt dann auch $P$ ein Prädikat.
  Allgemein heißt $Q$ ein \term{Prädikat} über einer Menge $M$,
  wenn $Q(x)$ eine Aussage im eigentlichen Sinne ist für alle $x \in M$,
\end{para}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{satz}
  \label{grund/fun:satz-umkehr}
  Es seien $R$ und $S$ Funktionen.
  Dann sind äquivalent:
  \begin{enumerate}
  \item\label{grund/fun:satz-umkehr:1} $R$ ist die Umkehrfunktion von $S$.
  \item\label{grund/fun:satz-umkehr:2} $S$ ist die Umkehrfunktion von $R$.
  \item\label{grund/fun:satz-umkehr:3}
    $\img(R) \subseteq \dom(S)$ und $\forall x \in \dom(R) \holds S(R(x)) = x$
  \item\label{grund/fun:satz-umkehr:4}
    $\forall y \in \img(R) \holds R(S(y)) = y$
  \end{enumerate}
  Gilt eine (und damit alle) dieser Aussagen,
  dann sind offenbar $S$ und $R$ jeweils injektiv.
\end{satz}

\begin{proof}
  \impref{grund/fun:satz-umkehr:1}{grund/fun:satz-umkehr:2}
  Es gilt $R^{-1} = (S^{-1})^{-1} = S$, wobei die erste Gleichheit die Voraussetzung ist.
  \par
  \impref{grund/fun:satz-umkehr:2}{grund/fun:satz-umkehr:3}
  Es ergibt sich $\dom(S) = \img(R)$ aus der Definition der Umkehrfunktion.
  Es sei nun $x \in \dom(R)$.
  Dann ist $(x,R(x)) \in R$, also $(R(x),x) \in R^{-1} = S$.
  Also ist $S(R(x)) = x$.
  \par
  \impref{grund/fun:satz-umkehr:3}{grund/fun:satz-umkehr:4}
  Es sei $y \in \img(R)$.
\end{proof}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{satz}
  Es sei $n \in \NN$ ungerade, d.h. es gibt $k \in \NNzero$ so, dass $n = 2k + 1$ gilt.
  Dann ist die Funktion $\fnx{\RR}{\RR}{x \mapsto x^n}$ streng monoton steigend (also injektiv)
  und surjektiv.
\end{satz}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{solution}
  \begin{enumerate}
  \item Wir zeigen beide Inklusionen $A \subseteq B$ und $B \subseteq A$.
    \begin{itemize}
    \item Es sei $(x,y) \in A$.
    \end{itemize}
  \end{enumerate}

  Für alle $(x,y) \in \RR^{2}$ gibt es $4$ Fälle:
  \begin{itemize}
  \item $x,y \geq 0$;
    in diesem Fall ist $xy \geq 0$ nach \autoref{grund/rl:ofield-rules}~\ref{grund/rl:ofield-rules:11}.
  \item $x,y \leq 0$.
    In diesem Fall ist $-x,-y \geq 0$,
    nach \autoref{grund/rl:ofield-rules}~\ref{grund/rl:ofield-rules:11}
    also $xy = (-x) (-y) \geq 0$.
  \item $x > 0$ und $y < 0$.
    In diesem Fall ist $xy < 0$ nach \autoref{grund/rl:ofield-rules}~\ref{grund/rl:ofield-rules:15}.
  \item $x < 0$ und $y > 0$.
    Auch in diesem Fall ist $xy < 0$ mit derselben Begründung.
  \end{itemize}
  An den ersten beiden Fällen sieht man $B \subseteq A$.
\end{solution}

\begin{exercise}
  Definiere:
  \begin{IEEEeqnarray*}{0l}
    A \df \set{(x,y) \in \RR^{2} \suchthat xy \geq 0} \\
    B \df \set{(x,y) \in \RR^{2} \suchthat x,y \geq 0 \lor x,y \leq 0} \\
    C \df \set{(x,y) \in \RR^{2} \suchthat \abs{x+y} = \abs{x} + \abs{y}}
  \end{IEEEeqnarray*}
  Zeigen Sie:
  \begin{enumerate}
  \item $A=B$
  \item $B=C$
  \end{enumerate}
\end{exercise}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{para}
  Meist unterscheiden wir sprachlich nicht zwischen einem Symbol und dem Objekt,
  für das es steht.
  Wir reden also vom Symbol $x$ ebenso wie von zum Beispiel der Zahl $x$,
  wenn das Symbol $x$ für eine Zahl steht.
  Aus dem Kontext geht hervor, was gemeint ist.
  (Möchte man es explizit machen, kann man ein Symbol, das als Symbol gemeint ist,
  in Anführungszeichen setzen, zum Beispiel~\enquote{$x$}.
  Dies ist auch im übrigen Sprachgebrauch üblich, zum Beispiel:
  \enquote{Auto} hat vier Buchstaben, aber ein Auto hat vier Räder.)
\end{para}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{para}
  \label{grund/meng:menge-von-mengen}
  Wir führen weitere Schreibweisen ein, die manchmal praktisch sind.
  Es sei $\cM$ eine Menge von Mengen.
  Dann definieren wir die \term{Vereinigung} \bzw den \term{Schnitt} der Mengen in $\cM$ als:
  \begin{IEEEeqnarray*}{0l+l}
    \bigcup \cM \df \set{x \suchthat \exists A \in \cM \holds x \in A} \\
    \bigcap \cM \df \set{x \suchthat \forall A \in \cM \holds x \in A}
    & \text{wenn $\cM \neq \emptyset$}
  \end{IEEEeqnarray*}
  Offenbar ist $\bigcup \emptyset = \emptyset$.
  Beim Schnitt ist der Fall $\cM = \emptyset$ problematisch.
  Eine direkte Fortsetzung obiger Definition liefert $\bigcap \emptyset$
  als die \enquote{Menge aller mathematischer Objekte},
  was aber zu Schwierigkeiten führt;
  Interessierte lesen dazu \autoref{grund/meng:russel} am Ende dieses Abschnitts.
  Ansonsten genügt es, sich zu merken, dass $\bigcap \emptyset$ nicht definiert ist.
  \par
  Wir erlauben auch Schreibweisen, die näher an den bekannten Indexschreibweisen sind:
  \begin{IEEEeqnarray*}{0l+l}
    \bigcup \cM = \bigcup_{A \in \cM} A &
    \bigcap \cM = \bigcap_{A \in \cM} A \quad \text{wenn $\cM \neq \emptyset$}
  \end{IEEEeqnarray*}
\end{para}

\begin{example}
  Es gilt:
  \begin{IEEEeqnarray*}{0l}
    \bigcup \set{\set{1,2,7}, \setn{4}, \set{-5,0,1,2}} = \set{-5,0,1,2,3,4,7} \\*
    \bigcup \set{\set{1,2,7}, \set{9,15}, \set{-5,0}} = \set{-5,0,1,2,7,9,15} \\
    \bigcap \set{\set{1,2,7}, \setn{4}, \set{-5,0,1,2}} = \set{1,2} \\*
    \bigcap \set{\set{1,2,7}, \set{9,15}, \set{-5,0}} = \emptyset
  \end{IEEEeqnarray*}
\end{example}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  \begin{IEEEeqnarray*}{0l+l+l}
    \pow(b,\veps) \df \exp\p{\veps \cdot \ln(b)} & b \in \RRpos, \quad \veps \in \RR \\
    \pow(0,a) \df 0 & a > 0
  \end{IEEEeqnarray*}
  \begin{IEEEeqnarray*}{0l+l+l}
    \pow(b,a) \df \exp\p{a \cdot \ln(b)} & b > 0, \quad a \in \RR \\
    \pow(0,a) \df 0 & a > 0
  \end{IEEEeqnarray*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  Wir lesen $b^{a}$ als \enquote{$b$ hoch $a$}
  oder sagen, dass $b$ mit $a$ \term{potenziert} wird.
  Wir nennen $b$ die \term{Basis} und $a$ den \term{Exponenten}.
  \par
  Wir erinnern an \autoref{grund/rl:xn}.
  Dort haben wir die Notation $b^{a}$ für den Fall $a \in \ZZ$ bereits mit einer Bedeutung versehen
  (wenn $a < 0$, dann muss $b \neq 0$ sein).
  Diese Bedeutung meinten wir insbesondere in \autoref{grund/rlfun:exp} \autoref{grund/rlfun:exp:n}.
  Es wäre unzulässig, die Notation $b^{a}$ für zwei verschiedene Bedeutungen zu verwenden.
  Die beiden Bedeutungen stimmen aber überein, wie man leicht sieht.
  Hier schauen wir uns das für $b>0$ und $n \in \NN$ an
  unter Verwendung von \autoref{grund/rlfun:exp} \autoref{grund/rlfun:exp:n}:
  \begin{IEEEeqnarray*}{0l}
    b^{n} \stackrel{\text{neu}}{=} \exp\p{n \cdot \ln(b)}
    = \p{\exp\p{\ln(b)}}^{n}
  \end{IEEEeqnarray*}

  \begin{para}[Allgemeine Potenz]
  Es seien $a, b \in \RR$.
  Definiere:
  \begin{IEEEeqnarray*}{0l+l}
    b^{a} \df \exp\p{a \cdot \ln(b)} & (b > 0) \\
    0^{a} \df 0 & (a > 0)
  \end{IEEEeqnarray*}
  Wir lesen $b^{a}$ als \enquote{$b$ hoch $a$}
  oder sagen, dass $b$ mit $a$ \term{potenziert} wird.
  Wir nennen $b$ die \term{Basis} und $a$ den \term{Exponenten}.
  \par
  Wir erinnern an \autoref{grund/rl:xn}.
  Dort haben wir die Notation $b^{a}$ für den Fall $a \in \ZZ$ bereits mit einer Bedeutung versehen
  (wenn $a < 0$, dann muss $b \neq 0$ sein).
  Diese Bedeutung meinten wir insbesondere in \autoref{grund/rlfun:exp} \autoref{grund/rlfun:exp:n}.
  Es wäre unzulässig, die Notation $b^{a}$ für zwei verschiedene Bedeutungen zu verwenden.
  Die beiden Bedeutungen stimmen aber überein, wie man leicht sieht.
  Hier schauen wir uns das für $b>0$ und $n \in \NN$ an
  unter Verwendung von \autoref{grund/rlfun:exp} \autoref{grund/rlfun:exp:n}:
  \begin{IEEEeqnarray*}{0l}
    b^{n} \stackrel{\text{neu}}{=} \exp\p{n \cdot \ln(b)}
    = \p{\exp\p{\ln(b)}}^{n}
  \end{IEEEeqnarray*}
\end{para}

  Wenn $\pow(b,\veps)$ und $b^{\veps}$ beide definiert sind,
  so stimmen sie überein.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  Es sind also $\sqrt{a}$ und $-\sqrt{a}$ die beiden einzigen reellen Zahlen,
  die quadriert~$a$ ergeben.
  Oder, etwas salopp:
  $\sqrt{a}$ und $-\sqrt{a}$ sind die einzigen \term{Lösungen} der Gleichung $x^{2} = a$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{para}[Gleichheit von Mengen]
  Zwei Mengen $M$ und $N$ nennen wir \term{gleich}, in Zeichen $M = N$,
  wenn $M \subseteq N$ und $N \subseteq M$ gilt.
  Offenbar gilt $M = N$ genau dann, wenn $N = M$ gilt.
  Wir schreiben $M \neq N$ um auszusagen, dass $M$ und $N$ nicht gleich sind,
  wozu wir auch \term{ungleich} sagen.
\end{para}

  Gleichheit von zwei Objekten verwenden wir in dem Sinne,
  dass wir diese ihre Eigenschaften betreffend nicht voneinander unterscheiden können;
  auch wenn sie vielleicht unterschiedlich dargestellt sind.
