\chapter{Logik}
\label{grund/logik}

\section{Grundlagen}

\begin{para}[Aussage, Wahrheitswert]
  Ein sprachliches Gebilde,
  von dem es sinnvoll ist zu sagen, es sei \term{wahr} oder \term{falsch},
  nennen wir eine \term{Aussage}.
  Diese Begriffsbildung findet sich bei Aristoteles (384--322 v. Chr),
  und sie ist grundlegend für die Mathematik.
  Ist eine Aussage wahr, so sagen wir auch,
  dass sie \term{gilt} oder dass sie den \term{Wahrheitswert} $\true$ hat;
  ist eine Aussage falsch, so sagen wir auch,
  dass sie \term{nicht gilt} oder dass sie den \term{Wahrheitswert} $\false$ hat.
  Wir haben Aussagen im vorigen Kapitel bereits ganz natürlich gebraucht.
  Es ist zum Beispiel ${2 \leq 3}$ eine Aussage (sie ist wahr),
  und $5 \in \NN$ ist eine Aussage (sie ist auch wahr),
  und ${2 \leq 1}$ ist eine Aussage (sie ist falsch).
\end{para}

\begin{para}[Implikation]
  Für den Rest dieses Kapitels mögen $A$ und $B$ für irgendwelche Aussagen stehen.
  Besonders wichtig ist ein gutes Verständnis der \term{Implikation} (in Zeichen:~$\implies$).
  Wir schreiben $A \implies B$ für den Sachverhalt,
  dass \emphasis{unter der Annahme dass $A$ gilt}, auch $B$ gilt,
  oder, jeweils mit anderen Worten:
  \begin{itemize}
  \item Wenn $A$ gilt, dann gilt $B$.
  \item Aus $A$ folgt $B$.
  \item $A$ impliziert $B$.
  \end{itemize}
  Es ist $A \implies B$ wieder eine Aussage.
  Man nennt hier die Aussage~$A$ die \term{Prämisse} oder \term{Voraussetzung}
  und die Aussage $B$ die \term{Konklusion} oder \term{Folgerung}.
  Das Zeichen $\impliedby$ wird auch gebraucht:
  Wir meinen mit $B \impliedby A$ dasselbe wie mit $A \implies B$.
\end{para}

\begin{para}[Diskussion zur Implikation]
  Wann ist die Aussage $A \implies B$ wahr und wann nicht?
  Wenn $A$ wahr ist, dann kann $A \implies B$ nur wahr sein, wenn auch $B$ wahr ist.
  Wenn $A$ falsch ist, so ist $A \implies B$ wahr unabhängig von~$B$,
  denn die Implikation sagt nur, dass \emphasis{wenn} $A$ wahr ist, auch $B$ wahr sein muss.
  Wenn $A$ falsch ist, so darf $B$ alles sein.
  Das ist nichts Neues für uns, sondern wir fassen hier lediglich präzise zusammen,
  was wir ohnehin schon immer unter \enquote{Wenn \dots\ dann \dots\ } verstanden haben,
  jedenfalls wenn es in einem ernsthaften Kontext verwendet wurde.
  Zum Beispiel könnte in einem Versicherungsvertrag sinngemäß stehen:
  \enquote{Wenn Sie Ihr Smartphone verlieren, zahlen wir Ihnen \EUR{500}.}
  Es gibt nur \emphasis{eine} Möglichkeit, dass diese Zusage verletzt wird,
  nämlich wenn der Kunde das Smartphone verliert (Prämisse wahr),
  aber die Versicherung keine \EUR{500} auszahlt (Konklusion falsch).
  In allen anderen Fällen, auch wenn die Versicherung dem Kunden einfach so \EUR{500} schenkt,
  verstehen wir die Zusage als erfüllt.
\end{para}

\begin{para}[Wahrheitstafel für Implikation, Negation, Junktoren]
  Wir halten die Bedeutung der Implikation und auch die Bedeutung der
  \term{Negation} $\neg$ (\enquote{nicht})
  und der \sog \term{Junktoren} $\land$ (\enquote{und}) und $\lor$ (\enquote{oder})
  in der folgenden Wahrheitstafel fest.
  In dieser Wahrheitstafel durchlaufen wir für $A$ und $B$
  alle möglichen Kombinationen an Wahrheitswerten.
  Dann geben wir auf dieser Grundlage die Wahrheitswerte
  von $\neg A$, $A \land B$, $A \lor B$ und $A \implies B$ an.
  Die ersten beiden Spalten der Tabelle sind also als Eingabe zu verstehen,
  und die übrigen Spalten folgen aus dieser Eingabe.
  Wenn also zum Beispiel $A$ wahr ist und $B$ falsch ist, dann ist $A \lor B$ wahr (zweite Zeile).
  Für die Spalte $\neg A$ ist die Spalte mit dem Wahrheitswert für $B$ offenbar irrelevant.
  \begin{inlinetable}
    \begin{tabular}{cc@{\hspace{2.0em}}cccc}
      \toprule
      $A$ & $B$ & $\neg A$ & $A \land B$ & $A \lor B$ & $A \implies B$ \\
      \midrule
      \true & \true & \false & \true & \true & \true \\
      \true & \false & \false & \false & \true & \false \\
      \false & \true & \true & \false & \true & \true \\
      \false & \false & \true & \false & \false & \true \\
      \bottomrule
    \end{tabular}
  \end{inlinetable}
  Wie auch schon bei der Implikation gilt: Diese Tabelle enthält für uns nichts Neues,
  sondern fasst nur zusammen, wie wir ohnehin schon \enquote{nicht}, \enquote{und},
  \enquote{oder} und \enquote{Wenn \dots\ dann \dots\ } verstanden haben.
  Um das tiefer einzusehen, versuchen Sie mal,
  diese Tabelle überhaupt zu lesen ohne ein Verständnis von dem,
  was man üblicherweise mit \enquote{und} und \enquote{Wenn \dots\ dann \dots\ } bezeichnet;
  das wird schwierig.
  Diese Tabelle hat also keinen definierenden Charakter,
  sondern fasst nur bereits Bekanntes zusammen.
  Einzige Ausnahme davon ist vielleicht die Konvention,
  dass \enquote{oder}, in Zeichen $\lor$, bei uns im inklusiven Sinne gemeint ist,
  d.h. $A \lor B$ ist auch dann wahr, wenn $A$ und $B$ beide wahr sind;
  das ist in der Umgangssprache nicht immer so klar geregelt.
\end{para}

\begin{para}[Redeweisen zur Implikation]
  Gilt $A \implies B$, so sagen wir auch, dass $A$ \term{hinreichend} für $B$ ist.
  Von $B$ sagen wir, dass es \term{notwendig} für $A$ ist.
  Diese Bezeichnung ist sinnvoll gewählt, denn wenn $B$ nicht gilt, so kann $A$ nicht gelten:
  Würde $A$ gelten, so hätten wir wegen $A \implies B$ sofort die Gültigkeit von~$B$.
\end{para}

\begin{para}[Äquivalenz]
  Gelten sowohl $A \implies B$ als auch $B \implies A$, dann schreiben wir $A \iff B$
  und sagen, dass $A$ und $B$ \term{äquivalent} sind
  oder dass $A$ \term{genau dann} gilt wenn $B$ gilt;
  die Formulierung \enquote{genau dann} haben wir im bisherigen Text auch schon in dieser Bedeutung verwendet.
  Es ist offenbar $A \iff B$ dasselbe wie $B \iff A$.
  Zur Verdeutlichung stellen wir eine Wahrheitstafel für die Äquivalenz auf:
  \begin{inlinetable}
    \begin{tabular}{cc@{\hspace{2.0em}}ccc}
      \toprule
      $A$ & $B$ & $A \implies B$ & $B \implies A$ & $A \iff B$ \\
      \midrule
      \true & \true & \true & \true & \true \\
      \true & \false & \false & \true & \false \\
      \false & \true & \true & \false & \false \\
      \false & \false & \true & \true & \true \\
      \bottomrule
    \end{tabular}
  \end{inlinetable}
  Es gilt $A \iff B$ also in genau den Fällen, wo $A$ und $B$ denselben Wahrheitswert haben.
\end{para}

\section{Einige einfache Aussagen}

\begin{example}[Aussagen mit $\forall$ und $\exists$]
  \label{grund/logik:bsp-1}
  Wir betrachten die folgende Aussage:
  \begin{IEEEeqnarray*}{0l}
    \forall x \in \RR \holds \parens{x > 2 \implies x > 0}
  \end{IEEEeqnarray*}
  Diese können wir auch wie folgt ohne Klammern schreiben, denn es ist vereinbart,
  dass $\implies$ und $\iff$ \term{stärker binden} als $\forall$:
  \begin{IEEEeqnarray*}{0l}
    \forall x \in \RR \holds x > 2 \implies x > 0
  \end{IEEEeqnarray*}
  Wir kennen $\forall$ schon aus \autoref{grund/meng:indexschreibweisen:2},
  daher wissen wir, dass obige Aussage zu lesen ist als:
  Für alle $x \in \RR$ gilt, dass wenn $x > 2$ gilt, auch $x > 0$ gilt.
  Diese Aussage ist wahr, denn wenn eine reelle Zahl größer als $2$ ist,
  dann ist sie (\enquote{erst recht}) größer als~$0$.
  Was passiert, wenn wir die Implikation zu einer Äquivalenz erweitern?
  Die folgende Aussage ist falsch:
  \begin{IEEEeqnarray*}{0l}
    \forall x \in \RR \holds x > 2 \iff x > 0
  \end{IEEEeqnarray*}
  Denn es gibt durchaus reelle Zahlen, die größer als $0$ sind,
  jedoch nicht größer als $2$ sind, nämlich zum Beispiel $x=1$ oder auch $x=2$.
  An diesem Beispiel sieht man, dass $\implies$ tatsächlich etwas anderes bedeutet als~$\iff$.
  \par
  Wie ist es mit folgender Aussage?
  \begin{IEEEeqnarray*}{0l}
    \exists x \in \RR \holds \p{x > 2 \iff x > 0}
  \end{IEEEeqnarray*}
  Zunächst können wir diese wieder ohne Klammern schreiben,
  denn $\implies$ und $\iff$ binden auch stärker  als $\exists$:
  \begin{IEEEeqnarray*}{0l}
    \exists x \in \RR \holds x > 2 \iff x > 0
  \end{IEEEeqnarray*}
  Dann erinnern wir an die Bedeutung von $\exists$,
  die wir in \autoref{grund/meng:indexschreibweisen:1} kennengelernt hatten.
  Es steht oben also:
  Es gibt $x \in \RR$ so, dass $x > 2$ genau dann gilt, wenn $x > 0$ gilt.
  Diese Aussage ist wahr, denn zum Beispiel $x = 0$ ist eine reelle Zahl,
  für die weder $x > 2$ noch $x > 0$ gilt;
  damit ist die behauptete Existenz nachgewiesen.
\end{example}

Einfache logische Zusammenhänge lassen sich oft über Wahrheitstafeln beweisen.
Das schauen wir uns nun an.

\begin{satz}[de Morgan]
  \begin{samepage}
    \label{grund/logik:de-morgan}
    Es gilt:
    \begin{enumerate}
    \item\label{grund/logik:de-morgan:1} $\neg (A \land B) \iff (\neg A \lor \neg B)$
    \item\label{grund/logik:de-morgan:2} $\neg (A \lor B) \iff (\neg A \land \neg B)$
    \end{enumerate}
  \end{samepage}
\end{satz}

\begin{proof}[nur für \ref*{grund/logik:de-morgan:1}]
  Wir geben den Beweis in Form der folgenden Wahrheitstafel:
  \begin{inlinetable}
    \begin{tabular}{cc@{\hspace{2.0em}}ccccc}
      \toprule
      $A$ & $B$ & $A \land B$ & $\neg (A \land B)$ & $\neg A$ & $\neg B$ & $\neg A \lor \neg B$ \\
      \midrule
      \true  & \true  & \true  & \false & \false & \false & \false \\
      \true  & \false & \false & \true  & \false & \true  & \true  \\
      \false & \true  & \false & \true  & \true  & \false & \true  \\
      \false & \false & \false & \true  & \true  & \true  & \true  \\
      \bottomrule
    \end{tabular}
  \end{inlinetable}
  Die zu $\neg (A \land B)$ und $\neg A \lor \neg B$
  gehörigen Spalten haben für alle möglichen Wahrheitswerte von $A$ und $B$ denselben Wahrheitswert.
  Damit sind die beiden Aussagen äquivalent.
\end{proof}

\begin{para}[Bindung]
  Die Klammern auf der jeweils rechten Seite
  in \ref*{grund/logik:de-morgan:1} und~\ref*{grund/logik:de-morgan:2}
  hätten wir auch fortlassen können,
  da vereinbart ist, dass $\lor$ und $\land$ stärker binden als~$\implies$ und $\iff$.
\end{para}

\begin{example}
  Wir untersuchen noch einige einfache Aussagen, sowohl richtige als auch falsche.
  Betrachten wir als erstes:
  \begin{IEEEeqnarray*}{0l}
    A \implies (A \lor B)
  \end{IEEEeqnarray*}
  Diese Aussage ist richtig (egal welche Wahrheitswerte $A$ und $B$ haben).
  Man kann dies mit einer Wahrheitstafel überprüfen oder mit Argumenten.
  Argumentativ sieht der Beweis so aus:
  Wir setzen voraus, dass $A$ gilt.
  Zu zeigen ist, dass $A \lor B$ gilt.
  Das ist erreicht, wenn wir wissen, dass $A$ gilt oder dass $B$ gilt.
  Mit dieser Einsicht sind wir schon fertig, denn wir wissen ja, dass $A$ gilt.
  Ob $B$ auch gilt oder nicht gilt, kann uns hier egal sein.
  Der Fall, dass $A$ falsch ist, braucht auch nicht weiter betrachtet zu werden,
  denn in diesem Falle ist die Implikation trivialerweise wahr.
\end{example}

\begin{example}
  Betrachten wir nun die Umkehrung:
  \begin{IEEEeqnarray*}{0l}
    (A \lor B) \implies A
  \end{IEEEeqnarray*}
  Diese ist nicht immer wahr.
  Denn wenn etwa $A$ falsch ist und $B$ wahr,
  dann gilt zwar $A \lor B$, also die Prämisse der Implikation ist wahr,
  die Konklusion,~$A$, jedoch gilt nicht.
  Kompakt wollen wir so eine Belegung so schreiben: $A \lassign \false$, $B \lassign \true$.
\end{example}

\begin{example}
  Die nächste Aussage, eine Äquivalenz, ist wiederum wahr:
  \begin{IEEEeqnarray*}{0l}
    (A \land B) \land (A \lor B) \iff (A \land B)
  \end{IEEEeqnarray*}
  Wir beweisen das argumentativ, indem wir beide Implikationen getrennt untersuchen.
  Die Implikation $(A \land B) \land (A \lor B) \implies (A \land B)$ ist sofort klar:
  Wenn zwei Aussagen jeweils gelten, dann gilt insbesondere eine davon.
  Nun zur anderen Implikation, d.h. wir müssen $(A \land B) \implies  (A \land B) \land (A \lor B)$ zeigen.
  Setzen wir also $A \land B$ voraus.
  Der erste Teil der Konklusion, $A \land B$, ist damit klar, denn er ist identisch zur Prämisse.
  Wir müssen noch $A \lor B$ zeigen.
  Das ist bald getan, denn wir hatten $A \land B$ vorausgesetzt,
  also gilt insbesondere $A$ und damit auch~$A \lor B$.
\end{example}

\section{Aussageformen, Prädikate und Quantoren}

\begin{para}[Freie und gebundene Symbole]
  Der Ausdruck $2 \leq x \leq 5$ ist keine Aussage,
  denn sein Wahrheitswert, ja sogar seine Sinnhaftigkeit, hängt davon ab, was $x$ ist.
  Zum Beispiel ergibt $2 \leq \emptyset \leq 5$ gar keinen Sinn,
  da für uns $\leq$ nur als Vergleich zwischen Zahlen definiert ist.
  Ferner ist $2 \leq -1 \leq 5$ falsch und $2 \leq 2 \leq 5$ wahr.
  In $2 \leq x \leq 5$ nennen wir $x$ ein \term{freies} Symbol;
  in $\forall x \in \RR \holds 2 \leq x \leq 5$
  und auch in ${\exists x \in \RR \holds 2 \leq x \leq 5}$ hingegen wäre $x$ \term{gebunden},\footnote{%
    Das hat nicht mit der in \autoref{grund/logik:bsp-1} gemeinten Bindung zu tun.}
  vermöge $\forall$ \bzw $\exists$.
  Betrachte noch folgendes Textstück: \enquote{Definiere $x \df 3$. Dann gilt $2 \leq x \leq 5$.}
  Darin ist $x$ in $2 \leq x \leq 5$ gebunden, denn es wird zuvor zu~$3$ definiert.
  Mit den Betrachtungen in diesem Absatz
  haben wir zwar noch keine saubere Definition für \enquote{frei} und \enquote{gebunden},
  die hier vermittelte ungefähre Vorstellung genügt aber für unsere Zwecke.
\end{para}

\begin{para}[Aussageform, Prädikat]
  Es sei nun $M$ eine Menge.
  Ein Ausdruck mit einem freien Symbol nennen wir eine \term{Aussageform} über $M$,
  wenn für jedes Element von $M$ der Ausdruck zu einer Aussage wird (wahr oder falsch),
  wenn man das freie Symbol durch dieses Element von $M$ ersetzt.
  Zum Beispiel ist $2 \leq x \leq 5$ eine Aussageform über $\RR$ (und auch eine über $\NN$).
  Oft gibt man solchen Aussageformen Namen,
  das wollen wir in der Form $P(x) \dfiff 2 \leq x \leq 5$ notieren.
  Danach nennen wir $P$ ein \term{Prädikat} über derselben Menge, über welcher die Aussageform besteht.
  Die Wahl von $\dfiff$ als Schreibweise ist systematisch,
  denn von nun an gilt $P(x) \iff 2 \leq x \leq 5$,
  d.h. $P(x)$ gilt genau dann, wenn $2 \leq x \leq 5$ gilt.
\end{para}

\begin{para}[Unscharfe Redeweisen]
  Im täglichen, mathematischen Sprachgebrauch wird nicht immer sauber zwischen
  Aussageformen und Aussagen unterschieden.
  Zum Beispiel sagt man:
  Die Aussage $x > 2 \implies x > 0$ gilt für alle $x \in \RR$.
  Genauer müsste es etwa so heißen:
  Die Aussageform $x > 2 \implies x > 0$ wird für alle $x \in \RR$
  zu einer wahren Aussage.
  Das liefert in der Regel aber keinen Mehrwert und ist nur unnötig umständlich.
  Man kann es auch so sehen, dass durch \enquote{für alle $x \in \RR$}
  in Gedanken nacheinander jede reelle Zahl für $x$ eingesetzt wird,
  und dann ist jeweils \enquote{$x > 2 \implies x > 0$} eine Aussage.
  Diese Sichtweise haben wir bislang auch schon ganz natürlich angenommen,
  als wir $A$ und $B$ für irgendwelche Aussage haben stehen lassen
  und dann bei daraus gebildeten Ausdrücken, wie zum Beispiel $A \implies B$,
  auch von einer Aussage gesprochen haben.
\end{para}

\begin{para}[Quantoren]
  Durch Bindung gelangt man von einer Aussageform oder einem Prädikat zu einer Aussage.
  Wir kennen bereits die Konzepte \enquote{für alle} (in Zeichen $\forall$)
  und \enquote{es gibt} (in Zeichen $\exists$),
  die sogenannten \term{Quantoren},
  wobei wir \enquote{für alle} \bzw $\forall$ den \term{Allquantor}
  und \enquote{es gibt} \bzw $\exists$ den \term{Existenzquantor} nennen.
  Das sehen wir uns nun genauer an.
  Es sei $M$ eine Menge und $P$ ein Prädikat über~$M$.
  \begin{itemize}
  \item Wir meinen mit ${\forall x \in M \holds P(x)}$,
    dass $P(x)$ eine wahre Aussage ist, egal welches Element aus $M$ wir anstelle von $x$ einsetzen.
  \item Wir meinen mit ${\exists x \in M \holds P(x)}$,
    dass es in $M$ wenigstens ein Element gibt,
    das $P(x)$ zu einer wahre Aussage macht, wenn man es für $x$ einsetzt.
  \end{itemize}
  Was bedeutet ${\neg (\forall x \in M \holds P(x))}$?
  Das heißt ja, dass nicht für alle Elemente von $M$ gilt,
  dass man sie in $P(x)$ für $x$ einsetzen kann und eine wahre Aussage erhält,
  d.h. für wenigstens ein Element von $M$ wird $P(x)$ zu einer falschen Aussage
  (d.h. $\neg P(x)$ gilt) wenn man es für $x$ einsetzt.
  Also:
  \begin{IEEEeqnarray*}{0l}
    \neg (\forall x \in M \holds P(x)) \iff \exists x \in M \holds \neg P(x)
  \end{IEEEeqnarray*}
  Ähnlich überlegt man sich:
  \begin{IEEEeqnarray*}{0l}
    \neg (\exists x \in M \holds P(x)) \iff \forall x \in M \holds \neg P(x)
  \end{IEEEeqnarray*}
  Wir merken uns:
  Bei der Negation einer Aussage mit Quantoren kehrt man die Quantoren um,
  es wird also aus $\forall$ ein $\exists$; und aus $\exists$ wird ein $\forall$;
  das Prädikat oder die Aussageform am Ende der Aussage wird negiert.
\end{para}

\begin{para}[Allgemeinere Prädikate]
  Der Begriff des Prädikats verallgemeinert sich in naheliegender Weise
  auf Aussageformen mit mehreren freien Symbolen.
  Wir sehen uns das nur anhand von Beispielen an.
  Zum Beispiel erhalten wir vermöge $P(x,y) \dfiff \frac{1}{x} < y$
  ein Prädikat über $\RRnz \times \RR$.
  Für alle $(x,y) \in \RRnz \times \RR$ ist $P(x,y)$ genau dann wahr,
  wenn der Kehrwert von $x$ kleiner als $y$ ist.
\end{para}

\begin{para}[Genauere Betrachtung der Quantoren]
  Es sei $P$ ein Prädikat über $M \times N$, wobei $M$ und $N$ Mengen sind.
  Dann könnte man sich für folgende Aussage interessieren:
  \begin{IEEEeqnarray*}{0l}
    \forall x \in M \innerholds \exists y \in N \holds P(x,y)
  \end{IEEEeqnarray*}
  Das bedeutet, dass es zu jedem Element in $M$ ein Element in $N$ so gibt,
  dass diese beiden Elemente in $P$ eingesetzt zu einer wahren Aussage führen.
  Für unser Beispiel aus dem vorigen Absatz sähe das so aus:
  \begin{IEEEeqnarray*}{0l}
    \forall x \in \RRnz \innerholds \exists y \in \RR \holds \frac{1}{x} < y
  \end{IEEEeqnarray*}
  Dies ist wahr, denn zu gegebenem $x \in \RRnz$ können wir etwa $y \df \frac{1}{x} + 1$ wählen.
  Was die Negation davon (die ja falsch ist) betrifft, haben wir:
  \begin{IEEEeqnarray*}{0l}
    \neg \p{\forall x \in \RRnz \innerholds \exists y \in \RR \holds \frac{1}{x} < y}
    \iff \exists x \in \RRnz \innerholds \forall y \in \RR \holds \neg \p{\frac{1}{x} < y} \\
    \eqin \iff \exists x \in \RRnz \innerholds \forall y \in \RR \holds \frac{1}{x} \geq y
  \end{IEEEeqnarray*}
  In der letzten Darstellung wird besonders gut klar, warum das falsch ist.
  Denn das würde bedeuten, dass es eine größte reelle Zahl gibt,
  was offenbar falsch ist.
\end{para}

\section{Kontraposition}

Wir haben bereits gesehen, dass aus $A \implies B$ nicht im Allgemeinen folgt, dass auch $B \implies A$ gilt.
Allerdings haben wir:

\begin{satz}[Kontraposition]
  \label{grund/logik:kontra}
  Es gilt:
  \begin{IEEEeqnarray*}{0l}
    (A \implies B) \iff (\neg B \implies \neg A)
  \end{IEEEeqnarray*}
  Von der Richtigkeit dieses Satzes kann man sich leicht mittels einer Wahrheitstafel überzeugen,
  was wir als zusätzliche Übung überlassen.
  Wir nennen $\neg B \implies \neg A$ die \term{Kontraposition} zu ${A \implies B}$.
\end{satz}

\begin{example}
  \label{grund/logik:bsp-kontra}
  Als Beispiel betrachten wir die folgende Aussage:
  \begin{IEEEeqnarray}{0l}
    \label{grund/logik:kontra:1}
    \forall n \in \NN \holds \text{$n^2$ ist gerade} \implies \text{$n$ ist gerade}
  \end{IEEEeqnarray}
  Per Kontraposition ist dies äquivalent zu:
  \begin{IEEEeqnarray}{0l}
    \label{grund/logik:kontra:2}
    \forall n \in \NN \holds \text{$n$ ist ungerade} \implies \text{$n^2$ ist ungerade}
  \end{IEEEeqnarray}
  Wir wollen beweisen, dass~\eqref*{grund/logik:kontra:1} gilt.
  Hier hilft uns die Kontraposition,
  denn es ist~\eqref*{grund/logik:kontra:2} leichter zu beweisen als~\eqref*{grund/logik:kontra:1}:
  Es sei $n \in \NN$ ungerade, also $n = 2k + 1$ für ein $k \in \NNzero$.
  Dann gilt $n^2 = (2k+1)^2 = 4k^2 + 4k + 1 = 2 (2k^2+2k) + 1$,
  also ist $n^2$ ungerade, denn wir können $n^2$ darstellen in der Form $2l+1$
  mit $l \in \NNzero$, nämlich $l = 2k^2+2k$.
  So wissen wir jetzt also, dass \eqref*{grund/logik:kontra:2} gilt
  und damit nach \autoref{grund/logik:kontra} auch~\eqref*{grund/logik:kontra:1}.
\end{example}

\section{Sätze}

\begin{para}[Bezeichnungen]
  Eine bewiesene mathematische Aussage nennen wir auch einen \term{Satz}.
  Ein Beweis kann wenige Zeilen lang sein oder aber sich über viele Seiten erstrecken.
  Wir haben in diesem Kapitel bereits einige kurze, einfache Beweise kennengelernt,
  zum Beispiel für \autoref{grund/logik:de-morgan} und \autoref{grund/logik:bsp-kontra}.
  Man nennt mathematische Sätze je nach Bedeutung und Komplexität
  \enquote{Satz}, \enquote{Lemma}, \enquote{Proposition}, \enquote{Bemerkung}, \etc.
  Der Einfachheit halber machen wir diese Unterscheidung in diesem Skript nicht.
  Besonders grundlegende Sätze zeichnen wir in diesem Skript als Sätze aus
  (wie zum Beispiel bei~\autoref{grund/logik:de-morgan} oder \autoref{grund/logik:kontra})
  und geben ansonsten einige Sätze einfach so als Absätze an
  oder nennen sie ohne besondere Auszeichnung im Text.
\end{para}

\begin{para}[Viele mögliche Formulierungen]
  Sätze werden auf verschiedene Arten formuliert.
  Man muss mit diesen Formulierungen vertraut sein,
  auch wenn man den Satz nur anwenden möchte.
  Als Beispiel betrachte:
  \begin{texteqn}
    \label{grund/logik:satz:1}
    \textbf{Satz.}
    Es gilt: \, $\forall n \in \NN \holds \text{$n^2$ ist gerade} \implies \text{$n$ ist gerade}$
  \end{texteqn}
  Wir wissen bereits, dass dies wahr ist (\autoref{grund/logik:bsp-kontra}).
  Dasselbe lässt sich mit mehr Worten formulieren:
  \begin{texteqn}
    \label{grund/logik:satz:2}
    \textbf{Satz.}
    Für alle $n \in \NN$ gilt: Wenn $n^{2}$ gerade ist, dann ist $n$ gerade.
  \end{texteqn}
  Üblich ist aber auch:
  \begin{texteqn}
    \label{grund/logik:satz:3}
    \textbf{Satz.}
    Es sei $n \in \NN$ so, dass $n^{2}$ gerade ist. Dann ist $n$ gerade.
  \end{texteqn}
  Dies sagt dasselbe aus.
  Den Bogen von \eqref*{grund/logik:satz:3} zu \eqref*{grund/logik:satz:1} spannt man,
  indem man \eqref*{grund/logik:satz:3} so liest:
  Wann immer wir eine natürliche Zahl $n$ gegeben haben (hier haben wir den Allquantor),
  für die $n^{2}$ gerade ist (Prämisse der Implikation),
  dann wissen wir auch, dass $n$ gerade ist (Konklusion der Implikation).
  \par
  Noch eine alternative Formulierung dieses Satzes ist:
  \begin{texteqn}
    \label{grund/logik:satz:4}
    \textbf{Satz.}
    Eine natürliche Zahl $n$, für die $n^{2}$ gerade ist, ist gerade.
  \end{texteqn}
  Diese Formulierung ist nicht ganz ohne Gefahr.
  Denn sie könnte so missverstanden werden, dass es eine natürliche Zahl $n$ \emphasis{gibt},
  für die $n^{2}$ und $n$ gerade sind;
  dabei würde also insbesondere anstatt eines Allquantors ein Existenzquantor verstanden.
  Typischerweise wird ein Existenzquantor durch Formulierungen wie
  \enquote{es existiert} oder \enquote{es gibt} oder \enquote{man findet} gekennzeichnet,
  somit würde in aller Regel \eqref{grund/logik:satz:4} richtig verstanden werden
  (nämlich so wie \eqref{grund/logik:satz:1} bis \eqref{grund/logik:satz:3}).
  Vermutlich haben Sie auch das Wort \enquote{Eine} am Anfang von \autoref{grund/meng:1}
  richtig verstanden, nämlich im Sinne von \enquote{Jede}.
  Im Zweifel sollte man lieber mehr als weniger explizit sein.
\end{para}

\begin{para}[Anwendung, Zuordnung]
  \label{grund/logik:satz-anwenden}
  Was helfen uns Sätze?
  Der oben diskutierte Satz hilft uns,
  wenn wir eine natürliche Zahl gegeben haben, deren Quadrat gerade ist,
  und wir gerne wissen möchten, ob auch diese Zahl selbst gerade ist.
  Der Satz garantiert uns, dass dies so ist.
  Dass so etwas wirklich nützlich ist,
  wird vielleicht deutlicher, wenn wir ein Beispiel aus der Statistik betrachten:
  \begin{texteqn*}
    \textbf{Satz.}
    Es seien $X, Y$ Zufallsvariablen so, dass $X$ und $Y$ unabhängig sind
    und endliche Erwartungswerte haben.
    Dann gilt $\expect{XY} = \expect{X} \cdot \expect{Y}$.
  \end{texteqn*}
  Wie Zufallsvariablen, Unabhängigkeit und der Erwartungswert (in Zeichen $\expectSymbol$) definiert sind,
  spielt für uns im Moment keine Rolle;
  wir beachten nur, dass dies wichtige Begriffe aus der Statistik sind.
  Der obige Satz hilft uns, wenn wir unabhängige Zufallsvariablen mit endlichen Erwartungswerten haben
  und wir etwas über den Erwartungswert des Produktes dieser Zufallsvariablen wissen möchten.
  Denn der Satz sagt uns, dass in so einer Situation der Erwartungswert des Produktes
  das Produkt der Erwartungswerte ist, in Zeichen $\expect{XY} = \expect{X} \cdot \expect{Y}$.
  Natürlich müssen dazu in der Anwendung die beiden Zufallsvariablen
  nicht unbedingt mit den Symbolen $X$ und $Y$ bezeichnet sein.
  Bei der Anwendung des Satzes muss man daher zunächst eine Zuordnung festlegen,
  welches Symbol aus der Formulierung des Satzes
  zu welchem Symbol oder Objekt in der Anwendung korrespondieren soll.
  Zum Beispiel könnten wir in der Anwendung unabhängige Zufallsvariablen $S$ und $T$
  mit endlichen Erwartungswerten haben.
  Dann ordnen wir zu $X \assign S$ und $Y \assign T$
  und erhalten $\expect{ST} = \expect{S} \cdot \expect{T}$.
  (Die Notation \enquote{$\assign$} für diesen Zweck ist nicht verbreitet in der Literatur,
  und wir verwenden sie nur gelegentlich.)
\end{para}

\exercisesection{grund/logik}

\begin{exercise}
  Führen Sie den Beweis zu \autoref{grund/logik:de-morgan}~\ref{grund/logik:de-morgan:2}
  über eine Wahrheitstafel aus.
\end{exercise}

\begin{exercise}
  Welche der folgenden Aussagen sind wahr (egal welche Wahrheitswerte $A$ und $B$ haben),
  und für welche gibt es eine Belegung mit Wahrheitswerten, die zu einer falschen Aussage führt?
  Führen Sie geeignete Argumente oder geben Sie geeignete Belegungen an.
  \begin{enumerate}
  \item $A \iff A$
  \item $A \iff (A \land B)$
  \item $\neg A \implies \neg (A \land B)$
  \item $(A \land \neg B) \implies (\neg A \lor \neg B)$
  \end{enumerate}
\end{exercise}

\begin{exercise}
  Diskutieren Sie die Unterschiede zwischen den beiden folgenden Aussagen
  und ob diese wahr oder falsch sind:
  \begin{IEEEeqnarray*}{0l}
    \forall x \in \RR \innerholds \exists y \in \RR \holds x < y \\
    \exists y \in \RR \innerholds \forall x \in \RR \holds x < y
  \end{IEEEeqnarray*}
\end{exercise}

%% FIXME
%% Die Indexschreibweise für Summen ist noch nicht bekannt!
%%
%% Vielleicht einfach die relevanten Absätze aus späterem Kapitel
%% hinter die Absätze mit der Indexschreibweise für Vereinigung und
%% Schnitt in Kapitel 1 kopieren?
%%
\begin{exercise}
  Wir nehmen folgenden Satz als bekannt an:
  \begin{texteqn*}
    \textbf{Satz.}
    Für alle $n \in \NN$ und alle $x,y \in \RR^{n}$ gilt:
    $$\p{\sum_{i=1}^{n} x_{i} y_{i}}^{2} \leq \p{\sum_{i=1}^{n} x_{i}^{2}} \p{\sum_{i=1}^{n} y_{i}^{2}}$$
  \end{texteqn*}
  Wir nehmen weiter an,
  in einer Anwendung haben wir $a,b,c \in \RR$ und $x \in \RR^{3}$.
  Geben Sie mindestens ein Beispiel, welche Erkenntnis der Satz uns in dieser Situation liefert.
\end{exercise}

\solutionsection

\begin{solution}
  Wir haben folgende Wahrheitstafel:
  \begin{inlinetable}
    \begin{tabular}{cc@{\hspace{2.0em}}ccccc}
      \toprule
      $A$ & $B$ & $A \lor B$ & $\neg (A \lor B)$ & $\neg A$ & $\neg B$ & $\neg A \land \neg B$ \\
      \midrule
      \true  & \true  & \true  & \false & \false & \false & \false \\
      \true  & \false & \true  & \false & \false & \true  & \false \\
      \false & \true  & \true  & \false & \true  & \false & \false \\
      \false & \false & \false & \true  & \true  & \true  & \true  \\
      \bottomrule
    \end{tabular}
  \end{inlinetable}
  \medskip
\end{solution}

\begin{solution}
  \begin{enumerate}
  \item $A \iff A$ ist wahr.
    Denn entweder steht rechts und links von \enquote{$\iff$} eine wahre Aussage (wenn $A$ wahr ist),
    oder es steht rechts und links von \enquote{$\iff$} eine falsche Aussage (wenn $A$ falsch ist).
  \item $A \iff (A \land B)$ ist falsch für $A \lassign \true$ und $B \lassign \false$.
    Denn mit dieser Belegung ist $A$ wahr, jedoch $A \land B$ ist falsch.
  \item $\neg A \implies \neg (A \land B)$ ist wahr.
    Denn: Wir setzen voraus, dass $\neg A$ gilt, also $A$ gilt nicht.
    Wir müssen $\neg (A \land B)$ zeigen,
    also wir müssen zeigen, dass $A \land B$ nicht gilt.
    Das erreichen wir, indem wir zeigen, dass $A$ nicht gilt oder $B$ nicht gilt.
    Wir wissen schon, dass $A$ nicht gilt. Damit sind wir fertig.
  \item $(A \land \neg B) \implies (\neg A \lor \neg B)$ ist wahr.
    Denn: Wir setzen voraus, dass $A \land \neg B$ gilt.
    Wir müssen $\neg A \lor \neg B$ zeigen.
    Das erreichen wir, indem wir $\neg A$ oder $\neg B$ zeigen.
    Letzteres gilt wegen $A \land \neg B$. Damit sind wir fertig.
  \end{enumerate}
\end{solution}

\begin{solution}
  Die erste Aussage ist wahr, was wir so einsehen:
  Es sei $x \in \RR$.
  Definiere $y \df x + 1$.
  Dann gilt $y \in \RR$ und $x < x+1 = y$.
  \par
  Die zweite Aussage ist falsch.
  Um das einzusehen, beweisen wir die Negation davon.
  Es gilt:
  \begin{IEEEeqnarray*}{0l}
    \neg \p{\exists y \in \RR \innerholds \forall x \in \RR \holds x < y}
    \iff \forall y \in \RR \innerholds \exists x \in \RR \holds \neg \p{x < y} \\
    \eqin \iff \forall y \in \RR \innerholds \exists x \in \RR \holds x \geq y
  \end{IEEEeqnarray*}
  In der letzten Darstellung der Aussage ist deren Richtigkeit leicht einzusehen, nämlich:
  Es sei $y \in \RR$. Definiere $x \df y$. Dann gilt $x \geq x = y$.
  \par
  Warum sind diese Aussage so unterschiedlich,
  obwohl \enquote{nur} die Reihenfolge der Quantoren vertauscht wurde?
  Das Prädikat am Ende ist gleich.
  Der Unterschied liegt in der logischen Abhängigkeit von $x$ und $y$.
  In der erste Aussage ist $x$ schon bekannt, wenn $y$ gefunden oder konstruiert werden muss.
  Das macht es sehr leicht, $y$ zu konstruieren: Wir nehmen eine Zahl größer als $x$, zum Beispiel $x+1$.
  In der zweiten Aussage müssen wir $y$ ohne Kenntnis von $x$ finden oder konstruieren.
  Dies ist jedenfalls keine einfachere Aufgabe, da wir mit weniger Information auskommen müssen.
  Es zeigt sich im konkreten Fall, dass die Aufgabe sogar unmöglich ist, also die Aussage falsch ist.
\end{solution}

\begin{solution}
  Eine mögliche Anwendung des Satz ergibt sich über folgende Zuordnung:
  \begin{IEEEeqnarray*}{0l+l+l+l+l}
    n \assign 3 & x_{1} \assign a & x_{2} \assign b & x_{3} \assign c & y \assign x
  \end{IEEEeqnarray*}
  Der Satz liefert nun:
  \begin{IEEEeqnarray*}{0l}
    \p{ax_{1} + bx_{2} + cx_{3}}^{2} \leq \p{a^{2}+b^{2}+c^{2}} \p{x_{1}^{2}+x_{2}^{2}+x_{3}^{2}}
  \end{IEEEeqnarray*}
\end{solution}
