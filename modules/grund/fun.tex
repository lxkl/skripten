\chapter{Funktionen}
\label{grund/fun}

\section{Relationen}

\begin{para}[Relation, Domain, Bildmenge]
  Es seien $A$ und $B$ Mengen.
  Eine Teilmenge von $A \times B$ nennen wir eine \term{Relation von $A$ nach $B$}.
  Wenn $A \subseteq A'$ und $B \subseteq B'$ gilt, dann ist offenbar jede Relation von $A$ nach $B$
  auch eine Relation von $A'$ nach $B'$.
  \par
  Es sei $R \subseteq A \times B$, also $R$ sei eine Relation von $A$ nach $B$.
  Wenn $(x,y) \in R$ gilt, so sagen wir, dass $x$ in Relation $R$ zu $y$ steht.
  Definiere die \term{Domain} oder den \term{Definitionsbereich} von $R$ als:
  \begin{IEEEeqnarray*}{0l}
    \dom(R) \df \set{x \in A \suchthat \exists y \in B \holds (x,y) \in R}
  \end{IEEEeqnarray*}
  Dies ist also die Menge aller ersten Komponenten der Paare in $R$.
  Analog definiere die \term{Bildmenge} von $R$ als:
  \begin{IEEEeqnarray*}{0l}
    \img(R) \df \set{y \in B \suchthat \exists x \in A \holds (x,y) \in R}
  \end{IEEEeqnarray*}
  Dies ist also die Menge aller zweiten Komponenten der Paare in $R$.
  Es sind $\dom(R)$ und $\img(R)$ offenbar unabhängig von der Wahl von $A$ und $B$,
  solange $R \subseteq A \times B$ gilt.
  Es spricht nichts dagegen, sich beide Definitionen so zu merken:
  \begin{IEEEeqnarray*}{0l+l}
    \dom(R) = \set{x \suchthat \exists y \holds (x,y) \in R} &
    \img(R) = \set{y \suchthat \exists x \holds (x,y) \in R}
  \end{IEEEeqnarray*}
\end{para}

\begin{example}
  \label{grund/fun:ex-1}
  Definiere $R \df \set{(1,2), (-8,\emptyset), (-8,7), (-1, -5), (\NN, 3)}$.
  Dann gilt:
  \begin{IEEEeqnarray*}{0l+l}
    \dom(R) = \set{1, -8, -1, \NN} &
    \img(R) = \set{2, \emptyset, 7, -5, 3}
  \end{IEEEeqnarray*}
\end{example}

\begin{para}[Funktionen: eindeutige Relationen]
  Es sei $R$ eine Relation.
  Wir nennen $R$ \term{eindeutig}, wenn gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall x \in \dom(R) \innerholds \forall y,z \in \img(R) \holds
    (x,y), (x,z) \in R \implies y = z
  \end{IEEEeqnarray*}
  Bei einer eindeutigen Relation gibt es also zu jedem Element $x$ der Domain
  \emphasis{genau ein} Element der Bildmenge, das zu $x$ in dieser Relation steht.
  Nach Definition einer Relation gibt es \emphasis{mindestens} ein solches Element;
  entscheidend ist also das \enquote{genau}.
  Eine eindeutige Relation $R$ nennen wir auch eine \term{Funktion}.
\end{para}

\begin{para}[Redeweisen]
  Es sei $f$ eine Funktion.
  Für alle $x \in \dom(f)$ bezeichnen wir das eindeutig bestimmte $y \in \img(f)$,
  für das $(x,y) \in f$ gilt, mit $f(x)$.
  Wir sagen auch, dass $f$ das $x \in \dom(f)$ auf $f(x)$ \term{abbildet}.
  Eine Funktion $f$ können wir mit einer Maschine vergleichen.
  Geben wir ein $x \in \dom(f)$ in die Machine ein, so wirft diese $f(x)$ aus.
  Weitere Redeweisen sind üblich für alle $x \in \dom(f)$:
  \begin{itemize}
  \item $f(x)$ ist das \term{Bild} oder der \term{Funktionswert} von $x$ unter~$f$;
  \item $f(x)$ ist der \term{Funktionswert} oder der \term{Wert} von $f$ \term{an der Stelle} $x$
    oder \term{in} $x$;
  \item $f$ nimmt \term{an der Stelle} $x$ oder \term{in} $x$ den \term{Funktionswert}
    oder den \term{Wert} $f(x)$ an.
  \end{itemize}
  Wenn $\dom(f) \subseteq \bigtimes_{i=1}^{n} A_{i}$ für Mengen $\eli{A}{n}$ ist,
  dann schreiben wir meistens $f(\eli{x}{n})$ anstelle von $f((\eli{x}{n}))$
  für alle $(\eli{x}{n}) \in \dom(f)$,
  also wir lassen die doppelten Klammern fort.
\end{para}

\begin{example}
  \begin{enumerate}
  \item Definiere $f \df \set{(1,2), (-8,\emptyset), (-1, -5), (\NN, 3)}$,
    was offenbar eine Funktion ist.
    Dann gilt $f(1) = 2$ und $f(-8) = \emptyset$ und $f(-1) = -5$ und $f(\NN) = 3$.
    In der Analogie mit der Maschine kann man sagen:
    Es wirft die Maschine $2$ aus, wenn wir $1$ eingeben;
    sie wirft $\emptyset$ aus, wenn wir $-8$ eingeben;
    sie wirft $-5$ aus, wenn wir $-1$ eingeben;
    und sie wirft $3$ aus, wenn wir $\NN$ eingeben.
  \item Die Relation $R$ aus \autoref{grund/fun:ex-1} ist keine Funktion,
    da gilt ${(-8,\emptyset), (-8,7) \in R}$ und $\emptyset \neq 7$.
    Entfernen wir $(-8,\emptyset)$ oder $(-8,7)$, so wird diese Relation eine Funktion.
  \item Definiere $f \df \set{ (x,x^{2}) \suchthat x \in \RR }$.
    Dann ist $f$ eine Funktion.
    Eine Moment könnte man daran Zweifel haben, da zum Beispiel $(2,4) \in f$ und $(-2,4) \in f$ gilt.
    Ein erneuter Blick auf die Definition der Eindeutigkeit klärt das aber.
    Übereinstimmung in der zweiten Komponente ist kein Problem für die Eindeutigkeit.
    Ein Problem für die Eindeutigkeit wäre hingegen eine Übereinstimmung in der ersten Komponente
    kombiniert mit einer Abweichung in der zweiten Komponente.
    Das kommt bei dieser Relation nicht vor, wie man sich leicht überlegt:
    Wenn $(x,y), (x,y') \in f$ gilt, dann haben wir $y = x^{2} = y'$ nach Definition,
    also auch die zweiten Komponenten sind dann gleich.
  \item Definiere $f \df \set{ ((x,y),x-y) \suchthat x,y \in \RR }$.
    Dann ist $\dom(f) = \RR^{2}$.
    Es gilt zum Beispiel $f(7,10) = -3$ und $f(-1,-1) = 0$;
    beachte, dass wir nicht $f((7,10))$ oder $f((-1,-1))$ schreiben,
    obwohl das vielleicht systematischer wäre.
  \end{enumerate}
\end{example}

\section{Notation für Funktionen}
\label{grund/fun:notation}

\begin{para}[Pfeilnotation]
  \label{grund/fun:ffn}
  Es sei $f$ eine Relation, und $A$ und $B$ seien Mengen.
  Definiere:
  \begin{IEEEeqnarray*}{0l}
    \ffn{f}{A}{B} \,\dfiff\,  \text{$f$ ist Funktion} \land \dom(f) = A \land \img(f) \subseteq B
  \end{IEEEeqnarray*}
  Es ist $\ffn{f}{A}{B}$ also eine Aussage,
  nämlich die Aussage, dass $f$ eine Funktion mit Domain $A$ ist
  und einer Bildmenge, die eine Teilmenge von $B$ ist.
  Wir sagen dazu auch, dass $f$ eine Funktion \term{von $A$ nach $B$} ist.
  Es gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall B' \supseteq B \holds\, \ffn{f}{A}{B} \implies \ffn{f}{A}{B'}
  \end{IEEEeqnarray*}
  Denn gefordert wird ja nur,
  dass die rechts vom Pfeil genannte Menge eine \emphasis{Obermenge} der Bildmenge ist.
\end{para}

\begin{para}[Totalität]
  Vom Anfang dieses Kapitels kennen wir die Redeweise,
  dass etwas eine Relation von $A$ nach $B$ ist.
  Wenn wir sagen, $f$ ist eine Funktion von $A$ nach~$B$, dann meinen wir damit aber nicht nur,
  dass $f$ ist eine eindeutige Relation von $A$ nach $B$ ist.
  Sondern wir meinen zusätzlich noch, dass $\dom(f) = A$ gilt.
  Üblich für den letzteren Sachverhalt ist auch die Redeweise:
  $f$ ist eine \term{totale} Relation von $A$ nach $B$.
  Damit können wir sagen: $\ffn{f}{A}{B}$ bedeutet genau,
  dass $f$ eine eindeutige und totale Relation von $A$ nach $B$ ist.
\end{para}

\begin{para}[Pfeilnotation zur Definition]
  Die Notation aus \autoref{grund/fun:ffn} verwenden wir auch zur Definition von Funktionen.
  Gegeben Mengen $A$ und $B$, so können wir eine Funktion $f$ von $A$ nach $B$ folgendermaßen definieren:
  \begin{IEEEeqnarray}{0l}
    \label{grund/fun:ffnx}
    \ffnx{f}{A}{B}{x \mapsto E}
  \end{IEEEeqnarray}
  Dabei ist $E$ ein Ausdruck,
  der für alle $\hatx \in A$ zu einem Element von $B$ wird,
  wenn wir jedes freie Vorkommen des Symbols $x$ in $E$ durch $\hatx$ ersetzen.
  Den Teil $x \mapsto E$ nennen wir die \term{Abbildungsvorschrift} der Funktion.
  Wenig überraschend gilt, dass auch ein anderes Symbol anstelle von $x$ verwendet werden kann.
  \par
  Die Notation \eqref{grund/fun:ffnx} wird nicht nur zur Definition,
  sondern auch als Aussage verwendet.
  Wenn bereits eine Relation $f$ bekannt ist,
  dann bedeutete \eqref{grund/fun:ffnx} die folgende Aussage:
  Es gilt $\ffn{f}{A}{B}$ (siehe \autoref{grund/fun:ffn})
  und außerdem gilt für alle $\hatx \in A$, dass $f(\hatx)$ das Objekt ist,
  das wir aus~$E$ erhalten, indem wir in $E$ jedes freie Vorkommen des Symbols $x$ durch $\hatx$ ersetzen.
\end{para}

\begin{example}
  \begin{enumerate}
  \item Definere:
    \begin{IEEEeqnarray*}{0l}
      f \df \set{(1,2), (4,5), (5,6), (7,9)}
    \end{IEEEeqnarray*}
    Dann gilt $\ffn{f}{\set{1,4,5,7}}{\RR}$, und vielmehr gilt noch:
    \begin{IEEEeqnarray}{0l}
      \label{grund/fun:ex-def}
      \ffnx{f}{\set{1,4,5,7}}{\RR}{x \mapsto
        \begin{cases}
          x+1 & \text{wenn $x < 7$} \\
          9 & \text{sonst}
        \end{cases}}
    \end{IEEEeqnarray}
    Anstelle von $\RR$ hätten wir jede Menge schreiben können,
    die $\img(f) = \set{2,5,6,9}$ als Teilmenge enthält.
    Auch hätten wir \eqref{grund/fun:ex-def} zur Definition von $f$ verwenden können.
  \item Definiere $\ffnx{f}{\RR}{\RR}{x \mapsto x^{2}}$.
    Dann gilt zum Beispiel ${(0,0), (-2,4), (2,4) \in f}$.
  \item Das Gebilde
    \begin{IEEEeqnarray*}{0l}
      \ffnx{f}{\RR}{\RR}{x \mapsto \frac{1}{x}}
    \end{IEEEeqnarray*}
    definiert keine Funktion, da nicht gesagt wird, worauf $0$ abgebildet werden soll.
  \item Wenn die Domain einer Funktion eine Teilmenge eines kartesischen Produktes ist,
    kann man auf der linken Seite des Pfeils in der Abbildungsvorschrift mehrere Symbole verwenden,
    für jede Komponente der Tupel eines.
    Zum Beispiel:
    \begin{IEEEeqnarray*}{0l}
      \ffnx{f}{\RR \times \RRnz \times \RR}{\RR}{(x,y,z) \mapsto \frac{z}{2y} + x}
    \end{IEEEeqnarray*}
    Dann gilt etwa $f(5,-1,8) = \frac{8}{2 \ccdot (-1)} + 5 = -4 + 5 = -1$.
    Beachte wieder, dass wir nicht $f((5,-1,8))$ schreiben.
  \end{enumerate}
\end{example}

\begin{para}[Anonyme Funktion]
  Manchmal möchte man eine Funktion definieren, aber kein Symbol für sie einführen.
  Man spricht dann auch von einer \term{anonymen Funktion}.
  Die Notation dafür ist naheliegend:
  \begin{IEEEeqnarray*}{0l}
    \fnx{A}{B}{x \mapsto E}
  \end{IEEEeqnarray*}
  Das kommt zum Beispiel in Formulierungen wie dieser vor:
  \enquote{Es hat $\fnx{\RRnn}{\RR}{x \mapsto x^{2}}$ eine bestimmte Eigenschaft,
    aber $\fnx{\RR}{\RR}{x \mapsto x^{2}}$ hat diese Eigenschaft nicht.}
\end{para}

\section{Umkehrung und Injektivität}

\begin{para}[Umkehrrelation, Injektivität]
  Es sei $R$ eine Relation.
  Wir nennen
  \begin{IEEEeqnarray*}{0l}
    R^{-1} \df \set{(y,x) \suchthat (x,y) \in R}
  \end{IEEEeqnarray*}
  die \term{Umkehrrelation} zu $R$.
  Offenbar gelten:
  \begin{itemize}
  \item $R^{-1}$ ist eine Relation.
  \item $\dom(R^{-1}) = \img(R)$ und $\img(R^{-1}) = \dom(R)$
  \item $(R^{-1})^{-1} = R$
  \end{itemize}
  Wir nennen eine Funktion $f$ \term{injektiv},
  wenn $f^{-1}$ wieder eine Funktion ist.
  Wenn dem so ist,
  dann nennen wir $f^{-1}$ die \term{Umkehrfunktion} von $f$, und es gilt dann:
  \begin{IEEEeqnarray*}{0l+l}
    \forall x \in \dom(f) \holds f^{-1}(f(x)) = x &
    \forall y \in \img(f) \holds f(f^{-1}(y)) = y
  \end{IEEEeqnarray*}
  Man kann sagen: Es heben $f$ und $f^{-1}$ ihre Wirkungen gegenseitig auf.
\end{para}

\begin{example}
  \label{grund/fun:ex-4}
  \begin{enumerate}
  \item   Definiere $f \df \set{(1,2), (-8,\emptyset), (-1, -5), (\NN, 3)}$,
    was offenbar eine Funktion ist.
    Dann gilt:
    \begin{IEEEeqnarray*}{0l}
      f^{-1} = \set{(2,1), (\emptyset,-8), (-5, -1), (3, \NN)}
    \end{IEEEeqnarray*}
    Offenbar ist $f^{-1}$ eine Funktion, also ist $f$ injektiv.
  \item Definiere
    $f \df \set{(1,2), (-8,\emptyset), (\RR, \emptyset), (-1, -5), (\NN, 3)}$.
    Dies ist eine Funktion, aber nicht injektiv,
    denn $(\emptyset,\RR), (\emptyset,-8) \in f^{-1}$, also ist $f^{-1}$ keine Funktion.
  \item Definiere $\ffnx{f}{\RR}{\RR}{x \mapsto x^{2}}$.
    Dann gilt ${(-2,4), (2,4) \in f}$, also:
    \begin{IEEEeqnarray*}{0l}
      (4,-2), (4,2) \in f^{-1}
    \end{IEEEeqnarray*}
    Also ist $f^{-1}$ keine Funktion und $f$ ist nicht injektiv.
  \end{enumerate}
\end{example}

\begin{para}[Charakterisierungen der Injektivität]
  \label{grund/fun:inj-char}
  Es sei $f$ eine Funktion.
  Wir schreiben die Injektivität etwas um:
  \begin{IEEEeqnarray*}{0l}
    \text{$f$ injektiv}
    \iff \text{$f^{-1}$ ist Funktion} \\
    \eqin \iff \p[big]{\forall x \in \dom(f^{-1}) \innerholds \forall y,z \in \img(f^{-1}) \holds
      (x,y), (x,z) \in f^{-1} \implies y = z} \\
    \eqin \iff \p[big]{\forall x \in \img(f) \innerholds \forall y,z \in \dom(f) \holds
      (y,x), (z,x) \in f \implies y = z} \\
    \eqin \iff \p[big]{\forall y,z \in \dom(f) \holds
      f(y) = f(z) \implies y = z} \\
    \eqin \iff \p[big]{\forall y,z \in \dom(f) \holds
      y \neq z \implies f(y) \neq f(z)}
  \end{IEEEeqnarray*}
  An der letzten Darstellung lesen wir ab:
  Injektive Funktionen sind dadurch charakterisiert,
  dass sie Verschiedenes auf Verschiedenes abbilden.
  Zum Merken schreiben wir die letzten beiden Charakterisierungen
  noch einmal mit vielleicht etwas eingängigeren Symbolen auf:
  \begin{IEEEeqnarray*}{0l}
    \text{$f$ injektiv}
    \iff \p[big]{\forall x,x' \in \dom(f) \holds
      f(x) = f(x') \implies x = x'} \\*
    \eqin \iff \p[big]{\forall x,x' \in \dom(f) \holds
      x \neq x' \implies f(x) \neq f(x')}
  \end{IEEEeqnarray*}
\end{para}

\begin{para}[Problem der unbekannten Bildmenge]
  In der Praxis ist für eine Funktion $f$ die Menge $\dom(f)$ meistens explizit angegeben;
  zum Beispiel wenn die im vorigen Abschnitt vorgestellte Schreibweise verwendet wird.
  Hingegen ist $\img(f)$ oft nicht so offensichtlich,
  ebenso gibt es oft zunächst Unklarheit darüber, ob $f$ injektiv ist
  oder wie \ggf die Umkehrfunktion aussieht.
  Der folgende Satz hilft in so einer Situation.
  Er zeigt, wie wir ausgehend von einer Vermutung,
  was $\img(f)$ ist und wie $f^{-1}$ aussieht, vorgehen können.
  (Insbesondere vermuten wir dann also, dass $f$ injektiv ist.)
\end{para}

\begin{satz}[Kriterium für Umkehrfunktion]
  \label{grund/fun:satz-umkehr}
  Es seien $A$ und $B$ Mengen.
  Es seien $\ffn{f}{A}{B}$ und $\ffn{g}{B}{A}$ so, dass gilt:
  \begin{IEEEeqnarray*}{0l+l}
    \forall x \in A \holds g(f(x)) = x
    &\forall y \in B \holds f(g(y)) = y
  \end{IEEEeqnarray*}
  Dann ist $\img(f) = B$ und $g$ die Umkehrfunktion zu $f$,
  also insbesondere ist $f$ injektiv.
\end{satz}

\begin{proof}
  Wir zeigen zuerst $\img(f) = B$.
  Nach Voraussetzung ist $\img(f) \subseteq B$,
  also genügt es $B \subseteq \img(f)$ zu zeigen.
  Es sei $y \in B$.
  Nach Voraussetzung gilt $f(g(y)) = y$, also ist $y \in \img(f)$.
  \par
  Es gilt ferner:
  \begin{IEEEeqnarray*}{0l"s+x*}
    g = \set{ (y, g(y)) \suchthat y \in B } \\*
    \eqin = \set{ (f(x), g(f(x))) \suchthat x \in A } & da $B = \img(f)$ \\*
    \eqin = \set{ (f(x), x) \suchthat x \in A }
    = f^{-1} && \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\begin{example}
  Definiere $\ffnx{f}{\intci{5}}{\RR}{x \mapsto 2x}$.
  Für alle $x \in \dom(f)$ gilt $2x \geq 2\cdot 5 = 10$,
  also gilt $\ffn{f}{\intci{5}}{\intci{10}}$.
  Die Idee ist nun, den obigen Satz mit $B \assign \intci{10}$ anzuwenden.\footnote{%
    Für die Schreibweise $\assign$ erinnern wir an \autoref{grund/logik:satz-anwenden}.}
  Um die Abbildungsvorschrift für die vermeintliche Umkehrfunktion zu erhalten,
  führen wir eine Nebenrechnung, in der wir \enquote{$2x=y$} hinschreiben und dies nach $x$ auflösen
  mit den von der Schule bekannten Regeln, was hier nur eine Division durch $2$ bedeutet.
  (Wir werden die Rechenregeln für reelle Zahlen in \autoref{grund/rl} genau wiederholen.)
  Definiere also $\ffnx{g}{\intci{10}}{\intci{5}}{x \mapsto \frac{x}{2}}$.
  Dies ist eine sinnvolle Definition,
  denn wenn $x \geq 10$ gilt, gilt $\frac{x}{2} \geq \frac{10}{2} = 5$,
  also ist die Menge $\intci{5}$ rechts vom Pfeil passend zur Domain und Abbildungsvorschrift gewählt.
  Schließlich überzeugt man sich leicht,
  dass $g(f(x)) = x$ für alle $x \in \intci{5}$
  gilt und dass $f(g(y)) = y$ für alle $y \in \intci{10}$ gilt.
  Nach \autoref{grund/fun:satz-umkehr} ist also $\img(f) = \intci{10}$
  und $g = f^{-1}$, insbesondere ist $f$ injektiv.
\end{example}

Injektive Funktionen erlauben es uns, Gleichungen in äquivalente Gleichungen umzuschreiben:

\begin{satz}[Äquivalente Umformulierung von Gleichheit mit injektiven Funktionen]
  \label{grund/fun:equiv-inj}
  Für alle injektiven Funktionen $f$ gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall x,x' \in \dom(f) \holds
    x = x' \iff f(x) = f(x')
  \end{IEEEeqnarray*}
\end{satz}
\begin{proof}
  Die Implikation $\implies$ ist trivial.
  Die Implikation $\impliedby$ ist äquivalent zur Injektivität,
  wie wir in \autoref{grund/fun:inj-char} gesehen hatten.
\end{proof}

\begin{para}[Einschränkung]
  \label{grund/fun:fnres}
  Es sei $\ffn{f}{A}{B}$ und $M \subseteq A$.
  Die \term{Einschränkung} von $f$ auf $M$ definieren wir als $\ffnx{\fnres{f}{M}}{M}{B}{x \mapsto f(x)}$.
  Hier wird also die Abbildungsvorschrift beibehalten und nur die Domain verändert.
  Durch Einschränkung können sich viele Eigenschaften einer Funktion verändern.
  Als Beispiel betrachte $\ffnx{f}{\RR}{\RRnn}{x \mapsto x^{2}}$.
  Dann ist $f$ nicht injektiv,
  jedoch sind $\fnres{f}{\intic{0}}$ und $\fnres{f}{\intci{0}}$ jeweils injektiv
  (was wir mit den Regeln aus \autoref{grund/rl} einsehen werden).
\end{para}

\section{Kombinationen von Funktionen}

\begin{para}[Komposition]
  Es gibt einfache Methoden, wie aus gegebenen Funktionen neue Funktionen erhalten werden können.
  Es seien $f$ und $g$ Funktionen so, dass $\img(f) \subseteq \dom(g)$ gilt.
  Dann definieren wir die \term{Komposition} oder \term{Hintereinanderausführung}
  $g \circ f$, gesprochen \enquote{$g$~nach~$f$}, durch:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{g \circ f}{\dom(f)}{\img(g)}{x \mapsto g(f(x))}
  \end{IEEEeqnarray*}
  Es wird also zuerst $f$ angewendet, und auf das dabei entstehende Ergebnis wird $g$ angewendet.
  Das ist für uns nichts Neues,
  denn schon bei der Umkehrfunktion haben wir Ausdrücke der Form \enquote{$f^{-1}(f(x))$} gehabt,
  wo zwei Funktionen hintereinander ausgeführt werden;
  nur die Schreibweise mit $\circ$ ist neu.
  Für ein weiteres Beispiel definieren wir:
  \begin{IEEEeqnarray}{0l+l}
    \label{grund/fun:beispiel-komposition}
    \ffnx{f}{\NN}{\RR}{x \mapsto 2x} & \ffnx{g}{\RR}{\RR}{x \mapsto x^2+1}
  \end{IEEEeqnarray}
  Dann gilt $(g \circ f)(7) = g(f(7)) = g(2\cdot 7) = g(14) = 14^2 + 1 = 197$.
\end{para}

\begin{para}[Skalierung, Addition, Multiplikation, Division]
  Es sei $\al \in \RR$.
  Es seien $f$ und $g$ Funktionen so,
  dass $\dom(f)=\dom(g)$ und $\img(f), \img(g) \subseteq \RR$ gelten;
  definiere $D \df \dom(f)=\dom(g)$.
  Dann definieren wir die Funktionen $\al f$ und $f+g$ und $fg$ durch:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{\al f}{D}{\RR}{x \mapsto \al f(x)}\\*
    \ffnx{f+g}{D}{\RR}{x \mapsto f(x) + g(x)}\\*
    \ffnx{fg}{D}{\RR}{x \mapsto f(x) g(x)}
  \end{IEEEeqnarray*}
  Wenn $g(x) \neq 0$ für alle $x \in D$ gilt, so definieren wir noch $\frac{1}{g}$ und $\frac{f}{g}$ als:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{\frac{1}{g}}{D}{\RR}{x \mapsto \frac{1}{g(x)}} \\*
    \ffnx{\frac{f}{g}}{D}{\RR}{x \mapsto \frac{f(x)}{g(x)}}
  \end{IEEEeqnarray*}
  Man spricht in allen diesen Fällen von der \term{punktweisen Verknüpfung},
  denn der Funktionswert der neuen Funktion (zum Beispiel $f+g$) an einer Stelle $x$ hängt
  nur von den Werten der beteiligten Funktionen an dieser einen Stelle $x$ ab
  (zum Beispiel im Falle $f+g$ von den Werten $f(x)$ und $g(x)$, die addiert werden).
\end{para}

\begin{example}
  Für die Funktionen aus~\eqref{grund/fun:beispiel-komposition} gilt zum Beispiel:
  \begin{IEEEeqnarray*}{0rCl}
    (3f)(7) & = & 3 \cdot f(7) = 3 \cdot 14 = 42 \\
    (f+g)(7) & = & f(7) + g(7) = 14 + (49+1) = 64 \\
    (fg)(7) & = & f(7) \cdot g(7) = 14 \cdot (49+1) = 700 \\*
    \parens{\frac{f}{g}}(7) & = & \frac{f(7)}{g(7)} = \frac{14}{49+1} = 0.28
  \end{IEEEeqnarray*}
\end{example}

\section{Bildmenge und Urbildmenge}

\begin{para}[Definition]
  Es sei $f$ eine Funktion.
  Wir kennen bereits die Bildmenge $\img(f)$ von~$f$.
  Diesen Begriff verallgemeinern wir nun.
  Für alle Mengen $X$ definiere die \term{Bildmenge von $X$ unter $f$} als:
  \begin{IEEEeqnarray*}{0l}
    \fnimg{f}(X) \df \set{f(x) \suchthat x \in X}
  \end{IEEEeqnarray*}
  Offenbar gilt $\fnimg{f}(\dom(f)) = \img(f)$.
  Für alle Mengen $Y$ definiere die \term{Urbildmenge von $Y$ unter $f$} als:
  \begin{IEEEeqnarray*}{0l}
    \fnpre{f}(Y) \df \set{x \in \dom(f) \suchthat f(x) \in Y}
  \end{IEEEeqnarray*}
  Es ist $\fnpre{f}(Y)$ also die Menge aller Elemente der Domain von $f$,
  welche durch $f$ in die Menge $Y$ hinein abgebildet werden.
  Offenbar gilt $\fnpre{f}(\img(f)) = \dom(f)$.
\end{para}

\begin{example}
  \begin{enumerate}
  \item Definiere $\ffnx{f}{\RR}{\RR}{x \mapsto x^2}$.
    Dann gilt $\fnimg{f}(\set{1,2,3}) = \set{1,4,9}$
    und $\fnpre{f}(\set{1,4,9}) = \set{1,2,3,{-1},{-2},{-3}}$.
  \item Definiere $\ffnx{f}{\RR}{\RR}{x \mapsto 2 + x^2}$.
    Dann gilt $\fnpre{f}(\set{-1,0,2}) = \set{0}$.
  \item Definiere:
    \begin{IEEEeqnarray*}{0l}
      \ffnx{f}{\RR}{\RR}{x \mapsto
        \begin{cases}
          -1 & \text{wenn $x \leq 0$} \\
          x & \text{sonst}
        \end{cases}}
    \end{IEEEeqnarray*}
    Dann gilt $\fnimg{f}(\intic{2}) = \set{-1} \cup \intoc{0}{2}$
    und $\fnpre{f}(\intic{0}) = \intic{0}$.
  \end{enumerate}
\end{example}

\begin{para}[Urbildmenge eines einzelnen Elementes]
  Es sei $y$ irgendein Objekt.
  Dann definieren wir die \term{Urbildmenge von $y$ unter $f$}
  als $\fnpre{f}(y) \df \fnpre{f}(\set{y}) = \set{x \in \dom(f) \suchthat f(x) = y}$.
  Dies ist also ein Spezialfall der Urbildmenge einer Menge.
  Wenn $y \not \in \img(f)$, dann gilt ${\fnpre{f}(y) = \emptyset}$.
  Wenn $y \in \img(f)$, dann gilt $\fnpre{f}(y) \neq \emptyset$.
  Wir nennen die Elemente von $\fnpre{f}(y)$ auch die \term{Urbilder von $y$ unter~$f$}.
  Wenn $y \in \img(f)$ und $f$ injektiv ist, dann gilt $\fnpre{f}(y) = \set{f^{-1}(y)}$,
  es gibt dann also genau ein Urbild von $y$, nämlich $f^{-1}(y)$.
\end{para}

\section{Codomain, Surjektivität und Bijektivität}

\begin{para}[Surjektivität]
  \label{grund/fun:cod-1}
  Es sei $R$ eine Relation und $B$ eine Menge.
  Dann nennen wir $R$ \term{surjektiv auf~$B$}, wenn $\img(R) = B$ gilt.
\end{para}

\begin{para}[Alternativer Funktionsbegriff]
  \label{grund/fun:cod-2}
  In der Teilen der Literatur ist es üblich, anstelle unseres Funktionsbegriffs
  (der ja ist: eine eindeutige Relation) einen etwas anderen Begriff zu haben.
  Nämlich dort heißt ein Paar $(R,C)$ eine \enquote{Funktion}
  -- wir wollen \term{\funcod} dafür schreiben --
  wenn $R$ eine eindeutige Relation ist und $\img(R) \subseteq C$ gilt;
  die Menge $C$ heißt die \term{Codomain} der \funcod.
  Sind dann $(R,C)$ und $(R,C')$ \funscod so, dass $C \neq C'$ gilt,
  so gilt natürlich auch $(R,C) \neq (R,C')$,
  also es handelt sich um unterschiedliche Funktionen$^{\ast}$.
  Dies bringt, wenn man es konsequent durchsetzt, erhebliche Probleme mit sich,
  die dann meistens dadurch \enquote{gelöst} werden, dass man doch meistens wieder so tut,
  als gäbe es die Codomain nicht.
  Details dazu sind für uns nicht relevant;
  Interessierte mit den nötigen Vorkenntnissen können dazu~\cite{Kliemann22} lesen.
  Wichtig ist nur, das Folgende zu kennen, falls es einem mal unterkommt:
\end{para}

\begin{para}[Surjektivität für Funktionen mit Codomain]
  \label{grund/fun:cod-3}
  Es sei $(R,C)$ eine \funcod.
  Dann nennen wir $(R,C)$ \term{surjektiv},
  wenn $R$ surjektiv auf $C$ ist im Sinne von \autoref{grund/fun:cod-1},
  also wenn $\img(R) = C$ gilt.
  Surjektivität ist jetzt also eine Eigenschaft einer \funcod
  und nicht mehr wie in \autoref{grund/fun:cod-1} die Eigenschaft einer
  Funktion \emphasis{bezüglich einer Menge}
  (die in \autoref{grund/fun:cod-1} $B$ hieß).
\end{para}

\begin{para}[Codomain an Notation ablesen]
  Nun werden \funscod meistens in der aus \autoref{grund/fun:notation} bekannten Notation eingeführt.
  Dann gilt als Codomain meistens die Menge, die rechts vom ersten Pfeil steht.
  Zum Beispiel ist dann $\fnx{\RR}{\RRnn}{x \mapsto x^{2}}$ surjektiv,
  aber $\fnx{\RR}{\RR}{x \mapsto x^{2}}$ ist nicht surjektiv.
\end{para}

\begin{para}[Bijektivität]
  Eine \funcod nennt man \term{bijektiv}, wenn sie injektiv und surjektiv ist.
  In diesem Kontext spricht man meistens von einer \enquote{Umkehrfunktion} nur für bijektive \funscod.
  Für eine unserer Funktionen hingegen liegt eine Umkehrfunktion vor,
  sobald unsere Funktion injektiv ist.
  Dieses Unterschiedes sollte man sich beim Studium mathematischer Literatur bewusst sein.
  Für das weitere Studium dieses Skriptes darf die Angelegenheit nun wieder vergessen werden.
\end{para}

\exercisesection{grund/fun}

\begin{exercise}
  Entscheiden Sie mit Begründung,
  ob die folgenden Funktionen jeweils injektiv sind und geben Sie \ggf die Umkehrfunktion an.
  Nützlich ist hier unser Kriterium für die Umkehrfunktion (\autoref{grund/fun:satz-umkehr}).
  \begin{enumerate}
  \item $\ffnx{f}{\RRnz}{\RR}{x \mapsto \frac{1}{x^2} + 1}$
  \item $\ffnx{g}{\RR}{\RR}{x \mapsto 9 x - 8}$
  \item $\ffnx{h}{\set{1}}{\RR}{x \mapsto 7}$
  \item $\ffnx{\phi}{\RRpos}{\RR}{x \mapsto \frac{1}{x}-1}$
  \item $\ffnx{\psi}{\RR \times \RR}{\RR}{(x,y) \mapsto x + 2y}$
  \end{enumerate}
\end{exercise}

\begin{exercise}
  Definiere:
  \begin{IEEEeqnarray*}{0l+l}
    \ffnx{f}{\RR}{\RR}{x \mapsto x^2} & \ffnx{g}{\RR}{\RR}{x \mapsto \frac{x}{2} + 1} \\
    \ffnx{\phi}{\RR}{\RR}{x \mapsto 5x - 1} & \ffnx{\psi}{\intoi{0}}{\RR}{x \mapsto \frac{1}{x}}
  \end{IEEEeqnarray*}
  Berechnen Sie:
  \begin{enumerate}
  \item $(f+g)(2)$
  \item $(f+g+\phi)(2)$
  \item $(\phi \cdot g)(2)$
  \item $(f \circ \psi)(10)$
  \item $(10 \cdot \psi)(5)$
  \item $(f \circ \phi \circ \psi)(10)$
  \item $(g \circ f \circ \phi \circ \psi)(10)$
  \item $(\psi \circ \psi)(2321)$
  \item $(f \circ g)(10)$
  \item $(g \circ f)(10)$
  \end{enumerate}
\end{exercise}

\pagebreak

\begin{exercise}
  Definiere:
  \begin{IEEEeqnarray*}{0l+l}
    \ffnx{f}{\RR}{\RR}{t \mapsto \frac{1}{t^{2}+1}} &
    \ffnx{g}{\RR \setminus \set{1}}{\RR}{t \mapsto \frac{7t+1}{t-1}}
  \end{IEEEeqnarray*}
  Berechnen Sie:
  \begin{enumerate}
  \item $\fnpre{f}(\frac{1}{10})$
  \item Alle Urbilder von $\frac{1}{10}$ unter $f$.
  \item $\fnpre{g}(2)$
  \item $\fnpre{g}(\set{0,2})$
  \end{enumerate}
\end{exercise}

\solutionsection

\begin{solution}
  Bei jeder Definitionen dieser Aufgabe überzeugt man sich leicht,
  dass es sich dabei tatsächlich um eine Funktion handelt,
  d.h. dass die Abbildungsvorschrift zu den beiden angegebenen Mengen passt.
  Wir erwähnen das nicht bei jeder der Teilaufgaben erneut.
  \begin{enumerate}
  \item Es ist $f$ nicht injektiv,
    denn es gilt $f(-1) = \frac{1}{(-1)^2}+1 = \frac{1}{1}+1 = 2$ und ebenso $f(1) = \frac{1}{1}+1 = 2$.
  \item Definiere $\ffnx{\tig}{\RR}{\RR}{y \mapsto \frac{y+8}{9}}$.
    Dies ist eine sinnvolle Definition,
    denn bei der Abbildungsvorschrift von $\tig$ kommen nur reelle Zahlen heraus
    (also ist die Menge $\RR$ rechts vom Pfeil passend zur Domain und Abbildungsvorschrift gewählt).
    Es gilt für alle $x, y \in \RR$:
    \begin{IEEEeqnarray*}{0l}
      \tig(g(x)) = \frac{g(x)+8}{9} = \frac{9x-8+8}{9} = \frac{9x}{9} = x\\
      g(\tig(y)) = 9 \tig(y) - 8 = 9 \frac{y+8}{9} - 8 = y + 8 - 8 = y
    \end{IEEEeqnarray*}
    Nach \autoref{grund/fun:satz-umkehr} ist somit $\tig = g^{-1}$, und insbesondere ist $g$ injektiv.
    \par
    \textit{Bemerkung.}
    Auf dieses $\tig$ kommt man, indem man in einer Nebenrechnung schreibt \enquote{$9x-8 = y$}
    und dies nach $x$ auflöst nach den üblichen Rechenregeln, die aus der Schule bekannt sind.
    Wir werden diese Rechenregeln in \autoref{grund/rl} noch genauer wiederholen.
  \item Es ist $h$ injektiv, und es gilt $\ffnx{h^{-1}}{\set{7}}{\RR}{x \mapsto 1}$.
  \item Da für alle $x > 0$ gilt $\frac{1}{x} > 0$, haben wir $\frac{1}{x} - 1 > -1$,
    also $\ffn{\phi}{\RRpos}{\intoi{{-1}}}$.
    Definiere $\ffnx{\tiphi}{\intoi{{-1}}}{\RRpos}{y \mapsto \frac{1}{y+1}}$.
    Dies ist eine sinnvolle Definition, denn für alle $y \in \intoi{{-1}}$ gilt $y + 1 > 0$,
    also ist $\frac{1}{y+1}$ definiert und außerdem Element von $\RRpos$.
    Es gilt für alle $x \in \RRpos$ und alle $y \in \intoi{{-1}}$:
    \begin{IEEEeqnarray*}{0l}
      \tiphi(\phi(x)) = \frac{1}{h(x) + 1} = \frac{1}{\frac{1}{x} - 1 + 1} = \frac{1}{\,\frac{1}{x}\,} = x \\
      \phi(\tiphi(y)) = \frac{1}{\tih(y)} - 1 = \frac{1}{\,\frac{1}{y+1}\,} - 1 = y + 1 - 1 = y
    \end{IEEEeqnarray*}
    Nach \autoref{grund/fun:satz-umkehr} ist somit $\tiphi = \phi^{-1}$, und insbesondere ist $\phi$ injektiv.
    \par
    \textit{Bemerkung.}
    Auf dieses $\tiphi$ kommt man, indem man in einer Nebenrechnung schreibt \enquote{$\frac{1}{x} - 1 = y$}
    und dies nach $x$ auflöst.
    Für die dabei anzuwendenden Rechenregeln gilt dieselbe Bemerkung wie oben.
  \item Es ist $\psi$ nicht injektiv,
    denn es gilt $\psi(1,0) = 1$ und $\psi(0,\frac{1}{2}) = 1$.
  \end{enumerate}
\end{solution}

\begin{solution}
  Es gilt:
  \begin{enumerate}
  \item $(f+g)(2) = 6$
  \item $(f+g+\phi)(2) = 15$
  \item $(\phi \cdot g)(2) = 18$
  \item $(f \circ \psi)(10) = \frac{1}{100}$
  \item $(10 \cdot \psi)(5) = 2$
  \item $(f \circ \phi \circ \psi)(10) = \frac{1}{4}$
  \item $(g \circ f \circ \phi \circ \psi)(10) = \frac{9}{8}$
  \item $(\psi \circ \psi)(2321) = 2321$
  \item $(f \circ g)(10) = 36$
  \item $(g \circ f)(10) = 51$
  \end{enumerate}
\end{solution}

\begin{solution}
  \begin{enumerate}
  \item Für alle $t \in \RR$ und alle $y \in \RRnz$ gilt:
    \begin{IEEEeqnarray*}{0l}
      f(t) = y \iff \frac{1}{t^{2}+1} = y
      \iff t^{2}+1 = \frac{1}{y}
      \iff t^{2} = \frac{1}{y} - 1
    \end{IEEEeqnarray*}
    Also gilt:
    \begin{IEEEeqnarray*}{0l}
      f(t) = \frac{1}{10} \iff t^{2} = 10 - 1 = 9 \iff t \in \set{-3,3}
    \end{IEEEeqnarray*}
    Es folgt $\fnpre{f}(\frac{1}{10}) = \set{-3,3}$.
  \item Das ist die vorige Teilaufgabe, nur anders formuliert.
    Somit ist nichts zu tun, wir wissen schon:
    Die Menge aller Urbilder von $\frac{1}{10}$ unter $f$ ist $\set{-3,3}$.
  \item Für alle $x \in \RR \setminus \set{1}$ und alle $y \in \RR \setminus \set{7}$ gilt:
    \begin{IEEEeqnarray*}{0l}
      f(x) = y \iff \frac{7x+1}{x-1} = y
      \iff 7x+1 = y x - y
      \iff 7x - y x = - y - 1 \\
      \eqin \iff (7 - y) x = - y - 1
      \iff x = \frac{- y - 1}{7 - y} = \frac{y+1}{y-7}
    \end{IEEEeqnarray*}
    Es folgt:
    \begin{IEEEeqnarray*}{0l}
      f(x) \in \set{0,2} \iff x \in \set{\frac{0+1}{0-7}, \frac{2+1}{2-7}} = \set{-\frac{1}{7}, -\frac{3}{5}}
    \end{IEEEeqnarray*}
    Die gesuchte Menge ist also $\set{-\frac{1}{7}, -\frac{3}{5}}$.
  \end{enumerate}
\end{solution}
