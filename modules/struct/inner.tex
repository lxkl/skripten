\chapter{Inneres Produkt}
\label{struct/inner}

Wir bleiben bei Vektorräumen über $\RR$ oder $\CC$.
Ein inneres Produkt ordnet je zwei Vektoren ein Element aus dem Körper zu,
unter Einhaltung bestimmter Eigenschaften.
Mit einem inneren Produkt kann man eine Norm konstruieren,
und noch vieles mehr machen.

\section*{Generalvoraussetzung}

Es sei in diesem Kapitel stets
$\KK \in \set{\RR, \CC}$.

\section{Definition und einfache Eigenschaften}

\begin{para}[Inneres Produkt, Prähilbertraum]
  Es sei $V$ ein Vektorraum über $\KK$.
  Es sei $\ffn{\inner{\ccdot}{\ccdot}}{V \times V}{\KK}$ so, dass gilt:
  \begin{enumerate}
  \item $\inner{v}{v} > 0$, d.h. $\inner{v}{v} \in \RRpos$, für alle $v \in \nz{V}$
    (\term{positive Definitheit}).
  \item $\inner{v}{w} = \conj{\inner{w}{v}}$ für alle $v,w \in V$ (\term{konjugierte Symmetrie}
    oder \term{Hermite\-/Symmetrie}\footnote{%
      Charles Hermite, Französischer Mathematiker, 1822--1901.}).
    Wenn $\KK = \RR$, dann haben wir hier offenbar nicht nur eine konjugierte Symmetrie,
    sondern Symmetrie.
  \item Linearität in der ersten Komponente, d.h.:
    \begin{IEEEeqnarray*}{0l+s*}
      \inner{v+v'}{w} = \inner{v}{w} + \inner{v'}{w} \quad \forall v,v',w \in V
      & \term{(Additivität in erster Komp.)} \\
      \inner{\lam v}{w} = \lam \inner{v}{w} \quad \forall \lam \in \KK \quad \forall v \in V
      & \term{(Homogenität in erster Komp.)}
    \end{IEEEeqnarray*}
  \end{enumerate}
  Dann nennen wir $\inner{\ccdot}{\ccdot}$ ein \term{inneres Produkt} oder \term{Skalarprodukt}\footnote{%
    Nicht zu verwechseln mit der Skalarmultiplikation zwischen einem Element aus $\KK$
    und einem Element aus $V$!}
  auf $V$,
  und $(V, \inner{\ccdot}{\ccdot})$ nennen wie einen \term{Prähilbertraum} oder einen \term{Skalarproduktraum}.
\end{para}

\begin{satz}[Rechenregeln]
  Es sei $(V,\innerfn)$ ein Prähilbertraum.
  Für alle ${\xv,\xv',\xw,\xw' \in V}$ und alle $\lam,\mu \in \KK$ gilt:
  \begin{enumerate}
  \item $\inner{\lam \xv + \mu \xv'}{\xw} = \lam \inner{\xv}{\xw} + \mu \inner{\xv'}{\xw}$
  \item $\inner{\xv}{\lam \xw + \mu \xw'} = \conj{\lam} \inner{\xv}{\xw} + \conj{\mu} \inner{\xv}{\xw'}$
  \item $\inner{\xv}{\xw + \xw'} = \inner{\xv}{\xw} + \inner{\xv}{\xw'}$
  \item $\inner{\xv}{\lam \xw} = \conj{\lam} \inner{\xv}{\xw}$
  \item $\inner{0}{\xv} = \inner{\xv}{0} = 0$
  \item $\inner{\xv}{\xw} + \inner{\xw}{\xv} = 2 \Re \inner{\xv}{\xw}$
  \item $\inner{\xv}{\xv} \geq 0$
  \item $\inner{\xv}{\xv} = 0 \iff \xv = 0$
  \end{enumerate}
\end{satz}

\begin{proof}
  \begin{enumerate}
  \item Folgt direkt aus der Linearität in der ersten Komponente.
  \item Es gilt:
    \begin{IEEEeqnarray*}{0l}
      \inner{\xv}{\lam \xw + \mu \xw'}
      = \conj{\inner{\lam \xw + \mu \xw'}{\xv}}
      = \conj{\lam \inner{\xw}{\xv} + \mu \inner{\xw'}{\xv}} \\*
      \eqin = \conj{\lam} \conj{\inner{\xw}{\xv}} + \conj{\mu} \conj{\inner{\xw'}{\xv}}
      = \conj{\lam} \inner{\xv}{\xw} + \conj{\mu} \inner{\xv}{\xw'}
    \end{IEEEeqnarray*}
  \item Folgt aus vorigem Punkt mit $\lam = \mu = 1$.
  \item Ebenso, mit $\mu = 0$.
  \item Es gilt $\inner{0}{\xv} = \inner{0\xv}{\xv} = 0 \inner{\xv}{\xv} = 0$
    und $\inner{\xv}{0} = \inner{\xv}{0\xv} = \conj{0} \inner{\xv}{\xv} = 0 \inner{\xv}{\xv} = 0$.
  \item Es gilt $\inner{\xv}{\xw} + \inner{\xw}{\xv} = \inner{\xv}{\xw} + \conj{\inner{\xv}{\xw}}
    = 2 \Re \inner{\xv}{\xw}$.
  \item Nach Definition des inneren Produktes ist $\inner{\xv}{\xv} > 0 \geq 0$ wenn $\xv \neq 0$.
    Wenn $\xv=0$, dann ist nach einem vorigen Punkt dieses Satzes $\inner{\xv}{\xv} = 0 \geq 0$.
  \item $\implies$: Sieht man leicht mit Kontraposition und der Definition des inneren Produktes.
    \par
    $\impliedby$: Folgt aus einem vorigen Punkt dieses Satzes.
    \qedhere
  \end{enumerate}
\end{proof}

\begin{satz}[Wirken auf Basis maßgeblich]
  \label{struct/inner:gram}
  Es sei $(V,\innerfn)$ ein Prähilbertraum
  und $B \in V^{n}$ eine Basis von~$V$.
  Dann ist $\innerfn$ bereits durch das Wirken auf $B$ festgelegt,
  d.h. wir kennen $\innerfn$ bereits
  wenn wir die Zahlen $\inner{B_{i}}{B_{j}}$ für alle $i,j \in \setn{n}$ kennen.
\end{satz}

\begin{proof}
  Es seien $v, w \in V$.
  Dann gibt es $\lam, \mu \in \KK^{n}$ mit $v = \sum_{i=1}^{n} \lam_{i} B_{i}$
  und ${w = \sum_{j=1}^{n} \mu_{j} B_{j}}$. Es folgt:
  \begin{IEEEeqnarray*}{0l+x*}
    \inner{v}{w} = \innern{\sum_{i=1}^{n} \lam_{i} B_{i}}{\sum_{j=1}^{n} \mu_{j} B_{j}}
    = \sum_{i=1}^{n} \lam_{i} \innern{B_{i}}{\sum_{j=1}^{n} \mu_{j} B_{j}} \\
    \eqin = \sum_{i=1}^{n} \lam_{i} \sum_{j=1}^{n} \conj{\mu_{j}} \inner{B_{i}}{B_{j}}
    = \sum_{i,j=1}^{n} \lam_{i} \conj{\mu_{j}} \inner{B_{i}}{B_{j}} & \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\begin{para}[Gram\-/Matrix]
  Es sei $(V,\innerfn)$ ein Prähilbertraum und ${S \in V^{n}}$.
  Dann nennen wir
  \begin{IEEEeqnarray*}{0l}
    \begin{bmatrix}
      \inner{S_{1}}{S_{1}} & \hdots & \inner{S_{1}}{S_{n}} \\
      \vdots && \vdots \\
      \inner{S_{n}}{S_{1}} & \hdots & \inner{S_{n}}{S_{n}}
    \end{bmatrix}
  \end{IEEEeqnarray*}
  die \term{Gram\-/Matrix}\footnote{%
    Jørgen Pedersen Gram, Dänischer Mathematiker, 1850--1916.}
  von $\innerfn$ bezüglich $S$.
  Ist $S$ eine Basis von $V$,
  dann sagt obiger Satz also aus, dass $\innerfn$
  bereits über seine Gram\-/Matrix bezüglich $S$ festgelegt ist.
\end{para}

\begin{para}[Standardskalarprodukt]
  Definiere das \term{Standardskalarprodukt} auf $\KK^{n}$:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{\bigcdot}{\KK^{n} \times \KK^{n}}{\KK}{(v,w) \mapsto \sum_{i=1}^n v_i \conj{w_i}}
  \end{IEEEeqnarray*}
  Die Notation findet in der Praxis natürlich infix statt,
  also wir schreiben $v \bigcdot w$ anstelle von $\bigcdot(v,w)$.
\end{para}

\begin{satz}
  Das Standardskalarprodukt ist tatsächlich ein Skalarprodukt,
  d.h. inneres Produkt, auf $\KK^{n}$.
\end{satz}

\begin{proof}
  Übung.
\end{proof}

\section{Induzierte Norm}

\begin{para}[Induzierte Norm]
  Es gibt einen fundamentalen Zusammenhang zwischen einem inneren Produkt und dem Normbegriff.
  Ist nämlich $\innerfn$ ein inneres Produkt auf dem Vektorraum $V$ über $\KK$,
  dann kann man zeigen, dass Folgendes eine Norm auf $V$ ist:
  \begin{IEEEeqnarray}{0l}
    \label{struct/inner:inner-norm}
    \fnx{V}{\RRnn}{v \mapsto \sqrt{\inner{v}{v}}}
  \end{IEEEeqnarray}
  Wir nennen dies die von $\innerfn$ \term{induzierte} Norm.
  Die ersten beiden Normeigenschaften sind einfach einzusehen;
  erledigen Sie dies gerne an dieser Stelle zur Übung, auch wenn wir die Beweise später angeben.
  Die Dreiecksungleichung macht etwas mehr Arbeit, was wir jetzt vorbereiten.
  Hilfreich sind folgende beiden Sätze.
\end{para}

\begin{satz}[Pythagoras]
  Es sei $(V,\innerfn)$ ein Prähilbertraum und $\normfn$ die von $\innerfn$ induzierte Norm.\footnote{%
    Wir verwenden diese Bezeichnung, auch wenn wir noch nicht bewiesen haben,
    dass es wirklich eine Norm ist.}
  Dann gilt für alle $\xv,\xw \in V$:
  \begin{IEEEeqnarray*}{0l}
    \norm{\xv+\xw}^{2} = \norm{\xv}^{2} + \norm{\xw}^{2} + 2 \Re \inner{\xv}{\xw}
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}
  Es gilt:
  \begin{IEEEeqnarray*}{0l+x*}
    \norm{\xv+\xw}^{2} = \inner{\xv}{\xv+\xw} + \inner{\xw}{\xv+\xw} \\*
    \eqin = \inner{\xv}{\xv} + \inner{\xv}{\xw} + \inner{\xw}{\xv} + \inner{\xw}{\xw}
    = \norm{\xv}^{2} + \norm{\xw}^{2} + 2 \Re \inner{\xv}{\xw} & \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\begin{satz}[Cauchy-Schwarz-Ungleichung]
  Es sei $(V,\innerfn)$ ein Prähilbertraum und $\normfn$ die von $\innerfn$ induzierte Norm.
  Dann gilt für alle $\xv,\xw \in V$:
  \begin{IEEEeqnarray*}{0l}
    \abs{\inner{\xv}{\xw}} \leq \norm{\xv} \ccdot \norm{\xw}
  \end{IEEEeqnarray*}
  Ferner liegt $=$ genau dann vor, wenn es $\lam \in \KK$ mit $\xv = \lam \xw$ gibt
  oder $\xw = 0$ gilt.
\end{satz}

\begin{proof}
  Der Fall $\xw=0$ ist klar, denn dann sind beide Seiten~$0$.
  Nun sei $\xw \neq 0$ und es gebe $\lam \in \KK$ mit $\xv = \lam \xw$.
  Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    \abs{\inner{\xv}{\xw}}
    = \abs{\inner{\lam \xw}{\xw}}
    = \abs{\lam} \ccdot \abs{\inner{\xw}{\xw}}
    = \abs{\lam} \ccdot \norm{\xw}^{2} \\
    \eqin = \abs{\lam} \ccdot \norm{\xw} \ccdot \norm{\xw}
    = \norm{\lam \xw} \ccdot \norm{\xw}
    = \norm{\xv} \ccdot \norm{\xw}
  \end{IEEEeqnarray*}
  Also gilt auch in diesem Falle die Gleichheit.
  \par
  Nun sei $\xw \neq 0$ und für alle $\lam \in \KK$ gelte ${\xv - \lam \xw \neq 0}$.
  Es folgt alle $\lam \in \KK$:
  \begin{IEEEeqnarray*}{0l}
    0 < \innern{\xv - \lam \xw}{\xv - \lam \xw} \\
    \eqin = \innern{\xv}{\xv - \lam \xw} - \lam \innern{\xw}{\xv - \lam \xw} \\
    \eqin = \inner{\xv}{\xv} - \conj{\lam} \inner{\xv}{\xw}
    - \lam \inner{\xw}{\xv} + \lam \conj{\lam} \inner{\xw}{\xw} \\
    \eqin = \norm{\xv}^{2} - (\conj{\lam} \inner{\xv}{\xw}
    + \conj{\conj{\lam} \inner{\xv}{\xw}}) + \lam \conj{\lam} \inner{\xw}{\xw} \\
    \eqin = \norm{\xv}^{2} - 2 \Re\p{\conj{\lam} \inner{\xv}{\xw}}
    + \lam \conj{\lam} \inner{\xw}{\xw}
  \end{IEEEeqnarray*}
  Dies nutzen wir aus für $\lam \df \frac{\inner{\xv}{\xw}}{\inner{\xw}{\xw}}$;
  beachte dass $\conj{\lam} = \frac{\conj{\inner{\xv}{\xw}}}{\inner{\xw}{\xw}}$ gilt:
  \begin{IEEEeqnarray*}{0l}
    0 < \norm{\xv}^{2} - 2 \Re\p{\frac{\conj{\inner{\xv}{\xw}}}{\inner{\xw}{\xw}} \inner{\xv}{\xw}}
    + \frac{\inner{\xv}{\xw}}{\inner{\xw}{\xw}}
    \frac{\conj{\inner{\xv}{\xw}}}{\inner{\xw}{\xw}} \inner{\xw}{\xw} \\*
    \eqin = \norm{\xv}^{2}
    - 2 \Re\p{\underbrace{\frac{\abs{\inner{\xv}{\xw}}^{2}}{\norm{\xw}^{2}}}_{\in \RR}}
    + \frac{\abs{\inner{\xv}{\xw}}^{2}}{\norm{\xw}^{2}}
    = \norm{\xv}^{2} - \frac{\abs{\inner{\xv}{\xw}}^{2}}{\norm{\xw}^{2}}
  \end{IEEEeqnarray*}
  Die Behauptung folgt durch Umstellen obiger Abschätzung;
  die Quadrate fallen weg, da alle quadrierten Zahlen reell und nicht\-/negativ sind.
\end{proof}

\begin{satz}
  Die induzierte Norm ist tatsächlich eine Norm.
\end{satz}

\begin{proof}
  Es sei $(V,\innerfn)$ ein Prähilbertraum und $\normfn$ die von $\innerfn$ induzierte Norm.
  Wir zeigen die drei Normeigenschaften.
  \begin{enumerate}
  \item Für alle $\xv \in V$ gilt mit der strengen Monotonie der Wurzel:
    \begin{IEEEeqnarray*}{0l}
      \norm{\xv} = 0
      \iff \inner{\xv}{\xv} = 0
      \iff \xv = 0
    \end{IEEEeqnarray*}
  \item Für alle $\lam \in \KK$ und alle $\xv \in V$ gilt:
    \begin{IEEEeqnarray*}{0l}
      \norm{\lam \xv} = \sqrt{\inner{\lam \xv}{\lam \xv}}
      = \sqrt{\lam \inner{\xv}{\lam \xv}}
      = \sqrt{\lam \conj{\lam} \inner{\xv}{\xv}}
      = \sqrt{\abs{\lam}^2 \inner{\xv}{\xv}}
      = \abs{\lam} \ccdot \norm{\xv}
    \end{IEEEeqnarray*}
  \item Für alle $\xv,\xw \in V$ gilt mit Pythagoras und der Cauchy\-/Schwarz\-/Ungleichung:
    \begin{IEEEeqnarray*}{0l+x*}
      \norm{\xv+\xw}^{2} = \norm{\xv}^{2} + \norm{\xw}^{2} + 2 \Re \inner{\xv}{\xw}
      \leq \norm{\xv}^{2} + \norm{\xw}^{2} + 2 \abs{\inner{\xv}{\xw}} \\
      \eqin \leq \norm{\xv}^{2} + \norm{\xw}^{2} + 2 \ccdot \norm{\xv} \ccdot \norm{\xw}
      = (\norm{\xv} + \norm{\xw})^{2}
      & \qedhere
    \end{IEEEeqnarray*}
  \end{enumerate}
\end{proof}

\begin{para}[Euklidische Norm]
  Wir nennen
  \begin{IEEEeqnarray*}{0l}
    \ffnx{\normfn_{2}}{\KK^{n}}{\RRnn}{v \mapsto \sqrt{\sum_{i=1}^{n} \abs{v_{i}}^{2}}}
  \end{IEEEeqnarray*}
  die \term{Euklidische Norm} oder \term{$2$\=/Norm} auf $\KK^{n}$.
  Es ist dies die vom Standardskalarprodukt induzierte Norm,
  da $\norm{z}^{2} = z \conj{z}$ für alle $z \in \KK$ gilt.
  Erhalte somit:
\end{para}

\begin{corollary}
  Es ist $(\KK^{n}, \normfn_{2})$ ein normierter Raum.
\end{corollary}

\section{Orthogonalität}

Es sei in diesem Abschnitt stets $(V,\innerfn)$ ein Prähilbertraum über $\KK$
und $\normfn$ die von $\innerfn$ induzierte Norm.

\begin{para}[Definitionen]
  Für alle $\xv,\xw \in V$
  nennen wir $\xv$ \term{orthogonal} oder \term{senkrecht} zu~$\xw$,
  in Zeichen $\xv \perp \xw$, wenn $\inner{\xv}{\xw} = 0$ gilt.
  Offenbar gilt $\xv \perp \xw \iff \xw \perp \xv$.
  Für alle $M \subseteq V$ schreiben wir $\xv \perp M$ wenn
  $\xv \perp \xw$ für alle $\xw \in M$ gilt.
  \par
  Es sei $\emptyset \neq S \subseteq \nz{V}$ endlich.
  Wir nennen $S$ \dots
  \begin{itemize}
  \item ein \term{Orthogonalsystem (OGS)}, wenn $\xv \perp \xw$ für alle $\xv,\xw \in S$ mit $\xv \neq \xw$ gilt;
  \item ein \term{Orthonormalsystem (ONS)}, wenn $S$ ein Orthogonalsystem ist
    und außerdem $\norm{\xv} = 1$ für alle $\xv \in S$ gilt;
  \item eine \term{Orthonormalbasis (ONB)} von $V$, wenn $S$ ein ONS ist und eine Basis von $V$.
  \end{itemize}
  Offenbar ist die Standardbasis von $\KK^{n}$ eine ONB des $\KK^{n}$ bezüglich des Standardskalarproduktes.
\end{para}

\begin{satz}[Eigenschaften von OGS, ONS, ONB]
  \hfill
  \fakephantomsection\label{struct/inner:orth}
  \begin{enumerate}
  \item Jedes OGS ist linear unabhängig.\footnote{%
      Achtung: In der Literatur wird beim OGS manchmal der Nullvektor zugelassen.
      Dann stimmt diese Aussage nicht, sondern man muss dafür zusätzlich voraussetzen,
      dass der Nullvektor nicht Element des OGS ist.}
  \item Es sei $S \subseteq V$ ein OGS.
    Dann gilt $\norm{\sum_{x \in S} x}^{2} = \sum_{x \in S} \norm{x}^{2}$.
  \item Es sei $N \subseteq V$ ein ONS und $v = \sum_{x \in N} \lam(x) x$
    eine Linearkombination von $N$ mit $\ffn{\lam}{N}{\KK}$.
    Dann gilt $\norm{v}^{2} = \sum_{x \in N} \abs{\lam(x)}^{2}$.
  \item\label{struct/inner:orth:koord} Es sei $B \subseteq V$ eine ONB und $v \in V$.
    Dann gilt $v = \sum_{x \in B} \inner{v}{x} x$.
  \end{enumerate}
\end{satz}

\begin{proof}
  \begin{enumerate}
  \item Es sei $S \subseteq V$ ein OGS und $\ffn{\lam}{S}{\KK}$ mit $\sum_{x \in S} \lam(x) x = 0$.
    Dann gilt für alle $y \in S$:
    \begin{IEEEeqnarray*}{0l}
      0 = \inner{0}{y} = \innern{\sum_{x \in S} \lam(x) x}{y}
      = \sum_{x \in S} \lam(x) \inner{x}{y}
      = \lam(y) \inner{y}{y}
    \end{IEEEeqnarray*}
    Da $y \neq 0$, gilt $\inner{y}{y} \neq 0$, also $\lam(y) = 0$.
  \item Wir führen Induktion über $\card{S}$.
    Die Aussage stimmt offenbar, wenn ${\card{S} = 1}$.
    Nun sei $\card{S} > 1$ und die Aussage gelte bereits für ONS von kleinerer Kardinalität.
    Es sei $y \in S$ und $S' \df S \setminus \set{y}$.
    Mit Pythagoras haben wir:
    \begin{IEEEeqnarray*}{0l}
      \norm{\sum_{x \in S} x}^{2}
      = \norm{y + \sum_{x \in S'} x}^{2}
      = \norm{y}^{2} + \norm{\sum_{x \in S'} x}^{2} + 2 \Re \inner{y}{\sum_{x \in S'} x} \\*
      \eqin = \norm{y}^{2} + \sum_{x \in S'} \norm{x}^{2}
      + 2 \Re \sum_{x \in S'} \underbrace{\inner{y}{x}}_{=0}
      = \sum_{x \in S} \norm{x}^{2}
    \end{IEEEeqnarray*}
  \item Mit \autoref{struct/inner:gram} gilt:
    \begin{IEEEeqnarray*}{0l}
      \norm{v}^{2} = \sum_{x,y \in N} \lam(x) \conj{\lam(y)} \inner{x}{y}
      = \sum_{x,y \in N} \lam(x) \conj{\lam(y)}
      \begin{cases}
        1 & x=y \\
        0 & \text{sonst}
      \end{cases} \\
      \eqin = \sum_{x \in N} \lam(x) \conj{\lam(x)}
      = \sum_{x \in N} \abs{\lam(x)}^{2}
    \end{IEEEeqnarray*}
  \item Da $B$ eine Basis ist, gibt es $\ffn{\lam}{B}{\KK}$ mit $v = \sum_{x \in B} \lam(x) x$.
    Dann gilt für alle $y \in B$:
    \begin{IEEEeqnarray*}{0l+x*}
      \inner{v}{y}
      = \innern{\sum_{x \in B} \lam(x) x}{y}
      = \sum_{x \in B} \lam(x) \inner{x}{y} = \lam(y)
      & \qedhere
    \end{IEEEeqnarray*}
  \end{enumerate}
\end{proof}

\begin{para}[Orthogonale Projektion]
  Es sei $W \leq V$ und $B$ eine ONB von $W$.
  Definiere:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{P_{W}}{V}{W}{v \mapsto \sum_{x \in B} \inner{v}{x} x}
  \end{IEEEeqnarray*}
  Wir nennen $P_{W}$ die \term{orthogonale Projektion} auf $W$.
  Zunächst könnte $P_{W}$ von der Wahl von $B$ abhängen.
  Im nächsten Satz (Approximationssatz) werden wir aber sehen, dass wir dieselbe Funktion $P_{W}$ bekommen,
  egal welche ONB von $W$ wir zur Definition von $P_{W}$ heranziehen.
  \par
  Es ist nicht verboten, dass $v$ in $W$ liegt.
  Dann gilt nach obigem Satz $P_{W}(v) = v$.
\end{para}

\begin{satz}[Approximationssatz]
  Es sei $W \leq V$ und $B$ eine ONB von $W$ und $P_{W}$ entsprechend wie im obigen Absatz definiert.
  Es sei $v \in V$. Dann gilt:
  \begin{enumerate}
  \item $(v-P_{W}(v)) \perp W$
  \item $\norm{v - P_{W}(v)} < \norm{v-w}$ für alle $w \in W \setminus \set{P_{W}(v)}$,
    d.h. es gibt in $W$ keinen Vektor, der im Sinne der induzierten Norm
    genauso nahe  bei $v$ liegt wie $P_{W}(v)$
    oder der näher bei $v$ liegt als $P_{W}(v)$.
    Daraus folgt insbesondere die versprochene Unabhängigkeit von $P_{W}$ von der ONB.
  \item $\norm{v - P_{W}(v)}^{2} = \norm{v}^{2} - \sum_{x \in B} \abs{\inner{v}{x}}^{2}$
  \end{enumerate}
\end{satz}

\begin{proof}
  \begin{enumerate}
  \item Man sieht leicht (Übung), dass es genügt, $(v-P_{W}(v)) \perp x$ für alle $x \in B$ zu zeigen,
    da $B$ eine Basis von $W$ ist.
    Für alle $x \in B$ gilt:
    \begin{IEEEeqnarray*}{0l}
      \innern{v-P_{W}(v)}{x}
      = \inner{v}{x} - \innern{\sum_{y \in B} \inner{v}{y} y}{x}
      = \inner{v}{x} - \sum_{y \in B} \inner{v}{y} \inner{y}{x} \\
      \eqin = \inner{v}{x} - \inner{v}{x} \inner{x}{x}
      = \inner{v}{x} - \inner{v}{x} = 0
    \end{IEEEeqnarray*}
  \item Es sei $w \in W \setminus \set{P_{W}(v)}$, also $w - P_{W}(v) \neq 0$
    und damit $\norm{w - P_{W}(v)} > 0$.
    Nach dem vorigen Punkt ist $(v-P_{W}(v)) \perp (P_{W}(v)-w)$.
    Es folgt mit Pythagoras:
    \begin{IEEEeqnarray*}{0l}
      \norm{v-w}^{2} = \norm{(v-P_{W}(v)) + (P_{W}(v)-w)}^{2} \\
      \eqin = \norm{v-P_{W}(v)}^{2} + \norm{P_{W}(v)-w}^{2}
      > \norm{v-P_{W}(v)}^{2}
    \end{IEEEeqnarray*}
  \item Es gilt:
    \begin{IEEEeqnarray*}{0l+x*}
      \norm{v - P_{W}(v)}^{2}
      = \innernn[big]{v - \sum_{x \in B} \inner{v}{x} x}{v - \sum_{y \in B} \inner{v}{y} y} \\
      \eqin = \innernn[big]{v}{v - \sum_{y \in B} \inner{v}{y} y}
      \; - \; \sum_{x \in B} \inner{v}{x} \innernn[big]{x}{v - \sum_{y \in B} \inner{v}{y} y} \\
      \eqin = \inner{v}{v} - \sum_{y \in B} \conj{\inner{v}{y}} \inner{v}{y} \\
      \eqin \phantom{=} \; - \; \sum_{x \in B} \inner{v}{x} \inner{x}{v}
      + \sum_{x \in B} \inner{v}{x} \sum_{y \in B} \conj{\inner{v}{y}} \inner{x}{y} \\
      \eqin = \norm{v}^{2} - \sum_{y \in B} \abs{\inner{v}{y}}^{2}
      \; - \; \sum_{x \in B} \inner{v}{x} \inner{x}{v}
      + \sum_{x \in B} \inner{v}{x} \conj{\inner{v}{x}} \\
      \eqin = \norm{v}^{2} - \sum_{y \in B} \abs{\inner{v}{y}}^{2}
      & \qedhere
    \end{IEEEeqnarray*}
  \end{enumerate}
\end{proof}

Die Berechnung der orthogonalen Projektion setzt die Kenntnis einer ONB für den relevanten Subraum voraus.
Daher stellt sich die Frage, wie man eine ONB praktisch berechnen kann.
Der folgende Satz liefert dafür eine Methode.

\begin{satz}[Orthonormalisierung nach Gram und Schmidt\protect\footnote{%
    Erhard Schmidt, deutscher Mathematiker, 1876--1959.}]
  Es sei $\seti{v_{i}}{n} \subseteq V$ linear unabhängig der Kardinalität~$n$.
  Definiere $\eli{x}{n} \in V$ rekursiv durch:
  \begin{IEEEeqnarray*}{0l+l+l}
    x_{t} \df \frac{z_{t}}{\norm{z_{t}}} &
    z_{t} \df v_{t} - \sum_{k=1}^{t-1} \inner{v_{t}}{x_{k}} x_{k} &
    (t = 1,\hdots,n)
  \end{IEEEeqnarray*}
  Dann ist $\seti{x_{i}}{n}$ eine ONB von $\spann(\seti{v_{i}}{n})$.
\end{satz}

\begin{proof}
  Für alle $t \in \setn{n}$ definiere $V_{t} \df \seti{v_{i}}{t}$
  und $X_{t} \df \seti{x_{i}}{t}$.
  Wir zeigen per Induktion, dass für alle $t \in \setn{n}$ gilt:
  $X_{t}$ ist eine ONB von $\spann(V_{t})$.
  Der Induktionsanfang $t=1$ ist klar, da $x_{1} = \frac{v_{1}}{\norm{v_{1}}}$.
  \par
  Es sei nun $t > 1$,
  und es gelte bereits, dass $X_{t-1}$ eine ONB von ${W \df \spann(V_{t-1})}$ ist.
  Bemerke $z_{t} = v_{t} - P_{W}(v_{t})$.
  Zunächst ist $z_{t} \neq 0$,
  denn sonst wäre ${v_{t} = P_{W}(v_{t}) \in W}$, ein Widerspruch dazu,
  dass $V_{t}$ linear unabhängig ist.
  Also ist $x_{t}$ wohldefiniert und $\norm{x_{t}} = 1$.
  Weiter folgt $x_{t} \perp W$ aus dem Approximationssatz,
  insbesondere also $x_{t} \perp x_{i}$ für alle $i \in \setn{t-1}$.
  Also ist $X_{t}$ ein ONS.
  \par
  Zu zeigen bleibt $\spann(X_{t}) = \spann(V_{t})$.
  Wir wissen $\spann(X_{t-1}) = \spann(V_{t-1})$.
  Es gilt:
  \begin{IEEEeqnarray*}{0l}
    x_{t} \in \spann(\set{v_{t}} \cup X_{t-1})
    \subseteq \spann(\set{v_{t}} \cup V_{t-1})
    = \spann(V_{t})
  \end{IEEEeqnarray*}
  Es folgt $\spann(X_{t}) \leq \spann(V_{t})$.
  Aus Dimensionsgründen,
  nämlich weil
  \begin{IEEEeqnarray*}{0l}
    \dim(\spann(X_{t})) = \dim(\spann(V_{t}))
  \end{IEEEeqnarray*}
  gilt, bedeutet das Gleichheit.
\end{proof}
