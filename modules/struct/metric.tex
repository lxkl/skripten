\chapter{Metrische Räume}
\label{struct/metric}

Bislang haben wir Analysis nur in $\RR$ betrieben.
Viele der analytischen Begriffe lassen sich sinnvoll auf allgemeinere Strukturen übertragen,
die keineswegs angeordnete Körper zu sein brauchen.
In diesem Kapitel beginnen wir mit einer sehr allgemeinen Struktur,
nämlich den sogenannten metrischen Räumen.
Im nächsten Kapitel wird es wieder etwas spezieller,
wenn wir Vektorräume über $\RR$ oder $\CC$ betrachten;
dort werden wir uns auf die Ergebnisse über metrische Räume stützen.

\section{Definition}

\begin{para}[Metrik, metrischer Raum]
  Es sei $M$ eine Menge.
  Eine Funktion
  \begin{IEEEeqnarray*}{0l}
    \ffn{d}{M \times M}{\RRnn}
  \end{IEEEeqnarray*}
  nennen wir eine \term{Metrik} auf $M$, wenn gilt:
  \begin{itemize}
  \item $d(x,y) = 0 \iff x = y$ für alle $x,y \in M$ (\term{positive Definitheit})
  \item $d(x,y) = d(y,x)$ für alle $x,y \in M$ (\term{Symmetrie})
  \item $d(x,y) \leq d(x,z) + d(z,y)$ für alle $x,y,z \in M$ (\term{Dreiecksungleichung})
  \end{itemize}
  Ist $d$ eine Metrik auf $M$, dann nennen wir $(M,d)$ einen \term{metrischen Raum}.
  Die Elemente von $M$ werden in diesem Kontext oft auch als \term{Punkte} bezeichnet.
  \par
  Oft gibt man einem metrischen Raum einen Namen, zum Beispiel $S$.
  Dann bezeichnen wir die Menge (die oben $M$ heißt) auch mit $S$
  und die Metrik mit~$d_{S}$.
  Das ist besonders dann nützlich, wenn man es mit mehreren metrischen Räumen zur Zeit zu tun hat.
\end{para}

\begin{example}
  Es sei $M$ eine Menge.
  Dann sieht man leicht (Übung), dass Folgendes jeweils eine Metrik auf $M$ ist:
  \begin{itemize}
  \item \term{Diskrete Metrik}:
    \begin{IEEEeqnarray*}{0l}
      \fnx{M \times M}{\RRnn}{%
        (x,y) \mapsto
        \begin{cases}
          0 & \text{wenn $x=y$} \\
          1 & \text{sonst}
        \end{cases}}
    \end{IEEEeqnarray*}
  \item Für alle $\ffn{f}{M}{\RR}$ injektiv die Funktion:
    \begin{IEEEeqnarray*}{0l}
      \fnx{M \times M}{\RRnn}{(x,y) \mapsto \abs{f(x) - f(y)}}
    \end{IEEEeqnarray*}
    Insbesondere gilt dies für $M = \RR$ und $f = \id_{\RR}$,
    was wir die \term{Betragsmetrik} auf $\RR$ nennen.
    Wenn wir $\RR$ an einer Stelle schreiben,
    wo ein metrischer Raum erwartet wird, dann meinen wir $\RR$ mit der Betragsmetrik,
    wenn nicht anders ausgewiesen.
    Ebenso verfahren wir mit $\CC$ und dem dortigen Betrag.
  \end{itemize}
\end{example}

\section{Umgebungen, \etc}

\begin{para}[Definitionen]
  Es sei $M$ ein metrischer Raum.
  Für alle $\veps > 0$ und alle $p \in M$ definiere die \term{offene Kugel} mit Radius $\veps$ um $p$
  in $M$, oder die \term{$\veps$\=/Kugel} um $p$ in $M$:
  \begin{IEEEeqnarray*}{0l}
    B_{M}(p,\veps) \df \set{x \in M \suchthat d_{M}(x,p) < \veps}
  \end{IEEEeqnarray*}
  Es sei $A \subseteq M$.
  \begin{itemize}
  \item Wir nennen $p \in A$ einen \term{inneren Punkt} von $A$ in $M$,
    wenn es $\veps > 0$ so gibt, dass $B_{M}(p,\veps) \subseteq A$ gilt.
  \item Wir nennen $A$ eine \term{Umgebung} von $p$ in $M$,
    wenn $p$ ein innerer Punkt von $A$ in $M$ ist.
    Die Menge aller solcher Umgebungen von $p$ bezeichnen wir mit $\nsets_{M}(p)$.
  \item Wir nennen $A$ \term{offen} in $M$,
    wenn alle Punkte von $A$ (d.h. alle Elemente von~$A$) innere Punkte von $A$ in $M$ sind;
    oder, äquivalent, wenn die Menge $A$ Umgebung jedes ihrer Punkte ist;
    oder, äquivalent, wenn gilt:
    \begin{IEEEeqnarray*}{0l}
      \forall p \in A \innerholds \exists \veps > 0 \holds B_{M}(p,\veps) \subseteq A
    \end{IEEEeqnarray*}
    Die Menge aller offenen Mengen in $M$ bezeichnen wir mit $\osets_{M}$.
  \item Wir nennen $A$ \term{abgeschlossen} in $M$,
    wenn $M \setminus A$ offen in $M$ ist.
    Die Menge aller in $M$ abgeschlossenen Mengen bezeichnen wir mit $\csets_{M}$.
  \end{itemize}
  Den Zusatz \enquote{in $M$} lässt man manchmal auch weg,
  wenn aus dem Kontext klar ist, welcher metrische Raum gemeint ist.
  Dies gilt generell bei Begriffen, die sich auf einen metrischen Raum beziehen.
\end{para}

\begin{example}[Offene und abgeschlossene Mengen]
  \hfill
  \begin{itemize}
  \item $\emptyset$ und $M$ sind jeweils offen und auch abgeschlossen in $M$.
  \item In $\RR$ (mit der Betragsmetrik) sind Intervalle der Form $\intoo{a}{b}$ mit $a,b \in \RRbar$ offen;
    und Intervalle der Form $\intcc{a}{b}$ mit $a,b \in \RR$ sind abgeschlossen.
    Es sind auch $\intci{a}$ und $\intic{a}$ mit $a \in \RR$ abgeschlossen.
    In der Literatur werden manchmal genau die Intervalle der Form
    $\intcc{a}{b}$ mit $a,b \in \RR$ als \term{geschlossen} (ohne \enquote{ab} davor) bezeichnet.
    Es gilt dann: Jedes geschlossene Intervall ist abgeschlossen,
    aber nicht jedes abgeschlossen Intervall ist geschlossen.
  \end{itemize}
\end{example}

\begin{satz}[Vereinigungen und Schnitte]
  Es sei $M$ ein metrischer Raum und $p \in M$. Dann gelten:
  \begin{enumerate}
  \item $\forall S \subseteq \osets_{M} \holds \bigcup S \in \osets_{M}$,
    d.h. die Vereinigung offener Mengen ist offen.
  \item $\forall S \subseteq \osets_{M} \with \text{$\emptyset \neq S$ endlich}
    \holds \bigcap S \in \osets_{M}$,
    d.h. der Schnitt endlich vieler offener Mengen ist offen.
  \item $\forall S \subseteq \nsets_{M}(p) \holds \bigcup S \in \nsets_{M}(p)$,
    d.h. die Vereinigung von Umgebungen von $p$ ist eine Umgebung von~$p$.
  \item $\forall S \subseteq \nsets_{M}(p) \with \text{$\emptyset \neq S$ endlich}
    \holds \bigcap S \in \nsets_{M}(p)$,
    d.h. der Schnitt endlich vieler Umgebungen von $p$ ist eine Umgebung von~$p$.
  \item $\forall S \subseteq \csets_{M} \with \text{$\emptyset \neq S$ endlich}
    \holds \bigcup S \in \csets_{M}$,
    d.h. die Vereinigung endlich vieler abgeschlossener Mengen ist abgeschlossen.
  \item $\forall S \subseteq \csets_{M} \holds \bigcap S \in \csets_{M}$,
    d.h. der Schnitt abgeschlossener Mengen ist abgeschlossen.
  \end{enumerate}
  Alle drei Aussagen, die $S$ als endlich fordern,
  werden ohne diese Einschränkung falsch.
\end{satz}

\begin{proof}
  Wir zeigen nur einige der Aussagen und überlassen die übrigen zur Übung.
  \begin{enumerate}
  \item Es sei $S \subseteq \osets_{M}$ und $p \in \bigcup S$.
    Dann finde $A \in S$ so, dass $p \in A$ gilt,
    und da $A$ offen ist, finde $\veps > 0$ so, dass $B_{M}(p,\veps) \subseteq A$ gilt.
    Nach der Definition der Vereinigung gilt $A \subseteq \bigcup S$,
    also $B_{M}(p,\veps) \subseteq \bigcup S$.
  \item Es sei $S \subseteq \osets_{M}$, so, dass $S \neq \emptyset$ gilt und $S$ endlich ist.
    Es sei $p \in \bigcap S$.
    Für alle $A \in S$ finde $\veps_{A} > 0$ so, dass $B_{M}(p, \veps_{A}) \subseteq A$ gilt.
    Definiere $\veps \df \min_{A \in S} \veps_{A}$.
    Da $A$ endlich ist, gibt es dieses Minimum, und es ist positiv.
    Es gilt $B_{M}(p,\veps) \subseteq A$ für alle $A \in S$,
    nach der Definition des Schnittes also $B_{M}(p,\veps) \subseteq \bigcap S$.
  \end{enumerate}
  Die vorige Aussage wird falsch, wenn man unendliche Mengen $S$ zulässt.
  Zum Beispiel gilt:
  \begin{IEEEeqnarray*}{0l}
    \bigcap_{n \in \NNone} \intoo{{-1}}{\tfrac{1}{n}} = \intoc{{-1}}{0}
  \end{IEEEeqnarray*}
  Hier haben wir einen Schnitt offener Mengen,
  der aber nicht offen ist; denn es ist $0$ kein innerer Punkt von $\intoc{{-1}}{0}$.
\end{proof}

Wir schließen den Abschnitt mit einem Ergebnis,
das wir schon von den Umgebungen in $\RR$ kennen.
Wir werden es im folgenden Abschnitt bei der Eindeutigkeit des Limes benötigen.

\begin{satz}[Trennungssatz]
  Es sei $M$ ein metrischer Raum und $a,b \in M$ mit $a \neq b$.
  Dann gibt es $S \in \nsets_{M}(a)$ und $T \in \nsets_{M}(b)$ so,
  dass $S \cap T = \emptyset$ gilt.
\end{satz}

\begin{proof}
  Definiere $\veps \df \frac{d_{M}(a,b)}{2} > 0$
  und $S \df B_{M}(a,\veps)$ und $T \df B_{M}(a,\veps)$.
  Dann gilt $S \in \nsets_{M}(a)$ und $T \in \nsets_{M}(b)$.
  Angenommen, es gibt $x \in S \cap T$.
  Dann gilt mit der Dreiecksungleichung $d_{M}(a,b) \leq d_{M}(a,x) + d_{M}(x,b) <  2 \veps = d_{M}(a,b)$,
  was falsch ist.
\end{proof}

\section{Konvergenz}

\begin{para}[Konvergenz]
  Es sei $M$ ein metrischer Raum.
  Es seien $\seq{x_{n}}{n} \in \seqset{M}$ und $p \in M$.
  Dann nennen wir $p$ einen \term{Limes} von $\seq{x_{n}}{n}$ in $M$,
  oder sagen, dass $\seq{x_{n}}{n}$ in $M$ gegen $p$ \term{konvergiert},
  wenn in jeder Umgebung von $p$ in $M$ fast alle Folgenkomponenten liegen.
  Letzteres ist so zu verstehen, dass, wenn $\mu$ der Startindex der Folge ist, die Menge
  \begin{IEEEeqnarray*}{0l}
    \set{n \geq \mu \suchthat x_{n} \not \in U}
  \end{IEEEeqnarray*}
  endlich ist für alle $U \in \nsets_{M}(p)$.
\end{para}

\begin{satz}[Eindeutigkeit des Limes]
  Es sei $M$ ein metrischer Raum und ${\seq{x_{n}}{n} \in \seqset{M}}$.
  Dann hat $\seq{x_{n}}{n}$ höchstens einen Limes.
\end{satz}

\begin{proof}
  Folgt leicht aus dem Trennungssatz (Übung).
\end{proof}

\begin{para}[Notation des Limes]
  Wir notieren den Limes von $\seq{x_{n}}{n}$ in $M$ \ggf mit $\limxx{n}{M} x_{n}$.
  Für alle $A \subseteq M$ schreiben wir $\limxx{n}{M} x_{n} \in A$,
  um auszusagen, dass $\seq{x_{n}}{n}$ in $M$ konvergiert und der Limes ein Element von $A$ ist.
  Insbesondere können wir mit $\limxx{n}{M} x_{n} \in M$ ausdrücken, dass dieser Limes existiert.
  \par
  Wir verwenden auch die Notation $x_{n} \tendsxx{n}{M} p$ dafür,
  dass $\limxx{n}{M} x_{n} = p$ gilt.
\end{para}

\begin{satz}[Mindestindex-Charakterisierung des Limes]
  Es seien $M$ ein metrischer Raum und $\seq{x_{n}}{n \geq \mu} \in \seqset{M}$ und $p \in M$.
  Dann sind äquivalent:
  \begin{enumerate}
  \item $p = \limxx{n}{M} x_{n}$
  \item $\forall U \in \nsets_{M}(p) \innerholds \exists n_{0} \innerholds \forall n \geq n_{0}
    \holds x_{n} \in U$
  \item $\forall \veps > 0 \innerholds \exists n_{0} \innerholds \forall n \geq n_{0}
    \holds x_{n} \in B_{M}(p,\veps)$
  \item $\forall \veps > 0 \innerholds \exists n_{0} \innerholds \forall n \geq n_{0}
    \holds d_{M}(x_{n},p) < \veps$
  \end{enumerate}
\end{satz}

\begin{proof}
  \imprefx{1}{2}
  Es sei $U \in \nsets_{M}(p)$.
  Es ist $E \df \set{n \geq \mu \suchthat x_{n} \not \in U}$ endlich nach Definition des Limes.
  Definiere $n_{0} \df \max(E) + 1$.
  Es sei $n \geq n_{0}$.
  Dann gilt $n \not\in E$, also gilt nicht $x_{n} \not \in U$,
  also gilt $x_{n} \in U$.
  \par
  \imprefx{2}{3}
  Folgt daraus, dass $B_{M}(p,\veps) \in \nsets_{M}(p)$ für alle $\veps > 0$ gilt.
  \par
  \imprefx{3}{1}
  Es sei $U \in \nsets_{M}(p)$.
  Dann finde $\veps > 0$ so, dass $B_{M}(p,\veps) \subseteq U$ gilt.
  Nach Voraussetzung finde $n_{0}$ so, dass $x_{n} \in B_{M}(p,\veps) \subseteq U$ für alle $n \geq n_{0}$ gilt.
  Also liegen höchstens die Folgenkomponenten mit den Indizes aus der Menge
  $\setft{\mu}{n_{0}-1}$ nicht in $U$, also liegen fast alle Folgenkomponenten in $U$.
  \par
  Schließlich gilt offenbar \iffrefx*{3}{4}.
\end{proof}

\begin{para}[Notation für einen Mindestindex]
  Es sei $\seq{x_{n}}{n} \in \seqset{M}$ konvergent in~$M$; notiere $p \df \limxx{n}{M} x_{n}$.
  Für alle ${U \in \nsets_{M}(p)}$ wählen wir $\nzero{M}(U,x)$ so, dass
  $x_{n} \in U$ für alle $n \geq \nzero{M}(U,x)$ gilt.
  Ein solches $\nzero{M}(U,x)$ gibt es nach obigem Satz,
  auch wenn es nicht eindeutig bestimmt ist.
  Ebenso für alle $\veps > 0$ wählen wir $\nzero{M}(\veps,x)$ so, dass
  $x_{n} \in B_{M}(p,\veps)$ für alle $n \geq \nzero{M}(\veps,x)$ gilt.
\end{para}

\begin{satz}[Abstandslimes]
  \label{struct/metric:abstandslimes}
  Es seien $M$ ein metrischer Raum und $\seq{x_{n}}{n} \in \seqset{M}$ und $p \in M$.
  Dann sind äquivalent:
  \begin{enumerate}
  \item $\limxx{n}{M} x_{n} = p$
  \item $\limxx{n}{\RR} d_{M}(x_{n}, p) = 0$
  \end{enumerate}
\end{satz}

\begin{proof}
  Übung.
\end{proof}

\begin{satz}[Folgenkriterium der Abgeschlossenheit]
  Es seien $M$ ein metrischer Raum und $A \subseteq M$.
  Dann sind äquivalent:
  \begin{enumerate}
  \item $A$ ist abgeschlossen.
  \item $\forall \seq{x_{n}}{n} \in \seqset{A} \with
    \limxx{n}{M} x_{n} \in M \holds \limxx{n}{M} x_{n} \in A$
  \end{enumerate}
\end{satz}

\begin{proof}
  \imprefx{1}{2}
  Für Widerspruch nehme an, es gibt $\seq{x_{n}}{n} \in \seqset{A}$ in $M$ konvergent so,
  dass $p \df \limxx{n}{M} x_{n} \not\in A$, also $p \in B \df M \setminus A$.
  Da $A$ abgeschlossen ist, ist $B$ offen.
  Also finde $\veps > 0$ so, dass $B_{M}(p,\veps) \subseteq B$ gilt.
  Es folgt $x_{n} \in B_{M}(p,\veps) \subseteq B$, also $x_{n} \not\in A$,
  für alle $n \geq \nzero{M}(\veps, x)$.
  Das kann nicht sein, da $\seq{x_{n}}{n} \in \seqset{A}$ gilt.
  \par
  \imprefx{2}{1}
  Wir führen Kontraposition, es gelte also \negrefx{1}.
  Dann ist $B \df M \setminus A$ nicht offen,
  also gibt es $p \in B$ so, dass gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall \veps > 0 \holds \exists x \in B_{M}(p,\veps) \cap A
  \end{IEEEeqnarray*}
  Es folgt:
  \begin{IEEEeqnarray*}{0l}
    \forall n \in \NNone \holds \exists x_{n} \in B_{M}(p,\tfrac{1}{n}) \cap A
  \end{IEEEeqnarray*}
  Dies definiert $\seq{x_{n}}{n} \in \seqset{A}$,
  und man sieht leicht (Übung), dass $\limxx{n}{M} x_{n} = p \not\in A$ gilt.
  Damit ist \negrefx{2} gezeigt.
\end{proof}

\begin{example}[Konvergente Folgen]
  \hfill
  \begin{itemize}
  \item In $\RR$ (mit der Betragsmetrik) erhalten wir via obige Definition
    genau die konvergenten Folgen, die aus der Einführung in die reelle Analysis
    aus früheren Semestern bekannt sind.
  \item Die bezüglich der diskreten Metrik konvergenten Folgen
    sind genau diejenigen Folgen, die schließlich konstant sind.
    \Genauer
    Ist $M$ eine Menge und $d$ darauf die diskrete Metrik,
    so ist $\seq{x_{n}}{n} \in \seqset{M}$ genau dann in $(M,d)$ konvergent,
    wenn es $n_{0}$ so gibt, dass $x_{n} = x_{m}$ für alle $n,m \geq n_{0}$ gilt.
  \end{itemize}
\end{example}

\section{Stetigkeit}

\begin{para}[Stetigkeit]
  Es seien $S$ und $T$ metrische Räume.
  Es seien $\Om \subseteq S$ und $\ffn{f}{\Om}{T}$ und $p \in \Om$.
  Wir sagen, $f$ ist \term{\stetig{S,T}} in $p$, wenn gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall \seq{x_{n}}{n} \in \seqset{\Om} \with \limxx{n}{S} x_{n} = p
    \holds \limxx{n}{T} f(x_{n}) = f(p)
  \end{IEEEeqnarray*}
  Wir sagen, $f$ ist \term{\stetig{S,T}} auf $A \subseteq \Om$,
  wenn $f$ \stetig{S,T} in jedem Punkt von $A$ ist.
  Wir sagen, $f$ ist \term{\stetig{S,T}},
  wenn $f$ \stetig{S,T} auf $\Om$ ist.
\end{para}

\begin{satz}[$\veps$\=/$\del$\=/Charakterisierung der Stetigkeit]
  \label{struct/metric:eps-del}
  Es seien $S,T,\Om,f,p$ wie oben.
  Dann sind äquivalent:
  \begin{enumerate}
  \item $f$ ist \stetig{S,T} in $p$.
  \item $\forall \veps > 0 \innerholds \exists \del > 0 \innerholds \forall x \in B_{S}(p, \del) \cap \Om
    \holds f(x) \in B_{T}(f(p), \veps)$
  \item $\forall \veps > 0 \innerholds \exists \del > 0 \holds \fnimg{f}(B_{S}(p, \del) \cap \Om)
    \subseteq B_{T}(f(p), \veps)$
  \item $\forall \veps > 0 \innerholds \exists \del > 0 \innerholds \forall x \in \Om
    \holds d_{S}(x,p) < \del \implies d_{T}(f(x), f(p)) < \veps$
  \end{enumerate}
\end{satz}

\begin{proof}
  \imprefx{1}{2}
  Für führen Kontraposition, es gelte also \negrefx{2}.
  Wir finden also $\veps > 0$ so, dass gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall \del > 0 \innerholds \exists x \in B_{S}(p, \del) \cap \Om
    \holds f(x) \not\in B_{T}(f(p), \veps)
  \end{IEEEeqnarray*}
  Es folgt:
  \begin{IEEEeqnarray*}{0l}
    \forall n \in \NNone \innerholds \exists x_{n} \in B_{S}(p, \tfrac{1}{n}) \cap \Om
    \holds f(x_{n}) \not\in B_{T}(f(p), \veps)
  \end{IEEEeqnarray*}
  Dies definiert $\seq{x_{n}}{n} \in \seqset{\Om}$,
  und man sieht leicht (Übung), dass $\limxx{n}{S} x_{n} = p$ gilt.
  Da aber $f(x_{n}) \not\in B_{T}(f(p), \veps)$ für alle $n$ gilt,
  haben wir nicht $\limxx{n}{T} f(x_{n}) = f(p)$.
  Damit ist \negrefx{1} gezeigt.
  \par
  \imprefx{2}{1}
  Es sei $\seq{x_{n}}{n} \in \seqset{\Om}$ so, dass $\limxx{n}{S} x_{n} = p$ gilt.
  Es sei $\veps > 0$.
  Wähle $\del > 0$ entsprechend der Voraussetzung.
  Dann gilt für alle $n \geq \nzero{S}(\del, x)$, dass $x_{n} \in B_{S}(p, \del) \cap \Om$,
  also $f(x_{n}) \in B_{T}(f(p), \veps)$.
  Das beweist $\limxx{n}{T} f(x_{n}) = f(p)$.
  \par
  Schließlich gelten offenbar \iffrefx*{2}{3} und \iffrefx*{3}{4},
  da es sich nur um einfache Umformulierungen handelt.
\end{proof}

\begin{example}[Stetige Funktionen]
  \hfill
  \begin{itemize}
  \item Sind die beiden metrischen Räume $\RR$ (mit der Betragsmetrik),
    so erhalten wir via obige Definition
    genau die Stetigkeit, die aus der Einführung in die reelle Analysis
    aus früheren Semestern bekannt ist.
  \item Haben wir auf dem metrischen Raum, in dem die Domain der Funktion liegt, (das ist $S$ oben)
    die diskrete Metrik, dann sind alle Funktionen stetig.
  \end{itemize}
\end{example}

\begin{satz}
  \label{struct/metric:komp}
  Stetigkeit überträgt sich auf die Komposition von Funktionen.
  \Genauer
  Es seien $S, T, U$ metrische Räume.
  Es seien $\Om_{S} \subseteq S$ und $\Om_{T} \subseteq T$
  und $\ffn{f_{S}}{\Om_{S}}{\Om_{T}}$ und $\ffn{f_{T}}{\Om_{T}}{U}$.
  Es sei $p \in \Om$ so,
  dass $f_{S}$ \stetig{S,T} in $p$ ist
  und $f_{T}$ \stetig{T,U} in $f_{S}(p)$ ist.
  Dann ist $f_{T} \circ f_{S}$ \stetig{S,U} in $p$.
\end{satz}

\begin{proof}
  Übung.
\end{proof}

\section{Urbildmengen unter stetigen Funktionen}

\begin{satz}
  \label{struct/metric:ur}
  Es seien $S,T$ metrische Räume und $\ffn{f}{S}{T}$ stetig.
  Dann gilt:
  \begin{enumerate}
  \item $\forall p \in S \innerholds \forall Y \in \nsets_{T}(f(p)) \holds \fnpre{f}(Y) \in \nsets_{S}(p)$
  \item $\forall Y \in \osets_{T} \holds \fnpre{f}(Y) \in \osets_{S}$
  \item $\forall Y \in \csets_{T} \holds \fnpre{f}(Y) \in \csets_{S}$
  \end{enumerate}
\end{satz}

\begin{proof}
  \begin{enumerate}
  \item Notiere $X \df \fnpre{f}(Y)$; zu zeigen ist, dass $p$ ein innerer Punkt von $X$ ist.
    Finde $\veps > 0$ so, dass ${B_{T}(f(p), \veps) \subseteq Y}$ gilt.
    Aufgrund der Stetigkeit finde $\del > 0$ so,
    dass $\fnimg{f}(B_{S}(p, \del)) \subseteq B_{T}(f(p), \veps) \subseteq Y$ gilt.
    Nach Definition von Bild- und Urbildmenge folgt $B_{S}(p, \del) \subseteq X$.
  \item Notiere $X \df \fnpre{f}(Y)$; zu zeigen ist,
    dass $X$ Umgebung jedes Punktes von $X$ ist.
    Es sei $p \in X$.
    Dann gilt $f(p) \in Y$, und da $Y$ offen ist, $Y \in \nsets_{T}(f(p))$.
    Somit gilt nach dem vorigen Punkt $X \in \nsets_{S}(p)$.
  \item Übung (man verwende zum Beispiel das Folgenkriterium der Abgeschlossenheit).\qedhere
  \end{enumerate}
\end{proof}

\begin{para}[Erste Warnung zu \autoref{struct/metric:ur}]
  Es ist essentiell, dass die Funktion auf dem gesamten metrischen Raum $S$ definiert ist.
  Betrachte zum Beispiel die stetige Funktion $\ffnx{f}{\intoc{0}{1}}{\RR}{x \mapsto 1}$.
  Es gilt $\fnpre{f}(Y) = \intoc{0}{1}$ für alle $Y \subseteq \RR$ mit ${1 \in Y}$,
  also insbesondere für $Y \df \intoo{{-1}}{1}$ (eine offene Menge)
  und $Y \df \set{1}$ (eine abgeschlossene Menge).
  Aber $\intoc{0}{1}$ ist weder offen noch abgeschlossen.
  Das führt auf folgenden Begriff:
\end{para}

\begin{para}[Einschränkung eines metrischen Raumes; relative Umgebung, \etc]
  Es sei $M$ ein metrischer Raum und $\Om \subseteq M$.
  Definiere die \term{Einschränkung} von $M$ auf $\Om$ durch
  $\fnres{M}{\Om} \df (\Om, \fnres{d_{M}}{\Om \times \Om})$.
  Man sieht leicht (Übung), dass $\fnres{M}{\Om}$ wieder ein metrischer Raum ist.
  Für alle $p \in \Om$ und alle $\veps > 0$ sieht man leicht,
  dass $B_{\fnres{M}{\Om}}(p,\veps) = B_{M}(p,\veps) \cap \Om$ gilt.
  \par
  Es sei $A \subseteq \Om$ und $p \in A$.
  Die folgenden Redeweisen meinen,
  dass die genannte Eigenschaft im metrischen Raum $\fnres{M}{\Om}$ gilt:
  \begin{itemize}
  \item $A$ ist relative Umgebung bezüglich $\Om$ von $p$ in $M$.
  \item $p$ ist relativer innerer Punkt bezüglich $\Om$ von $A$ in $M$.
  \item $A$ ist relativ offen bezüglich $\Om$ in $M$.
  \item $A$ ist relativ abgeschlossen bezüglich $\Om$ in $M$.
  \end{itemize}
  Für die relative Umgebung schreiben wir es exemplarisch aus; es sind äquivalent:
  \begin{itemize}
  \item $A$ ist relative Umgebung bezüglich $\Om$ von $p$ in $M$.
  \item $\exists \veps > 0 \innerholds B_{\fnres{M}{\Om}}(p,\veps) \subseteq A$
  \item $\exists \veps > 0 \innerholds B_{M}(p,\veps) \cap \Om \subseteq A$
  \end{itemize}
\end{para}

\begin{para}[Verallgemeinerung von \autoref{struct/metric:ur}]
  Es seien $S,T$ metrische Räume, $\Om \subseteq S$, und $\ffn{f}{\Om}{T}$ stetig.
  Dann gilt \autoref{struct/metric:ur} offenbar,
  wenn man in $\nsets_{S}, \osets_{S}, \csets_{S}$ das $S$ durch $\fnres{S}{\Om}$ ersetzt.
  Im Hinblick auf das Beispiel $\ffnx{f}{\intoc{0}{1}}{\RR}{x \mapsto 1}$
  bemerken wir, dass in der Tat $\intoc{0}{1}$ sowohl relativ offen als auch relativ abgeschlossen
  bezüglich $\intoc{0}{1}$ in $\RR$ ist.
\end{para}

\begin{para}[Zweite Warnung zu \autoref{struct/metric:ur}]
  Die Aussagen gelten nicht für Bildmengen,
  auch nicht, wenn man es relativ bezüglich der Bildmenge ($\img$) der Funktion betrachtet.
  Dazu sehen wir uns zwei Beispiele an.
  \begin{itemize}
  \item Definiere:
    \begin{IEEEeqnarray*}{0l}
      \ffnx{f}{\RR}{\RR}{x \mapsto
        \begin{cases}
          1 & x < 1 \\
          x & \text{sonst}
        \end{cases}}
    \end{IEEEeqnarray*}
    Es ist $f$ stetig, und es gilt $\img(f) = \intci{1}$.
    Definiere $X \df \intoo{{-1}}{1}$.
    Dann gilt $\fnimg{f}(X) = \set{1}$.
    Obwohl $X \in \nsets_{\RR}(0)$ gilt, ist $\set{1}$ keine Umgebung von $f(1) = 1$ in~$\RR$,
    auch nicht relativ bezüglich $\img(f)$.
    Insbesondere ist $\set{1}$ nicht offen, auch nicht relativ bezüglich $\img(f)$.
  \item Definiere:
    \begin{IEEEeqnarray*}{0l}
      \ffnx{f}{\RR}{\RR}{x \mapsto
        \begin{cases}
          e^{x} & x < 0 \\
          1-x & \text{sonst}
        \end{cases}}
    \end{IEEEeqnarray*}
    Es ist $f$ stetig, und es gilt $\img(f) = \intic{1}$.
    Es ist $X \df \intic{0}$ abgeschlossen, doch $\fnimg{f}(X) = \intoc{0}{1}$
    ist nicht abgeschlossen, auch nicht relativ bezüglich $\img(f)$.
  \end{itemize}
\end{para}

\section{Kompaktheit}

\begin{para}[Kompaktheit]
  Es sei $M$ ein metrischer Raum.
  Wir nennen $M$ \term{kompakt}, wenn jede Folge in $M$ eine in $M$ konvergente Teilfolge hat.
  Wir nennen $K \subseteq M$ kompakt in $M$,
  wenn $\fnres{M}{K}$ kompakt ist.
  Offenbar ist also $K \subseteq M$ kompakt in $M$ genau dann, wenn gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall \seq{x_{n}}{n} \in \seqset{K} \innerholds
    \exists \ffn{\sig}{\NN}{\NN} \with \text{streng monoton steigend}
    \holds \limxx{n}{M} x_{\sig(n)} \in K
  \end{IEEEeqnarray*}
\end{para}

\begin{satz}
  Stetige Funktionen bilden kompakte Mengen auf kompakte Mengen ab.
  \Genauer
  Es seien $S$ und $T$ metrische Räume.
  Es seien $\Om \subseteq S$ und $\ffn{f}{\Om}{T}$ eine \stetige{S,T} Funktion.
  Es sei $K \subseteq \Om$ kompakt in $S$.
  Dann ist $\fnimg{f}(K)$ kompakt in $T$.
\end{satz}

\begin{proof}
  Notiere $L \df \fnimg{f}(K)$.
  Es sei $\seq{y_{n}}{n} \in \seqset{L}$.
  Dann finde zu allen $n$ ein $x_{n} \in K$ so, dass $f(x_{n}) = y_{n}$ gilt.
  So erhalte $\seq{x_{n}}{n} \in \seqset{K}$.
  Da $K$ kompakt ist, finde $\ffn{\sig}{\NN}{\NN}$ streng monoton steigend so,
  dass $p \df \limxx{n}{S} x_{\sig(n)} \in K$ gilt.
  Wegen der Stetigkeit gilt $\limxx{n}{S} f(x_{\sig(n)}) = f(p)$, also:
  \begin{IEEEeqnarray*}{0l}
    \limxx{n}{S} y_{\sig(n)} = \limxx{n}{S} f(x_{\sig(n)}) = f(p) \in L
  \end{IEEEeqnarray*}
  Damit haben wir eine Teilfolge von $\seq{y_{n}}{n}$ gefunden,
  die in $S$ konvergiert, und zwar gegen einen Punkt in $L$.
\end{proof}

Aus einem früheren Kapitel wissen wir schon,
dass eine Teilmenge von $\RR$ genau dann kompakt in $\RR$ (als metrischem Raum mit der Betragsmetrik) ist,
wenn sie in $\RR$ abgeschlossen und beschränkt ist.
Damit zeigt man leicht:

\begin{satz}
  Nicht\-/leere, in $\RR$ kompakte Teilmengen von $\RR$ haben Maximum und Minimum.
\end{satz}

\begin{proof}
  Wir weisen nur das Maximum nach, da es für das Minimum analog funktioniert.
  Es sei $\emptyset \neq A \subseteq \RR$ kompakt in $\RR$.
  Dann ist $A$ abgeschlossen in $\RR$ und beschränkt.
  Wegen der Beschränktheit und wegen $A \neq \emptyset$, gilt ${s \df \sup(A) \in \RR}$.
  Nach der $\veps$\=/Charakterisierung des Supremums finde für alle $n \in \NNone$
  ein ${x_{n} \in A \cap B_{\RR}(s,\frac{1}{n})}$.
  Es folgt $\limxx{n}{\RR} x_{n} = s$ nach Konstruktion.
  Da $A$ abgeschlossen ist, folgt $s \in A$ mit dem Folgenkriterium der Abgeschlossenheit.
  Also ist $s = \max(A)$.
\end{proof}

\begin{corollary}
  Stetige Funktionen nach $\RR$ nehmen auf kompakten Mengen Maximum und Minimum an.
  \Genauer
  Es sei $S$ ein metrischer Raum.
  Es seien $\Om \subseteq S$ und $\ffn{f}{\Om}{\RR}$ eine \stetige{S,\RR} Funktion.
  Es sei $K \subseteq \Om$ kompakt in~$S$.
  Dann nimmt $f$ auf $K$ Maximum und Minimum an,
  d.h. es gibt $x^{*}, x_{*} \in K$ so, dass $f(x^{*}) = \max(\fnimg{f}(K))$
  und $f(x_{*}) = \min(\fnimg{f}(K))$ gilt.
\end{corollary}

\section{Weitere Stetigkeitsbegriffe}

\begin{para}[Gleichmäßige Stetigkeit]
  Es seien $S$ und $T$ metrische Räume.
  Es seien $\Om \subseteq S$ und $\ffn{f}{\Om}{T}$.
  Wir sagen, $f$ ist \term{gleichmäßig \stetig{S,T}}, wenn gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall \veps > 0 \innerholds \exists \del > 0 \innerholds \forall x,y \in \Om
    \with d_{S}(x,y) < \del \holds d_{T}(f(x),f(y)) < \veps
  \end{IEEEeqnarray*}
  Nach \autoref{struct/metric:eps-del} folgt daraus, dass $f$ \stetig{S,T} ist.
  Die Umkehrung gilt jedenfalls dann, wenn die Domain kompakt ist:
\end{para}

\begin{satz}
  Stetige Funktionen mit kompakter Domain sind gleichmäßig stetig.
  \Genauer
  Es seien $S$ und $T$ metrische Räume.
  Es seien $\Om \subseteq S$ kompakt in $S$ und $\ffn{f}{\Om}{T}$ sei \stetig{S,T}.
  Dann ist $f$ gleichmäßig \stetig{S,T}.
\end{satz}

\begin{proof}
  Für Widerspruch nehme an, es gilt:
  \begin{IEEEeqnarray*}{0l}
    \exists \veps > 0 \innerholds \forall \del > 0 \innerholds \exists x,y \in \Om
    \with d_{S}(x,y) < \del \land d_{T}(f(x),f(y)) \geq \veps
  \end{IEEEeqnarray*}
  Wähle $\veps > 0$ entsprechend.
  Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall n \in \NNone \innerholds \exists x_{n},y_{n} \in \Om
    \with d_{S}(x_{n},y_{n}) < \frac{1}{n} \land d_{T}(f(x_{n}),f(y_{n})) \geq \veps
  \end{IEEEeqnarray*}
  Dies ergibt $\seq{x_{n}}{n}, \seq{y_{n}}{n} \in \seqset{\Om}$.
  Da $\Om$ kompakt ist, gibt es $\ffn{\sig}{\NN}{\NN}$ streng monoton steigend so,
  dass $\seq{x_{\sig(k)}}{k}$ in $S$ konvergiert,
  und zwar gegen ${p \df \limxx{k}{S} x_{\sig(k)} \in \Om}$.
  Man sieht leicht (Übung), dass auch $\limxx{k}{S} y_{\sig(k)} = p$ gilt.
  Da $f$ \stetig{S,T} ist, folgt:
  \begin{IEEEeqnarray*}{0l}
    \limxx{k}{T} f(x_{\sig(k)}) = f(p) = \limxx{k}{T} f(y_{\sig(k)})
  \end{IEEEeqnarray*}
  Also gibt es $k_{1}$ so, dass $d_{T}(f(x_{\sig(k)}), f(p)) < \frac{\veps}{2}$ für alle $k \geq k_{1}$ gilt;
  es gibt $k_{2}$ so, dass $d_{T}(f(y_{\sig(k)}), f(p)) < \frac{\veps}{2}$ für alle $k \geq k_{2}$ gilt.%
  \footnote{Kürzer könnten wir mit unserer Notation schreiben
    $k_{1} \df \nzero{T}(\frac{\veps}{2}, \seq{f(x_{\sig(k)})}{k})$
    und $k_{2} \df \nzero{T}(\frac{\veps}{2}, \seq{f(y_{\sig(k)})}{k})$.}
  Mit $k_{0} \df \max\set{k_{1}, k_{2}}$ folgt:
  \begin{IEEEeqnarray*}{0l}
    d_{T}(f(x_{\sig(k_{0})}), f(y_{\sig(k_{0})}))
    \leq d_{T}(f(x_{\sig(k_{0})}), p) + d_{T}(p, f(y_{\sig(k_{0})}))
    < \frac{\veps}{2} + \frac{\veps}{2} = \veps
  \end{IEEEeqnarray*}
  Dies widerspricht der Tatsache,
  dass $d_{T}(f(x_{\sig(k)}), f(y_{\sig(k)})) \geq \veps$ für alle $k$ gilt.
\end{proof}

\begin{para}[Lipschitz-Stetigkeit]
  Es seien $S$ und $T$ metrische Räume.
  Es seien $\Om \subseteq S$ und $\ffn{f}{\Om}{T}$.
  Wir sagen, $f$ ist \term{\lipstetig{S,T}}, wenn gilt:
  \begin{IEEEeqnarray*}{0l}
    \exists L > 0 \innerholds \forall x,y \in \Om
    \holds d_{T}(f(x),f(y)) \leq L \ccdot d_{S}(x,y)
  \end{IEEEeqnarray*}
  Eine Zahl $L > 0$ mit dieser Eigenschaft wird eine \term{\LKonstante{S,T}}
  für $f$ genannt.
\end{para}

\begin{satz}
  Lipschitz\-/stetige Funktionen sind gleichmäßig stetig.
\end{satz}

\begin{proof}
  Übung.
\end{proof}

\begin{para}[Hierarchie der Stetigkeit]
  Es gilt also:
  \begin{IEEEeqnarray*}{0l}
    \text{Lipschitz-stetig} \implies \text{gleichmäßig stetig} \implies \text{stetig}
  \end{IEEEeqnarray*}
  Auf kompakter Domain haben wir ferner:
  $\text{gleichmäßig stetig} \iff \text{stetig}$.
  \par
  Es gibt stetige Funktionen, die nicht gleichmäßig stetig sind (Übung).
  \par
  Es gibt gleichmäßig stetige Funktionen, die nicht Lipschitz-stetig sind,
  obwohl sie kompakte Domain haben.
  Definiere dazu $\ffnx{f}{\intcc{0}{1}}{\RR}{x \mapsto \sqrt{x}}$.
  Es ist $f$ gleichmäßig stetig, da $f$ stetig ist und kompakte Domain hat.
  Wir zeigen:
  \begin{IEEEeqnarray*}{0l}
    \forall L > 0 \innerholds \exists x,y \in \intcc{0}{1}
    \holds \abs{f(x) - f(y)} > L \ccdot \abs{x-y}
  \end{IEEEeqnarray*}
  Es sei $L > 0$.
  Für alle $x,y \in \intoc{0}{1}$ mit $x \neq y$ gilt:
  \begin{IEEEeqnarray*}{0l}
    \frac{\sqrt{x} - \sqrt{y}}{x - y}
    = \frac{\sqrt{x} - \sqrt{y}}{(\sqrt{x} - \sqrt{y}) (\sqrt{x} + \sqrt{y})}
    = \frac{1}{\sqrt{x} + \sqrt{y}}
  \end{IEEEeqnarray*}
  Wähle $x,y \in \intoo{0}{\frac{1}{4 L^{2}}}$ mit $x \neq y$.
  Dann gilt $\frac{1}{\sqrt{x} + \sqrt{y}} > \frac{1}{\frac{1}{2 L} + \frac{1}{2 L}} = L$.
  Es folgt:
  \begin{IEEEeqnarray*}{0l}
    \abs{\sqrt{x} - \sqrt{y}} = \frac{\abs{x - y}}{\sqrt{x} + \sqrt{y}}
    > L \ccdot \abs{x-y}
  \end{IEEEeqnarray*}
\end{para}

\section{Funktionslimes}

\begin{para}[Häufungspunkt]
  Es sei $M$ ein metrischer Raum und $p \in M$ und $\Om \subseteq M$.
  Wir nennen $p$ einen \term{Häufungspunkt} von $\Om$ in $M$,
  wenn es $\seq{x_{n}}{n} \in \seqset{\Om \setminus \set{p}}$ so gibt, dass $\limxx{n}{M} x_{n} = p$ gilt.
\end{para}

\begin{satz}[Verhalten von Funktionswertfolgen]
  \label{struct/metric:funkfolg}
  Es seien $S,T$ metrische Räume, $\Om \subseteq S$, und $\ffn{f}{\Om}{T}$.
  Es sei $p \in S$ ein Häufungspunkt von $\Om$ in $S$.
  Dann sind äquivalent:
  \begin{enumerate}
  \item $\forall \seq{x_{n}}{n} \in \seqset{\Om \setminus \set{p}}
    \with \limxx{n}{S} x_{n} = p \holds \limxx{n}{T} f(x_{n}) \in T$
  \item $\exists y \in T \innerholds \forall \seq{x_{n}}{n} \in \seqset{\Om \setminus \set{p}}
    \with \limxx{n}{S} x_{n} = p \holds \limxx{n}{T} f(x_{n}) = y$
  \end{enumerate}
  \Ggf ist die in~(2) mit $y$ bezeichnete Größe eindeutig bestimmt.
\end{satz}

\begin{proof}
  Die Eindeutigkeit von $y$ folgt direkt daraus, dass $p$ Häufungspunkt von $\Om$ ist.
  Wir zeigen nun beide Implikationen.
  \par
  \imprefx{1}{2}
  Notiere $X \df \seqset{\Om \setminus \set{p}}$.
  Wir wählen irgendeine Folge $\seq{x_{n}}{n} \in X$ mit ${\limxx{n}{S} x_{n} = p}$;
  so eine gibt es gewiss, da $p$ Häufungspunkt von $\Om$ ist.
  Definiere ${y \df \limxx{n}{T} f(x_{n})}$, dann ist $y \in T$.
  Zu zeigen bleibt:
  \begin{IEEEeqnarray*}{0l}
    \forall \seq{\tix_{n}}{n} \in X \with \limxx{n}{S} \tix_{n} = p
    \holds \limxx{n}{T} f(\tix_{n}) = y
  \end{IEEEeqnarray*}
  Es sei also $\seq{\tix_{n}}{n} \in X$ so, dass $\limxx{n}{S} \tix_{n} = p$ gilt.
  \OBdA nehme an, dass der Startindex von $\seq{x_{n}}{n}$ und $\seq{\tix_{n}}{n}$ jeweils~$0$ ist.
  Definiere $\seq{z_{n}}{n \geq 0}$ durch:\footnote{%
    Wir verwenden \enquote{odd} und \enquote{even},
    da \enquote{ungerade} und \enquote{gerade} beim Sprechen zu leicht verwechselt werden.}
  \begin{IEEEeqnarray*}{0l}
    z_{n} \df
    \begin{cases}
      x_{\frac{n-1}{2}} & \text{$n$ odd} \\
      \tix_{\frac{n}{2}} & \text{$n$ even}
    \end{cases}
  \end{IEEEeqnarray*}
  Dann ist $\seq{z_{n}}{n} \in X$ und $\limxx{n}{S} z_{n} = p$,
  nach Voraussetzung also ${\limxx{n}{T} f(z_{n}) \in T}$.
  Bei $\seq{f(x_{n})}{n}$ und $\seq{f(\tix_{n})}{n}$
  handelt es sich um Teilfolgen von $\seq{f(z_{n})}{n}$;
  die Selektorfunktionen sind $\fnx{\NN}{\NN}{n \mapsto 2n+1}$ \bzw $\fnx{\NN}{\NN}{n \mapsto 2n}$.
  Alle Teilfolgen einer konvergenten Folge konvergieren gegen denselben Limes (Übung).
  Es folgt $\limxx{n}{T} f(\tix_{n}) = \limxx{n}{T} f(x_{n}) = y$.
  \par
  \imprefx{2}{1} trivial.
\end{proof}

\begin{para}[Notation des Funktionslimes]
  Das $y$ aus~(2), wenn es existiert, bezeichnen wir mit $\flimxx{x}{p}{S,T} f(x)$.
  Dabei darf auch ein anderes Symbol anstelle von $x$ verwendet werden:
  \begin{IEEEeqnarray*}{0l}
    \flimxx{x}{p}{S,T} f(x) = \flimxx{y}{p}{S,T} f(y)
    = \flimxx{\al}{p}{S,T} f(\al)
    = \flimxx{\kappa}{p}{S,T} f(\kappa)
    = \hdots
  \end{IEEEeqnarray*}
  Wir nennen dies den \term{Funktionslimes} von $f$ in $p$ bezüglich $(S,T)$.
  Für alle $A \subseteq T$ schreiben wir $\flimxx{x}{p}{S,T} f(x) \in A$,
  um auszusagen, dass dieser Funktionslimes existiert und ein Element von $A$ ist.
  Insbesondere können wir mit ${\flimxx{x}{p}{S,T} f(x) \in T}$ ausdrücken,
  dass dieser Funktionslimes existiert.
  \par
  Wir verwenden auch die Notation $f(x) \tendsastoxx{x}{p}{S,T} y$ dafür,
  dass $\flimxx{x}{p}{S,T} f(x) = y$ gilt.
\end{para}

\begin{satz}[Kompositionssatz für Funktionslimites]
  \label{struct/metric:komp-flim}
  Es seien $S, T, U$ metrische Räume.
  Es seien $\Om_{S} \subseteq S$ und $\Om_{T} \subseteq T$
  und $\ffn{f_{S}}{\Om_{S}}{\Om_{T}}$ und $\ffn{f_{T}}{\Om_{T}}{U}$.
  Es sei $p_{S} \in \Om_{S}$ ein Häufungspunkt von $\Om$ in $S$ so,
  dass $p_{T} \df \flimxx{x}{p_{S}}{S,T} f_{S}(x) \in \Om_{T}$ ein Häufungspunkt von $\Om_{T}$ in $T$ ist
  und $\fnpre{f_{S}}(p_{T}) \subseteq \set{p_{S}}$ gilt
  (also höchstens $p_{S}$ wird von $f_{S}$ auf $p_{T}$ abgebildet).
  Es möge ferner $p_{U} \df \flimxx{x}{p_{T}}{T,U} f_{T}(x) \in U$ gelten.
  Dann gilt $\flimxx{x}{p_{S}}{S,U} (f_{T} \circ f_{S})(x) = p_{U}$.
\end{satz}

\begin{proof}
  Übung.
\end{proof}

\begin{satz}[Lokalität des Funktionslimes]
  \label{struct/metric:ein-flim}
  Es seien $S,T$ metrische Räume, $\ffn{f}{S}{T}$.
  Es sei $p \in S$ ein Häufungspunkt von $S$ in $S$ und $U \in \nsets_{S}(p)$.
  Dann sind äquivalent:
  \begin{enumerate}
  \item $\flimxx{x}{p}{S,T} f(x) \in T$
  \item $\flimxx{x}{p}{S,T} \fnres{f}{U}(x) \in T$
  \end{enumerate}
  \Ggf sind die beiden Funktionslimites gleich; kurz geschrieben haben wir also:
  \begin{IEEEeqnarray*}{0l}
    \flimxx{x}{p}{S,T} f(x) = \flimxx{x}{p}{S,T} \fnres{f}{U}(x)
  \end{IEEEeqnarray*}
  Insbesondere gilt: Wenn $\fnres{f}{U}$ stetig in $p$ ist,
  dann ist $f$ stetig in $p$.
\end{satz}

\begin{proof}
  Übung.
\end{proof}

\begin{para}[Funktionslimes implizit gegebener Funktion]
  Bislang kennen wir die Schreibweise $\flimxx{x}{p}{S,T} f(x)$ mit einer Funktion $f$,
  die von einer Teilmenge von $S$ nach $T$ abbildet.
  Es ist wichtig, an dieser Stelle eine Funktion zu haben,
  denn deren Domain gibt vor, welche Folgen zu betrachten sind,
  nämlich Folgen in $\dom(f) \setminus \set{p}$, die in $S$ Limes $p$ haben.
  Schreiben wir $\flimxx{x}{p}{S,T} E$, wobei $E$ ein Ausdruck ist,
  so legen wir eine \term{implizit gegebene Funktion} zugrunde.\footnote{%
    In der Analysis gibt es auch den \enquote{Satz über implizite Funktionen}.
    Dieser ist hier nicht gemeint.}
  Deren Domain wird als die Menge aller $y \in S$ angenommen,
  für die dieser Ausdruck $E$ sinnvoll ist und ein Element aus $T$ ergibt,
  wenn man $y$ anstelle von $x$ dort einsetzt.
  Die Abbildungsvorschrift ergibt sich aus dem Ausdruck~$E$.
  \par
  Aufgrund der Lokalität des Funktionslimes genügt es,
  die Bestimmung der Domain der implizit gegebenen Funktion
  auf irgendeine Umgebung von $p$ in $S$ zu beschränken.
  Zum Beispiel könnte man bei $\flimxx{x}{1}{\RR,\RR} \frac{e^{-x}}{2-x}$
  die Menge $\RR \setminus \set{2}$ als Domain nehmen, oder auch $B_{\RR}(1,\veps) \setminus \set{2}$
  für alle $\veps > 0$.
  Bei $\flimxx{x}{0}{\RR,\RR} \frac{1}{\ln(x)}$ könnte man zum Beispiel
  die Menge $\RRpos \setminus \set{1}$ als Domain nehmen, oder auch $\intoo{0}{\veps} \setminus \set{1}$
  für alle $\veps > 0$.
\end{para}

\begin{para}[Uneigentlicher Limes]
  Aus früheren Semestern sind für Folgen in $\RR$ die uneigentlichen Limites $\pinfty$ und $\ninfty$ bekannt.
  Wenn wir von $\RR$ als metrischem Raum (mit der Betragsmetrik) sprechen,
  dann übernehmen wir diese Begriffe wie von früher bekannt.
  Also, für alle $\seq{x_{n}}{n} \in \seqset{\RR}$ definiere:
  \begin{IEEEeqnarray*}{0l}
    \limxx{n}{\RR} x_{n} = \pinfty \dfiff \forall c \in \RR \innerholds \exists n_{0}
    \innerholds \forall n \geq n_{0} \holds x_{n} > c \\
    \limxx{n}{\RR} x_{n} = \ninfty \dfiff \forall c \in \RR \innerholds \exists n_{0}
    \innerholds \forall n \geq n_{0} \holds x_{n} < c
  \end{IEEEeqnarray*}
  Für $A \subseteq \RRbar$ schreibe $\limxx{n}{\RR} x_{n} \in A$ um auszusagen,
  dass $\seq{x_{n}}{n}$ einen Limes oder einen uneigentlichen Limes hat
  und dieser Element von $A$ ist.
\end{para}

\begin{para}[Uneigentlicher Funktionslimes]
  Man sieht leicht (Übung), dass im Falle $T=\RR$ der \autoref{struct/metric:funkfolg}
  auch unter Zulassung uneigentlicher Limites gilt, d.h., es sind äquivalent:
  \begin{enumerate}
  \item $\forall \seq{x_{n}}{n} \in \seqset{\Om \setminus \set{p}}
    \with \limxx{n}{S} x_{n} = p \holds \limxx{n}{\RR} f(x_{n}) \in \RRbar$
  \item $\exists y \in \RRbar \innerholds \forall \seq{x_{n}}{n} \in \seqset{\Om \setminus \set{p}}
    \with \limxx{n}{S} x_{n} = p \holds \limxx{n}{\RR} f(x_{n}) = y$
  \end{enumerate}
  Wir verwenden $\flimxx{x}{p}{S,\RR} f(x) = \pinfty$
  und $\flimxx{x}{p}{S,\RR} f(x) = \ninfty$ in der entsprechenden Bedeutung.
\end{para}
