\chapter{Normierte Räume}
\label{struct/norm}

Metrische Räume brauchen abgesehen von der Metrik keine besondere Struktur zu haben.
Das ändert sich nun bei den normierten Räumen,
die auf der bekannten Struktur eines Vektorraumes über $\RR$ oder $\CC$ aufbauen.

\section*{Generalvoraussetzung}

Es sei in diesem Kapitel stets
$\KK \in \set{\RR, \CC}$.

\section{Definition}

\begin{para}[Norm, normierter Raum]
  Es sei $V$ ein Vektorraum über $\KK$.
  Eine Funktion
  \begin{IEEEeqnarray*}{0l}
    \ffn{\normfn}{V}{\RRnn}
  \end{IEEEeqnarray*}
  nennen wir eine \term{Norm} auf $V$, wenn folgende Bedingungen
  gelten:
  \begin{itemize}
  \item $\norm{v} = 0 \iff v = 0$ für alle $v \in V$
  \item $\norm{\lam v} = \abs{\lam} \ccdot \norm{v}$ für alle $\lam \in \KK$ und alle $v \in V$
  \item $\norm{u+v} \leq \norm{u} + \norm{v}$ für alle $u,v \in V$ \textit{(Dreiecksungleichung)}
  \end{itemize}
  Ist $\normfn$ eine Norm auf $V$, so nennen wir
  das Paar $(V, \normfn)$ einen \term{normierten Raum} über $\KK$.
  \par
  Oft gibt man einem normierten Raum einen Namen, zum Beispiel $S$.
  Dann bezeichnen wir den zugrundeliegenden Vektorraum (der oben $V$ heißt) auch mit $S$
  und die Norm mit~$\normfn_{S}$.
  Das ist besonders dann nützlich, wenn man es mit mehreren normierten Räumen zur Zeit zu tun hat.
  Wir erinnern uns, dass ein Vektorraum selbst wieder aus mehreren Komponenten besteht:
  einer Menge, einer Verknüpfung darauf, und der Skalarmultiplikation.
  %%
  %% FIXME
  %%
  %% Spricht etwas gegen V=(V,\normfn) als weitere Notation,
  %% die man hier und da (oder vielleicht sogar systematisch) verwenden könnte?
  %% Ebenso bei metrischen Räumen.
  %%
\end{para}

\begin{example}
  Man sieht leicht (Übung), dass Folgendes Normen auf $\KK^{n}$ sind:
  \begin{itemize}
  \item $\ffnx{\normfn_{1}}{\KK^{n}}{\RRnn}{v \mapsto \sum_{i=1}^n \abs{v_i}}$
  \item $\ffnx{\normfn_{\infty}}{\KK^{n}}{\RRnn}{v \mapsto \max_{i \in \setn{n}} \abs{v_i}}$
  \end{itemize}
  Insbesondere ist auf $\KK$ als Vektorraum über $\KK$ der Betrag eine Norm,
  genannt die \term{Betragsnorm}.
  Wenn wir $\KK$ an einer Stelle schreiben,
  wo ein normierter Raum erwartet wird, dann meinen wir dies,
  also den normierten Raum $(\KK, \absfn)$,
  wenn nicht anders ausgewiesen.
\end{example}

\begin{example}[Norm via linearer Abbildung]
  \label{struct/norm:norm-linabb}
  Es sei $V$ ein Vektorraum über $\KK$ und $(W,\normfn)$ ein normierter Raum über $\KK$.
  Es sei $\ffn{\phi}{V}{W}$ \linear{V,W} und injektiv.
  Dann ist $\normfn_{V} \df \normfn \circ \phi$ eine Norm auf $V$.
  Denn für alle $u,v \in V$ und alle $\lam \in \KK$ gilt:
  \begin{itemize}
  \item $\norm{v}_{V} = 0 \iff \norm{\phi(v)} = 0 \iff \phi(v) = 0 \iff v = 0$,
    Letzteres wegen der Injektivität von $\phi$.
  \item $\norm{\lam v}_{V} = \norm{\phi(\lam v)} = \norm{\lam \phi(v)}
    = \abs{\lam} \ccdot \norm{\phi(v)} = \abs{\lam} \ccdot \norm{v}_{V}$
  \item $\norm{u+v}_{V} = \norm{\phi(u+v)} = \norm{\phi(u)+\phi(v)}
    \leq \norm{\phi(u)} + \norm{\phi(v)} = \norm{u}_{V} + \norm{v}_{V}$
  \end{itemize}
  Durch diese Konstruktion haben wir nun auf jedem endlich\-/dimensionalen Vektorraum eine Norm:
  man nehme zum Beispiel als $(W,\normfn)$ den Vektorraum $\KK^{n}$ mit $\normfn_{1}$ oder $\normfn_{\infty}$
  und als $\phi$ die Koordinatenabbildung bezüglich einer Basis.\footnote{%
    Für den Fall, dass Koordinatenabbildungen nicht aus vorigen Semestern bekannt sind,
    geben wir hier kurz die Definition.
    Es sei $V$ ein Vektorraum über $\KK$ und $B \in V^{n}$ eine Basis von $V$.
    Dann nennen wir die Funktion $\ffn{\kappa^{B}}{V}{\KK^{n}}$,
    die über die Eigenschaft ${v = \sum_{i=1}^{n} \kappa^{B}_{i}(v) B_{i}}$ für alle $v \in V$ definiert ist,
    die \term{Koordinatenabbildung} bezüglich $B$ auf $V$.
    Man sieht leicht (Übung), dass dies eine sinnvolle Definition ist
    und $\kappa^{B}$ ein Isomorphismus zwischen $V$ und $\KK^{n}$ ist.}
\end{example}

\begin{satz}[Rechnen mit der Norm]
  \label{struct/norm:folg}
  Es sei $(V, \normfn)$ ein normierter Raum über~$\KK$.
  Dann gilt für alle $\xu, \xv \in V$:
  \begin{enumerate}
  \item $\norm{-\xv} = \norm{\xv}$
  \item $\abs[big]{\norm{\xu} - \norm{\xv}} \leq \norm{\xu - \xv}$
  \end{enumerate}
\end{satz}

\begin{proof}
  \begin{enumerate}
  \item Es gilt $\norm{-\xv} = \norm{(-1) \xv} = \abs{-1} \ccdot \norm{\xv} = \norm{\xv}$.
  \item Es gilt $\norm{\xu} = \norm{(\xu-\xv) + \xv} \leq \norm{\xu-\xv} + \norm{\xv}$,
    also $\norm{\xu} - \norm{\xv} \leq \norm{\xu-\xv}$.
    Analog zeigt man:
    \begin{IEEEeqnarray*}{0l}
      -(\norm{\xu} - \norm{\xv}) = \norm{\xv} - \norm{\xu} \leq \norm{\xv-\xu} = \norm{\xu-\xv}
    \end{IEEEeqnarray*}
    Die Behauptung folgt, da $\abs{a} = \max\set{a, -a}$ für alle $a \in \RR$ gilt.\qedhere
  \end{enumerate}
\end{proof}

\section{Konvergenz}

\begin{para}[Induzierte Metrik]
  Es sei $V$ ein normierter Raum über $\KK$.
  Dann (Übung) definiert Folgendes eine Metrik auf $V$:
  \begin{IEEEeqnarray*}{0l}
    \fnx{V \times V}{\RRnn}{(u,v) \mapsto \norm{u-v}_{V}}
  \end{IEEEeqnarray*}
  Wenn wir im Kontext von normierten Räumen von offenen und abgeschlossenen Mengen sprechen,
  von inneren Punkten, Umgebungen, konvergenten Folgen, stetigen Funktionen
  oder kompakten Mengen, dann meinen wir dies bezüglich dieser Metrik
  (wenn nicht anders ausgewiesen).
  Ebenso übernehmen wir die Notationen $\osets, \csets, \nsets, \lim, \nzero{}$ von den metrischen Räumen.
\end{para}

\begin{example}
  Wir erhalten den metrischen Raum $\KK$ (mit der Betragsmetrik)
  auf obige Weise aus dem normierten Raum $\KK$ (mit dem Betrag als Norm).
\end{example}

Aus dem Satz \enquote{Abstandslimes} bei metrischen Räumen
(\autoref{struct/metric:abstandslimes}) folgt direkt:

\begin{satz}[Abstandslimes]
  Es sei $V$ ein normierter Raum über $\KK$.
  Dann sind für alle ${\seq{v_{k}}{k} \in \seqset{V}}$ und alle $p \in V$ äquivalent:
  \begin{enumerate}
  \item $\limxx{k}{V} v_{k} = p$
  \item $\limxx{k}{\RR} \norm{v_{k} - p}_{V} = 0$
  \end{enumerate}
  Insbesondere gilt $\limxx{k}{V} v_{k} = 0 \iff \limxx{k}{\RR} \norm{v_{k}}_{V} = 0$.
\end{satz}

\begin{satz}
  \label{struct/norm:konv-beschraenkt}
  Konvergente Folgen sind beschränkt.
  \textit{Genauer:} Es sei $V$ ein normierter Raum über $\KK$.
  Es sei $\seq{v_{k}}{k} \in \seqset{V}$ konvergent in $V$.
  Dann gibt es $c > 0$ so, dass $\norm{v_{k}} < c$ gilt für alle $k$.
\end{satz}

\begin{proof}
  Notiere $p \df \limxx{k}{V} v_{k}$.
  Definiere $k_{0} \df \nzero{V}(1,v)$, und dann $r \df \max\set{\norm{v_{k}} \suchthat k < k_{0}}$.
  Definiere $c \df r + \norm{p} + 1$.
  Dann gilt für alle $k < k_{0}$ gewiss $\norm{v_{k}} < c$.
  Für alle $k \geq k_{0}$ gilt mit der Dreiecksungleichung
  $\norm{v_{k}} \leq \norm{v_{k} - p} + \norm{p} < 1 + \norm{p} \leq c$.
\end{proof}

\begin{satz}[Kombinationssatz für konvergente Folgen]
  Kombinationen konvergenter Folgen sind konvergent mit den entsprechenden Limites.
  \textit{Genauer:} Es sei $V$ ein normierter Raum über $\KK$.
  Es seien $\seq{v_{k}}{k}, \seq{w_{k}}{k} \in \seqset{V}$ konvergent in $V$
  und $\seq{\lam_{k}}{k} \in \seqset{\KK}$ konvergent in $\KK$.
  Dann gilt:
  \begin{enumerate}
  \item $\limxx{k}{V} v_{k} + w_{k} = \limxx{k}{V} v_{k} + \limxx{k}{V} w_{k}$
  \item $\limxx{k}{V} \lam_{k} v_{k} = \limxx{k}{\KK} \lam_{k} \ccdot \limxx{k}{V} v_{k}$
  \item $\limxx{k}{V} \frac{v_{k}}{\lam_{k}} = \frac{\limxx{k}{V} v_{k}}{\limxx{k}{\KK} \lam_{k}}$,
    solange niemals durch~$0$ geteilt wird.
  \end{enumerate}
\end{satz}

\begin{proof}
  \begin{enumerate}
  \item Übung (man verwende die Dreiecksungleichung).
  \item Notiere $\hat{\lam} \df \limxx{k}{\KK} \lam_{k}$ und $\hat{v} \df \limxx{k}{V} v_{k}$.
    Es sei $\veps > 0$.
    Definiere:
    \begin{IEEEeqnarray*}{0l+l}
      k_{1} \df \nzero{\KK}\p{\frac{\veps}{2 (\sup_{k} \norm{v_{k}} + 1)}, \lam} &
      k_{2} \df \nzero{V}\p{\frac{\veps}{2 (\abs{\hat{\lam}} + 1)}, v}
    \end{IEEEeqnarray*}
    Definiere $k_{0} \df \max\set{k_{1}, k_{2}}$.
    Dann gilt für alle $k \geq k_{0}$:
    \begin{IEEEeqnarray*}{0l}
      \norm{\lam_{k} v_{k} - \hat{\lam} \hat{v}}
      \leq \norm{\lam_{k} v_{k} - \hat{\lam} v_{k}} + \norm{\hat{\lam} v_{k} - \hat{\lam} \hat{v}} \\
      \eqin = \abs{\lam_{k} - \hat{\lam}} \ccdot \norm{v_{k}} + \abs{\hat{\lam}} \ccdot \norm{v_{k} - \hat{v}}
      < \frac{\veps}{2} + \frac{\veps}{2} = \veps
    \end{IEEEeqnarray*}
  \item Aus früheren Semestern ist
    $\limxx{k}{\KK} \frac{1}{\lam_{k}} = \frac{1}{\limxx{k}{\KK} \lam_{k}}$ bekannt.
    Die Behauptung folgt daraus mit dem vorigen Punkt.\qedhere
  \end{enumerate}
\end{proof}

\begin{satz}[Komponentensatz der Konvergenz]
  \label{struct/norm:komp-konv}
  In $V = (\KK^{n}, \normfn_{\infty})$ funktioniert Konvergenz komponentenweise.
  \Genauer
  Für alle $\seq{v_{k}}{k} \in \seqset{\KK^{n}}$ und alle $p \in \KK^{n}$ sind äquivalent:
  \begin{enumerate}
  \item $\limxx{k}{V} v_{k} = p$
  \item $\forall i \in \setn{n} \holds \limxx{k}{\KK} (v_{k})_{i} = p_{i}$
  \end{enumerate}
  Die Folge $\seq{(v_{k})_{i}}{k}$, die ja eine Folge in $\KK$ ist,
  nennen wir die \xte{i} \term{Komponentenfolge} von $\seq{v_{k}}{k}$.
  Eine Folge in $V$ konvergiert also genau dann gegen $p$,
  wenn ihre Komponentenfolgen gegen die Komponenten von $p$ konvergieren.
  (Wir werden später sehen, dass dies sogar mit jeder Norm auf $\KK^{n}$ gilt,
  nicht nur mit $\normfn_{\infty}$.)
\end{satz}

\begin{proof}
  \imprefx{1}{2}
  Es sei $i \in \setn{n}$ und $\veps > 0$.
  Für alle $k \geq \nzero{V}(\veps, v)$ gilt:
  \begin{IEEEeqnarray*}{0l}
    \abs{(v_{k})_{i} - p_{i}} \leq \norm{v_{k} - p}_{\infty} < \veps
  \end{IEEEeqnarray*}
  \imprefx{2}{1}
  Es sei $\veps > 0$.
  Definiere $k_{0} \df \max_{i \in \setn{n}} \nzero{V}(\veps, \seq{(v_{k})_{i}}{k})$.
  Es sei ${k \geq k_{0}}$. Dann gilt für alle $i \in \setn{n}$,
  dass $\abs{(v_{k})_{i} - p_{i}} < \veps$ ist.
  Es folgt ${\norm{v_{k} - p}_{\infty} < \veps}$.
\end{proof}

\section{Stetigkeit}

Stetigkeit von Funktionen, die von metrischen Räumen (oder normierten Räumen)
in normierte Räume abbilden, wird bezüglich der von der Norm induzierten Metrik verstanden.

\begin{satz}[Kombinationssatz für Stetigkeit]
  Kombinationen stetiger Funktionen von metrischen Räumen in normierte Räume sind stetig.
  \textit{Genauer:}
  Es seien $M$ ein metrischer Raum
  und $V$ ein normierter Raum über $\KK \in \set{\RR,\CC}$.
  Es sei $\Om \subseteq M$ und $\ffn{f,g}{\Om}{V}$ und $\ffn{\lam}{\Om}{\KK}$.
  Es sei $p \in \Om$ und $f$ und $g$ seien \stetig{M,V} in $p$
  und $\lam$ sei \stetig{M,\KK} in $p$.
  Dann sind die folgenden Funktionen \stetig{M,V} in $p$:
  \begin{itemize}
  \item $f+g$
  \item $\lam f$
  \item $\frac{f}{\lam}$, wenn $0 \not\in \img(\lam)$.
  \end{itemize}
\end{satz}

\begin{proof}
  Übung (folgt leicht aus dem Komb'satz für konvergente Folgen).
\end{proof}

In dem obigen Kombinationssatz für Stetigkeit scheint zunächst eine Aussage für die Komposition zu fehlen.
Doch diese steht schon in \autoref{struct/metric:komp},
der ja auch für von Normen induzierte Metriken gilt.

Von ganz ähnlicher Form ist der folgende Satz, den wir der Vollständigkeit halber angeben:

\begin{satz}[Kombinationssatz für Funktionslimites]
  Es seien $M$ ein metrischer Raum, $V$ ein normierter Raum, und $\Om \subseteq M$.
  Es sei $p \in M$ ein Häufungspunkt von $\Om$ in $M$.
  Es seien $\ffn{f,g}{\Om}{V}$ und $\ffn{\lam}{\Om}{\KK}$ so,
  dass $\flimxx{x}{p}{M,V} f(x) \in V$ und $\flimxx{x}{p}{M,V} g(x) \in V$
  und $\flimxx{x}{p}{M,\KK} \lam(x) \in \KK$ gilt.
  Dann gilt:
  \begin{itemize}
  \item $\flimxx{x}{p}{M,V} f(x)+g(x) = \flimxx{x}{p}{M,V} f(x) + \flimxx{x}{p}{M,V} g(x)$
  \item $\flimxx{x}{p}{M,V} \lam(x) f(x) = \flimxx{x}{p}{M,\KK} \lam(x) \ccdot \flimxx{x}{p}{M,V} f(x)$
  \item $\flimxx{x}{p}{M,V} \frac{f(x)}{\lam(x)}
    = \frac{\flimxx{x}{p}{M,V} f(x)}{\flimxx{x}{p}{M,\KK} \lam(x)}$,
    wenn $0 \not\in \img(\lam)$.
  \end{itemize}
\end{satz}

\begin{proof}
  Übung (folgt leicht aus dem Komb'satz für konvergente Folgen).
\end{proof}

\begin{satz}
  \label{struct/norm:norm-stetig}
  Normen sind stetig.
  \textit{Genauer:} Es sei $V$ ein normierter Raum über~$\KK$.
  Dann ist $\normfn_{V}$ \stetig{V,\RR}.
\end{satz}

\begin{proof}
  Es sei $p \in V$ und $\seq{v_{k}}{k} \in \seqset{V}$ so, dass $\limxx{k}{V} v_{k} = p$ gilt.
  Es sei $\veps > 0$ und $k \geq \nzero{V}(\veps, v)$.
  Dann gilt $\abs[big]{\norm{v_{k}}_{V} - \norm{p}_{V}} \leq \norm{v_{k} - p}_{V} < \veps$,
  nach \autoref{struct/norm:folg}.
\end{proof}

\begin{satz}[Komponentensatz der Stetigkeit]
  \label{struct/norm:komp-stetig}
  Es sei $M$ ein metrischer Raum und $\ffn{f}{M}{\KK^{n}}$.
  Dann sind äquivalent:
  \begin{itemize}
  \item $f$ ist \stetig{M,(\KK^n,\normfn_\infty)}.
  \item Alle Komponentenfunktionen von $f$ sind \stetig{M,\KK}.\footnote{%
      Die \xte{i} Komponentenfunktion von $f$ ist $\ffnx{f_{i}}{M}{\KK}{v \mapsto (f(v))_{i}}$.}
  \end{itemize}
\end{satz}

\begin{proof}
  Übung (folgt leicht aus dem Komponentensatz der Konvergenz).
\end{proof}

\begin{satz}[Stetigkeit linearer Abbildungen]
  \label{struct/norm:linear}
  Es seien $V$ und $W$ normierte Räume über $\KK$ und $\ffn{\phi}{V}{W}$ \linear{V,W}.
  Dann sind äquivalent:
  \begin{enumerate}
  \item $\phi$ ist \lipstetig{V,W}.
  \item $\phi$ ist gleichmäßig \stetig{V,W}.
  \item $\phi$ ist \stetig{V,W}.
  \item $\phi$ ist \stetig{V,W} in $0$.
  \item $\exists c > 0 \innerholds \forall v \in V \holds \norm{\phi(v)}_{W} \leq c \norm{v}_{V}$
  \end{enumerate}
\end{satz}

\begin{proof}
  \imprefx*{1}{2} und \imprefx*{2}{3} und \imprefx*{3}{4} sind bekannt oder trivial.
  \par\imprefx{4}{5}
  Finde $\del > 0$ so, dass $\fnimg{\phi}(B_{V}(0,\del)) \subseteq B_{W}(0,1)$ gilt.
  Definiere $c \df \frac{2}{\del}$.
  Für alle $v \in \nz{V} = V \setminus \set{0}$ gilt $\frac{1}{c \norm{v}_{V}} v \in B_{V}(0,\del)$, also:
  \begin{IEEEeqnarray*}{0l}
    \norm{\phi(v)}_{W}
    = c \norm{v}_{V}
    \norm{\phi\p{\frac{1}{c \norm{v}_{V}} v}}_{W}
    \leq c \norm{v}_{V}
  \end{IEEEeqnarray*}
  Für $v=0$ ist die zu zeigende Abschätzung trivial.
  \par\imprefx{5}{1}
  Für alle $u,v \in V$ gilt $\norm{\phi(u) - \phi(v)}_{W} = \norm{\phi(u-v)}_{W}
  \leq c \norm{u-v}_{V}$.
\end{proof}

\begin{para}[Operatornorm]
  Es seien $V$ und $W$ normierte Räume über $\KK$ so,
  dass $\nz{V} \neq \emptyset$ gilt.
  Dann bildet mit den punktweisen Verknüpfungen die Menge der
  \linearen{V,W}, \stetigen{V,W} Abbildungen von $V$ nach $W$ einen Vektorraum über $\KK$;
  das folgt aus dem Kombinationssatz für Stetigkeit.
  Diesen Vektorraum bezeichnen wir mit $L(V,W)$.
  Definiere:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{\normfn_{\op(V,W)}}{L(V,W)}{\RRnn}{\phi
      \mapsto \sup_{v \in \nz{V}} \frac{\norm{\phi(v)}_{W}}{\norm{v}_{V}}}
  \end{IEEEeqnarray*}
  Nach dem vorigen Satz bildet $\normfn_{\op(V,W)}$ tatsächlich nach $\RRnn$ ab.
  Man sieht leicht (Übung), dass $\normfn_{\op(V,W)}$ eine Norm auf $L(V,W)$ ist,
  die wir die \term{Operatornorm} nennen.
\end{para}

\section{Kompaktheit}

\begin{satz}[Kompaktheit in $(\KK^{n}, \normfn_{\infty})$]
  \label{struct/norm:heine-borel}
  Definiere $V \df (\KK^{n}, \normfn_{\infty})$.
  Es sind für alle $K \subseteq \KK^{n}$ äquivalent:
  \begin{enumerate}
  \item $K$ ist in $V$ kompakt.
  \item $K$ ist in $V$ abgeschlossen und beschränkt.
    (Beschränktheit bedeutet, es gibt $r > 0$ so, dass $K \subseteq B_{V}(0,r)$ gilt.)
  \end{enumerate}
\end{satz}

\begin{proof}
  \imprefx{1}{2}
  Es sei $\seq{v_{n}}{n} \in \seqset{K}$ konvergent, notiere $p \df \limxx{k}{V} v_{k}$.
  Da $K$ kompakt ist, finde streng monoton steigendes $\sig$ so,
  dass $\limxx{k}{V} v_{\sig(k)} \in K$ gilt.
  Der Limes dieser Teilfolge ist aber auch $p$, also $p \in K$.
  Nach dem Folgenkriterium der Abgeschlossenheit ist also $K$ abgeschlossen.
  \par
  Für Widerspruch nehmen wir an, $K$ ist nicht beschränkt.
  Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall k \in \NNone \innerholds \exists v_{k} \in K \holds \norm{v_{k}}_{\infty} > k
  \end{IEEEeqnarray*}
  Jede Teilfolge von $\seq{v_{k}}{k}$ ist unbeschränkt,
  kann also nach \autoref{struct/norm:konv-beschraenkt} nicht konvergieren.
  Also ist $K$ nicht kompakt, ein Widerspruch.
  \par\imprefx{2}{1}
  Jede Komponentenfolge von $\seq{v_{k}}{k}$ ist beschränkt (in $\KK$).
  Wir wenden den Satz von Bolzano\-/Weierstraß, der für $\KK$ bekannt ist, iterativ an.
  Wir finden streng monoton steigendes $\sig_{1}$ so,
  dass $\limxx{k}{\KK} (v_{\sig_{1}(k)})_{1} \in \KK$ gilt.
  Dann finden wir streng monoton steigendes $\sig_{2}$ so,
  dass $\limxx{k}{\KK} (v_{(\sig_{2} \circ \sig_{1})(k)})_{2} \in \KK$ gilt;
  es gilt dann auch $\limxx{k}{\KK} (v_{(\sig_{2} \circ \sig_{1})(k)})_{1} \in \KK$.
  Setze so fort und erhalte $\eli{\sig}{n}$ so, dass
  die Folge $\seq{v_{(\sig_{n} \circ \hdots \circ \sig_{1})(k)}}{k}$
  komponentenweise konvergiert, also konvergiert sie nach \autoref{struct/norm:komp-konv}.
  Da $K$ abgeschlossen ist, ist der Limes Element von $K$.
  Wir haben also eine Teilfolge von $\seq{v_{k}}{k}$ mit Limes in $K$ gefunden.
\end{proof}

\section{Äquivalenz von Normen}

\begin{para}[Äquivalente Normen]
  Es seien $(V,\normfn$) und $(V,\normfn')$ normierte Räume über~$\KK$;
  wir haben also einen Vektorraum und darauf zwei Normen.
  Wir nennen $\normfn$ und $\normfn'$ \term{äquivalent},
  wenn es $\al, \bet > 0$ so gibt, dass gilt:
  \begin{IEEEeqnarray}{0l}
    \label{struct/norm:norm-equiv}
    \forall v \in V \holds \al \norm{v} \leq \norm{v}' \leq \bet \norm{v}
  \end{IEEEeqnarray}
  Ist dies der Fall, so gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall v \in V \holds \frac{1}{\bet} \norm{v}'
    \leq \norm{v}
    \leq \frac{1}{\al} \norm{v}'
  \end{IEEEeqnarray*}
  Also sind $\normfn$ und $\normfn'$ äquivalent genau dann,
  wenn $\normfn'$ und $\normfn$ äquivalent sind.
  Möchte man \eqref{struct/norm:norm-equiv} nachweisen,
  so kann man sich offenbar auf $v \in \nz{V}$ beschränken,
  da für $v=0$ die Abschätzungen trivialerweise erfüllt sind.
\end{para}

\begin{satz}[Bedeutung der Äquivalenz von Normen]
  Äquivalente Normen definieren dieselben Umgebungen,
  und damit dieselben offenen und abgeschlossenen Mengen,
  dieselben konvergenten Folgen mit denselben Limites,
  dieselben stetigen Funktionen und dieselben kompakten Mengen.
\end{satz}

\begin{proof}
  Es genügt, die Aussage über die Umgebungen zu zeigen,
  weil der Rest dann leicht aus den Definitionen folgt.
  \par
  Es seien $\normfn$ und $\normfn'$ äquivalente Normen auf eine Vektorraum $V$
  mit Konstanten $\al, \bet > 0$ wie in \eqref{struct/norm:norm-equiv}.
  Wir bezeichnen mit $B$ und $\nsets$ die Kugeln \bzw Mengen von Umgebungen bezüglich $\normfn$
  und mit $B'$ und $\nsets'$ die Kugeln \bzw Mengen von Umgebungen bezüglich $\normfn'$.
  Es seien $p \in \Om \subseteq V$.
  \begin{itemize}
  \item Zu zeigen ist $\Om \in \nsets(p) \implies \Om \in \nsets'(p)$.
    Es gelte $\Om \in \nsets(p)$.
    Dann finde $\veps > 0$ so, dass $B(p,\veps) \subseteq \Om$ gilt.
    Definiere $\veps' \df \al \veps$, und es sei $y \in B'(p,\veps')$.
    Dann gilt:
    \begin{IEEEeqnarray*}{0l}
      \norm{y-p} \leq \frac{1}{\al} \norm{y-p}' < \frac{1}{\al} \veps' = \frac{1}{\al} \al \veps = \veps
    \end{IEEEeqnarray*}
    Es folgt $y \in B(p,\veps)$ und damit $y \in \Om$.
    Insgesamt also $B'(p,\veps') \subseteq \Om$.
  \item Zu zeigen ist $\Om \in \nsets'(p) \implies \Om \in \nsets(p)$.
    Die Details laufen analog zum vorigen Punkt und sind zur Übung überlassen.
    \qedhere
  \end{itemize}
\end{proof}

\begin{satz}
  Auf einem endlich\-/dimensionalen Vektorraum sind alle Normen äquivalent.
\end{satz}

\begin{proof}
  Wir behandeln zunächst nur die Vektorräume $\KK^{n}$ und zeigen später,
  wie man den allgemeinen (endlich\-/dimensionalen) Fall darauf zurück führt.
  Es sei $\normfn$ eine Norm auf $\KK^{n}$.
  Wir zeigen, dass diese äquivalent zu $\normfn_{\infty}$ ist,
  indem wir $\al,\bet > 0$ so finden, dass gilt:
  \begin{IEEEeqnarray}{0l}
    \label{struct/norm:norm-equiv-2}
    \forall v \in \KK^{n} \holds \al \norm{v}_{\infty} \leq \norm{v} \leq \bet \norm{v}_{\infty}
  \end{IEEEeqnarray}
  Es seien $\eli{e}{n}$ die bekannten Vektoren der Standardbasis des $\KK^{n}$
  (als Vektorraum über $\KK$).
  Definiere $\bet \df \sum_{i=1}^{n} \norm{e_{i}}$.
  Dann gilt für alle $v \in \KK^{n}$:
  \begin{IEEEeqnarray*}{0l}
    \norm{v} = \norm{\sum_{i=1}^{n} v_{i} e_{i}}
    \leq \sum_{i=1}^{n} \abs{v_{i}} \ccdot \norm{e_{i}}
    \leq \sum_{i=1}^{n} \norm{v}_{\infty} \norm{e_{i}}
    = \bet \norm{v}_{\infty}
  \end{IEEEeqnarray*}
  Damit haben wir schon den rechten Teil von \eqref{struct/norm:norm-equiv-2}.
  Ferner folgt mit \autoref{struct/norm:folg}:
  \begin{IEEEeqnarray*}{0l}
    \abs[big]{\norm{u}-\norm{v}} \leq \norm{u-v} \leq \bet \norm{u-v}_{\infty}
  \end{IEEEeqnarray*}
  Also ist $\normfn$ \lipstetig{(\KK^n,\normfn_{\infty}), \RR},
  also auch stetig bezüglich dieser normierten Räume ($\RR$ ist ja mit $\absfn$ als Norm zu verstehen).
  Definiere:
  \begin{IEEEeqnarray*}{0l}
    K \df \set{v \in \KK^{n} \suchthat \norm{v}_{\infty} = 1}
  \end{IEEEeqnarray*}
  Nach \autoref{struct/norm:norm-stetig} ist $\normfn_{\infty}$
  \stetig{(\KK^n,\normfn_{\infty}), \RR},
  also ist $K$ abgeschlossen in $(\KK^n,\normfn_{\infty})$,
  da es Urbildmenge der in $\RR$ abgeschlossen Menge $\set{1}$
  unter einer \stetigen{(\KK^n,\normfn_{\infty}), \RR} Funktion ist.
  Da $K \subseteq B_{(\KK^n,\normfn_{\infty})}(0,2)$ gilt,
  ist $K$ außerdem beschränkt.
  Nach \autoref{struct/norm:heine-borel} ist $K$ damit kompakt in $(\KK^n,\normfn_{\infty})$.
  Also nimmt $\normfn$ auf $K$ ihr Minimum an, wir finden also $v_{*} \in K$ so,
  dass $\al \df \norm{v_{*}} \leq \norm{u}$ für alle $u \in K$ gilt.
  Es gilt $\al > 0$, da $v_{*} \neq 0$ gilt.
  \par
  Für alle $v \in \nz{\KK^{n}}$ folgt:
  \begin{IEEEeqnarray*}{0l}
    \norm{v} = \norm{\frac{v}{\norm{v}_{\infty}}} \ccdot \norm{v}_{\infty} \geq \al \norm{v}_{\infty}
  \end{IEEEeqnarray*}
  Das schließt den Beweis für $\KK^{n}$,
  und wir wenden uns einem allgemeinen, endlich\-/dimensionalen Vektorraum $V$ über $\KK$ zu.
  Es seien $\normfn_{V}$ und $\normfn_{V}'$ Normen auf~$V$.
  Wir finden einen Isomorphismus $\ffn{\phi}{\KK^{n}}{V}$ zwischen $\KK^{n}$ und $V$.
  Nach \autoref{struct/norm:norm-linabb} sind $\normfn \df \normfn_{V} \circ \phi$
  und $\normfn' \df \normfn'_{V} \circ \phi$ Normen auf $\KK^{n}$.
  Also finden wir $\al,\bet > 0$ so, dass gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall v \in \KK^{n} \holds \al \norm{v} \leq \norm{v}' \leq \bet \norm{v}
  \end{IEEEeqnarray*}
  Für alle $v \in V$ folgt:
  \begin{IEEEeqnarray*}{0l}
    \al \norm{v}_{V}
    = \al \norm{\phi(\phi^{-1}(v))}_{V}
    = \al \norm{\phi^{-1}(v)} \\
    \eqin \leq \norm{\phi^{-1}(v)}'
    = \norm{\phi(\phi^{-1}(v))}_{V}'
    = \norm{v}_{V}'
  \end{IEEEeqnarray*}
  Ebenso:
  \begin{IEEEeqnarray*}{0l+x*}
    \norm{v}_{V}'
    = \norm{\phi(\phi^{-1}(v))}_{V}'
    = \norm{\phi^{-1}(v)}' \\
    \eqin \leq \bet \norm{\phi^{-1}(v)}
    = \bet \norm{\phi(\phi^{-1}(v))}_{V}
    = \bet \norm{v}_{V}
    & \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\begin{corollary}
  Die Komponentensätze der Konvergenz und der Stetigkeit
  (\autoref{struct/norm:komp-konv} und \autoref{struct/norm:komp-stetig}) gelten
  mit jeder Norm auf $\KK^{n}$ anstelle von $\normfn_{\infty}$.
\end{corollary}
