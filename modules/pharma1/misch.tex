\chapter{Mischungsrechnen}
\label{pharma1/misch}

\section{Rechnen mit Massen}

\begin{para}[Massenanteil]
  Es sei eine (chemische) Lösung mit Masse $m$ gegeben.
  Wir sagen, dass der Stoff $S$ daran den \term{Massenanteil} $w$ hat,
  wenn sich in der Lösung genau die Masse $wm$ des Stoffes $S$ befindet.
  Der Massenanteil ist eine Zahl im Intervall~$\intcc{0}{1}$.
  Bei der Angabe wird oft eine Schreibweise mit dem Prozentzeichen~$\%$ verwendet,
  beachte dazu, dass für alle $x \in \RR$ gilt $x\% = \frac{x}{100}$.
  Haben wir zum Beispiel $\SI{1.32}{\kg}$ einer Lösung mit $8\%$ Ethanol (Massenanteil),
  so liegen in dieser Lösung
  $8\% \cdot \SI{1.32}{\kg} = 0.08 \cdot \SI{1.32}{\kg} = \SI{0.1056}{\kg}$ Ethanol vor.
\end{para}

\begin{para}[Masterformel]
  Es seien $n$ Lösungen gegeben, sowie für einen bestimmten Stoff $S$
  der Massenanteil $w_i \in \intcc{0}{1}$, den $S$ an der Lösung $i$ hat, für alle $i \in \setn{n}$.
  Wir erzeugen eine neue Lösung, indem wir von jeder Lösung $i \in \setn{n}$ die Masse $m_i$ zusammenführen,
  und interessieren uns für den Massenanteil $w$ vom Stoff $S$ an der neuen Lösung.
  Betrachte folgende Formel für $w$:
  \begin{IEEEeqnarray}{0l}
    \label{pharma1/misch:massen:annahme}
    w = \frac{\sum_{i=1}^n w_i m_i}{\sum_{i=1}^n m_i}
  \end{IEEEeqnarray}
  Diese Formel beschreibt die Wirklichkeit passend,
  solange keine Prozesse (zum Beispiel chemische Reaktionen) stattfinden,
  durch die mehr von dem Stoff $S$ entsteht oder etwas von dem Stoff $S$ verbraucht wird,
  oder durch die es zu Massenveränderungen kommt.
  Denn dann steht im Zähler die Masse von Stoff $S$ in der neuen Lösung,
  und im Nenner steht die gesamte Masse der neuen Lösung.
  Wir werden stets stillschweigend voraussetzen,
  dass keine solchen Prozesse stattfinden;
  in Anwendungen ist dies mit naturwissenschaftlichem Sachverstand zu beurteilen.
  Natürlich ist~\eqref{pharma1/misch:massen:annahme} nur sinnvoll, wenn $\sum_{i=1}^n m_i > 0$ gilt,
  d.h. wenigstens von einer der Lösungen wird eine positive Masse zugeführt,
  was offenbar in allen Anwendungen erfüllt sein wird.
\end{para}

\begin{example}
  $\SI{100}{g}$ einer Lösung mit $70\%$ Ethanol (Massenanteil)
  und $\SI{150}{g}$ einer Lösung mit $40\%$ Ethanol (Massenanteil)
  werden zusammengefügt.
  Was ist der Massenanteil von Ethanol an der neuen Lösung?
  \par
  \textit{Antwort}. Es gilt $n=2$ und $w_1 = 0.7$ und $w_2 = 0.4$,
  ferner $m_1 = \SI{100}{g}$ und $m_2 = \SI{150}{g}$.
  Die Angabe $w_1 = 0.7$ zum Beispiel sagt, dass in einem Gramm der Lösung $\SI{0.7}{g}$ Ethanol sind.
  Also gilt für den gesuchten Massenanteil~$w$:
  \begin{IEEEeqnarray*}{0l}
    w = \frac{0.7 \cdot \SI{100}{g} + 0.4 \cdot \SI{150}{g}}{\SI{100}{g}+\SI{150}{g}} = 0.52 = 52\%
  \end{IEEEeqnarray*}
\end{example}

\begin{para}[Herstellung einer Lösung]
  Es stehen zwei Lösungen mit bekannten Massenanteilen $w_{1}$ und $w_{2}$ des Stoffes $S$ zur Verfügung,
  und es soll eine Lösung mit Masse $t > 0$ erzeugt werden,
  an welcher der Stoff $S$ den Massenanteil $w$ hat.
  Welche Masse $m_{1}$ von Lösung~$1$ und welche Masse $m_{2}$ von Lösung~$2$ leisten das,
  wenn wir sie zusammenführen?
  \par
  Zunächst betrachten wir einige Spezialfälle.
  Wenn $w_{1} = w_{2}$ gilt, dann hat jede Mischung aus den beiden Lösungen den Massenanteil $w_{1}$,
  also gibt es in diesem Fall eine Problemlösung\footnote{%
    Das Wort verwenden wir in diesem Kapitel,
    damit es keine Verwechslungen mit \enquote{Lösung} im chemischen Sinne gibt.}
  nur, wenn $w_{1} = w$ gilt;
  und dann ist das Problem offenbar trivial:
  Die gewünsche Lösung liegt schon als Lösung~$1$ und als Lösung~$2$ vor.
  Wir können uns also auf den Fall $w_{1} \neq w_{2}$ beschränken.
  \par
  Wenn $w_{1}=w$ oder $w_{2}=w$ gilt, ist auch klar, was wir zu tun haben:
  Man nehme nur Lösung~$1$ \bzw nur Lösung~$2$.
  Wir können uns also auf ${w_{1} \neq w} \land {w_{2} \neq w}$ beschränken.
  Unter dieser Bedingung ist klar, dass weder $m_{1}=0$ noch $m_{2}=0$ für eine Problemlösung in Frage kommen,
  wir können uns also ferner auf $m_{1}, m_{2} > 0$ beschränken.
  Es ergeben sich insgesamt folgende Rahmenbedingungen:
  \begin{IEEEeqnarray}{0l}
    \label{pharma1/misch:massen:rahmen}
    w_{1}, w_{2}, w \in \intcc{0}{1}
    \,\land\, w_{1} \neq w_{2}
    \,\land\, w_{1} \neq w
    \,\land\, w_{2} \neq w
    \,\land\, m_{1}, m_{2} > 0
  \end{IEEEeqnarray}
  Für alle $w_{1}, w_{2}, w, m_{1}, m_{2}$,
  welche \eqref{pharma1/misch:massen:rahmen} erfüllen,
  und alle $t > 0$ gilt:
  \begin{IEEEeqnarray}{0l}
    w = \frac{w_1 m_1 + w_2 m_2}{m_1 + m_2} \, \land \, m_1+m_2 = t \nonumber \\
    \eqin \iff w = \frac{w_1 m_1 + w_2 m_2}{t} \, \land \, m_2 = t - m_1 \nonumber \\
    \eqin \iff wt = w_1 m_1 + w_2 (t - m_1) \, \land \, m_2 = t - m_1 \nonumber \\
    \eqin \iff wt = (w_1 - w_2) m_1 + w_2 t \, \land \, m_2 = t - m_1 \nonumber \\
    \eqin \iff (w - w_2) t = (w_1 - w_2) m_1 \, \land \, m_2 = t - m_1 \nonumber \\
    \eqin \iff m_1 = \frac{w - w_2}{w_1 - w_2} t \, \land \, m_2 = t - m_1 \label{pharma1/misch:massen:m12}
  \end{IEEEeqnarray}
  Man bemerke, wie~\eqref{pharma1/misch:massen:rahmen} sicherstellt,
  dass bei dieser Rechnung nirgends durch~$0$ geteilt wird.
  \par
  Wenn überhaupt eine Problemlösung
  im Rahmen von~\eqref{pharma1/misch:massen:rahmen} möglich ist,
  dann sieht sie so aus wie in~\eqref{pharma1/misch:massen:m12} angegeben.
  In einer konkreten Situation können wir in~\eqref{pharma1/misch:massen:m12}
  unsere Zahlen für $w_{1},w_{2},w,t$ eingeben
  und $m_{1}$ und $m_{2}$ erhalten.
  Wenn dann tatsächlich $m_{1}, m_{2} > 0$ gilt,
  so haben wir eine Problemlösung gefunden;
  ist aber $m_{1} \leq 0 \lor m_{2} \leq 0$,
  dann ist keine Problemlösung im Rahmen von~\eqref{pharma1/misch:massen:rahmen} möglich.
\end{para}

\begin{example}
  \begin{enumerate}
  \item Es stehen Lösungen mit $35\%$ und mit $80\%$ Ethanol (Massenanteil) zur Verfügung.
    Es sollen $\SI{2}{\kg}$ mit $45\%$ Ethanol (Massenanteil) erzeugt werden.
    Der vorige Absatz liefert die Problemlösung:
    \begin{IEEEeqnarray*}{0l}
      m_1 = \frac{0.45 - 0.8}{0.35 - 0.8} \cdot \SI{2}{\kg} = \frac{14}{9} \si{kg} \approx \SI{1.556}{\kg} \\
      m_2 = \SI{2}{kg} - \frac{14}{9} \si{kg} = \frac{4}{9} \si{kg} \approx \SI{0.4444}{\kg}
    \end{IEEEeqnarray*}
  \item Es stehen Lösungen mit $50\%$ und mit $80\%$ Ethanol (Massenanteil) zur Verfügung.
    Es sollen wieder $\SI{2}{\kg}$ mit $45\%$ Ethanol (Massenanteil) erzeugt werden.
    Der vorige Absatz liefert als einzigen Kandidaten für eine Problemlösung:
    \begin{IEEEeqnarray*}{0l+l}
      m_{1} = \frac{7}{3} \si{kg} &
      m_{2} = - \frac{1}{3} \si{kg}
    \end{IEEEeqnarray*}
    Wegen $m_{2} < 0$ ist also keine Problemlösung möglich.
  \end{enumerate}
\end{example}

\begin{para}[Herstellung einer Lösung -- Mischungsverhältnis]
  Es stehen zwei Lösungen mit bekannten Massenanteilen $w_{1}$ und $w_{2}$ des Stoffes $S$ zur Verfügung,
  und es soll eine Lösung erzeugt werden,
  an welcher der Stoff $S$ den Massenanteil $w$ hat.
  Gesucht ist das Verhältnis, in dem wir Massen der Lösungen~$1$ und~$2$ zusammenführen müssen,
  um das zu erreichen.
  Ganz ähnlich wie zuvor behandelt man die Spezialfälle ${w_{1}=w_{2}}$ und ${w_{1}=w}$ und ${w_{2}=w}$.
  Wir können uns also von nun an wieder im Rahmen von~\eqref{pharma1/misch:massen:rahmen} bewegen.
  Für alle $w_{1}, w_{2}, w, m_{1}, m_{2}$,
  welche \eqref{pharma1/misch:massen:rahmen} erfüllen, gilt:
  \begin{IEEEeqnarray*}{0l}
    w = \frac{w_1 m_1 + w_2 m_2}{m_1 + m_2}
    \iff w \parens{m_1 + m_2} = w_1 m_1 + w_2 m_2 \\*
    \eqin \iff (w-w_1) m_1 = (w_2-w) m_2
    \iff \frac{m_1}{m_{2}} = \frac{w_2-w}{w-w_1}
  \end{IEEEeqnarray*}
  Man bemerke, wie~\eqref{pharma1/misch:massen:rahmen} wieder sicherstellt,
  dass bei dieser Rechnung nirgends durch~$0$ geteilt wird.
\end{para}

\begin{example}
  In welchem Verhältnis können wir eine Lösung mit $20\%$ Kochsalz
  mit einer Lösung mit $55\%$ Kochsalz (Massenanteil) mischen,
  um eine Lösung mit $30\%$ Kochsalz (Massenanteil) zu erhalten?
  Der vorige Absatz liefert $\frac{m_1}{m_2} = \frac{0.55-0.3}{0.3-0.2} = \frac{0.25}{0.1} = \frac{5}{2}$;
  das erforderliche Verhältnis ist also $5:2$.
\end{example}

\section{Rechnen mit Volumen}

\begin{para}[Volumenveränderung]
  Führen wir eine Lösung~1 mit Volumen~$V_1$ und eine Lösung~2 mit Volumen~$V_2$ zusammen,
  so braucht das Volumen $V$ der neuen Lösung nicht $V_1 + V_2$ zu sein.
  Es kann vielmehr zu Volumenkontraktion ($V < V_1 + V_2$) oder Volumendilatation $(V > V_1 + V_2)$ kommen.
  Das macht das Rechnen mit Volumen anstelle von Masse komplizierter.
  Möchte man die Volumenveränderungen vernachlässigen,
  so kann man die Formeln aus dem vorigen Abschnitt anstelle für Massen und Massenanteile
  auch für Volumen und etwa die unten eingeführte Volumenkonzentration verwenden.
  Andernfalls, wenn man die Volumenveränderungen berücksichtigen möchte,
  ist man auf Tabellen angewiesen, die Informationen über die Volumenveränderung enthalten.
  Das sehen wir uns nun genauer an.
\end{para}

\begin{para}[Volumenkonzentration]
  Es bestehe eine Lösung~1 mit Volumen $V_S$ ganz aus dem Stoff $S$, der uns interessiert,
  und in einer Lösung~2 komme $S$ nicht vor.
  Es sei $V$ das Volumen der Lösung, die durch Zusammenführen von Lösung~1 und Lösung~2 entsteht.
  Dann sagen wir, dass $S$ in dieser neuen Lösung
  die \term{Volumenkonzentration} $\sig = \frac{V_S}{V}$ hat.
  Diese Größe ist grundsätzlich temperaturabhängig.
  Wie im vorigen Absatz erklärt, braucht $V$ nicht genau die Summe aus $V_{S}$
  und dem Volumen von Lösung~2 zu sein.
  (Es gibt auch noch den Volumenanteil, der etwas anders definiert ist.
  Volumenkonzentration und Volumenanteil werden manchmal verwechselt;
  es ist also stets genau darauf zu achten, was tatsächlich gemeint ist.)
\end{para}

\begin{para}[Nur zwei Stoffe]
  Für den Rest dieses Abschnittes betrachten wir nur Lösungen aus einem Stoff~$S$
  und genau einem weiteren Stoff~$L$.
  In den Beispielen wird $S$ Ethanol sein und $L$ Wasser.
  Tabellen für Ethanol\-/Wasser\-/Lösungen findet man etwa in~\cite[Section~5.5]{PhEur8};
  den für unsere Beispiele relevanten Ausschnitt geben wir in~\autoref{pharma1/misch:tab-ethanol}.
\end{para}

\begin{para}[Umrechnung Volumenkonzentration, Dichte, Massenanteil]
  Wir nehmen ferner an, dass es eine injektive Funktion $\ffn{\rho}{\RRnn}{\RRpos}$ so gibt,
  dass $\rho(\sig)$ die Dichte einer Lösung (aus $S$ und $L$) ist,
  in der $S$ die Volumenkonzentration $\sig$ hat.
  Es bezeichne $\rho_{S} > 0$ die Dichte von $S$.
  (Dichten können temperaturabhängig sein, darauf ist zu achten.
  Alle von uns zur Zeit betrachteten Dichten mögen sich auf dieselbe Temperatur beziehen.)
  Es ergibt sich eine weitere Funktion,
  die zu gegebener Volumenkonzentration von $S$ den Massenanteil von $S$ berechnet, nämlich:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{w}{\RRnn}{\intcc{0}{1}}{\sig \mapsto \frac{\rho_{S} \sig}{\rho(\sig)}}
  \end{IEEEeqnarray*}
  Denn dass $S$ die Volumenkonzentration $\sig$ in einer Lösung mit Volumen $V > 0$ hat,
  bedeutet ja, dass darin die Masse $\rho_{S} \sig V$ von $S$ vorhanden ist,
  und die Masse der gesamten Lösung ist $\rho(\sig) V$.
  Wir nehmen an, dass auch die Funktion~$w$ injektiv ist.
  Bei Ethanol\-/Wasser\-/Lösungen sind die Injektivität von $\rho$ und $w$ augenscheinlich gegeben,
  wenn man die Tabellen in~\cite[Section~5.5]{PhEur8} betrachtet.
\end{para}

\begin{para}[Umweg über Massen]
  \label{pharma1/misch:vol:umweg}
  Es seien $n$ Lösungen gegeben,
  in denen der Stoff $S$ die Volumenkonzentrationen $\eli{\sig}{n} \in \RRnn$ hat.
  Es werden Volumina $\eli{V}{n}$ dieser Lösungen zusammengeführt.
  Was ist die Volumenkonzentration von $S$ in dieser neuen Lösung?
  \par
  Wir lösen dies, indem wir einen Umweg über die Massen gehen.
  Der Massenanteil $\tiw$ von $S$ an der neuen Lösung ist:
  \begin{IEEEeqnarray*}{0l}
    \tiw = \frac{\rho_S \cdot \sum_{i=1}^n \sig_i V_i}{\sum_{i=1}^n \rho(\sig_i) V_i}
  \end{IEEEeqnarray*}
  Dies ist tatsächlich der Massenanteil: Im Zähler steht die Masse von Stoff~$S$, die wir insgesamt erreichen,
  und im Nenner steht die Masse der neuen Lösung.
  Natürlich ist diese Formel nur dann sinnvoll, wenn $\sum_{i=1}^n \rho(\sig_i) V_i > 0$ gilt,
  d.h. wenigstens eine der Lösungen hat eine positive Dichte,
  und von dieser Lösung wird ein positives Volumen zugeführt,
  was offenbar in allen Anwendungen der Fall sein wird.
  Die Antwort auf die oben gestellte Frage ist nun $w^{-1}(\tiw)$,
  also die Umkehrfunktion von $w$ ausgewertet an der Stelle $\tiw$,
  wobei wir die Berechnung dieser Zahl typischerweise mit einem Blick in eine Tabelle erledigen.
\end{para}

\begin{table}[t]
  \tablestandard
  \begin{tabular}{n{3}{1}n{3}{2}n{3}{2}}
    \toprule
    {$\% V/V$} & {$\% m/m$} & {$\rho$ in $\si{\kg\per\m^3}$} \\
    \midrule
    0.0   &   0.00 & 998.20 \\
    10.0  &   8.01 & 984.71 \\
    20.0  &  16.21 & 973.56 \\
    43.0  &  35.99 & 943.06 \\
    45.0  &  37.80 & 939.54 \\
    57.2  &  49.32 & 915.27 \\
    57.3  &  49.42 & 915.05 \\
    62.0  &  54.09 & 904.60 \\
    73.7  &  66.39 & 876.18 \\
    73.8  &  66.50 & 875.92 \\
    94.0  &  91.01 & 815.18 \\
    100.0 & 100.00 & 789.24 \\
    \bottomrule
  \end{tabular}
  \caption{\label{pharma1/misch:tab-ethanol}Auszug aus der Tabelle
    für Lösungen aus Ethanol und Wasser aus~\cite[Section~5.5]{PhEur8}.
    Die linke Spalte gibt die Volumenkonzentration, die mittlere den Massenanteil, und die letzte die Dichte,
    bei Temperatur $\SI{20}{\celsius}$.}
\end{table}

\begin{example}
  Gegeben sind $\SI{100}{\ml}$ einer Ethanol\-/Wasser\-/Lösung
  mit ${\sig_1 = 10\%}$ Ethanol (Volumenkonzentration)
  und $\SI{120}{\ml}$ einer Ethanol\-/Wasser\-/Lösung mit ${\sig_2 = 94\%}$ Ethanol (Volumenkonzentration).
  Diese wollen wir zusammenführen.
  Die Temperatur nehmen wir hier und in den weiteren Beispielen als $\SI{20}{\celsius}$ an.
  Welche Volumenkonzentration Ethanol hat die neue Lösung?
  \par
  Der Massenanteil $\tiw$ von Ethanol an der neuen Lösung ist:
  \begin{IEEEeqnarray*}{0l}
    \tiw = \frac{\rho_{\mathrm{Ethanol}} \cdot \p{\sig_{1} V_{1} + \sig_{2} V_{2}}}%
    {\rho(\sig_1) V_1 + \rho(\sig_2) V_2} \\
    \eqin = \frac{\SI{0.78924}{\g/\ml} \cdot \p{0.1 \cdot \SI{100}{\ml} + 0.94 \cdot \SI{120}{\ml}}}%
    {\SI{0.98471}{\g/\ml} \cdot \SI{100}{\ml} + \SI{0.81518}{\g/\ml} \cdot \SI{120}{\ml}}
    \approx 49.37 \%
  \end{IEEEeqnarray*}
  Wir müssen nun $w^{-1}(\tiw)$ berechnen, die Tabelle gibt das aber nicht her.
  Daher interpolieren wir zwischen den Volumenkonzentrationen
  für die Massenanteil $49.32\%$ und $49.42\%$, für welche die Tabelle je eine Zeile hat.
  Als Ergebnis erhalten wir die Volumenkonzentration $\sig = 57.25\%$
  von Ethanol in der neuen Lösung.
  \par
  Auch die Dichte $\rho(\sig)$
  der neuen Lösung können wir der Tabelle durch Interpolation näherungsweise entnehmen,
  nämlich $\SI{0.9152}{\g\per\ml}$.
  Damit können wir auf das Volumen $V$ der neuen Lösung schließen:
  \begin{IEEEeqnarray*}{0l}
    V = \frac{\rho(\sig_1) V_1 + \rho(\sig_2) V_2}{\rho(\sig)} \approx \SI{214.5}{\ml}
  \end{IEEEeqnarray*}
  Daran erkennt man die Volumenkontraktion, denn die Summe der Volumina
  $\SI{100}{\ml}$ und $\SI{120}{\ml}$ der beiden zusammengeführten Lösungen ist $\SI{220}{\ml}$.
\end{example}

\begin{example}
  Es werden ${V_1 = \SI{500}{\ml}}$ Ethanol und ${V_2 = \SI{200}{\ml}}$ Wasser zusammengeführt.
  Welche Volumenkonzentration Ethanol hat die neue Lösung?
  \par
  Wir haben $\sig_{1} = 1$ und $\sig_{2} = 0$,
  also $\rho(\sig_{1}) = \rho_{\mathrm{Ethanol}} = \SI{0.78924}{\g/\ml}$
  und $\rho(\sig_{1}) = \rho_{\mathrm{Wasser}} = \SI{0.99820}{\g/\ml}$.
  Der Massenanteil $\tiw$ von Ethanol an der neuen Lösung ist:
  \begin{IEEEeqnarray*}{0l}
    \tiw = \frac{\rho_{\mathrm{Ethanol}} \cdot \p{1 \cdot V_{1} + 0 \cdot V_{2}}}%
    {\rho_{\mathrm{Ethanol}} \cdot V_1 + \rho_{\mathrm{Wasser}} \cdot V_2}
    = \frac{\rho_{\mathrm{Ethanol}} \cdot V_{1}}%
    {\rho_{\mathrm{Ethanol}} \cdot V_1 + \rho_{\mathrm{Wasser}} \cdot V_2}
    \approx 66.41 \%
  \end{IEEEeqnarray*}
  Aus der Tabelle lesen wir die zugehörige Volumenkonzentration $73.7\%$ ab
  (auf Interpolation verzichten wir hier).
\end{example}

\begin{para}[Herstellung einer Lösung]
  \label{pharma1/misch:vol:ziel}
  Es seien zwei Lösungen gegeben, in denen $S$ mit Volumenkonzentration $\sig_1 > 0$ \bzw $\sig_2$ vorkommt.
  Das Ziel sei, eine Lösung mit Volumen $V > 0$ zu erzeugen, in welcher $S$ die Volumenkonzentration~$\sig$ hat.
  Gegeben sind also $\sig_{1} > 0$ und $\sig_{2}, \sig \geq 0$ und $V > 0$;
  und gesucht sind $V_{1}, V_{2} \geq 0$ so, dass gilt:
  \begin{enumerate}
  \item Durch Zusammenführen eines Volumens von $V_{1}$ der Lösung~$1$
    und eines Volumens von $V_{2}$ der Lösung~$2$ entsteht eine Lösung,
    in der $S$ die Volumenkonzentration $\sig$ hat.
    Dies ist nach unseren Annahmen äquivalent dazu,
    dass an einer solchen Lösung der Stoff $S$ den Massenanteil $w(\sig)$ hat,
    was gleich $\frac{\rho_{S} \sig}{\rho(\sig)}$ ist.
  \item Durch Zusammenführen eines Volumens von $V_{1}$ der Lösung~$1$
    und eines Volumens von $V_{2}$ der Lösung~$2$ entsteht eine Lösung
    mit Volumen~$V$.
  \end{enumerate}
  Wir wissen, dass durch Zusammenführen eines Volumens von $V_{1}$ der Lösung~$1$
  und eines Volumens von $V_{2}$ der Lösung~$2$
  eine Lösung entsteht, \dots
  \begin{itemize}
  \item die Masse $\rho(\sig_1) V_1 + \rho(\sig_2) V_2$ hat;
  \item an welcher der Stoff $S$ den Massenanteil
    $\frac{\rho_{S} \p{\sig_{1} V_{1} + \sig_{2} V_{2}}}{\rho(\sig_1) V_1 + \rho(\sig_2) V_2}$ hat.
  \end{itemize}
  Für alle $V_{1}, V_{2} \geq 0$ gilt somit:
  \begin{IEEEeqnarray}{0l}
    (1) \land (2)
    \iff (1)
    \, \land \, \rho(\sig) V = \rho(\sig_1) V_1 + \rho(\sig_2) V_2 \nonumber \\
    \eqin \iff w(\sig)
    = \frac{\rho_{S} \p{\sig_{1} V_{1} + \sig_{2} V_{2}}}{\rho(\sig_1) V_1 + \rho(\sig_2) V_2}
    \, \land \, \rho(\sig) V = \rho(\sig_1) V_1 + \rho(\sig_2) V_2 \nonumber \\
    \eqin \iff \frac{\rho_{S} \sig}{\rho(\sig)}
    = \frac{\rho_{S} \p{\sig_{1} V_{1} + \sig_{2} V_{2}}}{\rho(\sig_1) V_1 + \rho(\sig_2) V_2}
    \, \land \, \rho(\sig) V = \rho(\sig_1) V_1 + \rho(\sig_2) V_2 \nonumber \\
    \eqin \iff \sig = \frac{\sig_1 V_1 + \sig_2 V_2}{V}
    \,\land\, \rho(\sig) V = \rho(\sig_1) V_1 + \rho(\sig_2) V_2 \nonumber \\
    \eqin \iff V_1 = \frac{\sig V - \sig_2 V_2}{\sig_1}
    \,\land\, \rho(\sig) V = \rho(\sig_1) \frac{\sig V - \sig_2 V_2}{\sig_1} + \rho(\sig_2) V_2 \nonumber \\
    \eqin \iff V_1 = \frac{\sig V - \sig_2 V_2}{\sig_1}
    \,\land\, \rho(\sig) V = \frac{\rho(\sig_1) \sig}{\sig_1} V
    + \frac{\rho(\sig_2) \sig_1 - \rho(\sig_1) \sig_2}{\sig_1} V_2 \nonumber \\
    \eqin \iff V_1 = \frac{\sig V - \sig_2 V_2}{\sig_1}
    \,\land\, \rho(\sig) \sig_{1} V
    = \rho(\sig_1) \sig V + \p{\rho(\sig_2) \sig_1 - \rho(\sig_1) \sig_2} V_2 \nonumber \\
    \eqin \iff V_1 = \frac{\sig V - \sig_2 V_2}{\sig_1}
    \,\land\, \p{\rho(\sig) \sig_{1} - \rho(\sig_1) \sig} V
    =  \p{\rho(\sig_2) \sig_1 - \rho(\sig_1) \sig_2} V_2 \nonumber \\
    \eqin \iff V_1 = \frac{\sig V - \sig_2 V_2}{\sig_1}
    \,\land\, V_{2} = \frac{\rho(\sig) \sig_{1} - \rho(\sig_1) \sig}{\rho(\sig_2) \sig_1 - \rho(\sig_1) \sig_2} V
    \label{pharma1/misch:vol:V1V2}
  \end{IEEEeqnarray}
  Die letzte Umformung ist nur möglich, wenn $\rho(\sig_2) \sig_1 - \rho(\sig_1) \sig_2 \neq 0$ gilt.
  Wir können uns auf diesen Fall beschränken, denn:
  \begin{itemize}
  \item Wenn $\sig_{2} \neq 0$ gilt, dann gilt, da wir $w$ als injektiv annehmen:
    \begin{IEEEeqnarray*}{0l}
      \rho(\sig_2) \sig_1 - \rho(\sig_1) \sig_2 = 0
      \iff \frac{\rho(\sig_2)}{\sig_2} - \frac{\rho(\sig_1)}{\sig_{1}} = 0 \\
      \eqin \iff \frac{\rho_{S}\rho(\sig_2)}{\sig_2} - \frac{\rho_{S}\rho(\sig_1)}{\sig_{1}} = 0
      \iff w(\sig_{2}) = w(\sig_{1})
      \iff \sig_{2} = \sig_{1}
    \end{IEEEeqnarray*}
    Das heißt, wäre $\rho(\sig_2) \sig_1 - \rho(\sig_1) \sig_2 = 0$,
    so wären wir in einem trivial zu behandelnden Fall.
  \item Wenn $\sig_{2} = 0$ gilt, dann gilt wegen $\sig_{1} > 0$:
    \begin{IEEEeqnarray*}{0l}
      \rho(\sig_2) \sig_1 - \rho(\sig_1) \sig_2 = 0
      \iff \rho(\sig_2) \sig_1 = 0
      \iff \rho(\sig_2) = 0
    \end{IEEEeqnarray*}
    Letzteres ist aber nicht möglich, da wir annehmen, dass Dichten immer positiv sind.
    Somit können wir diesen Unterfall ausschließen.
  \end{itemize}
  Die gegebene Aufgabe können wir nun also lösen,
  indem wir $V_{1}$ und $V_{2}$ wie in~\eqref{pharma1/misch:vol:V1V2} ausrechnen;
  zuerst $V_{2}$ und damit $V_{1}$.
  Wenn dann $V_{1}, V_{2} \geq 0$ gilt, so sind wir fertig
  (eine Probe kann zusätzlich über die Methode in \autoref{pharma1/misch:vol:umweg} gemacht werden).
  Wenn eine der beiden Größen negativ ist, so ist keine Problemlösung möglich.
\end{para}

\begin{para}[Spezialfall]
  \label{pharma1/misch:vol:ziel-spezial}
  Ein Spezialfall ist $\sig_1 = 1$ und $\sig_2 = 0$.
  In diesem Falle vereinfachen sich die Formeln zu:
  \begin{IEEEeqnarray*}{0l+l}
    V_1 = \sig V & V_2 = \frac{\rho(\sig) - \rho(\sig_1) \sig}{\rho(\sig_2)} V
  \end{IEEEeqnarray*}
\end{para}

\begin{example}
  Eine Lösung von $V = \SI{700}{\ml}$ mit $\sig = 45\%$ Volumenkonzentration Ethanol soll hergestellt werden.
  Es stehen Ethanol\-/Wasser\-/Lösungen mit ${\sig_{1} = 10\%}$ und ${\sig_{2} = 94\%}$
  Volumenkonzentration Ethanol zur Verfügung.
  Der Tabelle entnehmen wir:
  \begin{IEEEeqnarray*}{0l+l+l}
    \rho(\sig) = \SI{0.93954}{\g\per\ml} &
    \rho(\sig_{1}) = \SI{0.98471}{\g\per\ml} &
    \rho(\sig_{2}) = \SI{0.81518}{\g\per\ml}
  \end{IEEEeqnarray*}
  Nach \autoref{pharma1/misch:vol:ziel} errechnen wir die erforderlichen Volumina:
  \begin{IEEEeqnarray*}{0l+l}
    V_1 \approx \SI{428.2}{\ml} & V_2 \approx \SI{289.6}{\ml}
  \end{IEEEeqnarray*}
  Dabei haben wir zuerst $V_{2}$ berechnet und mithilfe dessen dann $V_{1}$.
  Wir machen die Probe; der Massenanteil Ethanol an der neuen Lösung ist $\tiw \approx 37.80\%$,
  was laut Tabelle genau einer Volumenkonzentration von $45\%$ entspricht.
\end{example}

\begin{example}
  Eine Lösung von $V = \SI{450}{\ml}$ mit $\sig = 43\%$ Volumenkonzentration Ethanol soll hergestellt werden.
  Es stehen Ethanol und Wasser zur Verfügung, also $\sig_{1} = 100\% = 1$ und $\sig_{2} = 0$.
  Der Tabelle entnehmen wir $\rho(\sig) = \SI{0.94306}{\g\per\ml}$
  und auch die Werte für $\rho(\sig_{1})$ (Dichte von Ethanol)
  und $\rho(\sig_{2})$ (Dichte von Wasser).
  Nach \autoref{pharma1/misch:vol:ziel-spezial} errechnen wir die erforderlichen Volumina:
  \begin{IEEEeqnarray*}{0c"s+c"s}
    V_1 \approx \SI{193.5}{\ml} & (Ethanol) & V_2 \approx \SI{272.1}{\ml} & (Wasser)
  \end{IEEEeqnarray*}
\end{example}

\section{Berücksichtigung mehrerer Stoffe}

\begin{para}[Formulierung des Problems]
  Es seien wieder $n$ Lösungen gegeben, und wir interessieren uns für $k$ verschiedene Stoffe.
  Der Massenanteil von Stoff $j \in \setn{k}$ an Lösung $i \in \setn{n}$ sei mit $w_{j,i}$ bezeichnet,
  mit $w_{j,i} \in \intcc{0}{1}$.
  Das Ziel sei, durch Mischung eine Lösung der Masse $d$ herzustellen,
  an der jeder Stoff $j \in \setn{k}$ den Massenanteil $w_j$ hat,
  für gewisse Zahlen $\eli{w}{k} \in \intcc{0}{1}$ und $d \in \RRpos$.
  Die Bedingungen für die zu wählenden Massen $\eli{x}{n}$ sind also:\footnote{%
    Wir verwenden hier die Buchstaben $\eli{x}{n}$ für die Massen,
    da diese Benennung der gesuchten Größen im Rahmen linearer Gleichungssystem üblicher ist als $\eli{m}{n}$.}
  \begin{IEEEeqnarray*}{0rCl}
    \forall j \in \setn{k} \holds \frac{\sum_{i=1}^n w_{j,i} x_i}{\sum_{i=1}^n x_i} & = & w_j \\*
    \sum_{i=1}^n x_i & = & d
  \end{IEEEeqnarray*}
  Das ist äquivalent zu:
  \begin{IEEEeqnarray}{0l}
    \label{pharma1/misch:lgs-roh}
    \begin{IEEEeqnarraybox}[][c]{rCl}
      \forall j \in \setn{k} \holds \sum_{i=1}^n (w_{j,i}-w_j) x_i & = & 0 \\
      \sum_{i=1}^n x_i & = & d
    \end{IEEEeqnarraybox}
  \end{IEEEeqnarray}
\end{para}

\begin{para}[Grundsätzliches zu linearen Gleichungssystemen]
  In Darstellung~\eqref{pharma1/misch:lgs-roh} erkennt man gut, dass es sich um ein lineares Gleichungssystem
  mit $k+1$ Gleichungen in $n$ Variablen handelt.
  Die Faktoren vor den Variablen heißen \term{Koeffizienten};
  hier sind die Koeffizienten in den ersten $k$ Gleichungen alle von der Form $w_{j,i}-w_j$
  für gewisse~$j$ und~$i$.
  Für ein solches System gibt es drei Möglichkeiten:
  es hat keine Problemlösung, es hat genau eine Problemlösung, oder es hat unendlich viele Problemlösungen
  (in diesem letzten Falle weist die Menge aller Problemlösungen eine gewisse Struktur auf).
  Es gibt Methoden, mit denen man bestimmen kann, welcher Fall vorliegt, sowie \ggf Problemlösungen finden kann.
  Diese Methoden sind in zahlreicher Standardsoftware implementiert,
  zum Beispiel in der freien Software GNU Octave~\cite{SoftwareOctave}.
  Wenn man von Hand rechnet, dann bietet sich das Gauß\-/Verfahren an.
  In jedem Falle ist es von Vorteil, wenn man das Gleichungssystem in Matrixschreibweise bringen~kann.
\end{para}

\begin{para}[Matrix]
  Eine \term{Matrix} ist ein rechteckiges Zahlenschema, bestehend aus Zeilen und Spalten, zum Beispiel:
  \begin{IEEEeqnarray*}{0l}
    A \df
    \begin{bmatrix}
      1 & 0 & 7 & 15 & 1 \\
      3 & 8 & 9 & 7 & 5 \\
      4 & 0 & 1 & 1 & 2
    \end{bmatrix}
  \end{IEEEeqnarray*}
  Mit dieser Definition ist $A$ eine Matrix mit $3$~Zeilen und $5$~Spalten;
  man sagt auch, $A$ ist eine $(3 \times 5)$-Matrix, oder man schreibt $A \in \RR^{3 \times 5}$.
  In diesem Beispiel ist sogar $A \in \parens{\NNzero}^{3 \times 5}$,
  denn als Einträge kommen nur Zahlen aus $\NNzero$ vor.
  Auf die Einträge in der Matrix bezieht man sich mit Indizes, wobei der Zeilenindex zuerst angegeben wird
  und man von oben zählt.
  Hier zum Beispiel $A_{1,3} = 7$ und ${A_{3,4} = 1}$ und ${A_{3,5} = 2}$.
  Das Komma zwischen den Indizes lässt man auch fort, wenn es keine Gefahr von Missverständnissen gibt.
  Spezialfälle sind Matrizen mit nur einer Zeile (auch \term{Zeilenvektor} genannt)
  oder nur einer Spalte (auch \term{Spaltenvektor} genannt).
\end{para}

\begin{para}[Lineares Gleichungssystem in Matrixform]
  Man kann jedes lineare Gleichungssystem durch eine Matrix und einen Spaltenvektor beschreiben:
  die \term{Koeffizientenmatrix} und die \term{rechte Seite}.
  Es korrespondieren die Zeilen der Koeffizientenmatrix zu den Gleichungen,
  man schreibt die Koeffizienten der \xten{i} Variablen in die \xte{i} Spalte.
  Für \eqref{pharma1/misch:lgs-roh} ist die Koeffizientenmatrix~$A$:
  \begin{IEEEeqnarray*}{0l}
    A =
    \begin{bmatrix}
      w_{1,1} - w_1 & w_{1,2} - w_1 & \hdots & w_{1,n} - w_1 \\
      w_{2,1} - w_2 & w_{2,2} - w_2 & \hdots & w_{2,n} - w_2 \\
      \vdots & \vdots & & \vdots \\
      w_{k,1} - w_k & w_{k,2} - w_k & \hdots & w_{k,n} - w_k \\
      1 & 1 & \hdots & 1
    \end{bmatrix}
    \in \RR^{(k+1) \times n}
  \end{IEEEeqnarray*}
  Mit anderen Worten: $A_{j,i} = w_{j,i} - w_j$ für alle $j \in \setn{k}$ und alle $i \in \setn{n}$;
  sowie ${A_{k+1,i} = 1}$ für alle $i \in \setn{n}$.
  Die \term{rechte Seite} $b$ ist ein Spaltenvektor mit so vielen Einträgen wie Gleichungen,
  in welchen in der richtigen Reihenfolge die Zahlen
  rechts von den Gleichheitszeichen des linearen Gleichungssystems stehen.
  Für~\eqref{pharma1/misch:lgs-roh} haben wir:
  \begin{IEEEeqnarray*}{0l}
    b =
    \begin{bmatrix}
      0 \\
      \vdots \\
      0 \\
      d
    \end{bmatrix}
    \in \RR^{(k+1)\times 1}
  \end{IEEEeqnarray*}
  Damit ist~\eqref{pharma1/misch:lgs-roh} genau das lineare Gleichungssystem \enquote{$A \cdot x = b$}.
  In der Form kann es in viele Standardsoftware eingegeben werden, also als Koeffizientenmatrix und rechte Seite.
\end{para}

\begin{para}[Produkt aus Matrix und Spaltenvektor]
  Die Schreibweise $A \cdot x$ ist nicht nur symbolisch zu verstehen,
  sondern wir können eine sinnvolle Multiplikation zwischen Matrizen definieren.
  Wir geben hier allgemein die Definition von $A \cdot x$ an
  für alle $A \in \RR^{p \times q}$ und alle $x \in \RR^{q \times 1}$,
  also das Produkt aus einer Matrix und einem Spaltenvektor.
  Das Ergebnis ist eine Matrix in $\RR^{p \times 1}$, also ein Spaltenvektor, den wir wie folgt berechnen:
  Wir drehen $x$ in Gedanken um $\SI{90}{\degree}$ gegen den Uhrzeigersinn
  und legen es über die erste Zeile von~$A$.
  Dann multiplizieren wir jeweils die übereinanderliegenden Zahlen miteinander
  und summieren alle diese Produkte auf.
  Das gibt den ersten Eintrag im Ergebnis.
  Dann fahren wir mit der zweiten Zeile fort und erhalten daraus den zweiten Eintrag im Ergebnis, usw.
  Im Hinblick auf lineare Gleichungssysteme spiegelt das genau die Idee wider,
  dass die Spalten der Koeffizientenmatrix mit den Variablen korrespondieren.
  Mit dieser Definition gilt für die Koeffizientenmatrix $A$
  und rechte Seite $b$ eines linearen Gleichungssystems:
  $x$ ist eine Problemlösung des Systems genau dann, wenn $A \cdot x = b$ gilt.
  Es folgt ein Zahlenbeispiel für Matrizenmultiplikation:
  \begin{IEEEeqnarray*}{0l}
    \begin{bmatrix}
      1 & 0 & 7 & 15 & 1 \\
      3 & 8 & 9 & 7 & 5 \\
      4 & 0 & 1 & 1 & 2
    \end{bmatrix}
    \cdot
    \colvec{8 \\ 3 \\ 2 \\ 11 \\ 4}
    =
    \begin{bmatrix}
      1 \cdot 8 + 0 \cdot 3 + 7 \cdot 2 + 15 \cdot 11 + 1 \cdot 4 \\
      3 \cdot 8 + 8 \cdot 3 + 9 \cdot 2 + 7 \cdot 11 + 5 \cdot 4 \\
      4 \cdot 8 + 0 \cdot 3 + 1 \cdot 2 + 1 \cdot 11 + 2 \cdot 4
    \end{bmatrix}
    =
    \begin{bmatrix}
      191 \\
      163 \\
      53
    \end{bmatrix}
  \end{IEEEeqnarray*}
\end{para}

\begin{para}[Methoden zum Lösen linearer Gleichungssysteme]
  Möchte man von Hand das Gauß\-/Verfahren anwenden, dann erweitert man die Koeffizientenmatrix
  um eine Spalte am rechten Ende, in der man genau die rechte Seite einträgt.
  Bei~\eqref{pharma1/misch:lgs-roh} sieht das so aus:
  \begin{IEEEeqnarray*}{0l}
    \begin{bmatrix}
      w_{1,1} - w_1 & w_{1,2} - w_1 & \hdots & w_{1,n} - w_1 & 0 \\
      w_{2,1} - w_2 & w_{2,2} - w_2 & \hdots & w_{2,n} - w_2 & 0 \\
      \vdots & \vdots & & \vdots & \vdots \\
      w_{k,1} - w_m & w_{k,2} - w_k & \hdots & w_{k,n} - w_k & 0\\
      1 & 1 & \hdots & 1 & d
    \end{bmatrix}
    \in \RR^{(k+1) \times (n+1)}
  \end{IEEEeqnarray*}
  Dann bringt man diese \term{erweiterte Koeffizientenmatrix} durch Zeilenumformungen auf \term{Zeilenstufenform}
  (engl. \enquote{row echelon form}).
  An dieser Form kann man dann Näheres über Lösungen oder Nicht\-/Existenz von Lösungen ablesen.
  Wir verzichten hier auf eine Vertiefung
  und verweisen lieber auf Anwendersysteme wie GNU Octave zum Lösen von linearen Gleichungssystemen.
  Wer sich für die Theorie linearer Gleichungssysteme interessiert siehe etwa~\cite{Jaenich08}.
\end{para}

\begin{example}
  Es stehen $n=3$ Lösungen zur Verfügung, und wir interessieren uns für $k=3$ darin
  in unterschiedlichen Massenanteilen befindliche Stoffe.
  In der folgenden Matrix tragen wir die bekannten Massenanteile zusammen,
  dabei stehen die Zeilen für Stoffe und die Spalten für Lösungen:
  \begin{IEEEeqnarray*}{0l}
    w =
    \begin{bmatrix}
      0.0 & 0.3 & 0.6 \\
      0.7 & 0.0 & 0.0 \\
      0.1 & 0.4 & 0.0
    \end{bmatrix}
  \end{IEEEeqnarray*}
  Wir haben also zum Beispiel $w_{1,2} = 0.3$,
  d.h. Stoff $1$ ist in Lösung $2$ mit einem Massenanteil von $30\%$ vertreten.
  Es sollen $\SI{200}{g}$ einer neuen Lösung aus diesen Lösungen gemischt werden,
  so dass jeder Stoff $j \in \setn{k}$ mit Massenanteil $w_j$ daran beteiligt ist,
  wobei $w_1 = 30\%$, $w_2 = 20\%$ und $w_3 = 20\%$.
  \par
  Für die Koeffizientenmatrix $A$ und rechte Seite $b$ des gesuchten linearen Gleichungssystems gilt:
  \begin{IEEEeqnarray*}{0l+l}
    A =
    \begin{bmatrix}
      -0.3 & \phantom{-}0.0 & \phantom{-}0.3 \\
      \phantom{-}0.5 & -0.2 & -0.2 \\
      -0.1 & \phantom{-}0.2 & -0.2 \\
      \phantom{-}1.0 & \phantom{-}1.0 & \phantom{-}1.0
    \end{bmatrix}
    & b =
    \begin{bmatrix}
      0 \\ 0 \\ 0 \\ 200
    \end{bmatrix}
  \end{IEEEeqnarray*}
  Wir geben diese Matrizen in GNU Octave ein:
  \lstinputlisting{snippet-misch-01.txt}
  Danach lassen wir Octave eine Problemlösung berechnen und speichern sie in der Variablen~\lstinline|x|:
  \lstinputlisting{snippet-misch-02.txt}
  Dies sind die Angaben in Gramm für die drei Lösungen,
  zum Beispiel von der ersten Lösung sollen wir $\SI{57.143}{\g}$ nehmen.
  Wir machen den Test in Octave:
  \lstinputlisting{snippet-misch-03.txt}
  Es kommt nicht genau die rechte Seite $b$ heraus, aber die ersten drei Einträge sind klein genug.
  So einen Test sollte man durchführen, denn Octave liefert bei \lstinline|A \ b| stets ein Ergebnis,
  auch wenn das lineare Gleichungssystem gar nicht lösbar ist;
  es wird dann ein \enquote{guter Versuch einer Problemlösung} ausgegeben,
  was aber nicht immer ein brauchbares Ergebnis ist.
  Eine andere Art zu testen, ob das lineare Gleichungssystem lösbar ist:
  Man berechnet \lstinline|rank(A)| und \lstinline|rank([A b])|.
  Lösbarkeit liegt genau dann vor, wenn die beiden Zahlen gleich sind.
  Diesen Test sollte man auch stets ausführen.
  Hier liefern beide Aufrufe den Wert~$3$.
  \par
  Wir testen außerdem, ob wir mit \lstinline|x| die richtigen Massenanteile haben.
  Das sollte der Fall sein, wenn wir die Koeffizientenmatrix richtig aufgestellt haben
  (und \lstinline|x| eine Problemlösung für das lineare Gleichungssystem ist).
  Wir geben die Matrix $w$ aus der Aufgabenstellung ein
  und multiplizieren sie mit der berechneten Lösung~\lstinline|x|:
  \lstinputlisting{snippet-misch-04.txt}
  Das sind die Massen (in Gramm) der drei Stoffe in der neuen Lösung.
  Um die Anteile zu erhalten, müssen wir noch durch $200$ teilen.
  Das gibt genau $30\%$, $20\%$ und $20\%$, wie gefordert.
\end{example}

\exercisesection{pharma1/misch}

\begin{exercise}
  Es sind die folgenden drei Lösungen mit den angegebenen Massenanteilen an Kochsalz gegeben:
  \begin{itemize}
  \item $\SI{102}{\g}$ mit $40\%$
  \item $\SI{250}{\g}$ mit $24\%$
  \item $\SI{93}{\g}$ mit $85\%$
  \end{itemize}
  Welchen Massenanteil Kochsalz hat die Lösung, die durch Zusammenführen der obigen drei entsteht?
\end{exercise}

\begin{exercise}
  Eine Lösung von $\SI{1}{\kg}$ mit $30\%$ Ethanol (Massenanteil)
  soll durch Zugabe geeigneter Lösungen auf $40\%$ Ethanol (Massenanteil) verstärkt werden.
  \begin{enumerate}
  \item Es steht eine Lösung mit $70\%$ Ethanol (Massenanteil) zur Verfügung.
    Wie viel davon müssen wir zugeben?
  \item\label{pharma1/misch:exc-1:2:b} Es stehen $\SI{450}{\g}$ einer Lösung mit $65\%$ Ethanol
    und $\SI{2}{\kg}$ einer Lösung mit $60\%$ Ethanol zur Verfügung (alles Massenanteile).
    Die zusätzliche Anforderung ist, dass zuerst die $65\%$ige Lösung aufgebraucht wird.
    Welche Möglichkeiten haben wir?
  \item Es stehen wieder $\SI{450}{\g}$ einer Lösung mit $65\%$ Ethanol
    und $\SI{2}{\kg}$ einer Lösung mit $60\%$ Ethanol zur Verfügung (alles Massenanteile).
    Die zusätzliche Anforderung ist nun, dass wir so viel $40\%$ige Lösung erzeugen wie möglich.
    Es steht keine weitere Flüssigkeit zur Verfügung, auch kein Wasser.
  \end{enumerate}
\end{exercise}

\begin{exercise}
  Eine Lösung von $V = \SI{750}{\ml}$ mit $\sig = 62\%$ Volumenkonzentration Ethanol soll hergestellt werden.
  Es stehen Ethanol\-/Wasser\-/Lösungen mit ${\sig_{1} = 20\%}$ und ${\sig_{2} = 73.8\%}$
  Volumenkonzentration Ethanol zur Verfügung.
  Welche Volumina davon müssen wir zusammenführen?
\end{exercise}

\solutionsection

\begin{solution}
  Der Massenanteil von Kochsalz an der neuen Lösung ist:
  \begin{IEEEeqnarray*}{0l}
    \frac{\SI{102}{\g} \cdot 0.4
      + \SI{250}{\g} \cdot 0.24
      + \SI{93}{\g} \cdot 0.85}%
    {\SI{102}{\g} + \SI{250}{\g} + \SI{93}{\g}}
    = \frac{\SI{179.85}{\g}}{\SI{445}{\g}}
    \approx 40.42\%
  \end{IEEEeqnarray*}
\end{solution}

\smallskip

\begin{solution}
  \begin{enumerate}
  \item Wir lösen die Formel $w = \frac{w_1 m_1 + w_2 m_2}{m_1 + m_2}$ nach $m_2$ auf
    und setzen dann die bekannten Werte für $w$, $w_1$, $w_2$ und $m_1$ ein:
    \begin{IEEEeqnarray*}{0l}
      w = \frac{w_1 m_1 + w_2 m_2}{m_1 + m_2}
      \iff w (m_1 + m_2) = w_1 m_1 + w_2 m_2 \\
      \eqin \iff (w-w_1) m_1 = (w_2-w) m_2
      \iff \frac{(w-w_1) m_1}{w_2-w} = m_2
    \end{IEEEeqnarray*}
    Dabei haben wir $w_{2} \neq w$ vorausgesetzt,
    was in der Aufgabe jedenfalls zutrifft.
    Einsetzen liefert:
    \begin{IEEEeqnarray*}{0l}
      m_2 = \frac{(w-w_1) m_1}{w_2-w} = \frac{(0.4-0.3) \cdot \SI{1}{\kg}}{0.7-0.4} = \frac{1}{3} \si{\kg}
    \end{IEEEeqnarray*}
  \item Wir nummerieren die Lösungen so: 1.~die zu verstärkende Lösung (zur Zeit mit $30\%)$,
    2.~die $65\%$ige Lösung (von der wir nur $\SI{450}{\g}$ haben und die wir zuerst aufbrauchen sollen),
    3.~die $60\%$ige Lösung (von der wir $\SI{2}{\kg}$ haben).
    Wir versuchen zuerst, nur mit der $65\%$igen Lösung auszukommen.
    Wenn das möglich ist, dann ist es optimal, da wir dann auf die dritte Lösung gar nicht zugreifen.
    Mit $m_3 = 0$ gilt:
    \begin{IEEEeqnarray*}{0c}
      m_2 = \frac{(w-w_1) m_1}{w_2-w}
      = \frac{(0.4-0.3) \cdot \SI{1}{\kg}}{0.65-0.4} = \SI{0.4}{\kg} = \SI{400}{\g}
    \end{IEEEeqnarray*}
    Da $\SI{450}{\g}$ zur Verfügung stehen, ist dies machbar.
  \item Wir versuchen zuerst, nur mit der $60\%$igen Lösung auszukommen.
    Wenn das möglich ist, dann ist es optimal,
    weil wir dann nur die weniger starke der beiden zur Verfügung stehenden Lösungen verwenden.
    Mit der Nummerierung wie im vorigen Punkt und $m_2=0$ haben wir:
    \begin{IEEEeqnarray*}{0c}
      m_3 = \frac{(w-w_1) m_1}{w_3-w} = \frac{(0.4-0.3) \cdot \SI{1}{\kg}}{0.6-0.4} = \frac{1}{2} \si{\kg}
    \end{IEEEeqnarray*}
    Da $\SI{2}{\kg}$ zur Verfügung stehen, ist dies machbar.
  \end{enumerate}
\end{solution}

\begin{solution}
  Der Tabelle entnehmen wir:
  \begin{IEEEeqnarray*}{0l+l+l}
    \rho(\sig) = \SI{0.90460}{\g\per\ml} &
    \rho(\sig_{1}) = \SI{0.97356}{\g\per\ml} &
    \rho(\sig_{2}) = \SI{0.87592}{\g\per\ml}
  \end{IEEEeqnarray*}
  Nach \autoref{pharma1/misch:vol:ziel} errechnen wir die erforderlichen Volumina:
  \begin{IEEEeqnarray*}{0l+l}
    V_1 \approx \SI{171.9}{\ml} & V_2 \approx \SI{583.5}{\ml}
  \end{IEEEeqnarray*}
  Wir machen die Probe; der Massenanteil Ethanol an der neuen Lösung ist $\tiw \approx 54.09\%$,
  was laut Tabelle genau einer Volumenkonzentration von $62\%$ entspricht.
\end{solution}
