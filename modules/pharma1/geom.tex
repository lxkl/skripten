\chapter{Wirkstoffkonzentration}
\label{pharma1/geom}

\section{Exponentieller Zerfall}

\begin{para}[Zerfall, Zerfallskonstante]
  Oft interessiert man sich in den Naturwissenschaften für Prozesse,
  bei denen eine bestimmte Messgröße mit der Zeit sinkt:
  Zum Beispiel die Anzahl der Atome einer radioaktiven Substanz,
  die mit der Zeit zerfallen,
  oder die Konzentration eines Wirkstoffes im Körper,
  der mit der Zeit vom Körper eliminiert wird.
  Oft hat man dabei einen \term{exponentiellen Zerfall},
  d.h. die Messgröße wird durch eine Funktion
  der folgenden Form beschrieben:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{f_{c,\lam}}{\RRnn}{\RRpos}{t \mapsto c e^{-\lam t}}
  \end{IEEEeqnarray*}
  Dabei sind $c,\lam > 0$ Parameter, die von der Anwendung abhängen,
  und $f_{c,\lam}(t)$ ist die Messgröße zum Zeitpunkt $t$.
  \par
  Es gilt $f_{c,\lam}(0) = c$, also ist der Parameter $c$ die Messgröße zum Zeitpunkt~$0$.
  Der Parameter $\lam$ heißt die \term{Zerfallskonstante}.
  Je größer $\lam$, umso schneller fällt die Messgröße.
  Rechnet man mit Einheiten, dann hat $\lam$ eine Einheit der Form $1/\mathrm{Zeit}$,
  zum Beispiel $\si{\per\hour}$, damit der Exponent im Ganzen einheitenfrei wird.
  Wir arbeiten zunächst ganz ohne Einheiten.
\end{para}

\begin{para}[Zeitspanne und übriger Anteil]
  Es sei $T > 0$. Dann gilt für alle $t \geq 0$:
  \begin{IEEEeqnarray*}{0l}
    \frac{f_{c,\lam}(t+T)}{f_{c,\lam}(t)}
    = \frac{c e^{-\lam (t+T)}}{c e^{-\lam t}}
    = e^{-\lam(t+T) + \lam t} = e^{-\lam T}
  \end{IEEEeqnarray*}
  Anders geschrieben also $f_{c,\lam}(t+T) = e^{-\lam T} f_{c,\lam}(t)$.
  Egal also von welchem Zeitpunkt aus man um die Zeit $T$ in die Zukunft schaut,
  man wird zu dem zukünftigen Zeitpunkt stets denselben Anteil
  vom gegenwärtigen Wert der Messgröße übrig haben, nämlich den Anteil $e^{-\lam T} \in \intoo{0}{1}$.
  Können wir $\lam$ erschließen, wenn wir nur diesen Anteil kennen, sagen wir $q \in \intoo{0}{1}$,
  und die zugehörige Zeitspanne $T$?
  Ja, denn es gilt:
  \begin{IEEEeqnarray*}{0l}
    e^{-\lam T} = q \iff -\lam T = \ln(q) \iff \lam = - \frac{\ln(q)}{T}
  \end{IEEEeqnarray*}
  Somit kennen wir nun eine zweite Möglichkeit, einen exponentiellen Zerfall zu beschreiben:
  Anstelle der Zerfallskonstanten geben wir $T$ und $q$ an und sagen dazu,
  dass nach der Zeit $T$ stets der Anteil $q$ übrig ist.
  Wenn $\lam$ die zu $T$ und $q$ passende Zerfallskonstante ist,
  also wenn $\lam = - \frac{\ln(q)}{T}$ gilt, dann haben wir für alle $t \geq 0$:
  \begin{IEEEeqnarray}{0l}
    \label{pharma1/geom:qtT}
    f_{c,\lam}(t) = c e^{-\lam t} = c e^{\frac{\ln(q)}{T} t}
    = c \p{e^{\ln(q)}}^{\frac{t}{T}} = c q^{\frac{t}{T}}
  \end{IEEEeqnarray}
\end{para}

\begin{para}[Halbwertzeit]
  Der Spezialfall $q = \frac{1}{2}$ ist besonders wichtig.
  Wir nennen das zu $q = \frac{1}{2}$ gehörige $T$ die \term{Halbwertzeit} des Prozesses.
  Ist $\halflife$ die Halbwertzeit, so berechnen wir daraus die Zerfallskonstante nach obiger Formel:
  \begin{IEEEeqnarray*}{0l}
    \lam = - \frac{\ln(\frac{1}{2})}{\halflife}
    = - \frac{\ln(1) - \ln(2)}{\halflife}
    = \frac{\ln(2)}{\halflife}
  \end{IEEEeqnarray*}
  Am besten merkt man sich, dass das Produkt aus Zerfallskonstante und Halbwertzeit $\ln(2)$ ist.
  Wenn $\lam$ die zur Halbwertzeit $\halflife$ passende Zerfallskonstante ist,
  also wenn $\lam = \frac{\ln(2)}{\halflife}$ gilt,
  dann haben wir nach \eqref{pharma1/geom:qtT} für alle $t \geq 0$:
  \begin{IEEEeqnarray*}{0l}
    f_{c,\lam}(t) = c \p{\frac{1}{2}}^{\frac{t}{\halflife}} = c 2^{-\frac{t}{\halflife}}
  \end{IEEEeqnarray*}
\end{para}

\section{Modell mit regelmäßiger Zufuhr}

\begin{para}[Annahmen für das Modell]
  Wir betrachten nun ein einfaches pharmazeutisches Modell für die Wirkstoffkonzentration im Körper.
  Dieses beruht auf den folgenden (unter Umständen stark) vereinfachenden Annahmen:
  \begin{itemize}
  \item Die Elimination des Wirkstoffes im Körper unterliegt einem exponentiellen Zerfallsgesetz,
    d.h. solange keine Zufuhr stattfindet,
    fällt die Konzentration des Wirkstoffes gemäß eines exponentiellen Zerfalls.
    Man nennt dies eine \term{Eliminationskinetik 1.~Ordnung}.
  \item Durch die Gabe eines Medikamentes können wir augenblicklich einen Anstieg
    der Konzentration dieses Wirkstoffes im Körper um einen bekannten Wert $\Del > 0$ bewirken.
  \end{itemize}
  Für Erklärungen zur Modellierung und für komplexere Modelle
  verweisen wir auf die Fachliteratur zur \term{Biopharmazie} und insbesondere zur \term{Pharmakokinetik}.
\end{para}

\begin{para}[Mathematisches Modell]
  Es sei $T > 0$ und $q \in \intoo{0}{1}$.
  Die Elimination des Wirkstoffes sei durch den übrigbleibenden Anteil $q$ nach der Zeit $T$ beschrieben,
  also hätten wir --~solange nur der Zerfall berücksichtigt wird!~--
  die Konzentration zum Zeitpunkt $t \geq 0$ als $c q^{\frac{t}{T}}$ für geeignetes $c > 0$.
  Allerdings verabreichen wir zu den Zeitpunkten in der Menge $\set{nT \suchthat n \in \NN}$,
  d.h. zum Zeitpunkt $T, 2T, 3T$, \usw, das Medikament,
  so dass ein sofortiger Anstieg der Konzentration um $\Del > 0$ stattfindet.
  Die eben angegebene Formel für die Wirkstoffkonzentration
  ist offenbar angesichts dieser regelmäßigen Erhöhungen nicht mehr zutreffend.
  Wir entwickeln im Folgenden unter der neuen Situation eine Formel für die Wirkstoffkonzentration
  zu den Zeitpunkten $nT$ mit $n \in \NN$.
  Wir einigen uns darauf, dass wir den Anstieg um $\Del$,
  der zum Zeitpunkt $nT$ erfolgt, bei der Wirkstoffkonzentration zum Zeitpunkt $nT$ schon mitrechnen.
  Die anfängliche Konzentration ist $c > 0$;
  ein interessanter Spezialfall ist $c = \Del$, d.h. wir geben auch zum Zeitpunkt~$0$ einmal das Medikament,
  und davor lag dieser Wirkstoff nicht im Körper vor.
\end{para}

\begin{para}[Fortsetzung Mathematisches Modell]
  Für die Beschreibung eines exponentiellen Zerfalls
  durch Zeit $T$ und Anteil $q$ ist der Wert von $T$ egal, solange $q$ dazu passt,
  nämlich $q$ muss der Anteil sein,
  auf den die Messgröße innerhalb der Zeit $T$ sinkt.
  In diesem Abschnitt betrachten wir aber nicht nur einen Zerfall,
  sondern einen Zerfall kombiniert mit einer regelmäßigen Zufuhr.
  Um diesen Prozess zu analysieren, ist es hilfreich,
  $T$ so zu wählen, dass es genau dem Zeitabstand zwischen zwei Zufuhren entspricht.
  Das $q$ muss dann zu diesem $T$ passend den Zerfall beschreiben.
  Im Folgenden bezeichne $f$ eine Funktion von $\RRnn$ nach $\RRpos$ so,
  dass $f(t)$ die Wirkstoffkonzentration zum Zeitpunkt $t \geq 0$
  im oben beschriebenen Modell ist mit den jeweils betrachteten Parametern $c,T,q,\Del$,
  wobei $c,T,\Del > 0$ gilt und $q \in \intoo{0}{1}$.
  Genauer könnte man $f_{c,T,q,\Del}$ statt $f$ schreiben,
  das würde aber hier keinen Mehrwert bringen.
\end{para}

\begin{figure}[t]
  \centering%
  \input{plot-geom}%
  \caption{Wirkstoffkonzentration für $c = 0.5$, $T=2$, $q=0.4$, und $\Del=1$.
    Auf der x\=/Achse ist die Zeit aufgetragen und auf der y\=/Achse die Konzentration.
    Die hervorgehobenen Punkte geben an, dass die Funktion an der jeweiligen Stelle den Wert annimmt,
    der durch die y\=/Koordinate des Punktes gegeben ist.
    Sonst würde aus dem Plot nur schwer erkennbar hervorgehen,
    dass etwa $f(2) = 1.2$ gilt und nicht $f(2) = 0.2$.\label{pharma1/geom:plot}}%
\end{figure}

\begin{para}[Diskussion eines Plots]
  Ein beispielhafter Plot ist in \autoref{pharma1/geom:plot} gegeben.
  Es scheint, dass sich die Werte zu Zeitpunkten der Form $nT$, $n \in \NN$, einem Wert immer weiter annähern;
  diesen vermuten wir knapp über $1.6$.
  Mit solchen Vermutungen nur aufgrund eines Plots muss man vorsichtig sein.
  Zum Beispiel könnte man durch Betrachten der \autoref{grund/rlfun:plot-pot} auf die Idee kommen,
  die Wurzelfunktion $\fnx{\RRnn}{\RRnn}{x \mapsto \sqrt[3]{x}}$ würde sich einem Wert annähern.
  Das ist aber nicht der Fall; die Kurve der Wurzelfunktion wird zwar flacher, wächst aber unbeschränkt.
\end{para}

\section{Formel für $f(nT)$}

\begin{para}[Einfach mal ausrechnen]
  \label{pharma1/geom:system}
  Wir schauen uns einige Werte an:
  \begin{IEEEeqnarray*}{0rCl}
    f(T) & = & c q + \Del \\
    f(2T) & = & f(T) q + \Del = (c q + \Del) q + \Del
    = c q^2 + \Del (1 + q) \\
    f(3T) & = & f(2T) q + \Del = (c q^2 + \Del (1 + q)) q + \Del \\
    & = & c q^3 + \Del (q + q^2) + \Del = c q^3 + \Del (1 + q + q^2)
  \end{IEEEeqnarray*}
  Man vermutet hier ein System, und man kann tatsächlich den folgenden Satz zeigen.
\end{para}

\begin{satz}[Formel für $f(nT)$]
  \label{pharma1/geom:fnT}
  Es gilt für alle $n \in \NN$:
  \begin{IEEEeqnarray*}{0l}
    f(nT)
    = c q^n + \Del \sum_{k=0}^{n-1} q^k
    = c q^n + \Del \frac{1-q^{n}}{1-q}
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}
  Die erste Gleichung gibt die allgemeine Fassung des Systems,
  das sich in \autoref{pharma1/geom:system} angedeutet hatte.
  Man kann dies leicht mit Induktion beweisen,
  worauf wir hier aber nicht weiter eingehen.
  \par
  Wir wenden uns der zweiten Gleichung zu.
  Für alle $n \in \NN$ gilt:
  \begin{IEEEeqnarray*}{0rCl"s}
    && (1-q) \sum_{k=0}^{n-1} q^k \\
    & = & 1 \ccdot \sum_{k=0}^{n-1} q^k - q \sum_{k=0}^{n-1} q^k & Ausmultiplizieren \\
    & = & \sum_{k=0}^{n-1} q^k - \sum_{k=0}^{n-1} q^{k+1} & Ausmultiplizieren \\
    & = & \sum_{k=0}^{n-1} q^k - \sum_{k=1}^{n} q^k & Indexverschiebung \\
    & = & q^0 + \sum_{k=1}^{n-1} q^k - q^{n} - \sum_{k=1}^{n-1} q^k & \enquote{Auskoppeln} von $q^0$ und $q^n$ \\
    & = & q^0 - q^{n} = 1 - q^{n}
  \end{IEEEeqnarray*}
  Division durch $1-q$ liefert die Aussage
  (beachte $1-q \neq 0$, da $q \neq 1$, weswegen wir so dividieren dürfen).
\end{proof}

\begin{para}[Hintergrund: Geometrische Reihe]
  Die Zahlenfolge
  \begin{IEEEeqnarray*}{0l}
    \sum_{k=0}^{0} q^k, \: \sum_{k=0}^{1} q^k, \: \sum_{k=0}^{2} q^k, \:
    \sum_{k=0}^{3} q^k, \: \sum_{k=0}^{4} q^k, \: \hdots
  \end{IEEEeqnarray*}
  bezeichnet man als \term{geometrische Reihe}.
  Anders geschrieben:
  \begin{IEEEeqnarray*}{0l}
    1, \: 1 + q, \: 1 + q + q^2, \: 1 + q + q^2 + q^3, \: 1 + q + q^2 + q^3 + q^4 , \: \hdots
  \end{IEEEeqnarray*}
  Sie spielt in vielen Bereichen der Mathematik eine fundamentale Rolle.
\end{para}

\section{Langzeitverhalten}

\begin{para}[Motivation]
  Wir haben mit \autoref{pharma1/geom:fnT} eine Möglichkeit,
  die Konzentration zu Zeitpunkten der Form $nT$ mit $n \in \NN$ einfach zu berechnen.
  Was passiert, wenn wir $n$ immer größer machen?
  Das war ja unsere ursprüngliche Frage: Wir hatten vermutet,
  dass sich diese Werte mit steigendem $n$ immer näher an einen bestimmten Wert bewegen.
  Um das besser zu verstehen, präzisieren wir zunächst das
  \enquote{immer näher an einen bestimmten Wert bewegen}.
\end{para}

\begin{para}[Kurze Vorstellung: Folgen und Limes]
  Wir betrachten zunächst allgemein eine Zahlenfolge $x_{1}, x_{2}, x_{3}, \hdots$,
  die man kurz auch $\seq{x_{n}}{n \geq 1}$ schreibt.
  Zu jedem $n \in \NN$ gibt es also eine bestimmte Zahl~$x_{n}$,
  genannte die \xte{n} Folgenkomponente.
  Wir nennen eine Zahl $a \in \RR$ einen \term{Limes} oder einen \term{Grenzwert} dieser Zahlenfolge,
  wenn gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall \veps > 0 \innerholds
    \exists n_{0} \in \NN \innerholds
    \forall n \in \NN \with n \geq n_{0} \holds \abs{x_{n} - a} < \veps
  \end{IEEEeqnarray*}
  Man sieht mehr, wenn man das kürzer schreibt, was ohne Gefahr von Missverständnissen möglich ist:
  \begin{IEEEeqnarray}{0l}
    \label{pharma1/geom:lim}
    \forall \veps > 0 \innerholds
    \exists n_{0} \innerholds
    \forall n \geq n_{0} \holds \abs{x_{n} - a} < \veps
  \end{IEEEeqnarray}
  Mit anderen Worten:
  Gegeben eine -- noch so kleine -- Zahl $\veps > 0$,
  so können wir uns sicher sein,
  dass $x_{n}$ weniger als $\veps$ von $a$ entfernt ist,
  wenn nur $n$ groß genug ist.
  Nicht jede Zahlenfolge hat einen Limes.
  Man kann aber zeigen, dass wenn eine Zahlenfolge einen Limes hat,
  dieser eindeutig bestimmt ist, man also von \emphasis{dem} Limes der Zahlenfolge sprechen kann.
\end{para}

\begin{para}[Geometrische Folge]
  Wie ist das mit der Zahlenfolge $q,q^{2},q^{3},\hdots$,
  die man kurz auch $\seq{q^{n}}{n \geq 1}$ schreibt?
  Wir wissen, dass Multiplikation einer positiven Zahl mit einer Zahl
  aus dem Intervall $\intoo{0}{1}$ zu einer Reduktion führt;
  das ist \autoref{grund/rl:ofield-rules}~\ref{grund/rl:ofield-rules:19}.
  Das legt die Vermutung nahe, dass $\seq{q^{n}}{n \geq 1}$ Limes $0$ hat.
  Das ist tatsächlich so, denn für alle $\veps > 0$ und alle $n \in \NN$ gilt:
  \begin{IEEEeqnarray*}{0l}
    \abs{q^{n} - 0} < \veps \iff
    q^{n} < \veps \iff n \, \ln(q) < \ln(\veps) \iff n > \frac{\ln(\veps)}{\ln(q)}
  \end{IEEEeqnarray*}
  Somit gilt:
  Für alle $\veps > 0$, existiert $n_{0}$ so,
  dass $\abs{q^{n} - 0} < \veps$ für alle $n \geq n_{0}$ gilt;
  nämlich wir können zum Beispiel $n_{0} \df \ceil{\frac{\ln(\veps)}{\ln(q)}} + 1$ wählen.
\end{para}

\begin{para}[Anwendung auf unsere Folge]
  Unser Interesse gilt nach wie vor der Zahlenfolge $\seq{f(nT)}{n \geq 1}$,
  die ja $\seq{c q^n + \Del \frac{1-q^{n}}{1-q}}{n \geq 1}$ ist.
  Nun kommt darin $q^{n}$ nicht alleine vor,
  sondern es kommt zweimal vor und es wird mit anderen Zahlen kombiniert.
  Man kann zeigen, dass es in so einem Falle wie hier zulässig ist,
  $q^{n}$ durch den Limes der Zahlenfolge $\seq{q^{n}}{n \geq 1}$, also durch $0$, zu ersetzen,
  um den Limes von $\seq{f(nT)}{n \geq 1}$ zu erhalten.
  Ein Beweis dafür wäre zu aufwändig für dieses Kapitel, daher belassen wir es bei diesem Zitat.
  Wir erhalten damit den folgenden Satz.
  Es ist dabei bemerkenswert, dass für den Limes die Anfangskonzentration $c$ keine Rolle spielt.
\end{para}

\begin{satz}
  Die Zahlenfolge $\seq{f(nT)}{n \geq 1}$ hat den Limes~$\frac{\Del}{1-q}$.
\end{satz}

\begin{example}
  \label{pharma1/geom:ex-1}
  Im Laufe eines Tages (24~Stunden) nimmt die Konzentration eines bestimmten Wirkstoffes im Körper
  um den Anteil $0.8$ ab.
  Durch Gabe eines Medikamentes können wir die Konzentration augenblicklich um $\SI{5}{\mg\per\litre}$ erhöhen.
  Am Anfang des ersten Tages wird das Medikament einmal gegeben,
  also ist die Anfangskonzentration $\SI{5}{\mg\per\litre}$.
  Danach wird das Medikament alle 24 Stunden gegeben.
  Wie hoch ist die Konzentration nach $2$ \bzw $5$ Tagen,
  wobei wir die Gabe des Medikamentes am Ende des zweiten \bzw fünften Tages
  (d.h. am Anfang des dritten \bzw sechsten Tages) noch mit einrechnen?
  Wie hoch ist die Konzentration direkt nach der Gabe im Limes?
  \par
  \textit{Antwort.}
  Wir haben $q = 1 - 0.8 = 0.2$ (denn $0.8$ ist der Anteil \emphasis{um} den es abnimmt)
  und $T = \SI{24}{\hour}$ und $c = \Del = \SI{5}{\mg\per\litre}$.
  Gesucht sind zunächst $f(2T)$ und $f(5 T)$.
  Es gilt:
  \begin{IEEEeqnarray*}{0l}
    f(2 T) = c q^2 + \Del \frac{1-q^{2}}{1-q}
    \approx \SI{6.200}{\mg\per\litre} \\
    f(5 T) = c q^5 + \Del \frac{1-q^{5}}{1-q}
    \approx \SI{6.250}{\mg\per\litre}
  \end{IEEEeqnarray*}
  Der Limes ist $\frac{\Del}{1-q} \approx \SI{6.250}{\mg\per\litre}$,
  also bei unserer Genauigkeit nicht von dem Wert nach dem fünften Tag zu unterscheiden.
\end{example}

\exercisesection{pharma1/geom}

\begin{exercise}
  \label{pharma1/geom:exc-vereinfacht}
  Zeigen Sie, dass sich \autoref{pharma1/geom:fnT} im Spezialfall $c = \Del$ vereinfacht zu:
  \begin{IEEEeqnarray*}{0l}
    f(nT) = \Del \frac{1-q^{n+1}}{1-q}
  \end{IEEEeqnarray*}
  Verifizieren Sie mit dieser Formel die Ergebnisse aus \autoref{pharma1/geom:ex-1}.
\end{exercise}

\begin{exercise}
  Die Gabe eines bestimmten Medikamentes bewirkt augenblicklich eine Steigerung
  der Konzentration des Wirkstoffes um $\Del = \SI{3.5}{\mg\per\litre}$.
  Für die Elimination des Wirkstoffes wird ein exponentielles Zerfallsgesetz derart angenommen,
  dass die Konzentration jede Stunde um den Anteil $0.05$ abnimmt.
  \par
  Anfänglich liegt kein Wirkstoff im Körper vor.
  Dann beginnen wir damit, alle $\SI{12}{\hour}$ das Medikament zu geben;
  die erste Gabe erfolgt zum Zeitpunkt~$0$, d.h. $c = \Del$.
  \begin{enumerate}
  \item Welche Konzentration liegt direkt nach der \xten{5} Gabe vor?
  \item Gewünscht sei im Limes direkt nach der Gabe des Medikamentes
    eine Konzentration von $\SI{12}{\mg\per\litre}$.
    Welches $\Del$ leistet dies,
    wenn wir alle anderen Parameter (Zerfallsgeschwindigkeit, Zeitintervall) beibehalten?
  \end{enumerate}
\end{exercise}

\begin{exercise}
  Wir befinden uns im Fall ${c = \Del}$.
  Es sei $\gam \in \intoo{0}{1}$.
  \begin{enumerate}
  \item Geben Sie eine Formel für $n$ so an, dass nach $n$ Zeitintervallen der Länge $T$
    zum ersten Mal direkt nach der Gabe des Medikamentes eine Konzentration
    vom Anteil $\gam$ des Limes' vorliegt,
    d.h. wo zum ersten Mal $f(nT) \geq \gam \frac{\Del}{1-q}$ gilt.
  \item Angenommen, es gilt $T = \halflife$.
    Berechnen Sie für diesen Fall die Werte für $n$ für $\gam = 0.90$ und $\gam = 0.95$.
  \end{enumerate}
\end{exercise}

\begin{exercise}
  Ein bestimmter Wirkstoff wird gemäß eines exponentiellen Zerfallsgesetzes
  mit der Zerfallskonstanten ${\lam = \SI{0.12}{\per\hour}}$ vom Körper eliminiert.
  Angenommen, durch die Gabe eines Medikamentes können wir die Konzentration augenblicklich
  um $\Del = \SI{5}{\mg\per\litre}$ steigern.
  Es wird alle ${T = \SI{24}{\hour}}$ das Medikament gegeben;
  die erste Gabe findet zum Zeitpunkt $0$ statt,
  und vor der ersten Gabe liegt kein Wirkstoff im Körper vor.
  \begin{enumerate}
  \item Welche Konzentration direkt nach der Gabe wird sich im Limes einstellen?
  \item Nach der wievielten Gabe wird das erste Mal
    ein Anteil von $0.999$ oder mehr dieses Limes' erreicht?
    (Achtung: Am Ende des \xten{n} Zeitintervalls der Länge~$T$ erfolgt die \xte{(n+1)} Gabe.)
  \end{enumerate}
\end{exercise}

\solutionsection

\begin{solution}
  Es sei $c = \Del$.
  Dann gilt für alle $n \in \NN$:
  \begin{IEEEeqnarray*}{0rCl"s}
    f(nT) &=& q^n c + \Del \frac{1-q^{n}}{1-q} & nach \autoref{pharma1/geom:fnT} \\
    &=& q^n \Del + \Del \frac{1-q^{n}}{1-q} & nach Voraussetzung \\
    &=& \Del \frac{(1-q) q^n + 1-q^{n}}{1-q} & Ausklammern, Bruchrechnen \\
    &=& \Del \frac{q^n - q^{n+1} + 1-q^{n}}{1-q} & Ausmultiplizieren \\
    &=& \Del \frac{1 - q^{n+1}}{1-q}
  \end{IEEEeqnarray*}
  Mit $q = 0.2$ und $\Del = \SI{5}{\mg\per\litre}$ wie in \autoref{pharma1/geom:ex-1} berechnen wir:
  \begin{IEEEeqnarray*}{0l}
    f(2 T) = \Del \frac{1 - q^{3}}{1-q} \approx \SI{6.200}{\mg\per\litre} \\
    f(5 T) = \Del \frac{1 - q^{6}}{1-q} \approx \SI{6.250}{\mg\per\litre}
  \end{IEEEeqnarray*}
\end{solution}

\begin{solution}
  \begin{enumerate}
  \item Das Zeitintervall ist $T = \SI{12}{\hour}$.
    Jede Stunde haben wir einen Abfall um den Anteil $0.05$, also ist
    \begin{IEEEeqnarray*}{0l}
      q \df \parens{1-0.05}^{12} \approx \np{0.5404}
    \end{IEEEeqnarray*}
    der zu unserem $T$ gehörige Anteil.
    Die Konzentration nach dem \xten{n} Zeitintervall
    (also nach der \xten{(n+1)} Gabe) ist nach \autoref{pharma1/geom:exc-vereinfacht} gegeben.
    Der gefragte Wert ist $f(4T)$, den wir mit den gegebenen Werten für $\Del$ und $q$ berechnen:
    \begin{IEEEeqnarray*}{0l}
      f(4T) = \Del \frac{1-q^{5}}{1-q} \approx \SI{7.264}{\mg\per\litre}
    \end{IEEEeqnarray*}
  \item Der Limes ist $\frac{\Del}{1-q}$. Es gilt:
    \begin{IEEEeqnarray*}{0l}
      \SI{12}{\mg\per\litre} = \frac{\Del}{1-q}
      \iff \SI{12}{\mg\per\litre} (1-q) = \Del
    \end{IEEEeqnarray*}
    Mit dem bekannten Wert für $q$ errechnet sich
    das gewünschte $\Del$ zu ${\Del \approx \SI{5.516}{\mg\per\litre}}$.
  \end{enumerate}
\end{solution}

\begin{solution}
  \begin{enumerate}
  \item Für alle $n \in \NN$ gilt:
    \begin{IEEEeqnarray*}{0rCl"s}
      && f(nT) \geq \gam \frac{\Del}{1-q} \\
      &\iff& \Del \frac{1-q^{n+1}}{1-q} \geq \gam \frac{\Del}{1-q} \\
      &\iff& 1-q^{n+1} \geq \gam & da $\Del, 1-q > 0$ \\
      &\iff& 1 - \gam \geq q^{n+1} \\
      &\iff& \ln(1 - \gam) \geq \ln\parens{q^{n+1}}
      & $\ln$ streng monoton steigend, \autoref{grund/rlfun:equiv-mono} \\
      &\iff& \ln(1 - \gam) \geq (n+1) \ln\parens{q}
      & Logarithmusregel \\
      &\iff& \frac{\ln(1 - \gam)}{\ln\parens{q}} \leq n+1 & da $\ln(q) < 0$ wegen $q < 1$ \\
      &\iff& \frac{\ln(1 - \gam)}{\ln\parens{q}} - 1 \leq n
    \end{IEEEeqnarray*}
    Die kleinste natürliche Zahl $n$, die obige Bedingung erfüllt, löst die Aufgabe.
  \item Wenn $T = \halflife$, dann ist $q = 0.5$.
    Wir erhalten für die beiden Werte von $\gam$ gerundet die Bedingungen:
    \begin{IEEEeqnarray*}{0l+l}
      2.322 \leq n & 3.322 \leq n
    \end{IEEEeqnarray*}
    Die gesuchten Werte sind also $n = 3$ und $n=4$.
  \end{enumerate}
\end{solution}

\begin{solution}
  \begin{enumerate}
  \item Wir bemerken zunächst, dass gilt $c = \Del$.
    Um unsere Formeln anwenden zu können, müssen wir von der Zerfallskonstanten $\lam$
    umrechnen auf den zum gegebenen $T$ passenden Anteil $q$.
    Wir wissen schon, wie das geht, nämlich $q = e^{-\lam T}$.
    Daraus errechnen wir hier $q \approx \np{5.613e-2}$
    und für den Limes $\frac{\Del}{1-q} \approx \SI{5.297}{\mg\per\litre}$.
  \item Wir verwenden die Formel aus der vorigen Aufgabe mit $\gam = 0.999$,
    was die Bedingung $n \geq 1.399$ für die Anzahl $n$ der Zeitintervalle ergibt,
    also tritt die gewünschte Situation nach $2$ Zeitintervallen ein,
    d.h. nach der \xten{3} Gabe.
  \end{enumerate}
\end{solution}
