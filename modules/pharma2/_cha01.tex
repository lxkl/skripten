%%% Copyright 2024 Lasse Kliemann <l.kliemann@math.uni-kiel.de>
%%% CC BY-SA 4.0

\chapter{Mathematische Modellierung des Zufalls}

Der Wetterbericht sagt:
\enquote{Die Regenwahrscheinlichkeit am Samstag ist $50\%$ und am Sonntag auch $50\%$.}
Bedeutet das, dass die Wahrscheinlichkeit für Regen am Wochenende $100\%$ ist?
Denn immerhin ist $50\%+50\% = 100\%$.
Hier stimmt etwas nicht, denn bei drei Tagen wären wir schon bei $150\%$.

Was ist die Wahrscheinlichkeit, dass es im Sternsystem Zeta Reticuli Leben gibt?
Da wir keine Ahnung haben, sagen wir $50\%$.
Was ist die Wahrscheinlichkeit, dass es im Sternsystem Zeta Reticuli \term{intelligentes} Leben gibt?
Da wir auch hier keine Ahnung haben, sagen wir wieder $50\%$.
Hier stimmt etwas nicht, denn man sollte erwarten,
dass die Wahrscheinlichkeit für intelligentes Leben geringer ist als für Leben allgemein.

Diese Beispiele geben einen ersten, kurzen Eindruck, was passieren kann,
wenn man mit Wahrscheinlichkeiten zu naiv umgeht.
Eine mathematische Herangehensweise kann helfen, solche Fehler zu vermeiden.
In diesem ersten Kapitel lernen wir die einfachsten wahrscheinlichkeitstheoretischen Strukuren kennen,
nämlich die endlichen Wahrschscheinlichkeitsräume.

\section{Endliche Wahrschscheinlichkeitsräume}

\begin{para}[W'maß und endlicher W'raum]
  Es sei $\Om$ eine nichtleere, endliche Menge.
  Wir nennen eine Funktion $\ffn{\prob{}}{\cP(\Om)}{\intcc{0}{1}}$ ein \term{Wahrscheinlichkeitsmaß (W'maß)}
  auf $\Om$, wenn gilt:
  \begin{description}[font=\it]
  \item[Normierung:] $\prob{\Om} = 1$
  \item[Additivität:]
    $\forall A,B \subseteq \Om \with A \cap B = \emptyset
    \holds \prob{A \dotcup B} = \prob{A} + \prob{B}$\par
    (Wir schreiben $A \dotcup B$ anstelle von $A \cup B$,
    um die Disjunktheit von $A$ und $B$ hervorzuheben.)
  \end{description}
  Das Paar $\ezwraum$ nennen wir einen \term{endlichen Wahrscheinlichkeitsraum (endlichen W'raum)}.
  In diesem Kontext nennen wir \dots
  \begin{itemize}
  \item $\Om$ den \term{Ergebnisraum} und jedes Element von $\Om$ ein \term{Ergebnis};
  \item jede Teilmenge von $\Om$, d.h. jedes Element von $\cP(\Om)$,
    ein \term{Ereignis} oder \term{Event}.
  \end{itemize}
  Für alle $A \subseteq \Om$, d.h. für alle Events $A$, nennen wir \dots
  \begin{itemize}
  \item $\prob{A}$ die \term{Wahrscheinlichkeit (W'keit)} von $A$;
  \item $A^{c} \df \Om \setminus A$
    das \term{Gegenereignis} oder \term{Gegenevent} zu~$A$;
  \item und $\prob{A^{c}}$ die \term{Gegegenw'keit} zu $A$.
  \end{itemize}
  Wir verzichten in diesem und den nächsten beiden Abschnitten bewusst
  auf eine Diskussion von Anwendungen oder Analogien;
  sondern wir nehmen die Bezeichnungen als solche hin.
\end{para}

\begin{para}[Familien]
  Wir kennen schon lange Mengen und Tupel, um mehrere Objekte in einem Objekt zusammenzufassen.
  Eine weitere Möglichkeit sind die \sog Familien,
  die in der W'keitstheorie hilfreich sind und die wir hier kurz vorstellen.
  Es sei $M$ eine Menge.
  Eine \term{Familie} in $M$ oder eine \term{Familie von Elementen} von $M$ ist eine Funktion $\ffn{f}{I}{M}$,
  mit einer nichtleeren Menge $I$.
  In diesem Kontext nennt man die Domain $I$ auch die \term{Indexmenge}.
  Als Notation wird häufig $\seq{f_{i}}{i \in I}$ verwendet, oft mit einem Zusatz,
  aus dem die Menge $M$ hervorgeht.
  Möchten wir uns später mit einem einzigen Symbol auf die Familie beziehen,
  so führen wir diese in der Form $f = \seq{f_{i}}{i \in I}$ ein;
  auf die Familie können wir uns dann schlicht mit $f$ beziehen.
  Die Menge aller Familien in $M$ mit Indexmenge $I$ notieren wir mit $M^{I}$.
  Die Notation und Redeweisen sind also ähnlich wie bei Tupeln.
  Eine Familie nennen wir \term{endlich} wenn sie endliche Indexmenge hat.
  Offenbar können Tupel als endliche Familien aufgefasst werden,
  und zwar mit Indexmengen der Form $\setn{n}$ mit $n \in \NNone$.
\end{para}

\begin{satz}[Grundlegende Eigenschaften der W'keit]
  \label{pharma2/cha01:wkeit}
  Es sei $\ezwraum$ ein endlicher W'raum.
  Es seien $A,B$ Events und $\famii{C_{i}}$ eine endliche Familie von Events,
  d.h. $A,B \subseteq \Om$,
  und $I$ ist eine nichtleere, endliche Menge, und $C_{i} \subseteq \Om$ für alle $i \in I$.
  Dann gilt:
  \begin{enumerate}
  \item $\prob{\emptyset} = 0$
  \item $A \subseteq B \implies \prob{B \setminus A} = \prob{B} - \prob{A}$ \label{pharma2/cha01:wkeit:diff}
  \item $A \subseteq B \implies \prob{A} \leq \prob{B}$
  \item $\prob{A^{c}} = 1 - \prob{A}$
  \item $\prob{A \cup B} = \prob{A} + \prob{B} - \prob{A \cap B}$\par
    Gleichheit gilt offenbar, wenn $\prob{A \cap B} = 0$ gilt,
    was insbesondere dann der Fall ist, wenn $A \cap B = \emptyset$ gilt.
  \item $\prob{A \cup B} \leq \prob{A} + \prob{B}$ \label{pharma2/cha01:wkeit:leq}
  \item $\prob{\dotbigcup_{i \in I} C_{i}} = \sum_{i \in I} \prob{C_{i}}$,
    wenn $\famii{C_{i}}$ disjunkt ist,
    d.h. wenn $C_{i} \cap C_{j} = \emptyset$ für alle $i,j \in I$ mit $i \neq j$ gilt.
  \item $\prob{\bigcup_{i \in I} C_{i}} \leq \sum_{i \in I} \prob{C_{i}}$
  \end{enumerate}
\end{satz}

\begin{proof}
  \begin{enumerate}
  \item Es gilt mit der Additivität:
    \begin{IEEEeqnarray*}{0l}
      \prob{\emptyset} = \prob{\emptyset \dotcup \emptyset} = \prob{\emptyset} + \prob{\emptyset}
    \end{IEEEeqnarray*}
    Durch Abziehen von $\prob{\emptyset}$ folgt die Behauptung.
  \item Wegen $A \subseteq B$ gilt $B = (B \setminus A) \dotcup A$
    und somit $\prob{B} = \prob{B \setminus A} + \prob{A}$.
  \item Folgt direkt aus dem vorigen Punkt.
  \item Es gilt $\Om = A \dotcup A^{c}$, also $1 = \prob{\Om} = \prob{A} + \prob{A^{c}}$.
  \item Es gilt $A \cup B = A \dotcup (B \setminus (A \cap B))$
    und $A \cap B \subseteq B$.
    Es folgt, zunächst mit der Additivität und dann mit \autoref{pharma2/cha01:wkeit:diff}:
    \begin{IEEEeqnarray*}{0l}
      \prob{A \cup B} = \prob{A} + \prob{B \setminus (A \cap B)}
      = \prob{A} + \prob{B} - \prob{A \cap B}
    \end{IEEEeqnarray*}
  \item Folgt direkt aus dem vorigen Punkt.
  \item Für $\card{I} = 1$ ist es trivial, für $\card{I} = 2$ ist es die Additivität.
    Die übrigen Fälle folgen leicht mit Induktion aus der Additivität.
    Es ist wieder nicht schlimm, wenn Sie Induktion nicht kennen;
    überlegen Sie sich zur Übung nur, wie man die Fälle $\card{I} \in \set{3,4}$ erschließen kann.
  \item Folgt mit Induktion aus \autoref{pharma2/cha01:wkeit:leq}.\qedhere
  \end{enumerate}
\end{proof}

\begin{para}[Fortsetzungseigenschaft]
  Es sei $\ezwraum$ ein endlicher W'raum.
  Dann genügt es, wenn wir von $\prob{}$ gewisse Teile kennen,
  um andere Teile von $\prob{}$ zu erschließen.
  Wenn wir zum Beispiel $\prob{A}$ und $\prob{B}$
  für gewisse disjunkte Events $A,B$
  kennen, dann kennen wir $\prob{A \cup B}$, auch $\prob{A \dotcup B}$ geschrieben;
  nämlich nach der Additivität ist das $\prob{A} + \prob{B}$.
  \par
  Insbesondere gilt: Wenn wir $\prob{\set{\om}}$ für alle $\om \in \Om$ kennen,
  dann kennen wir $\prob{A}$ für alle $A \subseteq \Om$,
  nämlich es gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall A \subseteq \Om \holds \prob{A} = \sum_{\om \in A} \prob{\set{\om}}
  \end{IEEEeqnarray*}
  In der Tat kann man leicht zeigen:
  Für alle nichtleeren, endlichen Mengen $\Om$
  und alle $\fami{p_{\om}}{\om}{\Om} \in \intcc{0}{1}^{\Om}$ mit $\sum_{\om \in \Om} p_{\om} = 1$
  gibt es einen W'maß $\prob{}$ auf $\Om$ mit $\prob{\set{\om}} = p_{\om}$,
  und dieses W'maß ist dadurch vollständig festgelegt.
  \par
  Hier deutet sich das Hauptgeschäft der W'keitstheorie an,
  das wir an dieser Stelle salopp so zusammenfassen:
  Wir legen die W'keit einiger Events fest und berechnen daraus die W'keit anderer Events,
  an denen wir interessiert sind.
\end{para}

\section{Zufallsvariablen}

\begin{para}[ZV]
  Es sei $\ezwraum$ ein endlicher W'raum.
  Jede Funktion von $\Om$ nach $\RR$ nennen wir eine \term{Zufallsvariable} (ZV) über diesem W'raum.
  Wann immer wir mit mehreren ZV zur Zeit arbeiten,
  so versteht sich, dass diese über demselben W'raum sind
  (wenn nicht ausdrücklich anders erklärt).
  Es hat sich gezeigt, dass es meistens einfacher ist,
  mit ZV zu arbeiten und nicht direkt mit dem darunterliegenden W'raum.
  Das wird im Laufe dieses und der folgenden Kapitel deutlich werden.
\end{para}

\begin{para}[Events über logische Formeln]
  Über ZV lassen sich Events elegant formulieren.
  Wir führen die zugehörige Notation nicht formal ein, sondern es genügen einige Beispiele.
  Es sei $X$ eine ZV und $U \subseteq \RR$ und $u \in \RR$.
  Beispiele für Notationen und Redeweisen:
  \begin{IEEEeqnarray*}{0l+s}
    \event{X \in U} \df \set{\om \in \Om \suchthat X(\om) \in U}
    & ($X$ nimmt einen Wert in $U$ an) \\
    \event{X \leq u} \df \set{\om \in \Om \suchthat X(\om) \leq u}
    & ($X$ nimmt einen Wert höchstens $u$ an) \\
    \event{X = u} \df \set{\om \in \Om \suchthat X(\om) = u}
    & ($X$ nimmt den Wert $u$ an)
  \end{IEEEeqnarray*}
  Wenn wir die W'keiten solcher Events notieren,
  dann lassen wir die eckigen Klammern meistens fort, also wir haben:
  \begin{IEEEeqnarray*}{0l+s}
    \prob{X \in U}
    & (W'keit, dass $X$ einen Wert in $U$ annimmt) \\
    \prob{X \leq u}
    & (W'keit, dass $X$ einen Wert höchstens $u$ annimmt) \\
    \prob{X = u}
    & (W'keit, dass $X$ den Wert $u$ annnimmt)
  \end{IEEEeqnarray*}
  In naheliegender Weise setzen wir das auf alle geeigneten logischen Formeln fort,
  auch mit mehreren ZV.
  Zum Beispiel für eine endliche Familie $\famii{X_{i}}$ von ZV
  und $\famii{U_{i}} \in \cP(\RR)^{I}$,
  d.h. $\famii{U_{i}}$ ist eine mit $I$ indizierte Familie von Teilmengen von $\RR$,
  haben wir:
  \begin{IEEEeqnarray*}{0l}
    \prob{\bigwedge_{i \in I} \p{X_{i} \in U_{i}}}
    = \prob{\forall {i \in I} \holds X_{i} \in U_{i}}
    = \prob{\set{\om \in \Om \suchthat \forall {i \in I} \holds X_{i}(\om) \in U_{i}}}
  \end{IEEEeqnarray*}
  Das ist die W'keit, dass $X_{i}$ eine Wert in $U_{i}$ annimmt, für alle $i \in I$.
\end{para}

\begin{example}[Events über logische Formeln]
  Definiere als Ergebnisraum ${\Om \df \setn{10}^{3}}$.
  Definiere folgende ZV:
  \begin{IEEEeqnarray*}{0l+l}
    \ffnx{X}{\Om}{\RR}{\om \mapsto \om_{2}} &
    \ffnx{Y}{\Om}{\RR}{\om \mapsto \om_{1}+\om_{2}+\om_{3}} \\
    \ffnx{Z}{\Om}{\RR}{\om \mapsto \om_{1}\om_{2}}
  \end{IEEEeqnarray*}
  Dann gilt zum Beispiel:
  \begin{IEEEeqnarray*}{0l}
    \event{X < 4 \land Y = 5} \\
    \eqin = \set{(1,1,3),\, (2,1,2),\, (3,1,1),\, (1,2,2),\, (2,2,1),\, (1,3,1)} \\
    \event{Y^{2} \leq 9 \lor (X=7 \land Y \leq 12 \land Z \in \set{14,20})} \\
    \eqin = \set{(1,1,1),\, (7,2,1),\, (7,2,2),\, (7,2,3)} \\
    \event{X \in \set{7,8,10} \land Y \in \intic{11} \land Z \in \intcc{14}{21}} \\
    \eqin = \set{(7,2,1),\, (7,2,2),\, (7,3,1),\, (8,2,1)}
  \end{IEEEeqnarray*}
  Das letzte Event sehen wir uns genauer an,
  um besser mit dem Formalismus der Familien vertraut zu werden.
  Definiere $U_{1} \df \set{7,8,10}$ und $U_{2} \df \intic{11}$ und $U_{3} \df \intcc{14}{21}$.
  Dann ist $(U_{1},U_{2},U_{3})$, auch $(U_{i})_{i \in \setn{3}}$ geschrieben,
  eine Familie von Teilmengen von $\RR$;
  die Indexmenge dieser Familie ist $\setn{3}$.
  Es ist $T \df (X,Y,Z)$ eine Familie von ZV, auch mit Indexmenge $\setn{3}$,
  da es einfach ein Tupel ist.
  Das letzte Event ist also $\event{\bigwedge_{i \in \setn{3}} \p{T_{i} \in U_{i}}}$.
\end{example}

\begin{para}[Implikation]
  Es seien $\vphi$ und $\psi$ Formeln der oben vorgestellten Art.
  Dann schreiben wir $\vphi \evimplies \psi$ und sagen, $\vphi$ \term{impliziert} $\psi$,
  wenn $\event{\vphi} \subseteq \event{\psi}$ gilt.
  \par
  Man überlegt sich leicht, dass $\vphi \evimplies \psi$ jedenfalls dann gilt, wenn wir Folgendes haben:
  Betrachten wir die ZV in den beiden Formeln als freie Variablen,
  dann gilt die übliche Implikation $\vphi \implies \psi$ unter Allquantifizierung dieser Variablen
  über die Bildmengen der jeweiligen ZV.
\end{para}

\begin{example}[Implikation erkennen]
  Es sei $X$ eine ZV und $a,b \in \RR$ mit $a \leq b$.
  Über den Trick mit der Allquantifizierung erkennen wir auf einen Blick,
  dass $X \leq a \evimplies X \leq b$ gilt,
  denn es gilt $\forall x \in \RR \holds x \leq a \implies x \leq b$.
  Es gilt also $\event{X \leq a} \subseteq \event{X \leq b}$,
  also $\prob{X \leq a} \leq \prob{X \leq b}$ nach \autoref{pharma2/cha01:wkeit}.
\end{example}

\begin{para}[Äquivalenz]
  Wir schreiben $\vphi \eviff \psi$ und sagen, $\vphi$ und $\psi$ sind \term{äquivalent},
  wenn $\event{\vphi} = \event{\psi}$ gilt.
  Offenbar gilt dies genau dann, wenn $\vphi \evimplies \psi \land \psi \evimplies \vphi$ gilt.
  Den Trick mit der Allquantifizierung über reelle Variablen können wir auch hier anwenden.
\end{para}

\begin{para}[Gegenseitiger Ausschluss]
  Es seien $\trueprop$ und $\falseprop$ irgendwelche Aussagen,
  von denen die erste wahr und die zweite falsch ist, zum Beispiel $0=0$ \bzw $0\neq0$.
  Offenbar ergibt sich ${\event{\falseprop} = \emptyset}$ und $\event{\trueprop} = \Om$.
  Wir sagen, $\vphi$ und $\psi$ \term{schließen sich gegenseitig aus},
  wenn ${\vphi \land \psi \eviff \falseprop}$ gilt,
  was, wie man leicht sieht, äquivalent zu $\event{\vphi} \cap \event{\psi} = \emptyset$ ist.
  Der gegenseitige Ausschluss logischer Formeln entspricht also genau der Disjunktheit von Events.
  Wenn sich $\vphi$ und $\psi$ gegenseitig ausschließen, gilt natürlich $\prob{\vphi \land \psi} = 0$.
\end{para}

\begin{para}[Redeweise]
  Von nun an werden wir die Bezeichnung \enquote{Event} im Sinne einer logischen Formel
  wie den hier vorgestellten verstehen,
  wenn nicht anders ausgewiesen oder durch den Kontext anders bestimmt.
  Wir formulieren für zukünftige Referenz eine Version
  von \autoref{pharma2/cha01:wkeit} für solche Events:
\end{para}

\begin{satz}[Grundlegende Eigenschaften der W'keit -- Version für logische Formeln]
  Es seien $\vphi, \psi$ Events und $\phi = \famii{\phi_{i}}$ eine endliche Familie von Events.
  Dann gilt:
  \begin{enumerate}
  \item $\prob{\trueprop} = 1$ und $\prob{\falseprop} = 0$
  \item $\vphi \evimplies \psi \implies \prob{\psi \land \neg \vphi} = \prob{\psi} - \prob{\vphi}$
  \item $\vphi \evimplies \psi \implies \prob{\vphi} \leq \prob{\psi}$
  \item $\prob{\neg \vphi} = 1 - \prob{\vphi}$
  \item $\prob{\vphi \lor \psi} = \prob{\vphi} + \prob{\psi} - \prob{\vphi \land \psi}$\par
    Gleichheit gilt offenbar, wenn $\prob{\vphi \land \psi} = 0$ gilt.
    Letzteres ist insbesondere dann der Fall, wenn $\vphi$ und $\psi$ sich gegenseitig ausschließen,
    also gilt:
    $\prob{\vphi \lor \psi} = \prob{\vphi} + \prob{\psi}$,
    wenn sich $\vphi$ und $\psi$ gegenseitig ausschließen.
  \item $\prob{\vphi \lor \psi} \leq \prob{\vphi} + \prob{\psi}$
  \item $\prob{\dotbigvee_{i \in I} \phi_{i}} = \sum_{i \in I} \prob{\phi_{i}}$,
    wenn sich die Events in $\phi$ paarweise gegenseitig ausschließen,
    d.h. wenn sich $\phi_{i}$ und $\phi_{j}$ gegenseitig ausschließen für alle $i,j \in I$ mit $i \neq j$.
  \item $\prob{\bigvee_{i \in I} \phi_{i}} \leq \sum_{i \in I} \prob{\phi_{i}}$
  \end{enumerate}
\end{satz}

\section{Verteilung einer ZV}

\begin{para}[Verteilung]
  Es sei $X$ eine ZV.
  Definiere:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{\probX{}}{\cP(\RR)}{\intcc{0}{1}}{U \mapsto \prob{X \in U}}
  \end{IEEEeqnarray*}
  Wir nennen diese Funktion die \term{Verteilung} von $X$.
  Man sieht leicht:
  \begin{itemize}
  \item $\probX{\RR} = 1$
  \item $\forall A,B \subseteq \RR \with A \cap B = \emptyset
    \holds \probX{A \dotcup B} = \probX{A} + \probX{B}$
  \end{itemize}
  Es verhält sich $\probX{}$ also ganz ähnlich einem W'maß,
  nur dass die unterliegende Menge hier nicht endlich ist.
  (Tatsächlich ist $(\RR,\cP(\RR),\probX{})$ ein W'raum in einem allgemeineren Sinne,
  der in der W'theorie vorkommt.)
\end{para}

\begin{para}[PMF einer ZV, Verteilung]
  Wie können wir die Verteilung einer ZV $X$ konzise darstellen?
  Wir definieren den \term{Träger} von $X$ als:
  \begin{IEEEeqnarray*}{0l}
    \supp(X) \df \set{u \in \RR \suchthat \prob{X=u} > 0}
  \end{IEEEeqnarray*}
  Offenbar gilt $\supp(X) \subseteq \img(X)$, und letztere Menge ist endlich,
  also ist auch $\supp(X)$ endlich.
  Für alle $U \subseteq \RR$ gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{X \in U} = \prob{X \in U \cap \supp(X)} + \prob{X \in U \setminus \supp(X)} \\
    = \prob{X \in U \cap \supp(X)}
    = \sum_{u \in U \cap \supp(X)} \prob{X=u}
  \end{IEEEeqnarray*}
  Es genügt also, die W'keiten der \term{Punktevents} $X = u$ für alle $u \in \supp(X)$ zu kennen,
  um $\prob{X \in U}$ für alle $U \subseteq \RR$ zu berechnen.
  Mit dem Verständnis, dass die Summe auch bei unendlicher Menge $U$
  nur endlich viele Summanden $\neq 0$ enthält,
  schreibt man auch kurz:
  \begin{IEEEeqnarray*}{0l}
    \prob{X \in U} = \sum_{u \in U} \prob{X=u}
  \end{IEEEeqnarray*}
  Das werden wir zukünftig öfters kommentarlos so handhaben.
  \par
  Wir nennen folgende Funktion die \term{Zähldichte}
  oder die \term{probability mass function (PMF)} von $X$:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{\rho_{X}}{\RR}{\intcc{0}{1}}{u \mapsto \prob{X=u}}
  \end{IEEEeqnarray*}
  Die PMF einer ZV $X$ hängt offenbar nur von ihrer Verteilung ab.
  Daher macht es Sinn, von der \term{PMF einer Verteilung} zu sprechen;
  das ist die PMF, welche alle ZV mit dieser Verteilung haben.
  Später werden wir Symbole für bestimmte wichtige Verteilungen einführen.
  Ist $\cD$ eine Verteilung, so notieren wir mit $\rho_{\cD}$ die zugehörige PMF.
\end{para}

\begin{para}[PMF]
  Für jede Funktion $\ffn{\rho}{\RR}{\intcc{0}{1}}$ definieren wir deren \term{Träger} als:
  \begin{IEEEeqnarray*}{0l}
    \supp(\rho) \df \set{u \in \RR \suchthat \rho(u) > 0}
  \end{IEEEeqnarray*}
  Wir nennen $\rho$ eine \term{Zähldichte} oder eine \term{probability mass function (PMF)},
  wenn gilt:
  \begin{itemize}
  \item $\supp(\rho)$ ist endlich;\footnote{%
      In der Literatur werden die Bezeichnungen
      \enquote{Zähldichte} oder \enquote{probability mass function (PMF)}
      meistens für einen etwas allgemeineren Begriff verwendet,
      wo $\supp(\rho)$ einen gewissen Grad an Unendlichkeit haben darf.
      Bedenken Sie dies, wenn Sie Literatur studieren.}
  \item $\sum_{u \in \supp(\rho)} \rho(u) = 1$.
  \end{itemize}
  Offenbar ist die PMF einer jeden ZV eine PMF in diesem Sinne.
  Der folgende Satz zeigt, dass auch der umgekehrte Weg möglich ist.
\end{para}

\begin{satz}[Existenz bei gegebener PMF]
  Es sei $\ffn{\rho}{\RR}{\intcc{0}{1}}$ eine PMF.
  Dann gibt es einen endlichen W'raum und darüber eine ZV $X$ so,
  dass $\rho_{X} = \rho$ gilt.
  Ferner kann $X$ so gewählt werden, dass $\img(X) = \supp(\rho)$ gilt.
\end{satz}

\begin{proof}
  Wähle Ergebnisraum $\Om \df \supp\p{\rho}$,
  und definiere $\prob{\set{\om}} \df \rho(\om)$ für alle $\om \in \Om$;
  damit liegt $\prob{}$ fest nach der Fortsetzungseigenschaft.
  Definiere schließlich $\ffnx{X}{\Om}{\RR}{\om \mapsto \om}$.
\end{proof}

\begin{example}[PMF]
  Es seien $n \in \NNone$ und $q \in \intoo{0}{1}$.
  Definiere $c \df \frac{1-q^{n+1}}{1-q}$
  und $\ffnx{\rho}{\setft{0}{n}}{\intcc{0}{1}}{k \mapsto \frac{1}{c} q^{k}}$.
  Mit der bekannten Formel für die Geometrischen Reihe sieht man,
  dass $\rho$ eine PMF ist.
  Nach obigem Satz gibt es eine ZV $X$ (über einem endlichen W'raum),
  deren PMF genau dieses $\rho$ ist.
  Es gilt $\supp(X) = \supp(\rho) = \setft{0}{n}$.
  \par
  Betrachte den Fall $n \df 10$ und $q \df \frac{2}{7}$;
  dann gilt $c \approx 1.400$.
  Wir berechnen beispielsweise:
  \begin{IEEEeqnarray*}{0l}
    \prob{X \leq 2 \lor X > 8} = \frac{1}{c} \p{\rho(0) + \rho(1) + \rho(2) + \rho(9) + \rho(10)}
    \approx 0.6976
  \end{IEEEeqnarray*}
  Beachte, wie wir uns bei der Berechnung auf den Träger konzentrieren dürfen.
\end{example}

\begin{para}[Uniforme Verteilung]
  Es sei $M \subseteq \RR$ endlich.
  Für eine ZV $X$ schreiben wir $X \distr \Unif(M)$
  und sagen, dass $X$ auf $M$ \term{uniform verteilt} ist
  oder auf $M$ \term{uniforme Verteilung} hat, wenn gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall u \in M \holds \prob{X=u} = \frac{1}{\card{M}}
  \end{IEEEeqnarray*}
  Zu jeder solchen Menge $M$ findet man eine solche ZV nach dem Existenzsatz,
  denn Folgendes ist eine PMF, wie man leicht nachrechnet:
  \begin{IEEEeqnarray*}{0l}
    \fnx{\RR}{\intcc{0}{1}}{u \mapsto
      \begin{cases}
        \frac{1}{\card{M}} & u \in M \\
        0 & \text{sonst}
      \end{cases}}
  \end{IEEEeqnarray*}
  Dies ist also $\rho_{\Unif(M)}$, die PMF der Verteilung $\Unif(M)$.
\end{para}

\begin{para}[Bernoulliverteilung]
  Es sei $p \in \intcc{0}{1}$.
  Für eine ZV $X$ schreiben wir ${X \distr \Ber(p)}$
  und sagen, dass $X$ \term{Bernoulliverteilung mit Erfolgsw'keit} $p$ hat,
  wenn gilt:
  \begin{IEEEeqnarray*}{0l+l}
    \prob{X=1} = p & \prob{X=0} = 1-p
  \end{IEEEeqnarray*}
  Wieder nach dem Existenzsatz findet man zu jedem $p$ eine solche ZV,
  denn Folgendes ist eine PMF:
  \begin{IEEEeqnarray*}{0l}
    \fnx{\RR}{\intcc{0}{1}}{u \mapsto
      \begin{cases}
        p & u = 1 \\
        1-p & u = 0 \\
        0 & \text{sonst}
      \end{cases}}
  \end{IEEEeqnarray*}
  Dies ist also $\rho_{\Ber(p)}$, die PMF der Verteilung $\Ber(p)$.
\end{para}

\section{Verteilung einer Familie von ZV}

Oft hantiert man mit mehreren ZV über demselben endlichen W'raum zur Zeit.
Jede dieser ZV hat eine Verteilung nach vorigem Abschnitt.
Alleine diese Verteilungen zu kennen, genügt aber oft nicht.
Es seien $X$ und $Y$ ZV, deren Verteilung uns jeweils bekannt sei.
Dann kennen wir $\prob{X \in U}$ und $\prob{Y \in V}$ für alle $U,V \subseteq \RR$.
Aber wir kennen ohne Weiteres noch nicht $\prob{X \in U \land Y \in V}$.
Dies führt auf den Begriff der Verteilung einer Familie, hier der Familie $(X,Y)$.

Wir benötigen eine Vorbereitung:

\begin{para}[Kartesisches Produkt einer Familie]
  Es sei $I$ eine endliche Indexmenge und $D$ eine Menge.
  Für eine Familie $M = \famii{M_{i}} \in \cP(D)^{I}$
  von Teilmengen von $D$ definiere deren kartesisches Produkt:
  \begin{IEEEeqnarray*}{0l}
    \bigtimes_{i \in I} M_{i} = \set{\famii{x_{i}} \suchthat \forall i \in I \holds x_{i} \in M_{i}}
  \end{IEEEeqnarray*}
  Im Fall $I = \setn{n}$ für ein $n \in \NN$ ist dies das bekannte kartesische Produkt von~$n$ Mengen.
  Ist $M_{i}$ endlich für jedes~$i$, so ist $\bigtimes_{i \in I} M_{i}$ auch endlich.
  \par
  Wir verwenden meistens eine vereinfachte Schreibweise,
  nämlich wir schreiben schlicht die Familie selbst,
  also $M$ oder $\famii{M_{i}}$, anstelle von $\bigtimes_{i \in I} M_{i}$.
  Genauer schreiben wir für alle $x = \famii{x_{i}} \in D^{I}$:
  \begin{IEEEeqnarray*}{0l}
    x \in \famii{M_{i}} \dfiff \forall i \in I \holds x_{i}  \in M_{i}
  \end{IEEEeqnarray*}
  Die Mengeninklusion $\subseteq$ wird entsprechend verstanden.
  Unsere Schreibweise ist vor allem daher von Vorteil,
  weil sie auch im Fall $\card{I}=1$ natürlich erscheint.
\end{para}

\begin{para}[Verteilung einer Familie]
  Die Verteilung einer Familie von ZV ist ganz ähnlich definiert wie die einer einzelnen ZV,
  es wird nur notationell aufwendiger.
  Es sei $X = \famii{X_{i}}$ eine endliche Familie von ZV.
  Für Familien $u = \famii{u_{i}} \in \RR^{I}$ und $U = \famii{U_{i}} \in \cP(\RR)^{I}$
  verstehen wir nach vorigem Absatz die folgenden Events:
  \begin{IEEEeqnarray*}{0l+l}
    X \in U \eviff \forall i \in I \holds X_{i} \in U_{i} &
    X = u \eviff \forall i \in I \holds X_{i} = u_{i}
  \end{IEEEeqnarray*}
  Definiere:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{\probX{}}{\cP(\RR^{I})}{\intcc{0}{1}}{U \mapsto \prob{X \in U}}
  \end{IEEEeqnarray*}
  Wir nennen diese Funktion die \term{Verteilung} von $X$.
  Im Falle $\card{I} \geq 2$ nennen wir es zur Verdeutlichung,
  dass wir mehrere ZV haben, manchmal die \term{gemeinsame Verteilung} von~$X$.
  Man sieht wieder leicht:
  \begin{itemize}
  \item $\probX{\RR^{I}} = 1$
  \item $\forall A,B \subseteq \RR^{I} \with A \cap B = \emptyset
    \holds \probX{A \dotcup B} = \probX{A} + \probX{B}$
  \end{itemize}
\end{para}

\begin{para}[PMF einer Familie]
  Wieder suchen wir nach Wegen, eine solche Verteilung konzise anzugeben.
  Definiere den \term{Träger} von $X = \famii{X_{i}}$ als:
  \begin{IEEEeqnarray*}{0l}
    \supp(X) \df \set{u \in \RR^{I} \suchthat \prob{X=u} > 0}
    \subseteq \famii{\img(X_{i})}
  \end{IEEEeqnarray*}
  Der Träger ist als Teilmenge des kartesischen Produktes der Bildmengen wieder endlich.
  Analog zum Fall einer einzelnen ZV überlegt man sich, dass gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{X \in U}
    = \sum_{u \in U \cap \supp(X)} \prob{X=u}
    = \sum_{u \in U} \prob{X=u}
  \end{IEEEeqnarray*}
  Wir erhalten also äußerlich dieselbe Formel.
  Der Unterschied zu vorher ist, dass wir bei den Summen über Familien von Zahlen laufen,
  deren Indexmenge jeweils $I$ ist.
  Die letzte Summe schreiben wir wieder im Wissen, dass nur endlich viele Summanden $\neq 0$ sind.
  Wir nennen die Funktion $\ffnx{\rho_{X}}{\RR^{I}}{\intcc{0}{1}}{u \mapsto \prob{X=u}}$
  die \term{Zähldichte} oder die \term{probability mass function (PMF)} von $X$.
  Im Falle $\card{I} \geq 2$ sprechen wir zur Verdeutlichung manchmal von der \term{gemeinsamen PMF} von~$X$.
  Der Fall $\card{I} = 1$ entspricht genau dem aus dem vorigen Abschnitt Bekannten.
  \par
  Offenbar hängt die PMF einer Familie von ZV nur von deren Verteilung ab.
  Man spricht daher auch hier von der PMF einer Verteilung,
  und wir schreiben $\rho_{\cD}$ dafür, wobei $\cD$ die Verteilung bezeichnet.
\end{para}

\begin{para}[Notation als Tupel]
  Wer die Familien unhandlich findet, darf gerne zunächst an den Spezialfall $I=\setn{n}$
  mit $n \in \NNone$ denken, also wo wir ein Tupel $X=(\eli{X}{n})$ von ZV haben.
  Dann sieht das so aus:
  \begin{IEEEeqnarray*}{0l}
    X = u \eviff (\eli{X}{n}) = (\eli{u}{n}) \eviff \forall i \in \setn{n} \holds X_{i} = u_{i} \\
    \supp(X) = \set{u \in \RR^{n} \suchthat \prob{X=u} > 0}
    \subseteq \bigtimes_{i=1}^{n} \img(X_{i}) \subseteq \RR^{n} \\
    \ffnx{\rho_{X}}{\RR^{n}}{\intcc{0}{1}}{u \mapsto \prob{X=u}}
  \end{IEEEeqnarray*}
  Wem auch das noch unheimlich ist, schreibe zunächst den Fall $n=2$ aus, dann $n=3$.
\end{para}

\begin{para}[PMF]
  Es sei $I$ eine endliche Indexmenge.
  Für jede Funktion $\ffn{\rho}{\RR^{I}}{\intcc{0}{1}}$ definieren wir deren \term{Träger} als:
  \begin{IEEEeqnarray*}{0l}
    \supp(\rho) \df \set{u \in \RR^{I} \suchthat \rho(u) > 0}
  \end{IEEEeqnarray*}
  Wir nennen $\rho$ eine \term{Zähldichte} oder eine \term{probability mass function (PMF)},
  wenn gilt:
  \begin{itemize}
  \item $\supp(\rho)$ ist endlich;
  \item $\sum_{u \in \supp(\rho)} \rho(u) = 1$.
  \end{itemize}
  Offenbar ist die PMF einer jeden endlichen Familie von ZV eine PMF in diesem Sinne.
  Der folgende Satz zeigt, dass auch der umgekehrte Weg möglich ist:
\end{para}

\begin{satz}[Existenz bei gegebener PMF -- Version für Familien]
  Es sei $I$ eine endliche Indexmenge und $\ffn{\rho}{\RR^{I}}{\intcc{0}{1}}$ eine PMF.
  Dann gibt es einen endlichen W'raum und darüber eine Familie $X = \famii{X_{i}}$ von ZV so,
  dass $\rho_{X} = \rho$ gilt.
  Ferner kann $X$ so gewählt werden, dass $\img(X) = \supp(\rho)$ gilt.
\end{satz}

\begin{proof}
  Wähle Ergebnisraum $\Om \df \supp\p{\rho}$,
  und definiere $\prob{\set{\om}} \df \rho(\om)$ für alle $\om \in \Om$;
  damit liegt $\prob{}$ fest nach der Fortsetzungseigenschaft.
  Für alle $i \in I$ definiere schließlich $\ffnx{X_{i}}{\Om}{\RR}{\om \mapsto \om_{i}}$.
  Die erforderlichen Eigenschaften rechnet man leicht nach
  (Sie brauchen das aber nicht unbedingt zu tun).
\end{proof}

\begin{para}[Verteilung einer Teilfamilie]
  Es sei $\famii{X_{i}}$ eine endliche Familie von ZV,
  deren Verteilung uns bekannt sein möge.
  Dann kennen wir auch die Verteilung von $\famij{X_{j}}$ für alle $J \subseteq I$,
  insbesondere kennen wir die Verteilung von $X_{i}$ für alle $i \in I$.
  Den Beweis dafür führen wir nicht in Allgemeinheit,
  da das zwar nicht schwierig, aber notationell aufwendig ist.
  Wir beweisen nur einen Spezialfall, nämlich $I = \setn{n}$ mit $n \geq 3$,
  und wir möchten die Verteilung von $(X_{1}, X_{2})$ berechnen.
  Es sei $(u_{1}, u_{2}) \in \RR^{2}$, und notiere für später:
  \begin{IEEEeqnarray*}{0l}
    A \df \set{a \in \RR^{n} \suchthat
      (a_{1}, a_{2}) = (u_{1}, u_{2})
      \land (a_{3}, \hdots, a_{n}) \in \supp((X_{3}, \hdots, X_{n}))}
  \end{IEEEeqnarray*}
  Es gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{(X_{1}, X_{2}) = (u_{1}, u_{2})} \\
    = \prob{(X_{1}, X_{2}) = (u_{1}, u_{2}) \land (X_{3}, \hdots, X_{n}) \in \RR^{n-2}} \\
    = \prob{(X_{1}, X_{2}) = (u_{1}, u_{2}) \land (X_{3}, \hdots, X_{n}) \in \supp((X_{3}, \hdots, X_{n}))} \\
    = \prob{X \in A} = \sum_{a \in A} \prob{X = a}
    = \sum_{a \in A} \rho_{X}(a)
  \end{IEEEeqnarray*}
  Damit haben wir $\prob{(X_{1}, X_{2}) = (u_{1}, u_{2})}$ auf die Summe
  gewisser Werte der bekannten PMF von $X$ zurückgeführt.
  Diese Summe können wir offenbar auch so schreiben:
  \begin{IEEEeqnarray*}{0l}
    \sum_{(a_{3}, \hdots, a_{n}) \in \supp((X_{3}, \hdots, X_{n}))} \rho_{X}(u_{1}, u_{2}, \elix{a}{3}{n})
  \end{IEEEeqnarray*}
  Daran sieht man gut, dass die ersten beiden Argumente an die PMF fest sind,
  die übrigen $n-3$ Argumente laufen.
  In der Praxis muss man das nicht immer soweit umformen,
  sondern es genügt, eine Form herzustellen, an der man den Bezug zur PMF sieht.
  Dafür kann auch schon diese Darstellung dienen:
  \begin{IEEEeqnarray*}{0l}
    \prob{(X_{1}, X_{2}) = (u_{1}, u_{2}) \land (X_{3}, \hdots, X_{n}) \in \supp((X_{3}, \hdots, X_{n}))}
  \end{IEEEeqnarray*}
  Wir werden das in Kürze in Beispielen sehen.
  Eine Rechnung dieser Art nennt man eine \term{Marginalisierung}.
  Eine angebliche Motivation dieser Bezeichnung liegt darin,
  dass die Einbringung des Events
  \begin{IEEEeqnarray*}{0l}
    (X_{3}, \hdots, X_{n}) \in \RR^{n-2}
  \end{IEEEeqnarray*}
  in der zweiten Zeile der Rechnung zum Ausdruck bringt,
  dass uns egal ist, was die ZV in der Familie $(X_{3}, \hdots, X_{n})$ machen.
  Diese ZV werden somit marginalisiert.
  Es gibt noch eine ganz andere Motivation für diese Bezeichnung,
  die wir im folgenden Beispiel kennenlernen werden.
\end{para}

\begin{example}[Marginalisierung]
  Die Verteilung von $(X,Y)$ sei durch die in folgender Tabelle gegebene PMF von $(X,Y)$ festgelegt,
  was laut unserem Existenzsatz zulässig ist,
  da die Zahlen aus $\intcc{0}{1}$ sind und sich insgesamt zu~$1$ addieren:\par
  \begin{tabular}{r|n{1}{2}n{1}{2}n{1}{2}n{1}{2}}%
    \toprule
    \diagbox{$X$}{$Y$}&1&2&3&4\\
    \midrule
    1&0.1&0.15&0.1&0.2\\
    2&0.05&0.01&0.05&0.02\\
    3&0.04&0.25&0.02&0.01\\
    \bottomrule
  \end{tabular}
  \medskip\par
  Die Verteilungen von $X$ und $Y$ ergeben sich durch Summation
  entlang der Zeilen \bzw Spalten.
  Zum Beispiel gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{X=1} = \prob{X=1 \land Y \in \setn{4}} \\
    = \sum_{v \in \setn{4}} \prob{X=1 \land Y = v}
    = 0.1 + 0.15 + 0.1 + 0.2 = 0.55
  \end{IEEEeqnarray*}
  Dies führen wir für alle Zeilen und Spalten aus und schreiben die Ergebnisse in den Rand der Tabelle,
  auf Englisch also in den \textit{margin};
  dies ist die zweite mögliche Motivation für die Bezeichnung \enquote{Marginalisierung}.\par
  \begin{tabular}{r|n{1}{2}n{1}{2}n{1}{2}n{1}{2}|n{1}{2}}%
    \toprule
    \diagbox{$X$}{$Y$}&1&2&3&4\\
    \midrule
    1&0.1&0.15&0.1&0.2&0.55\\
    2&0.05&0.01&0.05&0.02&0.13\\
    3&0.04&0.25&0.02&0.01&0.32\\
    \bottomrule & 0.19 & 0.41 & 0.17 & 0.23
  \end{tabular}
  \medskip\par
\end{example}

Wir verallgemeinern die uniforme Verteilung:

\begin{para}[Uniforme Verteilung -- allgemeine Version]
  Es sei $I$ eine endliche Indexmenge und $M \subseteq \RR^{I}$ endlich.
  Für eine Familie ${X = \famii{X_{i}}}$ von ZV schreiben wir $X \distr \Unif(M)$
  und sagen, dass $X$ auf $M$ \term{uniform verteilt} ist
  oder auf $M$ \term{uniforme Verteilung} hat, wenn gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall u \in M \holds \prob{X=u} = \frac{1}{\card{M}}
  \end{IEEEeqnarray*}
  Zu jeder solchen Menge $M$ findet man eine solche Familie von ZV nach Existenzsatz,
  denn Folgendes ist eine PMF, wie man leicht nachrechnet:
  \begin{IEEEeqnarray*}{0l}
    \fnx{\RR^{I}}{\intcc{0}{1}}{u \mapsto
      \begin{cases}
        \frac{1}{\card{M}} & u \in M \\
        0 & \text{sonst}
      \end{cases}}
  \end{IEEEeqnarray*}
  Dies ist also $\rho_{\Unif(M)}$, die PMF der Verteilung $\Unif(M)$.
\end{para}

\begin{example}[Uniforme Verteilung auf Teilmenge des $\RR^{2}$]
  Es sei:
  \begin{IEEEeqnarray*}{0l}
    (X_{1}, X_{2}) \distr \Unif(\set{(0,1), (0,2), (2,1)})
  \end{IEEEeqnarray*}
  Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{(X_{1}, X_{2}) = (0,1)}
    = \prob{(X_{1}, X_{2}) = (0,2)}
    = \prob{(X_{1}, X_{2}) = (2,1)}
    = \frac{1}{3}
  \end{IEEEeqnarray*}
  Beispielhaft führen wir eine Marginalisierung aus:
  \begin{IEEEeqnarray*}{0l}
    \prob{X_{1} = 0}
    = \prob{(X_{1} = 0) \land (X_{2} = 1 \lor X_{2} = 2)} \\
    = \prob{(X_{1}, X_{2}) = (0,1) \lor (X_{1}, X_{2}) = (0,2)} \\
    = \prob{(X_{1}, X_{2}) = (0,1)} + \prob{(X_{1}, X_{2}) = (0,2)}
    = \frac{2}{3}
  \end{IEEEeqnarray*}
\end{example}

\lxklKitWipWarning

\section{Grundlagen der Anwendung}

%%% TODO (im wesentlichen Kopie, aber Erklärungen mit ZV machen)

\section{Konditionierung und Unabhängigkeit}

%%% TODO
%%% - Definitionen (Events und ZV)
%%% - Charakterisierung der Unabhängigkeit (über Konditionierung)
%%% - Motivation der Konditionierung
%%% - Umgang mit W'keit in 3. Version (mit Konditionierung)
%%% - Blocklemma
%%% - Bayes --> später bei den Tests (cha02)
