\chapter{Erwartungswert und Varianz diskreter Verteilungen}
\label{wtfusion/cha04}

Es sei in diesem Kapitel, wenn nicht anders ausgewiesen,
$(\Om, \SIG, \prob{})$ ein W'raum
und $\ZV$ die Menge aller ZV darüber.

\section{Erwartungswert}

\begin{para}[Definition]
  Es sei $X \in \ZV$ diskret.
  Wir sagen, dass $X$ einen Erwartungswert (EW) besitzt oder eine ZV mit EW ist,
  wenn $\sum_{u \in \supp(X)} \abs{u} \cdot \prob{X=u} \in \RR$ gilt.
  Es besitze nun $X$ einen EW,
  dann definieren wir den \term{Erwartungswert} (EW) von $X$ als:
  \begin{IEEEeqnarray*}{0l}
    \expect{X} \df \sum_{u \in \supp(X)} u \cdot \prob{X=u}
    = \sum_{u \in \RR} u \cdot \prob{X=u}
  \end{IEEEeqnarray*}
  Die Voraussetzung (dass $X$ einen EW besitzt)
  stellt sicher, dass die Reihe unbedingt konvergiert,
  also in jeder Umordnung zur selben Summe konvergiert.
  Die zweite Darstellung, wo wir über $u \in \RR$ laufen,
  verwenden wir im Verständnis, dass nur abzählbar viele Summanden $\neq 0$ sind.
  Wenn $X$ endlichen Träger hat, dann besitzt $X$ offenbar einen EW,
  und obige Summen sind endlich.
\end{para}

\begin{para}[EW einer Verteilung]
  Offenbar hängt der EW einer ZV nur von deren Verteilung ab,
  daher macht der Begriff des EW einer Verteilung Sinn.
  Wir bezeichnen den EW einer Verteilung $D$ mit $\expect{}_{D}$.
  Zum Beispiel gilt $\expect{}_{\Ber(p)} = p$ für alle $p \in \intcc{0}{1}$,
  wie man sofort sieht (Übung).
\end{para}

Das Bemerkenswerte an folgendem Satz ist, dass wir den EW von $f \circ X$ berechnen können,
ohne explizit die Verteilung von $f \circ X$ zu betrachten;
wir benutzen nur die Verteilung von $X$ in der Formel.
Der Name \enquote{LOTUS} steht für \enquote{Law of the Unconscious Statistician}.
Dieser Name beruht darauf, dass manchmal übersehen wird,
dass dies ein Satz ist und nicht die Definition von~$\expect{f \circ X}$.
Beachte auch, dass $f \circ X$ keinen EW zu besitzen braucht (Übung: Gegenbeispiel konstruieren);
das ist daher eine \emphasis{Voraussetzung} des Satzes.

\begin{satz}[LOTUS]
  Es sei $\zvtup{X} = (\eli{X}{n}) \in \ZV^{n}$ diskret.
  Es sei $\Lam \subseteq \RR^{n}$ und $\ffn{f}{\Lam}{\RR}$.
  Es möge $\img(\zvtup{X}) \subseteq \Lam$
  und $f \circ \zvtup{X} \in \ZV$ gelten und $f \circ \zvtup{X}$ einen EW besitzen.
  Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    \expect{f \circ \zvtup{X}} = \sum_{u \in \supp(\zvtup{X})} f(u) \cdot \prob{\zvtup{X} = u}
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}[nur endlicher Träger]
  Der Einfachheit halber geben wir nur einen Beweis für den Fall,
  dass $\supp(\zvtup{X})$ endlich ist
  (für den allgemeinen Fall müsste man den
  \enquote{Großen Umordnungssatz} für Reihen bemühen~\cite[Abschnitt~6.3]{Koenigsberger04Ana1}).
  Es gilt:
  \begin{IEEEeqnarray*}{0l"s}
    \expect{f \circ \zvtup{X}}
    \eqin = \sum_{v \in \supp(f \circ \zvtup{X})} v \cdot \prob{f \circ \zvtup{X} = v} \\
    \eqin = \sum_{v \in \fnimg{f}(\supp(\zvtup{X}))} v \cdot \prob{f \circ \zvtup{X} = v} \\
    \eqin = \sum_{v \in \fnimg{f}(\supp(\zvtup{X}))} v
    \sum_{u \in \supp(\zvtup{X})} \prob{f \circ \zvtup{X}=v \land \zvtup{X}=u}
    & Additivität \\
    \eqin = \sum_{v \in \fnimg{f}(\supp(\zvtup{X}))}
    \sum_{u \in \supp(\zvtup{X})} v \cdot \prob{f \circ \zvtup{X}=v \land \zvtup{X}=u} \\
    \eqin = \sum_{v \in \fnimg{f}(\supp(\zvtup{X}))}
    \sum_{u \in \supp(\zvtup{X})} f(u) \cdot \prob{f(u)=v \land \zvtup{X}=u} \\
    \eqin = \sum_{u \in \supp(\zvtup{X})}
    \sum_{v \in \fnimg{f}(\supp(\zvtup{X}))} f(u) \cdot \prob{f(u)=v \land \zvtup{X}=u} \\
    \eqin = \sum_{u \in \supp(\zvtup{X})} f(u)
    \sum_{v \in \fnimg{f}(\supp(\zvtup{X}))} \prob{f(u)=v \land \zvtup{X}=u} \\
    \eqin = \sum_{u \in \supp(\zvtup{X})} f(u) \cdot \prob{\zvtup{X}=u}
  \end{IEEEeqnarray*}
  Letzteres gilt, da die zweite Summe für genau ein $v$, nämlich für $v=f(u)$,
  einen Beitrag~$\neq 0$ liefert, und dieser ist $\prob{X=u}$.
\end{proof}

Das Bemerkenswerte an folgendem Satz ist,
dass er ohne die Unabhängigkeit der ZV auskommt.

\begin{satz}[Linearität des EW]
  Es seien $\eli{X}{n} \in \ZV$ diskret mit EW und ${\eli{\lam}{n} \in \RR}$.
  Dann bestizt $\sum_{i=1}^n \lam_i X_i$ einen EW, und gilt:
  \begin{IEEEeqnarray*}{0l}
    \expect{\sum_{i=1}^n \lam_i X_i} = \sum_{i=1}^n \lam_i \expect{X_i}
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}[nur endlicher Träger]
  Es genügt, die Aussage für zwei ZV zu zeigen,
  weil die allgemeine Version durch triviale Induktion daraus folgt.
  Es seien also $X,Y \in \ZV$ diskret mit EW und,
  wieder der Einfachheit halber, mit endlichem Träger.
  Es seien $\lam_{X}, \lam_{Y} \in \RR$.
  Definiere $\ffnx{f}{\RR^{2}}{\RR}{(u,v) \mapsto \lam_{X} u + \lam_{Y} v}$.
  Dann ist $f$ stetig, und mit LOTUS gilt:
  \begin{IEEEeqnarray*}{0l+x*}
    \expect{\lam_X X + \lam_{Y} Y}
    = \expect{f \circ (X,Y)} \\
    \eqin = \sum_{(u,v) \in \RR^{2}} f(u,v) \cdot \prob{(X,Y)=(u,v)} \\
    \eqin = \sum_{u \in \RR} \sum_{v \in \RR} (\lam_{X} u + \lam_{Y} v) \cdot \prob{(X,Y)=(u,v)} \\
    \eqin = \lam_{X} \sum_{u \in \RR} u \sum_{v \in \RR} \prob{(X,Y)=(u,v)}
    + \lam_{Y} \sum_{v \in \RR} v \sum_{u \in \RR} \prob{(X,Y)=(u,v)} \\
    \eqin = \lam_{X} \sum_{x \in \RR} u \cdot \prob{X=u}
    + \lam_{Y} \sum_{v \in \RR} v \cdot \prob{Y=v} \\
    \eqin = \lam_{X} \expect{X} + \lam_{Y} \expect{Y} & \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\begin{satz}[Einfache Folgerungen aus der Linearität]
  \hfill
  \begin{enumerate}
  \item Verschiebung: Es sei $X \in \ZV$ diskret mit EW und $a \in \RR$.
    Dann gilt $\expect{X+a} = \expect{X} + a$.
  \item Monotonie: Es seien $X,Y \in \ZV$ diskret mit EW so,
    dass $\prob{X \leq Y} = 1$ gilt.\footnote{%
      Letzteres gilt insbesondere wenn $X \leq Y$ gilt,
      womit wir das punktweise $\leq$ zwischen Funktionen menen,
      also $X(\om) \leq Y(\om)$ für alle $\om \in \Om$.}
    Dann gilt $\expect{X} \leq \expect{Y}$.
  \item Es gilt $\expect{}_{\Bin(n,p)} = np$ für alle $n \in \NNone$ und alle $p \in \intcc{0}{1}$.
  \end{enumerate}
\end{satz}

\begin{proof}
  \begin{enumerate}
  \item Fasse $a$ als konstante ZV auf.
  \item Definiere $Z \df Y - X$.
    Dann gilt $\supp(Z) \in \RRnn$, also $\expect{Z} \geq 0$ nach Definition.
    Es folgt $\expect{Y} - \expect{X} = \expect{Y-X} = \expect{Z} \geq 0$.
  \item Es sei $\zvtupiid{X}{\Ber(p)}{n}$ \iid über irgeneinem W'raum;
    wir wissen, dass es so etwas gibt.
    Es gilt $S \df \sum_{i=1}^{n} X_{i} \distr \Bin(n,p)$,
    und da der EW nur von der Verteilung abhängt, gilt:
    \begin{IEEEeqnarray*}{0l+x*}
      \expect{}_{\Bin(n,p)} = \expect{S}
      = \sum_{i=1}^{n} \expect{X_{i}}
      = n \cdot \expect{}_{\Ber(p)} = np & \qedhere
    \end{IEEEeqnarray*}
  \end{enumerate}
\end{proof}

\begin{para}[IVZ]
  Oft ist folgender einfacher Begriff nützlich.
  Es sei $A \in \SIG$.
  Wir definieren die \term{Indikatorzufallsvariable} (IZV) von $A$ als:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{\chi_{A}}{\Om}{\RR}{\om \mapsto
      \begin{cases}
        1 & \om \in A \\
        0 & \text{sonst}
      \end{cases}}
  \end{IEEEeqnarray*}
  Offenbar handelt es sich tatsächlich um eine ZV.
  Es gilt $\chi_{A} \distr \Ber(\prob{A})$ und $\expect{\chi_{A}} = \prob{A}$.
\end{para}

\begin{example}[Analyse von Algorithmen]
  Ein Algorithmus erhält als Eingabe $v \in \setn{n}_{\neq}^{n}$,
  also eine Permutation der natürlichen Zahlen von $1$ bis $n$,
  wobei dem Algorithmus zuerst $v_{1}$, dann $v_{2}$, \usw gezeigt wird,
  und zuletzt $v_{n}$.
  Wenn bei Präsentation von $v_{k}$ gilt, dass $v_{i} \leq v_{k}$ für alle $i \leq k$ ist,
  dann führt der Algorithmus eine kostspielige Operation $H$ aus.
  \par
  Längere Geschichte:
  Dem Algorithmus werden nacheinander $n$ Kandidaten gezeigt,
  die je mit einer Bewertung daherkommen,
  wobei jede Bewertung eine Zahl aus $\setn{n}$ ist
  und die Zuordnung von Kandidaten zu Bewertungen injektiv ist.
  Der Algorithmus setzt den im Schritt $k$ präsentierten Kandidaten für eine bestimmte Position ein
  und entlässt (außer im ersten Schritt, wo die Position noch unbesetzt ist)
  den bisher dort befindlichen Kandidaten,
  wenn der in Schritt $k$ präsentierte Kandidat besser als alle bis dahin gesehenen Kandidaten ist;
  das ist gerade die Bedingung $\forall i \leq k \holds v_{i} \leq v_{k}$.
  Dieser Vorgang wird als kostspielig angenommen und ist die genannte Operation $H$.
  \par
  Wir betrachten die Anzahl an Operationen $H$.
  Diese Anzahl ist im günstigsten Fall~$1$,
  nämlich wenn $v_{i} > v_{i+1}$ für alle $i \in \setn{n-1}$ gilt.
  Im schlimmsten Fall ist diese Anzahl~$n$,
  nämlich wenn $v_{i} < v_{i+1}$ für alle $i \in \setn{n-1}$ gilt.
  Anstatt nur solche Extremfälle zu betrachten,
  führen wir nun eine \sog Average\-/Case\-/Analyse.
  Dabei nehmen wir ein, dass die Eingabe zufällig ist,
  und berechnen den EW der Anzahl der Operationen~$H$.
  \par
  Wir verwenden das Modell $(\eli{X}{n}) \distr \Unif(\setn{n}_{\neq}^{n})$,
  wobei $X_{k}$ für die im Schritt~$k$ präsentierte Bewertung steht für alle $k \in \setn{n}$.
  Es sei $Y_{k}$ die IZV für das Event $\forall i \leq k \holds X_{i} \leq X_{k}$.
  Dann ist die gesuchte Größe $\expect{\sum_{k=1}^{n} Y_{k}}$.
  Wegen der Linearität des EW genügt es, $\expect{Y_{k}}$ für alle $k$ zu berechnen.
  Es sei $k \in \setn{n}$.
  Ein saloppes Argument geht so:
  Jede der Bewertungen $\eli{X}{k}$ hat dieselbe W'keit, also $\frac{1}{k}$,
  die bislang beste zu sein; also gilt $\expect{Y_{k}} = \prob{Y_{k}=1} = \frac{1}{k}$.
  Wenn das stimmt, so folgt, wobei $c \in \RR$ unabhängig von $n$ ist:
  \begin{IEEEeqnarray*}{0l}
    \expect{\sum_{k=1}^{n} Y_{k}}
    = \sum_{k=1}^{n} \expect{Y_{k}}
    = \sum_{k=1}^{n} \frac{1}{k}
    \leq \ln(n) + c
  \end{IEEEeqnarray*}
  Wir geben ein rigoroses Argument, warum tatsächlich $\expect{Y_{k}} = \frac{1}{k}$ gilt.
  Es gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{Y_{k}=1}
    = \frac{\card{\set{v \in \setn{n}_{\neq}^{n} \suchthat \forall i \leq k \holds v_{i} \leq v_{k}}}}{n!} \\
    \eqin = \frac{\sum_{t=k}^{n} (t-1)_{k-1} (n-k)!}{n!}
    = \frac{\sum_{t=k}^{n} (t-1)_{k-1}}{(n)_{k}}
  \end{IEEEeqnarray*}
  Wir zeigen per Induktion nach $n$:
  \begin{IEEEeqnarray*}{0l}
    \sum_{t=k}^{n} (t-1)_{k-1} = \frac{(n)_{k}}{k}
  \end{IEEEeqnarray*}
  \begin{description}[font=\it]
  \item[Induktionsanfang $n=k$.]
    Für die linke Seite gilt:
    \begin{IEEEeqnarray*}{0l}
      \sum_{t=k}^{n} (t-1)_{k-1}
      = \sum_{t=k}^{k} (t-1)_{k-1}
      = (k-1)_{k-1}
      = (k-1)!
    \end{IEEEeqnarray*}
    Für die rechte Seite gilt:
    \begin{IEEEeqnarray*}{0l}
      \frac{(n)_{k}}{k} = \frac{(k)_{k}}{k} = (k-1)!
    \end{IEEEeqnarray*}
  \item[Induktionsschritt.]
    Es sei $n \geq k$ und es gelte bereits $\sum_{t=k}^{n} (t-1)_{k-1} = \frac{(n)_{k}}{k}$.
    Dann gilt:
    \begin{IEEEeqnarray*}{0l}
      \sum_{t=k}^{n+1} (t-1)_{k-1}
      = (n)_{k-1} + \sum_{t=k}^{n} (t-1)_{k-1}
      = (n)_{k-1} + \frac{(n)_{k}}{k} \\
      \eqin = \frac{k (n)_{k-1} + (n)_{k}}{k}
      = \frac{k (n)_{k-1} + (n)_{k}}{k (n+1)_{k}} (n+1)_{k} \\
      \eqin = \frac{k (n)_{k-1} + (n)_{k}}{k (n+1) (n)_{k-1}} (n+1)_{k}
      = \frac{k + n-k+1}{k (n+1)} (n+1)_{k}
      = \frac{1}{k} (n+1)_{k}
    \end{IEEEeqnarray*}
    Das schließt den Induktionsbeweis.
  \end{description}
\end{example}

\begin{satz}[Dreieckungleichung]
  Es seien $X,Y \in \ZV$ diskret mit EW.
  Dann besitzt $\abs{X}$ einen EW, und es gilt $\abs{\expect{X}} \leq \expect{\abs{X}}$.
\end{satz}

\begin{proof}
  Einfache Übung.
\end{proof}

Schließlich bringen wir die Unabhängigkeit ins Spiel:

\begin{satz}[Produkt unabhängiger ZV]
  Es sei $(\eli{X}{n}) \in \ZV^{n}$ unabhängig,
  und $X_{i}$ sei diskret mit EW für alle $i \in \setn{n}$.
  Dann besitzt $\prod_{i=1}^n X_i$ einen EW, und es gilt:
  \begin{IEEEeqnarray*}{0l}
    \expect{\prod_{i=1}^n X_i} = \prod_{i=1}^n \expect{X_i}
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}[nur endlicher Träger]
  Es genügt, die Aussage für zwei ZV zu zeigen,
  weil die allgemeine Version durch triviale Induktion daraus folgt.
  Es seien also $X,Y \in \ZV$ diskret mit EW und,
  wieder der Einfachheit halber, mit endlichem Träger.
  Es möge $(X,Y)$ unabhängig sein.
  Es gilt:
  \begin{IEEEeqnarray*}{0l+x*}
    \expect{XY}
    = \sum_{w \in \RR} w \cdot \prob{XY=w} \\
    \eqin = \sum_{w \in \RR} \sum_{u \in \RR} \sum_{v \in \RR}
    w \cdot \prob{XY=w \land X=u \land Y=v} \\
    \eqin = \sum_{u \in \RR} \sum_{v \in \RR} \sum_{w \in \RR}
    w \cdot \prob{XY=w \land X=u \land Y=v} \\
    \eqin = \sum_{u \in \RR} \sum_{v \in \RR} uv \cdot \prob{X=u \land Y=v} \\
    \eqin = \sum_{u \in \RR} \sum_{v \in \RR} uv \cdot \prob{X=u} \cdot \prob{Y=v} \\
    \eqin = \sum_{u \in \RR} u \cdot \prob{X=u} \cdot \sum_{v \in \RR} v \cdot \prob{Y=v}
    = \expect{X} \cdot \expect{Y} & \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\section{Varianz}

\begin{para}[Definition]
  Es sei $X \in \ZV$ diskret mit EW.
  Dann ist $(X - \expect{X})^{2} \in \ZV$.
  Wenn diese ZV einen EW besitzt, dann nennen wir diesen die \term{Varianz} von $X$,
  Bezeichnung $\var{X}$, und sagen, dass $X$ eine Varianz besitzt oder dass $X$ mit Varianz ist.
  \Ggf definieren wir also:
  \begin{IEEEeqnarray*}{0l}
    \var{X} \df \expect{(X - \expect{X})^{2}}
  \end{IEEEeqnarray*}
  Wenn $X$ eine Varianz besitzt, dann definieren wir die \term{Standardabweichung} von $X$
  als $\sig(X) \df \sqrt{\var{X}}$.
\end{para}

\begin{satz}[Skalierung und Verschiebung]
  Es sei $X \in \ZV$ diskret mit Varianz und $\lam, a \in \RR$.
  Dann gilt $\var{\lam X + a} = \lam^2 \ccdot \var{X}$.
\end{satz}

\begin{proof}
  Folgt leicht aus der Linearität des EW (Übung).
\end{proof}

\begin{satz}[Quadrat innen -- Quadrat außen]
  Es sei $X \in \ZV$ diskret mit Varianz.
  Dann gilt $\var{X} = \expect{X^2} - \expect{X}^2$.
\end{satz}

\begin{proof}
  Es gilt:
  \begin{IEEEeqnarray*}{0l"s+x*}
    \expect{\parens{X-\expect{X}}^2} \\
    \eqin = \expect{X^2 - 2 \cdot X \cdot \expect{X} + \expect{X}^2}
    & binomische Formel \\
    \eqin = \expect{X^2} - \expect{2 \cdot X \cdot \expect{X}} + \expect{\expect{X}^2}
    & Linearität \\
    \eqin = \expect{X^2} - 2 \cdot \expect{X} \cdot \expect{X} + \expect{\expect{X}^2}
    & Linearität \\
    \eqin = \expect{X^2} - 2 \cdot \expect{X}^2 + \expect{X}^2
    & $\expect{X}^2$ ist konstante ZV \\
    \eqin = \expect{X^2} - \expect{X}^2 && \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\begin{para}[Varianz einer Verteilung]
  Offenbar hängt die Varianz einer ZV nur von deren Verteilung ab,
  daher macht der Begriff der Varianz einer Verteilung Sinn.
  Wir bezeichnen die Varianz einer Verteilung $D$ mit $\var{}_{D}$.
  Zum Beispiel gilt nach obigem Satz $\var{}_{\Ber(p)} = p - p^{2} = p(1-p)$.
\end{para}

\begin{satz}[Summe unabhängiger ZV]
  Es sei $(\eli{X}{n}) \in \ZV^{n}$ unabhängig,
  und $X_{i}$ sei diskret mit Varianz für alle $i \in \setn{n}$.
  Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    \var{\sum_{i=1}^n X_i} = \sum_{i=1}^n \var{X_i}
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}
  Es gilt:
  \begin{IEEEeqnarray*}{0l+x*}
    \var{\sum_{i=1}^n X_i}
    = \expect{\parens{\sum_{i=1}^n X_i}^{2}} - \expect{\sum_{i=1}^n X_i}^{2} \\
    \eqin = \expect{\sum_{i=1}^n X_i^{2} + 2 \sum_{\substack{i,j\in\setn{n}\\i < j}} X_i X_{j}}
    - \parens{\sum_{i=1}^n \expect{X_i}}^{2} \\
    \eqin = \sum_{i=1}^n \expect{X_i^{2}}
    + 2 \sum_{\substack{i,j\in\setn{n}\\i < j}} \expect{X_i X_{j}}
    - \sum_{i=1}^n \expect{X_i}^{2}- 2 \sum_{\substack{i,j\in\setn{n}\\i < j}}
    \underbrace{\expect{X_i} \expect{X_{j}}}_{=\expect{X_i X_{j}}} \\
    \eqin = \sum_{i=1}^n \expect{X_i^{2}} - \expect{X_i}^{2}
    = \sum_{i=1}^n \var{X_i} & \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\begin{satz}[Varianz der Binomialverteilung]
  Es gilt $\var{}_{\Bin(n,p)} = np(1-p)$.
\end{satz}

\begin{proof}
  Betrachte wieder eine Summe unabhängiger $\Ber(p)$\=/ZV
  und wende den obigen Satz an.
\end{proof}

\section{Konzentration}

Besonders interessant sind EW und Varianz,
weil man über diese Größen einfache Abschätzungen für die W'keit geben kann,
dass eine ZV in einen gewissen Bereich fällt.

\begin{satz}[Markow\protect\footnote{%
    \foreignlanguage{russian}{Андрей Андреевич Марков}, russischer Mathematiker, 1856--1922}%
  -Ungleichung]
  Es sei $X \in \ZV$ diskret mit EW.
  Es gelte $\prob{X \geq 0} = 1$, und es sei $a > 0$.
  Dann gilt $\prob{X \geq a} \leq \frac{\expect{X}}{a}$.
\end{satz}

%%% FIXME: alternativer Beweis:
%%% E[X]=E[ X*1_{X < a} + X*1_{X\geq a} ]\geq E[X*1_{X\geq a} ] \geq P[a*1_{X\geq a} ] = a*P[ X\geq a ].

\begin{proof}[Markow-Ungleichung]
  Definiere $I \df \chi_{\event{X \geq a}}$,
  dann gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{I \leq \frac{X}{a}} = \prob{a I \leq X} = \prob{0 \leq X} = 1
  \end{IEEEeqnarray*}
  Es folgt mit der Monotonie des EW:
  \begin{IEEEeqnarray*}{0l+x*}
    \prob{X \geq a}
    = 0 \cdot \prob{X < a} + 1 \cdot \prob{X \geq a} \\
    \eqin = 0 \cdot \prob{I=0} + 1 \cdot \prob{I=1}
    = \expect{I}
    \leq \expect{\frac{X}{a}}
    = \frac{\expect{X}}{a} & \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\begin{satz}[Tschebyscheff\protect\footnote{%
    \foreignlanguage{russian}{Пафнутий Львович Чебышёв}, russischer Mathematiker, 1821--1894}%
  -Ungleichung]
  Es sei $X \in \ZV$ diskret mit Varianz und $a > 0$.
  Dann gelten:
  \begin{IEEEeqnarray*}{0l}
    \prob{\abs{X - \expect{X}} \geq a} \leq \frac{\var{X}}{a^2} \\
    \prob{X - \expect{X} \geq a} \leq \frac{\var{X}}{\var{X} + a^2}
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}
  Es ist $(X - \expect{X})^{2}$ eine ZV wie in der Markov\-/Ungleichung.
  Damit gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{\abs{X - \expect{X}} \geq a}
    = \prob{(X - \expect{X})^{2} \geq a^{2}} \\
    \eqin \leq \frac{\expect{(X - \expect{X})^{2}}}{a^{2}}
    = \frac{\var{X}}{a^{2}}
  \end{IEEEeqnarray*}
  Um die zweite Aussage einzusehen, bemerke für alle $b \geq 0$:
  \begin{IEEEeqnarray*}{0l}
    \prob{X - \expect{X} \geq a}
    = \prob{X - \expect{X} + b \geq a + b} \\
    \eqin \leq \prob{(X - \expect{X} + b)^{2} \geq (a + b)^{2}} \\
    \eqin \leq \frac{\expect{(X - \expect{X} + b)^{2}}}{(a + b)^{2}}
    = \frac{\var{X} + b^{2}}{(a + b)^{2}}
  \end{IEEEeqnarray*}
  Insbesondere gilt dies für $b = \frac{\var{X}}{a}$, also:
  \begin{IEEEeqnarray*}{0l+x*}
    \prob{X - \expect{X} \geq a}
    \leq \frac{\var{X} + \frac{\var{X}^{2}}{a^{2}}}{(a + \frac{\var{X}}{a})^{2}} \\
    \eqin = \frac{a^{2} \ccdot \var{X} + \var{X}^{2}}{(a^{2} + \var{X})^{2}}
    = \frac{\var{X}}{a^{2} + \var{X}} & \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\begin{para}[Vielfache der Standardabweichung]
  Vielleicht etwas einprägsamer lässt sich die erste Aussage aus diesem Satz
  schreiben mit $\mu \df \expect{X}$ und ${\sigma \df \std{X}}$:
  \begin{IEEEeqnarray*}{0l}
    \prob{\mu - k \sigma < X < \mu + k \sigma} = \prob{\abs{X-\mu} < k \sigma} \geq 1 - \frac{1}{k^2}
  \end{IEEEeqnarray*}
  Also die W'keit, dass die ZV um weniger als $k$ Standardabweichungen vom EW abweicht,
  ist mindestens $1 - \frac{1}{k^2}$.
  Eine nützliche Aussage ergibt sich natürlich erst für $k \geq 2$.
  Für $k=2$ hat man auf der rechten Seite $0.75$, und für $k=3$ hat man dort mindestens $0.88$.
  Wir wenden das in dem nächsten Beispiel an.
\end{para}

\begin{example}
  Bei einem Glücksspiel (frei nach einem Beispiel in~\cite[Kapitel~38.1]{ArensEtAl15})
  gibt es $n \in \NNone$ Spieler und die Bank.
  Jeder Spieler zahlt eine Einheit (zum Beispiel einen Euro) in die Bank.
  Danach wirft jeder Spieler \xmal{3} eine $\Ber(\frac{1}{2})$\=/Münze und zählt,
  wie oft \enquote{Kopf} gezeigt wird.
  Die Auszahlung an den Spieler richtet sich nach der Anzahl der Köpfe, die dieser Spieler wirft
  (die genaue Beziehung wird unten über die Funktion~$f$ festgelegt).
  Für alle $i \in \setn{n}$ stehe $\tiX_i$ für die Anzahl der von Spieler $i$ geworfenen Köpfe.
  Wir modellieren $(\eli{\tiX}{n}) \distr \Bin(3,\frac{1}{2})^{n}$ \iid
  Die folgende Funktion $f$ stellt die Beziehung zwischen geworfenen Köpfen und der Auszahlung her,
  es ist $f(k)$ die Auszahlung bei $k$ Köpfen:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{f}{\setft{0}{3}}{\NN}{k \mapsto
      \begin{cases}
        0 & k \in \set{0,1} \\
        1 & k = 2 \\
        5 & k = 4
      \end{cases}}
  \end{IEEEeqnarray*}
  Dann ist $X_{i} \df f \circ \tiX_{i}$ eine ZV, welche der Auszahlung an Spieler~$i$ entspricht,
  und $(\eli{X}{n})$ ist unabhängig.
  Wir fassen die PMF von $\tiX_{i}$ und von $X_{i}$ zusammen:
  \inlinerow{\input{05-auszahlungen-1.tex}}{}\par
  Es gilt:
  \begin{IEEEeqnarray*}{0l}
    {\expect{X_i} = 1 \cdot \frac{3}{8} + 5 \cdot \frac{1}{8} = 1}
  \end{IEEEeqnarray*}
  Ferner ist $Y \df n - \sum_{i=1}^n X_i$ der Gewinn der Bank.
  Es gilt:
  \begin{IEEEeqnarray*}{0l}
    \expect{Y}
    = n - \sum_{i=1}^n \expect{X_i}
    = n - \sum_{i=1}^n 1 = n - n \cdot 1 = 0
  \end{IEEEeqnarray*}
  Für die Varianzen berechnen wir zunächst mit LOTUS:
  \begin{IEEEeqnarray*}{0l}
    \expect{X_i^2} = 1^2 \cdot \frac{3}{8} + 5^2 \cdot \frac{1}{8} = 3.5
  \end{IEEEeqnarray*}
  Daraus folgt $\var{X_i} = 3.5 - 1^2 = 2.5$ und:
  \begin{IEEEeqnarray*}{0l}
    \var{Y}
    = \var{n + \parens{- \sum_{i=1}^n X_i}} \\
    \eqin = (-1)^2 \cdot \var{\sum_{i=1}^n X_i}
    = \sum_{i=1}^n \var{X_i} = 2.5 n
  \end{IEEEeqnarray*}
  Es folgt $\sigma(Y) = \sqrt{2.5 n}$.
  Entsprechend der Tschebyscheff\-/Ungleichung tragen wir in der folgenden Tabelle
  Prognosen und andere Kennwerte für einige konkrete Werte von $n$ zusammen.
  Die obere Zeile gibt die verschiedenen Werte für~$n$ an.
  Die zweite Zeile gibt den maximalen Verlust der Bank an, der $4n$ ist,
  denn im, aus der Sicht der Bank, schlimmsten Fall zahlt die Bank an jeden Spieler $5$ Einheiten aus,
  und zuvor nimmt sie von jedem Spieler $1$ Einheit ein.
  Die dritte Zeile gibt den maximalen Gewinn der Bank, der~$n$ ist.
  \par
  Die beiden letzten Zeilen in der Tabelle geben (auf $1$ Stelle nach dem Dezimalpunkt gerundet)
  $2$ \bzw $3$ Standardabweichungen an.
  Wir wissen nach der Tschebyscheff\-/Ungleichung,
  dass $\abs{Y - \expect{Y}} = \abs{Y}$ mit W'keit mindestens $0.75$ \bzw $0.88$ unterhalb dieser Werte liegt.\par
  \input{05-prognosen-1.tex}\par
  Im Vergleich zur Betrachtung lediglich der Extremwerte
  werden die auf der Tschebyscheff\-/Ungleichung basierenden Prognosen
  mit steigendem $n$ immer besser.
  Für $n=30$ Spieler etwa können wir durch Betrachten von maximalem Verlust und maximalem Gewinn
  nur ${\prob{-120 \leq Y \leq 30} = 1}$ erhalten,
  jedoch wissen wir aus der Tschebyscheff\-/Ungleichung,
  dass ${\prob{-17.3 < Y < 17.3} \geq 0.75}$ gilt.
  (Letzteres stimmt nicht ganz, da $17.3$ ein gerundeter Wert ist.
  Korrekt ist aber $\prob{-17.4 < Y < 17.4} \geq 0.75$.)
\end{example}

\begin{para}[Simulationen]
  Die Tschebyscheff\-/Ungleichung erklärt, warum gewisse Simulationen funktionieren.
  Für alle $u \in \RR^{n}$ notiere den \term{Mittelwert} von $u$ als
  ${\mean{u} \df \frac{1}{n} \sum_{i=1}^{n} u_{i}}$.
  Es sei $X \in \ZV$ diskret mit positiver Varianz.
  Dann gibt es für alle $n \in \NNone$ einen W'raum und darüber $\zvtup{X} = (\eli{X}{n})$ unabhängig so,
  dass $X_{i}$ dieselbe Verteilung hat wie $X$, für alle $i \in \setn{n}$.
  Man nennt die $\eli{X}{n}$ \term{unabhängige Kopien} von $X$.
  Es gilt:
  \begin{IEEEeqnarray*}{0l}
    \expect{\mean{\zvtup{X}}}
    = \frac{1}{n} \sum_{i=1}^{n} \expect{X_{i}}
    = \expect{X} \\
    \var{\mean{\zvtup{X}}}
    = \frac{1}{n^{2}} \sum_{i=1}^{n} \var{X_{i}}
    = \frac{\var{X}}{n}
  \end{IEEEeqnarray*}
  Obwohl sich der EW nicht verändert, reduzieren wir die Varianz.
  \par
  Es möge nun ein Experiment, dessen Ergebnis durch $X$ gut modelliert ist,
  auf dem Computer ausführbar sein.
  Dann führen wir das Experiment auf dem Computer \xmal{n} aus,
  mitteln über die $n$ Ergebnisse.
  Wenn $n$ groß genug ist, dann wird dieser Mittelwert mit \enquote{hoher W'keit} nahe $\expect{X}$ liegen.
  Diesen Mittelwert können wir also in einem gewissen Sinne als Schätzung für $\expect{X}$ verwenden.
  Wenn $X$ eine IZV für ein Event $A$ ist, dann erhalten wir so eine Schätzung für $\prob{A}$.
  Etwas genauer haben wir:
  \par
  Wir geben $\veps, \del > 0$ vor und möchten $n$ so bestimmen, dass gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{\abs{\mean{\zvtup{X}} - \expect{X}} \geq \veps} \leq \del
  \end{IEEEeqnarray*}
  Es gilt:
  \begin{IEEEeqnarray*}{0l}
    \frac{\var{\mean{\zvtup{X}}}}{\veps^{2}} \leq \del
    \iff \frac{\var{{X}}}{n \veps^{2}} \leq \del
    \iff \frac{\var{{X}}}{\veps^{2} \del} \leq n
  \end{IEEEeqnarray*}
  Damit wissen wir, welche Wahl von $n$ jedenfalls funktioniert.
  \par
  Als Beispiel betrachte $X \distr \Ber(p)$ mit unbekanntem $p$.
  Eine solche Verteilung liegt insbesondere vor, wenn $X$ eine IZV ist.
  Es gilt $\var{X} = p(1-p) \leq \frac{1}{4}$,
  wie man durch ein bisschen Analysis sieht.
  Eine hinreichende Bedingung für $n$ ist also $\frac{1}{4 \veps^{2} \del} \leq n$.
\end{para}

Für ZV bestimmter Bauart gibt es Resultate mit ähnlicher Intention wie die Tschebyscheff\-/Ungleichung,
die aber bessere Abschätzungen liefern können.
Wir stellen hier eines vor; weitere sind in der Literatur zu finden.

\begin{satz}[Chernoff-Ungleichung]
  Es seien $p \in \intcc{0}{1}^{n}$ und ${(\eli{X}{n}) \in \ZV^{n}}$ unabhängig so,
  dass $X_{i} \distr \Ber(p_{i})$ für alle $i \in \setn{n}$ gilt.
  Es seien $w \in \intcc{0}{1}^{n}$, und definiere $\psi \df \sum_{i=1}^{n} w_{i} X_{i}$.
  Dann gilt für alle $\bet \geq 0$:
  \begin{IEEEeqnarray*}{0l}
    \prob{\psi \geq (1+\bet) \expect{\psi}}
    \leq \p{\frac{e^{\bet}}{(1+\bet)^{(1+\bet)}}}^{\expect{\psi}}
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}
  Wir brauchen zwei Fakten über die Exponentialfunktion,
  die sich leicht mit etwas Analysis beweisen lassen:
  \begin{itemize}
  \item $\forall y \geq 0 \holds 1+y \leq e^{y}$
  \item $\forall y \geq 0 \innerholds \forall u \in \intcc{0}{1} \holds (1+y)^{u} \leq 1+uy$
  \end{itemize}
  Für alle $t > 0$ gilt:
  \begin{IEEEeqnarray*}{0l"s}
    \prob{\psi \geq (1+\bet) \expect{\psi}} \\
    \eqin = \prob{e^{t \psi} \geq e^{t (1+\bet) \expect{\psi}}} \\
    \eqin \leq \frac{\expect{e^{t \psi}}}{e^{t (1+\bet) \expect{\psi}}}
    & Markow\-/Ungleichung \\
    \eqin = \frac{\expect{\prod_{i=1}^{n}e^{t w_{i} X_{i}}}}{e^{t (1+\bet) \expect{\psi}}}
    & Funktionalgleichung \\
    \eqin = \frac{\prod_{i=1}^{n}\expect{e^{t w_{i} X_{i}}}}{e^{t (1+\bet) \expect{\psi}}}
    & Unabhängigkeit
  \end{IEEEeqnarray*}
  Daraus folgt für alle $i \in \setn{n}$ mit LOTUS:
  \begin{IEEEeqnarray*}{0l}
    \expect{e^{t w_{i} X_{i}}}
    = \sum_{u \in \OI} e^{t w_{i} u} \cdot \prob{X_{i}=u} \\
    \eqin = e^{t w_{i}} p_{i} + (1-p_{i})
    = 1 + p_{i} (e^{t w_{i}} - 1)
    \leq e^{ p_{i} (e^{t w_{i}} - 1) }
  \end{IEEEeqnarray*}
  Dies werten wir aus für $t \df \ln(1+\bet) > 0$, womit wir erhalten:
  \begin{IEEEeqnarray*}{0l}
    \expect{(1+\bet)^{w_{i} X_{i}}}
    \leq e^{ p_{i} ((1+\bet)^{w_{i}} - 1) }
    \leq e^{ p_{i} (1+ \bet w_{i} - 1) }
    \leq e^{ p_{i} \bet w_{i} }
  \end{IEEEeqnarray*}
  Dies setzen wir in unsere erste Rechnung ein und erhalten:
  \begin{IEEEeqnarray*}{0l}
    \prob{\psi \geq (1+\bet) \expect{\psi}}
    \leq \frac{\prod_{i=1}^{n}\expect{e^{t w_{i} X_{i}}}}{e^{t (1+\bet) \expect{\psi}}} \\
    \eqin \leq \frac{\prod_{i=1}^{n} e^{ p_{i} \bet w_{i} } }{(1+\bet)^{(1+\bet) \expect{\psi}}}
    = \frac{ e^{ \bet \sum_{i=1}^{n} p_{i} w_{i} } }{(1+\bet)^{(1+\bet) \expect{\psi}}}
    = \frac{ e^{ \bet \expect{\psi} } }{(1+\bet)^{(1+\bet) \expect{\psi}}}
  \end{IEEEeqnarray*}
  Letzteres gilt, da $\expect{\psi}
  = \sum_{i=1}^{n} w_{i} \expect{X_{i}}
  = \sum_{i=1}^{n} w_{i} p_{i}$ gilt.
\end{proof}

\begin{para}[Vergleich mit Tschebyscheff]
  Nach Tschebyscheff gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{\psi \geq (1+\bet) \expect{\psi}}
    = \prob{\psi - \expect{\psi} \geq \bet \ccdot \expect{\psi} }
    \leq \frac{\var{\psi}}{\var{\psi} + (\bet \ccdot \expect{\psi})^{2}}
  \end{IEEEeqnarray*}
  Wir schreiben EW und Varianz von $\psi$ aus:
  \begin{IEEEeqnarray*}{0l+l}
    \expect{\psi} = \sum_{i=1}^{n} w_{i} p_{i} &
    \var{\psi} = \sum_{i=1}^{n} w_{i} p_{i} (1-p_{i})
  \end{IEEEeqnarray*}
  Für einen numerischen Vergleich fixiere $w_{i}\df1$ und $p_{i}\df\frac{1}{2}$ für alle $i \in \setn{n}$,
  also $\psi \distr \Bin(n,\frac{1}{2})$.
  Dann gilt $\expect{\psi} = \frac{n}{2}$ und $\var{\psi} = \frac{n}{4}$,
  also sind die rechten Seiten von Tschebyscheff und Chernoff:
  \begin{IEEEeqnarray*}{0l+l}
    \frac{\frac{n}{4}}{\frac{n}{4} + (\bet \ccdot \frac{n}{2})^{2}}
    = \frac{1}{1 + \bet^{2} \ccdot n} &
    \p{\frac{e^{\bet}}{(1+\bet)^{(1+\bet)}}}^{\frac{n}{2}}
  \end{IEEEeqnarray*}
  In den folgenden Plots lassen wir $\bet$ von $0.1$ bis $1.0$ laufen;
  links ist $n=10$ und rechts ist $n=1000$;
  die Schranke von Tschebyscheff ist in Blau dargestellt
  und die von Chernoff in Schwarz.
  \inlinerow{\input{plot-vergleich-10}}{\input{plot-vergleich-1000}}
\end{para}
