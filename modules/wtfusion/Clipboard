    \begin{description}[font=\it]
    \item[Additivität':] $\displaystyle
      \forall (\eli{A}{k}) \in \SIG^{k} \with \text{paarweise disjunkt} \holds
      \prob{\dotbigcup_{i=1}^{k} A_{i}} = \sum_{i=1}^{k} \prob{A_{i}}$
    \end{description}

    Notiere $\borel{} \df \borel{1}$ für die Menge der \dimensionalen{1} Borelmengen.

  Zum Beispiel mit $b \in \RR$:
  \begin{IEEEeqnarray*}{0l}
    \prob{X \leq b} = \prob{\set{\om \in \Om \suchthat X(\om) \leq b}}
  \end{IEEEeqnarray*}

  für diese Menge ein, nämlich $\event{X \in A}$.


  Ebenso $\event{X \leq u} = \fnpre{X}(\intic{u})$ für alle $u \in \RR$.
  Allgemeiner, wenn wir ein Tupel $\zvtup{X} = (\eli{X}{n}) \in \ZV^{n}$ von ZV haben,
  so schreiben wir für alle $A \in \borel{n}$ und alle $u \in \RR^{n}$:
  \begin{IEEEeqnarray*}{0l}
    \event{\zvtup{X} \in A} = \fnpre{\zvtup{X}}(A)
    = \setst{\om \in \Om \suchthat \zvtup{X}(\om) \in A}
    = \setst{\om \in \Om \suchthat (X_{1}(\om), \hdots, X_{n}(\om)) \in A}
  \end{IEEEeqnarray*}

  Wir verwenden diese Schreibweise auch mit anderen logischen Formeln und für mehrere ZV,
  was wir hier kurz vorstellen möchten.

  Für jeden Ausdruck $\vphi$ und für alle $\om \in \Om$ bezeichnen wir mit $\vphi(\om)$ den Ausdruck,
  der aus $\vphi$ entsteht,
  indem wir jede in $\vphi$ vorkommende ZV $X$ durch $X(\om)$ ersetzen, was ja eine reelle Zahl ist.
  Es sei nun $\vphi$ so,
  dass $\vphi(\om)$ für alle $\om \in \Om$ eine Aussage ist,
  d.h. wahr oder falsch ist.
  Definiere:
  \begin{IEEEeqnarray*}{0l}
    \event{\vphi} \df \set{\om \in \Om \suchthat \vphi(\om)}
  \end{IEEEeqnarray*}
  Hier sammeln wir also alle Ergebnisse ein, die $\vphi$ zu einer wahren Aussage machen.
  Wir nennen $\vphi$ ein \term{Event}, wenn

  Wir nennen eine logische Formel $\vphi$ ein \term{Event},
  wenn


\begin{satz}[Tupel von ZV]
  \label{wtinf/cha01:n-zv}
  Es sei $\zvtup{X} = (\eli{X}{n}) \in \ZV^{n}$.
  Dann gilt:
  \begin{IEEEeqnarray}{0l}
    \label{wtinf/cha01:n-pre}
    \forall A \in \borel{n} \holds \event{\zvtup{X} \in A} \in \SIG
  \end{IEEEeqnarray}
\end{satz}

\begin{wtinf}
  \begin{proof}
    Für alle $u \in \RR^{n}$ gilt:
    \begin{IEEEeqnarray*}{0l}
      \event{\zvtup{X} \leq u}
      = \bigcap_{i=1}^{n} \underbrace{\event{X_{i} \leq u_{i}}}_{\in \SIG}
      \in \SIG
    \end{IEEEeqnarray*}
    Die Behauptung folgt mit einem ähnlichen Argument wie im Beweis von \autoref{wtfusion/cha01:krit-ZV},
    da $\borelgen{n} \subseteq \set{A \in \RR^{n} \suchthat \event{\zvtup{X} \in A} \in \SIG}$ gilt.
  \end{proof}
\end{wtinf}

\begin{para}[Gegenseitiger Ausschluss]
  Es sei $(\Om, \SIG)$ ein Eventraum.
  Für alle $A,B \in \SIG$ sagen wir zu $A \cap B = \emptyset$,
  dass sich $A$ und $B$ \term{gegenseitig ausschließen}.
  Es sei $\seqone{A_{i}}{i} \in \seqset{\SIG}$,
  also $\seqone{A_{i}}{i}$ ist eine Folge von Events.
  Wir nennen $\seqone{A_{i}}{i}$ \term{gegenseitig ausschließend},
  wenn $A_{i} \cap A_{j} = \emptyset$ gilt für alle $i,j \geq 1$ mit $i \neq j$.
  Wenn $\seqone{A_{i}}{i}$ gegenseitig ausschließend ist,
  dann schreiben wir oft $\dotbigcupone{i} A_{i}$ anstelle von $\bigcupone{i} A_{i}$,
  um an den gegenseitigen Ausschluss zu erinnern.
  Entsprechendes definieren wir für Tupel $(\eli{A}{n}) \in \SIG^{n}$.
\end{para}

\begin{exercise}
  Es sei $p \in \intcc{0}{1}$
  und $(\eli{X}{n}) \in \ZV^{n}$ unabhängig so, dass $X_{i} \distr \Ber(p)$ für alle $i \in \setn{n}$ gilt.
  (So etwas gibt es laut \autoref{wtfusion/cha01:diskret-exists}.)
  Definiere $S \df \sum_{i=1}^{n} X_{i}$.
  Geben Sie eine Formel für $\prob{S=u}$ für alle $u \in \RR$ an.
\end{exercise}


  \begin{enumerate}
  \item Es sei $X \in \ZV$ mit diskreter Verteilung.
    Es sei $U \in \borel{}$.
    Dann gilt:
    \begin{IEEEeqnarray*}{0l}
      \prob{X \in U}
      = \sum_{u \in U \cap \supp(X)} \rho_{X}(u)
      = \sum_{u \in U \cap \supp(X)} \prob{X=u}
    (Der vorige Punkt ist offenbar der Spezialfall $n=1$ hiervon.)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{satz}[Kriterium für diskrete Verteilung]
  Es sei $\zvtup{X} = (\eli{X}{n}) \in \ZV^{n}$ so,
  dass es eine abzählbare Menge $U \in \borel{n}$ so gibt,
  dass $\prob{\zvtup{X} \in U} = 1$ gilt.
\end{satz}

\begin{proof}
  Folgt leicht aus \autoref{wtfusion/cha01:wkeit}.
  \begin{wtinf}
    (Übung.)
  \end{proof}
\end{wtpharma}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  Ebenso bezeichnen wir jede Formel~$\vphi$,
  für die es $A \in \borel{n}$ so gibt, dass $\event{\vphi} = \event{\zvtup{X} \in A}$ gilt.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    Wir gehen sparsam vor, in dem Sinne, dass wir hier möglichst kein Merkmal betrachten,
    das schon durch die anderen Merkmale festliegt.
    Wenn wir zum Beispiel ein Experiment mit $k$ Teilexperimenten haben,
    die jeweils Erfolg oder Misserfolg liefern, dann gibt es zwei naheliegende Möglichkeiten:
    \begin{itemize}
    \item Betrachte $k$ Merkmale,
      nämlich pro Teilexperiment das Merkmal \enquote{Erfolg oder Misserfolg},
      durch $0$ \bzw $1$ ausgedrückt.
      Die Anzahl der Erfolge
    \item Betrachte nur die Anzahl Erfolge, als Zahl in $\setft{0}{k}$ ausgedrückt.
    \end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{para}[Diskussion der $\LR$]
  Wenn $\LR(B,A) > 1$ gilt, dann gilt $\odds{A \cond B} > \odds{A}$ nach obigem Satz;
  man sagt auch, die Beobachtung $B$ spricht für $A$.
  Wenn ${\LR(B,A) < 1}$, dann spricht $B$ gegen $A$.
  Wir verdeutlichen die Formel von Bayes an einem Beispiel~\cite{OHagan94}:
  Sie besuchen Ihre Freunde zum ersten Mal in ihrem neuen Haus.
  Dort schauen Sie aus dem Fenster und machen ein Objekt aus mit einem Stamm, Ästen und Blättern.
  Sie schließen sofort, dass (höchstwahrscheinlich) vor dem Fenster ein Baum steht.
  Wie kommen Sie zu dieser Überzeugung?
  Es sei $A$ das Event, dass vor dem Fenster ein Baum steht;
  und $B$ das Event, dass Sie durch das Fenster ein Objekt beobachten, das Stamm, Äste und Blätter hat.
  Dann ist $\prob{B \cond A}$ nahe~$1$, denn die allermeisten Bäume haben Stamm, Äste und Blätter.
  Es ist ferner $\prob{B \cond A^{c}}$ nahe~$0$,
  denn es gibt kaum andere Objekte, die so aussehen.
  Sagen wir, $\prob{B \cond A} = 0.95$ und $\prob{B \cond A^{c}} = 0.01$.
  Da Bäume, auch in der Nähe von Häusern, nicht selten vorkommen,
  ist $\prob{A}$ nicht zu klein, sagen wir $\prob{A} = 0.1$.
  Es folgt:
  \begin{IEEEeqnarray*}{0l}
    o \df \odds{A \cond B}
    = \odds{A} \cdot \LR(B,A)
    = \frac{0.1}{0.9} \cdot \frac{0.95}{0.01}
    = \frac{0.1}{0.9} \cdot 95
    \approx 10.56
  \end{IEEEeqnarray*}
  Also gilt $\prob{A \cond B} = \frac{o}{o+1} \approx 0.9135$.
  Sie erhalten also einen DOB von circa $93\%$, dass vor dem Fenster ein Baum steht.
  \par
  Es sei noch $A'$ das Event,
  dass vor dem Fenster eine Baum\-/Attrappe aus Papier und Kunststoff steht.
  Man kann dann wieder argumentieren, dass $\LR(B, A')$ hoch ist
  (wenn auch eher kleiner als $\LR(B, A)$).
  Auf der anderen Seite ist $\prob{A'}$ sehr nahe bei~$0$, denn solche Attrappen kommen sehr selten vor.
  Damit ist $\prob{A' \cond B}$ viel kleiner als $\prob{A \cond B}$.
\end{para}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  Es sei $t \in \setft{\max\set{0,\,k-b}}{\min\set{r,k}}$.
  Wir berechnen $\prob{Y = t}$
  und sagen später etwas zu dem komplizierten Bereich, aus dem $t$ stammt.
  Wie viele Ziehungen gibt es, in denen genau $t$ rote Kugeln (und somit genau $k-t$ blaue Kugeln) vorkommen?
  Es gibt $\binom{r}{t}$ Möglichkeiten, die roten Kugeln auszuwählen,
  und dann $\binom{b}{k-t}$ Möglichkeiten, die blauen Kugeln auszuwählen,
  was $\binom{r}{t} \binom{b}{k-t}$ Möglichkeiten insgesamt ergibt.
  Es folgt:
  \begin{IEEEeqnarray*}{0l}
    \prob{Y = t} = \frac{\binom{r}{t} \binom{b}{k-t}}{N}
    = \frac{\binom{r}{t} \binom{b}{k-t}}{\binom{r+b}{k}}
  \end{IEEEeqnarray*}
  Wir zeigen noch $\sum_{t=\max\set{0,\,k-b}}^{\min\set{r,k}} \prob{Y=t} = 1$.
  Damit ist dann die PMF von $Y$ vollständig erschlossen.

  und $\eli{\bet}{N}$ seien die zugehörigen Anzahlen roter Kugeln darin,
  d.h. $\bet_{j}$ ist die Anzahl roter Kugeln in $A_{j}$ für alle $j \in \setn{N}$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{para}[Wann ist Unabhängigkeit sinnvoll?]
  Wir beschreiben einen oft vorkommenden Fall,
  wo ein Modell mit unabhängigen ZV jedenfalls sinnvoll ist:
  \begin{itemize}
  \item In dem Experiment kommen $n$ Teilexperimente vor,
    die zeitlich nacheinander ausgeführt werden und sich zeitlich nicht überlappen.
    Die Teilexperimente brauchen hier nicht das gesamte Experiment auszumachen,
    sondern das Experiment kann auch Schritte außerhalb dieser Teilexperimente enthalten.
  \item Wir modellieren mit ZV $\zvtupdefx{X}{n}$.
    Es steht $X_{i}$ für ein Merkmal, das nach Ausführung des \xten{i} Teilexperimentes feststeht,
    für alle $i \in \setn{n}$.
  \item Die relevanten Bedingungen in der physischen Welt
    für jedes der Teilexperimente stehen vor Beginn der Ausführung des (gesamten) Experimentes fest.
    Diese Bedingungen brauchen nicht bekannt zu sein, nur festzustehen.
  \end{itemize}
  Dann ist es sinnvoll, das Modell so zu wählen, dass $\zvtup{X}$ unabhängig ist.
  Wir geben zwei Beispiele:
  \begin{description}[font=\it]
  \item[Unabhängigkeit sinnvoll.]
    Wir werfen eine Münze \xmal{n}, und es steht vor Beginn des Experimentes fest,
    wie genau diese Münze beschaffen ist.
    Dann ist das Modell $\zvtupiidx{X}{\Ber(p)}{n}$ \iid sinnvoll,
    wobei $p \in \intcc{0}{1}$ die Beschaffenheit der Münze ausdrückt
    und $X_{i}$ für das Ergebnis des \xten{i} Münzwurfes steht für alle $i \in \setn{n}$
    (zum Beispiel steht $0$ für \enquote{Zahl} und $1$ für \enquote{Kopf}, oder umgekehrt).
    Es braucht $p$ nicht bekannt zu sein, aber es muss vor Beginn des Experimentes feststehen.
    Wir werden im nächsten Kapitel ein Beispiel studieren,
    wo die Beschaffenheit der Münze auch vom Zufall abhängt,
    und da sieht die Sache dann anders aus.
  \item[Unabhängigkeit nicht sinnvoll.]
    Ziehen von Kugeln aus einer Urne ohne Zurücklegen:
    Die physischen Bedingungen für den zweiten Zug liegen nicht vor Beginn des Experimentes fest,
    sondern werden durch den ersten Zug mitbestimmt.
    Denn nach dem ersten Zug fehlt eine Kugel in der Urne,
    und es steht vor Beginn des Experimentes nicht fest, welche das sein wird.
  \end{description}
\end{para}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  Wir schreiben $(u,v) \in S$ anstelle von $(u,\eli{v}{n}) \in S$
  und $\rho(u,v)$ anstelle von $\rho(u,\eli{v}{n}) \in S$.

  \begin{IEEEeqnarray*}{0l}
    \sum_{(u,v) \in S} \rho(u,v)
    = \sum_{(u,v) \in S} \rho_{0}(u) \rho_{u}(v)
    = \sum_{\substack{u \in \supp(\rho_{0}) \\ v \in \supp(\rho_{u})}} \rho_{0}(u) \rho_{u}(v)
    = \sum_{u \in \supp(\rho_{0})} \rho_{0}(u) \sum_{v \in \supp(\rho_{u})} \rho_{u}(v)
  \end{IEEEeqnarray*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

CDF nach Kapitel 12 verschieben:

\begin{para}[CDF einer ZV]
  Es sei $X \in \ZV$. Definiere:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{F_{X}}{\RR}{\intcc{0}{1}}{u \mapsto \prob{X \leq u}}
  \end{IEEEeqnarray*}
  Diese Funktion nennen wir die \term{kumulative Verteilungsfunktion} von $X$.
  Als Abkürzung verwenden wir \term{CDF},
  was auf die englische Bezeichnung \enquote{cumulative distribution function} zurückgeht.
\end{para}

\begin{satz}[CDF liefert Verteilung]
  Die Verteilung von $X \in \ZV$ ist bereits durch $F_{X}$ festgelegt.
\end{satz}

\begin{proof}
  Hier nicht; Interessierte mit ausreichend Vorkenntnissen lesen~\cite[Satz (1.12)]{Georgii09}.
  \begin{wtpharma}
    Wir schauen uns nur einen winzigen Teil des Beweises an.
    Nämlich für alle $u \in \RR$ gilt $\prob{X > u} = 1 - \prob{X \leq u} = 1 - F_{X}(u)$.
    So erschließen wir die W'keit von $X > u$ aus $F_{X}$.
  \end{wtpharma}
  \begin{wtinf}
    Als Übung zeige man,
    wie für alle Intervalle $I \subseteq \RR$
    die W'keit $\prob{X \in I}$ aus $F_{X}$ erschlossen werden kann.
  \end{wtinf}
\end{proof}

\begin{para}[CDF mehrerer ZVs]
  Es sei $\zvtup{X} = (\eli{X}{n}) \in \ZV^{n}$. Definiere die \term{CDF} von $\zvtup{X}$:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{F_{\zvtup{X}}}{\RR^{n}}{\intcc{0}{1}}{u \mapsto \prob{\zvtup{X} \leq u}}
  \end{IEEEeqnarray*}
  Das $\leq$ ist bekanntlich komponentenweise gemeint, es gilt also für alle $u \in \RR^{n}$:
  \begin{IEEEeqnarray*}{0l}
    F_{\zvtup{X}}(u) = \prob{\bigwedge_{i=1}^{n} X_{i} \leq u_{i}}
    = \prob{\forall i \in \setn{n} \holds X_{i} \leq u_{i}}
  \end{IEEEeqnarray*}
\end{para}

\begin{satz}[CDF liefert Verteilung]
  Die Verteilung von $\zvtup{X} \in \ZV^{n}$ ist bereits durch $F_{\zvtup{X}}$ festgelegt.
\end{satz}

\begin{proof}
  Interessierte werden wieder auf~\cite[Satz (1.12)]{Georgii09} verwiesen.
\end{proof}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{wtpharma}
  \begin{exercise}
    Definiere:
    \begin{IEEEeqnarray*}{0l}
      \ffnx{F}{\RR}{\tandom}{x \mapsto \frac{\atan(x) + \frac{\pi}{2}}{\pi}}
    \end{IEEEeqnarray*}
    Dabei ist $\atan$ der Arkustangens,
    eine bestimmte reelle Funktion, die zum Beispiel mit dem Taschenrechner berechnet werden kann.
    Gemeint ist hier die Bogenmaß\-/Version;
    möglicherweise müssen Sie Ihre Taschenrechner erst auf das Bogenmaß umstellen.
    Sie brauchen nichts weiter über diese Funktion zu wissen zur Bearbeitung dieser Aufgabe.
    Fertigen Sie zunächst eine einfache grafische Darstellung von $F$ an.
    \par
    Es sei $X \in \ZV$ so, dass $F_{X} = F$ gilt.
    (So etwas gibt es tatsächlich, auch wenn wir den zugehörgen Satz nicht aufgeführt haben.)
    Berechnen Sie:
    \begin{enumerate}
    \item $\prob{X \leq -2}$
    \item $\prob{X+10 \leq -2}$
    \item $\prob{X > 0}$
    \item $\prob{X \leq 0}$
    \item $\prob{X > 100}$
    \item $\prob{X \leq 0 \lor X > 100}$
    \item $\prob{X > 0 \land X > 100}$
    \end{enumerate}
  \end{exercise}
\end{wtpharma}

\begin{wtpharma}
  \begin{solution}
    Es gilt:
    \begin{enumerate}
    \item $\prob{X \leq -2} = F(-2) \approx 0.1476$
    \item $\prob{X+10 \leq -2} = \prob{X \leq -12} = F(-12) \approx 0.02646$
    \item $\prob{X > 0} = 1 - \prob{X \leq 0} = F(0) = \frac{1}{2}$
    \item $\prob{X \leq 0} = \frac{1}{2}$
    \item $\prob{X > 100} = 1 - \prob{X \leq 100} = 1 - F(100) \approx 0.003183$
    \item $\prob{X \leq 0 \lor X > 100} = \prob{X \leq 0} + \prob{X > 100} \approx 0.5032$,
      denn die Events $X \leq 0$ und $X > 100$ sind disjunkt.
    \item $\prob{X > 0 \land X > 100} = \prob{X > 100} \approx 0.003183$
    \end{enumerate}
  \end{solution}
\end{wtpharma}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  Nämlich für viele CDFs $F$ gibt es $\ffn{f}{\RR}{\RRnn}$ so,

    und  $F(u) = \int_{\ninfty}^{u} f$ für alle $u \in \RR$ gilt.

    dass $F(u)$ alle $u \in \RR$ die Fläche zwischen x\=/Achse und $f$ ist,

    die sich links von $u$ bis ins Unendliche erstreckt.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{para}[Zur Gewinnung der Stichprobe]
  Oft haben wir viele Möglichkeiten,
  welche Objekte wir vermessen, zu welchen Zeitpunkten wir messen, \etc
  Das Merkmal, das wir messen wollen, darf keinen Einfluss auf unsere Wahl haben.
  Hier sind drei einfache Beispiele,
  wie man bei der Gewinnung der Stichprobe \emphasis{nicht} vorgehen sollte:
  \begin{itemize}
  \item Wir haben eine Menge von Objekten gegeben, zum Beispiel eine Lieferung von Produkten,
    und möchten eine Aussage über deren Gewicht machen, indem wir einige davon wiegen.
    Unsere Waage misst aber nur bis $\SI{40}{\kg}$ mit brauchbarer Genauigkeit.
    Daher ignorieren wir Objekte über $\SI{40}{\kg}$.
    Dies ist offenbar kein sinnvolles Vorgehen,
    denn so hat das Gewicht Einfluss darauf, ob ein Objekt in der Stichprobe erfasst wird.
  \item Eine Person möchte herausfinden,
    welcher Anteil der Bevölkerung eine bestimmte politische Meinung unterstützt.
    Als Stichprobe fragt sie alle ihre Freunde.
    Das ist kein sinnvolles Vorgehen,
    denn vermutlich haben Personen, die zum selben Freundeskreis gehören,
    ähnliche politische Meinungen.
    Es werden also Personen mit einer bestimmten Meinung bevorzugt in der Stichprobe erfasst.
  \item Eine Maschine stellt Produkte her.
    Wir möchten wissen, welcher Anteil der Produkte defekt sind.
    Dazu betrachten wir an einem Tag die ersten $30$ Produkte,
    die von der Maschine hergestellt wurden, nachdem die Maschine die Nacht über stillstand.
    Das kann problematisch sein, wenn die Maschine ein anderes Verhalten zeigt,
    wenn sie kalt ist, im Vergleich zum warmgelaufenen Zustand.
    Es werden hier also möglicherweise eine Art von Produkten
    (defekte Produkte oder funktionstüchtige Produkte,
    je nach dem ob die Maschine kalt oder warm besser arbeitet)
    bevorzugt in der Stichprobe erfasst.
  \end{itemize}
\end{para}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  \par
  Wir bezeichnen mit $\ZVEV \subseteq \ZV$ die Menge aller ZV über $(\Om, \SIG, \prob{})$,
  die einen Erwartungswert besitzen.
