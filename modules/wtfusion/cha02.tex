\chapter{Grundlagen der Anwendung}
\label{wtfusion/cha02}

\section{Modellierung}

\begin{para}[Problemstellung]
  \label{wtfusion/cha02:problemstellung}
  Wir betrachten folgendes Problem:
  Wir interessieren uns für den Zustand eines bestimmten Teils der
  Welt zu einem bestimmten Zeitpunkt;
  dieser Zustand ist uns aber nicht bekannt.
  Beispiele:
  \begin{enumerate}
  \item Aus einer Urne mit $n$ Kugeln,
    nummeriert von $1$ bis~$n$, wird ohne Hinsehen eine Kugel gezogen.
    Welche Nummer wird diese Kugel haben?
  \item Wie das vorige Experiment, doch die Kugel wurde bereits gezogen,
    uns aber nicht gezeigt.
    Welche Nummer hat die gezogene Kugel?
  \item Wir werfen eine Münze.
    Welche Seite wird nach der Landung nach oben zeigen?
  \item\label{wtfusion/cha02:problemstellung:patienten}
    Wir behandeln $n$ Patienten mit einem neuen Medikament.
    Wie vielen von ihnen wird es in einer Woche besser gehen?
    Oder mit mehr Details:
    Welchen der $n$ Patienten wird es in einer Woche besser gehen
    und welchen nicht?
    \begin{wtinf}
    \item Wir führen einen Algorithmus aus, der die Zufallsbits des Computers nutzt.
      Welches Ergebnis wird er liefern?
    \end{wtinf}
  \item Hat eine bestimmte Person zum aktuellen Zeitpunkt eine bestimmte Krankheit?
  \item Wird es morgen regnen?
  \end{enumerate}
\end{para}

\begin{para}[Experiment]
  In den ersten
  \begin{wtinf}
    fünf
  \end{wtinf}
  \begin{wtpharma}
    vier
  \end{wtpharma}\xspace
  Beispielen haben wir eine Beschreibung
  eines \term{Experimentes} oder \term{Zufallsprozesses},
  und der Zustand der Welt, nach dem wir fragen,
  ist geprägt durch das Ergebnis des Experimentes.
  Auch die Fragen nach dem aktuellen Gesundheitszustand einer Person
  und nach dem Wetter des nächsten Tages lassen sich so auffassen:
  Das Experiment ist das Leben der Person bis zum aktuellen Zeitpunkt,
  oder die Person wurde zufällig aus der Bevölkerung gewählt,
  \bzw das Experiment umfasst die Vorgänge in der Troposphäre der Erde.
  Ab jetzt werden wir stets von Experimenten und ihren Ergebnissen sprechen.
\end{para}

\begin{para}[Motivation der Modellierung]
  Wir wollen solche Situationen mathematisch so modellieren,
  dass mithilfe des Modells
  die uns interessierenden Fragen möglichst genau beantwortet werden können.
  Dabei erhalten wir zunächst
  --~mittels exakter Schlüsse, wie in der Mathematik üblich~--
  Aussagen im Rahmen des Modells.
  Diese können nach geeigneten Vorüberlegungen oft in naheliegender Weise
  in Bezug auf die Welt außerhalb der Mathematik interpretiert werden.
\end{para}

\begin{para}[Ablauf einer Modellierung und Anwendung]
  Unser Modell wird immer ein W'raum sein.
  Wir arbeiten allerdings nicht direkt mit diesem, sondern mit~ZV.
  Vereinfacht dargestellt läuft das so ab:
  \begin{itemize}
  \item Identifiziere die grundlegenden Merkmale eines Ergebnisses des Experimentes so,
    dass sich jedes dieser Merkmale als reelle Zahl ausdrücken lässt.
    Für diese Beschreibung sei $n$ die Anzahl dieser grundlegenden Merkmale.
  \item Formuliere eine Verteilung für $n$ ZV, für jedes der $n$ Merkmale eine ZV,
    so dass darin unser Verständnis über das Experiment möglichst gut zum Ausdruck kommt.
    Zitiere ein Existenzresultat, das sagt, dass es einen solchen W'raum $\wraum$
    und darüber solche ZV tatsächlich gibt.
  \item Berechne die W'keit der Events, die uns interessieren.
  \end{itemize}
  Die Vorstellung ist, dass die Ausführung des Experimentes ein Element $\om$ von $\Om$
  als die zutreffende Repräsentation der Wirklichkeit auswählen wird.
  Die ZV liefern verschiedene Sichten auf dieses $\om$.
  Im Beispiel mit den Patienten aus
  \autoref{wtfusion/cha02:problemstellung}~\ref{wtfusion/cha02:problemstellung:patienten}
  würde man ZV $\eli{X}{n}$ wählen, für jeden Patienten eine.
  Es enthält jedes Ergebnis $\om$ die volle Information,
  und $X_{i}$ extrahiert daraus die relevante Information für den \xten{i} Patienten,
  indem $X_{i}(\om)$ angibt, ob es Patient~$i$ besser geht nach einer Woche oder nicht
  (üblicherweise hätte $X_{i}$ Bernoulliverteilung).
\end{para}

\begin{para}[Diskussion der Wahrscheinlichkeit]
  Wir haben nun also:
  \begin{itemize}
  \item Die W'keit, die vom W'maß kommt.
  \item Eine Wahrscheinlichkeit mit Bezug auf die Anwendung,
    nämlich als einen \term{Grad der Überzeugung}:
    Wie stark wollen wir davon ausgehen,
    dass der uns interessierende Teil der Welt tatsächlich einen bestimmten Zustand hat?
  \end{itemize}
  Um eine Modellierung wie im vorigen Absatz zu rechtfertigen,
  sollte eine enge Beziehung zwischen W'keit und Grad der Überzeugung bestehen.
  Zumindest sollte man sich davon überzeugen,
  dass die beiden definierenden Eigenschaften eines W'maßes,
  nämlich die Normierung und die Additivität,
  im Einklang damit sind, wie wir über den Grad der Überzeugung denken.
  Bei der Normierung ist das einfach:
  Dass überhaupt etwas passiert, hat für uns höchstmöglichen Grad der Überzeugung
  und wird damit über die höchstmögliche W'keit ausgedrückt, nämlich über~$1$.
  Die Additivität leuchtet üblicherweise auch schnell ein;
  das ist aber schwieriger in Worte zu fassen.
  Wir vertiefen das hier nicht weiter.
\end{para}

\section{Konditionierung und Unabhängigkeit}

\begin{para}[Berücksichtigung neuer Informationen]
  Es sei $\wraum$ ein W'raum,
  der durch eine Modellierung zustande gekommen ist.
  Es sei $A \in \SIG$ ein Event, für das wir uns interessieren;
  dieses ist üblicherweise als logische Formel über ZV formuliert.
  Nun kommt es oft vor, dass wir nach erfolgter Modellierung neue Informationen erhalten,
  die vielleicht nicht einwandfrei klären, ob $A$ eintritt oder nicht,
  jedoch schon unseren Grad der Überzeugung bezüglich $A$ verändern.
  Beispiel: $A$ ist das Event, dass es am Samstag regnen wird.
  Am Freitag liegt $\prob{A}$ fest aufgrund der dann verfügbaren Informationen.
  Am Samstagmorgen regnet es zwar noch nicht, aber wir sehen schwarze Wolken am Himmel.
  Unser Grad der Überzeugung für Regen am Samstag ist nun größer als $\prob{A}$.
  Grad der Überzeugung und W'keit weichen nun voneinander ab.
\end{para}

\begin{para}[Darstellung über konditionierte W'keit]
  Wie können wir dies systematisch behandeln?
  Zunächst muss unser Modell die Möglichkeit bieten, die Beobachtungen als Event auszudrücken.
  In obigem Beispiel bräuchten wir also nicht nur eine ZV die angibt,
  ob es am Samstag regnet, sondern auch eine ZV dafür, ob wir am Samstagmorgen schwarze Wolken sehen
  (beide ZV hätten üblicherweise Bernoulliverteilung).
  \par
  Es sei $A$ das Event, das uns interessiert, und es sei $B$ das Event, das wir beobachten,
  von dem wir also inzwischen wissen, dass es eintritt.
  Nach der Beobachtung von $B$ ist unser Grad der Überzeugung bezüglich $A$
  möglicherweise nicht mehr~$\prob{A}$.
  Wir wollen uns kurz überlegen, dass unser Grad der Überzeugung nun $\prob{A \cond B}$ sein sollte
  (vorausgesetzt natürlich, dass $\prob{B} > 0$ gilt, sonst geht das nicht).
  \par
  Als ersten Ansatz betrachten wir
  $\ffnx{\widetilde{\prob{}}}{\SIG}{\intcc{0}{1}}{E \mapsto \prob{E \cap B}}$
  als Vorschlag für ein aktualisiertes W'maß.
  Damit berücksichtigen wir, dass unter dem Wissen, dass $B$ eintritt,
  von $E$ nur noch der Anteil eintreten kann, der mit $B$ übereinstimmt.
  Dieser Ansatz ist aber noch nicht sachgerecht,
  denn es gilt $\widetilde{\prob{}}\p{E} \leq \prob{E}$ für alle $E \in \SIG$,
  d.h. der Grad der Überzeugung eines Events kann niemals größer werden durch neue Informationen.
  Das ist offenbar nicht was wir wollen.
  Außerdem kann bei $\widetilde{\prob{}}$ die Normierung verletzt sein.
  Um diese wieder herzustellen, teile durch $\prob{B}$,
  und damit sind wir bei der konditionierten W'keit.
  \par
  Wir geben durch Betrachtung einiger Extremfälle weitere Argumente,
  warum die konditionierten W'keit der richtige Weg ist, um neue Informationen zu integrieren:
  \begin{itemize}
  \item Es gilt $\prob{A \cond \Om} = \prob{A}$.
    Das ist sinnvoll,
    denn die Information, dass $\Om$ eintritt, hat keinen Gehalt
    -- dass $\Om$ eintritt, wussten wir von Anfang an.
  \item Es gilt $\prob{B \cond B} = 1$.
    Das ist sinnvoll,
    denn wenn wir wissen, dass $B$ eintritt,
    sollten $B$ den höchsten Grad der Überzeugung für uns haben.
    Daher passt es, dass die auf $B$ konditionierte W'keit von $B$
    den höchstmöglichen Wert bekommt, nämlich~$1$.
  \item Es gelte $A \cap B = \emptyset$.
    Dann gilt $\prob{A \cond B} = 0$.
    Das ist sinnvoll,
    denn wenn wir wissen, dass $B$ eintritt, dann tritt $A$ gewiss nicht ein.
    Es sollte daher $A$ den niedrigsten Grad der Überzeugung für uns haben.
    Daher passt es, dass die auf $B$ konditionierte W'keit von $A$
    den niedrigsten möglichen Wert bekommt, nämlich~$0$.
  \item Es sei $B \subseteq A$.
    Dann gilt $\prob{A \cond B} = 1$.
    Das ist sinnvoll,
    denn wenn wir wissen, dass $B$ eintritt, dann tritt $A$ gewiss ein.
    Es sollte daher $A$ den höchsten Grad der Überzeugung für uns haben.
    Daher passt es, dass die auf $B$ konditionierte W'keit von $A$
    den höchstmöglichen Wert bekommt, nämlich~$1$.
  \end{itemize}
\end{para}

Folgender Satz stellt die Verbindung zur Unabhängigkeit her:

\begin{satz}[Charakterisierung der Unabhängigkeit]
  \label{wtfusion/cha02:unab-kond}
  Es sei $\wraum$ ein W'raum und $\ZV$ wie üblich.
  Es sei $\zvtupdef{X}{n}$.
  Dann sind äquivalent:
  \begin{enumerate}
  \item $\zvtup{X}$ ist unabhängig.
  \item Für alle Formeln $\vphi_{1}$ und $\vphi_{2}$, die Events beschreiben, gilt Folgendes.
    Es möge $M_{1}, M_{2} \subseteq \setn{n}$ so geben,
    dass $M_{1} \cap M_{2} = \emptyset$ gilt,
    und für alle $i \in \set{1,2}$
    kommen in $\vphi_{i}$ nur ZV $X_{j}$ mit $j \in M_{i}$ oder Kombinationen aus denen vor.
    Es möge noch $\prob{\vphi_{2}} > 0$ gelten.
    Dann gilt $\prob{\vphi_{1}} = \prob{\vphi_{1} \cond \vphi_{2}}$, d.h. die Konditionierung ist wirkungslos.
  \end{enumerate}
\end{satz}

\begin{proof}
  \imprefx{1}{2} Folgt direkt aus \autoref{wtfusion/cha01:blocklemma}.\par
  \imprefx{2}{1}
  Es sei $u \in \RR^{n}$.
  Wir zeigen $F_{\zvtup{X}}(u) = F_{X_{1}}(u_{1}) \cdot F_{(\elix{X}{2}{n})}(\elix{u}{2}{n})$;
  die Behauptung folgt daraus leicht induktiv.
  \par
  Definiere $\vphi_{1} \evdfiff X_{1} \leq u_{1}$
  und $\vphi_{2} \evdfiff \bigwedge_{i=2}^{n} {X_{i} \leq u_{i}}$.
  Zu zeigen ist:
  \begin{IEEEeqnarray*}{0l}
    \prob{\vphi_{1} \land \vphi_{2}}
    =  \prob{\vphi_{1}} \cdot \prob{\vphi_{2}}
  \end{IEEEeqnarray*}
  Wenn $\prob{\vphi_{2}} = 0$ gilt,
  dann gilt $\prob{\vphi_{1} \land \vphi_{2}} = 0$, und diese Gleichung gilt.
  Wenn $\prob{\vphi_{2}} > 0$ gilt,
  dann folgt die Gleichung direkt aus der Annahme $\prob{\vphi_{1}} = \prob{\vphi_{1} \cond \vphi_{2}}$.
\end{proof}

\begin{example}[Anwendung des Satzes]
  Es sei $\zvtupdef{X}{7}$ unabhängig.
  Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{X_{1} =0} = \prob{X_{1} =0 \cond X_{3} = 1 \lor X_{7} = 0} \\
    \prob{X_{1} < 1 \land X_{4} > 0} = \prob{X_{1} < 1 \land X_{4} > 0 \cond X_{3} = 0 \lor X_{7} = 0} \\
    \prob{X_{1} = X_{2}} = \prob{X_{1}=X_{2} \cond X_{3} < X_{7}}
  \end{IEEEeqnarray*}
  Hier wird --~wie immer~-- stets vorausgesetzt, dass das Event,
  auf das wir konditionieren, eine positive W'keit hat.
\end{example}

\begin{para}[Weitere Ausführungen zum Satz]
  Dass ein Tupel aus gewissen ZV unabhängig ist, bedeutet also:
  Wenn wir etwas über einige dieser ZV wissen,
  dann wissen wir dadurch nicht mehr über die übrigen dieser~ZV.
  Etwas genauer formuliert haben wir:
  Unser Grad der Überzeugung bezüglich eines Events,
  das über einige dieser ZV formuliert ist,
  verändert sich nicht, wenn wir erfahren, dass ein Event eintritt,
  das mit einigen oder allen der übrigen ZV formuliert ist
  (und das positive W'keit hat, sonst können wir nicht darauf konditionieren).
  Bei der Unabhängigkeit und der Konditionierung geht es also um den Grad der Überzeugung.
  Das ist besonders wichtig zu verstehen in Bezug auf die Unabhängigkeit,
  denn oft wird --~fälschlicherweise~-- Unabhängigkeit nur als eine physische Unabhängigkeit interpretiert.
  In \autoref{wtfusion/cha03}~\ref{wtfusion/cha03:zufaellige-muenze}
  werden wir ein Experiment analysieren, welches dies besonders verdeutlicht.
\end{para}

\section{Formel von Bayes}

Es sei in diesem Abschnitt $\wraum$ ein W'raum.

\begin{satz}[Bayes\protect\footnote{%
    Thomas Bayes, englischer Mathematiker, 1701--1761.}]
  \label{wtfusion/cha02:bayes}
  Es seien $A,B \in \SIG$ so, dass $\prob{A}, \prob{B} > 0$ gilt.
  Dann~gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{A \cond B} = \frac{\prob{A}}{\prob{B}} \cdot \prob{B \cond A}
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}
  Folgt direkt aus der Definition.
\end{proof}

Die Formel von Bayes ist oft handlicher in einer anderen Formulierung.
Für diese benötigen wir zwei weitere Begriffe.

\begin{para}[Odds]
  Es seien $A, B \in \SIG$ so, dass $\prob{B} > 0$ gilt.
  Wir definieren die \term{Odds} von $A$ \term{konditioniert} auf $B$
  \bzw die \term{Odds} von $A$ als:
  \begin{IEEEeqnarray*}{0l}
    \odds{A \cond B} \df
    \begin{cases}
      \frac{\prob{A \cond B}}{\prob{A^{c} \cond B}}
      = \frac{\prob{A \cond B}}{1-\prob{A \cond B}}
      & \prob{A \cond B} < 1 \\
      \pinfty & \prob{A \cond B} = 1
    \end{cases} \\
    \odds{A} \df \odds{A \cond \Om} =
    \begin{cases}
      \frac{\prob{A}}{\prob{A^{c}}}
      = \frac{\prob{A}}{1-\prob{A}}
      & \prob{A} < 1 \\
      \pinfty & \prob{A} = 1
    \end{cases}
  \end{IEEEeqnarray*}
  Wir bilden also den Quotienten aus (konditionierter) W'keit und (konditionierter) Gegenw'keit.
  Im Spezialfall, dass diese Gegenw'keit~$0$ ist, definiert man die Odds zu~$\pinfty$.
  Odds sind eine andere Art, W'keiten anzugeben.
  Gelegentlich wird auch das Wort \enquote{Chancen} dafür verwendet,
  wir wollen aber bei \enquote{Odds} bleiben.
  \par
  Die folgenden einfachen Umrechnungsformeln genügen oft:
  \begin{itemize}
  \item Ist $p \in \intco{0}{1}$ eine W'keit (egal wie konditioniert),
    dann sind ${p' \df \frac{p}{1-p}}$ die zugehörigen Odds.
  \item Sind $o \in \RRnn$ Odds (egal wie konditioniert),
    dann ist $\frac{o}{o+1}$ die zugehörige W'keit.
  \end{itemize}
  So können wir also leicht zwischen W'keit und Odds umrechnen.
\end{para}

\begin{para}[Likelihood\-/Ratio]
  Es seien $A,B \in \SIG$ so, dass gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{A}, \, \prob{A^{c}}, \, \prob{B \cond A^{c}} > 0
  \end{IEEEeqnarray*}
  Wir definieren die \term{Likelihood\-/Ratio} von $(B,A)$ als:
  \begin{IEEEeqnarray*}{0l}
    \LR(B,A) \df \frac{\prob{B \cond A}}{\prob{B \cond A^{c}}}
    = \frac{\prob{B \cond A}}{1 - \prob{B^{c} \cond A^{c}}}
  \end{IEEEeqnarray*}
  Dieser Wert gibt also an, wie viel höher die W'keit
  von $B$ konditioniert auf $A$ ist als konditioniert auf $A^{c}$.
\end{para}

Wir sind nun in der Lage, die angekündigte andere Version der Formel von Bayes anzugeben:

\begin{satz}[Odds\-/Version der Formel von Bayes]
  \label{wtfusion/cha02:bayes-odds}
  Es seien $A,B \in \SIG$ so, dass gilt:
  \begin{IEEEeqnarray*}{0l}
    \prob{A}, \, \prob{A^{c}}, \, \prob{B \cond A^{c}} > 0
  \end{IEEEeqnarray*}
  Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    \odds{A \cond B} = \odds{A} \cdot \LR(B,A)
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}
  Folgt leicht aus der Definition und der bekannten Formel von Bayes
  von \autoref{wtfusion/cha02:bayes}.
  Die Voraussetzunge stellen sicher, dass:
  \begin{itemize}
  \item die W'keiten aller Events, auf die konditioniert wird, positiv sind;
  \item bei der Likelihood\-/Ratio nicht durch~$0$ geteilt wird;
  \item alle Odds endlich sind.\qedhere
  \end{itemize}
\end{proof}

\begin{para}[Bedeutung der $\LR$]
  \label{wtfusion/cha02:lr}
  Wenn $\LR(B,A) > 1$ gilt, dann gilt $\odds{A \cond B} > \odds{A}$ nach obigem Satz;
  man sagt auch, die Beobachtung $B$ spricht für $A$.
  Wenn ${\LR(B,A) < 1}$, dann spricht $B$ gegen $A$.
\end{para}
