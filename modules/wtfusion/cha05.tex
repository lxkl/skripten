\chapter{Konfidenzintervallschätzung}
\label{wtfusion/cha05}

\lxklKitWipWarning

\section{Grundsätzliche Problemstellung}

\begin{para}[Stichprobe]
  In der Wissenschaft hat man oft die Situation,
  dass eine gewisse Beobachtung gegeben ist und man aufgrund dieser Beobachtung
  Aussagen über die zugrunde liegenden Gesetzmäßigkeiten machen möchte.
  Wir nennen eine solche Beobachtung die \term{Stichprobe}.
  Bei uns werden Stichproben immer aus einem oder mehreren Tupeln reeller Zahlen bestehen.
  Beispiele:
  \begin{itemize}
  \item Man hat ein Tupel $x \in \RR^{n}$, zum Beispiel aus Luftdruckwerten
    an einem bestimmten Ort über $n$ Tage.
  \item Man hat Tupel $a \in \RR^{n_{\grA}}$ und $b \in \RR^{n_{\grB}}$,
    zum Beispiel aus bestimmten Blutwerte von zwei Patientengruppen,
    die Größe $n_{\grA}$ \bzw $n_{\grB}$ haben.
  \item Man hat bei einer Serie von $n$ Teilexperimenten,
    die jeweils Erfolg oder Misserfolg liefern können,
    beobachtet, welche davon Erfolg und welche Misserfolg geliefert haben.
    Das ergibt $x \in \OI^{n}$.
  \end{itemize}
\end{para}

\begin{para}[Rolle der Wahrscheinlichkeitstheorie]
  Die Wahrscheinlichkeitstheorie hilft uns.
  Wir modellieren das Experiment, das zu der Beobachtung geführt hat,
  typischerweise mit einer ZV pro Komponente in der Stichprobe.
  Diese ZV heißen \term{Stichprobenvariablen}.
  Dabei legen wir die Verteilung der Stichprobenvariablen nicht genau fest,
  sondern wir legen nur eine gewisse Menge an möglichen Verteilungen fest.
  Diese Menge ist typischerweise über einen oder mehrere Parameter beschrieben.
  Zum Beispiel könnte man sich auf die Menge der Bernoulliverteilungen einigen,
  das ist $\set{\Ber(p) \suchthat p \in \intcc{0}{1}}$; hier hat man genau einen Parameter.
  Die Aufgabe ist dann, aus der Beobachtung einen sinnvollen Vorschlag
  für einen oder mehrere der Parameter zu machen;
  man spricht hier von einer \term{Schätzung}.
  Besonders interessant sind Methoden zur Schätzung,
  die sich w'keitstheoretisch analysieren lassen.
\end{para}

Neben der Bernoulliverteilung kommt hier die Normalverteilung viel zum Einsatz.
Die kennen wir noch nicht, es ist keine diskrete Verteilung.
Wir müssen uns daher zunächst mit einer neuen Art von Verteilungen befassen.

\section{Verteilungen mit PDF}

\begin{para}[CDF]
  Es sei $\ffn{F}{\RR}{\intcc{0}{1}}$.
  Wir nennen $F$ eine \term{kumulative Verteilungsfunktion}
  oder eine \term{cumulative distribution function} (CDF), wenn gilt:
  \begin{enumerate}
  \item $F$ ist monoton steigend.
    \begin{wtinf}
    \item $F$ ist rechtsseitig stetig,
      d.h. es gilt $\flim{\tiu}{u^{+}} F(\tiu) = F(u)$ für alle $u \in \RR$.
    \item Es ist $\flim{u}{\pinfty} F(u) = 1$ und $\flim{u}{\ninfty} F(u) = 0$.
    \end{wtinf}
    \begin{wtpharma}
    \item $F$ ist rechtsseitig stetig,
      d.h. für alle $u \in \RR$ gilt:
      Wenn man sich $u$ auf der reellen Achse von rechts nähert,
      dann näheren sich die zugehörigen Funktionswerte $f(u)$ an.
    \item $F$ nähert sich rechts dem Wert~$1$ und links dem Wert~$0$ an.
    \end{wtpharma}
  \end{enumerate}
  \begin{wtpharma}
    Die beiden letzten Punkte sind nur informell beschrieben,
    da uns für eine strenge Behandlung die Grundlagen der Analysis fehlen.
    Das ist nicht schlimm, das wir damit nicht weiter arbeiten werden.
    Es genügt eine ungefähre Vorstellung.
  \end{wtpharma}
\end{para}

\begin{para}[CDF einer ZV]
  Es sei $\wraum$ ein W'raum und $X$ eine ZV darüber. Definiere:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{F_{X}}{\RR}{\intcc{0}{1}}{u \mapsto \prob{X \leq u}}
  \end{IEEEeqnarray*}
  Wir nennen $F_{X}$ die CDF von $X$,
  und in der Tat kann man zeigen, dass es sich dabei um eine CDF im Sinne des vorigen Absatzes handelt
  \begin{wtinf}
    (Übung)
  \end{wtinf}.
\end{para}

\begin{satz}[CDF liefert Verteilung]
  Die Verteilung einer ZV $X$ ist bereits durch $F_{X}$ festgelegt.
\end{satz}

\begin{proof}
  Hier nicht; Interessierte mit ausreichend Vorkenntnissen lesen~\cite[Satz (1.12)]{Georgii09}.
  \begin{wtpharma}
    Wir schauen uns nur einen winzigen Teil des Beweises an.
    Nämlich für alle $u \in \RR$ gilt $\prob{X > u} = 1 - \prob{X \leq u} = 1 - F_{X}(u)$.
    So erschließen wir die W'keit von $X > u$ aus $F_{X}$.
  \end{wtpharma}
  \begin{wtinf}
    Als Übung zeige man,
    wie für alle Intervalle $I \subseteq \RR$
    die W'keit $\prob{X \in I}$ aus $F_{X}$ erschlossen werden kann.
  \end{wtinf}
\end{proof}

\begin{satz}[CDF-basierter Existenzsatz]
  Es seien $\eli{F}{n}$ CDFs.
  Dann gibt es einen W'raum und darüber ZV $\eli{X}{n}$ so, dass gilt:
  \begin{itemize}
  \item $F_{X_{i}} = F_{i}$ für alle $i \in \setn{n}$.
  \item $(\eli{X}{n})$ ist unabhängig.
  \end{itemize}
\end{satz}

\begin{proof}
  Hier nicht; Interessierte mit ausreichend Vorkenntnissen lesen zum Beispiel~\cite{Georgii09,Rosenthal06}.
\end{proof}

\begin{para}[Bedeutung der Borelmengen]
  Aus den beiden vorigen Sätze folgt:
  Wenn $F$ eine CDF ist, dann gibt es genau eine Verteilung,
  d.h. genau ein W'maß auf $(\RR, \borel{})$,
  das jede ZV mit CDF $F$ hat
  (und es gibt auch tatsächlich einen W'raum und darüber eine ZV mit CDF $F$).
  Diese Aussage wird \emphasis{falsch},
  wenn wir anstelle von $(\RR, \borel{})$ den Eventraum $(\RR, \cP(\RR))$ setzen.
  Hier ist es also wichtig, dass wir die Borelmengen haben.
  Den Beweis, dass es mit der Potenzmenge nicht geht,
  könnten wir sogar verstehen, hilft beim aktuellen Thema aber wenig weiter;
  Interessierte können \cite[Section 1.2]{Rosenthal06} lesen und Weiteres daraus herleiten.
\end{para}

\begin{para}[PDF]
  Für viele CDFs gibt es eine alternative Repräsentation, die oft angenehmer zu handhaben ist.
  Es sei $\ffn{f}{\RR}{\RRnn}$ so, dass
  \begin{wtinf}
    $f$ über kompakte Intervalle integrierbar ist
    und $\int_{\ninfty}^{\pinfty} f = 1$ gilt.
  \end{wtinf}
  \begin{wtpharma}
    die Fläche zwischen x\=/Achse und $f$ genau~$1$ ist.
    (Dies ist wieder eine informelle Beschreibung, die für uns genügt.)
  \end{wtpharma}
  Dann nennen wir $f$ eine \term{Wahrscheinlichkeitsdichtefunktion}
  oder \term{probability density function} (PDF).
  Wenn $f$ eine PDF ist, dann
  \begin{wtinf}
    ist $\fnx{\RR}{\intcc{0}{1}}{u \mapsto \int_{\ninfty}^{u} f}$ eine CDF,
    sogar eine stetige CDF.
  \end{wtinf}
  \begin{wtpharma}
    erhalten wir eine CDF $F$ dadurch,
    dass wir $F(u)$ für alle $u \in \RR$ als die Fläche zwischen x\=/Achse und $f$ definieren.
  \end{wtpharma}
  Wir sagen, dass $F$ aus $f$ durch \term{Integration} hervorgeht.
\end{para}

\begin{para}
  Es möge die CDF $F$ einer einer PDF $f$ durch Integration hevorgehen,
  und es sei $X$ eine ZV mit CDF $F$.
  Man sieht leicht, dass dann $\prob{X=u} = 0$ für alle $u \in \RR$ gilt;
  es handelt sich als gewiss um keine diskrete Verteilung.
  Es folgt ferner für alle $a,b \in \RR$ mit $a \leq b$:
  \begin{IEEEeqnarray*}{0l}
    \prob{X \in \intcc{a}{b}}
    = \prob{X \in \intoo{a}{b}}
    = \prob{X \in \intoc{a}{b}}
    = \prob{X \in \intco{a}{b}}
  \end{IEEEeqnarray*}
  Diese W'keit ist ferner
  \begin{wtinf}
    $\int_{a}^{b} f$.
  \end{wtinf}
  \begin{wtpharma}
    die Fläche zwischen x\=/Achse und $f$ im Bereich zwischen $a$ und $b$.
  \end{wtpharma}
\end{para}

\begin{para}[Redeweisen]
  \hfill
  \begin{itemize}
  \item Wir nennen eine PDF $f$ eine PDF einer ZV $X$,
    wenn $F_{X}$ aus $f$ durch Integration hervorgeht.
    (Eine ZV kann mehrere verschiedene PDFs haben,
    aber nur eine CDF.)
  \item Wir nennen eine CDF die CDF einer Verteilung $\cD$,
    und schreiben $F_{\cD}$ dafür,
    wenn $F_{\cD}$ die CDF aller ZV mit $X \distr \cD$ ist.
  \item Wir nennen eine PDF $f$ eine PDF einer Verteilung $\cD$,
    wenn die CDF von $\cD$ aus $f$ durch Integration hervorgeht.
  \end{itemize}
\end{para}

\section{Normalverteilung}

\begin{para}[Definition]
  Es seien $\mu \in \RR$ und $\sig \in \RRpos$.
  Wir sagen, dass eine ZV $X$ \term{Normalverteilung}
  mit Erwartungswert $\mu$ und Varianz $\sig^{2}$ hat,
  in Zeichen $X \distr \Norm(\mu,\sig^{2})$,
  wenn $X$ die folgende PDF hat:
  \begin{IEEEeqnarray}{0l}
    \label{wtfusion/cha05:norm-pdf}
    \fnx{\RR}{\RRnn}{u \mapsto
      \frac{1}{\sqrt{2\pi} \sig} \euler^{-\frac{1}{2} \parens{\frac{u-\mu}{\sig}}^{2}}}
  \end{IEEEeqnarray}
  Die Verteilung $\Norm(0,1)$ heißt \term{Standardnormalverteilung}.
  Wir zeigen Plots einer PDF und der CDF der $\Norm(0,1)$.
  \begin{description}[font=\it]
  \item[Links:] PDF, d.h. die Funktion aus~\eqref{wtfusion/cha05:norm-pdf} mit $\mu=0$ und $\sig=1$.
    Der farbige Teil ist ein Beispiel für eine Fläche zwischen Graph und x\=/Achse.
    Der Inhalt der farbigen Fläche ist der Wert~$F_{\Norm(0,1)}(-1.5)$.
  \item[Rechts:] CDF, d.h. die Funktion $F_{\Norm(0,1)}$.
    Die y\=/Koordinate des farbigen Punktes ist der Flächeninhalt
    der farbigen Fläche aus dem Plot auf der linken Seite.
  \end{description}
  \inlinerow{\input{07-stdnorm-1-pdf}}{\input{07-stdnorm-1-cdf}}
\end{para}

\begin{para}[Bedeutung der Parameter]
  Wir kennen aus \autoref{wtfusion/cha04} Erwartungswert und Standardabweichung diskreter Verteilungen.
  Das Konzept lässt sich auf Verteilungen mit PDF erweitern.
  Wir geben hier nicht die Definition dazu.
  Wichtig für uns zu wissen:
  \begin{itemize}
  \item Die $\Norm(\mu,\sig^{2})$ hat tatsächlich Erwartungswert $\mu$ und Standardabweichung $\sig$.
  \item Erwartungswert und Standardabweichung sind auch für Verteilungen mit PDF
    wichtige Parameter einer Verteilung,
    insbesondere gilt auch die Tschebyscheff\-/Ungleichung.
    (Allerdings haben nicht alle Verteilungen mit PDF Erwartungswert und Standardabweichung.)
  \end{itemize}
\end{para}

\begin{satz}[Rechnen mit der Normalverteilung]
  \begin{enumerate}
  \item Es seien $\mu \in \RR$ und $\sig \in \RRpos$.
    Es sei $X \distr \Norm(\mu, \sig^{2})$ und $c \in \RRnz$ und $d \in \RR$.
    Dann gilt:
    \begin{IEEEeqnarray*}{0l}
      c X + d \distr \Norm(c \mu + d, \, c^{2} \sig^{2})
    \end{IEEEeqnarray*}
  \item Es seien $\mu \in \RR^{n}$ und $\sig \in \RRpos^{n}$.
    Es sei $(\eli{X}{n})$ unabhängig mit
    $X_i \distr \Norm(\mu_i, \sig_i^{2})$ für alle $i \in \setn{n}$.
    Dann gilt:
    \begin{IEEEeqnarray*}{0l}
      \sum_{i=1}^n X_i \distr \Norm(\sum_{i=1}^n \mu_i, \, \sum_{i=1}^n \sig^{2}_i)
    \end{IEEEeqnarray*}
  \end{enumerate}
\end{satz}

\section{Modellklassen}

\begin{para}[Definition]
  Wir konzentrieren uns auf die folgenden fünf Klassen von Modellen:
  \begin{enumerate}[label=(\Roman*)]
  \item\label{wtfusion/cha05:1}
    Es sei $\mu \in \RR$ und $\sig \in \RRpos$.
    Die Stichprobenvariablen sind in einem Tupel:
    \begin{IEEEeqnarray*}{0l}
      \zvtup{X} = (\eli{X}{n}) \distr \Norm(\mu,\sig^2)^{n} \quad\text{\iid}
    \end{IEEEeqnarray*}
    Es ist $\mu$ \emphasis{unbekannt} und $\sig$ bekannt;
    gesucht ist~$\mu$.
  \item\label{wtfusion/cha05:2}
    Wie ein Modell aus Klasse~\ref{wtfusion/cha05:1},
    doch es sind $\mu$ und $\sig$ \emphasis{sämtlich unbekannt};
    gesucht ist~$\mu$ und eventuell auch~$\sig$.
  \item\label{wtfusion/cha05:3}
    Es seien $\mu_{\grA}, \mu_{\grB} \in \RR$ und $\sig_{\grA}, \sig_{\grB} \in \RRpos$.
    Die Stichprobenvariablen sind in zwei Tupeln:
    \begin{IEEEeqnarray*}{0l}
      \zvtup{A} = (\eli{A}{n_{\grA}}) \distr \Norm(\mu_{\grA},\sig_{\grA}^2)^{n_{\grA}} \quad\text{\iid} \\
      \zvtup{B} = (\eli{B}{n_{\grB}}) \distr \Norm(\mu_{\grB},\sig_{\grB}^2)^{n_{\grB}} \quad\text{\iid}
    \end{IEEEeqnarray*}
    Es sind $\mu_{\grA}, \mu_{\grB} \in \RR$ und $\sig_{\grA}, \sig_{\grB} \in \RRpos$
    \emphasis{sämtlich unbekannt};
    gesucht ist~${\mu_{\grA} - \mu_{\grB}}$.
  \item\label{wtfusion/cha05:4}
    Es seien $\mu_{\grA}, \mu_{\grB} \in \RR$ und $\sig_{D} \in \RRpos$.
    Die Stichprobenvariablen sind in zwei Tupeln:
    \begin{IEEEeqnarray*}{0l}
      \zvtup{A} = (\eli{A}{m}) \\
      \zvtup{B} = (\eli{B}{m})
    \end{IEEEeqnarray*}
    Definiere $D_i \df A_i - B_i$ für alle $i \in \setn{m}$.
    Über die Verteilung wird Folgendes angenommen:
    \begin{IEEEeqnarray*}{0l}
      \forall i \in \setn{m} \holds \expect{A_i} = \mu_{\grA} \land \expect{B_i} = \mu_{\grB} \\
      \eli{D}{m} \distr \Norm(\mu_{\grA}-\mu_{\grB}, \, \sig_D^2)  \quad\text{\iid}
    \end{IEEEeqnarray*}
    Es sind $\mu_{\grA},\mu_{\grB}$ und $\sig_D$ \emphasis{sämtlich unbekannt};
    gesucht ist~${\mu_{\grA} - \mu_{\grB}}$.
  \item\label{wtfusion/cha05:5}
    Es sei $p \in \intcc{0}{1}$.
    Die Stichprobenvariablen sind in einem Tupel:
    \begin{IEEEeqnarray*}{0l}
      \zvtup{X} = (\eli{X}{n}) \distr \Ber(p)^{n} \quad\text{\iid}
    \end{IEEEeqnarray*}
    Es ist $p$ \emphasis{unbekannt} und gesucht.
  \end{enumerate}
\end{para}

\begin{para}[Mindestvoraussetzungen]
  Damit das alles sinnvoll ist, müssen mindestens die beiden folgenden Bedingungen erfüllt sein:
  \begin{itemize}
  \item Das Modell muss gut zu dem Experiment passen, durch das die Stichprobe entstanden ist.
    Hier können im ersten Ansatz grafische Methoden,
    wie zum Beispiel ein \sog Quantil\-/Quantil\-/Plot helfen.
    Wir werden grafische Methoden in einem späteren Kapiteln besprechen.
  \item Die gesuchten Parameter haben eine sinnvolle Interpretation.
    Zum Beispiel in einem Modell der Klasse~\ref{wtfusion/cha05:3} könnte
    $\zvtup{A}$ zu einer Gruppe Patienten gehören, die ein neues Medikament erhält,
    und $\zvtup{B}$ gehört zu einer Gruppe Patienten, die ein Placebo erhält.
    Bei den gemessenen Werten gelte, dass kleiner besser ist.
    Dann könnte man $\Del \df \mu_{\grA} - \mu_{\grB}$ als ein Maß
    für die Wirksamkeit des neuen Medikamentes interpretieren,
    wobei es für das neue Medikament umso besser ist, je kleiner $\Del$ ist.
    Weitere Ideen für Anwendungen werden in den später diskutierten Beispielen gegeben werden.
  \end{itemize}
\end{para}

\section{Schätzung}

\begin{para}[Punktschätzung]
  Wir wollen die gesuchten Parameter ($\mu$, $\sig$, $\mu_{\grA} - \mu_{\grB}$,~$p$)
  aufgrund der gegebenen Stichprobe schätzen.
  Die vielleicht einfachste Art der Schätzung ist die \term{Punktschätzung}:
  Es wird ein einziger Wert für den Parameter errechnet.
  Bei den Erwartungswerten $\mu$ und $\mu_{\grA} - \mu_{\grB}$
  und der Erfolgsw'keit $p$ ist das Natürlichste wohl der Mittelwert.
  In einem Modell der Klasse~\ref{wtfusion/cha05:1} oder~\ref{wtfusion/cha05:2} zum Beispiel
  würde man zu gegebener Stichprobe $x \in \RR^{n}$
  den Mittelwert $\mean{x} \df \frac{1}{n} \sum_{i=1}^{n} x_{i}$ als Punktschätzung für $\mu$ berechnen.
  Ein einzelner Wert hat die Schwäche,
  dass keine Informationen über die damit behaftete Unsicherheit zum Ausdruck kommt.
  Gewiss würden wir der Punktschätzung eher vertrauen, wenn $n=1000$ ist als wenn $n=10$ ist.
  Aber um wie viel mehr?
  Formalisiert wird diese Idee durch \term{Konfidenzintervallschätzer}, die wir nun vorstellen werden.
\end{para}

\begin{para}[Definition CIE]
  Wir fixieren eine Klasse $\cC$ von Modellen,
  zum Beispiel eine der Klassen~\ref{wtfusion/cha05:1} bis~\ref{wtfusion/cha05:5}.
  Es sei $\Theta \subseteq \RR^{r}$ so,
  dass jedes $\vtet \in \Theta$ genau ein Modell in der Klasse spezifiziert;
  wir nennen $\Theta$ die Menge der Parameter.
  Für Klasse~\ref{wtfusion/cha05:2} könnten wir $\Theta = \RR \times \RRpos$ wählen.
  Es sei $\ffn{K}{\RR^n}{\cP(\RR)}$ und $\ffn{f}{\Theta}{\RR}$ so, dass gilt:
  \begin{itemize}
  \item $K(x) \subseteq \RR$ ist ein Intervall für alle $x \in \RR^{n}$.
  \item Für alle $\vtet \in \Theta$ gilt:
    Wenn $\zvtupdefx{X}{n}$ ein Tupel ist,
    das sämtliche Stichprobenvariablen eines Modells in $\cC$ mit Parametern $\vtet$ umfasst,
    dann gilt $\prob{f(\vtet) \in K(\zvtup{X})} \geq \gam$.
  \end{itemize}
  Dann nennen wir $K$ einen
  \term{Konfidenzintervallschätzer} oder \term{Vertrauensintervallschätzer}
  oder \term{confidence interval estimator} (CIE)
  zum \term{Konfidenzniveau} oder zum \term{Vertrauensniveau}
  oder kurz zum \term{Niveau} $\gam$ für $(f, \cC)$.
  \par
  In der Praxis verwendet man meistens eine saloppe, aber besser verständliche Redeweise.
  Zum Beispiel würde man von einem
  CIE zum Niveau~$\gam$ für $\mu$ in Klasse~\ref{wtfusion/cha05:2} sprechen,
  in der offensichtlichen Bedeutung, nämlich:
  \begin{IEEEeqnarray*}{0l}
    \forall \mu \in \RR \innerholds
    \forall \sig \in \RRpos \innerholds
    \forall n \in \NNone \innerholds
    \forall \zvtup{X} \distr \Norm(\mu,\sig^2)^{n} \: \text{\iid} \holds
    \prob{\mu \in K(\zvtup{X})} \geq \gam
  \end{IEEEeqnarray*}
  Das entspricht einer Funktion $f$ aus der vorigen Beschreibung mit der Definition
  $\ffnx{f}{\RR \times \RRpos}{\RR}{(\mu,\sig) \mapsto \mu}$,
  die also aus dem Tupel der beiden Parameter den zu schätzenden heraussucht.
  \par
  Für Klasse~\ref{wtfusion/cha05:3} hätten wir:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{f}{\RR \times \RR \times \RRpos \times \RRpos}{\RR}%
    {(\mu_{\grA},\mu_{\grA}, \sig_{\grA}, \sig_{\grB}) \mapsto \mu_{\grA} - \mu_{\grB}}
  \end{IEEEeqnarray*}
  Wir würden aber von einem CIE zum Niveau~$\gam$ für $\mu_{\grA} - \mu_{\grB}$
  in Klasse~\ref{wtfusion/cha05:3} sprechen.
\end{para}

%%% FIXME Wir müssen besser zwischen bekannten und unbekannten
%%% Parametern unterscheiden können. Es sind ja zum Beispiel auch die
%%% Längen der Tupel bekannt.
