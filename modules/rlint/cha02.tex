\chapter{Hauptsatz der Differential- und Integralrechnung}

\section*{Generalvoraussetzung}

Es seien in diesem Kapitel, wenn nicht anders ausgewiesen,
$a, b \in \RR$ so, dass $a < b$ gilt.

\section{Der Hauptsatz}

\begin{para}[Stammfunktion]
  Es seien $\Om \subseteq \RR$ und $\ffn{f, F}{\Om}{\RR}$.
  Wir nennen $F$ eine \term{Stammfunktion} von $f$ wenn $F$ differenzierbar ist und $F'=f$ gilt.
  (Die Differenzierbarkeit von $F$ schließt für uns ein,
  dass jeder Punkt von $\Om$ Häufungspunkt von $\Om$ ist.
  Das ist insbesondere dann der Fall, wenn $\Om$ ein echtes Intervall ist.)
\end{para}

\begin{satz}[Eindeutigkeit bis auf additive Konstante]
  Es seien $\ffn{f, F, G}{\intcc{a}{b}}{\RR}$ so, dass $F$ eine Stammfunktion von $f$ ist.
  Dann sind äquivalent:
  \begin{enumerate}
  \item\label{rlint/cha02:antiderivative-constant:1}
    $F$ und $G$ sind Stammfunktionen von $f$.
  \item\label{rlint/cha02:antiderivative-constant:2}
    $F$ und $G$ unterscheiden sich nur durch eine additive Konstante,
    d.h., es gibt $c \in \RR$ so, dass $F = G + c$ gilt.
  \end{enumerate}
\end{satz}

\begin{proof}
  \impref{rlint/cha02:antiderivative-constant:1}{rlint/cha02:antiderivative-constant:2}
  Definiere $\phi \df F - G$.
  Dann ist $\phi$ differenzierbar und $\phi' = F' - G' = f - f = 0$.
  Dann ist $\phi$ konstant, d.h. es gibt $c \in \RR$ mit $c = \phi = F - G$.
  \par
  \impref{rlint/cha02:antiderivative-constant:2}{rlint/cha02:antiderivative-constant:1}
  Folgt sofort aus den Ableitungsregeln.
\end{proof}

\begin{satz}[Hauptsatz]
  Es sei $\ffn{f}{\intcc{a}{b}}{\RR}$ integrierbar.
  \begin{enumerate}
  \item Es möge $f$ eine Stammfunktion $F$ haben.
    Dann gilt $\int_a^b f = F(b) - F(a)$.
  \item Definiere:
    \begin{IEEEeqnarray*}{0l}
      \ffnx{F}{\intcc{a}{b}}{\RR}{x \mapsto \int_a^x f}
    \end{IEEEeqnarray*}
    Es sei $\xi \in \intcc{a}{b}$ und $f$ sei stetig in $\xi$.
    Dann ist $F$ differenzierbar in $\xi$, und es gilt $F'(\xi) = f(\xi)$.
    Wenn $f$ stetig ist,
    ist also $F$ eine Stammfunktion von $f$.
  \end{enumerate}
\end{satz}

\begin{proof}
  \begin{enumerate}
  \item Es sei $\veps > 0$ und wähle $\del > 0$ entsprechend der Integrierbarkeit von $f$ bezüglich~$\veps$,
    d.h. $\del$ ist so gewählt, dass gilt:
    \begin{IEEEeqnarray*}{0l}
      \forall (x,\xi) \in \intTP(a,b,\del) \holds
      \abs{R(f,x,\xi) - \int_{a}^{b} f} < \veps
    \end{IEEEeqnarray*}
    Es sei $x \in \intPart(a,b,\del)$, schreibe $n \df \nu(x)$;
    zum Beispiel können wir eine äquidistante Partition wählen wie in \autoref{rlint/cha01:eindeutig}.
    Für alle $j \in \setn{n}$ wählen wir die Stützstelle $\xi_j \in \intcc{x_{j-1}}{x_j}$ so,
    dass gilt:
    \begin{IEEEeqnarray*}{0l}
      F'(\xi_j) = \frac{F(x_j) - F(x_{j-1})}{x_j - x_{j-1}} = \frac{F(x_j) - F(x_{j-1})}{\ell(x,j)}
    \end{IEEEeqnarray*}
    Dies ist möglich durch den Mittelwertsatz.
    Es folgt:
    \begin{IEEEeqnarray*}{0l"s}
      R(f,x,\xi) = \sum_{j=1}^n \ell(x,j) f(\xi_j) \\
      \eqin = \sum_{j=1}^n \ell(x,j) F'(\xi_j) & da $F$ Stammfunktion von $f$ \\
      \eqin = \sum_{j=1}^n  \ell(x,j) \frac{F(x_j) - F(x_{j-1})}{\ell(x,j)} \\
      \eqin = \sum_{j=1}^n F(x_j) - F(x_{j-1}) \\
      \eqin = F(x_n) - F(x_0) & Teleskopsumme \\
      \eqin = F(b) - F(a)
    \end{IEEEeqnarray*}
    Es folgt weiter:
    \begin{IEEEeqnarray*}{0l}
      \abs{F(b) - F(a) - \int_a^b f} = \abs{R(f,x,\xi) - \int_a^b f} < \veps
    \end{IEEEeqnarray*}
    Da dies für alle $\veps > 0$ gilt, folgt die Behauptung.
  \item Definiere:
    \begin{IEEEeqnarray*}{0l}
      \ffnx{\phi}{\intcc{a}{b}}{\RR}{x \mapsto \sup_{u \in \intcc{x}{\xi} \cup \intcc{\xi}{x}} f(u)}
    \end{IEEEeqnarray*}
    Wir zeigen zunächst:
    \begin{IEEEeqnarray}{0l}
      \label{rlint/cha02:flimphi}
      \flim{x}{\xi} \phi(x) = f(\xi)
    \end{IEEEeqnarray}
    Offenbar gilt $\phi(x) \geq \phi(\xi) = f(\xi)$ für alle $x \in \intcc{a}{b}$.
    Nun sei $\veps > 0$ und wähle $\del > 0$ so, dass $f(u) \in B(f(\xi), \frac{\veps}{2})$ gilt,
    insbesondere, $f(u) < f(\xi) + \frac{\veps}{2}$,
    für alle $u \in B(\xi, \del) \cap \intcc{a}{b}$.
    Das ist möglich, da $f$ stetig in $\xi$ ist.
    Es sei $\seq{x_{n}}{n} \in \seqset{\intcc{a}{b}}$ so, dass $\limx{n} x_{n} = \xi$ gilt,
    und definiere $n_0 \df \nzero{}(B(\xi, \del), x)$.
    Dann gilt für alle~${n \geq n_0}$:
    \begin{IEEEeqnarray*}{0l}
      f(\xi) - \veps < f(\xi) \leq \phi(x_n)
      \leq \sup_{u \in B(\xi, \del) \cap \intcc{a}{b}} f(u)
      \leq f(\xi) + \frac{\veps}{2} < f(\xi) + \veps
    \end{IEEEeqnarray*}
    Es folgt $\phi(x_n) \in B(f(\xi), \veps)$.
    Das zeigt~\eqref{rlint/cha02:flimphi}.
    Ebenso können wir $\flim{x}{\xi} \psi(x) = f(\xi)$
    mit $\ffnx{\psi}{\intcc{a}{b}}{\RR}{x \mapsto \inf_{u \in \intcc{x}{\xi} \cup \intcc{\xi}{x}} f(u)}$ zeigen.
    \par
    Für alle $x \in \intcc{a}{b}$ mit $x \neq \xi$ gilt:
    \begin{IEEEeqnarray*}{0l}
      \frac{F(x) - F(\xi)}{x - \xi}
      = \frac{\int_a^x f - \int_a^{\xi} f}{x - \xi}
      = \frac{\int_{\xi}^x f}{x - \xi}
    \end{IEEEeqnarray*}
    Wenn $x > \xi$ gilt, haben wir mit den Standardabschätzungen (\autoref{rlint/cha01:monotone-bounds}):
    \begin{IEEEeqnarray*}{0l}
      \frac{\int_{\xi}^x f}{x - \xi}
      \leq \frac{(x-\xi) \sup_{u \in \intcc{\xi}{x}} f(u)}{x - \xi} = \phi(x) \\
      \frac{\int_{\xi}^x f}{x - \xi}
      \geq \frac{(x-\xi) \inf_{u \in \intcc{\xi}{x}} f(u)}{x - \xi} = \psi(x)
    \end{IEEEeqnarray*}
    Da $\flim{x}{\xi} \phi(x) = \flim{x}{\xi} \psi(x) = f(\xi)$ gilt,
    folgt mit dem Sandwichsatz:
    \begin{IEEEeqnarray*}{0l}
      \flim{x}{\xi^+} \frac{F(x) - F(\xi)}{x - \xi} = \flim{x}{\xi^+} \frac{\int_{\xi}^x f}{x - \xi} = f(\xi)
    \end{IEEEeqnarray*}
    \par
    Der Fall $x < \xi$ kann ähnlich behandelt werden:
    \begin{IEEEeqnarray*}{0l}
      \frac{\int_{\xi}^x f}{x - \xi}
      = \frac{\int_{x}^{\xi} f}{\xi - x} \leq \frac{(\xi-x) \sup_{u \in \intcc{x}{\xi}} f(u)}{\xi - x}
      = \phi(x) \\
      \frac{\int_{\xi}^x f}{x - \xi}
      = \frac{\int_{x}^{\xi} f}{\xi - x} \geq \frac{(\xi-x) \inf_{u \in \intcc{x}{\xi}} f(u)}{\xi - x}
      = \psi(x)
    \end{IEEEeqnarray*}
    Es folgt $\flim{x}{\xi^-} \frac{F(x) - F(\xi)}{x - \xi} = f(\xi)$.
    Insgesamt haben wir gezeigt, dass $F$ differenzierbar in $\xi$ ist und $F'(\xi) = f(\xi)$ gilt.\qedhere
  \end{enumerate}
\end{proof}

\begin{table}[t]
  \centering
  \setlength{\extrarowheight}{2pt}
  \begin{tabular}{lllll}
    \toprule
    Domain & $f(x)$ & $F(x)$ & Parameter \\
    \midrule
    $\RR$ & $c$ & $cx$ & $c \in \RR$ \\
    $\RR$ & $ax+b$ & $\frac{a}{2} x^2 + bx$ & $a,b \in \RR$ \\
    $\RR$ & $\sum_{k=0}^n a_k x^k$ & $\sum_{k=0}^n \frac{a_k}{k+1} x^{k+1}$ & $\eliz{a}{n} \in \RR$ \\
    $\RRnn$ & $x^\al$ & $\frac{1}{\al+1} x^{\al+1}$ & $\al \in \RRpos$ \\
    $\RRpos$ & $x^{-\al}$ & $\frac{1}{1-\al} x^{1-\al}$ & $\al \in \RRpos \setminus \set{1}$ \\
    $\RRpos$ & $\frac{1}{x}$ & $\ln(x)$ \\
    $\RR$ & $e^x$ & $e^x$ \\
    $\RR$ & $b^x$ & $\frac{b^x}{\ln(b)}$ & $b \in \RRpos \setminus \set{1}$ \\
    $\RRpos$ & $\ln(x)$ & $x \ln(x) - x$ \\
    $\RRpos$ & $\log_b(x)$ & $\tfrac{x \ln(x) - x}{\ln(b)}$ & $b \in \RRpos \setminus \set{1}$ \\
    $\RR$ & $\cos(x)$ & $\sin(x)$ \\
    $\RR$ & $\sin(x)$ & $-\cos(x)$ \\
    $\intoo{{-1}}{1}$ & $\frac{-1}{\sqrt{1-x^2}}$ & $\acos(x)$ \\
    $\intoo{{-1}}{1}$ & $\frac{1}{\sqrt{1-x^2}}$ & $\asin(x)$ \\
    $\RR$ & $\frac{1}{1+x^2}$ & $\atan(x)$ \\
    \bottomrule
  \end{tabular}
  \caption{Gundlegende Funktionen $f$ mit Stammfunktion $F$.}%
  \label{rlint/cha02:antiderivatives}%
\end{table}

\section{Integrationstechniken}

Der Hauptsatz hilft uns in vielen Fällen,
die Berechnung von Integralen auf das Finden von Stammfunktionen zu reduzieren.
Wir geben Stammfunktionen einiger grundlegender Funktionen in \autoref{rlint/cha02:antiderivatives} an.
Man kann diese leicht durch Differenzieren beweisen;
für $\acos$ und $\asin$ verwendet man dabei den Satz
über die Ableitung der Umkehrfunktion.
Bei zusammengesetzten Funktionen ist das Finden von Stammfunktionen nicht ganz so geradlinig
wie das Differenzieren.
Wir lernen zwei wichtige Techniken im Rest in diesem Abschnitt kennen.

\begin{para}[Notation]
  Es seien $u,v \in \RR$.
  Für eine Funktion $\phi$ mit $u,v \in \dom(\phi)$
  schreiben wir $\fneval{\phi}{u}{v} \df \phi(v) - \phi(u)$.
  Der erste Teil des Hauptsatzes kann nun als $\int_a^b f = \fneval{F}{a}{b}$ geschrieben werden.
  Es steht $\fneval{E}{x=u}{v}$ für die Zahl, die sich folgendermaßen ergibt:
  In dem an Stelle von $E$ stehenden Ausdruck ersetze jedes freie Vorkommen des Symbols $x$ durch~$v$,
  erhalte eine Zahl;
  dann ersetze jedes freie Vorkommen das Symbols $x$ in $E$ durch~$u$ und erhalte wieder eine Zahl;
  dann ziehe diese zweite Zahl von der ersten ab.
  Zum Beispiel ist:
  \begin{IEEEeqnarray*}{0l}
    \fneval{x^2 + \sin(x)}{x=u}{v} = v^2 + \sin(v) - (u^2 + \sin(u))
  \end{IEEEeqnarray*}
  Natürlich kann wie immer anstelle von $x$ auch ein anderes Symbol verwendet werden.
  Wie auch bei der in \autoref{rlint/cha01:notation} vorgestellten Schreibweise gilt:
  Wenn das Symbol (hier $x$) frei ist, kann man die Schreibweise vereinfachen,
  hier zu~$\fneval{E}{u}{v}$.
  \par
  Bemerke, dass für alle $c \in \RR$
  gilt $\fneval{c E}{x=u}{v} = c \fneval{E}{x=u}{v}$.
\end{para}

Unsere erste Integrationstechnik ist sozusagen \enquote{Produktregel rückwärts}:

\begin{satz}[Partielle Integration]
  Es seien $\ffn{f,g}{\intcc{a}{b}}{\RR}$ stetig differenzierbar. Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    \int_a^b f g' = \fneval{fg}{a}{b} - \int_a^b f'g
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}
  Es sind $fg'$ und $f'g$ stetig und somit integrierbar,
  so dass alle vorkommenden Integrale definiert sind.
  Es gilt $(fg)' = fg' + f'g$, also ist $fg$ eine Stammfunktion von $fg' + f'g$.
  Es folgt mit dem Hauptsatz:
  \begin{IEEEeqnarray*}{0l+x*}
    \fneval{f g}{a}{b} = \int_a^b (fg' + f'g) = \int_a^b f g' + \int_a^b f' g
    & \qedarray[\qedhere]{1}%
  \end{IEEEeqnarray*}
\end{proof}

\begin{example}
  Es gilt:
  \begin{IEEEeqnarray*}{0l}
    \int_a^b x \cos(x) \de x = \fneval{x \sin(x)}{x=a}{b} - \int_a^b \sin(x) \de x \\
    \eqin = \fneval{x \sin(x)}{x=a}{b} - \fneval{-\cos(x)}{x=a}{b}
    = \fneval{x \sin(x) + \cos(x)}{x=a}{b}
  \end{IEEEeqnarray*}
  Mit konkreten Integrationsgrenzen als Beispiel:
  \begin{IEEEeqnarray*}{0l}
    \int_0^1 x \cos(x) \de x = \fneval{x \sin(x) + \cos(x)}{x=0}{1} \\
    \eqin = \sin(1) + \cos(1) - (0 + \cos(0)) = \sin(1) + \cos(1) - 1 \approx 0.3818
  \end{IEEEeqnarray*}
\end{example}

\begin{example}
  Es gilt:
  \begin{IEEEeqnarray*}{0l}
    \int_a^b \cos(x) \sin(x) \, \de x
    = \fneval{\sin(x) \sin(x)}{x=a}{b} - \int_a^b \sin(x) \cos(x) \, \de x
  \end{IEEEeqnarray*}
  Es folgt $2 \int_a^b \cos(x) \sin(x) \, \de x = \fneval{\sin^2(x)}{x=a}{b}$, also:
  \begin{IEEEeqnarray*}{0l}
    \int_a^b \cos(x) \sin(x) \, \de x = \fneval{\tfrac{1}{2} \sin^2(x)}{x=a}{b}
  \end{IEEEeqnarray*}
  Mit konkreten Integrationsgrenzen als Beispiel:
  \begin{IEEEeqnarray*}{0l}
    \int_0^1 \cos(x) \sin(x) \, \de x
    = \fneval{\tfrac{1}{2} \sin^2(x)}{x=0}{1} = \tfrac{1}{2} \sin^2(1) - 0 \approx 0.3540
  \end{IEEEeqnarray*}
\end{example}

Unsere zweite Integrationstechnik ist sozusagen \enquote{Kettenregel rückwärts}:

\begin{satz}[Substitutionsregel]
  \label{rlint/cha02:subst}
  Es sei $f$ eine stetige reelle Funktion.
  Es sei $\phi$ eine stetig differenzierbare reelle Funktion und
  $u,v \in \dom(\phi)$ so,
  dass $I \df \intcc{u}{v} \cup \intcc{v}{u} \subseteq \dom(\phi)$ und $\fnimg{\phi}(I) \subseteq \dom(f)$ gilt.
  Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    \int_{\phi(u)}^{\phi(v)} f(x) \de x
    = \int_{u}^{v} f(\phi(x)) \ccdot \phi'(x) \de x
  \end{IEEEeqnarray*}
  In konziser Notation:
  \begin{IEEEeqnarray*}{0l}
    \int_{\phi(u)}^{\phi(v)} f
    = \int_{u}^{v} (f \circ \phi) \ccdot \phi'
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}
  Wir stellen zunächst sicher, dass die Integrale definiert sind.
  Definiere $\psi \df (f \circ \fnres{\phi}{I}) \ccdot \phi'$.
  Die Komposition $f \circ \fnres{\phi}{I}$ ist definiert wegen ${\fnimg{\phi}(I) \subseteq \dom(f)}$.
  Ferner ist $\psi$ stetig, also integrierbar.
  Also ist das rechte Integral definiert.
  \par
  Definiere $J \df \intcc{\phi(u)}{\phi(v)} \cup \intcc{\phi(v)}{\phi(u)}$.
  Dann ist $J \subseteq \dom(f)$ wegen $\phi(u), \phi(v) \in \fnimg{\phi}(I) \subseteq \dom(f)$
  und da $\fnimg{\phi}(I)$ ein Intervall ist.
  Außerdem ist $f$ stetig.
  Das linke Integral ist also auch definiert.
  \par
  Wir kommen zur Gleichheit der beiden Integrale.
  Nach dem Hauptsatz hat $\fnres{f}{J}$ eine Stammfunktion~$F$.
  Nach der Kettenregel ist $F \circ \fnres{\phi}{I}$ eine Stammfunktion von~$\psi$.
  Daraus folgt mit zweimaliger Anwendung des Hauptsatzes:
  \begin{IEEEeqnarray*}{0l+x*}
    \int_{u}^{v} (f \circ \phi) \ccdot \phi'
    = \int_{u}^{v} \psi
    = \fneval{F \circ \phi}{u}{v}
    = \fneval{F}{\phi(u)}{\phi(v)}
    = \int_{\phi(u)}^{\phi(v)} f
    & \qedarray[\qedhere]{1}%
  \end{IEEEeqnarray*}
\end{proof}

\begin{example}
  Es seien $a,b \in \RR$ mit $a \leq b$. Es gilt:
  \begin{IEEEeqnarray*}{0l}
    \int_a^b x \, \sin\parens{x^2} \de x
    = \frac{1}{2} \int_a^b \sin\parens{x^2} \parens{2x} \, \de x
    = \frac{1}{2} \int_{a^2}^{b^2} \sin\parens{x} \de x
    = \frac{1}{2} \fneval{{-\cos(x)}}{x=a^2}{b^2}
  \end{IEEEeqnarray*}
  Wir haben hier die Substitutionsregel von rechts nach links verwendet
  mit $\ffnx{\phi}{\RR}{\RR}{x \mapsto x^2}$ und $u=a$ und $v=b$.
  Wenn wir dieses Ergebnis durch Differenzieren überprüfen möchten,
  dann müssen wir darauf achten, die richtige Funktion abzuleiten.
  Diese ist nicht $\fnx{\RR}{\RR}{x \mapsto -\frac{1}{2}\cos(x)}$,
  sondern $\fnx{\RR}{\RR}{x \mapsto -\frac{1}{2}\cos(x^2)}$.
  Diese erhalten wir, indem wir das Ergebnis in die Form $\fneval{ E }{x=a}{b}$ bringen.
\end{example}

\begin{example}
  Wir berechnen $\int_a^b e^{\sqrt{x}} \de x$ mit $0 \leq a < b$.
  Dieses Mal wenden wir die Substitutionsregel von links nach rechts an
  mit $\ffnx{\phi}{\RR}{\RR}{x \mapsto x^2}$ und $u=\sqrt{a}$ und $v=\sqrt{b}$.
  Es gilt:
  \begin{IEEEeqnarray*}{0l}
    \int_a^b e^{\sqrt{x}} \de x
    = \int_{\phi(\sqrt{a})}^{\phi(\sqrt{b})} e^{\sqrt{x}} \de x
    = \int_{\sqrt{a}}^{\sqrt{b}} e^{\sqrt{\phi(x)}} \phi'(x) \de x
    = 2 \int_{\sqrt{a}}^{\sqrt{b}} e^{x} x \de x
  \end{IEEEeqnarray*}
  Wir fahren mit partieller Integration fort (den Faktor $2$ fortlassend):
  \begin{IEEEeqnarray*}{0l}
    \int_{\sqrt{a}}^{\sqrt{b}} e^{x} x \de x
    = \fneval{e^x x}{x=\sqrt{a}}{\sqrt{b}} - \int_{\sqrt{a}}^{\sqrt{b}} e^x \de x \\
    \eqin = \fneval{e^x x}{x=\sqrt{a}}{\sqrt{b}} - \fneval{e^x}{x=\sqrt{a}}{\sqrt{b}}
    = \fneval{e^x (x-1)}{x=\sqrt{a}}{\sqrt{b}}
  \end{IEEEeqnarray*}
\end{example}

\section{Uneigentliche Integrale}

Unser Integriebarkeitsbegriff bezieht sich nur auf Funktionen,
deren Domain ein kompaktes Intervall ist, d.h. $\intcc{a}{b}$ mit $a,b \in \RR$.
Dabei wird es auch bleiben.
Eine Erweiterung ergibt sich durch die uneigentlichen Integrale,
die wir in diesem Abschnitt besprechen.

\begin{para}[Verschiedene Formen des uneigentlichen Integrals]
  \hfill
  \begin{enumerate}
  \item Kritische Grenze rechts:
    Es seien $a \in \RR$ und $b \in \intoI{a}$ und $\ffn{f}{\intco{a}{b}}{\RR}$.
    Es möge $f$ integrierbar über alle kompakten Teilintervalle von $\intco{a}{b}$ sein.\footnote{%
      Dies ist äquivalent dazu,
      dass $f$ integrierbar von $a$ nach $c$ für alle $c \in \dom(f)$ ist,
      d.h. dass $\fnres{f}{\intcc{a}{c}}$ integrierbar ist für alle $c \in \dom(f)$.}
    Wir definieren das \term{uneigentliche Integral über $f$}, wenn der Limes existiert:
    \begin{IEEEeqnarray*}{0l}
      \int f \df \int_{a}^b f \df \flim{r}{b} \int_{a}^r f
    \end{IEEEeqnarray*}
    Wir sagen, dass das \term{uneigentliche Integral über $f$ konvergiert}, wenn $\int_{a}^b f \in \RR$ gilt.
    Etwas salopp sagt man dazu auch, dass $\int_{a}^b f$ konvergiert.
    \par
    Für alle $\al \in \dom(f)$ meinen wir mit $\int_{\al}^b f$
    das uneigentliche Integral über $\fnres{f}{\intco{\al}{b}}$.
    Etwas salopp sagen wir, dass $\int \fnres{f}{\intco{\al}{b}}$ konvergiert,
    wenn das uneigentliche Integral über $\fnres{f}{\intco{\al}{b}}$ konvergiert.
    \par
    Die Konvergenz des uneigentlichen Integrals $\int_{a}^b f$ ist klar,
    wenn $b \in \RR$ gilt und $f$ nach $b$ integrierbar fortsetzbar ist,
    etwa zu einer integrierbaren Funktion $\ffn{\tilde{f}}{\intcc{a}{b}}{\RR}$.
    Denn dann gilt nach der stetigen Abhängigkeit von der Integralgrenze (\autoref{rlint/cha01:stetig}),
    dass $\flim{r}{b} \int_{a}^r f = \int_{a}^b \tif$.
    Zum Beispiel konvergiert $\int_{0}^{1} e^{-\frac{1}{x}} \de x$,
    da $\flim{x}{0} e^{-\frac{1}{x}} = 0$ gilt
    und daher der Integrand nach $0$ integrierbar (sogar stetig) fortsetzbar ist.
  \item Kritische Grenze links:
    Analog zum vorigen Punkt, aber für Funktionen
    $\ffn{f}{\intoc{a}{b}}{\RR}$
    mit $b \in \RR$ und $a \in \intIo{b}$.
  \item Beide Grenzen kritisch:
    Es seien $a, b \in \RRbar$ so, dass $a < b$ gilt,
    und es sei $\ffn{f}{\intoo{a}{b}}{\RR}$.
    Es möge $f$ integrierbar über alle kompakten Teilintervalle von $\intoo{a}{b}$ sein.
    Wenn es $c \in \intoo{a}{b}$ so gibt,
    dass das uneigentliche Integral über $\fnres{f}{\intoc{a}{c}}$
    und über $\fnres{f}{\intco{c}{b}}$ konvergiert,
    dann sagen wir,
    dass das \term{uneigentliche Integral über $f$ konvergiert}
    und definieren das \term{uneigentliche Integral über $f$}:
    \begin{IEEEeqnarray*}{0l}
      \int f \df \int_{a}^{b} f \df \int_{a}^c f + \int_{c}^b f
    \end{IEEEeqnarray*}
    Man kann leicht zeigen, dass dies eine sinnvolle Definition ist,
    nämlich sie hängt nicht von der Wahl von $c$ ab.
    Genauer gilt (Übung):
    \par
    Wenn es $c \in \intoo{a}{b}$ so gibt,
    dass das uneigentliche Integral über $\fnres{f}{\intoc{a}{c}}$
    und über $\fnres{f}{\intco{c}{b}}$ konvergiert,
    dann gilt:
    Für alle $d \in \intoo{a}{b}$
    konvergiert das uneigentliche Integral über $\fnres{f}{\intoc{a}{d}}$
    und über $\fnres{f}{\intco{d}{b}}$,
    und es gilt:
    \begin{IEEEeqnarray*}{0l}
      \int_{a}^c f + \int_{c}^b f = \int_{a}^d f + \int_{d}^b f
    \end{IEEEeqnarray*}
  \end{enumerate}
\end{para}

\begin{satz}[Uneigentliches Integral über $\frac{1}{x^{\al}}$]
  Es sei $\al > 0$.
  Dann gilt:
  \begin{enumerate}
  \item Wenn $\al > 1$, dann $\int_1^{\pinfty} \frac{1}{x^\al} \de x = \frac{1}{\al - 1}$,
    und wenn $0 < \al \leq 1$, dann divergiert das uneigentliche Integral.
  \item Wenn $0 < \al < 1$, dann $\int_0^1 \frac{1}{x^\al} \de x = \frac{1}{1 - \al}$,
    und wenn $\al \geq 1$, dann divergiert das uneigentliche Integral.
  \end{enumerate}
  \begin{proof}
    \begin{enumerate}
    \item Für alle $r > 1$ gilt:
      \begin{IEEEeqnarray*}{0l}
        \int_1^{r} \frac{1}{x^\al} \de x
        = \begin{cases}
          \frac{1}{1-\al} \fneval{x^{1-\al}}{x=1}{r} & \al \neq 1 \\
          \fneval{\ln(x)}{x=1}{r} & \al = 1
        \end{cases}
        = \begin{cases}
          \frac{1}{1-\al} (r^{1-\al} - 1) & \al \neq 1 \\
          \ln(r) & \al = 1
        \end{cases}
      \end{IEEEeqnarray*}
      Die Behauptung folgt, denn:
      \begin{IEEEeqnarray*}{0l+l}
        \flim{r}{+\infty} r^{1-\al}
        = \begin{cases}
          +\infty & \al < 1 \\
          0 & \al > 1
        \end{cases}
        &
        \flim{r}{+\infty} \ln(r) = +\infty
      \end{IEEEeqnarray*}
    \item Für alle $0 < r < 1$ gilt:
      \begin{IEEEeqnarray*}{0l}
        \int_r^{1} \frac{1}{x^\al} \de x
        = \begin{cases}
          \frac{1}{1-\al} \fneval{x^{1-\al}}{x=r}{1} & \al \neq 1 \\
          \fneval{\ln(x)}{x=r}{1} & \al = 1
        \end{cases}
        = \begin{cases}
          \frac{1}{1-\al} (1 - r^{1-\al}) & \al \neq 1 \\
          -\ln(r) & \al = 1
        \end{cases}
      \end{IEEEeqnarray*}
      Die Behauptung folgt, denn:
      \begin{IEEEeqnarray*}{0l+l+x*}
        \flim{r}{0^+} r^{1-\al}
        = \begin{cases}
          0 & \al < 1 \\
          +\infty & \al > 1
        \end{cases}
        &
        \flim{r}{0^+} \ln(r) = -\infty
        & \qedarray[\qedhere]{1}%
      \end{IEEEeqnarray*}
    \end{enumerate}
  \end{proof}
\end{satz}

\begin{para}[Unkritische Grenze egal für Konvergenz]
  \label{rlint/cha02:var-grenze}
  Es seien $a \in \RR$ und $b \in \intoI{a}$ und $\ffn{f}{\intco{a}{b}}{\RR}$.
  Es möge $f$ integrierbar über alle kompakten Teilintervalle von $\intco{a}{b}$ sein.
  Es seien $u,v \in \dom(f)$.
  Dann konvergiert $\int_{u}^{b} f$ genau dann, wenn $\int_{v}^{b} f$ konvergiert.
  Das sieht man leicht über den Aneinanderstückelungssatz.
  Aus obigem Satz folgt also zum Beispiel, dass $\int_{u}^{\pinfty} \frac{1}{x^{\al}} \de x$
  konvergiert für alle $u > 0$ und alle $\al > 1$.
  \par
  Eine analoge Aussage gilt mit linker kritischer Grenze.
  Wir betrachten die Situation mit zwei kritischen Grenzen.
  Es seien $a, b \in \RRbar$ so, dass $a < b$ gilt,
  und es sei $\ffn{f}{\intoo{a}{b}}{\RR}$.
  Es möge $f$ integrierbar über alle kompakten Teilintervalle von $\intoo{a}{b}$ sein.
  Es seien $u,v \in \dom(f)$.
  Dann konvergiert $\int_{a}^{b} f$ genau dann,
  wenn $\int_{a}^{u} f$ und $\int_{v}^{b} f$ konvergieren.
\end{para}

\begin{para}[Berechnung über Limes]
  Es seien $a, b \in \RRbar$ so, dass $a < b$ gilt,
  und es sei $\ffn{f}{\intoo{a}{b}}{\RR}$.
  Es möge das uneigentliche Integral über $f$ konvergieren.
  Es seien $\seq{r_{n}}{n}, \seq{s_{n}}{n} \in \seqset{\intoo{a}{b}}$ so, dass
  $\limx{n} r_{n} = a$ und $\limx{n} s_{n} = b$ gilt.
  Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    \int_{a}^{b} f = \limx{n} \int_{r_{n}}^{s_{n}} f
  \end{IEEEeqnarray*}
  \emphasis{Vorsicht:}
  Dies funktioniert nicht als hinreichendes Kriterium für die Konvergenz
  des uneigentlichen Integrals!
  Zum Beispiel haben wir $\limx{n} \int_{-n}^{n} x \de x = 0$.
  Aber $\int_{a}^{\pinfty} x \de x = \pinfty$ für alle $a \in \RR$,
  also konvergiert $\int_{\ninfty}^{\pinfty} x \de x$ nicht.
\end{para}

Ohne Beweis geben wir:

\begin{satz}[Vergleichskriterium]
  Es seien $a \in \RR$ und $b \in \intoI{a}$.
  Es seien $\ffn{f,g}{\intco{a}{b}}{\RR}$ integrierbar
  über alle kompakten Teilintervalle von $\intco{a}{b}$.
  Es gelte $\abs{f} \leq g$,
  und das uneigentliche Integral über $g$ möge konvergieren.
  Dann konvergieren auch die uneigentlichen Integrale über $f$ und über $\abs{f}$.
  Eine entsprechende Aussage gilt bei linker kritischer Grenze.
\end{satz}

\begin{example}
  \begin{enumerate}
  \item Wir betrachten $\int_{1}^{\pinfty} \frac{\cos(x)}{x^{2}} \de x$.
    Es gilt $\abs{\frac{\cos(x)}{x^{2}}} \leq \frac{1}{x^{2}}$ für alle ${x \in \intci{1}}$,
    und $\int_{1}^{\pinfty} \frac{1}{x^{2}} \de x$ konvergiert.
    Also konvergiert $\int_{1}^{\pinfty} \frac{\cos(x)}{x^{2}} \de x$.
  \item Es seien $a,b > 0$, und wir betrachten $\int_{\ninfty}^\pinfty \frac{\cos(a x)}{x^2 + b^2} \de x$.
    Es gilt $\abs{\frac{\cos(a x)}{x^2 + b^2}} \leq \frac{1}{x^2}$ für alle $x \in \RRnz$.
    Also konvergieren $\int_{1}^\pinfty \frac{\cos(a x)}{x^2 + b^2} \de x$
    und $\int_{\ninfty}^{-1} \frac{\cos(a x)}{x^2 + b^2} \de x$.
    Also konvergiert $\int_{\ninfty}^\pinfty \frac{\cos(a x)}{x^2 + b^2} \de x$.
    (Beachte \autoref{rlint/cha02:var-grenze}.)
  \end{enumerate}
\end{example}
