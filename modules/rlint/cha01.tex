\chapter{Integrierbarkeit}

\section*{Notation}

\begin{itemize}
\item Für alle Mengen $M$ bezeichnen wir mit $\seqset{M}$ die Menge aller Folgen in $M$
  mit beliebigem Startindex,
  also $\seqset{M} = \bigcup_{n \in \ZZ} M^{\ZZ_{\geq n}}$.
\item Für alle $\veps > 0$ und alle $x \in \RR$ definieren wir die \term{offene Kugel}
  in $\RR$ mit Radius $\veps$ um $x$ als:
  \begin{IEEEeqnarray*}{0l}
    B(x, \veps) \df \set{y \in \RR \suchthat \abs{y-x} < \veps}
  \end{IEEEeqnarray*}
\item Es sei $\seq{x_{n}}{n} \in \seqset{\RR}$ konvergent, notiere $p \df \limx{n} x_{n}$.
  Für alle $\veps > 0$ gibt es dann $n_{0}$ so,
  dass $x_{n} \in B(p, \veps)$ für alle $n \geq n_{0}$ gilt.
  Ein beliebiges solches $n_{0}$ fixieren wir als $\nzero{}(B(p, \veps), \seq{x_{n}}{n})$
  oder auch kurz als $\nzero{}(B(p, \veps), x)$.
\item Es sei $\ffn{f}{A}{B}$ eine Funktion und $M \subseteq A$.
  Dann schreiben wir $\fnimg{f}(M) \df \set{f(x) \suchthat x \in M}$
  für die \term{Bildmenge} von $M$ unter $f$.
  (In der Literatur ist auch einfach $f(M)$ dafür üblich,
  was aber zu Missverständnissen führen kann.)
\end{itemize}

\section*{Generalvoraussetzung}

Es seien in diesem Kapitel, wenn nicht anders ausgewiesen,
$a, b \in \RR$ so, dass $a < b$ gilt.

\section{Definition}

\begin{para}[Riemann\-/Summe]
  Für alle $n \in \NNone$ nennen wir $x = (\eliz{x}{n}) \in \intcc{a}{b}^{n+1}$
  eine \term{Partition} von $\intcc{a}{b}$ mit $n$ \term{Teilintervallen}, wenn gilt:
  \begin{IEEEeqnarray*}{0l}
    a = x_0 < x_1 < \hdots < x_n = b
  \end{IEEEeqnarray*}
  Für alle $j \in \setn{n}$ nennen wir $\intcc{x_{j-1}}{x_{j}}$ das \xte{j} \term{Teilintervall} der Partition
  und $\ell(x,j) \df x_j - x_{j-1}$ die \term{Länge} des \xten{j} Teilintervalls der Partition.
  Wir nennen $\mu(x) \df \max_{j \in \setn{n}} \ell(x,j)$ die \term{Feinheit} der Partition.
  Mit $\intPart(a,b)$ bezeichnen wir die Menge aller Partitionen von $\intcc{a}{b}$.
  Für alle $\del > 0$ bezeichnen wir mit $\intPart(a,b,\del)$
  die Menge aller Partitionen von $\intcc{a}{b}$,
  deren Feinheit weniger als $\del$ ist;
  es gilt insbesondere $\intPart(a,b) = \bigcup_{\del > 0} \intPart(a,b,\del)$.
  \par
  Ist zu $x \in \intPart(a,b)$ mit $n$ Teilintervallen
  ein ${\xi = (\eli{\xi}{n})} \in \intcc{a}{b}^n$ so gegeben,
  dass $\xi_j \in \intcc{x_{j-1}}{x_j}$ für alle $j \in \setn{n}$ gilt,
  dann nennen wir $\xi$ ein Tupel von \term{Stützstellen} für $x$
  und $(x,\xi)$ eine \term{Partition mit Stützstellen} von $\intcc{a}{b}$ mit $n$ Teilintervallen.
  Mit $\intTP(a,b)$ bezeichnen wir die Menge aller Partitionen mit Stützstellen von $\intcc{a}{b}$.
  Für alle $\del > 0$ bezeichnen wir mit $\intTP(a,b,\del)$
  die Menge aller Partitionen mit Stützstellen von $\intcc{a}{b}$,
  deren Feinheit weniger als $\del$ ist;
  es gilt insbesondere $\intTP(a,b) = \bigcup_{\del > 0} \intTP(a,b,\del)$.
  \par
  Es sei $\ffn{f}{\intcc{a}{b}}{\RR}$.
  Für alle $(x,\xi) \in \intTP(a,b)$ mit $n$ Teilintervallen definiere
  die \term{Riemann\-/Summe} von $f$ bezüglich $(x,\xi)$ als:
  \begin{IEEEeqnarray*}{0l}
    R(f,x,\xi) \df \sum_{j=1}^{n} \ell(x,j) f(\xi_j)
  \end{IEEEeqnarray*}
  Wir nennen $f$ \term{Riemann\-/integrierbar} oder kurz \term{integrierbar}, wenn gilt:
  \begin{IEEEeqnarray}{0l}
    \label{rlint/cha01:intbar}
    \exists R_{0} \in \RR \innerholds
    \forall \veps > 0 \innerholds
    \exists \del > 0 \innerholds
    \forall (x,\xi) \in \intTP(a,b,\del) \holds
    \abs{R(f,x,\xi) - R_0} < \veps
  \end{IEEEeqnarray}
\end{para}

\begin{para}[Eindeutigkeit von $R_{0}$]
  \label{rlint/cha01:eindeutig}
  Die Zahl $R_{0}$ aus \eqref{rlint/cha01:intbar} ist eindeutig bestimmt
  (wenn sie existiert).
  Es seien dazu $R_{0}$ und $R_{1}$ so, dass für alle $i \in \OI$ gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall \veps > 0 \innerholds
    \exists \del > 0 \innerholds
    \forall (x,\xi) \in \intTP(a,b,\del) \holds
    \abs{R(f,x,\xi) - R_i} < \veps
  \end{IEEEeqnarray*}
  Es sei $\veps > 0$.
  Für alle $i \in \OI$ wähle $\del_{i}$ entsprechend der obigen Eigenschaft,
  also $\del_{i}$ ist so gewählt, dass
  \begin{IEEEeqnarray}{0l}
    \label{rlint/cha01:eq-1}
    \forall (x,\xi) \in \intTP(a,b,\del_{i}) \holds
    \abs{R(f,x,\xi) - R_i} < \veps
  \end{IEEEeqnarray}
  Definiere $\del \df \min\set{\del_{0}, \del_{1}}$ und $n \df \ceil{\frac{b-a}{\del}} + 1$
  und $\del' \df \frac{b-a}{n} < \frac{b-a}{\,\frac{b-a}{\del}\,} = \del$.
  Definiere eine Partition $x$ via $x_{j} \df a + j\del'$ für alle $j \in \setft{0}{n}$,
  und definiere Stützstellen $\xi$ via $\xi_{j} \df a + j\del'$ für alle $j \in \setn{n}$.
  Dann gilt $(x,\xi) \in \intTP(a,b,\del)$ und somit,
  da $\del \leq \del_{0}, \del_{1}$ gilt, nach \eqref{rlint/cha01:eq-1}:
  \begin{IEEEeqnarray*}{0l}
    \abs{R_{0}-R_{1}}
    = \abs{R_{0}-R(f,x,\xi)+R(f,x,\xi)-R_{1}} \\
    \eqin \leq \abs{R_{0}-R(f,x,\xi)}+\abs{R(f,x,\xi)-R_{1}}
    < \frac{\veps}{2} + \frac{\veps}{2} = \veps
  \end{IEEEeqnarray*}
  Da dies für alle $\veps > 0$ gilt, folgt $R_{0} = R_{1}$.
  \par
  Nebenbei haben wir gezeigt, dass $\intTP(a,b,\del) \neq \emptyset$ gilt
  für alle $\del > 0$.
\end{para}

\begin{para}[Integral]
  Wenn $f$ integrierbar ist,
  dann nennen wir die eindeutig bestimmte Zahl $R_{0}$ aus \eqref{rlint/cha01:intbar}
  das \term{Riemann\-/Integral} oder kurz \term{Integral} von $f$
  und schreiben $\int f$ dafür.
\end{para}

\begin{para}[Notation für das Integral]
  \label{rlint/cha01:notation}
  Möchte man die Grenzen der Domain von $f$ hervorheben,
  so schreibt man $\int_a^b f$ anstelle von $\int f$.
  In so einem Konstrukt nennen wir $a$ die \term{untere Integralgrenze},
  und $b$ die \term{obere Integralgrenze};
  wir nennen die Funktion $f$ den \term{Integranden},
  und wir sagen, dass diese Funktion \term{integriert} wird.
  Das Hervorgeben der sogenannten Integralgrenzen wird besonders relevant werden,
  wenn wir in Kürze (in \autoref{rlint/cha01:teilbereich}) Integrale über Teilbereiche der Domain betrachten.
  \par
  Üblich ist auch die Schreibweise $\int_a^b f(x) \de x$,
  oder mit einem anderen Symbol anstelle von $x$,
  zum Beispiel $\int_a^b f(u) \de u$.
  Diese Notation ist hilfreich, wenn die Funktion keinen Namen hat.
  Es sei $E$ ein Ausdruck,
  der für alle $\hatx \in \intcc{a}{b}$ zu einer reellen Zahl wird,
  wenn man in $E$ jedes freie Vorkommen des Symbols $x$ durch $\hatx$ ersetzt.
  Dann schreiben wir:
  \begin{IEEEeqnarray*}{0l}
    \int_{a}^{b} E \de x \df \int (\fnx{\intcc{a}{b}}{\RR}{x \mapsto E})
  \end{IEEEeqnarray*}
  Zum Beispiel ist~$\int_a^b x^2 \de x$ das Integral der Funktion $\fnx{\intcc{a}{b}}{\RR}{x \mapsto x^{2}}$
  (wenn diese integrierbar ist, was wir später einsehen werden).
  Auch alles das geht natürlich auch mit einem anderen Symbol anstelle von $x$,
  zum Beispiel ist:
  \begin{IEEEeqnarray*}{0l}
    \int_a^b x^2 \de x =
    \int_a^b u^2 \de u =
    \int_a^b t^2 \de t =
    \int_a^b \vtet^2 \de \vtet =
    \int_a^b \gamma^2 \de \gamma = \hdots
  \end{IEEEeqnarray*}
  Wenn das Symbol (hier $x, u, t, \vtet, \gamma, \hdots$)
  im gegebenen Kontext frei ist,
  so besteht kaum eine Gefahr von Missverständnissen,
  wenn man das $\de \hdots$ fortlässt.
  Obiges sieht dann so aus:
  \begin{IEEEeqnarray*}{0l}
    \int_a^b x^2 =
    \int_a^b u^2 =
    \int_a^b t^2 =
    \int_a^b \vtet^2 =
    \int_a^b \gamma^2
  \end{IEEEeqnarray*}
\end{para}

\section{Erste Kriterien}

\begin{satz}[Beschränktheitskriterium]
  \label{rlint/cha01:int-bounded}
  Integrierbare Funktionen sind beschränkt.
\end{satz}

\begin{proof}
  Es sei $\ffn{f}{\intcc{a}{b}}{\RR}$ integrierbar.
  Für Widerspruch nehme an, $f$ ist nicht beschränkt.
  Es sei $\del > 0$, und es sei $\del' \leq \del$ klein genug, so dass gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall (x,\xi) \in \intTP(a,b,\del') \holds \abs{R(f,x,\xi) - \int f} < 1
  \end{IEEEeqnarray*}
  Es sei $(x,\xi) \in \intTP(a,b,\del')$ mit $n$ Teilintervallen.
  Definiere $M \df \max_{j \in \setn{n}} \abs{f(\xi_j)}$.
  Aufgrund der Unbeschränktheit gibt es $u \in \intcc{a}{b}$ so,
  dass $\abs{f(u)} > M + \frac{2}{\mu(x)}$ gilt.
  Es folgt:
  \begin{IEEEeqnarray*}{0l}
    \forall j \in \setn{n} \holds \abs{f(u) - f(\xi_j)} > \frac{2}{\mu(x)}
  \end{IEEEeqnarray*}
  Es sei $k$ der Index eines Teilintervalls von $x$, der $u$ enthält,
  und definiere $\eta_k \df u$ und $\eta_j \df \xi_j$ für alle $j \neq k$.
  Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    (x,\eta) \in \intTP(a,b,\del') \subseteq \intTP(a,b,\del)
  \end{IEEEeqnarray*}
  Es folgt:
  \begin{IEEEeqnarray*}{0l"s}
    \abs{R(f,x,\eta) - \int f}
    = \abs{R(f,x,\eta) - R(f,x,\xi) + R(f,x,\xi) - \int f} \\
    \eqin = \abs{\ell(x,k) (f(u) - f(\xi_k)) + R(f,x,\xi) - \int f} \\
    \eqin \geq \abs{\underbrace{\ell(x,k) \abs{f(u) - f(\xi_k)}}_{\geq \ell(x,k) \frac{2}{\mu(x)} \geq 2}
      - \underbrace{\abs{R(f,x,\xi) - \int f}}_{<1}}
    \geq 1
  \end{IEEEeqnarray*}
  Im Ganzen haben wir folgenden Widerspruch zur Integrierbarkeit gezeigt:
  \begin{IEEEeqnarray*}{0l}
    \exists \veps > 0 \innerholds
    \forall \del > 0 \innerholds
    \exists (z,\zeta) \in \intTP(a,b,\del) \holds
    \abs{R(f,z,\zeta) - \int f} \geq \veps
  \end{IEEEeqnarray*}
  Nämlich, wir können $\veps \df 1$ definieren,
  und dann für alle $\del > 0$ anstelle von $(z,\zeta)$
  die oben konstruierte Partition mit Stützstellen $(x,\eta)$ verwenden.
\end{proof}

\begin{satz}[Konvergenzkriterium]
  \label{rlint/cha01:conv}
  Es seien $\ffn{f}{\intcc{a}{b}}{\RR}$ und $R_0 \in \RR$.
  Dann sind die folgenden Aussagen äquivalent:
  \begin{enumerate}
  \item\label{rlint/cha01:conv:1} $f$ ist integrierbar und $R_0 = \int f$.
  \item\label{rlint/cha01:conv:2} $\forall \seq{\idx{x}{n}, \idx{\xi}{n}}{n} \in \seqset{\intTP(a,b)}
    \with \limx{n} \mu(\idx{x}{n}) = 0 \holds \limx{n} R(f,\idx{x}{n},\idx{\xi}{n}) = R_0$
  \end{enumerate}
\end{satz}

\begin{proof}
  \impref{rlint/cha01:conv:1}{rlint/cha01:conv:2}
  Es sei $\seq{\idx{x}{n},\idx{\xi}{n}}{n} \in \seqset{\intTP(a,b)}$ so,
  dass ${\limx{n} \mu(\idx{x}{n}) = 0}$ gilt.
  Um $\limx{n} R(f,\idx{x}{n},\idx{\xi}{n}) = R_0$ zu zeigen, sei $\veps > 0$.
  Aufgrund der Integrierbarkeit gibt es $\del > 0$ so, dass gilt:
  \begin{IEEEeqnarray}{0l}
    \label{rlint/cha01:conv-eq}
    \forall (y,\eta) \in \intTP(a,b,\del) \holds
    \abs{R(f,y,\eta) - R_0} < \veps
  \end{IEEEeqnarray}
  Da ${\limx{n} \mu(\idx{x}{n}) = 0}$ gilt, gibt es $n_0$ so,
  dass $\mu(\idx{x}{n}) < \del$ für alle $n \geq n_0$ gilt,
  also $(\idx{x}{n}, \idx{\xi}{n}) \in \intTP(a,b,\del)$ für alle $n \geq n_0$.
  Wegen~\eqref{rlint/cha01:conv-eq} folgt
  $\abs{R(f,\idx{x}{n},\idx{\xi}{n}) - R_0} < \veps$ für alle $n \geq n_0$.
  \par
  \impref{rlint/cha01:conv:2}{rlint/cha01:conv:1}
  Wir führen Kontraposition.
  Es sei also $f$ nicht integrierbar, oder es sei $f$ integrierbar aber mit Integral ungleich~$R_{0}$.
  In jedem Falle gibt es $\veps > 0$ so, dass gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall \del > 0 \innerholds
    \exists (x,\xi) \in \intTP(a,b,\del) \holds
    \abs{R(f,x,\xi) - R_0} \geq \veps
  \end{IEEEeqnarray*}
  Es folgt insbesondere:
  \begin{IEEEeqnarray*}{0l}
    \forall n \geq 1 \innerholds
    \exists (\idx{x}{n},\idx{\xi}{n}) \in \intTP(a,b,\tfrac{1}{n}) \holds
    \abs{R(f,\idx{x}{n},\idx{\xi}{n}) - R_0} \geq \veps
  \end{IEEEeqnarray*}
  Dies ergibt $\seq{\idx{x}{n},\idx{\xi}{n}}{n} \in \seqset{\intTP(a,b)}$ so,
  dass ${\limx{n} \mu(\idx{x}{n}) = 0}$ gilt,
  aber auch $\limx{n} R(f,\idx{x}{n},\idx{\xi}{n}) \neq R_0$.
\end{proof}

\begin{para}[Limesmethode]
  \label{rlint/cha01:limesmethode}
  Wenn wir also wissen, dass $f$ integrierbar ist,
  dann können wir nach dem Konvergenzkriterium
  eine uns genehme Folge $\seq{\idx{x}{n}, \idx{\xi}{n}}{n} \in \seqset{\intTP(a,b)}$
  mit $\limx{n} \mu(\idx{x}{n}) = 0$ wählen und versuchen,
  den Limes der Folge der zugehörigen Riemann\-/Summen zu bestimmen.
  Damit hätten wir dann das Integral berechnet.
  Wir wollen das die \term{Limesmethode} nennen.
  Unser Problem momentan ist noch, dass wir
  -- bis auf offensichtliche Trivialfälle, wie zum Beispiel konstante Funktionen --
  noch keine integrierbaren Funktionen kennen.
  \par
  Die Limesmethode kann zu Fehlern führen,
  wenn man nicht sicherstellt, dass die Funktion integrierbar ist.
  Als Beispiel betrachte die charakteristische Funktion von $\QQ$ auf $\intcc{0}{1}$:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{f}{\intcc{0}{1}}{\RR}{x \mapsto
      \begin{cases}
        1 & x \in \QQ \\
        0 & \text{sonst}
      \end{cases}}
  \end{IEEEeqnarray*}
  Wähle $\seq{\idx{x}{n}, \idx{\xi}{n}}{n} \in \seqset{\intTP(a,b)}$
  mit $\limx{n} \mu(\idx{x}{n}) = 0$ so,
  dass alle Stützstellen rationale Zahlen sind.
  Dann ist die zugehörige Folge der Riemann\-/Summen konstant~$1$, konvergiert also.
  Man könnte nun denken, dass $\int f = 1$ gilt.
  Das stimmt aber nicht, denn $f$ ist nicht integrierbar, es gibt also gar kein $\int f$.
  Das sieht man etwa so:
  Wähle $\seq{\idx{x}{n}, \idx{\xi}{n}}{n} \in \seqset{\intTP(a,b)}$
  mit $\limx{n} \mu(\idx{x}{n}) = 0$ so,
  dass alle Stützstellen irrationale Zahlen sind.
  Dann ist die zugehörige Folge der Riemann\-/Summen konstant~$0$, konvergiert also,
  aber offenbar gegen einen anderen Wert als die Folge mit den rationalen Stützstellen.
  Damit ist $f$ nicht integrierbar,
  denn bei integrierbarem $f$ wären nach dem Konvergenzkriterium beide Limites gleich gewesen.
\end{para}

\begin{satz}[Cauchykriterium für Integrierbarkeit]
  \label{rlint/cha01:cauchy}
  Es sei $\ffn{f}{\intcc{a}{b}}{\RR}$.
  Dann sind die folgenden Aussagen äquivalent:
  \begin{enumerate}
  \item\label{rlint/cha01:cauchy:1} $f$ ist integrierbar.
  \item\label{rlint/cha01:cauchy:2}
    $\forall \veps > 0 \innerholds
    \exists \del > 0 \innerholds
    \forall (x,\xi), (y,\eta) \in \intTP(a,b,\del) \holds
    \abs{R(f,x,\xi) - R(f,y,\eta)} < \veps$
  \end{enumerate}
\end{satz}

\begin{proof}
  \impref{rlint/cha01:cauchy:1}{rlint/cha01:cauchy:2}
  Definiere $R_0 \df \int f$.
  Es sei $\veps > 0$, und wähle $\del > 0$ wie in der Definition der Integriebarkeit
  bezüglich $\frac{\veps}{2}$, d.h. es gilt:
  \begin{IEEEeqnarray}{0l}
    \label{rlint/cha01:cauchy:proof:1}
    \forall (x,\xi) \in \intTP(a,b,\del) \holds
    \abs{R(f,x,\xi) - R_0} < \tfrac{\veps}{2}
  \end{IEEEeqnarray}
  Es seien $(x,\xi),(y,\eta) \in \intTP(a,b,\del)$.
  Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    \abs{R(f,x,\xi) - R(f,y,\eta)}
    = \abs{R(f,x,\xi) - R_0 + R_0 - R(f,y,\eta)} \\
    \eqin \leq \abs{R(f,x,\xi) - R_0} + \abs{R_0 - R(f,y,\eta)}
    < \tfrac{\veps}{2} + \tfrac{\veps}{2} = \veps
  \end{IEEEeqnarray*}
  \impref{rlint/cha01:cauchy:2}{rlint/cha01:cauchy:1}
  Wir möchten das Konvergenzkriterium (\autoref{rlint/cha01:conv}) anwenden,
  also sei $\seq{(\idx{x}{n},\idx{\xi}{n})}{n} \in \seqset{\intTP(a,b)}$ so,
  dass ${\limx{n} \mu(\idx{x}{n}) = 0}$ gilt.
  Wir müssen $\limx{n} R(f,\idx{x}{n},\idx{\xi}{n}) = R_0$ zeigen,
  für ein $R_0 \in \RR$ das unabhängig von der konkreten Wahl von $\seq{(\idx{x}{n},\idx{\xi}{n})}{n}$ ist.
  Zunächst zeigen wir, dass $\seq{R(f,\idx{x}{n},\idx{\xi}{n})}{n}$ eine Cauchyfolge ist und somit konvergiert.
  Es sei $\veps > 0$, und wähle $\del > 0$ entsprechend der Voraussetzung, es gilt also:
  \begin{IEEEeqnarray*}{0l}
    \forall (x,\xi), (y,\eta) \in \intTP(a,b,\del) \holds
    \abs{R(f,x,\xi) - R(f,y,\eta)} < \veps
  \end{IEEEeqnarray*}
  Da ${\limx{n} \mu(\idx{x}{n}) = 0}$ gilt, gibt es $n_0$ so, dass $\mu(\idx{x}{n}) < \del$,
  also $(\idx{x}{n},\idx{\xi}{n}) \in \intTP(a,b,\del)$ gilt, für alle $n \geq n_0$.
  Es folgt $\abs{R(f,\idx{x}{n},\idx{\xi}{n}) - R(f,\idx{x}{m},\idx{\xi}{m})} < \veps$ für alle $n,m \geq n_0$.
  Das zeigt die Cauchyeigenschaft der Folge $\seq{R(f,\idx{x}{n},\idx{\xi}{n})}{n}$.
  \par
  Schließlich haben wir zu zeigen, dass $R_{0}$ unabhängig von der konkreten Wahl der Folge ist.
  Dazu seien $\seq{(\idx{x}{n},\idx{\xi}{n})}{n},
  \seq{(\idx{y}{n},\idx{\eta}{n})}{n} \in \seqset{\intTP(a,b)}$ so, dass
  $\limx{n} \mu(\idx{x}{n}) = \limx{n} \mu(\idx{y}{n}) = 0$ gilt.
  Definiere $\seq{(\idx{z}{n},\idx{\zeta}{n})}{n}$ als die Folge,
  die abwechselnd Komponenten von $\seq{(\idx{x}{n},\idx{\xi}{n})}{n}$
  und $\seq{(\idx{y}{n},\idx{\eta}{n})}{n}$ besucht.
  Dann gilt ${\limx{n} \mu(\idx{z}{n}) = 0}$ und folglich, nach dem Argument im vorigen Absatz,
  $\limx{n} R(f,\idx{z}{n},\idx{\zeta}{n}) \in \RR$.
  Damit haben
  $\seq{R(f,\idx{x}{n},\idx{\xi}{n})}{n}$ und $\seq{R(f,\idx{y}{n},\idx{\eta}{n})}{n}$ denselben Limes,
  da es sich jeweils um eine Teilfolge von $\seq{R(f,\idx{z}{n},\idx{\zeta}{n})}{n}$ handelt.
\end{proof}

\begin{para}[Integral über Teilbereich]
  \label{rlint/cha01:teilbereich}
  Es seien $\intcc{a}{b} \subseteq \Om \subseteq \RR$ und $\ffn{f}{\Om}{\RR}$.
  Dann nennen wir $f$ \term{integrierbar über $\intcc{a}{b}$} oder \term{integrierbar von $a$ nach $b$},
  wenn $\fnres{f}{\intcc{a}{b}}$ integrierbar ist.
  Wir schreiben dann $\int_a^b f \df \int \fnres{f}{\intcc{a}{b}}$
  und nennen dies das \term{Integral von $f$ über $\intcc{a}{b}$}
  oder das \term{Integral von $f$ von $a$ nach $b$}.
  Die in \autoref{rlint/cha01:notation} vorgestellten weiteren Schreib- und Redeweisen
  finden sinngemäß Anwendung.
\end{para}

\begin{satz}[Vererbung der Integrierbarkeit]
  Es seien $\ffn{f}{\intcc{a}{b}}{\RR}$ integrierbar und $c,d$ so, dass $a \leq c < d \leq b$ gilt.
  Dann $f$ ist integrierbar über $\intcc{c}{d}$, d.h. $\fnres{f}{\intcc{c}{d}}$ ist integrierbar.
\end{satz}

\begin{proof}
  Zur Anwendung des Cauchykriteriums sei $\veps > 0$.
  Da $f$ integrierbar ist, gibt es $\del > 0$ so, dass:
  \begin{IEEEeqnarray}{0l}
    \label{rlint/cha01:sub-int-eq}
    \forall (x,\xi), (y,\eta) \in \intTP(a,b,\del) \holds
    \abs{R(f,x,\xi) - R(f,y,\eta)} < \veps
  \end{IEEEeqnarray}
  Es seien $(\tix,\tixi), (\tiy,\tieta) \in \intTP(c,d,\del)$.
  Wir erweitern jede dieser Partitionen mit Stützstellen von $\intcc{c}{d}$
  zu einer Partition mit Stützstellen von $\intcc{a}{b}$ mit Feinheit weniger als $\del$.
  Nenne diese $(x,\xi)$ und $(y,\eta)$.
  Diese Erweiterung wird so ausgeführt, dass außerhalb von $\intcc{c}{d}$
  die Partitionen mit Stützstellen $(x,\xi)$ und $(y,\eta)$ übereinstimmen.
  Zum Beispiel können wir $N \in \NNone$ so wählen,
  dass $\frac{c-a}{N}, \frac{b-d}{N} < \del$ gilt und dann definieren,
  wobei $n$ die Anzahl der Teilintervalle in $\tix$ sei:
  \begin{IEEEeqnarray*}{0rCl}
    x &\df& (a, \, a+\tfrac{c-a}{N}, \, a+2\tfrac{c-a}{N}, \, \hdots, \, a+N\tfrac{c-a}{N}{=}c{=}\tix_0, \\
    && \tix_1, \, \hdots, \, \tix_{n}{=}d, \\
    && d + \tfrac{b-d}{N}, \, d + 2 \tfrac{b-d}{N}, \, \hdots, \, d + N \tfrac{b-d}{N}{=}b)
  \end{IEEEeqnarray*}
  Ebenso für $(\tiy,\tieta)$ und $(y,\eta)$.
  Mit~\eqref{rlint/cha01:sub-int-eq} folgt:
  \begin{IEEEeqnarray*}{0l+x*}
    \abs{R(f,\tix,\tixi) - R(f,\tiy,\tieta)} = \abs{R(f,x,\xi) - R(f,y,\eta)} < \veps
    & \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\section{Stetige Funktionen sind integrierbar}

Unser nächstes Ziel ist zu zeigen, dass stetige Funktionen integrierbar sind.
Dazu wenden wir wieder das Cauchykriterium an,
nach einigen Vorbereitungen in den folgenden Sätzen.
Da wir nun häufiger die Anzahl der Teilintervalle einer Partition $x$ benötigen,
führen wir dazu die Schreibweise $\nu(x)$ ein.

\begin{para}[Verfeinerung]
  Es seien $x, y$ Partitionen von $\intcc{a}{b}$.
  Dann nennen wir $y$ eine \term{Verfeinerung} von $x$, wenn gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall j \in \setn{\nu(x)} \innerholds \exists k \in \setn{\nu(y)}
    \holds x_{j} = y_{k}
  \end{IEEEeqnarray*}
  D.h. wenn jede Komponente von $x$ auch als Komponente von $y$ vorkommt.
  Nach Definition der Partition (strikte Ungleichheit)
  ist für gegebenes $j$ diese Zahl $k$ eindeutig bestimmt.
  Wenn $y$ eine Verfeinerung von $x$,
  so gilt offenbar $\nu(x) \leq \nu(y)$ und $\mu(y) \leq \mu(x)$.
\end{para}

\begin{satz}[Riemann\-/Summe einer Verfeinerung]
  \label{rlint/cha01:eps-verfeinerung}
  Es seien $\ffn{f}{\intcc{a}{b}}{\RR}$ und $\veps > 0$.
  Es sei $\del > 0$ so, dass $\abs{f(t) - f(t')} < \veps$ für alle $t,t' \in \intcc{a}{b}$
  mit $\abs{t-t'} < \del$ gilt.
  Es seien $(x,\xi), (y,\eta) \in \intTP(a,b,\del)$, wobei $y$ eine Verfeinerung von $x$ sei.
  Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    \abs{R(f,x,\xi) - R(f,y,\eta)} < \veps (b-a)
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}
  Für alle $j \in \setn{\nu(x)}$ definiere:
  \begin{IEEEeqnarray*}{0l}
    S_{j} \df \set{s \in \setn{\nu(y)} \suchthat x_{j-1} < y_{s} \leq x_{j}}
  \end{IEEEeqnarray*}
  Dann ergibt sich $y$ aus der Aneinanderfügung von zuerst $a$,
  dann allen $y_{s}$ mit $s \in S_{1}$, aufsteigend sortiert,
  dann allen $y_{s}$ mit $s \in S_{2}$, aufsteigend sortiert, \usw
  Es folgt:
  \begin{IEEEeqnarray*}{0l+x*}
    \abs{R(f,x,\xi) - R(f,y,\eta)}
    = \abs{\sum_{j=1}^{\nu(x)} f(\xi_j) \ell(x,j) - \sum_{j=1}^{\nu(y)} f(\eta_j) \ell(y,j)} \\
    \eqin = \abs{\sum_{j=1}^{\nu(x)} f(\xi_j) \sum_{s \in S_{j}} \ell(y,s)
      - \sum_{j=1}^{\nu(x)} \sum_{s \in S_{j}} f(\eta_s) \ell(y,s)} \\
    \eqin = \abs{\sum_{j=1}^{\nu(x)} \sum_{s \in S_{j}} \p{f(\xi_j) - f(\eta_s)} \ell(y,s)}
    \leq \sum_{j=1}^{\nu(x)} \sum_{s \in S_{j}} \abs{f(\xi_j) - f(\eta_s)} \ell(y,s) \\
    \eqin < \veps \sum_{j=1}^{\nu(x)} \sum_{s \in S_{j}} \ell(y,s)
    = \veps (b-a) & \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\begin{satz}[Gemeinsame Verfeinerung]
  Es seien $x, y$ Partitionen von $\intcc{a}{b}$.
  Dann gibt es eine Partition von $\intcc{a}{b}$,
  die eine Verfeinerung von $x$ und von $y$ ist.
\end{satz}

\begin{proof}
  Sortiere die Elemente der Menge $\set{\eliz{x}{\nu(x)}} \cup \set{\eliz{y}{\nu(y)}}$
  aufsteigend und schreibe sie in ein Tupel.
\end{proof}

\begin{para}[Gleichmäßige Stetigkeit]
  Obiger \autoref{rlint/cha01:eps-verfeinerung} führt auf folgende Begriffsbildung,
  die wir gleich in einem allgemeinen Rahmen ausführen.
  Es seien $\Om \subseteq \RR$ und $\ffn{f}{\Om}{\RR}$.
  Dann nennen wir $f$ \term{gleichmäßig stetig}, wenn gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall \veps > 0 \innerholds \exists \del > 0 \innerholds \forall x,y \in \Om
    \with \abs{x-y} < \del \holds \abs{f(x) - f(y)} < \veps
  \end{IEEEeqnarray*}
  Nehmen Sie sich einen Moment Zeit, um sich vor Augen zu führen,
  wo der Unterschied zur Stetigkeit liegt.
\end{para}

\begin{satz}
  Gleichmäßig stetige Funktionen sind integrierbar.
\end{satz}

\begin{proof}
  Es sei $\ffn{f}{\intcc{a}{b}}{\RR}$ gleichmäßig stetig.
  Wir wenden das Cauchykriterium an.
  Dazu sei $\veps > 0$.
  Wähle $\del > 0$ entsprechend der gleichmäßigen Stetigkeit bezüglich $\veps' \df \frac{\veps}{2(b-a)}$,
  d.h. wir haben:
  \begin{IEEEeqnarray*}{0l}
    \forall x,y \in \intcc{a}{b}
    \with \abs{x-y} < \del \holds \abs{f(x) - f(y)} < \veps'
  \end{IEEEeqnarray*}
  Es seien $(x,\xi), (y,\eta) \in \intTP(a,b,\del)$.
  Es sei $z$ eine Verfeinerung von $x$ und von $y$,
  und $\zeta$ seien irgendwelche Stützstellen für $z$.
  Mit \autoref{rlint/cha01:eps-verfeinerung} folgt:
  \begin{IEEEeqnarray*}{0l+x*}
    \abs{R(f,x,\xi) - R(f,y,\eta)} \\
    \eqin \leq \abs{R(f,x,\xi) - R(f,z,\zeta)} + \abs{R(f,z,\zeta) - R(f,y,\eta)}
    < 2 \veps' (b-a) = \veps & \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\begin{para}[Kompaktheit]
  Es bietet sich an dieser Stelle folgende topologische Begriffsbildung an,
  die möglicherweise schon aus früheren Semestern bekannt ist.
  Eine Menge $\Om \subseteq \RR$ nennen wir \term{kompakt},
  wenn jede Folge in $\Om$ eine konvergente Teilfolge hat,
  deren Limes in $\Om$ liegt.
  Wir zitieren das Kompaktheitskriterium von Heine und Borel.
  Nämlich für alle $\Om \subseteq \RR$ sind äquivalent:
  \begin{enumerate}
  \item $\Om$ ist kompakt.
  \item $\Om$ ist beschränkt und abgeschlossen.
  \end{enumerate}
\end{para}

\begin{satz}
  \label{rlint/cha01:stetig-int}
  Stetige Funktionen mit kompakter Domain sind gleichmäßig stetig.
\end{satz}

\begin{proof}
  Es seien $\Om \subseteq \RR$ kompakt und $\ffn{f}{\Om}{\RR}$ stetig.
  Für Widerspruch nehme an, es gilt:
  \begin{IEEEeqnarray*}{0l}
    \exists \veps > 0 \innerholds \forall \del > 0 \innerholds \exists x,y \in \Om
    \with \abs{x-y} < \del \holds \abs{f(x) - f(y)} \geq \veps
  \end{IEEEeqnarray*}
  Wähle $\veps > 0$ entsprechend.
  Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall n \geq 1 \innerholds \exists x_{n},y_{n} \in \Om
    \with \abs{x_{n}-y_{n}} < \frac{1}{n} \holds \abs{f(x_{n}) - f(y_{n})} \geq \veps
  \end{IEEEeqnarray*}
  Dies ergibt $\seq{x_{n}}{n}, \seq{y_{n}}{n} \in \seqset{\Om}$.
  Da $\Om$ kompakt ist, gibt es $\ffn{\sig}{\NN}{\NN}$ streng monoton steigend so,
  dass $\seq{x_{\sig(k)}}{k}$ konvergiert,
  und zwar gegen ${p \df \limx{k} x_{\sig(k)} \in \Om}$.
  Man sieht leicht (Übung), dass auch $\limx{k} y_{\sig(k)} = p$ gilt.
  Da $f$ stetig ist, folgt:
  \begin{IEEEeqnarray*}{0l}
    \limx{k} f(x_{\sig(k)}) = f(p) = \limx{k} f(y_{\sig(k)})
  \end{IEEEeqnarray*}
  Also gibt es $k_{1}$ so, dass $\abs{f(x_{\sig(k)}) - f(p)} < \frac{\veps}{2}$ für alle $k \geq k_{1}$ gilt;
  es gibt $k_{2}$ so, dass $\abs{f(y_{\sig(k)}) - f(p)} < \frac{\veps}{2}$ für alle $k \geq k_{2}$ gilt.%
  \footnote{Kürzer könnten wir mit unserer Notation schreiben
    $k_{1} \df \nzero{}(B(f(p), \frac{\veps}{2}), \seq{f(x_{\sig(k))}}{k})$
    und $k_{2} \df \nzero{}(B(f(p), \frac{\veps}{2}), \seq{f(y_{\sig(k))}}{k})$.}
  Mit $k_{0} \df \max\set{k_{1}, k_{2}}$ folgt:
  \begin{IEEEeqnarray*}{0l}
    \abs{f(x_{\sig(k_{0})}) - f(y_{\sig(k_{0})})}
    = \abs{f(x_{\sig(k_{0})}) - p + p - f(y_{\sig(k_{0})})} \\
    \eqin \leq \abs{f(x_{\sig(k_{0})}) - p} + \abs{p - f(y_{\sig(k_{0})})}
    < \frac{\veps}{2} + \frac{\veps}{2} = \veps
  \end{IEEEeqnarray*}
  Dies widerspricht der Tatsache,
  dass $\abs{f(x_{\sig(k)}) - f(y_{\sig(k)})} \geq \veps$ für alle $k$ gilt.
\end{proof}

Da nach Heine\-/Borel insbesondere $\intcc{a}{b}$ kompakt ist, folgt:

\begin{corollary}
  Stetige Funktionen sind integrierbar.
\end{corollary}

\begin{para}[Berechnung eines Integrals]
  Wir können jetzt grundsätzlich für stetige Funktionen
  das Integral nach der Limesmethode (\autoref{rlint/cha01:limesmethode}) berechnen.
  Zum Beispiel gilt
  $\int_{a}^{b} x \de x = \frac{b^{2}-a^{2}}{2}$.
  Das sieht man folgerndermaßen durch Betrachtung äquidistanter Partitionen,
  die wir schon in \autoref{rlint/cha01:eindeutig} benutzt hatten.
  Für alle $n \geq 1$ definiere $\idx{\del}{n} \df \frac{b-a}{n}$
  und weiter eine Partition $\idx{x}{n}$
  via $\idx{x}{n}_{j} \df a + j \idx{\del}{n}$ für alle $j \in \setft{0}{n}$
  mit Stützstellen $\idx{\xi}{n}$ definiert via
  $\idx{\xi}{n}_{j} \df a + j\idx{\del}{n}$ für alle $j \in \setn{n}$.
  Mit der Formel von Faulhaber gilt für alle $n$:
  \begin{IEEEeqnarray*}{0l}
    R(\id_{\intcc{a}{b}}, \idx{x}{n}, \idx{\xi}{n})
    = \sum_{j=1}^{n} \idx{\del}{n} \p{a+j\idx{\del}{n}}
    = (b-a) a + \p{\idx{\del}{n}}^{2} \sum_{j=1}^{n} j \\
    \eqin = ba - a^{2} + \frac{(b-a)^{2}}{n^{2}} \frac{n(n+1)}{2}
    = ba - a^{2} + \frac{b^{2}-2ab+a^{2}}{2} \frac{n+1}{n} \\
    \eqin \tendsx{n} ba - a^{2} + \frac{b^{2}-2ab+a^{2}}{2}
    = \frac{b^{2}-a^{2}}{2}
  \end{IEEEeqnarray*}
  Die Behauptung folgt aus dem Konvergenzkriterium.
\end{para}

Einfacher lassen sich Integrale oft über den Hauptsatz berechnen,
den wir im nächsten Kapitel kennenlernen werden.

\section{Weitere Eigenschaften des Integrals}

\begin{satz}[Linearität des Integrals]
  Es seien $\ffn{f,g}{\intcc{a}{b}}{\RR}$ integrierbar,
  und es sei $\lam \in \RR$.
  Dann sind $f+g$ und $\lam f$ integrierbar, und es gilt:
  \begin{IEEEeqnarray*}{0l+l}
    \int_a^b f+g = \int_a^b f + \int_a^b g
    &\int_a^b \lam f = \lam \int_a^b f
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}
  Übung.
\end{proof}

\begin{para}[Gleiche und umgedrehte Grenzen]
  Es seien $\Om \subseteq \RR$ und $\ffn{f}{\Om}{\RR}$.
  Definiere $\int_t^t f \df 0$ für alle $t \in \Om$;
  insbesondere nennen wir $f$ integrierbar über $\intcc{t}{t}$.
  Wenn $\intcc{a}{b} \subseteq \Om$ gilt und $f$ integrierbar über $\intcc{a}{b}$ ist,
  definiere $\int_b^a f \df - \int_a^b f$.
\end{para}

\begin{satz}[Abschätzungen]
  \label{rlint/cha01:monotone-bounds}
  Es seien $\ffn{f,g}{\intcc{a}{b}}{\RR}$ integrierbar.
  \begin{enumerate}
  \item Es gelte $f \leq g$, d.h. $f(u) \leq g(u)$ für alle $u \in \intcc{a}{b}$.
    Dann gilt $\int_a^b f \leq \int_a^b g$.
    Das nennt man \term{Monotonie des Integrals}.
  \item\label{rlint/cha01:monotone-bounds:2}
    Es gilt:
    \begin{IEEEeqnarray*}{0l}
      (b-a) \inf_{u \in \intcc{a}{b}} f(u) \leq \int_a^b f
      \leq (b-a) \sup_{u \in \intcc{a}{b}} f(u)
    \end{IEEEeqnarray*}
    Dabei sind Supremum und Infimum in $\RR$,
    da integrierbare Funktionen beschränkt sind (\autoref{rlint/cha01:int-bounded}).
    Das nennt man die \term{Standardabschätzung des Integrals}.
  \item\label{rlint/cha01:monotone-bounds:3}
    Es gilt $\abs{\int_a^b f} \leq (b-a) \sup_{u \in \intcc{a}{b}} \abs{f(u)}$.
    Für alle $x,y \in \intcc{a}{b}$ gilt:
    \begin{IEEEeqnarray*}{0l}
      \abs{\int_x^y f} \leq \abs{x-y} \sup_{u \in \intcc{x}{y} \cup \intcc{y}{x}} \abs{f(u)}
    \end{IEEEeqnarray*}
  \item\label{rlint/cha01:monotone-bounds:4}
    Wenn $\abs{f}$ integrierbar ist,\footnote{%
      Wir wissen, dass dies insbesondere dann der Fall ist,
      wenn $f$ stetig ist, da dann $\abs{f}$ stetig ist.
      Man kann sogar zeigen, dass immer $\abs{f}$ integrierbar ist,
      wenn $f$ integrierbar ist,
      aber wir lassen den Beweis aus.}
    haben wir:
    \begin{IEEEeqnarray*}{0l}
      \abs{\int_a^b f} \leq \int_a^b \abs{f} \leq (b-a) \sup_{u \in \intcc{a}{b}} \abs{f(u)}
    \end{IEEEeqnarray*}
    Die erste Abschätzung nennt man die \term{Dreiecksungleichung für Integrale}.
  \end{enumerate}
\end{satz}

\begin{proof}
  \begin{enumerate}
  \item Es sei $\seq{(\idx{x}{n}, \idx{\xi}{n})}{n} \in \seqset{\intTP(a,b)}$
    so, dass $\limx{n} \mu(x_n) = 0$ gilt.
    Nach dem Konvergenzkriterium (\autoref{rlint/cha01:conv}) gilt:
    \begin{IEEEeqnarray*}{0l+s+l}
      \limx{n} R(f,\idx{x}{n},\idx{\xi}{n}) = \int_a^b f
      & und &
      \limx{n} R(g,\idx{x}{n},\idx{\xi}{n}) = \int_a^b g
    \end{IEEEeqnarray*}
    Nach Voraussetzung gilt $R(f,\idx{x}{n},\idx{\xi}{n}) \leq R(g,\idx{x}{n},\idx{\xi}{n})$ für alle $n$.
    Die Behauptung folgt mit der Monotonie des Limes.
  \item Definiere $r \df \inf_{u \in \intcc{a}{b}} f(u)$ und $s \df \sup_{u \in \intcc{a}{b}} f(u)$.
    Nach dem vorigen Punkt gilt:
    \begin{IEEEeqnarray*}{0l}
      (b-a) r = \int_a^b r \leq \int_a^b f \leq \int_a^b s = (b-a) s
    \end{IEEEeqnarray*}
    Die Integrale der konstanten Funktionen berechnet man sehr leicht über die Riemann\-/Summen.
  \item
    Man kann zeigen (Übung),
    dass für alle $\emptyset \neq A \subseteq \RR$ gilt:
    \begin{IEEEeqnarray*}{0l}
      \max\set{\abs{\inf(A)}, \, \abs{\sup(A)}} = \sup\set{\abs{a} \suchthat a \in A}
    \end{IEEEeqnarray*}
    Damit folgt der erste Teil der Behauptung aus dem vorigen Punkt.
    Für den zweiten Teil mache eine Fallunterscheidung zwischen $x \leq y$ und $x > y$
    und bemerke $\abs{\int_x^y f} = \abs{\int_y^x f}$.
  \item Es genügt, die erste Abschätzung zu zeigen,
    da die zweite aus~\ref{rlint/cha01:monotone-bounds:2} folgt.
    Es sei $\seq{(\idx{x}{n}, \idx{\xi}{n})}{n} \in \seqset{\intTP(a,b)}$ so,
    dass $\limx{n} \mu(\idx{x}{n}) = 0$ gilt.
    Dann gilt mit der bekannten Dreiecksungleichung für Summen
    $\abs{R(f,\idx{x}{n}, \idx{\xi}{n})} \leq R(\abs{f},\idx{x}{n}, \idx{\xi}{n})$ für alle $n$.
    Ferner gilt $\limx{n} R(f,\idx{x}{n}, \idx{\xi}{n}) = \int_a^b f$ und mit der Stetigkeit des Betrages
    somit $\limx{n} \abs{R(f,\idx{x}{n}, \idx{\xi}{n})} = \abs{\int_a^b f}$.
    Außerdem ist ${\limx{n} R(\abs{f},\idx{x}{n}, \idx{\xi}{n}) = \int_a^b \abs{f}}$.
    Mit der Monotonie des Limes folgt ${\abs{\int_a^b f} \leq \int_a^b \abs{f}}$.
    \qedhere
  \end{enumerate}
\end{proof}

Der Beweis des folgenden Satzes ist etwas technisch und wird übersprungen:

\begin{satz}[Aneinanderstückelungssatz]
  Es sei $\ffn{f}{\intcc{a}{b}}{\RR}$.
  \begin{enumerate}
  \item Es sei $c \in \intcc{a}{b}$,
    und es sei $f$ integrierbar über $\intcc{a}{c}$ und über $\intcc{c}{b}$.
    Dann ist $f$ integrierbar, und es gilt:
    \begin{IEEEeqnarray*}{0l}
      \int_a^b f = \int_a^c f + \int_c^b f
    \end{IEEEeqnarray*}
  \item Es sei $f$ integrierbar.
    Dann gilt für alle $u,v,w \in \intcc{a}{b}$:
    \begin{IEEEeqnarray*}{0l}
      \int_u^w f = \int_u^v f + \int_v^w f
    \end{IEEEeqnarray*}
    Zwei andere Arten, das zu schreiben, ist:
    \begin{IEEEeqnarray*}{0l+l}
      \int_u^w f - \int_u^v f = \int_v^w f
      &
      \int_v^u f - \int_w^u f  = \int_v^w f
    \end{IEEEeqnarray*}
  \end{enumerate}
\end{satz}

\begin{satz}[Stetige Abhängigkeit von der Integralgrenze]
  \label{rlint/cha01:stetig}
  Es sei $\ffn{f}{\intcc{a}{b}}{\RR}$ integrierbar.
  Es sei $u \in \intcc{a}{b}$, und definiere:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{F}{\intcc{a}{b}}{\RR}{x \mapsto \int_u^x f}
  \end{IEEEeqnarray*}
  Dann ist $F$ stetig.
\end{satz}

\begin{proof}
  Es sei $x \in \intcc{a}{b}$ und $\veps > 0$.
  Da $f$ integrierbar ist, ist $f$ beschränkt,
  also gibt es $M > 0$ so, dass $\abs{f(y)} < M$ für alle $y \in \intcc{a}{b}$ gilt.
  Definiere $\del \df \frac{\veps}{M}$.
  Es sei $y \in \intcc{a}{b} \cap B(x, \del)$.
  Dann gilt:
  \begin{IEEEeqnarray*}{0l+x*}
    \abs{F(x) - F(y)}
    = \abs{\int_u^x f - \int_u^y f}
    = \abs{\int_y^x f}
    \leq \abs{y-x} \ccdot M
    < \del M = \veps
    & \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\begin{satz}[Änderungssatz]
  Es sei $\ffn{f}{\intcc{a}{b}}{\RR}$ integrierbar
  und $\eli{c}{n} \in \intcc{a}{b}$ paarweise verschieden und $\eli{v}{n} \in \RR$.
  Definiere:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{\tif}{\intcc{a}{b}}{\RR}{x \mapsto
      \begin{cases}
        v_{i} & \text{wenn $x=c_{i}$} \\
        f(x) & \text{sonst}
      \end{cases}}
  \end{IEEEeqnarray*}
  Dann ist $\tif$ integrierbar, und es gilt $\int f = \int \tif$.
\end{satz}

\begin{proof}
  Es genügt, die Aussage für die Änderung an einer Stelle
  $c \in \intcc{a}{b}$ auf einen Wert $v \in \RR$ zu behandeln,
  da die allgemeine Aussage daraus leicht induktiv folgt.
  Wir zeigen:
  \begin{IEEEeqnarray*}{0l}
    \forall \veps > 0 \innerholds
    \exists \del > 0 \innerholds
    \forall (x,\xi) \in \intTP(a,b,\del) \holds
    \abs{R(\tif,x,\xi) - \int f} < \veps
  \end{IEEEeqnarray*}
  Dazu sei $\veps > 0$.
  Da $f$ integrierbar ist, gibt es $\del_{0} > 0$ so, dass gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall (x,\xi) \in \intTP(a,b,\del_{0}) \holds
    \abs{R(f,x,\xi) - \int f} < \frac{\veps}{2}
  \end{IEEEeqnarray*}
  Definiere $\del_{1} \df \frac{\veps}{4 (\abs{v} + \abs{f(c)} + 1)}$
  und $\del \df \min\set{\del_{0}, \del_{1}}$.
  Es sei $(x,\xi) \in \intTP(a,b,\del)$.
  Definiere $C \df \set{j \suchthat \xi_{j} = c}$,
  d.h. $C$ ist die Menge der Indizes der Teilintervalle, in denen $c$ Stützstelle ist.
  Es gilt $\abs{C} \leq 2$ und ferner:
  \begin{IEEEeqnarray*}{0l}
    \abs{R(\tif,x,\xi) - R(f,x,\xi)}
    = \abs{\sum_{j \in C} (v - f(c)) (x_{j} - x_{j-1})} \\
    \eqin \leq \sum_{j \in C} \abs{v - f(c)} \ccdot \abs{x_{j} - x_{j-1}}
    \leq 2 \del \abs{v - f(c)}
    \leq 2 \del \p{\abs{v} + \abs{f(c)}} < \frac{\veps}{2}
  \end{IEEEeqnarray*}
  Letzteres, da $\del \leq \del_{1}$.
  Insgesamt haben wir:
  \begin{IEEEeqnarray*}{0l}
    \abs{R(\tif,x,\xi) - \int f}
    = \abs{R(\tif,x,\xi) - R(f,x,\xi) + R(f,x,\xi) - \int f} \\
    \eqin \leq \abs{R(\tif,x,\xi) - R(f,x,\xi)} + \abs{R(f,x,\xi) - \int f} \\
    \eqin < \frac{\veps}{2} + \abs{R(f,x,\xi) - \int f}
    < \frac{\veps}{2} + \frac{\veps}{2} = \veps
  \end{IEEEeqnarray*}
  Bei der letzten Abschätzung haben wir $\del \leq \del_{0}$ benutzt.
\end{proof}
