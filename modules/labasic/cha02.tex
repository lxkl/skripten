%%% Copyright 2023 Lasse Kliemann <l.kliemann@math.uni-kiel.de>
%%% CC BY-SA 4.0

\chapter{Lineare Gleichungssysteme}

\lxklKitReviewWarning

\section{Lösungsraum}

\begin{para}[Fragestellung]
  \label{labasic/cha02:ex-lgs-1}
  In Anwendungen tauchen oft Probleme der folgenden Form auf.
  Zum Beispiel könnte man sich fragen, ob es $x \in \RR^{4}$ so gibt, dass gilt:
  \begin{IEEEeqnarray*}{0l}
    x_1 = -2 x_3 + 7 \\*
    4x_2 + x_3 - x_4 + 1 = x_1 \\*
    x_1 + x_2 = x_3 - x_4 - 5
  \end{IEEEeqnarray*}
  Wenn es solche Vektoren in $\RR^{4}$ gibt,
  stellt sich weiter die Frage, wie viele es gibt,
  wie man sie berechnet und ob sie eine besondere Struktur bilden.
  Wir werden in diesem Abschnitt sehen, dass Subräume dabei eine Rolle spielen.
\end{para}

\begin{para}[Formulierung mit Matrizen]
  Mithilfe von Matrizen können wir ein solches Problem elegant formulieren.
  Wir führen dies an dem in \autoref{labasic/cha02:ex-lgs-1} gegebenen Beispiel vor.
  Zunächst sortieren wir, so dass rechts nur noch Zahlen stehen:
  \begin{IEEEeqnarray*}{0l}
    x_1 + 0x_2 + 2 x_3 + 0x_4 = 7 \\
    -x_1 + 4x_2 + x_3 - x_4 = -1 \\
    x_1 + x_2 - x_3 + x_4 = - 5
  \end{IEEEeqnarray*}
  Nun definiere:
  \begin{IEEEeqnarray*}{0l+l}
    A \df
    \begin{bmatrix}
      1 & 0 & 2 & 0 \\
      -1 & 4 & 1 & -1 \\
      1 & 1 & -1 & 1
    \end{bmatrix}
    &
    b \df \ColVec{7 , -1 , -5}
  \end{IEEEeqnarray*}
  Dabei nennen wir $A$ die \term{Koeffizientenmatrix} und $b$ die \term{rechte Seite}.
  Offenbar ist $x \in \RR^{4}$ eine Lösung für das Problem aus \autoref{labasic/cha02:ex-lgs-1} genau dann,
  wenn $Ax=b$ gilt.
\end{para}

Das führt auf folgende Definition:

\begin{para}[Generalvoraussetzung, LGS, Lösungsmenge]
  Von nun an seien in diesem Kapitel $K$ ein Körper
  und $d,n \in \NNone$ und $A \in K^{d \times n}$ und $b \in K^{d}$,
  wenn nicht anders ausgewiesen.
  Wir nennen $(A,b)$ ein \term{Lineares Gleichungssystem} (LGS)
  mit $n$ \term{Unbekannten} und $d$ \term{Gleichungen} über $K$.
  Wir nennen $x \in K^{n}$ eine \term{Lösung} von $(A,b)$ wenn $Ax=b$ gilt
  und bezeichnen die Menge aller Lösungen von $(A,b)$ mit $L(A,b) \df \set{x \in K^{n} \suchthat Ax=b}$.
  Man spricht bei $L(A,b)$ auch, etwas salopp, von der \term{allgemeinen Lösung} von $(A,b)$.
  \par
  Bei der Beschreibung von $L(A,b)$ spielt der Kern von $A$ eine wichtige Rolle.
  Bemerke $\ker(A) = L(A,0)$.
  Das LGS $(A,0)$ nennt man auch das \term{homogene LGS} zu $(A,b)$.
\end{para}

Für alle $u \in K^{n}$ und alle $M \subseteq K^{n}$
schreiben wir $u + M \df \set{u+v \suchthat v \in M}$.
Damit gilt:

\begin{satz}[Struktur der Lösungsmenge]
  Es gilt $L(A,b) = u + \ker(A)$ für alle $u \in L(A,b)$,
  d.h. die Lösungsmenge von $(A,b)$ ist der Kern von $A$
  verschoben um eine beliebige Lösung von $(A,b)$.
\end{satz}

\begin{proof}
  Es sei $u \in L(A,b)$.
  \par
  $\subseteq$:
  Es sei ${x \in L(A,b)}$.
  Definiere $v \df x - u$.
  Dann gilt $Av = A(x-u) = Ax - Au = b-b = 0$, also $x-u \in \ker(A)$.
  Da $x = u + (x-u)$ gilt, folgt $x \in u + \ker(A)$.
  \par
  $\supseteq$: Es sei $x \in u + \ker(A)$.
  Dann gibt es $v \in \ker(A)$ so, dass $x = u + v$.
  Es folgt $Ax = A(u+v) = Au + Av = b+0 = b$, also $x \in L(A,b)$.
\end{proof}

\begin{para}[Konzise Angabe der Lösungsmenge]
  Wir können $L(A,b)$ also in konziser Form angeben,
  indem wir eine Lösung von $(A,b)$ angeben und eine Basis von $\ker(A)$.
  Wir entwickeln im nächsten Abschnitt eine Methode dafür.
\end{para}

\section{Elementare Zeilenumformungen}

\begin{para}[Definition]
  \label{labasic/cha02:umform}
  Mit \term{elementaren Zeilenumformungen} können wir Matrizen bearbeiten.
  Es gibt drei Typen:
  \begin{enumerate}[label=(\Roman*)]
  \item\label{labasic/cha02:typ1} Vertauschen zweier Zeilen
  \item\label{labasic/cha02:typ2} Multiplikation einer Zeile mit einem Skalar ungleich Null
  \item\label{labasic/cha02:typ3} Addition eines skalaren Vielfachen einer Zeile zu einer anderen Zeile
  \end{enumerate}
\end{para}

\begin{example}
  Wir demonstrieren die drei Typen an je einem Beispiel:
  \begin{IEEEeqnarray*}{0l+C+l}
    \begin{gmatrix}[b]
      1 & 2 & 3 & 4 \\
      5 & 6 & 7 & 8 \\
      9 & 10 & 11 & 12
      \rowops
      \swap{0}{1}
    \end{gmatrix}
    &\longrightarrow&
    \begin{gmatrix}[b]
      5 & 6 & 7 & 8 \\
      1 & 2 & 3 & 4 \\
      9 & 10 & 11 & 12
    \end{gmatrix}
    \\
    \begin{gmatrix}[b]
      1 & 2 & 3 & 4 \\
      5 & 6 & 7 & 8 \\
      9 & 10 & 11 & 12
      \rowops
      \mult{0}{\cdot 5}
    \end{gmatrix}
    &\longrightarrow&
    \begin{gmatrix}[b]
      5 & 10 & 15 & 20 \\
      5 & 6 & 7 & 8 \\
      9 & 10 & 11 & 12
    \end{gmatrix}
    \\
    \begin{gmatrix}[b]
      1 & 2 & 3 & 4 \\
      5 & 6 & 7 & 8 \\
      9 & 10 & 11 & 12
      \rowops
      \add[2]{1}{2}
    \end{gmatrix}
    &\longrightarrow&
    \begin{gmatrix}[b]
      1 & 2 & 3 & 4 \\
      5 & 6 & 7 & 8 \\
      19 & 22 & 25 & 28
    \end{gmatrix}
  \end{IEEEeqnarray*}
\end{example}

\begin{para}[Bezeichnung]
  Die Matrix $\RowVec{A , b} \in K^{d \times (n+1)}$,
  also die Matrix die aus $A$ entsteht, indem wir $b$ als weitere Spalte rechts anfügen,
  nennen wir die \term{erweiterte Koeffizientenmatrix} von $(A,b)$
  und wird mit $A\vert b$ bezeichnet.
\end{para}

\begin{satz}[Invarianz der Lösungsmenge]
  Die erweiterte Koeffizientenmatrix $A'\vert b'$ gehe aus $A\vert b$ durch elementare Zeilenumformungen hervor.
  Dann gilt $L(A,b) = L(A',b')$ und $\ker(A) = \ker(A')$.
\end{satz}

\begin{proof}
  Wir behandeln nur Typ~\ref{labasic/cha02:typ3} und überlassen die anderen zur Übung.
  Es sei $\al \in K$ und ${s,t \in \setn{d}}$ mit $s \neq t$.
  Es gehe $A'\vert b'$ aus $A\vert b$ hervor,
  indem das \xfache{\al} von Zeile $s$ zu Zeile $t$ addiert wird.
  Es gilt also:
  \begin{IEEEeqnarray*}{0l}
    \forall i \in \setn{d} \setminus \set{t} \holds A'_{i\ast} = A_{i\ast} \land b'_{i} = b_{i} \\
    A'_{t\ast} = \al A_{s\ast} + A_{t\ast} \,\land\, b'_{t} = \al b_{s} + b_{t}
  \end{IEEEeqnarray*}
  Es sei $x \in K^{n}$.
  Nach Definition des Matrixproduktes gilt für alle $i \in \setn{d} \setminus \set{t}$:
  \begin{IEEEeqnarray}{0l}
    \label{labasic/cha02:invar:1}
    (A'x)_{i} = b'_{i} \iff (Ax)_{i} = b_{i}
  \end{IEEEeqnarray}
  Ferner:
  \begin{IEEEeqnarray*}{0l}
    (A'x)_{t} = b'_{t} \iff \sum_{k=1}^{n} A'_{tk} x_{k} = b'_{t}
    \iff \sum_{k=1}^{n} (\al A_{sk} + A_{tk}) x_{k} = \al b_{s} + b_{t} \\
    \iff \al \sum_{k=1}^{n} A_{sk} x_{k} + \sum_{k=1}^{n} A_{tk} x_{k} = \al b_{s} + b_{t}
    \iff \al (Ax)_{s} + (Ax)_{t} = \al b_{s} + b_{t} \\
    \iff (Ax)_{t} = b_{t}
  \end{IEEEeqnarray*}
  Beim letzten Schritt haben wir \eqref{labasic/cha02:invar:1} benutzt,
  bemerke dazu $s \in \setn{d} \setminus \set{t}$.
  \par
  Insgesamt folgt $(A'x)_{i} = b'_{i} \iff (Ax)_{i} = b_{i}$ für alle $i \in \setn{d}$,
  also $x \in L(A',b') \iff x \in L(A,b)$.
\end{proof}

\begin{para}[Vorbereitung der Methode]
  Wir können also versuchen,
  $A\vert b$ durch elementare Zeilenumformungen auf eine uns genehme Gestalt zu bringen,
  deren Lösungen leichter abzulesen sind.
  Dies sind dann genau die Lösungen für das originale LGS.
  Wir werden bald sehen, dass manchmal ein weiterer Typ von Umformung nützlich ist,
  nämlich das Vertauschen von zwei Spalten.
  Dadurch kann sich die Lösungsmenge verändern, aber in überschaubarer Weise:
\end{para}

\begin{satz}[Lösungsmenge unter Spaltenvertauschungen]
  Die erweiterte Koeffizientenmatrix $A'\vert b'$ gehe aus $A\vert b$ durch elementare Zeilenumformungen
  und Vertauschung von Spalten im Bereich der ersten $n$ Spalten hervor.
  Es bezeichnen $\eli{t}{l} \in \setn{n}^{2}$ die Vertauschungen der Spalten.
  Es sei $\tilde{L}$ die Menge, die aus $L(A',b')$ durch Vertauschen der Komponenten in den Vektoren
  gemäß $\elix{t}{l}{1}$ hervorgeht,
  also die Spaltenvertauschungen werden sinngemäß in umgekehrter Reihenfolge
  auf die Komponenten der Vektoren angewendet.
  Dann gilt $L(A,b) = \tilde{L}$.
\end{satz}

\begin{proof}
  Übung.
\end{proof}

\section{Kanonische Zeilenstufenform}

Wir untersuchen, wie eine günstige Gestalt der erweiterten Koeffizientenmatrix aussehen könnte
und was wir daran ablesen können.
Danach zeigen wir im nächsten Abschnitt,
wie diese durch elementare Zeilenumformungen und Spaltenvertauschungen zu erreichen ist.

\begin{para}[Definition]
  \label{labasic/cha02:kanonische}
  Die gewünschte Form der erweiterten Koeffizientenmatrix nennt man \term{kanonische Zeilenstufenform},
  und diese sieht so aus mit $r,k \in \NNzero$:
  \begin{IEEEeqnarray*}{0l}
    \begin{bmatrix}
      1 & & & & & s_{11} & \hdots & \hdots & \hdots & s_{1k} & b'_1 \\
      & \ddots & & \text{\LARGE$0$} & & \vdots & & & & \vdots & \vdots \\
      & & \ddots & & & \vdots & & & & \vdots & \vdots \\
      & \text{\LARGE$0$} & & \ddots & & \vdots & & & & \vdots & \vdots \\
      & & & & 1 & s_{r1} & \hdots & \hdots & \hdots & s_{rk} & b'_r \\
      & & & & & & & & & & b'_{r+1} \\
      & & \text{\LARGE$0$} & & & & & \text{\LARGE$0$} & & & \vdots \\
      & & & & & & & & & & b'_d
    \end{bmatrix}
  \end{IEEEeqnarray*}
  Die ersten $r$ Diagonaleinträge sind also~$1$.
  Die Einträge $s_{ij}$ mit $i \in \setn{r}$ und $j \in \setn{k}$
  sowie die $\eli{b'}{d}$ können beliebig sein.
  Alle anderen Einträge sind Null.
\end{para}

\begin{para}[Generalvoraussetzung]
  Im Folgenden sei stets vorausgesetzt, dass $A\vert b$ durch elementare Zeilenumformungen
  und Spaltenvertauschungen (innerhalb der Spalten $1,\hdots,n$) in $A'\vert b'$ überführt wurde
  und $A'\vert b'$ kanonische Zeilenstufenform wie im obigen Absatz hat.
  Es seien $r,k$ und $s_{ij}$ mit $i \in \setn{r}$ und $j \in \setn{k}$ wie dort.
\end{para}

\begin{satz}[Ablesen von Rang und Kerndimension]
  Es gilt:
  \begin{enumerate}
  \item $\rank(A) = r$
  \item $\dim(\ker(A)) = k$
  \end{enumerate}
\end{satz}

\begin{proof}
  \begin{enumerate}
  \item Wir betrachten in der kanonischen Zeilenstufenform nur $A'$, also den aus $A$ hervorgegangenen Teil,
    d.h. die Matrix aus den ersten~$n$ Spalten.
    Elementare Zeilenumformungen und Vertauschen von Spalten verändern den Rang nicht (Übung).
    Wie man leicht sieht, sind die ersten $r$ Zeilen linear unabhängig,
    und die übrigen $d-r$ Zeilen bestehen nur aus Null\-/Einträgen, verändern somit den Zeilenraum nicht.
    Es folgt, dass der Zeilenrang und damit der Rang von $A'$ und damit der von $A$ genau $r$ ist.
  \item Es gilt mit der Dimensionsformel:
    \begin{IEEEeqnarray*}{0l+x*}
      r + k = n = \rank(A') + \dim(\ker(A'))
      = r + \dim(\ker(A')) = r + \dim(\ker(A)) & \qedhere
    \end{IEEEeqnarray*}
  \end{enumerate}
\end{proof}

\begin{satz}[Lösbarkeitskriterium]
  Wenn es $i > r$ mit $b'_i \neq 0$ gibt, so ist $(A,b)$ unlösbar, d.h. ${L(A,b) = \emptyset}$.
\end{satz}

\begin{proof}
  In der Zeile $i > r$ von $A'\vert b'$ stehen links von $b'_i$ nur Null\-/Einträge.
  Somit ist $(A'x)_i = 0 \neq b'_i$ für alle $x \in K^n$.
  Also ist $(A',b')$ nicht lösbar und damit auch $(A,b)$ nicht.
\end{proof}

\begin{satz}[Ablesen einer Lösung von $(A',b')$]
  Wenn $b'_i = 0$ für alle $i > r$ gilt, so ist $(A,b)$ lösbar,
  und eine Lösung von $(A',b')$ ist gegeben durch:
  \begin{IEEEeqnarray*}{0l+x*}
    u' \df \ColVec{b'_1, \vdots , b'_r , 0 , \vdots , 0} \in K^n
  \end{IEEEeqnarray*}
  D.h. es werden unter den $\eli{b'}{r}$ genau $k$ Nullen angefügt.
\end{satz}

\begin{proof}
  Für alle $i \leq r$ ist $(A'u')_i = u'_i = b'_i$,
  da der Eintrag $u'_i=b'_i$ genau auf die $1$ der Diagonalen in $A'$ trifft
  und alles andere Null ist.
  Für alle $i > r$ ist $(A'u')_i = 0 = b'_i$, da $A'$ in den Zeilen $r+1,\hdots,d$ nur Null\-/Einträge hat
  und nach Voraussetzung~${b'_i = 0}$ gilt.
\end{proof}

\begin{para}[Rücktauschen]
  Beachte, dass obiger Satz zwar über die Lösbarkeit von $(A,b)$ spricht,
  aber eine Lösung von $(A',b')$ konstruiert.
  Eine Lösung von $(A,b)$ erhält man aus dem $u'$ des vorangehenden Satzes
  durch Vertauschen von Komponenten entsprechend der Spaltenvertauschungen,
  die zur Erreichung von $A'\vert b'$ durchgeführt wurden.
  Wenn keine Spaltenvertauschungen notwendig waren, so ist $u'$ eine Lösung von~$(A,b)$.
\end{para}

\begin{satz}[Lösbarkeitskriterium via Rang]
  \label{labasic/cha02:rank-gleich}
  Es ist $(A,b)$ genau dann lösbar, wenn $\rank(A) = \rank(A \vert b)$ gilt.
\end{satz}

\begin{proof}
  Wir dürfen $A'$ und $A'\vert b'$ anstelle von $A$ und $A \vert b$ betrachten.
  An $A'\vert b'$ sieht man sofort:
  \begin{IEEEeqnarray*}{0l}
    b'_{r+1} = \hdots = b'_{d} = 0 \implies \rank(A'\vert b') = \rank(A')
  \end{IEEEeqnarray*}
  Für die umgekehrte Implikation führen wir Kontraposition.
  Es sei $s > r$ mit $b_{s}' \neq 0$.
  Definiere $Z \df \seti{(A' \vert b')_{i\ast}}{r}$ und $Z' \df Z \cup \set{(A' \vert b')_{s\ast}}$.
  Dann ist $\card{Z} = r$ und $\card{Z'} = r+1$.
  Es ist $Z$ linear unabhängig, und da sich $(A' \vert b')_{s,\ast}$
  offenbar nicht als Linearkombination der ersten $r$ Zeilen schreiben lässt,
  ist auch $Z'$ linear unabhängig.
  Es folgt $\rank(A' \vert b') > r = \rank(A')$.
\end{proof}

Wir geben noch einen zweiten Beweis, der ohne Betrachtung der kanonischen Zeilenstufenform auskommt:

\begin{proof}[Zweiter Beweis für \autoref{labasic/cha02:rank-gleich}]
  Definiere $S \df \setj{A_{\ast j}}{n}$.
  Es gilt:
  \begin{IEEEeqnarray*}{0l"s+x*}
    \text{$(A,b)$ lösbar} \\
    \eqin \iff
    b \in \spann(S) \\
    \eqin \iff
    \spann(S) = \spann(S \cup \set{b}) & (Übung) \\
    \eqin \iff
    \dim(\spann(S)) = \dim(\spann(S \cup \set{b})) & \autoref{labasic/cha01:dim-gleich} \\
    \eqin \iff
    \rank(A) = \rank(A\vert b)
    && \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\begin{para}[Was fehlt noch?]
  Mit diesen beiden Sätzen wissen wir, wie der unlösbare vom lösbaren Fall unterschieden werden kann,
  und wir finden auch sofort eine Lösung im lösbaren Fall.
  Es fehlt noch eine Basis von $\ker(A)$.
  Diese liefert der folgende Satz.
  Die Idee ist, die Standardbasis vom $K^k$ zu nehmen
  und oben auf jeden Standardbasisvektor noch $r$ geeignete Komponenten zu setzen.
  Man erhält so zunächst eine Basis von $\ker(A')$,
  daraus ergibt sich aber eine Basis von $\ker(A)$
  durch die bereits beschriebene Vertauschung der Komponenten entsprechend den
  beim Übergang von $A\vert b$ zu $A'\vert b'$ durchgeführten Spaltenvertauschungen (in umgekehrter Reihenfolge).
  Die gewünschte Basis kann man sehr einfach an der kanonischen Zeilenstufenform ablesen:
\end{para}

\begin{satz}[Ablesen einer Kernbasis]
  Die Spalten der folgenden Matrix bilden eine Basis von $\ker(A')$:
  \begin{IEEEeqnarray*}{0l}
    B \df
    \begin{bmatrix}
      - s_{11} & \hdots & \hdots & \hdots & - s_{1k} \\
      \vdots & & & & \vdots \\
      \vdots & & & & \vdots \\
      \vdots & & & & \vdots \\
      - s_{r1} & \hdots & \hdots & \hdots & - s_{rk} \\
      1\\
      &\ddots\\
      &\!\!\text{\LARGE$0$}&\ddots&\,\,\text{\LARGE$0$}\\
      &&& \ddots \\
      &&&& 1
    \end{bmatrix}
    \in K^{n \times k}
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}
  Wegen der Standardbasis im unteren Teil ist $S \df \setj{B_{\ast j}}{k}$
  eine linear unabhängige Menge mit $k$ Elementen,
  genau so viele wie die Dimension von $\ker(A')$.
  Die Behauptung folgt also, wenn wir $S \subseteq \ker(A')$ zeigen.
  \par
  Es sei also $j \in \setn{k}$.
  Wir zeigen $B_{\ast j} \in \ker(A')$, also $(A'B_{\ast j})_i = 0$ für alle $i \in \setn{d}$.
  Dies ist klar für $r < i \leq d$,
  da für solches $i$ die \xte{i} Zeile von $A'$ nur aus Null\-/Einträgen besteht.
  Also sei $i \leq r$. Dann gilt:
  \begin{IEEEeqnarray*}{0l+x*}
    (A'B_{\ast j})_i = 1 \cdot (-s_{ij}) + s_{ij} = 0&\qedhere
  \end{IEEEeqnarray*}
\end{proof}

\section{Erreichen der kanonischen Zeilenstufenform}

\begin{para}[Einrichten der ersten Spalte]
  Wenn $A=0$, so ist nichts zu tun; es gilt dann $L(A,b) = K^n$ wenn $b=0$ und $L(A,b) = \emptyset$ sonst.
  Es sei also $A \neq 0$.
  Dann können wir \ggf durch Vertauschen von Spalten sicherstellen,
  dass es in der ersten Spalte einen Eintrag ungleich Null gibt.
  Durch Multiplikation dieser Zeile mit einem geeigneten Skalar machen wir diesen Eintrag zu~$1$.
  Dann vertauschen wir Zeilen so, dass diese~$1$ ganz oben steht.
  Schließlich addieren wir geeignete skalare Vielfache dieser neuen ersten Zeile zu allen anderen Zeilen,
  so dass unterhalb der~$1$ nur noch Nullen stehen. So bekommen wir sicher die folgende Form:
  \begin{IEEEeqnarray*}{0l}
    \begin{bmatrix}
      1\\
      0\\
      \vdots&&\text{\huge$\ast$}&&\\
      \vdots\\
      0
    \end{bmatrix}
  \end{IEEEeqnarray*}
  Der Stern deutet an, dass wir über die übrigen Werte noch nichts aussagen.
\end{para}

\begin{para}[Fortsetzung der Methode]
  Als nächstes schauen wir auf den Teilbereich
  der aus den Zeilen $2,\hdots,d$ und den Spalten $2,\hdots,n$ besteht.
  Wenn dort nur Nullen stehen, so haben wir schon die kanonische Zeilenstufenform erreicht!
  Sonst können wir \ggf durch Vertauschen von Spalten sicherstellen,
  dass sich in der zweiten Spalte auf oder unter der Diagonalen ein Eintrag ungleich Null befindet.
  Durch Vertauschen von Zeilen und Multiplikation mit einem Skalar bekommen wir so an die Stelle $(2,2)$ eine~$1$.
  Mit dieser können wir alle Einträge darüber und darunter zu Null machen.
  An der ersten Spalte ändert sich dabei nichts, da die Zeile,
  deren skalare Vielfache wir zu anderen Zeilen addieren,
  eine Null in der ersten Spalte hat. So bekommen wir die folgende Form:
  \begin{IEEEeqnarray*}{0l}
    \begin{bmatrix}
      1 & 0\\
      0 & 1 \\
      0 & 0 \\
      \vdots&\vdots&&\text{\huge$\ast$}&&\\
      \vdots&\vdots\\
      0&0
    \end{bmatrix}
  \end{IEEEeqnarray*}
  Auf diese Weise fahren wir fort, bis wir auf Zeile $d$ oder Spalte $n$ angekommen sind
  oder wir in dem Bereich rechts unten nur noch Nullen haben.
  In jedem Fall haben wir dann die kanonische Zeilenstufenform erreicht.
  Wir fassen diese Methode formal in \autoref{labasic/cha02:algo} zusammen.
\end{para}

\begin{algorithm}
  \caption{Erreichen der Kanonischen Zeilenstufenform}\label{labasic/cha02:algo}
  \KwIn{Matrix $A \vert b \in K^{d \times (n+1)}$, die \ggf verändert wird}
  $j \df 1$\;
  $s \df ()$\;
  \While{$j \leq \min\set{d,n}$}{%
    \If{$\exists i \geq j \holds A_{ij} \neq 0$}{%
      vertausche Zeile $i$ mit Zeile $j$\;
      skaliere Zeile $j$ mit $A_{jj}^{-1}$, so dass wir $A_{j,j}=1$ erreichen\;
      addiere Vielfache von Zeile $j$ zu anderen Zeilen um alle Einträge in Spalte~$j$ zu~$0$ zu machen,
      abgesehen von Eintrag $(j,j)$\;
      $j \df j + 1$\;
    }
    \ElseIf{$\exists i \geq j, t > j \holds A_{i,t} \neq 0$}{%
      vertausche Spalten $j$ und $t$\;
      füge $(j,t)$ rechts an $s$ an\;
    }
    \lElse{Schleife fertig}
  }
  \Return{bearbeitete Matrix $A \vert b$ und Tupel von Spaltenvertauschungen $s$}\;
\end{algorithm}

\begin{example}[Lösen eines LGS]
  \label{labasic/cha02:ex-1}
  Das LGS $(A,b)$ über $\RR$ sei gegeben durch:
  \begin{IEEEeqnarray*}{0l+l}
    A =
    \begin{bmatrix}
      20 & 28 & 18 & 22 & 15\\
      21 & 27 & 15 & 25 & 19\\
      18 & 22 & 11 & 39 & 22
    \end{bmatrix}
    &
    b = \ColVec{16 , 17 , 23}
  \end{IEEEeqnarray*}
  Die erweiterte Koeffizientenmatrix (also eine $(3 \times 6)$-Matrix)
  wird in kanonische Zeilenstufenform gebracht,
  wobei wir über die Spaltenvertauschungen buchführen:
  \begin{IEEEeqnarray*}{0l"s}
    \begin{gmatrix}[b]
      20 & 28 & 18 & 22 & 15 & 16\\
      21 & 27 & 15 & 25 & 19 & 17\\
      18 & 22 & 11 & 39 & 22 & 23
      \rowops
      \mult{0}{\cdot \tfrac{1}{20}}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & \tfrac{7}{5} & \tfrac{9}{10} & \tfrac{11}{10} & \tfrac{3}{4} & \tfrac{4}{5}\\
      21 & 27 & 15 & 25 & 19 & 17\\
      18 & 22 & 11 & 39 & 22 & 23
      \rowops
      \add[-21]{0}{1}
      \add[-18]{0}{2}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & \tfrac{7}{5} & \tfrac{9}{10} & \tfrac{11}{10} & \tfrac{3}{4} & \tfrac{4}{5}\\
      0 & -\tfrac{12}{5} & -\tfrac{39}{10} & \tfrac{19}{10} & \tfrac{13}{4} & \tfrac{1}{5}\\
      0 & -\tfrac{16}{5} & -\tfrac{26}{5} & \tfrac{96}{5} & \tfrac{17}{2} & \tfrac{43}{5}
      \rowops
      \mult{1}{\cdot -\tfrac{5}{12}}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & \tfrac{7}{5} & \tfrac{9}{10} & \tfrac{11}{10} & \tfrac{3}{4} & \tfrac{4}{5}\\
      0 & 1 & \tfrac{13}{8} & -\tfrac{19}{24} & -\tfrac{65}{48} & -\tfrac{1}{12}\\
      0 & -\tfrac{16}{5} & -\tfrac{26}{5} & \tfrac{96}{5} & \tfrac{17}{2} & \tfrac{43}{5}
      \rowops
      \add[-\tfrac{7}{5}]{1}{0}
      \add[\tfrac{16}{5}]{1}{2}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & 0 & -\tfrac{11}{8} & \tfrac{53}{24} & \tfrac{127}{48} & \tfrac{11}{12}\\
      0 & 1 & \tfrac{13}{8} & -\tfrac{19}{24} & -\tfrac{65}{48} & -\tfrac{1}{12}\\
      0 & 0 & 0 & \tfrac{50}{3} & \tfrac{25}{6} & \tfrac{25}{3}
      \rowops
    \end{gmatrix}&Spaltentausch $(3,4)$\\
    \begin{gmatrix}[b]
      1 & 0 & \tfrac{53}{24} & -\tfrac{11}{8} & \tfrac{127}{48} & \tfrac{11}{12}\\
      0 & 1 & -\tfrac{19}{24} & \tfrac{13}{8} & -\tfrac{65}{48} & -\tfrac{1}{12}\\
      0 & 0 & \tfrac{50}{3} & 0 & \tfrac{25}{6} & \tfrac{25}{3}
      \rowops
      \mult{2}{\cdot \tfrac{3}{50}}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & 0 & \tfrac{53}{24} & -\tfrac{11}{8} & \tfrac{127}{48} & \tfrac{11}{12}\\
      0 & 1 & -\tfrac{19}{24} & \tfrac{13}{8} & -\tfrac{65}{48} & -\tfrac{1}{12}\\
      0 & 0 & 1 & 0 & \tfrac{1}{4} & \tfrac{1}{2}
      \rowops
      \add[\tfrac{19}{24}]{2}{1}
      \add[-\tfrac{53}{24}]{2}{0}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & 0 & 0 & -\tfrac{11}{8} & \tfrac{67}{32} & -\tfrac{3}{16}\\
      0 & 1 & 0 & \tfrac{13}{8} & -\tfrac{37}{32} & \tfrac{5}{16}\\
      0 & 0 & 1 & 0 & \tfrac{1}{4} & \tfrac{1}{2}
    \end{gmatrix}
  \end{IEEEeqnarray*}
  Wir lesen ab: $\rank(A) = 3$ und $\dim(\ker(A))=2$. Es gilt:
  \begin{IEEEeqnarray*}{0l}
    L(A',b') = \set[big]{\colvec{-\tfrac{3}{16}\\\tfrac{5}{16}\\\tfrac{1}{2}\\0\\0}
      + \lam_1 \colvec{\tfrac{11}{8}\\-\tfrac{13}{8}\\0\\1\\0}
      + \lam_2 \colvec{-\tfrac{67}{32}\\\tfrac{37}{32}\\-\tfrac{1}{4}\\0\\1}
      \suchthat \lam_1, \lam_2 \in \RR}
  \end{IEEEeqnarray*}
  Um die Lösungsmenge zu $(A,b)$ zu erhalten, müssen wir nur noch die Vertauschung $(3,4)$ rückgängig machen:
  \begin{IEEEeqnarray*}{0l}
    L(A,b) = \set[big]{\colvec{-\tfrac{3}{16}\\\tfrac{5}{16}\\0\\\tfrac{1}{2}\\0}
      +\lam_1 \colvec{\tfrac{11}{8}\\-\tfrac{13}{8}\\1\\0\\0}
      + \lam_2 \colvec{-\tfrac{67}{32}\\\tfrac{37}{32}\\0\\-\tfrac{1}{4}\\1}
      \suchthat \lam_1, \lam_2 \in \RR}
  \end{IEEEeqnarray*}
  Das kann man natürlich auch so schreiben:
  \begin{IEEEeqnarray*}{0l}
    L(A,b) = \colvec{-\tfrac{3}{16}\\\tfrac{5}{16}\\0\\\tfrac{1}{2}\\0}
    +\spann\p[big]{\colvec{\tfrac{11}{8}\\-\tfrac{13}{8}\\1\\0\\0},
      \colvec{-\tfrac{67}{32}\\\tfrac{37}{32}\\0\\-\tfrac{1}{4}\\1}}
  \end{IEEEeqnarray*}
\end{example}

\begin{example}[Lösen eines LGS]
  Das LGS $(A,b)$ über $\RR$ sei gegeben durch:
  \begin{IEEEeqnarray*}{0l+l}
    A =
    \begin{bmatrix}
      13 & 4 & 12 &  6 & 7 \\
      8  & 1 &  6 &  2 & 3 \\
      2  & 1 &  2 &  0 & 3 \\
      9  & 6 & 12 & 12 & 3
    \end{bmatrix}
    &
    b = \bigcolvec{3\\2\\0\\3}
  \end{IEEEeqnarray*}
  Die erweiterte Koeffizientenmatrix (also eine $(4 \times 6)$-Matrix)
  wird in kanonische Zeilenstufenform gebracht,
  wobei wir in diesem Beispiel keine Spaltenvertauschungen benötigen:
  \begin{IEEEeqnarray*}{0l"s}
    \begin{gmatrix}[b]
      13 & 4 & 12 &  6 & 7 & 3 \\
      8  & 1 &  6 &  2 & 3 & 2 \\
      2  & 1 &  2 &  0 & 3 & 0 \\
      9  & 6 & 12 & 12 & 3 & 3
      \rowops
      \mult{2}{\cdot \tfrac{1}{2}}
      \swap{0}{2}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1  & \tfrac{1}{2} &  1 &  0 & \tfrac{3}{2} & 0 \\
      8  & 1 &  6 &  2 & 3 & 2 \\
      13 & 4 & 12 &  6 & 7 & 3 \\
      9  & 6 & 12 & 12 & 3 & 3
      \rowops
      \add[-8]{0}{1}
      \add[-13]{0}{2}
      \add[-9]{0}{3}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1  & \tfrac{1}{2} &  1 &  0 & \tfrac{3}{2} & 0 \\
      0  & -3 & -2 & 2 & -9 & 2 \\
      0  & -\tfrac{5}{2} & -1 &  6 & -\tfrac{25}{2} & 3 \\
      0  & \tfrac{3}{2} & 3 & 12 & -\tfrac{21}{2} & 3
      \rowops
      \mult{1}{\cdot -\tfrac{1}{3}}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1  & \tfrac{1}{2} &  1 &  0 & \tfrac{3}{2} & 0 \\
      0  & 1 & \tfrac{2}{3} & -\tfrac{2}{3} & 3 & -\tfrac{2}{3} \\
      0  & -\tfrac{5}{2} & -1 &  6 & -\tfrac{25}{2} & 3 \\
      0  & \tfrac{3}{2} & 3 & 12 & -\tfrac{21}{2} & 3
      \rowops
      \add[-\tfrac{1}{2}]{1}{0}
      \add[\tfrac{5}{2}]{1}{2}
      \add[-\tfrac{3}{2}]{1}{3}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1  & 0 & \tfrac{2}{3} & \tfrac{1}{3} & 0 & \tfrac{1}{3} \\
      0  & 1 & \tfrac{2}{3} & -\tfrac{2}{3} & 3 & -\tfrac{2}{3} \\
      0  & 0 & \tfrac{2}{3} & \tfrac{13}{3} & -5 & \tfrac{4}{3} \\
      0  & 0 & 2 & 13 & -15 & 4
      \rowops
      \mult{2}{\cdot \tfrac{3}{2}}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1  & 0 & \tfrac{2}{3} & \tfrac{1}{3} & 0 & \tfrac{1}{3} \\
      0  & 1 & \tfrac{2}{3} & -\tfrac{2}{3} & 3 & -\tfrac{2}{3} \\
      0  & 0 & 1 & \tfrac{13}{2} & -\tfrac{15}{2} & 2 \\
      0  & 0 & 2 & 13 & -15 & 4
      \rowops
      \add[-\tfrac{2}{3}]{2}{0}
      \add[-\tfrac{2}{3}]{2}{1}
      \add[-2]{2}{3}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1  & 0 & 0 & -4 & 5 & -1 \\
      0  & 1 & 0 & -5 & 8 & -2 \\
      0  & 0 & 1 & \tfrac{13}{2} & -\tfrac{15}{2} & 2 \\
      0  & 0 & 0 & 0 & 0 & 0
      \rowops
    \end{gmatrix}
  \end{IEEEeqnarray*}
  Wir lesen ab: $\rank(A) = 3$ und $\dim(\ker(A))=2$. Es gilt:
  \begin{IEEEeqnarray*}{0l}
    L(A,b) = \ColVec{-1 , -2 , 2 , 0 , 0}
    +\spann\p[big]{\ColVec{4 , 5 , -\frac{13}{2} , 1 , 0},
      \ColVec{-5 , -8 , \frac{15}{2} , 0 , 1}}
  \end{IEEEeqnarray*}
\end{example}

\section{Weitere Anwendungen}

\begin{para}[Lineare Unabhängigkeit überprüfen]
  Es sei $X = \seti{x_{i}}{r} \subseteq K^{n}$ endlich.
  Mithilfe der in diesem Kapitel vorgestellten Mittel kann man in systematischer Weise überprüfen,
  ob $X$ linear unabhängig ist.
  Definiere:
  \begin{IEEEeqnarray*}{0l}
    A = \RowVec{x_{1} , \hdots , x_{r}} \in K^{n \times r}
  \end{IEEEeqnarray*}
  Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    \rank(A) = r \iff \dim(\colspace(A)) = r \\
    \iff \text{$X$ ist Basis von $\colspace(A)$}
    \iff \text{$X$ ist linear unabhängig}
  \end{IEEEeqnarray*}
  Den Rang von $A$ erhält man, indem man $A$ auf kanonische Zeilenstufenform bringt.
  Wir demonstrieren diese Methode anhand der Menge aus \autoref{labasic/cha01:example-lu}:
  \begin{IEEEeqnarray*}{0l"s}
    \begin{gmatrix}[b]
      3 & 0 & 1 \\
      2 & 0 & 2 \\
      1 & -1 & 1
      \rowops
      \swap{0}{2}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & -1 & 1 \\
      2 & 0 & 2 \\
      3 & 0 & 1
      \rowops
      \add[-2]{0}{1}
      \add[-3]{0}{2}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & -1 & 1 \\
      0 & 2 & 0 \\
      0 & 3 & -2
      \rowops
      \mult{1}{\cdot \tfrac{1}{2}}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & -1 & 1 \\
      0 & 1 & 0 \\
      0 & 3 & -2
      \rowops
      \add[-1]{1}{0}
      \add[-3]{1}{2}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & 0 & 1 \\
      0 & 1 & 0 \\
      0 & 0 & -2
      \rowops
      \mult{2}{\cdot -\tfrac{1}{2}}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & 0 & 1 \\
      0 & 1 & 0 \\
      0 & 0 & 1
      \rowops
      \add[-1]{2}{0}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & 0 & 0 \\
      0 & 1 & 0 \\
      0 & 0 & 1
    \end{gmatrix}
  \end{IEEEeqnarray*}
  Damit haben wir das Resultat aus \autoref{labasic/cha01:example-lu} bestätigt.
  \par
  Übung: Funktioniert die Methode auch,
  wenn wir die Vektoren als \emphasis{Zeilen} in die Matrix schreiben?
\end{para}

\begin{para}[Koordinatenabbildung berechnen]
  Es sei $B \in (K^{n})^{n}$ eine Basis von $K^{n}$.
  Wie können wir $\kappa^{B}(v)$ für ein gegebenes $v \in K^{n}$ berechnen?
  Wir schreiben die Vektoren von $B$ als Spalten in eine Matrix:
  \begin{IEEEeqnarray*}{0l}
    A = \RowVec{B_{1} , \hdots , B_{n}} \in K^{n \times n}
  \end{IEEEeqnarray*}
  An der Spaltenkombinationsformel sieht man, dass für alle $x \in K^{n}$ gilt:
  \begin{IEEEeqnarray*}{0l}
    x = \kappa^{B}(v) \iff Ax = v
  \end{IEEEeqnarray*}
  Damit ist klar: $L(A,v)$ besteht aus genau einem Element,
  und dies ist $\kappa^{B}(v)$.
  Mit unserer Methode zur Berechnung von $L(A,v)$ können wir also $\kappa^{B}(v)$ berechnen.
  \par
  Was ist, wenn wir danach $\kappa^{B}(w)$ für ein $w \in K^{n}$ mit $w \neq v$ brauchen?
  Offenbar kann man dazu $L(A,w)$ berechnen.
  Bei näherem Hinsehen kommt das aber unnötig umständlich vor:
  Bis auf die letzte Spalte sind die Operationen genau dieselben wie bei der Berechnung von $L(A,v)$.
  Es wäre gut, wenn wir diese Operationen irgendwie speichern könnten
  und dann nur auf den relevanten Vektor, nun also auf~$w$, anwenden.
  Es stellt sich heraus, dass wir alle diese Operationen
  in einer Matrix $\widetilde{A} \in K^{n \times n}$ speichern können,
  so dass $\kappa^{B}(x) = \widetilde{A} x$ für alle $x \in K^{n}$ gilt.
  Das ist insofern keine Überraschung,
  als dass wir schon wissen, dass $\kappa^{B} \in \cL(K^{n}, K^{n})$ gilt,
  also ist $\widetilde{A}$ einfach nur $M_{\kappa^{B}}$, siehe \autoref{labasic/cha01:matrix-la}.
  Die Frage ist also nur noch, wie wir $\widetilde{A}$ aus $B$ berechnen können.
  Das lernen wir im nächsten Kapitel.
\end{para}
