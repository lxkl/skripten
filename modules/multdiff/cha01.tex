\chapter{Differenzierbarkeit}
\label{multdiff/cha01}

\section*{Notation}

Es sei $\KK \in \set{\RR, \CC}$.
Es ist $\KK^{d \times n}$ die Menge der Matrizen mit $d$ Zeilen und $n$ Spalten mit Einträgen aus $\KK$.
Wir schreiben Matrizen mit eckigen Klammern:
\begin{IEEEeqnarray*}{0l}
  A =
  \begin{bmatrix}
    A_{11} & \hdots & A_{1n} \\
    \vdots & & \vdots \\
    A_{d1} & \hdots & A_{dn}
  \end{bmatrix}
  \in \KK^{d \times n}
\end{IEEEeqnarray*}
Spezialfälle von Matrizen sind  Zeilenvektoren und Spaltenvektoren:
\begin{IEEEeqnarray*}{0l+l}
  B = \RowVec{B_{11} , {\hdots} , B_{1n}} \in \KK^{1 \times n} &
  C = \ColVec{C_{11} , {\vdots} , C_{n1}} \in \KK^{n \times 1}
\end{IEEEeqnarray*}
Den redundanten Index $1$ lässt man dabei auch fort,
also $B_{i} = B_{1i}$ und $C_{i} = C_{i1}$ für alle $i \in \setn{n}$.
Spaltenvektoren identifizieren wir mit den bekannten Tupeln,
die horizontal, mit \emphasis{runden} Klammern und mit Kommata geschrieben werden.\footnote{%
  Das wird auch in~\cite{BoydVandenberghe09} so gemacht.}
Insgesamt also:
\begin{IEEEeqnarray*}{0l}
  \ColVec{C_{11} , {\vdots} , C_{n1}}
  = \ColVec{C_{1} , {\vdots} , C_{n}}
  = (C_{1}, \hdots, C_{n})
\end{IEEEeqnarray*}
In diesem Sinne ist also $\KK^{n \times 1} = \KK^{n}$.

Für alle $v,w \in \RR^{n}$ gilt $v\tran w = v \bigcdot w = \sum_{i=1} v_{i} w_{i}$;
es bezeichnet $\bigcdot$ also das Standardskalarprodukt.

\section{Grundlagen in normierten Räumen}

Es gelte in diesem Abschnitt, wenn nicht anders ausgewiesen:
\begin{itemize}
\item $V$ und $W$ sind normierte Räume über $\KK \in \set{\RR, \CC}$.
\item $\Om \subseteq V$
\item $v \in \Om$ ist innerer Punkt von $\Om$ in $V$,
  d.h. $\Om \in \nsets_{V}(v)$;
  man sieht leicht (Übung), dass $v$ dann Häufungspunkt von $\Om$ in $V$ ist.
\item $\ffn{f}{\Om}{W}$
\end{itemize}

\begin{para}[Differenzierbarkeit, Ableitung]
  Wir nennen $\ffn{f}{\Om}{W}$ \term{\diffbar{V,W}} in $v$,
  wenn es $\ffn{\phi}{V}{W}$ \linear{V,W} und \stetig{V,W}
  (d.h. $\phi \in L(V,W)$) so gibt, dass gilt:
  \begin{IEEEeqnarray*}{0l}
    \flimxx{\tiv}{v}{V,W} \frac{f(\tiv) - (f(v) + \phi(\tiv-v))}{\norm{\tiv - v}_{V}} = 0
  \end{IEEEeqnarray*}
  Äquivalent dazu ist offenbar:
  \begin{IEEEeqnarray*}{0l}
    \flimxx{\tiv}{v}{V,\RR} \frac{\norm{f(\tiv) - (f(v) + \phi(\tiv-v))}_{W}}{\norm{\tiv - v}_{V}} = 0
  \end{IEEEeqnarray*}
  Man sagt dazu: Die Funktion $\fnx{V}{W}{\tiv \mapsto f(v) + \phi(\tiv-v)}$
  approximiert $f$ in der Nähe von $v$ so, dass der Approximationsfehler schneller gegen~$0$
  geht als der Abstand zu $v$.
  Eine Funktion $\phi$ mit diesen Eigenschaften nennen wir eine \term{\Ableitung{V,W}} von $f$ in~$v$.
  Wir sagen, $f$ ist \term{\diffbar{V,W}} auf einer offenen Menge $U \subseteq \Om$,
  wenn $f$ \diffbar{V,W} in jedem Punkt von $U$ ist.
  Wir sagen, $f$ ist \term{\diffbar{V,W}},
  wenn $\Om$ offen ist und $f$ \diffbar{V,W} auf $\Om$ ist.
\end{para}

\begin{para}[Inkrementversion]
  Definiere:
  \begin{IEEEeqnarray*}{0l}
    \Om - v \df \set{x-v \suchthat x \in \Om}
    = \set{y \in V \suchthat v+y \in \Om}
  \end{IEEEeqnarray*}
  Man sieht leicht (Übung), dass $0$ ein innerer Punkt von $\Om - v$ in $V$ ist.
  Ferner ist $f$ \diffbar{V,W} in $v$ genau dann,
  wenn es $\phi \in L(V,W)$ so gibt, dass gilt:
  \begin{IEEEeqnarray}{0l}
    \label{multdiff/cha01:diff-h}
    \flimxx{h}{0}{V,W} \frac{f(v+h) - (f(v) + \phi(h))}{\norm{h}_{V}} = 0
  \end{IEEEeqnarray}
  Äquivalent dazu ist offenbar:
  \begin{IEEEeqnarray*}{0l}
    \flimxx{h}{0}{V,\RR} \frac{\norm{f(v+h) - (f(v) + \phi(h))}_{W}}{\norm{h}_{V}} = 0
  \end{IEEEeqnarray*}
  Wir werden meistens mit einer dieser Inkrementversionen arbeiten.
  Nach unseren Konventionen über den Funktionslimes einer implizit gegebenen Funktion
  bedeutet \eqref{multdiff/cha01:diff-h}:
  \begin{IEEEeqnarray*}{0l}
    \forall \seq{h_{k}}{k} \in \seqset{\nz{(\Om - v)}}
    \with \limxx{k}{V} h_{k} = 0 \holds
    \limxx{k}{W} \frac{f(v+h_{k}) - (f(v) + \phi(h_{k}))}{\norm{h_{k}}_{V}} = 0
  \end{IEEEeqnarray*}
\end{para}

\begin{para}[Vergleich mit bekanntem Begriff]
  Wir schauen uns die Differenzierbarkeit kurz für $V=W=\RR$
  als Vektorräume über $\RR$ mit der Betragsnorm an.
  Jede \lineare{V,W} Abbildung hat dann die Form $\fnx{\RR}{\RR}{t \mapsto a t}$ mit $a \in \RR$
  und ist automatisch stetig.
  Es folgt (wir lassen $(\RR,\RR)$ am $\lim$ fort):
  \begin{IEEEeqnarray*}{0l}
    \text{$f$ ist \diffbar{\RR,\RR} in $v$ im oben neu eingeführten Sinne} \\*
    \eqin\iff \exists a \in \RR \holds
    \flim{h}{0} \frac{\abs{f(v+h) - (f(v) + a h)}}{\abs{h}} = 0 \\*
    \eqin\iff \exists a \in \RR \holds
    \flim{h}{0} \frac{f(v+h) - (f(v) + a h)}{h} = 0 \\*
    \eqin\iff \exists a \in \RR \holds
    \flim{h}{0} \frac{f(v+h) - f(v)}{h} - \frac{a h}{h} = 0 \\*
    \eqin\iff \exists a \in \RR \holds \flim{h}{0} \frac{f(v+h) - f(v)}{h} - a = 0 \\*
    \eqin\iff \exists a \in \RR \holds \flim{h}{0} \frac{f(v+h) - f(v)}{h} = a \\*
    \eqin\iff \text{$f$ ist differenzierbar in $v$ im bekannten Sinne}
  \end{IEEEeqnarray*}
  Im Falle der Differenzierbarkeit ist die gesuchte lineare Abbildung also
  $\fnx{\RR}{\RR}{t \mapsto f'(v) t}$.
\end{para}

\begin{satz}[Eindeutigkeit der Ableitung]
  \label{multdiff/cha01:eind}
  Es sei $f$ \diffbar{V,W} in $v$.
  Dann ist die \Ableitung{V,W} von $f$ in $v$ eindeutig bestimmt.
\end{satz}

\begin{proof}
  Für einen Widerspruchsbeweis nehmen wir an,
  es seien $\phi, \psi$ \Ableitungen{V,W}
  von $f$ in $v$ mit $\phi \neq \psi$.
  Dann gibt es $x \in \nz{V}$ so, dass $\phi(x) \neq \psi(x)$ gilt.
  Da $v$ innerer Punkt von $\Om$ in $V$ ist,
  ist $0$ innerer Punkt von $\Om - v$ in $V$, und somit
  gilt $\seq{\frac{1}{k} x}{k \geq \mu} \in \seqset{\Om - v}$
  für $\mu$ groß genug.
  Außerdem ist $\limxx{k}{V} \frac{1}{k} x = 0$.
  Es folgt ein Widerspruch:
  \begin{IEEEeqnarray*}{0l+x*}
    0 = \limxx{k}{W} \frac{f(v+\frac{1}{k}x) - (f(v) + \phi(\frac{1}{k} x))}{\norm{\frac{1}{k} x}_{V}}
    - \frac{f(v+\frac{1}{k}x) - (f(v) + \psi(\frac{1}{k} x))}{\norm{\frac{1}{k} x}_{V}} \\
    \eqin = \limxx{k}{W} \frac{\psi(x) - \phi(x)}{\norm{x}_{V}} \neq 0
    & \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\begin{para}[Notation der Ableitung]
  Wenn $f$ in $v$ \diffbar{V,W} ist,
  dann bezeichnen wir die eindeutig bestimmte \Ableitung{V,W} von $f$ in $v$ mit $D_{(V,W)}f(v)$.
  Es ist dann also $\ffn{D_{(V,W)}f(v)}{V}{W}$ \linear{V,W} und \stetig{V,W},
  kurz geschrieben $D_{(V,W)}f(v) \in L(V,W)$.
\end{para}

\begin{satz}
  Differenzierbare Funktionen sind stetig.
  \Genauer
  Wenn $f$ in $v$ \diffbar{V,W} ist, dann ist $f$ in $v$ \stetig{V,W}.
\end{satz}

\begin{proof}
  Wegen $\flimxx{h}{0}{V,\RR} \frac{1}{\norm{h}_{V}} = \pinfty$ gilt:
  \begin{IEEEeqnarray*}{0l}
    \flimxx{h}{0}{V,W} f(v+h) - (f(v) + D_{(V,W)}f(v)(h)) = 0
  \end{IEEEeqnarray*}
  Wegen $\flimxx{h}{0}{V,W} D_{(V,W)}f(v)(h) = 0$
  folgt daraus $\flimxx{h}{0}{V,W} f(v+h) - f(v) = 0$,
  also $\flimxx{h}{0}{V,W} f(v+h) = f(v)$.
\end{proof}

\section{Ableitungsregeln in normierten Räumen}

In diesem Abschnitt gelten dieselben Voraussetzungen wie im vorigen.
Den Zusatz \enquote{$(V,W)$} lassen wir im Sinne der Übersichtlichkeit ab jetzt meistens fort,
also wir sagen zum Beispiel \enquote{differenzierbar} anstelle von \enquote{\diffbar{V,W}},
und wir schreiben $Df(v)$ anstelle von $D_{(V,W)} f(v)$.
Sind andere normierte Räume im Spiel, dann geben wir diese aber nach wie vor explizit an.

\begin{satz}[Komponentensatz der Differenzierbarkeit]
  Es sei der Fall ${W = (\KK^{d},\normfn)}$ gegeben,
  es ist also $\normfn$ eine Norm auf $\KK^{d}$.
  Dann sind äquivalent:
  \begin{enumerate}
  \item $f$ ist differenzierbar in $v$.
  \item Jede Komponentenfunktion von $f$ ist \diffbar{V,\KK} in $v$.
  \end{enumerate}
  \Ggf gilt $Df(v) = (D_{(V,\KK)}f_{1}(v), \hdots, D_{(V,\KK)}f_{d}(v))$,
  also die Komponentenfunktionen der Ableitung sind die Ableitungen der Komponentenfunktionen.
\end{satz}

\begin{proof}
  Übung (folgt leicht aus dem Komponentensatz der Konvergenz).
\end{proof}

\begin{para}[Notation der Skalarmultiplikation]
  Für den folgenden Kombinationssatz ist es hilfreich,
  für alle $\lam \in \KK$ und alle $w \in W$ zu definieren $w \lam \df \lam w$.
  Wenn $W = \KK^{d}$, dann gilt ohnehin $\lam w = w \lam$,
  wenn wir auf der rechten Seite $\lam$ als $(1 \times 1)$\=/Matrix auffassen.
\end{para}

\begin{satz}[Kombinationssatz für Differenzierbarkeit]
  \label{multdiff/cha01:kombi}
  Es sei $f$ differenzierbar in~$v$.
  \begin{enumerate}
  \item Es sei $\ffn{g}{\Om}{W}$ differenzierbar in $v$ und $\lam \in \KK$.
    Dann sind $\lam f$ und $f + g$ differenzierbar in $v$,
    und es gilt:
    \begin{IEEEeqnarray*}{0l+l}
      D(f + g)(v) = Df(v) + Dg(v) &
      D(\lam f)(v) = \lam Df(v)
    \end{IEEEeqnarray*}
  \item Es sei $\ffn{\lam}{\Om}{\KK}$ \diffbar{V,\KK} in~$v$.
    Dann ist $\lam f$ differenzierbar in~$v$,
    und es gilt:
    \begin{IEEEeqnarray*}{0l}
      D(\lam f)(v) = f(v) D_{(V,\KK)}\lam(v) + \lam(v) Df(v)
    \end{IEEEeqnarray*}
    Mit der rechten Seite ist folgende lineare Abbildung gemeint:
    \begin{IEEEeqnarray*}{0l}
      \fnx{V}{W}{h \mapsto
        \underbrace{f(v)}_{\in W}
        \underbrace{D_{(V,\KK)}\lam(v)(h)}_{\in \KK}
        + \underbrace{\lam(v)}_{\in \KK}
        \underbrace{Df(v)(h)}_{\in W}}
    \end{IEEEeqnarray*}
    (Übung: Man mache sich klar, dass diese Abbildung tatsächlich linear ist.)
  \item Es sei $\ffn{\lam}{\Om}{\nz{\KK}}$ \diffbar{V,\KK} in~$v$.
    Dann ist $\frac{1}{\lam}$ \diffbar{V,\KK} in $v$,
    und es gilt:
    \begin{IEEEeqnarray*}{0l}
      D\frac{1}{\lam}(v) = - \frac{D_{(V,\KK)}\lam(v)}{(\lam(v))^{2}}
    \end{IEEEeqnarray*}
  \item Es sei $\ffn{\lam}{\Om}{\nz{\KK}}$ \diffbar{V,\KK} in~$v$.
    Dann ist $\frac{f}{\lam}$ \diffbar{V,\KK} in~$v$,
    und es gilt:
    \begin{IEEEeqnarray*}{0l}
      D\frac{f}{\lam}(v) = \frac{\lam(v) Df(v) - f(v) D_{(V,\KK)}\lam(v)}{(\lam(v))^{2}}
    \end{IEEEeqnarray*}
  \item (Kettenregel)
    Es sei $W'$ ein normierter Raum über $\KK$ und $\ffn{g}{\Om_{g}}{W'}$ so,
    dass ${\img(f) \subseteq \Om_{g}} \subseteq W$ gilt
    und $f(v)$ ein innerer Punkt von $\Om_{g}$ in $W'$ ist.
    Es möge $g$ \diffbar{W,W'} in $f(v)$ sein.
    Dann ist $g \circ f$ \diffbar{V,W'} in~$v$,
    und es gilt:
    \begin{IEEEeqnarray*}{0l}
      D_{(V,W')}(g \circ f)(v) = D_{(W,W')}g(f(v)) \circ Df(v)
    \end{IEEEeqnarray*}
  \end{enumerate}
\end{satz}

\begin{proof}
  \begin{enumerate}
  \item Übung (man lese zunächst die Beweise der folgenden Punkte).
  \item Es gilt:\footnote{Es ist hier ohne Gefahr,
      auf eine explizite Allquantifizierung für $h$ zu verzichten.}
    \begin{IEEEeqnarray*}{0l}
      (\lam f)(v+h) - \p[big]{(\lam f)(v) + \p{f(v) D_{(V,\KK)}\lam(v) + \lam(v) Df(v)}(h)} \\
      \eqin = \lam(v+h) f(v+h) - \lam(v) f(v)
      - f(v) D_{(V,\KK)}\lam(v)(h) - \lam(v) Df(v)(h) \\
      \eqin = \p[big]{\lam(v+h) - \lam(v)} f(v+h)
      - f(v) D_{(V,\KK)}\lam(v)(h) \\
      \eqincont{=} + \lam(v) \p[big]{f(v+h) - f(v) - Df(v)(h)} \\
      \eqin = \p[big]{\lam(v+h) - \lam(v) - D_{V,\KK}\lam(v)(h)} f(v+h) \\
      \eqincont{=} - D_{(V,\KK)}\lam(v)(h) \p[big]{f(v)-f(v+h)} \\
      \eqincont{=} + \lam(v) \p[big]{f(v+h) - f(v) - Df(v)(h)}
    \end{IEEEeqnarray*}
    Wir schauen uns die letzten drei Zeilen einzeln an
    und wie sie auf Division mit $\norm{h}_{V}$ im Limes $h \tends 0$ reagieren.
    Für die erste Zeile gilt:
    \begin{IEEEeqnarray*}{0l}
      \frac{\lam(v+h) - \lam(v) - D_{V,\KK}\lam(v)(h)}{\norm{h}_{V}} f(v+h)
      \tendsasto{h}{0} 0 f(v) = 0
    \end{IEEEeqnarray*}
    Für die zweite Zeile gilt:
    \begin{IEEEeqnarray*}{0l}
      \frac{\norm{D_{(V,\KK)}\lam(v)(h) \p[big]{f(v)-f(v+h)}}_{W}}{\norm{h}_{V}} \\
      \eqin = \frac{\abs{D_{(V,\KK)}\lam(v)(h)}}{\norm{h}_{V}} \norm{f(v)-f(v+h)}_{W} \\
      \eqin \leq \norm{D_{(V,\KK)}\lam(v)}_{\op(V,\KK)} \norm{f(v)-f(v+h)}_{W} \\
      \eqin \tendsasto{h}{0} \norm{D_{(V,\KK)}\lam(v)}_{\op(V,\KK)} \ccdot 0 = 0
    \end{IEEEeqnarray*}
    Für die dritte Zeile gilt:
    \begin{IEEEeqnarray*}{0l}
      \lam(v) \frac{f(v+h) - f(v) - Df(v)(h)}{\norm{h}_{V}}
      \tendsasto{h}{0} \lam(v) 0 = 0
    \end{IEEEeqnarray*}
  \item Es gilt:
    \begin{IEEEeqnarray*}{0l}
      \frac{1}{\lam}(v+h)
      - \p[big]{\frac{1}{\lam}(v) - \frac{D_{(V,\KK)}\lam(v)}{(\lam(v))^{2}}(h)} \\
      \eqin = \frac{1}{\lam(v+h)}
      - \frac{1}{\lam(v)} + \frac{D_{(V,\KK)}\lam(v)(h)}{(\lam(v))^{2}} \\
      \eqin = \frac{\lam(v)-\lam(v+h)}{\lam(v)\lam(v+h)}
      + \frac{D_{(V,\KK)}\lam(v)(h)}{(\lam(v))^{2}} \\
      \eqin = \frac{\lam(v)-\lam(v+h) + D_{(V,\KK)}\lam(v)(h)}{\lam(v)\lam(v+h)}
      - \frac{D_{(V,\KK)}\lam(v)(h)}{\lam(v)\lam(v+h)}
      + \frac{D_{(V,\KK)}\lam(v)(h)}{(\lam(v))^{2}} \\
      \eqin = \frac{\lam(v)-\lam(v+h) + D_{(V,\KK)}\lam(v)(h)}{\lam(v)\lam(v+h)}
      - \frac{\lam(v) - \lam(v+h)}{(\lam(v))^{2}\lam(v+h)} D_{(V,\KK)}\lam(v)(h)
    \end{IEEEeqnarray*}
    Wir schauen uns die beiden Terme einzeln an
    und wie sie auf Division mit $\norm{h}_{V}$ im Limes $h \tends 0$ reagieren.
    Für den ersten Term gilt:
    \begin{IEEEeqnarray*}{0l}
      \frac{1}{\lam(v)\lam(v+h)} \frac{\lam(v)-\lam(v+h) + D_{(V,\KK)}\lam(v)(h)}{\norm{h}_{V}}
      \tendsasto{h}{0} \frac{1}{(\lam(v))^{2}} 0 = 0
    \end{IEEEeqnarray*}
    Für den zweiten Term gilt:
    \begin{IEEEeqnarray*}{0l}
      \frac{\norm{\frac{\lam(v) - \lam(v+h)}{(\lam(v))^{2}\lam(v+h)} D_{(V,\KK)}\lam(v)(h)}_{W}}%
      {\norm{h}_{V}}
      \leq \abs{\frac{\lam(v) - \lam(v+h)}{(\lam(v))^{2}\lam(v+h)}}
      \ccdot \norm{D_{(V,\KK)}\lam(v)}_{\op(V,\KK)} \\
      \eqin \tendsasto{h}{0} \abs{\frac{0}{(\lam(v))^{3}}}
      \ccdot \norm{D_{(V,\KK)}\lam(v)}_{\op(V,\KK)} = 0
    \end{IEEEeqnarray*}
  \item Folgt leicht aus den beiden vorigen Punkten (Übung).
  \item Ähnlich wie oben, aber etwas aufwändiger; hier nicht.\qedhere
  \end{enumerate}
\end{proof}

\section{Partielle Ableitungen}

Bis auf Weiteres betrachten wir die Differenzierbarkeit nur für normierte Räume
der Form $(\RR^{n}, \normfn)$ über $\RR$.
Dabei ist es wegen der endlichen Dimension unerheblich,
welche Norm wir verwenden.
Es ist nun auch nicht mehr nötig,
die normierten Räume bei Konvergenz, Funktionslimes, Stetigkeit, Differenzierbarkeit, Linearität
und den topologischen Begriffen (innerer Punkt, offene Menge, \etc) anzuführen.
Also zum Beispiel statt \stetig{\RR^n,\RR^d} oder \stetig{\RR^n, \RR}
sagen wir einfach \enquote{stetig}.
Wir schreiben auch einfach $Df(v)$ für die Ableitung einer Funktion $f$ in $v$.

Kugeln und Umgebungen benötigen wir meistens bezüglich $\normfn_{\infty}$,
wir schreiben $B_{n,\infty}$ und $\nsets_{n,\infty}$ anstelle von
$B_{(\RR^{n}, \normfn_{\infty})}$ \bzw $\nsets_{(\RR^{n}, \normfn_{\infty})}$.

Es gelte in diesem Abschnitt, wenn nicht anders ausgewiesen:
\begin{itemize}
\item $\Om \subseteq \RR^{n}$
\item $v \in \Om$ ist innerer Punkt von $\Om$, d.h. $\Om \in \nsets_{n,\infty}(v)$.
\item $\ffn{f}{\Om}{\RR^{d}}$
\end{itemize}

\begin{para}[Alle Variablen bis auf eine fixieren]
  \label{multdiff/cha01:partial-1}
  Für alle $i \in \setn{n}$ und alle $t \in \RR$ definiere:
  \begin{IEEEeqnarray*}{0l}
    v^{i,t} \df (\eli{v}{i-1}, t ,\elix{v}{i+1}{n})
  \end{IEEEeqnarray*}
  Es ist $v^{i,t}$ also $v$, wo die \xte{i} Komponente gegen $t$ ausgetauscht wird.
  Für alle $j \in \setn{d}$ definiere:
  \begin{IEEEeqnarray*}{0l+l}
    \Om_{v,i} \df \set{t \in \RR \suchthat v^{i,t} \in \Om} &
    \ffnx{f_{j,v,i}}{\Om_{v,i}}{\RR}{t \mapsto f_{j}(v^{i,t})}
  \end{IEEEeqnarray*}
  Wenn $f$ nur eine Komponentenfunktion hat (d.h. wenn $d=1$ gilt),
  so schreibe natürlich $f_{v,i} \df f_{1,v,i}$.
  \par
  Bemerke, dass $f_{j,v,i}$ keinerlei Abhängigkeit von $v_{i}$ hat,
  denn die \xte{i} Komponente in $v$ wird ja durch das Argument der Funktion ersetzt.
  Auch für die Menge $\Om_{v,i}$ ist es egal, was $v_{i}$ ist.
\end{para}

\begin{para}[Partielle Differenzierbarkeit]
  \label{multdiff/cha01:partial-2}
  Es sei $i \in \setn{n}$ und $j \in \setn{d}$.
  Wir nennen $f_{j}$ in $v$ \term{partiell differenzierbar
  nach der \xten{i} Variable}, wenn $f_{j,v,i}$ in $v_{i}$ differenzierbar ist.
  Bemerke, dass $v_{i}$ ein innerer Punkt von $\Om_{v,i}$ ist,
  also insbesondere ein Häufungspunkt von $\Om_{v,i}$,
  da $v$ innerer Punkt von $\Om$ ist (Übung).
  Wenn $f_{j}$ in $v$ partiell differenzierbar nach der \xten{i} Variable ist, definieren wir
  \begin{IEEEeqnarray*}{0l}
    \partial_{i} f_{j} (v) \df f_{j,v,i}'(v_{i})
  \end{IEEEeqnarray*}
  und nennen dies die \term{partielle Ableitung}
  von $f_{j}$ \term{nach der \xten{i} Variable} in~$v$.
  Wenn $f$ nur eine Komponentenfunktion hat (d.h. wenn $d=1$ gilt),
  so schreibe natürlich $\partial_{i} f (v) \df \partial_{i} f_{1} (v)$.
\end{para}

\begin{example}
  \label{multdiff/cha01:ex-1}
  Definiere:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{f}{\RR \times \RRnn}{\RR^{4}}{(x,y) \mapsto (x+y, \, xy, \, x^{2} + 3y - 6, \, -2 \sqrt{y})}
  \end{IEEEeqnarray*}
  Dann gelten zum Beispiel für alle $(x,y) \in \RR \times \RRnn$:
  \begin{itemize}
  \item $\partial_{2} f_{1} (x,y) = 1$
  \item $\partial_{1} f_{2} (x,y) = y$
  \item $\partial_{1} f_{3} (x,y) = 2x$
  \item $\partial_{2} f_{3} (x,y) = 3$
  \item $\partial_{1} f_{4} (x,y) = 0$
  \end{itemize}
  Bei der Ableitung von $f_{4}$ nach der zweiten Variable muss man aufpassen,
  da die Wurzel in $0$ nicht differenzierbar ist.
  Wir haben aber $\partial_{2} f_{4}(x,y) = -\frac{1}{\sqrt{y}}$
  für alle $(x,y) \in \RR \times \RRpos$.
\end{example}

\begin{para}[Gradient und Jacobi\-/Matrix\protect\footnote{%
    Carl Gustav Jacob Jacobi, 1804--1851, deutscher Mathematiker.}]
  Wir nennen $f_{j}$ in $v$ \term{partiell differenzierbar},
  wenn $f_{j}$ in $v$ nach allen $n$ Variablen partiell differenzierbar ist.
  Wenn dem so ist,
  dann können wir den \term{Gradienten} von $f_{j}$ in $v$ definieren:
  \begin{IEEEeqnarray*}{0l}
    \grad{f_{j}} (v) \df (\partial_{1} f_{j}(v), \hdots, \partial_{n} f_{j}(v)) \in \RR^{n}
  \end{IEEEeqnarray*}
  Wenn $f$ nur eine Komponentenfunktion hat (d.h. wenn $d=1$ gilt),
  so schreibe natürlich $\grad{f} (v) \df \grad{f_{1}} (v)$.
  \par
  Wenn $f_{j}$ für alle $j \in \setn{d}$ partiell differenzierbar in~$v$ ist,
  dann nennen wir $f$ \term{partiell differenzierbar} in $v$
  und fassen alle partiellen Ableitungen von $f$ in $v$
  in der \term{Jacobi\-/Matrix} oder \term{Funktionalmatrix} von $f$ in~$v$ zusammen:
  \begin{IEEEeqnarray*}{0l}
    \Jacobian{f} (v) \df \begin{bmatrix}
      \partial_{1} f_{1} (v) & \hdots & \partial_{n} f_{1}(v) \\
      \vdots & & \vdots \\
      \partial_{1} f_{d} (v) & \hdots & \partial_{n} f_{d}(v)
    \end{bmatrix}
  \end{IEEEeqnarray*}
  Wir sagen, $f$ ist \term{partiell differenzierbar} auf einer offenen Menge $U \subseteq \Om$,
  wenn $f$ partiell differenzierbar in allen Punkten von $U$ ist.
  Wir sagen, $f$ ist \term{partiell differenzierbar},
  wenn $\Om$ offen ist und $f$ \term{partiell differenzierbar} auf $\Om$ ist.
\end{para}

\begin{para}[Merksatz zur Jacobi\-/Matrix]
  Es ist generell eine gute Idee, unter der \xten{j} Zeile einer Matrix $A \in \RR^{n \times d}$
  den Spaltenvektor ${A_{j\ast} \df (A_{j1}, \hdots, A_{jn}) \in \RR^{n}}$ zu verstehen.
  Damit gilt dann $(\Jacobian{f} (v))_{j\ast} = \grad{f_{j}}(v)$,
  und wir können uns merken:
  \emphasis{Die Zeilen der Jacobi\-/Matrix sind die Gradienten der Komponentenfunktionen.}
\end{para}

\begin{para}[Spezialfälle]
  \fakephantomsection\label{multdiff/cha01:spezial}
  \begin{description}[font=\it]
  \item[Von Dimension $1$ nach Dimension $d$:]
    Wir sind hier im Fall ${\Om \subseteq \RR}$.
    Die Jacobi\-/Matrix in $v$ besteht aus genau einer Spalte.
    In Zeile $i$ steht die partielle Ableitung von $f_{i}$ nach der ersten Variable in $v$,
    was $f_{i}'(v)$ ist.
    Daher schreiben wir in diesem Fall auch $f'(v) \df \Jacobian{f}(v) \in \RR^{d}$.
  \item[Von Dimension $n$ nach Dimension $1$:]
    Die Jacobi\-/Matrix in $v$ besteht nun aus genau einer Zeile,
    nämlich es ist $\Jacobian{f}(v) = (\grad{f} (v)) \tran$.
    Für alle $h \in \RR^{n}$ gilt $\Jacobian{f}(v) \, h = \grad{f} (v) \bigcdot h
    = h\tran \, \grad{f} (v)$.
    Wir verwenden oft die Schreibweise mit dem Standardskalarprodukt.
  \end{description}
\end{para}

\begin{para}[Lineare Abbildungen und Matrizen]
  Wir erinnern an den Zusammenhang zwischen linearen Abbildungen und Matrizen:
  \begin{itemize}
  \item Für alle $A \in \RR^{d \times n}$ ist $\fnx{\RR^{n}}{\RR^{d}}{u \mapsto Au}$ linear.
    Außerdem ist nach dem Kombinationssatz diese lineare Abbildung stetig.
  \item Für alle $\ffn{\phi}{\RR^{n}}{\RR^{d}}$ linear gibt es
    $M_{\phi} \in \RR^{d \times n}$ so, dass $\phi(u) = M_{\phi} \, u$ für alle $u \in \RR^{n}$ gilt.
    Daraus folgt nun auch, dass lineare Abbildungen zwischen den hier betrachteten normierten Räumen
    immer stetig sind.
  \end{itemize}
  Differenzierbarkeit von $f$ in $v$ ist nun also äquivalent dazu,
  dass es $A \in \RR^{d \times n}$ so gibt, dass gilt:
  \begin{IEEEeqnarray*}{0l}
    \flim{h}{0} \frac{f(v+h) - (f(v) + A h)}{\norm{h}} = 0
  \end{IEEEeqnarray*}
\end{para}

\begin{para}[Inkrementversion der partiellen Ableitungen]
  \label{multdiff/cha01:part-h}
  Natürlich können wir die Ableitung von $f_{j,v,i}(v_{i})$
  in Inkrementform ausdrücken, und dann können wir weiter umformen:
  \begin{IEEEeqnarray*}{0l}
    \partial_{i} f_{j} (v) = f_{j,v,i}'(v_{i})
    = \flimxx{h}{0}{\RR,\RR} \frac{f_{j,v,i}(v_{i}+h) - f_{j,v,i}(v_{i})}{h} \\
    \eqin = \flimxx{h}{0}{\RR,\RR} \frac{f_{j}(v^{i,v_{i}+h}) - f_{j}(v^{i,v_{i}})}{h}
    = \flimxx{h}{0}{\RR,\RR} \frac{f_{j}(v+h e_{i}) - f_{j}(v)}{h}
  \end{IEEEeqnarray*}
  Für alle $a \in \RR$ sind also äquivalent:
  \begin{itemize}
  \item $f_{j}$ ist in $v$ nach der \xten{i} Variable partiell differenzierbar
    und $\partial_{i} f_{j} (v) = a$.
  \item $\flimxx{h}{0}{\RR,\RR} \frac{f_{j}(v+h e_{i}) - (f_{j}(v) + ah)}{h} = 0$
  \end{itemize}
  Außerdem überlegt man sich leicht (Übung), dass diese Äquivalenz richtig bleibt,
  wenn man Letzteres ersetzt durch jeweils:
  \begin{itemize}
  \item $\flimxx{h}{0}{\RR,\RR} \frac{\abs{f_{j}(v+h e_{i}) - (f_{j}(v) + ah)}}{\abs{h}} = 0$
  \item $\flimxx{h}{0}{\RR,\RR} \frac{f_{j}(v+h e_{i}) - (f_{j}(v) + ah)}{\abs{h}} = 0$
  \end{itemize}
\end{para}

\begin{satz}[Jacobi\-/Matrix ist einziger Kandidat für Ableitung]
  Es sei $f$ differenzierbar in $v$.
  Dann ist $f$ partiell differenzierbar in~$v$, und es gilt:
  \begin{IEEEeqnarray*}{0l}
    M_{Df(v)} = \Jacobian{f}(v)
  \end{IEEEeqnarray*}
  Im Falle $d=1$ also $M_{Df(v)} = (\grad{f}(v)) \tran$.
\end{satz}

\begin{proof}
  Notiere $A \df M_{D f(v)}$.
  Es sei $i \in \setn{n}$ und $j \in \setn{d}$.
  Es gilt:
  \begin{IEEEeqnarray*}{0l}
    \frac{f_{j}(v+h e_{i}) - (f_{j}(v) + A_{ji} h)}{\abs{h}}
    = \frac{f_{j}(v+h e_{i}) - (f_{j}(v) + (A (h e_{i}))_{j})}{\abs{h}} \\
    \eqin = \frac{f_{j}(v+h e_{i}) - (f_{j}(v) + (A (h e_{i}))_{j})}{\norm{he_{i}}_{\infty}}
    \tendsastoxx{h}{0}{\RR,\RR} 0
  \end{IEEEeqnarray*}
  Letzteres gilt wegen $\flimxx{h}{0}{\RR,\RR^{n}} h e_{i} = 0$.
\end{proof}

\begin{para}[Umkehrung gilt nicht]
  Wir wissen nun also insbesondere,
  dass Differenzierbarkeit die partielle Differenzierbarkeit impliziert.
  Geht das auch in die andere Richtung? Nein!
  Es kann $f$ durchaus partiell differenzierbar in $v$ sein und gleichzeitig unstetig in~$v$,
  also insbesondere nicht differenzierbar in $v$.
  Ein einfaches Beispiel dafür ist:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{f}{\RR^{2}}{\RR}{(x,y) \mapsto
      \begin{cases}
        1 & x > 0 \land y = x^{2} \\
        0 & \text{sonst}
      \end{cases}}
  \end{IEEEeqnarray*}
  Dann gilt $f_{(0,0),i} = 0$ für alle $i \in \set{1,2}$,
  also sind die für die partiellen Ableitungen in $(0,0)$ zu betrachtenden Funktionen konstant~$0$
  und somit differenzierbar.
  Also ist $f$ in $(0,0)$ partiell differenzierbar.
  Gleichwohl ist $f$ nicht stetig in $(0,0)$,
  denn $\limx{k} f(\frac{1}{k}, \frac{1}{k^{2}}) = 1 \neq 0 = f(0,0)$.
  \par
  Der folgende Satz löst diese unbefriedigende Situation auf:
\end{para}

\begin{satz}
  \label{multdiff/cha01:stetig-partiell}
  Stetig partiell differenzierbare Funktionen sind differenzierbar.
  \Genauer
  Es sei $U \subseteq \Om$ eine offene Umgebung von $v$ so,
  dass $f$ partiell differenzierbar auf~$U$ ist, und alle partiellen Ableitungen stetig in~$v$ sind.
  Dann ist $f$ differenzierbar in $v$.
\end{satz}

\begin{proof}
  Wegen des Komponentensatzes genügt es, den Fall $d=1$ zu betrachten,
  also $\ffn{f}{\Om}{\RR}$.
  \par
  Finde $\veps > 0$ so, dass $B \df B_{n,\infty}(v, \veps) \subseteq U$ gilt.
  Definiere $H \df B_{n,\infty}(0, \veps)$.
  Für alle $h \in H$ und alle $l \in \setn{n+1}$ definiere:
  \begin{IEEEeqnarray*}{0l+l}
    v(h,l) \df v + (\eli{h}{l-1}, 0, \hdots, 0)
    = (v_{1}+h_{1}, \hdots, v_{l-1}+h_{l-1}, \, v_{l}, \hdots, v_{n})
  \end{IEEEeqnarray*}
  Spezialfälle:
  \begin{IEEEeqnarray*}{0l+l}
    v(h,1) = v & v(h,n+1) = v + h
  \end{IEEEeqnarray*}
  Für alle $h \in H$ und alle $l \in \setn{n}$
  und alle
  $t \in I(h,l) \df \intcc{v_{l}}{v_{l}+h_{l}} \cup \intcc{v_{l}+h_{l}}{v_{l}}$ definiere:
  \begin{IEEEeqnarray*}{0l}
    v(h,l,t) \df v(h,l)^{l,t} = (v_{1}+h_{1}, \hdots, v_{l-1}+h_{l-1}, \, t, \, v_{l+1}, \hdots, v_{n})
  \end{IEEEeqnarray*}
  Spezialfälle:
  \begin{IEEEeqnarray*}{0l+l}
    v(h,l,v_{l}) = v(h,l) &
    v(h,l,v_{l}+h_{l}) = v(h,l+1)
  \end{IEEEeqnarray*}
  Wir erinnern an die Notation aus \autoref{multdiff/cha01:partial-1}.
  Es gilt $f_{v(h,l),l}(t) = f(v(h,l,t))$ für alle $t \in I(h,l)$.
  Da $v(h,l,t) \in B$ für alle $t \in I(h,l)$ gilt und $f$ partiell differenzierbar auf $B$ ist,
  ist $f_{v(h,l),l}$ differenzierbar auf $I(h,l)$,
  mit Ableitung $\partial_{l} f(v(h, l, \cdot ))$.
  Nach dem Mittelwertsatz finde $\tau(h,l) \in I(h,l)$ so,
  dass gilt:
  \begin{IEEEeqnarray*}{0l}
    h_{l} \, \partial_{l} f(v(h,l,\tau(h,l)))
    = f(v(h,l,v_{l}+h_{l})) - f(v(h,l,v_{l})) \\
    \eqin = f(v(h,l+1)) - f(v(h,l))
  \end{IEEEeqnarray*}
  Es folgt mit einem Teleskopsummenargument:
  \begin{IEEEeqnarray*}{0l}
    f(v+h) - f(v)
    = f(v(h,n+1)) - f(v(h,1)) \\
    \eqin = \sum_{l=1}^{n} f(v(h,l+1)) - f(v(h,l))
    = \sum_{l=1}^{n} h_{l} \, \partial_{l} f(v(h,l,\tau(h,l)))
  \end{IEEEeqnarray*}
  Es folgt:
  \begin{IEEEeqnarray*}{0l}
    \abs{f(v+h) - f(v) - \grad{f}(v) \bigcdot h} \\
    \eqin = \abs{\sum_{l=1}^{n} h_{l} \, \partial_{l} f(v(h,l,\tau(h,l)))
      - \sum_{l=1}^{n} \partial_{l} f(v) h_{l}} \\
    \eqin \leq
    \sum_{l=1}^{n} \abs{h_{l}} \, \abs{\partial_{l} f(v(h,l,\tau(h,l))) - \partial_{l} f(v)} \\
    \eqin \leq \norm{h}_{\infty}
    \sum_{l=1}^{n} \abs{\partial_{l} f(v(h,l,\tau(h,l))) - \partial_{l} f(v)}
  \end{IEEEeqnarray*}
  Für alle $l$ gilt $\flim{h}{0} v(h,l,\tau(h,l)) = v$,
  wegen der Stetigkeit von $\partial_{l} f$
  also $\flim{h}{0} \partial_{l} f(v(h,l,\tau(h,l))) - \partial_{l} f(v) = 0$.
  Die Behauptung folgt.
\end{proof}

\begin{para}[Stetigkeit der partiellen Ableitungen]
  Wir machen uns klar, welche Funktionen in vorigem Satz als stetig vorausgesetzt werden.
  Beim partiellen Differenzieren werden die eindimensionalen Funktionen $f_{j,v,i}$ betrachtet.
  Die partiellen Ableitungen sind dann aber wieder Funktionen von $n$ Variablen,
  nämlich, im Kontext des obigen Satzes, $\ffn{\partial_{i} f_{j}}{U}{\RR}$
  mit $i \in \setn{n}$ und $j \in \setn{d}$.
  Alle diese Funktionen, $nd$ an der Zahl, werden im obigen Satz als stetig in $v$ vorausgesetzt.
\end{para}

\begin{para}[$\cC^{1}$\=/Funktion]
  Wir nennen $\ffn{f}{\Om}{\RR^{d}}$ eine $\cC^{1}$\=/Funktion,
  wenn $\Om$ offen ist und $f$ in jedem Punkt von $\Om$ stetig partiell differenzierbar ist.
  Nach \autoref{multdiff/cha01:stetig-partiell} ist eine $\cC^{1}$\=/Funktion differenzierbar.
  Man sieht leicht (Übung), dass sie sogar \term{stetig differenzierbar} ist,
  d.h., die Funktion $\ffn{Df}{\Om}{L(\RR^{n},\RR^{d})}$ ist stetig.
  Dabei ist es egal, welche Norm wir auf $L(\RR^{n},\RR^{d})$ betrachten,
  da dieser Vektorraum endliche Dimension hat;
  zum Beispiel könnte man die Operatornorm verwenden.
\end{para}

\begin{para}[Projektionen]
  Für die Praxis eignet sich wie so oft ein Kombinationssatz.
  Dafür benötigen wir noch einen Begriff.
  Für alle $i \in \setn{n}$ definiere $\ffnx{\pi_{i}}{\RR^{n}}{\RR}{v \mapsto v_{i}}$,
  die \term{Projektion} auf die \xte{i} Variable.
  Es ist leicht zu sehen, dass $\pi_{i}$ stetig ist (Übung).
  Für $f$ aus \autoref{multdiff/cha01:ex-1} gilt etwa:
  \begin{IEEEeqnarray*}{0l+l}
    f_{1} = \fnres{\pi_{1}}{\RR \times \RRnn} + \fnres{\pi_{2}}{\RR \times \RRnn} &
    f_{4} = -2 (\sqrt{\cdot} \circ \fnres{\pi_{2}}{\RR \times \RRnn})
  \end{IEEEeqnarray*}
\end{para}

\begin{satz}[Kombinationssatz für $\cC^{1}$]
  \label{multdiff/cha01:kombi-C1}
  Jede Komponentenfunktionen von $f$ sei eine Kombination aus den Projektionen
  und stetig differenzierbaren eindimensionalen Funktionen.
  Dann ist $f$ eine $\cC^{1}$\=/Funktion.
\end{satz}

\begin{proof}[Skizze]
  Durch das partielle Differenzieren entstehen nur Funktionen,
  die Kombinationen aus den Projektionen und stetiger eindimensionaler Funktionen sind.
\end{proof}

\begin{example}
  Die folgende Funktion ist $\cC^{1}$:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{f}{\RRpos^{3}}{\RR^{2}}%
    {(x,y,z) \mapsto \colvec{\frac{\sin(xyz)}{\sqrt{x+y}} \\ e^{1/(x^{2}+\cos(z)+1)}}}
  \end{IEEEeqnarray*}
  Das sieht man daran, dass es sich bei allen Komponentenfunktionen um eine Kombination
  aus den Funktionen $\sin$ und $\fnres{\sqrt{\cdot}}{\RRpos}$ und $\exp$ und $\cos$,
  und den Projektionen handelt.
  Die partiellen Ableitungen und somit die Jacobi\-/Matrix erhält man für alle $(x,y,z) \in \RRpos^{3}$
  über die bekannten eindimensionalen Ableitungsregeln:
  \begin{smallerenv}[3]
    \begin{IEEEeqnarray*}{0l}
      \Jacobian{f}(x,y,z) =
      \begin{bmatrix}
        \frac{\cos(xyz)yz\sqrt{x+y} - \sin(xyz)\frac{1}{2} (x+y)^{-\frac{1}{2}}}{x+y}
        & \frac{\cos(xyz)xz\sqrt{x+y} - \sin(xyz)\frac{1}{2} (x+y)^{-\frac{1}{2}}}{x+y}
        & \frac{\cos(xyz)xy}{\sqrt{x+y}} \\
        \frac{-2x e^{1/(x^{2}+\cos(z)+1)}}{(x^{2}+\cos(z)+1)^{2}}
        & 0
        & \frac{\sin(z) e^{1/(x^{2}+\cos(z)+1)}}{(x^{2}+\cos(z)+1)^{2}}
      \end{bmatrix}
    \end{IEEEeqnarray*}
  \end{smallerenv}
  Als Zahlenbeispiel berechnen wir und geben auf vier signifikante Stellen gerundet an:
  \begin{IEEEeqnarray*}{0l}
    A \df \Jacobian{f}(3,2,8) \approx
    \begin{bmatrix}
      -4.546 & -6.836 & -1.718 \\
      -0.06838 & 0 & 0.01128
    \end{bmatrix}
  \end{IEEEeqnarray*}
  Dies ist also die eindeutig bestimmte Matrix $A$ mit der Eigenschaft,
  dass gilt:
  \begin{IEEEeqnarray*}{0l}
    \flim{(x,y,z)}{(3,2,8)}
    \frac{f(x,y,z) - (f(3,2,8) + A ((x,y,z)-(3,2,8))}{\norm{(x,y,z)-(3,2,8)}} = 0
  \end{IEEEeqnarray*}
\end{example}

\begin{satz}[Kombinationssatz für Differenzierbarkeit -- Matrixversion]
  \label{multdiff/cha01:matrix}
  Es sei $f$ differenzierbar in $v$.
  \begin{enumerate}
  \item Es sei $\ffn{g}{\Om}{\RR^{d}}$ differenzierbar in $v$ und $\lam \in \RR$.
    Dann sind $\lam f$ und $f + g$ differenzierbar in $v$,
    und es gilt:
    \begin{IEEEeqnarray*}{0l+l}
      \Jacobian{f + g}(v) = \Jacobian{f}(v) + \Jacobian{g}(v) &
      \Jacobian{\lam f}(v) = \lam \Jacobian{f}(v)
    \end{IEEEeqnarray*}
  \item Es sei $\ffn{\lam}{\Om}{\RR}$ differenzierbar in $v$.
    Dann ist $\lam f$ differenzierbar in $v$,
    und es gilt:
    \begin{IEEEeqnarray*}{0l}
      \Jacobian{\lam f}(v) = f(v) \, \p{\grad{\lam}(v)}\tran + \lam(v) \, \Jacobian{f}(v)
    \end{IEEEeqnarray*}
  \item Es sei $\ffn{\lam}{\Om}{\nz{\KK}}$ differenzierbar in $v$.
    Dann ist $\frac{1}{\lam}$ differenzierbar in $v$,
    und es gilt:
    \begin{IEEEeqnarray*}{0l}
      \Jacobian{\frac{1}{\lam}}(v) = - \frac{\p{\grad{\lam}(v)}\tran}{(\lam(v))^{2}}
    \end{IEEEeqnarray*}
  \item Es sei $\ffn{\lam}{\Om}{\nz{\KK}}$ differenzierbar in $v$.
    Dann ist $\frac{f}{\lam}$ differenzierbar in $v$,
    und es gilt:
    \begin{IEEEeqnarray*}{0l}
      \Jacobian{\frac{f}{\lam}}(v)
      = \frac{\lam(v) \, \Jacobian{f}(v) - f(v) \, \p{\grad{\lam}(v)}\tran}{(\lam(v))^{2}}
    \end{IEEEeqnarray*}
  \item\label{multdiff/cha01:matrix:kettenregel} (Kettenregel)
    Es sei $\ffn{g}{\Om_{g}}{\RR^{k}}$ so,
    dass ${\img(f) \subseteq \Om_{g}} \subseteq \RR^{d}$ gilt
    und $f(v)$ innerer Punkt von $\Om_{g}$ ist.
    Es möge $g$ differenzierbar in $f(v)$ sein.
    Dann ist $g \circ f$ differenzierbar in $v$,
    und es gilt:
    \begin{IEEEeqnarray*}{0l}
      \Jacobian{g \circ f}(v) = \Jacobian{g}(f(v)) \, \Jacobian{f}(v)
    \end{IEEEeqnarray*}
  \end{enumerate}
\end{satz}

\begin{proof}
  Folgt leicht aus \autoref{multdiff/cha01:kombi}.
  Man bedenke für die Kettenregel, dass $M_{\psi \circ \phi} = M_{\psi} \, M_{\phi}$ gilt
  für alle linearen Abbildungen
  $\ffn{\phi}{\RR^{n}}{\RR^{d}}$ und $\ffn{\psi}{\RR^{d}}{\RR^{k}}$.
\end{proof}

\section{Richtungsableitungen}

Es gelte in diesem Abschnitt, wenn nicht anders ausgewiesen:
\begin{itemize}
\item $\Om \subseteq \RR^{n}$
\item $v \in \Om$ ist innerer Punkt von $\Om$.
\item $\ffn{f}{\Om}{\RR}$
\end{itemize}
Wir haben also fast dieselben Voraussetzungen wie im vorigen Abschnitt,
beschränken uns aber auf den Fall $d=1$.

\begin{para}[Spezialfall der Kettenregel]
  \label{multdiff/cha01:ketten-spezial}
  Ein wichtiger Spezialfall der Kettenregel ist eine Komposition der Form
  ${\RR \rightarrow \RR^{n} \rightarrow \RR}$.
  Wir formulieren das, indem wir eine Funktion vor $f$ schalten
  (die Kettenregel in \autoref{multdiff/cha01:matrix}\ref{multdiff/cha01:matrix:kettenregel}
  ist mit einer \emphasis{nach} $f$ geschaltete Funktion fomuliert).
  Es sei $T \subseteq \RR$
  und $\ffn{\gam}{T}{\Om}$ sei differenzierbar in $t \in T$,
  welches ein innerer Punkt von $T$ sei.
  Es möge $f$ differenzierbar in $\gam(t)$ sein,
  welches ein innerer Punkt von $\Om$ sei.
  Dann gilt:
  \begin{itemize}
  \item Es besteht $\Jacobian{f}(\gam(t))$ aus genau einer Zeile,
    nämlich dem transponierten Gradienten von $f$ an der Stelle $\gam(t)$.
  \item Es besteht $\Jacobian{\gam}(t)$ aus genau einer Spalte,
    wofür wir auch $\gam'(t)$ schreiben.
  \item Da $f \circ \gam$ von Dimension~$1$ nach Dimension~$1$ abbildet,
    ist $(f \circ \gam)'(t) \in \RR$.
  \end{itemize}
  Mit der Kettenregel folgt:
  \begin{IEEEeqnarray*}{0l}
    (f \circ \gam)'(t)
    = \Jacobian{f}(\gam(t)) \, \Jacobian{\gam}(t)
    = \grad{f}(\gam(t)) \bigcdot \gam'(t) \\
    \eqin = \sum_{i=1}^{n} \partial_{i} f (\gam(t)) \, \gam_{i}'(t)
    = \sum_{i=1}^{n} (\partial_{i} f \circ \gam)(t) \, \gam_{i}'(t)
  \end{IEEEeqnarray*}
\end{para}

\begin{para}[Richtungsableitung]
  Definiere:
  \begin{IEEEeqnarray*}{0l}
    S \df \set{r \in \RR^{n} \suchthat \norm{r}_{2} = 1}
  \end{IEEEeqnarray*}
  Für alle $r \in S$ definiere:
  \begin{IEEEeqnarray*}{0l+l}
    \Om_{v,r} \df \set{t \in \RR \suchthat v + tr \in \Om} &
    \ffnx{f_{v,r}}{\Om_{v,r}}{\RR}{t \mapsto f(v + tr)}
  \end{IEEEeqnarray*}
  Da $v$ innerer Punkt von $\Om$ ist, ist $0$ ein innerer Punkt von $\Om_{v,r}$,
  insbesondere also ein Häufungspunkt von $\Om_{v,r}$.
  Wenn $f_{v,r}$ in $0$ differenzierbar ist,
  dann nennen wir $f$ in $v$ \term{differenzierbar in Richtung} $r$
  und definieren die \term{Richtungsableitung} von $f$ in $v$ in Richtung $r$ als:
  \begin{IEEEeqnarray*}{0l}
    \partial_{r} f(v) \df f_{v,r}'(0)
  \end{IEEEeqnarray*}
\end{para}

\begin{satz}[Zusammenhang mit partieller Ableitung]
  Für alle $i \in \setn{n}$ sind äquivalent:
  \begin{itemize}
  \item $f$ ist in $v$ differenzierbar in Richtung $e_{i}$.
  \item $f$ ist in $v$ partiell differenzierbar nach der \xten{i} Variable.
  \end{itemize}
  \Ggf gilt $\partial_{e_{i}} f(v) = \partial_{i} f(v)$.
\end{satz}

\begin{proof}
  Folgt aus \autoref{multdiff/cha01:part-h}.
\end{proof}

\begin{satz}[Vom Gradient zur Richtungsableitung]
  Wenn $f$ in $v$ differenzierbar ist,
  dann ist $f$ in $v$ in alle Richtungen differenzierbar, und es gilt für alle $r \in S$:
  \begin{IEEEeqnarray*}{0l}
    \partial_{r} f(v) = \grad{f}(v) \bigcdot r
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}
  Es gilt $f_{v,r} = f \circ \gam$ mit $\ffnx{\gam}{\Om_{v,r}}{\Om}{t \mapsto v + tr}$.
  Es gilt $\gam_{i} (t) = v_{i} + t r_{i}$ für alle $i \in \setn{n}$ und alle $t \in \Om_{v,r}$,
  also ist $\gam_{i}$ differenzierbar mit $\gam_{i}'(t) = r_{i}$,
  also ist $\gam$ differenzierbar mit $\gam'(t) = r$.
  Es gilt $\gam(0) = v$.
  Nach der Kettenregel (Spezialfall) ist $f \circ \gam$ in $0$ differenzierbar, und es gilt:
  \begin{IEEEeqnarray*}{0l+x*}
    \partial_{r} f(v) = f_{v,r}'(0) = (f \circ \gam)'(0)
    = \grad{f} (\gam(0)) \bigcdot \gam'(0)
    = \grad{f} (v) \bigcdot r
    & \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\begin{satz}[Stärkster Anstieg in Richtung des Gradienten]
  Es sei $f$ in $v$ differenzierbar
  und $g \df \grad{f} (v) \neq 0$.
  Definiere $\tig \df \frac{g}{\norm{g}_{2}}$.
  Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall r \in S \setminus \set{\tig} \holds
    \partial_{r} f(v) < \partial_{\tig} f(v)
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}
  Definiere:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{\psi}{S}{\RR}{r \mapsto \frac{g \bigcdot r}{\norm{g}_{2}}}
  \end{IEEEeqnarray*}
  Für alle $r \in S$ gilt zum einen $\partial_{r} f(v) = \norm{g}_{2} \psi(r)$.
  Zum anderen
  $\psi(r) = \frac{g \bigcdot r}{\norm{g}_{2} \ccdot \norm{r}_{2}}$
  und nach der Cauchy\-/Schwarz\-/Ungleichung somit $\abs{\psi(r)} \leq 1$.
  Ferner mit der Cauchy\-/Schwarz\-/Ungleichung für alle $r \in S$:
  \begin{IEEEeqnarray*}{0l"s}
    \abs{\psi(r)} = 1 \iff \p{\exists \al \in \RR \holds g = \al r} \lor r = 0 \\
    \eqin \iff \exists \al \in \RRnz \holds g = \al r
    & \text{da $g, r \neq 0$} \\
    \eqin \iff \exists \al \in \RRnz \holds r = \frac{g}{\al} \\
    \eqin \iff \exists \al \in \RRnz \holds r = \frac{g}{\al} \land \abs{\al} = \norm{g}_{2}
    & \text{da $\norm{r}_{2} = 1$} \\
    \eqin \iff r = \tig \lor r = -\tig
  \end{IEEEeqnarray*}
  Es gilt:
  \begin{IEEEeqnarray*}{0l}
    \psi(-\tig) = \frac{g \bigcdot \p{-\frac{g}{\norm{g}_{2}}}}{\norm{g}_{2}}
    = -\frac{1}{\norm{g}_{2}} \frac{g \bigcdot g}{\norm{g}_{2}}
    = -1
  \end{IEEEeqnarray*}
  Es folgt:
  \begin{IEEEeqnarray*}{0l+s+l}
    \forall r \in S \holds \psi(r) = 1 \iff r = \tig
    & und damit &
    \forall r \in S \setminus \set{\tig} \holds \psi(r) < \psi(\tig)
  \end{IEEEeqnarray*}
  Damit für alle $r \in S \setminus \set{\tig}$:
  \begin{IEEEeqnarray*}{0l+s*}
    \partial_{r} f(v)
    = \underbrace{\norm{g}_{2}}_{> 0} \psi(r)
    < \norm{g}_{2} \psi(\tig)
    = \partial_{\tig} f(v) & \qedhere
  \end{IEEEeqnarray*}
\end{proof}
