\chapter{Zweite Ordnung und Extremalrechnung}
\label{multdiff/cha02}

Bis auf die Erkenntnis, dass der Gradient (wenn ungleich~$0$)
in die Richtung des stärksten Anstiegs zeigt,
war das vorige Kapitel arm an Anwendungen.
Das wird sich nun ändern.

\section{Approximation zweiter Ordnung}

\begin{para}[Erinnerung an Taylorformel in Dimension~$1$]
  \label{multdiff/cha02:taylor-1}
  Aus vorigen Semestern ist folgender Satz so oder so ähnlich bekannt:
  Es sei $I \subseteq \RR$ ein Intervall und $t \in I$ und $h \in \nz{\RR}$ so, dass $t+h \in I$ gilt.
  Es sei $n \in \NNzero$ und $\ffn{f}{I}{\RR}$ sei \xmal{n} stetig differenzierbar.
  Dann gibt es $\tau(h)$ zwischen $t$ und $t+h$ so, dass gilt:
  \begin{IEEEeqnarray*}{0l}
    f(t+h) = \sum_{k=0}^{n-1} \frac{\diff{f}{k}(t)}{k!} h^{k}
    + \frac{\diff{f}{n}(\tau(h))}{n!} h^{n} \\
    \eqin = \sum_{k=0}^{n} \frac{\diff{f}{k}(t)}{k!} h^{k}
    + \frac{\diff{f}{n}(\tau(h)) - \diff{f}{n}(t) }{n!} h^{n}
  \end{IEEEeqnarray*}
  Da $\diff{f}{n}$ stetig ist, gilt $\flim{h}{0} \diff{f}{n}(\tau(h)) = \diff{f}{n}(t)$.
  Es folgt:
  \begin{IEEEeqnarray*}{0l}
    \frac{f(t+h) - (f(t) + \sum_{k=1}^{n} \frac{\diff{f}{k}(t)}{k!} h^{k})}{h^{n}}
    = \frac{\diff{f}{n}(\tau(h)) - \diff{f}{n}(t) }{n!}
    \tendsasto{h}{0} 0
  \end{IEEEeqnarray*}
  Unsere Redeweise vom vorigen Kapitel fortsetzend, kann man also sagen:
  Die Funktion $\fnx{\RR}{\RR}{h \mapsto f(t) + \sum_{k=1}^{n} \frac{\diff{f}{k}(t)}{k!} h^{k}}$
  approximiert $f$ in der Nähe von $t$ so,
  dass der Approximationsfehler schneller gegen~$0$ geht als der Abstand zu~$t$
  in der \xten{n} Potenz.
  Der Spezialfall $n=2$ sieht so aus:
  \begin{IEEEeqnarray}{0l}
    \label{multidiff/cha02:taylor-2}
    \frac{f(t+h) - (f(t) + f'(t) h + \frac{f''(t)}{2} h^{2})}{h^{2}}
    \tendsasto{h}{0} 0
  \end{IEEEeqnarray}
  Unser Ziel ist nun eine Verallgemeinerung von \eqref{multidiff/cha02:taylor-2}
  für Funktionen von Dimension~$n$ nach Dimension~$1$.
\end{para}

\begin{para}[$\cC^{2}$\=/Funktion]
  Es sei $\Om \subseteq \RR^{n}$ offen und $\ffn{f}{\Om}{\RR}$.
  Wir nennen $f$ eine $\cC^{2}$\=/Funktion, wenn gilt:
  \begin{itemize}
  \item $f$ ist eine $\cC^{1}$\=/Funktion.
  \item Für alle $i \in \setn{n}$ ist $\ffn{\partial_{i} f}{\Om}{\RR}$ stetig partiell differenzierbar.
    Offenbar ist dies äquivalent dazu,
    dass $\ffn{\grad{f}}{\Om}{\RR^{n}}$ eine $\cC^{1}$\=/Funktion ist.
    Die partielle Ableitung von $\partial_{i} f$ nach der \xten{j} Variable
    bezeichnen wir mit $\partial_{j} \partial_{i} f$.
  \end{itemize}
\end{para}

\begin{satz}[Kombinationssatz für $\cC^{2}$]
  \label{multdiff/cha02:kombi}
  Jede Komponentenfunktionen von $f$ sei eine Kombination aus den Projektionen
  und \xmal{2} stetig differenzierbaren eindimensionalen Funktionen.
  Dann ist $f$ eine $\cC^{2}$\=/Funktion.
\end{satz}

\begin{proof}
  Ähnlich wie \autoref{multdiff/cha01:kombi-C1}.
\end{proof}

\begin{para}[Generalvoraussetzung]
  Es gelte von nun an in diesem Kapitel, wenn nicht anders ausgewiesen:
  \begin{itemize}
  \item $\Om \subseteq \RR^{n}$ offen.
  \item $\ffn{f}{\Om}{\RR}$ ist eine $\cC^{2}$\=/Funktion.
  \item $\normfn$ ist irgendeine Norm auf $\RR^{n}$;
    Kugeln und Umgebungen zu dieser Norm notieren wir mit $B_{n}$ \bzw $\nsets_{n}$.
  \end{itemize}
\end{para}

\begin{para}[Hesse-Matrix]
  Für alle $v \in \Om$ definieren wir die
  \term{Hesse\-/Matrix}\footnote{%
    Otto Hesse, 1811--1874, deutscher Mathematiker.}
  von $f$ in $v$ als:
  \begin{IEEEeqnarray*}{0l}
    \Hessian{f}(v) \df
    \begin{bmatrix}
      \partial_{1} \partial_{1} f (v) & \hdots & \partial_{1} \partial_{n} f(v) \\
      \partial_{2} \partial_{1} f (v) & \hdots & \partial_{2} \partial_{n} f(v) \\
      \vdots & & \vdots \\
      \partial_{n} \partial_{1} f (v) & \hdots & \partial_{n} \partial_{n} f(v)
    \end{bmatrix}
    \in \RR^{n \times n}
  \end{IEEEeqnarray*}
  Offenbar gilt $\Hessian{f}(v) = \p{\Jacobian{\grad{f}}(v)}^{\tran}$,
  denn die Spalten der Hesse\-/Matrix sind die Gradienten der Komponentenfunktionen von $\grad{f}$.
  Die Transposition kann man auch fortlassen, wie der folgende Satz lehrt.
\end{para}

\begin{satz}[Satz von Schwarz\protect\footnote{Hermann A. Schwarz, 1843--1921, deutscher Mathematiker.}]
  Die Hesse\-/Matrix ist symmetrisch,
  d.h. es gilt $\partial_{i} \partial_{j} f (v) = \partial_{j} \partial_{i} f (v)$
  für alle $i,j \in \setn{n}$ und alle $v \in \Om$.
\end{satz}

\begin{proof}
  Hier nicht; es ist wieder eine Anwendung des Mittelwertsatzes.
\end{proof}

\begin{para}[Funktion für relativen Approximationsfehler zweiter Ordnung]
  Für alle $v \in \Om$ definiere
  $\Om - v = \set{u-v \suchthat u \in \Om}$,
  also ist $v+h \in \Om$ für alle $h \in \Om - v$.
  Ferner:
  \begin{IEEEeqnarray*}{0l}
    \Om_{v} \df \nz{(\Om-v)} \\
    \ffnx{\Del_{v}}{\Om_{v}}{\RR}{%
      h \mapsto
      \frac{f(v+h) - \p[big]{f(v) + \grad{f}(v) \bigcdot h + \frac{1}{2} h\tran \, \Hessian{f}(v) \, h}}%
      {\norm{h}^{2}}}
  \end{IEEEeqnarray*}
\end{para}

\begin{satz}[Approximation zweiter Ordnung]
  \label{multdiff/cha01:apx-2}
  Es gilt $\flim{h}{0} \Del_{v}(h) = 0$ für alle $v \in \Om$.
\end{satz}

\begin{proof}
  Es sei $v \in \Om$.
  Da $\Om$ offen ist, finde $\del_{0} \in \intoc{0}{1}$ so,
  dass $B_{n}(v,\del_{0}) \subseteq \Om$ gilt.
  Es sei $r \in \RR^{n}$ so, dass $\norm{r} = \frac{\del_{0}}{2}$ gilt, und definiere
  $\ffnx{\gam}{\intcc{0}{1}}{\RR^{n}}{t \mapsto v + tr}$;
  dann gilt $\img(\gam) \subseteq B_{n}(v,\del_{0}) \subseteq \Om$.
  Es ist $\gam$ eine $\cC^{2}$\=/Funktion (Übung: was sind die Komponentenfunktionen von $\gam$?).
  Es gilt $\gam'(t) = r$ und $\gam''(t) = 0$.
  Definiere $\vphi \df f \circ \gam$.
  Nach der Kettenregel (\autoref{multdiff/cha01:ketten-spezial}) gilt:
  \begin{IEEEeqnarray*}{0l}
    \vphi'(t) = \sum_{i=1}^{n} (\partial_{i} f \circ \gam)(t) \, \gam_{i}'(t)
  \end{IEEEeqnarray*}
  Da $f$ eine $\cC^{2}$\=/Funktion ist,
  sind die partiellen Ableitungen $\partial_{i} f$ mit $i \in \setn{n}$
  stetig partiell differenzierbar, also differenzierbar;
  wir wenden jeweils wieder die Kettenregel und nun auch die Produktregel an:
  \begin{IEEEeqnarray*}{0l}
    \vphi''(t) = \sum_{i=1}^{n} (\partial_{i} f \circ \gam)'(t) \, \underbrace{\gam_{i}'(t)}_{=r_{i}}
    + (\partial_{i} f \circ \gam)(t) \, \underbrace{\gam_{i}''(t)}_{=0} \\*
    \eqin = \sum_{i=1}^{n}
    \sum_{j=1}^{n} (\partial_{j} \partial_{i} f \circ \gam)(t) \, \gam_{j}'(t) r_{i} \\
    \eqin = \sum_{i=1}^{n}
    \sum_{j=1}^{n} \p{\Hessian{f}(\gam(t))}_{ji} r_{j} r_{i} \\
    \eqin = \sum_{j=1}^{n}
    \p{\sum_{i=1}^{n} \p{\Hessian{f}(\gam(t))}_{ji} r_{i}} r_{j} \\
    \eqin = \sum_{j=1}^{n} \p{\Hessian{f}(\gam(t)) \, r}_{j} r_{j}
    = r\tran \, \Hessian{f}(\gam(t)) \, r
  \end{IEEEeqnarray*}
  Wegen $\gam(0) = v$ gilt:
  \begin{IEEEeqnarray*}{0l}
    \vphi'(0) = \sum_{i=1}^{n} \partial_{i} f (v) \, r_{i} = \grad{f}(v) \bigcdot r \\
    \vphi''(0) = r\tran \, \Hessian{f}(v) \, r
  \end{IEEEeqnarray*}
  Nach \autoref{multdiff/cha02:taylor-1} gibt es für alle $t \in \intoc{0}{1}$
  ein $\tau(t,r) \in \intoo{0}{t}$ so, dass gilt:
  \begin{IEEEeqnarray*}{0l}
    f(v + tr) = \vphi(t) = \vphi(0+t) \\
    \eqin = \vphi(0) + \vphi'(0) \, t
    + \frac{1}{2} \vphi''(0) \, t^{2}
    + \frac{\vphi''(\tau(t,r)) - \vphi''(0)}{2} t^{2} \\
    \eqin = f(v) + \p{\grad{f}(v) \bigcdot r} \, t
    + \frac{1}{2} \p{r\tran \, \Hessian{f}(v) \, r} \, t^{2}
    + \frac{\vphi''(\tau(t,r)) - \vphi''(0)}{2} t^{2} \\
    \eqin = f(v) + \p{\grad{f}(v) \bigcdot (tr)}
    + \frac{1}{2} \p{(tr)\tran \, \Hessian{f}(v) \, (tr)}
    + \frac{\vphi''(\tau(t,r)) - \vphi''(0)}{2} t^{2}
  \end{IEEEeqnarray*}
  Definiere $\del_{1} \df \frac{\del_{0}^{2}}{2} < \del_{0}$.
  Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    \Del_{v}(tr) = \frac{\vphi''(\tau(t,r)) - \vphi''(0)}{2 \, \norm{tr}^{2}} t^{2}
    = \frac{\vphi''(\tau(t,r)) - \vphi''(0)}{\del_{1}}
  \end{IEEEeqnarray*}
  Es sei nun $\seq{h_{k}}{k} \in \seqset{\Om_{v}}$ so, dass $\limx{k} h_{k} = 0$ gilt.
  Zu zeigen ist ${\limx{k} \Del_{v}(h_{k}) = 0}$.
  Es sei $\veps > 0$.
  Da $\varphi''$ stetig ist, finde $\del \in \intoc{0}{1}$ so,
  dass $\abs{\vphi''(s) - \vphi''(0)} < \veps \del_{1}$ für alle $s \in \intco{0}{\del}$ gilt.
  Es sei $k \geq \nzero{}(\frac{\del\del_{1}}{2}, h)$;
  dann ist $\norm{h_{k}} < \frac{\del\del_{1}}{2} < \frac{\del\del_{0}}{2}$.
  Definiere $t \df \frac{2 \, \norm{h_{k}}}{\del_{0}}$
  und $r \df \frac{h_{k}}{t}$;
  dann ist $\norm{r} = \frac{\del_{0}}{2}$ und $tr = h_{k}$.
  Ferner ist $\tau(t,r) < t < \frac{2 \, \frac{\del\del_{0}}{2}}{\del_{0}} = \del \leq 1$,
  also $\tau(t,r) \in \intco{0}{\del}$.
  Es folgt:
  \begin{IEEEeqnarray*}{0l+x*}
    \abs{\Del_{v}(h_{k})} = \abs{\Del_{v}(tr)}
    = \frac{\abs{\vphi''(\tau(t,r)) - \vphi''(0)}}{\del_{1}}
    < \frac{\veps \del_{1}}{\del_{1}} = \veps
    & \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\section{Lokale Extremstellen}

\begin{para}[Lokale Extremstellen]
  \hfill
  \begin{itemize}
  \item Wir nennen $v \in \Om$ eine \term{lokale Maximumstelle} von $f$,
    oder sagen, dass $f$ in $v$ ein \term{lokales Maximum} hat,
    wenn es $U \in \nsets_{n}(v)$
    so gibt, dass $f(u) \leq f(v)$ für alle $u \in U \cap \Om$ gilt.
    Definiere:
    \begin{IEEEeqnarray*}{0l}
      \LMAX(f) \df \set{v \in \Om \suchthat \text{$v$ ist lokale Maximumstelle von $f$}}
    \end{IEEEeqnarray*}
  \item Wir nennen $v \in \Om$ eine \term{lokale Minimumstelle} von $f$,
    oder sagen, dass $f$ in $v$ ein \term{lokales Minimum} hat,
    wenn es $U \in \nsets_{n}(v)$
    so gibt, dass $f(u) \geq f(v)$ für alle $u \in U \cap \Om$ gilt.
    Definiere:
    \begin{IEEEeqnarray*}{0l}
      \LMIN(f) \df \set{v \in \Om \suchthat \text{$v$ ist lokale Minimumstelle von $f$}}
    \end{IEEEeqnarray*}
  \item Definiere $\LEXT(f) \df \LMAX(f) \cup \LMIN(f)$,
    genannt die Menge der \term{lokalen Extremstellen} von $f$.
  \end{itemize}
  Auch wenn die obigen Begriffe für die von uns fixierte Norm $\normfn$ formuliert sind,
  bekommen wir genau dieselben Mengen $\LMAX$ und $\LMIN$ wenn wir zu einer anderen Norm übergehen
  (Übung).
\end{para}

\begin{para}[Definitheit einer Matrix]
  Es sei $A \in \RR^{n \times n}$ symmetrisch.
  Dann nennen wir~$A$~\dots
  \begin{itemize}
  \item \term{positiv definit}, wenn $v\tran A v > 0$ für alle $v \in \nz{\RR^{n}}$ gilt;
  \item \term{negativ definit}, wenn $v\tran A v < 0$ für alle $v \in \nz{\RR^{n}}$ gilt;
  \item \term{indefinit}, wenn es $v,w \in \RR^{n}$ so gibt, dass $v\tran A v < 0$ und $w\tran A w > 0$ gilt.
  \end{itemize}
\end{para}

\begin{satz}[Lokale Extremstellen via Gradient und Hesse\-/Matrix]
  Definiere die Menge der \term{kritischen Stellen} von $f$ als
  $K(f) \df \set{v \in \Om \suchthat \grad{f}(v) = 0}$.
  Dann gilt $\LEXT(f) \subseteq K(f)$ und für alle $v \in K(f)$ gilt:
  \begin{enumerate}
  \item $\text{$\Hessian{f}(v)$ positiv definit} \implies v \in \LMIN(f)$
  \item $\text{$\Hessian{f}(v)$ negativ definit} \implies v \in \LMAX(f)$
  \item $\text{$\Hessian{f}(v)$ indefinit} \implies v \not\in \LEXT(f)$
  \end{enumerate}
\end{satz}

\begin{proof}
  Wir zeigen nur (1) und überlassen alle übrigen Aussagen
  (das sind $\LEXT(f) \subseteq K(f)$ und (2) und (3)) zur Übung.
  Es sei also $v \in K(f)$ und $\Hessian{f}(v)$ positiv definit.
  Definiere $S \df \set{r \in \RR^{n} \suchthat \norm{r}=1}$ und weiter:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{\psi}{S}{\RRpos}{r \mapsto r\tran \, \Hessian{f}(v) \, r}
  \end{IEEEeqnarray*}
  Diese Funktion bildet tatsächlich nach $\RRpos$ ab, da $\Hessian{f}(v)$ positiv definit ist.
  Da $S$ kompakt und $\psi$ stetig ist, hat $\img\p{\psi}$ ein Minimum,
  definiere $\mu \df \min\p{\img\p{\psi}} > 0$.
  \par
  Da $\grad{f}(v) = 0$ ist, gilt für alle $h \in \Om_{v}$:
  \begin{IEEEeqnarray*}{0l}
    \Del_{v}(h)
    = \frac{f(v+h) - f(v)}{\norm{h}^{2}} - \frac{1}{2} \psi\p{\frac{h}{\norm{h}}}
    \leq \frac{f(v+h) - f(v)}{\norm{h}^{2}} - \frac{\mu}{2}
  \end{IEEEeqnarray*}
  Da $\flim{h}{0} \Del_{v}(h) = 0$ gilt,
  finde $\del > 0$ so, dass $\abs{\Del_{v}(h)} < \frac{\mu}{3}$
  für alle $h \in B \df \nz{(B_{n}(0, \del))}$ gilt;
  implizit wählen wir $\del$ dabei so klein, dass $B \subseteq \Om - v$ gilt,
  was möglich ist wegen der Offenheit von $\Om$.
  Es sei $h \in B$. Dann gilt:
  \begin{IEEEeqnarray*}{0l}
    -\frac{\mu}{3} < \Del_{v}(h) \leq \frac{f(v+h) - f(v)}{\norm{h}^{2}} - \frac{\mu}{2}
  \end{IEEEeqnarray*}
  Also:
  \begin{IEEEeqnarray*}{0l}
    0 < -\frac{\mu}{3} + \frac{\mu}{2} < \frac{f(v+h) - f(v)}{\norm{h}^{2}}
  \end{IEEEeqnarray*}
  Wegen $\norm{h}^{2} > 0$ also $f(v) < f(v+h)$ für alle $h \in B$,
  also $f(v) \leq f(u)$ für alle $u \in (B_{n}(v, \del))$,
  also ist $v$ eine lokale Minimumstelle von~$f$.
\end{proof}

\begin{para}[Warnung]
  Die Voraussetzung, dass $\Om$ offen ist, ist essentiell für den Satz.
  Das sieht man schon in Dimension~$1$, und diese Warnung sollte aus früheren Semestern bekannt sein.
  Zum Beispiel hat $f \df \id_{\intcc{0}{1}}$ ein lokales Minimum in~$0$, jedoch gilt $f'(0) = 1$,
  also $0$ ist keine kritische Stelle von $f$.
\end{para}

Wir brauchen Kriterien für die Definitheit einer Matrix.

\begin{para}[Streichen von Zeilen und Spalten]
  Es sei ${A \in \RR^{n \times n}}$ symmetrisch und $k \in \setn{n}$.
  Dann bezeichnen wir mit $A_{k}$ die $(k \times k)$\=/Matrix,
  die aus $A$ durch Streichen der letzten $n-k$ Zeilen und Spalten hervorgeht.
\end{para}

\begin{satz}[Kriterium für Definitheit]
  Es sei $A \in \RR^{n \times n}$ symmetrisch.
  \begin{enumerate}
  \item Es sind äquivalent:
    \begin{itemize}
    \item $A$ ist positiv definit.
    \item Alle Eigenwerte von $A$ sind positiv.
    \item Für alle $k \in \setn{n}$ gilt $\det(A_{k}) > 0$.
    \end{itemize}
  \item Es sind äquivalent:
    \begin{itemize}
    \item $A$ ist negativ definit.
    \item Alle Eigenwerte von $A$ sind negativ.
    \item Für alle $k \in \setn{n}$ gilt $(-1)^{k} \det(A_{k}) > 0$.
    \end{itemize}
  \item Es sind äquivalent:
    \begin{itemize}
    \item $A$ ist indefinit.
    \item $A$ hat sowohl positive als auch negative Eigenwerte.
    \end{itemize}
  \end{enumerate}
\end{satz}

\begin{proof}
  Hier nicht.
\end{proof}

\begin{satz}[Kriterium für Definitheit von $(2 \times 2)$\=/Matrizen]
  Es seien $a,b,c \in \RR$ und definiere:
  \begin{IEEEeqnarray*}{0l}
    A \df
    \begin{bmatrix}
      a & b \\
      b & c
    \end{bmatrix}
    \in \RR^{2 \times 2}
  \end{IEEEeqnarray*}
  Dann gilt:
  \begin{enumerate}
  \item $\text{$A$ positiv definit} \iff a > 0 \land \det(A) > 0$.
  \item $\text{$A$ negativ definit} \iff a < 0 \land \det(A) > 0$.
  \item $\text{$A$ indefinit} \iff \det(A) < 0$.
  \end{enumerate}
\end{satz}

\begin{proof}
  Die ersten beiden Aussagen folgen leicht aus dem vorigen Satz.
  Die dritte überlassen wir zur Übung (voriger Satz darf verwendet werden).
\end{proof}

\begin{example}
  Es sei $a  \in \RR$ und definiere:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{f}{\RR^{2}}{\RR}{(x,y) \mapsto a  x y - x^{3} - y^{2}}
  \end{IEEEeqnarray*}
  Wir bestimmen $\LMAX(f)$ und $\LMIN(f)$.
  \par
  Nach dem Kombinationssatz ist $f$ eine $\cC^{2}$\=/Funktion.
  Also können die entsprechenden Sätze über lokale Extremstellen angewendet werden.
  \par
  Für alle $(x,y) \in \RR^{2}$ gilt:
  \begin{IEEEeqnarray*}{0l+l}
    \grad{f}(x,y) = (a  y - 3x^{2} , \, a  x - 2y)
    &
    \Hessian{f}(x,y) =
    \begin{bmatrix}
      -6x & a  \\
      a  & -2
    \end{bmatrix}
  \end{IEEEeqnarray*}
  Wenn $a  \neq 0$, so gilt:
  \begin{IEEEeqnarray*}{0l}
    \grad{f}(x,y) = 0
    \iff (y = \tfrac{3}{a } x^{2}) \land (y = \tfrac{a }{2} x)
    \iff (y = \tfrac{a }{2} x) \land (\tfrac{3}{a } x^{2} = \tfrac{a }{2} x) \\
    \quad\iff (y = \tfrac{a }{2} x) \land (x = 0 \lor x = \tfrac{a ^{2}}{6})
    \iff (x,y) \in \set{ (0,0), \, (\tfrac{a ^{2}}{6}, \tfrac{a ^{3}}{12}) }
  \end{IEEEeqnarray*}
  Wenn $a  = 0$, so gilt offenbar $\grad{f}(x,y) = 0 \iff (x,y)=(0,0)$,
  somit ist in jedem Falle $\set{ (0,0), \, (\tfrac{a ^{2}}{6}, \tfrac{a ^{3}}{12}) }$
  die Menge der kritischen Stellen.
  Wir haben:
  \begin{IEEEeqnarray*}{0l+l}
    A \df \Hessian{f}(0,0) =
    \begin{bmatrix}
      0 & a  \\
      a  & -2
    \end{bmatrix}
    &
    B \df \Hessian{f}(\tfrac{a ^{2}}{6}, \tfrac{a ^{3}}{12}) =
    \begin{bmatrix}
      -a ^{2} & a  \\
      a  & -2
    \end{bmatrix}
  \end{IEEEeqnarray*}
  \needspace{\baselineskip}%
  Es folgt wenn $a  \neq 0$:
  \begin{itemize}
  \item $\det(A) = - a ^{2} < 0$, also ist $A$ indefinit.
  \item $\det(B_{11}) = -a ^{2} < 0$ und $\det(B) = 2a ^{2} - a ^{2} = a ^{2} > 0$.
    Also ist $B$ negativ definit.
  \end{itemize}
  Im ganzen haben wir also im Falle $a  \neq 0$,
  dass $\LMAX(f) = \set{(\tfrac{a ^{2}}{6}, \tfrac{a ^{3}}{12})}$ und ${\LMIN(f) = \emptyset}$.
  \par
  Wenn $a =0$ gilt, so müssen wir die eine kritische Stelle $(0,0)$ händisch untersuchen.
  Für alle $x \in \RR$ haben wir $f(x,0) = -x^{3}$,
  also ist $(0,0) \not\in \LEXT(f)$.
  Es folgt $\LMAX(f) = \emptyset$ und $\LMIN(f) = \emptyset$.
\end{example}

\section{Ausblick: Höhere Ableitungen}

Durch Einbeziehen höherer partieller Ableitungen,
kann man noch bessere Approximationen erzielen.
Das schauen wir uns kurz an.
Wir formulieren es alleine durch partielle Ableitungen,
die ja immer Funktionen von der Domain unserer Funktion nach $\RR$ sind.

\begin{para}[$\cC^{k}$\=/Funktion]
  Es sei in diesem Abschnitt $\Om \subseteq \RR^{n}$ offen
  und $f$ sei eine $\cC^{k}$\=/Funktion mit $k \in \NN$,
  d.h. es existieren die partiellen Ableitungen
  $\ffn{\partial_{i_{1}} \hdots \partial_{i_{r}} f}{\Om}{\RR}$
  für alle ${\eli{i}{r} \in \setn{n}}$ mit $r \leq k$ und sind stetig.
\end{para}

\begin{para}[Multiindex]
  Für alle $\al \in \NNzero^{n}$ und alle $h \in \RR^{n}$ definiere:
  \begin{IEEEeqnarray*}{0l+l+l}
    \abs{\al} \df \sum_{i=1}^{n} \al_{i} &
    \al ! \df \prod_{i=1}^{n} \al_{i}! &
    h^{\al} \df \prod_{i=1}^{n} h_{i}^{\al_{i}}
  \end{IEEEeqnarray*}
  Außerdem definiere, wenn $\abs{\al} \leq k$ gilt:
  \begin{IEEEeqnarray*}{0l}
    \partial^{\al} f \df
    \underbrace{\partial_{1} \hdots \partial_{1}}_{\text{\xmal{\al_{1}}}}
    \underbrace{\partial_{2} \hdots \partial_{2}}_{\text{\xmal{\al_{2}}}}
    \hdots
    \underbrace{\partial_{n} \hdots \partial_{n}}_{\text{\xmal{\al_{n}}}}
    f
  \end{IEEEeqnarray*}
  Die Reihenfolge der Variablen bei der Bildung der partiellen Ableitungen
  ist nach dem Satz von Schwarz egal.
  Wenn $\abs{\al} = 0$, dann verstehen wir $\partial^{\al} f = f$.
\end{para}

\begin{satz}[Mehrdimensionale Taylorformel]
  Für alle $v \in \Om$ gilt:
  \begin{IEEEeqnarray*}{0l}
    \flim{h}{0} \frac{f(v+h)
      - \sum_{\al \in \NNzero^{n} \holds \abs{\al} \leq k} \frac{1}{\al!} \partial^{\al} f(v) h^{\al}}%
    {\norm{h}^{k}} = 0
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}
  Hier nicht; es geht ähnlich wie der Beweis zu \autoref{multdiff/cha01:apx-2}.
\end{proof}
