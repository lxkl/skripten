\chapter{Reelle Zahlen}

\begin{MathInfB}
  \section*{Notation}

  \begin{description}
  \item[Natürliche Zahlen:] $\NN = \set{0,1,2,3,\hdots}$
  \item[Positive natürliche Zahlen:] $\NNone = \set{1,2,3,\hdots}$
  \item[Bildmenge:]
    Es seien $\Om, Y$ Mengen und $\ffn{f}{\Om}{Y}$ eine Funktion.
    Definiere die \term{Bildmenge} von $f$ als
    $\img(f) \df \set{f(x) \suchthat x \in \Om}$.
  \end{description}
\end{MathInfB}

\section{Angeordnete Körper}

Aus früheren
\begin{MathInfB}
  Semestern
\end{MathInfB}
\begin{MathIngI}
  Kapiteln
\end{MathIngI}\xspace
sind Körper und Ordnungen bekannt.
Wir bringen diese beiden Begriffe zusammen.

\begin{para}[Körper]
  Wir sagen, $K=(K,+,\cdot,0,1)$ ist ein \term{Körper}, wenn gilt:
  \begin{itemize}
  \item $(K,+)$ ist eine kommutative Gruppe mit neutralem Element~$0$,
    genannt die \term{additive Gruppe} des Körpers
    (wobei die Symbole \enquote{$+$} und \enquote{$0$} nicht für das zu stehen brauchen,
    wofür sie üblicherweise im Kontext der reellen Zahlen stehen);
  \item $(\nz{K},\cdot)$, wobei $\nz{K} \df K\setminus\set{0}$,
    ist eine kommutative Gruppe mit neutralem Element~$1$,
    genannt die \term{multiplikative Gruppe} des Körpers,
    insbesondere gilt $0 \neq 1$
    (wobei die Symbole \enquote{$\cdot$} und \enquote{$1$} nicht für das zu stehen brauchen,
    wofür sie üblicherweise im Kontext der reellen Zahlen stehen);
  \item es gilt das \term{Distributivgesetz}, d.h. $\forall x,y,z \in K \holds x(y+z) = xy + xz$.
  \end{itemize}
  Wir bezeichnen mit $K$ sowohl eine Menge als
  auch das Quintupel $(K,+,\cdot,0,1)$,
  welches die Menge $K$ mit den beiden Verknüpfungen und neutralen Elementen zusammenfasst
  und somit den Körper bildet.
\end{para}

\begin{para}[Notationen für Körper, Potenz mit ganzzahligem Exponenten]
  \label{rlbasic/cha01:field-notation}
  \begin{MathInfB}
    Wir fassen grundlegende Notationen zusammen,
    die auch aus früheren Semestern bekannt sein dürften.
  \end{MathInfB}\xspace
  Es sei $K=(K,+,\cdot,0,1)$ ein Körper.
  Für alle $x \in K$ notieren wir das additiv Inverse zu $x$ als $-x$,
  und für alle $x \in \nz{K}$ notieren wir das multiplikativ Inverse zu $x$ als $x^{-1}$ oder als $\frac{1}{x}$.
  Weitere Notationen für alle $x,y \in K$:
  \begin{itemize}
  \item $x-y \df x+(-y)$
  \item $xy \df x \ccdot y$
  \item $x \frac{1}{y} \df \frac{x}{y}$, wenn $y \neq 0$ gilt.
  \end{itemize}
  \par
  Für alle $n \in \NN$ und alle $x \in K$ definieren wir die \xte{n} \term{Potenz} von $x$,
  geschrieben~$x^{n}$, rekursiv:
  \begin{IEEEeqnarray*}{0l+l}
    x^{0} \df 1 & x^{n} \df x x^{n-1} \quad (n \geq 1)
  \end{IEEEeqnarray*}
  Es wird $x^{2} = xx$ auch das \term{Quadrat} von $x$ genannt.
  Für alle $x \in \nz{K}$ definiere $x^{-n} \df \frac{1}{x^n}$,
  was sich offenbar mit den Notationen $x^{-1}$ und $\frac{1}{x}$ für das Inverse verträgt.
  In allen diesen Fällen wird $x$ als die \term{Basis} und $n$ \bzw $-n$ als der \term{Exponent} bezeichnet;
  man sagt auch, dass die Basis mit dem Exponenten \term{potenziert} wird.
  \par
  Man zeigt leicht per Induktion (Übung), dass für alle $x,y \in K$ und alle $n,m \in \ZZ$ gilt,
  solange bei negativem Exponenten die Basis ungleich~$0$ ist:
  \begin{IEEEeqnarray*}{0l+l+l}
    x^{n}x^{m} = x^{n+m} & (x^{n})^{m} = x^{nm} & x^{n}y^{n} = (xy)^{n}
  \end{IEEEeqnarray*}
\end{para}

\begin{MathInfB}
  Die Aussagen des folgenden Satzes dürften aus früheren Semestern bekannt sein.
\end{MathInfB}
\begin{MathIngI}
  Die Aussagen des folgenden Satzes dürften von der Schule für die dort verwendeten reellen Zahlen bekannt sein.
\end{MathIngI}\xspace
Einige folgen direkt aus der Definition,
die übrigen lassen sich leicht beweisen (Übung).

\begin{satz}[Regeln in Körpern]
  \label{rlbasic/cha01:field-rules}
  Es sei $K=(K,+,\cdot,0,1)$ ein Körper.
  Für alle $x,y,z \in K$ und alle $a,b \in \nz{K}$ und alle $n \in \NN$ gilt:
  \begin{enumerate}
  \item $x+y = y+x$ \quad (Kommutativität der Addition)
  \item $(x+y) + z = x + (y+z)$ \quad (Assoziativität der Addition)
  \item $xy = yx$ \quad (Kommutativität der Multiplikation)
  \item $(xy)z = x(yz)$ \quad (Assoziativität der Multiplikation)
  \item $x(y+z) = xy + xz$ und $(y+z)x = xy + xz$ \quad (Distributivgesetz)
  \item $-(-x) = x$
  \item $a^{-1} \neq 0$
  \item $(a^{-1})^{-1} = a$
  \item $-(x+y) = (-x) + (-y)$
  \item $(ab)^{-1} = b^{-1} a^{-1} = a^{-1} b^{-1}$
  \item $(-x) y = -(xy) = x(-y)$
  \item $(-x) (-y) = x y$
  \item $x^n = (-1)^n \cdot (-x)^n$
  \item $x = y \iff x + z = y + z$
  \item $x \neq y \iff x + z \neq y + z$
  \item $x = y \iff a x = a y$
  \item $x \neq y \iff a x \neq a y$
  \item $xy = 0 \iff (x = 0 \lor y = 0)$
  \item $\frac{x}{a} = 0 \iff x = 0$
  \item $x=-x \iff x=0$ \quad (wenn $1+1 \neq 0$ gilt)
  \end{enumerate}
\end{satz}

\begin{para}[Angeordnete Körper]
  \label{rlbasic/cha01:angeordnet}
  Es sei $K=(K,+,\cdot,0,1)$ ein Körper.
  Es sei $\leq$ eine \term{totale Ordnung} oder \term{lineare Ordnung} auf $K$,
  d.h. $\leq$ ist eine Relation auf $K$ mit:
  \begin{itemize}
  \item $\leq$ ist \term{reflexiv}, d.h. $\forall x \in K \holds x \leq x$;
  \item $\leq$ ist \term{antisymmetrisch},
    d.h. $\forall x,y \in K \holds x \leq y \land y \leq x \implies x=y$;
  \item $\leq$ ist \term{transitiv},
    d.h. $\forall x,y,z \in K \holds x \leq y \land y \leq z \implies x \leq z$;
  \item $\leq$ ist \term{linear},
    d.h. es gilt $\forall x,y \in K \holds x \leq y \lor y \leq x$.
  \end{itemize}
  Wir verwenden die üblichen Notationen für alle $x,y \in K$:
  \begin{itemize}
  \item $x < y \dfiff x \leq y \land x \neq y$
  \item $x \geq y \dfiff y \leq x$
  \item $x > y \dfiff y < x$
  \end{itemize}
  Damit gilt (Übung) für alle $x,y \in K$:
  \begin{itemize}
  \item $x < y \lor x > y \lor x = y$
  \item $\neg (x \leq y) \iff x > y$
  \item $\neg (x \geq y) \iff x < y$
  \end{itemize}
  Wenn $x > 0$ gilt, nennen wir $x$ \term{positiv},
  und wenn $x < 0$ gilt, nennen wir $x$ \term{negativ}.
  \par
  In diesem Absatz haben wir bis hier hin die Körperstruktur nicht genutzt.
  Das ändert sich nun.
  Wir nennen $(K,+,\cdot,0,1,\leq)$ einen \term{angeordneten Körper}, wenn zusätzlich gilt:
  \begin{enumerate}
  \item[(O1)] $\forall x,y,z \in K \holds x \leq y \implies x+z \leq y+z$
  \item[(O2)] $\forall x,y \in K \holds x,y \geq 0 \implies xy \geq 0$
  \end{enumerate}
\end{para}

Der folgende Satz listet viele Regeln in angeordneten Körpern.
Wir beweisen nur ein paar davon und überlassen die Beweise der übrigen zur Übung.

\begin{satz}[Regeln in angeordneten Körpern]
  \label{rlbasic/cha01:ofield-rules}
  Es sei $(K,+,\cdot,0,1,\leq)$ ein angeordneter Körper.
  Für alle ${x,y,u,v \in K}$ und alle $a \in \nz{K}$ gilt:
  \begin{enumerate}
  \item\label{rlbasic/cha01:ofield-rules:0<1}
    $0 < 1$
  \item\label{rlbasic/cha01:ofield-rules:07}
    $x \leq y \iff x + u \leq y + u$
  \item\label{rlbasic/cha01:ofield-rules:08}
    $x < y \iff x + u < y + u$
  \item\label{rlbasic/cha01:ofield-rules:09}
    $x \leq y \land u \leq v \implies x + u \leq y + v$
  \item\label{rlbasic/cha01:ofield-rules:10}
    $x < y \land u \leq v \implies x + u < y + v$
  \item\label{rlbasic/cha01:ofield-rules:01}
    $x \leq y \iff x - y \leq 0 \iff 0 \leq y - x \iff -y \leq -x$
  \item\label{rlbasic/cha01:ofield-rules:02}
    $0 \leq x \iff -x \leq 0$
  \item\label{rlbasic/cha01:ofield-rules:03}
    $x < y \iff x - y < 0 \iff 0 < y - x \iff -y < -x$
  \item\label{rlbasic/cha01:ofield-rules:04}
    $0 < x \iff -x < 0$
  \item\label{rlbasic/cha01:ofield-rules:06}
    $0 < x \iff 0 < x^{-1}$
  \item\label{rlbasic/cha01:ofield-rules:10a}
    $x^{2} \geq 0$
  \item $a^{2} > 0$
  \item\label{rlbasic/cha01:ofield-rules:11}
    $0 \leq u \land x \leq y \implies ux \leq uy$
  \item\label{rlbasic/cha01:ofield-rules:12}
    $0 < a \implies (x \leq y \iff ax \leq ay)$
  \item\label{rlbasic/cha01:ofield-rules:13}
    $0 < a \implies (x < y \iff ax < ay)$
  \item\label{rlbasic/cha01:ofield-rules:14}
    $a < 0 \implies (x \leq y \iff ax \geq ay)$
  \item\label{rlbasic/cha01:ofield-rules:15}
    $a < 0 \implies (x < y \iff ax > ay)$
  \item\label{rlbasic/cha01:ofield-rules:16}
    $0 < xy \iff (0 < x \land 0 < y) \lor (x < 0 \land y < 0)$
  \item\label{rlbasic/cha01:ofield-rules:17a}
    $0 < x \leq y \implies (0 < y^{-1} \leq x^{-1}) \land (1 \leq x^{-1} y)$
  \item\label{rlbasic/cha01:ofield-rules:17b}
    $0 < x < y \implies (0 < y^{-1} < x^{-1}) \land (1 < x^{-1} y)$
  \item\label{rlbasic/cha01:ofield-rules:18}
    $1 < a \implies (0 < x \iff x < ax)$
  \item\label{rlbasic/cha01:ofield-rules:19}
    $0 < a < 1 \implies (0 < x \iff ax < x)$
  \item\label{rlbasic/cha01:ofield-rules:20}
    $0 \leq x \leq y \land 0 \leq u \leq v \implies 0 \leq xu \leq yv$
  \item\label{rlbasic/cha01:ofield-rules:21}
    $0 \leq x < y \land 0 \leq u < v \implies 0 \leq xu < yv$
  \item\label{rlbasic/cha01:ofield-rules:22}
    $0 < x \leq y \land 0 \leq u < v \implies 0 \leq xu < yv$
  \end{enumerate}
\end{satz}

\begin{proof}[Beispiele]
  \begin{enumerate}
  \item[\ref*{rlbasic/cha01:ofield-rules:0<1}]
    Es gilt:
    \begin{IEEEeqnarray*}{0l"s}
      0 \geq 1 \implies 0 + (-1) \geq 1 + (-1) & (O1) \\
      \implies -1 \geq 0 & neutrales \bzw inverses Element \\
      \implies (-1) (-1) \geq 0 & (O2) \\
      \implies 1 \cdot 1 \geq 0 & \autoref{rlbasic/cha01:field-rules} \\
      \implies 1 \geq 0
    \end{IEEEeqnarray*}
    Die Annahme $0 \geq 1$ führt also auf $1 \geq 0$,
    also auf $0=1$, was falsch ist.
  \item[\ref*{rlbasic/cha01:ofield-rules:07}]
    Die eine Implikation ist offenbar (O1).
    Für die andere Implikation bemerke:
    \begin{IEEEeqnarray*}{0l"s}
      x + u \leq y + u \implies x + u + (-u) \leq y + u + (-u) & (O1) \\
      \implies x \leq y & additiv Inverses
    \end{IEEEeqnarray*}
  \item[\ref*{rlbasic/cha01:ofield-rules:08}]
    Mit \autoref{rlbasic/cha01:ofield-rules:07} und \autoref{rlbasic/cha01:field-rules} gilt:
    \begin{IEEEeqnarray*}{0l"s}
      x < y \iff x \leq y \land x \neq y
      \iff x + u \leq y + u \land x + u \neq y + u
      \iff x + u < y + u
    \end{IEEEeqnarray*}
  \item[\ref*{rlbasic/cha01:ofield-rules:11}]
    Es gilt:
    \begin{IEEEeqnarray*}{0l"s+x*}
      0 \leq a \land x \leq y
      \implies 0 \leq a \land 0 \leq y - x & \autoref{rlbasic/cha01:ofield-rules:01} \\
      \implies 0 \leq a (y-x) & (O2) \\
      \implies 0 \leq ay - ax & Distributivgesetz \\
      \implies ax \leq ay & \autoref{rlbasic/cha01:ofield-rules:01} & \qedhere
    \end{IEEEeqnarray*}
  \end{enumerate}
\end{proof}

\begin{para}[Immer wieder plus $1$]
  \label{rlbasic/cha01:kette}
  Aus dem Satz folgt:
  \begin{IEEEeqnarray*}{0l}
    0 < 1 < 1+1 < 1+1+1 < 1+1+1+1 < \hdots
  \end{IEEEeqnarray*}
  Insbesondere gilt $1+1 \neq 0$, also nach \autoref{rlbasic/cha01:field-rules}
  gilt $\forall x \in K \holds x=-x \iff x=0$.
\end{para}

\section{Supremumseigenschaft}

\begin{para}[Erinnerung zu geordneten Mengen]
  Aus früheren
  \begin{MathInfB}
    Semestern
  \end{MathInfB}
  \begin{MathIngI}
    Kapiteln
  \end{MathIngI}\xspace
  sind die folgenden Begriffe in geordneten
  und insbesondere in linear geordneten Mengen bekannt:
  Obere und untere Schranke,
  größtes und kleinstes Element,
  Supremum und Infimum.
  Wir wiederholen diese kurz, vereinfacht und angepasst für unsere Zwecke.
  Es sei $K$ eine linear geordnete Menge und $M \subseteq K$.
  Definitionen:
  \begin{itemize}
  \item $\upperbounds{M}$ und $\lowerbounds{M}$ schreiben wir
    für die Menge der oberen \bzw unteren Schranken von $M$.
  \item Es hat $M$ ein größtes Element genau dann,
    wenn $M \cap \upperbounds{M} \neq \emptyset$ gilt;
    und dann besteht dieser Schnitt aus genau einem Element,
    welches das größte Element von $M$ ist (Übung).
    Wie im Kontext der Analysis üblich,
    nennen wir dieses das \term{Maximum} von $M$ und bezeichnen es mit $\max(M)$.
    Analog definiere das \term{Minimum} von $M$, geschrieben $\min(M)$,
    als das kleinste Element von $M$, wenn dieses existiert;
    oder, äquivalent, als das einzige Element von
    $M \cap \lowerbounds{M}$, wenn diese Menge nichtleer ist.
  \item Wenn $\upperbounds{M}$ ein Minimum hat,
    dann nennen wir dies das \term{Supremum} von $M$ und schreiben $\sup(M) \df \min(\upperbounds{M})$.
    Es hat $M$ also ein Supremum genau dann,
    wenn $\upperbounds{M} \cap \lowerbounds{(\upperbounds{M})}$ nichtleer ist,
    und dann ist das Supremum das einzige Element dieser Menge.
    Analog definiere das \term{Infimum} von $M$ als $\inf(M) \df \max(\lowerbounds{M})$,
    wenn dieses Maximum existiert;
    oder, äquivalent, als das einzige Element von
    $\lowerbounds{M} \cap \upperbounds{(\lowerbounds{M})}$, wenn diese Menge nichtleer ist.
  \item Man sieht leicht (Übung): Wenn $M$ ein Maximum hat,
    dann gilt $\max(M) = \sup(M)$; analog für Minimum und Infimum.
    Es gibt durchaus Beispiele, wo zwar das Supremum existiert aber kein Maximum;
    oder wo das Infimum existiert aber kein Minimum.
  \end{itemize}
\end{para}

\begin{satz}[$\veps$\=/Charakterisierung von Supremum und Infimum]
  Es sei $(K,+,\cdot,0,1,{\leq})$ ein angeordneter Körper.
  Es sei $\emptyset \neq M \subseteq K$ und $s \in \upperbounds{M}$.
  Dann sind äquivalent:
  \begin{enumerate}
  \item\label{rlbasic/cha01:sup-veps:1} $s = \sup(M)$
  \item\label{rlbasic/cha01:sup-veps:2} $\forall \veps > 0 \innerholds \exists x \in M \holds s - \veps < x$
  \end{enumerate}
  Eine analoge Aussage gilt für untere Schranken und das Infimum.
\end{satz}

\begin{proof}
  \impref{rlbasic/cha01:sup-veps:1}{rlbasic/cha01:sup-veps:2}
  Es sei $\veps > 0$.
  Dann ist $s - \veps < s$, also $s - \veps \not\in \upperbounds{M}$, denn ${s = \min(\upperbounds{M})}$.
  Nach der Definition der oberen Schranke gibt es also $x \in M$ so,
  dass $s - \veps < x$ gilt.
  \par
  \impref{rlbasic/cha01:sup-veps:2}{rlbasic/cha01:sup-veps:1}
  Wir haben $s \in \upperbounds{M}$ und $s \in \lowerbounds{(\upperbounds{M})}$ zu zeigen.
  Das erste ist klar nach Voraussetzung.
  Es sei nun $t \in \upperbounds{M}$, zu zeigen ist $s \leq t$.
  Angenommen, $s > t$.
  Dann ist $s - t > 0$, und somit gibt es nach \ref*{rlbasic/cha01:sup-veps:2} ein $x \in M$ so,
  dass $s - (s-t) < x$ gilt, also $t < x$.
  Dies, zusammen mit $x \in M$, ist ein Widerspruch zu $t \in \upperbounds{M}$.
\end{proof}

\begin{para}[Supremumseigenschaft]
  Es sei $K=(K,+,\cdot,0,1,{\leq})$ ein angeordneter Körper.
  Wir sagen, $K$ hat die \term{Supremumseigenschaft},
  insgesamt also, dass $K$ ein \term{angeordneter Körper mit Supremumseigenschaft} ist,
  wenn jede nach oben beschränkte Teilmenge von $K$ ein Supremum besitzt,
  bezüglich der geordneten Menge $(K,\leq)$.
  Man sieht leicht (Übung), dass zur Supremumseigenschaft äquivalent ist,
  dass jede nach unten beschränkte Menge ein Infimum besitzt.
\end{para}

Ohne Beweis zitieren wir folgenden fundamentalen Satz:

\begin{satz}
  Bis auf Isomorphie gibt es genau einen angeordneten Körper mit Supremumseigenschaft.
\end{satz}

\begin{para}[Reelle Zahlen]
  \begin{MathInfB}
    In früheren Semestern und auf der Schule kam öfters der Körper $\RR$ der reellen Zahlen vor
    (auch wenn er auf der Schule vielleicht nicht als \enquote{Körper} bekannt war).
  \end{MathInfB}
  \begin{MathIngI}
    Auf der Schule kam öfters der Körper $\RR$ der reellen Zahlen vor
    (auch wenn er dort vielleicht nicht als \enquote{Körper} bekannt war).
  \end{MathIngI}\xspace
  Dies ist ein angeordneter Körper mit Supremumseigenschaft und damit,
  nach obigem Satz, isomorph zu jedem anderen angeordneten Körper mit Supremumseigenschaft.
  Wir werden in der Analysis viel mit $\RR$ arbeiten.
  Dabei setzen wir bei den theoretischen Resultaten nur voraus,
  dass es sich um einen angeordneten Körper mit Supremumseigenschaft handelt;
  und wir verwenden die im folgenden Absatz angegebene Einbettung der natürlichen Zahlen,
  woraus wir die ganzen und rationalen Zahlen als Elemente von $\RR$ erhalten
  und auch ihre Darstellung als Dezimalzahlen und als Brüche aus Dezimalzahlen,
  sowie die Möglichkeit, mit diesen zu rechnen.
  In Beispielen verwenden wir auch Darstellungen mit Dezimalpunkt,
  obwohl wir diese nicht formal eingeführt haben.
\end{para}

\begin{para}[Natürliche Zahlen]
  \label{rlbasic/cha01:nat}
  Wir setzen ohne formale Definition
  die natürlichen Zahlen $\NN = \set{0,1,2,3,\hdots}$
  inklusive ihrer Darstellung, Addition, Multiplikation, Ordnung voraus.
  Zum Beispiel wissen wir $1+3=4$ und $2 \cdot 9 = 18$ und $7 < 12$.
  Wir setzen ferner voraus:
  \begin{itemize}
  \item Jede nichtleere Teilmenge der natürlichen Zahlen hat ein Minimum.
  \item Jede endliche Teilmenge der natürlichen Zahlen hat Maximum und Minimum.
  \end{itemize}
\end{para}

\begin{para}[Einbettungen, Rechnen]
  In diesem Absatz indizieren wir die Konstanten,
  die Operationen und die Ordnung der natürlichen Zahlen
  zunächst mit dem Symbol~\enquote{$\NN$}.
  Insbesondere ist $n -_{\NN} m$ die gewöhnliche Differenz
  für alle $n,m \in \NN$ mit $n \geq_{\NN} m$.
  Hingegen bezeichnen ab jetzt $+$, $\cdot$ und $\leq$
  die Addition, Multiplikation und die Ordnung auf dem angeordneten Körper mit Supremumseigenschaft $\RR$
  (der, wie oben erklärt, irgendein angeordneter Körper mit Supremumseigenschaft sein darf).
  Notationen aus \autoref{rlbasic/cha01:field-notation}
  und \autoref{rlbasic/cha01:angeordnet} finden Anwendung.
  Definiere:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{\vphi}{\NN}{\RR}{n \mapsto \sum_{i=1_{\NN}}^{n} 1}
  \end{IEEEeqnarray*}
  Dabei bezieht sich $\sum$ auf die Addition in $\RR$,
  und freilich ist $\sum_{i=1_{\NN}}^{0_{\NN}} 1 \df 0$ vereinbart,
  also es ist $\vphi(0_{\NN}) = 0$; außerdem $\vphi(1_{\NN}) = 1$.
  Man sieht leicht (Übung), dass $\vphi$ streng monoton steigend
  (insbesondere injektiv) ist und sich damit die Aussagen aus \autoref{rlbasic/cha01:nat}
  über die Existenz von Maximum und Minimum auf Teilmengen von $\img(\vphi)$ übertragen.
  Ferner sieht man leicht (Übung):
  \begin{IEEEeqnarray*}{0l}
    \forall n \in \NN \innerholds \forall x \in \RR \holds \vphi(n) x = \sum_{i=1_{\NN}}^{n} x
  \end{IEEEeqnarray*}
  Ebenso:
  \begin{IEEEeqnarray}{0l}
    \label{rlbasic/cha01:rechnen:1}
    \forall n,m \in \NN \holds \vphi(n) + \vphi(m) = \vphi(n+_{\NN}m) \\
    \label{rlbasic/cha01:rechnen:2}
    \forall n,m \in \NN, n \geq_{\NN} m \holds \vphi(n) - \vphi(m) = \vphi(n-_{\NN}m)
    \land \vphi(m) - \vphi(n) = -\vphi(n-_{\NN}m) \\
    \label{rlbasic/cha01:rechnen:3}
    \forall n,m \in \NN \holds  \vphi(n)\vphi(m) = \vphi(n \cdot_{\NN} m)
  \end{IEEEeqnarray}
  Damit können wir viele Rechnungen in $\RR$ durchführen.
  Zum Beispiel berechnen wir $\vphi(n) + \vphi(m)$ mit $n,m \in \NN$,
  indem wir zunächst wie gewohnt $n+_{\NN}m$ berechnen,
  und das Ergebnis dann in $\vphi$ einsetzen, d.h. eine entsprechende Anzahl $1$ addieren.
  Zum Beispiel ist $\vphi(2_{\NN}) + \vphi(5_{\NN}) = \vphi(7_{\NN}) = 1+1+1+1+1+1+1$.
  \par
  Wir machen eine Konvention, die uns erlauben wird, mit $\RR$ im Wesentlichen so umzugehen,
  wie wir es aus der Schule
  \begin{MathInfB}
    und früheren Semestern
  \end{MathInfB}\xspace
  gewohnt sind
  -- obwohl wir weiterhin nur voraussetzen, dass $\RR$ ein angeordneter Körper mit Supremumseigenschaft ist.
  Nämlich wir vereinbaren, dass wir wo immer eine natürliche Zahl $n$ steht,
  statt dessen $\vphi(n)$ verstehen; es sei denn,
  an der Stelle ergibt nur eine natürliche Zahl Sinn,
  wie zum Beispiel bei den Grenzen einer Summe;
  in $\sum_{i=1}^{n}$ verstehen wir $1$ und $n$ als natürliche Zahlen im ursprünglichen Sinne
  (und dürfen daher wieder $1$ statt $1_{\NN}$ schreiben).
  Es gilt nun also zum Beispiel $3x = x+x+x$ für alle $x \in \RR$.
  Wir verstehen vermöge dieser Konvention von nun an $\NN$ als Teilmenge von $\RR$,
  also $\NN \subseteq \RR$,
  und nur wenn an einer Stelle eine natürliche Zahl im ursprünglichen Sinne benötigt wird,
  dann meinen wir dort das entsprechende Urbild unter $\vphi$.
  Zum Beispiel verstehen wir, wie eben schon mit anderen Worten erklärt,
  $\sum_{i=1}^{n}$ als~$\sum_{i=\vphi^{-1}(1)}^{\vphi^{-1}(n)}$.
  \par
  Die Kette aus \autoref{rlbasic/cha01:kette} schreibt sich damit so:
  \begin{IEEEeqnarray*}{0l}
    0 < 1 < 2 < 3 < 4 < \hdots
  \end{IEEEeqnarray*}
  Auf obiger Einbettung der natürlichen Zahlen aufbauend
  definieren wir die ganzen und die rationalen Zahlen als Teilmengen von $\RR$:
  \begin{IEEEeqnarray*}{0l+l}
    \ZZ \df \NN \cup \set{-n \suchthat n \in \NN} \subseteq \RR &
    \QQ \df \set{\frac{n}{m} \suchthat n, m \in \ZZ \land m \neq 0} \subseteq \RR
  \end{IEEEeqnarray*}
  Auch damit können wir mithilfe von
  \eqref{rlbasic/cha01:rechnen:1},
  \eqref{rlbasic/cha01:rechnen:2}
  und \eqref{rlbasic/cha01:rechnen:3}
  rechnen wie gewohnt,
  zum Beispiel hier einmal ganz ausführlich das Addieren zweier Brüche:
  \begin{IEEEeqnarray*}{0l"s}
    \frac{12}{7} + \frac{2}{3}
    = \frac{12}{7} \cdot 1 + \frac{2}{3} \cdot 1 & neutrales Element \\
    = \frac{12}{7} \cdot \frac{3}{3} + \frac{2}{3} \cdot \frac{7}{7} & inverses Element \\
    = (12 \cdot 3) \cdot (7 \cdot 3)^{-1} + (2 \cdot 7) \cdot (3 \cdot 7)^{-1} & Assoziativität \\
    = 36 \cdot 21^{-1} + 14 \cdot 21^{-1} & \eqref{rlbasic/cha01:rechnen:3} \\
    = (36 + 14) \cdot 21^{-1} & Distributivgesetz \\
    = 50 \cdot 21^{-1} & \eqref{rlbasic/cha01:rechnen:1} \\
    = \frac{50}{21}
  \end{IEEEeqnarray*}
  Insbesondere auch Addition und Differenzbildung zwischen ganzen Zahlen funktioniert wie gewohnt,
  wie man sich anhand von~\eqref{rlbasic/cha01:rechnen:2} leicht überlegt.
\end{para}

\begin{MathInfB}
  Aus früheren Semestern dürfte der folgende Satz bekannt sein.
  Der Beweis lässt sich auch leicht unter unseren jetzigen Voraussetzungen führen,
  d.h. wenn wir nur wissen, dass $\RR$ ein angeordneter Körper mit Supremumseigenschaft ist
  und die Konventionen aus obigem Absatz verwendet werden.
\end{MathInfB}

\begin{satz}[Binomischer Lehrsatz]
  Für alle $n \in \NN$ und alle $x,y \in \RR$ gilt:
  \begin{IEEEeqnarray*}{0l}
    (x+y)^{n} = \sum_{k=0}^{n} \binom{n}{k} x^k y^{n-k}
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}
  Übung.
\end{proof}

\begin{para}[Unendlich]
  \label{rlbasic/cha01:infty}
  Wir fixieren mit $\pinfty$ und $\ninfty$ irgendwelche zwei verschiedene Objekte,
  die nicht Elemente von $\RR$ sind,
  und nennen sie \term{unendlich} \bzw \term{minus unendlich}.
  Die Relation $\leq$ wird erweitert auf $\RRbar \df \RR \cup \set{\pinfty, \ninfty}$,
  nämlich wir definieren, dass
  $x \leq \pinfty$ und $\ninfty \leq x$ gilt für alle $x \in \RRbar$,
  also insbesondere $x < \pinfty$ und $\ninfty < x$ wenn $x \in \RR$,
  und $\ninfty < \pinfty$.
  \par
  Es ist wichtig zu verstehen,
  dass die Bedeutung von $\pinfty$ und $\ninfty$
  nur durch unsere Definitionen zustandekommt.
  Daher sind zum Beispiel Ausdrücke der Form
  $\frac{1}{\pinfty}$ oder $1000 + (\ninfty)$ oder $\ninfty + (\pinfty)$ \etc \emphasis{nicht sinnvoll},
  solange wir nicht gesagt haben, was sie für uns bedeuten sollen.
  \par
  Für $M \subseteq \RR$ bleiben $\upperbounds{M}$ und $\lowerbounds{M}$ so definiert wie zuvor,
  nämlich bezüglich der geordneten Menge $(\RR,\leq)$.
  Damit bleiben $\sup(M), \inf(M), \max(M), \min(M)$ auch so definiert wie bisher,
  wenn existent.
\end{para}

\begin{para}[Intervalle]
  Für alle $a, b \in \RRbar$ notieren wir:
  \begin{IEEEeqnarray*}{0rCl+s}
    \intcc{a}{b} & \df
    & \set{x \in \RRbar \suchthat a \leq x \leq b} & abgeschlossenes Intervall \\
    \intoo{a}{b} & \df
    & \set{x \in \RRbar \suchthat a < x < b} & offenes Intervall \\
    \intco{a}{b} & \df
    & \set{x \in \RRbar \suchthat a \leq x < b} & halboffenes Intervall \\
    \intoc{a}{b} & \df
    & \set{x \in \RRbar \suchthat a < x \leq b} & halboffenes Intervall
  \end{IEEEeqnarray*}
  Wenn $a < b$ gilt, so nennen wir das Intervall ein \term{echtes Intervall};
  das Intervall enthält dann unendlich viele Elemente (Übung).
  Man nennt $a$ und $b$ die \term{Grenzen} oder \term{Randpunkte} des Intervalls.
  Genauer ist $a$ die \term{linke Grenze} und $b$ die \term{rechte Grenze},
  oder \term{linker Randpunkt} \bzw \term{rechter Randpunkt}.
  Man sieht leicht (Übung),
  dass wenn $I$ ein nichtleeres Intervall ist,
  linker und rechter Randpunkt von $I$ durch $I$ tatsächlich eindeutig bestimmt sind.
  \par
  Für jedes Intervall $I$ mit linker Grenze $a$ und rechter Grenze $b$
  nennen wir $\intoo{a}{b}$ das \term{Innere} von $I$;
  es gilt offenbar $\intoo{a}{b} \subseteq I$.
  \par
  Spezielle Intervalle:
  \begin{IEEEeqnarray*}{0l"s}
    \RRnn  \df \intci{0} & nicht-negative reelle Zahlen \\
    \RRpos \df \intoi{0} & positive reelle Zahlen
  \end{IEEEeqnarray*}
\end{para}

\section{Folgerungen aus der Supremumseigenschaft}

\begin{satz}
  \label{rlbasic/cha01:unbeschraenkt}
  $\NN$ ist in $\RR$ nach oben unbeschränkt.
\end{satz}

\begin{proof}
  Angenommen, $\NN$ ist nach oben beschränkt.
  Nach der Supremumseigenschaft existiert dann $s \df \sup(\NN) \in \RR$.
  Dann gibt es $n \in \NN$ so, dass $s-1 < n$ gilt.
  Es folgt $s < n+1 \in \NN$, ein Widerspruch zu $s \in \upperbounds{\NN}$.
\end{proof}

\begin{corollary}[Archimedes-Ordnung]
  Es gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall x,y > 0 \innerholds \exists n \in \NN \holds nx > y
  \end{IEEEeqnarray*}
  (Natürlich meinen wir \enquote{$x,y \in \RRpos$} mit \enquote{$x,y > 0$}.)
\end{corollary}

\begin{proof}
  Übung.
\end{proof}

\begin{satz}[Aufrunden und Abrunden]
  Es sei $x \in \RR$.
  Dann gibt es $\ceil{x}, \floor{x} \in \ZZ$ so,
  dass $\floor{x} \leq x \leq \ceil{x}$ und $\ceil{x}-\floor{x} \leq 1$ gilt.
\end{satz}

\begin{proof}
  Es sei zunächst $x > 0$.
  Definiere $L \df \intic{x} \cap \NN$ und $U \df \intci{x} \cap \NN$.
  Dann ist $L \neq \emptyset$, denn $0 \in L$.
  Es ist auch $U \neq \emptyset$, wegen \autoref{rlbasic/cha01:unbeschraenkt};
  wähle irgendein $n \in U$.
  Dann ist $L \subseteq \setft{0}{n}$, also ist $L$ endlich.
  Definiere $a \df \max(L)$.
  Da $U$ nichtleer ist, können wir auch $b \df \min(U)$ definieren.
  Offenbar gilt $a \leq x \leq b$.
  \par
  Wir untersuchen noch die Differenz.
  Für Widerspruch nehmen wir $b - a > 1$ an.
  Dann gilt $a + 1 < b$, also $a + 1 \not \in U$.
  Da aber $a + 1 \in \NN$ gilt, bedeutet das $a + 1 \not \in \intci{x}$,
  also $a + 1 < x$.
  Daraus folgt $a + 1 \in L$, ein Widerspruch zur Definition von $a$.
  Das schließt den Beweis für $x > 0$.
  \par
  Der Fall $x=0$ ist klar, es sei also noch $x < 0$.
  Da $-x > 0$ gilt, haben wir schon $\ceil{-x}, \floor{-x} \in \NN$
  mit den entsprechenden Eigenschaften.
  Definiere $\ceil{x} \df -\floor{-x}$ und $\floor{x} \df -\ceil{-x}$.
  Dann gilt $\ceil{x}, \floor{x} \in \ZZ$.
  Ferner gilt:
  \begin{IEEEeqnarray*}{0l}
    \floor{x} = -\ceil{-x} \leq x \leq -\floor{-x} = \ceil{x}
  \end{IEEEeqnarray*}
  Schließlich gilt $\ceil{x} - \floor{x}
  = -\floor{-x} - (-\ceil{-x}) = \ceil{-x} - \floor{-x} \leq 1$.
\end{proof}

Als nächstes wollen wir die Existenz von Wurzeln beweisen.
Wir beginnen mit einer Beobachtung, die nichts mit der Supremumseigenschaft zu tun hat.

\begin{satz}[Monotonie der Potenz]
  \label{rlbasic/cha01:monotonie-potenz}
  Es sei $n \in \NNone$.
  Dann ist $\ffnx{f}{\RRnn}{\RRnn}{x \mapsto x^n}$ streng monoton steigend, also injektiv.
\end{satz}

\begin{proof}
  Es seien $x,y \in \RRnn$ mit $x < y$.
  Definiere $\veps \df y - x > 0$.
  Dann gilt:
  \begin{IEEEeqnarray*}{0l+x*}
    f(y) = f(x+\veps)
    = \sum_{k=0}^n \binom{n}{k} x^k \veps^{n-k}
    = \binom{n}{n} x^n + \sum_{k=0}^{n-1} \binom{n}{k} x^k \veps^{n-k} \\
    \geq f(x) + \binom{n}{0} x^0 \veps^n
    = f(x) + \veps^n > f(x)
    &\qedhere
  \end{IEEEeqnarray*}
\end{proof}

\begin{satz}[Existenz und Eindeutigkeit von Wurzeln]
  \label{rlbasic/cha01:wurzeln}
  Es sei $a \in \RRnn$ und $n \in \NNone$.
  Dann gibt es in $\RRnn$ genau ein $x$ so, dass $x^n = a$ gilt.
\end{satz}

\begin{proof}
  Die Eindeutigkeit ist klar wegen der Monotonie der Potenz.
  Um die Existenz nachzuweisen, definiere:
  \begin{IEEEeqnarray*}{0l}
    Y \df \set{y \in \RRnn \suchthat y^n \leq a}
  \end{IEEEeqnarray*}
  Dann gilt $0 \in Y$, also $Y \neq \emptyset$.
  Ferner ist $Y$ nach oben beschränkt, zum Beispiel durch $1+a$, was man wie folgt sieht:
  Wenn $y > 1+a$ gilt, dann $y^n > (1+a)^n \geq 1+a \geq a$, also $y \not \in Y$.
  \par
  Nach der Supremumseigenschaft gibt es $s \df \sup(Y) \in \RR$.
  Offenbar gilt $s \geq 0$, da $Y \subseteq \RRnn$.
  Wir zeigen $s^n = a$.
  \par
  Angenommen $s^{n} \neq a$.
  Wir behandeln nur den Fall $s^n < a$ und überlassen den anderen zur Übung.
  Definiere:
  \begin{IEEEeqnarray*}{0l}
    \veps \df \min\set{ \frac{a - s^n}{\sum_{k=0}^{n-1} \tbinom{n}{k} s^k} , \, 1 }
  \end{IEEEeqnarray*}
  Dann gilt $0 < \veps \leq 1$ und auch $0 < \veps^k \leq \veps \leq 1$ für alle $k \in \NNone$.
  Es folgt:
  \begin{IEEEeqnarray*}{0l}
    (s+\veps)^n
    = \sum_{k=0}^n \tbinom{n}{k} s^k \veps^{n-k}
    = s^n + \sum_{k=0}^{n-1} \tbinom{n}{k} s^k \veps^{n-k}
    \leq s^n + \veps \sum_{k=0}^{n-1} \tbinom{n}{k} s^k
    \leq s^n + (a - s^n) = a
  \end{IEEEeqnarray*}
  Also $s+\veps \in Y$.
  Das ist wegen $\veps > 0$ ein Widerspruch zu $s \in \upperbounds{S}$.
\end{proof}

\begin{para}[Notation und Monotonie der Wurzel]
  Die Zahl $x$ aus dem obigen Satz nennen wir die \xte{n} \term{Wurzel} von $a$
  und bezeichnen sie mit $\sqrt[n]{a}$, wobei $\sqrt{a} \df \sqrt[2]{a}$.
  Die Funktion $\ffnx{\sqrt[n]{\cdot}}{\RRnn}{\RRnn}{a \mapsto \sqrt[n]{a}}$
  ist als Umkehrfunktion einer streng monoton steigenden Funktion (nämlich der \xten{n} Potenz)
  selbst streng monoton steigend.
\end{para}

Wir nennen $\RR \setminus \QQ$ die Menge der \term{irrationalen Zahlen}.
Noch wissen wir nicht, ob diese Menge überhaupt Elemente enthält.
Der folgende Satz klärt dies:

\begin{satz}
  $\sqrt{2}$ ist irrational.
\end{satz}

\begin{proof}
  Angenommen $\sqrt{2} \in \QQ$.
  Dann finde $p,q \in \NNone$ so, dass $p$ und $q$ teilerfremd sind
  und gilt $\sqrt{2} = \frac{p}{q}$.
  Es folgt $2 = \frac{p^{2}}{q^{2}}$, also $2 q^{2} = p^{2}$.
  Also ist $p^{2}$ gerade;
  man sieht leicht (Übung), dass dann auch $p$ gerade ist,
  etwa $p = 2k$ mit $k \in \NNone$.
  Es folgt $2 q^{2} = p^{2} = 4 k^{2}$,
  also $q^{2} = 2 k^{2}$, also ist auch $q^{2}$ und damit $q$ gerade.
  Also sind $p$ und $q$ beide gerade, ein Widerspruch zur Teilerfremdheit.
\end{proof}

\begin{satz}
  Zwischen je zwei verschiedenen reellen Zahlen
  liegen unendlich viele rationale und unendlich viele irrationale Zahlen.
\end{satz}

\begin{proof}
  Übung.
\end{proof}
