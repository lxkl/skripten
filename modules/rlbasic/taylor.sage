f(z) = 1/(1+z^2)
def taylor(z_k, n_max):
    show([N(( abs(f.diff(n)(z_k)) / factorial(n) )^(1/n))
        for n in range(n_max-20,n_max)])
taylor(0, 500)
taylor(1, 500)
