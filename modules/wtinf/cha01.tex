\chapter{Grundlagen der Wahrscheinlichkeitstheorie}
\label{wtinf/cha01}

\lxklKitReviewWarning

\section{Wahrscheinlichkeitsraum}

\begin{para}[\sigalg, Eventraum]
  Es sei $\Om$ eine nicht\-/leere Menge und $\SIG \subseteq \cP(\Om)$.
  Für jedes $A \subseteq \Om$ definiere das \term{Komplement} von $A$ (in $\Om$)
  als $A^{c} \df \Om \setminus A$.
  Wir nennen $\SIG$ eine \term{\sigalg} über~$\Om$,
  wenn gilt:
  \begin{enumerate}
  \item $\Om \in \SIG$
  \item $\forall A \in \SIG \holds A^{c} \in \SIG$;
    man sagt: $\SIG$ ist \term{abgeschlossen unter Komplementen}.
  \item $\forall \MM \subseteq \SIG \with \text{abzählbar} \holds \bigcup_{A \in \MM} A \in \SIG$;
    man sagt: $\SIG$ ist \term{abgeschlossen unter abzählbaren Vereinigungen}.
    Bei uns bedeutet \term{abzählbar}: endlich oder abzählbar unendlich.
    Es bezieht sich hier auf die Anzahl der Mengen, die vereinigt werden,
    nicht auf diese Mengen oder ihre Vereinigung selbst,
    d.h. $A \in \MM$ darf überabzählbar sein.
  \end{enumerate}
  Das Paar $(\Om, \SIG)$ nennen wir einen \term{Ereignisraum} oder \term{Eventraum}.
  In diesem Kontext nennen wir \dots
  \begin{itemize}
  \item $\Om$ den \term{Ergebnisraum} und jedes Element von $\Om$ ein \term{Ergebnis};
  \item jedes Element von $\SIG$ ein \term{Ereignis} oder \term{Event}.
  \end{itemize}
  Wir verzichten hier und auch im weiteren Verlauf dieses Kapitels bewusst
  auf eine Diskussion von Anwendungen oder Analogien;
  sondern wir nehmen die Bezeichnungen als solche hin.
\end{para}

\begin{example}[Eventraum]
  Es sei $\Om$ eine Menge.
  Dann ist $\set{\emptyset, \Om}$ und auch $\cP(\Om)$ eine \sigalg über $\Om$,
  also $(\Om, \set{\emptyset, \Om})$ und $(\Om, \cP(\Om))$ sind Eventräume.
  Als weiteres Beispiel definiere $\Om \df \setn{4}$
  und $\SIG \df \set{\emptyset, \set{1,2}, \set{3,4}, \Om}$;
  dann ist $(\Om, \SIG)$ ein Eventraum.
  Die dazugehörigen Beweise sind leicht zu erbringen und zur Übung empfohlen.
\end{example}

\begin{satz}[Einfache Eigenschaften eines Eventraumes]
  Es sei $(\Om,\SIG)$ ein Eventraum. Dann gilt:
  \begin{enumerate}
  \item $\emptyset \in \SIG$
  \item $\forall \MM \subseteq \SIG \with \text{abzählbar} \with \MM \neq \emptyset
    \holds \bigcap_{A \in \MM} A \in \SIG$
  \item $\forall A, B \in \SIG \holds A \setminus B \in \SIG$
  \end{enumerate}
\end{satz}

\begin{proof}
  \begin{enumerate}
  \item Es gilt $\emptyset = \Om^{c} \in \SIG$; Letzteres da $\Om \in \SIG$ gilt
    und $\SIG$ unter Komplementen abgeschlossen ist.
  \item Es sei $\MM \subseteq \SIG$ abzählbar mit $\MM \neq \emptyset$. Wir zeigen:
    \begin{IEEEeqnarray*}{0l}
      \bigcap_{A \in \MM} A = \p{\bigcup_{A \in \MM} A^{c}}^{c}
    \end{IEEEeqnarray*}
    Daraus folgt die Behauptung, denn $A^{c}$ ist Element von $\SIG$ für alle $A \in \MM$,
    und damit auch $\bigcup_{A \in \MM} A^{c}$
    und damit auch das Komplement davon.
    \par
    Um nun obige Mengengleichheit einzusehen, bemerke dass für alle $x \in \Om$ gilt:
    \begin{IEEEeqnarray*}{0l}
      x \in \p{\bigcup_{A \in \MM} A^{c}}^{c}
      \iff
      x \not\in \bigcup_{A \in \MM} A^{c}
      \iff
      \forall A \in \MM \holds x \not\in A^{c} \\
      \eqin \iff
      \forall A \in \MM \holds x \in A
      \iff
      x \in \bigcap_{A \in \MM} A
    \end{IEEEeqnarray*}
  \item Es gilt $A \setminus B = A \cap B^{c} \in \SIG$.\qedhere
  \end{enumerate}
\end{proof}

\begin{para}[W'maß]
  Es sei $(\Om, \SIG)$ ein Eventraum.
  Wir nennen $\MM \subseteq \SIG$ \term{disjunkt},
  wenn die Elemente von $\MM$ \term{paarweise disjunkt} sind,
  d.h. wenn $A \cap B = \emptyset$ gilt für alle $A, B \in \MM$ mit $A \neq B$.
  \par
  Wir nennen $\ffn{\PR}{\SIG}{\intcc{0}{1}}$ ein \term{Wahrscheinlichkeitsmaß (W'maß)}
  auf $(\Om, \SIG)$, wenn gilt:
  \begin{description}[font=\it]
  \item[Normierung:] $\PR(\Om) = 1$
  \item[Additivität:] $\forall \MM \subseteq \SIG \with \text{abzählbar, disjunkt} \holds
    \PR\p{\dotbigcup_{A \in \MM} A} = \sum_{A \in \MM} \PR(A)$
  \end{description}
  Das Tripel $(\Om, \SIG, \PR)$ nennen wir einen \term{Wahrscheinlichkeitsraum (W'raum)}
  und $\PR(A)$ die \term{Wahrscheinlichkeit} (W'keit) von $A$ für jedes $A \in \SIG$,
  also für jedes Event~$A$.
  Wir nennen $\PR\p{A^{c}}$ die \term{Gegenw'keit} von $A$.
\end{para}

\begin{para}[Erklärung zur Summe]
  Was ist mit $\sum_{A \in \MM} \PR(A)$ gemeint,
  wenn $\MM$ abzählbar unendlich ist
  und für unendlich viele $A \in \MM$ gilt $\PR\p{A} > 0$?
  Dann meinen wir damit $\sum_{i=1}^{\infty} \PR\p{A_{i}}$,
  also den Limes dieser Reihe,
  wobei $\seq{A_{i}}{i \geq 1}$ irgendeine Abzählung von $\MM$ ist.
  Nach einem Satz aus der Analysis ist dieser Limes unabhängig von dieser Abzählung,
  da die Summanden nicht negativ sind.
\end{para}

\begin{satz}[Einfache Eigenschaften eines W'raumes]
  \label{wtinf/cha01:wraum}
  Es sei $(\Om, \SIG, \PR)$ ein W'raum. Dann gilt:
  \begin{enumerate}
  \item $\PR\p{\emptyset} = 0$
  \item $\forall A, B \in \SIG
    \with A \cap B = \emptyset \holds \PR\p{A \dotcup B} = \PR\p{A} + \PR\p{B}$
  \item $\forall A, B \in \SIG \with
    A \subseteq B \holds \PR\p{B \setminus A} = \PR\p{B} - \PR\p{A}$
  \item\label{wtinf/cha01:wraum:mono}
    $\forall A, B \in \SIG \with A \subseteq B \holds \PR\p{A} \leq \PR\p{B}$
  \item $\forall A \in \SIG \holds \PR\p{A^{c}} = 1 - \PR\p{A}
    \,\land\, \PR\p{A} = 1 - \PR\p{A^{c}}$
  \item\label{wtinf/cha01:wraum:inklexkl}
    $\forall A, B \in \SIG \holds \PR\p{A \cup B}
    = \PR\p{A} + \PR\p{B} - \PR\p{A \cap B}$
  \item\label{wtinf/cha01:wraum:union-2}
    $\forall A, B \in \SIG \holds \PR\p{A \cup B}
    \leq \PR\p{A} + \PR\p{B}$
  \item $\forall \seq{A_{i}}{i \geq 1} \in \seqset{\SIG}
    \with \text{monoton steigend\footnotemark}
    \holds \limx{i} \PR\p{A_{i}} = \PR\p{\bigcup_{i=1}^{\infty} A_{i}}$
    \footnotetext{$A_{1} \subseteq A_{2} \subseteq \hdots$}
  \item $\forall \seq{A_{i}}{i \geq 1} \in \seqset{\SIG}
    \holds \limx{k} \PR\p{\bigcup_{i=1}^{k} A_{i}} = \PR\p{\bigcup_{i=1}^{\infty} A_{i}}$
  \item $\forall \MM \subseteq \SIG \with \text{abzählbar} \holds
    \PR\p{\bigcup_{A \in \MM} A} \leq \sum_{A \in \MM} \PR\p{A}$
  \end{enumerate}
\end{satz}

\begin{proof}
  \begin{enumerate}
  \item Es gilt wegen der Additivität, angewendet auf $\MM = \set{\emptyset, \Om}$
    unter Beachtung von $\Om \neq \emptyset$:
    \begin{IEEEeqnarray*}{0l}
      1 = \PR\p{\Om} = \PR\p{\Om \dotcup \emptyset}
      = \PR\p{\Om} + \PR\p{\emptyset}
      = 1 + \PR\p{\emptyset}
    \end{IEEEeqnarray*}
    Es folgt $0 = \PR\p{\emptyset}$.
  \item Das sieht zunächst nach einem einfachen Spezialfall der Additivität aus,
    namlich mit $\MM = \set{A,B}$.
    Das trifft aber nur zu, wenn $A \neq B$ gilt.
    Der einzige Grund, weswegen wir diesen Punkt aufgenommen haben,
    ist die Klärung des Falles $A = B$.
    Wenn $A = B$ und $A \cap B = \emptyset$ gilt,
    dann ist $A = B = \emptyset$, und es gilt nach dem vorigen Punkt
    $\PR\p{A \dotcup B} = \PR\p{\emptyset} = 0 = 0 + 0 = \PR\p{A} + \PR\p{B}$.
  \item Es seien $A, B \in \SIG$ so, dass $A \subseteq B$ gilt.
    Es gilt $B = (B \setminus A) \dotcup A$ und somit $\PR(B) = \PR(B \setminus A) + \PR(A)$.
  \item Es seien wieder $A, B \in \SIG$ so, dass $A \subseteq B$ gilt.
    Aus dem vorigen Punkt folgt $\PR\p{B} - \PR\p{A} = \PR\p{B \setminus A} \geq 0$.
  \item Es gilt $\PR(A^{c}) = \PR(\Om \setminus A) = \PR(\Om) - \PR(A) = 1 - \PR(A)$.
  \item Es gilt $A \cup B = A \dotcup (B \setminus (A \cap B))$
    und $A \cap B \subseteq B$.
    Es folgt
    $\PR(A \cup B) = \PR(A) + \PR(B \setminus (A \cap B))
    = \PR(A) + \PR(B) - \PR(A \cap B)$.
  \item Folgt direkt aus dem vorigen Punkt.
  \item Zunächst ist die Folge $\seq{\PR\p{A_{i}}}{i}$
    auf der linken Seite monoton steigend und beschränkt
    (durch $1$), also konvergent und zwar gegen einen Wert von höchstens~$1$.
    Definiere $B_{1} \df A_{1}$, und weiter
    für alle $k \geq 2$ definiere $B_{k} \df A_{k} \setminus \bigcup_{i=1}^{k-1} A_{i}$.
    Dann gilt
    $A_{k} = \bigcup_{i=1}^{k} A_{i} = \dotbigcup_{i=1}^{k} B_{i}$ für alle $k \geq 1$
    und somit auch $\bigcup_{i=1}^{\infty} A_{i} = \dotbigcup_{i=1}^{\infty} B_{i}$.
    Mit der Additivität folgt:
    \begin{IEEEeqnarray*}{0l}
      \PR\p{\bigcup_{i=1}^{\infty} A_{i}}
      = \PR\p{\dotbigcup_{i=1}^{\infty} B_{i}}
      = \limx{k} \sum_{i=1}^{k} \PR\p{B_{i}}
      = \limx{k} \PR\p{\dotbigcup_{i=1}^{k} B_{i}}
      = \limx{k} \PR\p{A_{k}}
    \end{IEEEeqnarray*}
  \item Für alle $k \geq 1$ definiere $B_{k} \df \bigcup_{i=1}^{k} A_{i}$.
    Dann ist $\seq{B_{k}}{k \geq 1} \in \seqset{\SIG}$ monoton steigend,
    und es gilt $\bigcup_{i=1}^{\infty} B_{i} = \bigcup_{i=1}^{\infty} A_{i}$.
    Die Behauptung folgt aus dem vorigen Punkt.
  \item Für endliches $\MM$ folgt die Behauptung leicht per Induktion aus~\ref{wtinf/cha01:wraum:union-2}.
    Es sei nun $\MM$ abzählbar unendlich und $\seq{A_{i}}{i \geq 1}$ eine Abzählung von $\MM$.
    Wenn $s \df \sum_{i=1}^{\infty} \PR\p{A_{i}} = \pinfty$ gilt, dann ist nichts zu tun;
    es gelte also $s \in \RRnn$.
    Für alle $k \in \NN$ gilt:
    \begin{IEEEeqnarray*}{0l}
      \PR\p{\bigcup_{i=1}^{k} A_{i}} \leq \sum_{i=1}^{k} \PR\p{A_{i}}
      \leq s
    \end{IEEEeqnarray*}
    Die Folge $\seq{\PR\p{\bigcup_{i=1}^{k} A_{i}}}{k \geq 1}$ konvergiert,
    da sie monoton steigend ist (wegen~\ref{wtinf/cha01:wraum:mono})
    und nach oben durch~$s$ beschränkt ist.
    Nach dem vorigen Punkt und der Monotonie des Limes folgt:
    \begin{IEEEeqnarray*}{0l+x*}
      \PR\p{\bigcup_{i=1}^{\infty} A_{i}}
      = \limx{k} \PR\p{\bigcup_{i=1}^{k} A_{i}}
      \leq s & \qedhere
    \end{IEEEeqnarray*}
  \end{enumerate}
\end{proof}

\begin{para}[Fortsetzungseigenschaft]
  Es sei $(\Om, \SIG, \PR)$ ein W'raum.
  Dann genügt es, wenn wir von $\PR$ gewisse Teile kennen,
  um andere Teile von $\PR$ zu erschließen.
  Wenn wir zum Beispiel $\PR(A)$ und $\PR(B)$ für gewisse $A,B \in \SIG$ mit ${A \cap B = \emptyset}$
  kennen, dann kennen wir $\PR(A \cup B)$, auch $\PR(A \dotcup B)$ geschrieben;
  nämlich nach der Additivität ist das $\PR(A) + \PR(B)$.
  \par
  Ein interessanter Spezialfall ist: $(\Om, \SIG, \PR)$ ist ein W'raum so,
  dass $\Om$ abzählbar ist, $\SIG = \cP(\Om)$ gilt,
  und $\PR(\set{\om})$ bekannt ist für alle $\om \in \Om$.
  Nach der Additivität gilt $\PR(A) = \sum_{\om \in A} \PR(\set{\om})$
  für alle $A \subseteq \Om$,
  also ist $\PR$ vollständig bekannt.
\end{para}

\lxklKitWipWarning

\section{Zufallsvariablen}

Es sei in diesem Abschnitt $(\Om, \SIG, \PR)$ ein W'raum.

\begin{para}[Quader]
  Für alle $a, b \in \RR^{n}$ schreibe folgende kartesischen Produkte, die Teilmengen von~$\RR^{n}$ sind:
  \begin{IEEEeqnarray*}{0l+l}
    \intcc{a}{b} \df \bigcart_{i=1}^{n} \intcc{a_{i}}{b_{i}} &
    \intoo{a}{b} \df \bigcart_{i=1}^{n} \intoo{a_{i}}{b_{i}} \\
    \intoc{a}{b} \df \bigcart_{i=1}^{n} \intoc{a_{i}}{b_{i}} &
    \intco{a}{b} \df \bigcart_{i=1}^{n} \intco{a_{i}}{b_{i}} \\
    \intic{b} \df \bigcart_{i=1}^{n} \intic{b_{i}} &
    \intio{b} \df \bigcart_{i=1}^{n} \intio{b_{i}}
  \end{IEEEeqnarray*}
  Dies sind Verallgemeinerungen der bekannten Intervalle.
  Sobald $a_{i} > b_{i}$ für ein $i \in \setn{n} \df \setft{1}{n}$ gilt,
  so sind die ersten vier dieser Mengen leer.
  Wenn $a<b$, dann nennen wir $\intco{a}{b}$ einen \term{halboffenen Quader},
  wobei $a < b$ bedeutet, dass $a_{i} < b_{i}$ für alle $i \in \setn{n}$ gilt.
  %%% TODO: auch für \leq
\end{para}

\begin{para}[Borelmengen]
  Es sei $\Lam$ eine nicht\-/leere Menge und $\cG \subseteq \cP(\Lam)$.
  Definiere die von $\cG$ \term{erzeugte} \sigalg:
  \begin{IEEEeqnarray*}{0l}
    \sig(\cG)
    \df \bigcap_{\substack{\cG \subseteq \cH \subseteq \cP(\Lam) \\ \text{$\cH$ ist \sigalg über $\Lam$} }}
    \cH
  \end{IEEEeqnarray*}
  Es ist $\sig(\cG)$ tatsächlich eine \sigalg, wie man leicht sieht,
  und zwar die bezüglich der Inklusion kleinste \sigalg über $\Lam$,
  welche $\cG$ noch als Teilmenge enthält.
  Wir brauchen diese Konstruktion nur einmal kurz.
  Und zwar definiere für alle $n \in \NNone$:
  \begin{IEEEeqnarray*}{0l+l}
    \cG^{n} \df \set{\intic{u} \suchthat u \in \RR^{n}} &
    \cB^{n} \df \sig\p{\cG^{n}}
  \end{IEEEeqnarray*}
  Wir nennen $\cB^{n}$ die \term{Borel\-/\sigalg} in $\RR^{n}$;
  schreibe $\cG \df \cG^{1}$ und $\cB \df \cB^{1}$.
  Die Mengen in $\cB^{n}$ nennen wir die \term{Borelmengen} in $\RR^{n}$.
\end{para}

\begin{para}[Beispiele für Borelmengen]
  So ziemlich alle Mengen, die uns üblicherweise unterkommen,
  sind Borelmengen.
  \begin{enumerate}
  \item Vereinigungen abzählbar vieler Borelmengen,
    Schnitte abzählbar vieler Borelmengen,
    sowie Mengendifferenzen zwischen zwei Borelmengen
    und Komplemente von Borelmengen sind wieder Borelmengen.
    Dies folgt leicht aus den Eigenschaften einer \sigalg.
  \item Offene Mengen und abgeschlossene Mengen sind Borelmengen~\cite[Absatz~(1.8)]{Georgii09}.
  \item Es enthält $\cB$ ale Elemente sämtliche Intervalle,
    denn jedes Intervall ist offen, abgeschlossen, oder,
    im halboffenen Fall, das Komplement der Vereinigung von zwei Intervallen,
    die jeweils offen oder abgeschlossen sind.
    Zum Beispiel gilt $\intoc{a}{b} = \RR^{n} \setminus (\intic{a} \cup \intoi{b})$
    für alle $a, b \in \RR$ mit $a \leq b$.
  \item Kartesische Produkte eindimensionaler Borelmengen
    sind Borelmengen~\cite[Absatz~(1.9)]{Georgii09}, d.h.:
    \begin{IEEEeqnarray*}{0l}
      \forall \eli{A}{n} \in \cB \holds \bigcart_{i=1}^{n} A_{i} \in \cB^{n}
    \end{IEEEeqnarray*}
  \item Daraus folgert man leicht: Abzählbare Mengen sind Borelmengen.
  \item Weitere Beispiele für Mengen in $\cB^{n}$, wobei $a,b,c,d \in \RR^{n}$:
    \begin{IEEEeqnarray*}{0l+l+l+l}
      \intcc{a}{b} & \intoo{a}{b} & \intoc{a}{b} & \intco{a}{b} \\
      \intcc{a}{b} \cup \intcc{c}{d} &
      \intcc{a}{b} \cup \intoo{c}{d} &
      \intcc{a}{b} \cup \intoc{c}{d} &
      \intcc{a}{b} \cup \intco{c}{d} \\
      \intic{b} & \intio{b} &
      \intci{a} & \intoi{a}
    \end{IEEEeqnarray*}
    Ebenso sind in $\cB^{n}$:
    \begin{IEEEeqnarray*}{0l+l}
      \intic{b} \cup \intci{a} &
      \intic{b} \cup \intoi{a} \\
      \intio{b} \cup \intci{a} &
      \intio{b} \cup \intoi{a}
    \end{IEEEeqnarray*}
    Es gilt auch $\RR, \RRnz, \RRnn, \RRpos \in \cB$.
  \end{enumerate}
\end{para}

\begin{para}[Logische Formeln über ZV]
  Für jeden Ausdruck $\vphi$ und für alle $\om \in \Om$ bezeichnen wir mit $\vphi(\om)$ den Ausdruck,
  der aus $\vphi$ entsteht,
  indem wir jede in $\vphi$ vorkommende
  Funktion der Form $\ffn{X}{\Om}{\RR}$ durch $X(\om)$ ersetzen, was ja eine reelle Zahl ist.
  Wir nennen $\vphi$ eine \term{logische Formel über ZV},
  wenn $\vphi(\om)$ für alle $\om \in \Om$ eine Aussage ist,
  d.h. wahr oder falsch ist.
  Definiere:
  \begin{IEEEeqnarray*}{0l}
    \event{\vphi} \df \set{\om \in \Om \suchthat \vphi(\om)}
  \end{IEEEeqnarray*}
  Hier sammeln wir also alle Ergebnisse ein, die $\vphi$ zu einer wahren Aussage machen.
  Wenn $\event{\vphi} \in \SIG$ gilt, dann nennen wir $\vphi$ ein \term{Event}
  und schreiben $\prob{\vphi} \df \PR\p{\event{\vphi}}$.
  \par
  Zum Beispiel haben wir für alle $\ffn{X}{\Om}{\RR}$ und $A \subseteq \RR$ und $u \in \RR$:
  \begin{IEEEeqnarray*}{0l}
    \event{X \in A} = \fnpre{X}(A) \\
    \event{X \leq u} = \event{X \in \intic{u}} = \fnpre{X}(\intic{u})
  \end{IEEEeqnarray*}
  Wenn es sich dabei um Events handelt,
  dann sind $\prob{X \in A}$ \bzw $\prob{X \leq u}$ ihre W'keiten.
\end{para}

\begin{para}[Zufallsvariable, Verteilung]
  Es sei $\ffn{X}{\Om}{\RR}$ eine Funktion so, dass gilt:
  \begin{IEEEeqnarray}{0l}
    \label{wtinf/cha01:ZV}
    \forall A \in \cB \holds \event{X \in A} \in \SIG
  \end{IEEEeqnarray}
  Dann nennen wir $X$ eine \term{Zufallsvariable (ZV)},
  genauer eine \term{ZV über} $(\Om, \SIG, \PR)$.
  Die Menge aller ZV über $(\Om, \SIG, \PR)$ notieren wir mit $\ZV$.
  Für alle $X \in \ZV$ definiere:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{\PR_{X}}{\cB}{\intcc{0}{1}}{A \mapsto \prob{X \in A}}
  \end{IEEEeqnarray*}
  Wir nennen $\PR_{X}$ die \term{Verteilung} von $X$.
  Man sieht leicht, dass $\PR_{X}$ ein W'maß auf dem Eventraum $(\RR, \cB)$ ist.
\end{para}

\begin{satz}[Kriterium für ZV]
  Es sei $\ffn{X}{\Om}{\RR}$. Dann sind äquivalent:
  \begin{enumerate}
  \item $X \in \ZV$
  \item $\forall u \in \RR \holds \event{X \leq u} \in \SIG$
  \end{enumerate}
\end{satz}

\begin{proof}
  \imprefx{1}{2} trivial.\par
  \imprefx{2}{1}
  Definiere $\SIG' \df \set{A \in \RR \suchthat \event{X \in A} \in \SIG}$.
  Man sieht leicht (Übung), dass $\SIG'$ eine \sigalg über $\RR$ ist.
  Nach Voraussetzung gilt $\cG \subseteq \SIG'$.
  Da $\cG$ ein Erzeuger für $\cB$ ist,
  kommt $\SIG'$ also als eine der Mengen vor, die der Bildung von $\cB$ geschnitten werden.
  Es folgt $\cB \subseteq \SIG$ und damit die Behauptung.
\end{proof}

\begin{satz}[Kombinationssatz für ZV]
  Es sei $\vec{X} = (\eli{X}{n}) \in \ZV^{n}$.
  Es sei $\Lam \in \cB^{n}$ so, dass $\img(\vec{X}) \subseteq \Lam$,
  und es sei $\ffn{f}{\Lam}{\RR}$ stetig.
  Dann ist $f \circ \vec{X} \in \ZV$.
  Insbesondere gilt:
  \begin{IEEEeqnarray*}{0l}
    \forall X,Y \in \ZV \innerholds \forall \lam \in \RR \innerholds \forall k \in \NN \holds
    \lam X, \, X^{k}, \, X+Y, \, XY \in \ZV
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}
  %%% TODO
\end{proof}

\begin{satz}
  \label{wtinf/cha01:n-zv}
  Es sei $\vec{X} = (\eli{X}{n}) \in \ZV^{n}$.
  Dann gilt:
  \begin{IEEEeqnarray}{0l}
    \label{wtinf/cha01:n-pre}
    \forall A \in \cB^{n} \holds \event{\vec{X} \in A} \in \SIG
  \end{IEEEeqnarray}
  Somit ist Folgendes ein W'maß auf $(\RR^{n}, \cB^{n})$:
  \begin{IEEEeqnarray*}{0l}
    \ffnx{\PR_{\vec{X}}}{\cB^{n}}{\intcc{0}{1}}{A \mapsto \prob{\vec{X} \in A}}
  \end{IEEEeqnarray*}
\end{satz}

\begin{proof}
  Wir zeigen nur~\eqref{wtinf/cha01:n-pre};
  die Eigenschaften eines W'maßes können danach leicht für $\PR_{\vec{X}}$ überprüft werden.
  Für alle $u \in \RR^{n}$ gilt:
  \begin{IEEEeqnarray*}{0l}
    \event{\vec{X} \leq u}
    = \bigcap_{i=1}^{n} \underbrace{\event{X_{i} \leq u_{i}}}_{\in \SIG}
    \in \SIG
  \end{IEEEeqnarray*}
  Die Behauptung folgt mit einem ähnlichen Argument wie im Beweis des vorigen Satzes,
  da $\cG^{n} \subseteq \set{A \in \RR^{n} \suchthat \event{\vec{X} \in A} \in \SIG}$ gilt.
\end{proof}

\begin{para}[Verteilung]
  Wir nennen $\PR_{\vec{X}}$ die \term{Verteilung} von $\vec{X}$.
  Aus der Verteilung von $\vec{X}$ kann leicht
  die Verteilung jeder Permutation von $\vec{X}$ hergeleitet werden (Übung).
  Kennen wir die Verteilung von $\vec{X}$,
  dann kennen wir zum Beispiel auch die von $(X_{2},X_{1},\elix{X}{3}{n})$.
\end{para}

\begin{para}[CDF]
  \begin{enumerate}
  \item Es sei $X \in \ZV$. Definiere:
    \begin{IEEEeqnarray*}{0l}
      \ffnx{F_{X}}{\RR}{\intcc{0}{1}}{u \mapsto \prob{X \leq u}}
    \end{IEEEeqnarray*}
    Diese Funktion nennen wir die \term{kumulative Verteilungsfunktion} von $X$.
    Als Abkürzung verwenden wir \term{CDF},
    was auf die englische Bezeichnung \enquote{cumulative distribution function} zurückgeht.
  \item Als Verallgemeinerung des vorigen Punktes definiere die CDF von
    $\vec{X} = (\eli{X}{n}) \in \ZV^{n}$ als:
    \begin{IEEEeqnarray*}{0l}
      \ffnx{F_{\vec{X}}}{\RR}{\intcc{0}{1}}{u \mapsto \prob{\vec{X} \leq u}}
    \end{IEEEeqnarray*}
  \end{enumerate}
\end{para}

\begin{satz}[CDF liefert Verteilung]
  \label{wtinf/cha01:cdf->verteilung}
  Die Verteilung von $\vec{X} \in \ZV^{n}$
  ist bereits durch $F_{\vec{X}}$ festgelegt.
  (Daraus folgt natürlich,
  dass für alle $X \in \ZV$ die Verteilung von $X$ bereits durch $F_{X}$ festgelegt ist.
  Dass eine einzelne ZV ein Spezialfall eines Tupels von ZV ist,
  ist offensichtlich, und wird zukünftig meistens nicht mehr erwähnt werden.)
\end{satz}

\begin{proof}
  Hier nicht; Interessierte lesen~\cite[Satz (1.12)]{Georgii09}.
  Als Übung zeige man,
  wie für alle $X \in \ZV$ und alle Intervalle $I \subseteq \RR$
  die W'keit $\prob{X \in I}$ aus $F_{X}$ erschlossen werden kann.
\end{proof}

\begin{para}[Relationen zwischen Formeln]
  Es seien $\vphi, \psi$ logische Formeln über ZV.
  Wir schreiben:
  \begin{itemize}
  \item $\vphi \evimplies \psi$, wenn $\event{\vphi} \subseteq \event{\psi}$ gilt.
    Wir sagen dazu: $\vphi$ \term{impliziert} $\psi$.
  \item $\vphi \eviff \psi$, wenn $\event{\vphi} = \event{\psi}$ gilt.
    Wir sagen dazu: $\vphi$ ist \term{äquivalent} zu $\psi$.
  \item Zur Definition verwenden wir $\evdfiff$.
    Zum Beispiel $\vphi \evdfiff X > 0 \lor X + Y < 0$ mit $X,Y \in \ZV$.
  \item Wir sagen, dass sich $\vphi$ und $\psi$ \term{gegenseitig ausschließen},
    wenn $\event{\vphi \land \psi} = \emptyset$ gilt.
  \item Mit $\top$, gesprochen \enquote{Top}, bezeichnen wir irgendeine logische Formel über ZV,
    für die $\event{\top} = \Om$ gilt.
    Zum Beispiel könnte man $0=0$ nehmen.
  \item Mit $\bot$, gesprochen \enquote{Bottom}, bezeichnen wir irgendeine logische Formel über ZV,
    für die $\event{\bot} = \emptyset$ gilt.
    Zum Beispiel könnte man $0=1$ nehmen.
    Offenbar schließen sich $\vphi$ und $\psi$ genau dann gegenseitig aus,
    wenn $\vphi \land \psi \eviff \bot$ gilt.
  \end{itemize}
\end{para}

\begin{para}[Formeln, die Borelmengen entsprechen]
  In der Menge $\EV$ sammeln wir alle $\vphi$ ein, für die gilt:
  \begin{itemize}
  \item $\vphi$ ist eine logische Formel über ZV.
  \item $\vphi$ enthält höchstens endlich viele ZV, diese seien etwa $\eli{X}{n}$.
  \item Es gibt $A \in \cB^{n}$ so,
    dass $\event{\vphi} = \event{(\eli{X}{n}) \in A}$ gilt.
    Man sieht leicht, dass es dabei nicht auf die Reihenfolge $\eli{X}{n}$ ankommt.
  \end{itemize}
  Wir wissen nach \autoref{wtinf/cha01:n-zv},
  dass für alle $\vphi \in \EV$
  die Menge $\event{\vphi}$ ein Event ist im Sinne einer Teilmenge von~$\Om$ ist,
  und somit ist die W'keit $\prob{\vphi}$ definiert.
\end{para}

\begin{satz}[Beispiele für Formeln in $\EV$]
  \begin{enumerate}
  \item \label{wtinf/cha01:ev:topbot}
    Es gilt $\top, \bot \in \EV$.
  \item\label{wtinf/cha01:ev:int}
    Für alle $X \in \ZV$ und alle Intervalle $A \in \cB$ gilt $\p{X \in A} \in \EV$.
    (Die Klammern setzen wir hier und in ähnlichen Situationen nur zur besseren Lesbarkeit.)
  \item\label{wtinf/cha01:ev:neg}
    Für alle $\vphi \in \EV$ gilt $\p{\neg \vphi} \in \EV$.
    Es enthält $\EV$ also zu jeder Formel aus $\EV$ auch die entsprechende negierte Formel.
  \item\label{wtinf/cha01:ev:and}
    Für alle $\vphi, \psi \in \EV$ gilt $\p{\vphi \land \psi} \in \EV$.
    Es enthält $\EV$ also zu jeden zwei Formeln aus $\EV$ auch die Und\-/Verknüpfung dieser beiden Formeln.
  \item\label{wtinf/cha01:ev:or}
    Für alle $\vphi, \psi \in \EV$ gilt $\p{\vphi \lor \psi} \in \EV$.
    Es enthält $\EV$ also zu jeden zwei Formeln aus $\EV$ auch die Oder\-/Verknüpfung dieser beiden Formeln.
  \item\label{wtinf/cha01:ev:iff}
    Für alle $\vphi \in \EV$ und alle logische Formeln über ZV $\psi$,
    für die $\vphi \eviff \psi$ gilt, gilt~${\psi \in \EV}$.
    Es enthält $\EV$ also zu jeder Formel auch alle dazu äquivalenten Formeln.
  \end{enumerate}
\end{satz}

\begin{proof}
  Übung.
\end{proof}

\begin{para}[Allgemeinerer Verteilungsbegriff]
  Es sei $\MM \subseteq \ZV$ endlich.
  Mit ${\EV(\MM) \subseteq \EV}$ bezeichnen wir diejenigen Events aus $\EV$,
  in denen keine anderen ZV außer denen in $\MM$ vorkommen.
  Wir nennen die Information, was $\prob{\vphi}$ für alle $\vphi \in \EV(\MM)$ ist,
  die \term{Verteilung} von $\MM$.
  Im Spezialfall $\MM=\set{X}$ sprechen wir auch von der Verteilung der ZV~$X$.
  \par
  Wir ordnen die Elemente von $\MM$ an, etwa $\MM = \set{\eli{X}{n}}$.
  Aus \autoref{wtinf/cha01:cdf->verteilung} folgt (Übung),
  dass die Verteilung von $\MM$ bereits durch $F_{(\eli{X}{n})}$ festgelegt ist.
  Daran sieht man auch, dass die Verteilung von $\MM$
  die Verteilung für jede Teilmenge $\MM' \subseteq \MM$ festlegt.
\end{para}

\begin{para}[Konditionierung]
  Es seien $\vphi, \bet \in \EV$ so, dass $\prob{\bet} > 0$ gilt.
  Wir definieren die \term{W'keit von $\vphi$ konditioniert auf $\bet$},
  oder \term{W'keit von $\vphi$ gegeben $\bet$}:
  \begin{IEEEeqnarray*}{0l}
    \prob{\vphi \cond \bet} \df \frac{\prob{\vphi \land \bet}}{\prob{\bet}}
  \end{IEEEeqnarray*}
  Offenbar gilt $\prob{\vphi \cond \top} = \prob{\vphi}$;
  die bisherigen W'keiten waren also Spezialfälle von konditionierten W'keiten.
\end{para}

\begin{satz}[Umgang mit W'keiten]
  \label{wtinf/cha01:kond-wkeit}
  Es sei $\bet \in \EV$ so, dass $\prob{\bet} > 0$ gilt.
  Dann gilt:
  \begin{enumerate}
  \item $\prob{\top \cond \bet} = 1$ und $\prob{\bot \cond \bet} = 0$
  \item\label{wtinf/cha01:kond-wkeit:diff}
    $\forall \vphi, \psi \in \EV \holds
    \vphi \evimplies \psi \implies \prob{\psi \land \neg \vphi \cond \bet}
    = \prob{\psi \cond \bet} - \prob{\vphi \cond \bet}$
  \item $\forall \vphi, \psi \in \EV \holds \vphi \evimplies \psi
    \implies \prob{\psi \cond \bet} \leq \prob{\vphi \cond \bet}$
  \item $\forall \vphi \in \EV \holds \prob{\neg \vphi \cond \bet} = 1 - \prob{\vphi \cond \bet}
    \,\land\, \prob{\vphi \cond \bet} = 1 - \prob{\neg \vphi \cond \bet}$
  \item $\forall \vphi, \psi \in \EV \holds \prob{\vphi \lor \psi \cond \bet}
    = \prob{\vphi \cond \bet} + \prob{\psi \cond \bet} - \prob{\vphi \land \psi \cond \bet}$
    \par
    Insbesondere gilt $\prob{\vphi \lor \psi \cond \bet} = \prob{\vphi \cond \bet} + \prob{\psi \cond \bet}$,
    wenn $\prob{\vphi \land \psi \cond \bet} = 0$ gilt,
    was insbesondere der Fall ist, wenn sich $\vphi$ und $\psi$ gegenseitig ausschließen.
  \item $\forall \MM \subseteq \EV \with \text{endlich} \holds
    \prob{\bigvee_{\vphi \in \MM} \vphi \cond \bet} \leq \sum_{\vphi \in \MM} \prob{\vphi \cond \bet}$
    \par
    Gleichheit gilt, wenn sich die Events in $\MM$ gegenseitig ausschließen.
  \end{enumerate}
\end{satz}

\begin{proof}
  Übung; man bearbeite zunächst den Fall $\bet \eviff \top$.
  \autoref{wtinf/cha01:wraum} hilft.
\end{proof}
